//WebExportDatasetToExcel.cs
//Copyright (C) 2011  The Outercurve Foundation

//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// Ron Fluegge
// GADS Open Source Project Coordinator
// Coordinator@GADSOpenSource.com
// 972-625-5653

using System;
using System.Data;
using System.Text;
using System.Web;

namespace WebExportDatasetToExcel
{
	
	public class Export
	{

		private static HttpResponse Response
		{
			get { return HttpContext.Current.Response; }
		}

		public static void DataSetToExcel( DataSet Data )
		{

			Response.Clear();
			Response.ContentType = "application/ms-excel";
			Response.AddHeader( "Content-Disposition",  "Filename=" + Data.DataSetName + ".xls" );
		
			WriteWorkbookHeader();
			
			foreach( DataTable table in Data.Tables )
				WriteTable( table );

			WriteWorkbookFooter();
			Response.End();
		
		}

		private static void WriteWorkbookHeader()
		{

			Response.Write( "<?xml version=\"1.0\"?>\r\n" );
			Response.Write( "<?mso-application progid=\"Excel.Sheet\"?>\r\n" );
			Response.Write( "<Workbook xmlns=\"urn:schemas-microsoft-com:office:spreadsheet\"\r\n" );
			Response.Write( "xmlns:o=\"urn:schemas-microsoft-com:office:office\"\r\n" );
			Response.Write( "xmlns:x=\"urn:schemas-microsoft-com:office:excel\"\r\n" );
			Response.Write( "xmlns:ss=\"urn:schemas-microsoft-com:office:spreadsheet\"\r\n" );
			Response.Write( "xmlns:html=\"http://www.w3.org/TR/REC-html40\">\r\n" );
			Response.Write( "<DocumentProperties xmlns=\"urn:schemas-microsoft-com:office:office\">\r\n" );
			Response.Write( "<LastAuthor>MSINC</LastAuthor>\r\n" );
			Response.Write( "  <Created>" + DateTime.Now.ToString() + "</Created>\r\n" );
			Response.Write( "  <Version>11.5703</Version>\r\n" );
			Response.Write( "</DocumentProperties>\r\n" );
			Response.Write( "<ExcelWorkbook xmlns=\"urn:schemas-microsoft-com:office:excel\">\r\n" );
			Response.Write( "  <ProtectStructure>False</ProtectStructure>\r\n" );
			Response.Write( "  <ProtectWindows>False</ProtectWindows>\r\n" );
			Response.Write( "</ExcelWorkbook>\r\n" );
			Response.Write( " <Styles>\r\n" );
			Response.Write( "  <Style ss:ID=\"s1\">\r\n" );
			Response.Write( "   <Font ss:Bold=\"1\"/>\r\n" );
			Response.Write( "  </Style>\r\n" );
			Response.Write( " </Styles>\r\n" );

		}

		private static void WriteWorkbookFooter()
		{
			Response.Write( "</Workbook>\r\n" );
		}

		private static void WriteTableCaption( string tableName, int colSpan )
		{
			Response.Write( "<Row>\r\n" );
			Response.Write( "<Column colspan='" + colSpan + "'>" + tableName + "</Column>\r\n" );
			Response.Write( "</Row>\r\n" );
		}

		private static void WriteTableHeader( DataTable table )
		{

			foreach( DataColumn column in table.Columns )
				Response.Write( "<Column>" + column.ColumnName + "</Column>\r\n" );

			Response.Write( "<Row>\r\n" );
		
			foreach( DataColumn column in table.Columns )
				Response.Write( "<Cell ss:StyleID=\"s1\"><Data ss:Type=\"String\">" + column.ColumnName + "</Data></Cell>\r\n" );

			Response.Write( "</Row>\r\n" );

		}

		private static void WriteTable( DataTable table )
		{

			Response.Write( "<Worksheet ss:Name='" + table.TableName + "'>\r\n" );
			Response.Write( "<Table ss:ExpandedColumnCount=\"" + table.Columns.Count + "\" ss:ExpandedRowCount=\"" + (table.Rows.Count + 1) + "\" x:FullColumns=\"1\" x:FullRows=\"1\">\r\n" );
			WriteTableHeader( table );
			WriteTableRows( table );
			Response.Write( "</Table>\r\n" );
			Response.Write( "</Worksheet>\r\n" );			

		}

		private static void WriteTableRows( DataTable table )
		{
			foreach( DataRow Row in table.Rows )
				WriteTableRow( Row );
		}

		private static bool IsNumber( string Value )
		{

			if( Value == "" )
				return false;

			char[] chars = Value.ToCharArray();

			foreach( char ch in chars )
			{
				if( ch != '$' && ch != '.' && ch != ',' && !char.IsNumber( ch ) )
					return false;
			}

			return true;

		}

		private static string GetExcelType( object Value )
		{

			if( Value == null || Value == DBNull.Value || Value is string )
				return "String";
				//			else if( Value is DateTime )
				//				return "Date";
			else if( IsNumber( Value.ToString() ) )
				return "Number";
			else
				return "String";

		}

		private static void WriteTableRow( DataRow Row )
		{

			Response.Write( "<Row>\r\n" );

			foreach( object loop in Row.ItemArray )
			{

				Response.Write( "<Cell><Data ss:Type=\"" + GetExcelType( loop ) + "\">" );

				if( loop != null && loop != DBNull.Value )
				{

					if( loop is byte[] )
						Response.Write( "(...)" );
					else if( loop is decimal )
					{
						Response.Write( ((decimal) loop).ToString() );
					}
					else if( loop is DateTime )
					{
						Response.Write( ((DateTime) loop).ToString( "yyyy-MM-dd HH:mm:ss" ) );
					}
					else
					{
						Response.Write( loop.ToString() );
					}

				}

				Response.Write( "</Data></Cell>\r\n" );

			}

			Response.Write( "</Row>\r\n" );

		}

	}

}
