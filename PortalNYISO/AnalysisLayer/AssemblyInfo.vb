Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("GADS OS AR Analysis Layer")> 
<Assembly: AssemblyDescription("GADS Open Source Analysis and Reporting Analysis Layer")> 
<Assembly: AssemblyCompany("GADS Open Source Project")> 
<Assembly: AssemblyProduct("GADS Analysis and Reporting")> 
<Assembly: AssemblyCopyright("Copyright � 2002-2015, The Outercurve Foundation All rights reserved.")> 
<Assembly: AssemblyTrademark("GADS Open Source is a service mark of GADS Open Source Project")>
<Assembly: CLSCompliant(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("CF63779B-B6AA-497D-AD8B-39479440196E")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:

<Assembly: AssemblyVersion("12.0.0.0")> 
<Assembly: ComVisibleAttribute(False)> 
<Assembly: AssemblyFileVersionAttribute("16.1.8.0")> 