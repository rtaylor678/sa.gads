'********************************************************************************
' Base.vb

'Copyright (C) 2011  The Outercurve Foundation

'This program is free software; you can redistribute it and/or
'modify it under the terms of the GNU General Public License
'as published by the Free Software Foundation; either version 2
'of the License, or (at your option) any later version.

'This program is distributed in the hope that it will be useful,
'but WITHOUT ANY WARRANTY; without even the implied warranty of
'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'GNU General Public License for more details.

'You should have received a copy of the GNU General Public License
'along with this program; if not, write to the Free Software
'Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

' Ron Fluegge
' GADS Open Source Project Coordinator
' Coordinator@GADSOpenSource.com
' 972-625-5653      
'********************************************************************************
Option Strict On
Option Explicit On

Imports System.IO
Imports System.Xml
Imports System.Runtime.Serialization
Imports SetupSettings
Imports Microsoft.DSG.Security.CryptoServices
Imports Microsoft.ApplicationBlocks.ExceptionManagement
Imports GADSNG.DAL

Namespace GADSNG.Base

    'FOLDERNAME x:\Data Factory\code\DataFactory\base\
    Public MustInherit Class DALBase

        Private myDF As DataFactory
        Protected Provider As String
        Protected Connect As String
        Protected UserID As String
        Protected DataSource As String
        Protected Password As String
        Protected GADSNGTableName As String
        Protected myDataSource As String
        Protected myCatalog As String = "GADSNG"
        Protected myPassword As String = "GADSNG"
        Protected myUserID As String = "GADSNG"
        Protected myDBType As String
        Protected myConnect As String
        Protected moXML As Settings
        Protected AppName As String = "GADSNG"

        Protected myOracleServers As String
        Protected myInstanceName As String
        Protected myServerName As String
        Protected myServiceName As String
        Protected myProtocol As String
        Protected myPort As String

        ' The key and IV must be no larger than 192 bits/24 bytes.  Refer to the TripleDES documentation
        ' When using ASCII encoding, the key and IV strings may be no larger than 24 characters.
        ' When using Unicode encoding, the key and IV strings may be no larger than 12 characters.
        ' The key and IV specified are 12 characters each for Unicode encoding.
        Protected EncryptionKey As String = "F3l1u4e1g5g9e2G6r5o3u5p9"
        Protected EncryptionIV As String = "3F1l4u1e5g9g2e6G5r3o5u9p"

        Protected Sub New(ByVal connect As String)
            ' connect should be either "Setup", "Admin", "DataEntry" or "Analysis"
            _initClass(connect, Nothing)
        End Sub

        Protected Sub New(ByVal connect As String, ByRef reader As StreamReader)
            ' connect should be either "Setup", "Admin", "DataEntry" or "Analysis"
            _initClass(connect, reader)
        End Sub

        Private Sub _initClass(ByVal connect As String, ByRef reader As StreamReader)

            ' connect should be either "DataEntry" or "Analysis"

            ' on instantiation, save XML filename and open the XmlDocument
            ' Public Sub New(ByVal AppName As String)

            'default to appname.xml in app.path
            ' reader is for XBAP ONLY

            Try

                If reader Is Nothing Then

                    Select Case connect.ToUpper

                        Case "ANALYSIS"

                            moXML = New Settings(AppName)

                        Case "DATAENTRY"

                            moXML = New Settings(AppName)

                        Case "SETUP"

                            ' I do not find "Setup" used anywhere, but it is referenced
                            moXML = New Settings(AppName)

                        Case "ADMIN"

                            ' I do not find "Admin" used anywhere, but it is referenced
                            moXML = New Settings(AppName)

                        Case Else

                            ' the below "connect" is where the root web is installed
                            ' for web UI the GADSNG.xml file needs to go into the "Secure" folder

                            'If connection to a remote server (via IP address):

                            '	oSQLConn.ConnectionString = "Network Library=DBMSSOCN;" & _
                            '								"Data Source=xxx.xxx.xxx.xxx,1433;" & _
                            '								"Initial Catalog=mySQLServerDBName;" & _
                            '								"User ID=myUsername;" & _
                            '								"Password=myPassword"
                            'Where:
                            '- "Network Library=DBMSSOCN" tells SqlConnection to use TCP/IP Q238949
                            '- xxx.xxx.xxx.xxx is an IP address.  
                            '- 1433 is the default port number for SQL Server.  Q269882 and Q287932
                            '- You can also add "Encrypt=yes" for encryption 

                            ' For WebHost4Life the Data Source is 207.182.248.25,1433 in GADSNG.XML

                            ' SettingsFile specifies filename & optional path, path defaults to CurrentDirectory

                            ' Public Sub New(ByVal AppName As String, ByVal SettingsFile As String)

                            If connect.IndexOf("/") > 0 Then

                                If Not connect.EndsWith("/") Then
                                    connect += "/"
                                End If

                                moXML = New Settings(AppName, connect & "Secure/GADSNG.xml")

                            ElseIf connect.IndexOf("\") > 0 Then

                                If Not connect.EndsWith("\") Then
                                    connect += "\"
                                End If

                                moXML = New Settings(AppName, connect & "Secure\GADSNG.xml")

                            Else

                                Dim xmlTR As New XmlTextReader("Secure/GADSNG.xml")

                            End If

                    End Select

                Else

                    moXML = New Settings(AppName, Nothing, reader)

                End If

                myDataSource = moXML.GetSettingStr("Connection", "DataSource", "GADSNG.MDB")
                myDBType = moXML.GetSettingStr("Connection", "DBType", "OleDb")

                If myDBType = "ODBC" Then
                    myDBType = "OleDb"   ' Force OleDb if GADSNG.XML has ODBC
                    moXML.SaveSettingStr("Connection", "DBType", "OleDb")
                End If

                myPassword = Decrypt(moXML.GetSettingStr("Connection", "Kennwort", Encrypt("GADSNG"))).Trim
                myUserID = Decrypt(moXML.GetSettingStr("Connection", "UserID", Encrypt("GADSNG"))).Trim

                Select Case myDBType.ToUpper

                    Case "SQLCLIENT"

                        myDBType = "SqlClient"

                        myCatalog = moXML.GetSettingStr("Connection", "Catalog", "GADSNG")

                        myConnect = "data source=" & myDataSource & ";initial catalog=" & myCatalog.Trim() & ";User ID=" & myUserID & ";Password=" & myPassword & ";Connect Timeout=90;Min Pool Size=1;"

                        ' Connection pooling behavior is controlled by the connection string parameters. 
                        ' The following are four parameters that control most of the connection pooling behavior: 

                        ' Connect Timeout - controls the wait period in seconds when a new connection is 
                        ' requested, if this timeout expires, an exception will be thrown. Default is 15 seconds. 

                        ' Max Pool Size - specifies the maximum size of your connection pool. Default is 100. 
                        ' Most Web sites do not use more than 40 connections under the heaviest load but it 
                        ' depends on how long your database operations take to complete. 

                        ' Min Pool Size - initial number of connections that will be added to the pool upon 
                        ' its creation. Default is zero; however, you may choose to set this to a small number 
                        ' such as 5 if your application needs consistent response times even after it was idle 
                        ' for hours. In this case the first user requests won't have to wait for those database 
                        ' connections to establish. 

                        ' Pooling - controls if your connection pooling on or off. 
                        ' Default as you may've guessed is true. Read on to see when you may use Pooling=false 
                        ' setting. 

                    Case "ORACLE"

                        myDBType = "Oracle"

                        myInstanceName = moXML.GetSettingStr("Connection", "InstanceName", " ")
                        myServerName = moXML.GetSettingStr("Connection", "ServerName", " ")
                        myServiceName = moXML.GetSettingStr("Connection", "ServiceName", " ")
                        myProtocol = moXML.GetSettingStr("Connection", "Protocol", " ")
                        myPort = moXML.GetSettingStr("Connection", "Port", " ")

                        If myInstanceName.Trim = String.Empty Or _
                            myServerName.Trim = String.Empty Or _
                            myServiceName.Trim = String.Empty Or _
                            myProtocol.Trim = String.Empty Or _
                            myPort.Trim = String.Empty Then

                            myConnect = "Data Source=" & myDataSource & ";User ID=" & myUserID & ";Password=" & myPassword

                        Else
                            myConnect = "Data Source=(DESCRIPTION=" & _
                                    "(ADDRESS_LIST=(ADDRESS=(PROTOCOL=" & myProtocol.Trim() & ")(HOST=" & myServerName.Trim() & ")(PORT=" & myPort.Trim() & ")))" & _
                                    "(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=" & myServiceName.Trim() & ")));" & _
                                    "User Id=" & myUserID.Trim & ";Password=" & myPassword
                        End If


                    Case "OLEDB"

                        myDBType = "OleDb"

                        'myConnect = "Provider=Microsoft.Jet.OLEDB.4.0;Password="""";User ID=Admin;Data Source=" & myDataSource
                        myConnect = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & myDataSource & ";Persist Security Info=False;"

                    Case "ODBC"

                        'myConnect = "DSN=GADSNG;User=" & myUserID & ";Password=" & myPassword & ";Database=GADSNG"
                        myConnect = "Driver={Microsoft Access Driver (*.mdb)};Dbq=" & myDataSource & ";Uid=Admin;Pwd=;"

                    Case Else

                        ' Assuming that if the GADSNG file is missing or no value, then use the local GADSNG.MDB file
                        myDataSource = "GADSNG.MDB"
                        myDBType = "OleDb"
                        myConnect = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & myDataSource & ";Persist Security Info=False;"

                End Select

                Me.Connect = myConnect
                Me.Provider = myDBType
                Me.UserID = myUserID
                Me.Password = myPassword
                Me.DataSource = myDataSource

                myDF = New DataFactory(Me.Connect, Me.Provider)

                myDF.Password = myPassword
                myDF.UserID = myUserID
                myDF.DataSource = myDataSource

            Catch ex As System.Exception
                'Me.ThrowGADSNGException("Error in Base _initClass", ex)
                Me.ThrowGADSNGException("Error in Base _initClass: " + ex.ToString + ex.InnerException.ToString, ex)
            End Try

        End Sub

        Protected ReadOnly Property Factory() As DataFactory
            Get
                Return myDF
            End Get
        End Property

        Public ReadOnly Property ThisProvider() As String
            Get
                Return Me.Provider
            End Get
        End Property

        Protected Sub ThrowGADSNGException(ByVal message As String, ByVal e As System.Exception)

            Dim newMine As New GADSNGException(message, e)

            ' Throw
            Throw newMine

        End Sub

#Region " Public encryption methods "

        Public Function Encrypt(ByVal aString As String) As String
            ' Note that the key and IV must be the same for the encrypt and decrypt calls.
            Dim results As String

            Try
                Dim tdesEngine As New TDES(EncodingType.ASCIIEncoding)
                tdesEngine.StringKey = EncryptionKey
                tdesEngine.StringIV = EncryptionIV
                results = tdesEngine.Encrypt(aString)
            Catch anError As System.Exception

                Me.ThrowGADSNGException("Error in Encrypt" & vbCrLf & "An exception occurred during encryption." & vbCrLf & _
                 "Error:" & vbTab + anError.Message & vbCrLf & "Source:" + vbTab + anError.Source.ToString, _
                  anError)
                results = ""

            End Try

            Return (results)

        End Function

        Public Function Decrypt(ByVal aString As String) As String
            ' Note that the key and IV must be the same for the encrypt and decript calls.
            Dim results As String

            Try
                Dim tdesEngine As New TDES(EncodingType.ASCIIEncoding)
                tdesEngine.StringKey = EncryptionKey
                tdesEngine.StringIV = EncryptionIV
                results = tdesEngine.Decrypt(aString)
            Catch anError As System.Exception

                Me.ThrowGADSNGException("Error in Decrypt" & vbCrLf & _
                "An exception occurred during decryption." + vbCrLf + _
                 "Error:" + vbTab + anError.Message + vbCrLf + _
                 "Source:" + vbTab + anError.Source.ToString, _
                 anError)
                results = ""
            End Try

            Return (results)

        End Function

#End Region

    End Class

    <Serializable()> _
    Public Class GADSNGException : Inherits BaseApplicationException

        Public ConnectString As String
        Public Provider As String

        Public Sub New()
            MyBase.New()
        End Sub

        Public Sub New(ByVal message As String)
            MyBase.New(message)
        End Sub

        Public Sub New(ByVal message As String, ByVal innerException As System.Exception)
            MyBase.New(message, innerException)
        End Sub

        Public Sub New(ByVal info As SerializationInfo, ByVal context As StreamingContext)
            MyBase.New(info, context)
        End Sub

    End Class

End Namespace
