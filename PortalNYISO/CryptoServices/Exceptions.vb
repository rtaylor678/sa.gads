'********************************************************************************
' Exceptions.vb

'Copyright (C) 2011  The Outercurve Foundation

'This program is free software; you can redistribute it and/or
'modify it under the terms of the GNU General Public License
'as published by the Free Software Foundation; either version 2
'of the License, or (at your option) any later version.

'This program is distributed in the hope that it will be useful,
'but WITHOUT ANY WARRANTY; without even the implied warranty of
'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'GNU General Public License for more details.

'You should have received a copy of the GNU General Public License
'along with this program; if not, write to the Free Software
'Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

' Ron Fluegge
' GADS Open Source Project Coordinator
' Coordinator@GADSOpenSource.com
' 972-625-5653                                                  
'********************************************************************************
Imports System

' Add Serializable attributes...
Public Class InvalidKeyLengthException
    Inherits System.ApplicationException
    'Custom exception class for invalid key values.

#Region " Constructors "
    Public Sub New()
        'Default constructor.
    End Sub

    Public Sub New(ByVal aMessage As String)
        MyBase.New(aMessage)
    End Sub

    Public Sub New(ByVal aMessage As String, ByVal innerException As System.Exception)
        MyBase.New(aMessage, innerException)
    End Sub
#End Region
End Class

' Add Serializable attributes...
Public Class InvalidIVLengthException
    Inherits System.ApplicationException
    'Custom exception class for invalid IV values.

#Region " Constructors "
    Public Sub New()
        'Default constructor.
    End Sub

    Public Sub New(ByVal aMessage As String)
        MyBase.New(aMessage)
    End Sub

    Public Sub New(ByVal aMessage As String, ByVal innerException As System.Exception)
        MyBase.New(aMessage, innerException)
    End Sub
#End Region
End Class

'Add additional custom exception classes...