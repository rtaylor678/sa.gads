'********************************************************************************
' Triple Data Encryption Standard (Triple-DES)
' TDES.vb

'Copyright (C) 2011  The Outercurve Foundation

'This program is free software; you can redistribute it and/or
'modify it under the terms of the GNU General Public License
'as published by the Free Software Foundation; either version 2
'of the License, or (at your option) any later version.

'This program is distributed in the hope that it will be useful,
'but WITHOUT ANY WARRANTY; without even the implied warranty of
'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'GNU General Public License for more details.

'You should have received a copy of the GNU General Public License
'along with this program; if not, write to the Free Software
'Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

' Ron Fluegge
' GADS Open Source Project Coordinator
' Coordinator@GADSOpenSource.com
' 972-625-5653                                                  
'********************************************************************************

Imports System
Imports System.IO
Imports System.Security.Cryptography

#Region " Public Enumerations "
Public Enum EncodingType
    UnicodeEncoding = 0
    ASCIIEncoding = 1
End Enum
#End Region

Public Class TDES

#Region " Private Member Variables "
    Private myCryptoProvider As TripleDESCryptoServiceProvider
    Private myEncodingType As EncodingType
    Private myEncoder As System.Text.Encoding
    Private myKey As Byte()
    Private myIV As Byte()
    Private stringMyKey As String
    Private stringMyIV As String

#End Region

#Region " Constructors "
    Private Sub New()
        'Disallow the default constructor.
    End Sub

    Public Sub New(ByVal Encoding As EncodingType)
        'Construct class with requested encoding type.
        Me.CryptoProvider = New TripleDESCryptoServiceProvider()
        Me.Encoding = Encoding
    End Sub

    Public Sub New(ByVal Encoding As EncodingType, ByVal Key As String, ByVal IV As String)
        'Construct class with requested encoding type, key, and IV.
        Me.CryptoProvider = New TripleDESCryptoServiceProvider()
        Me.Encoding = Encoding
        Me.StringKey = Key
        Me.StringIV = IV
    End Sub

    Public Sub New(ByVal Encoding As EncodingType, ByVal Key As Byte(), ByVal IV As Byte())
        'Construct class with requested encoding type, key, and IV.
        Me.CryptoProvider = New TripleDESCryptoServiceProvider()
        Me.Encoding = Encoding
        Me.Key = Key
        Me.IV = IV
    End Sub
#End Region

#Region " Public Properties "
    Public Property Encoding() As EncodingType
        Get
            Return (myEncodingType)
        End Get
        Set(ByVal Value As EncodingType)
            myEncodingType = Value

            'Instantiate the appropriate encoding class.
            If Value = EncodingType.UnicodeEncoding Then
                Me.Encoder = New System.Text.UnicodeEncoding()
            Else
                Me.Encoder = New System.Text.ASCIIEncoding()
            End If
        End Set
    End Property

    Public Property Key() As Byte()
        Get
            Return (myKey)
        End Get
        Set(ByVal Value As Byte())
            'Validate the key size.
            If ValidKeySize(Value) Then
                myKey = Value
            Else
                Throw New CryptoServices.InvalidKeyLengthException("The key must be " + (Me.CryptoProvider.KeySize / 8).ToString + " bytes in size.")
            End If
        End Set
    End Property

    Public Property StringKey() As String
        Get
            Return (Convert.ToString(myKey))
        End Get
        Set(ByVal Value As String)
            'Validate the key size.
            If ValidKeySize(Value) Then
                myKey = Me.Encoder.GetBytes(Value)
            Else
                Throw New CryptoServices.InvalidKeyLengthException("The key must be " + (Me.CryptoProvider.KeySize / 8).ToString + " bytes in size.")
            End If
        End Set
    End Property

    Public Property IV() As Byte()
        Get
            Return (myIV)
        End Get
        Set(ByVal Value As Byte())
            'Validate the IV size.
            If ValidKeySize(Value) Then
                myIV = Value
            Else
                Throw New CryptoServices.InvalidIVLengthException("The initialization vector must be " + (Me.CryptoProvider.KeySize / 8).ToString + " bytes in size.")
            End If
        End Set
    End Property

    Public Property StringIV() As String
        Get
            Return (Convert.ToString(myIV))
        End Get
        Set(ByVal Value As String)
            'Validate the IV size.
            If ValidKeySize(Value) Then
                myIV = Me.Encoder.GetBytes(Value)
            Else
                Throw New CryptoServices.InvalidIVLengthException("The initialization vector must be " + (Me.CryptoProvider.KeySize / 8).ToString + " bytes in size.")
            End If
        End Set
    End Property
#End Region

#Region " Private Properties "
    Private Property CryptoProvider() As TripleDESCryptoServiceProvider
        Get
            Return (myCryptoProvider)
        End Get
        Set(ByVal Value As TripleDESCryptoServiceProvider)
            myCryptoProvider = Value
        End Set
    End Property

    Private Property Encoder() As System.Text.Encoding
        Get
            Return (myEncoder)
        End Get
        Set(ByVal Value As System.Text.Encoding)
            myEncoder = Value
        End Set
    End Property
#End Region

#Region " Public Methods "
    ' Implement file encryption and decryption methods...
    Public Function Encrypt(ByVal StringToEncrypt As String) As String
        'Forward the encryption call to the private encrypt method.
        ' Trap error conditions (no key or IV) and throw a custom exception.
        Return (Me.Encrypt(myEncoder.GetBytes(StringToEncrypt), Me.Key, Me.IV))
    End Function

    Public Function Encrypt(ByVal StringToEncrypt As String, ByVal Key As String, ByVal IV As String) As String
        'Check the key and IV size and forward the encryption call to the private encrypt method if they are valid.
        If ValidKeySize(Key) Then
            If ValidKeySize(IV) Then
                Return (Me.Encrypt(myEncoder.GetBytes(StringToEncrypt), myEncoder.GetBytes(Key), myEncoder.GetBytes(IV)))
            Else
                Throw New CryptoServices.InvalidIVLengthException("The initialization vector must be " + (Me.CryptoProvider.KeySize / 8).ToString + " bytes in size.")
            End If
        Else
            Throw New CryptoServices.InvalidKeyLengthException("The key must be " + (Me.CryptoProvider.KeySize / 8).ToString + " bytes in size.")
        End If
    End Function

    Public Function Decrypt(ByVal StringToDecrypt As String) As String
        'Forward the decryption call to the private decrypt method.
        ' Trap error conditions (no key or IV) and throw a custom exception.
        Return (Me.Decrypt(System.Convert.FromBase64String(StringToDecrypt), Me.Key, Me.IV))
    End Function

    Public Function Decrypt(ByVal StringToDecrypt As String, ByVal Key As String, ByVal IV As String) As String
        'Check the key and IV size and forward the decryption call to the private decrypt method if they are valid.
        If ValidKeySize(Key) Then
            If ValidKeySize(IV) Then
                Return (Me.Decrypt(System.Convert.FromBase64String(StringToDecrypt), myEncoder.GetBytes(Key), myEncoder.GetBytes(IV)))
            Else
                Throw New CryptoServices.InvalidIVLengthException("The initialization vector must be " + (Me.CryptoProvider.KeySize / 8).ToString + " bytes in size.")
            End If
        Else
            Throw New CryptoServices.InvalidKeyLengthException("The key must be " + (Me.CryptoProvider.KeySize / 8).ToString + " bytes in size.")
        End If
    End Function
#End Region

#Region " Private Methods "
    ' Implement file encryption and decryption methods...
    Private Function ValidKeySize(ByVal aKey As Byte()) As Boolean
        'Determine whether the key is of a valid length or not.
        Return (myCryptoProvider.ValidKeySize(aKey.Length * 8))
    End Function

    Private Function ValidKeySize(ByVal aKey As String) As Boolean
        'Determine whether the key is of a valid length or not.
        Return (myCryptoProvider.ValidKeySize(Me.Encoder.GetBytes(aKey).Length * 8))
    End Function

    Private Function Encrypt(ByVal StringToEncrypt As Byte(), ByVal Key As Byte(), ByVal IV As Byte()) As String
        'Instantiate the encryptor.
        Dim anEncryptor As ICryptoTransform = Me.CryptoProvider.CreateEncryptor(Key, IV)

        'Instantiate a stream object to recieve the encrypted data.
        Dim aMemoryStream As MemoryStream = New MemoryStream()

        'Instantiate a cryptographical stream to encrypt the data.
        Dim aCryptoStream As CryptoStream = New CryptoStream(aMemoryStream, anEncryptor, CryptoStreamMode.Write)

        'Write the string to encrypt to the encrypted stream
        aCryptoStream.Write(StringToEncrypt, 0, StringToEncrypt.Length)
        aCryptoStream.FlushFinalBlock()

        'Return the encrypted output to the caller as a string. Base64 encode the string.
        Dim aBuffer As Byte() = aMemoryStream.GetBuffer()
        Dim bufferLength As Integer = StringLength(aBuffer)
        Dim encryptedValue As String = System.Convert.ToBase64String(aBuffer, 0, bufferLength)

        'Close the streams.
        aMemoryStream.Close()

        Return (encryptedValue)
    End Function

    Private Function Decrypt(ByVal StringToDecrypt As Byte(), ByVal Key As Byte(), ByVal IV As Byte()) As String
        'Instantiate the decryptor.
        Dim aDecryptor As ICryptoTransform = Me.CryptoProvider.CreateDecryptor(Key, IV)

        'Instantiate a stream object to recieve the decrypted data.
        Dim aMemoryStream As New MemoryStream(StringToDecrypt, 0, StringToDecrypt.Length)

        'Instantiate a cryptographical stream to decrypt the data.
        Dim oCryptoStream As New CryptoStream(aMemoryStream, aDecryptor, CryptoStreamMode.Read)

        'Read the result from the cryptographical stream.
        Dim aReader As New System.IO.StreamReader(oCryptoStream, Me.Encoder)
        Dim decryptedValue As String = aReader.ReadToEnd()

        'Close the streams.
        aReader.Close()
        aMemoryStream.Close()

        Return (decryptedValue)
    End Function

    Private Function StringLength(ByVal someData As Byte()) As Integer
        Dim lengthIterator As Integer = 0

        'Determine the length of the data in the memory buffer.
        'The cryptography providers work in blocks, so the block may not be full.
        For lengthIterator = (someData.Length - 1) To 0 Step -1
            If someData(lengthIterator) <> 0 Then
                Exit For
            End If
        Next lengthIterator

        Return (lengthIterator + 1)
    End Function
#End Region

End Class