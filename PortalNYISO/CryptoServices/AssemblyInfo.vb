Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Cryptography Service Provider")> 
<Assembly: AssemblyDescription("Provides encrypted strings using TripleDES")> 
<Assembly: AssemblyCompany("GADS Open Source Project")> 
<Assembly: AssemblyProduct("CryptoServices")> 
<Assembly: AssemblyCopyright("Copyright � 2011 by The Outercurve Foundation, All Rights Reserved.")> 
<Assembly: AssemblyTrademark("GADS Open Source is a trademark of GADS Open Source")>
<Assembly: CLSCompliant(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("26296BF8-0BB1-444B-961C-5437E996EA17")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:

<Assembly: AssemblyVersion("12.0.0.0")> 

<Assembly: ComVisibleAttribute(False)> 
<Assembly: AssemblyFileVersionAttribute("16.1.8.0")> 