// GADSNGDataFactory.cs
// Description:   Data Access Layer
// 
//Copyright (C) 2011  The Outercurve Foundation

//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

// Ron Fluegge
// GADS Open Source Project Coordinator
// Coordinator@GADSOpenSource.com
// 972-625-5653

using System.Collections;
using System.Collections.Specialized;
using System.Runtime.Serialization;
using System.Data.SqlClient;
//using Oracle.DataAccess.Client;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using System.Data.Odbc;
using System.Data.OleDb;
using System.Data.Common;
using System.Data;
using System;
using Microsoft.ApplicationBlocks.ExceptionManagement;


namespace GADSNG.DAL
{
    //FOLDERNAME F:\Data Factory\code\DataFactory\GADSNGDataFactory\
    public sealed class DataFactory
    {

        #region " Declarations "
        private string _provider;
        private string _connect;
        private string _GADSNGTableName;
        private string _UserID;
        private string _Password;
        private string _DataSource;

        private IDbConnection _connection;
        private Type _conType, _comType, _daType, _drType, _parmType;

        private static Hashtable _sqlParamTypes = new Hashtable();
        private static Hashtable _oledbParamTypes = new Hashtable();
        private static Hashtable _odbcParamTypes = new Hashtable();
        private static Hashtable _oracleParamTypes = new Hashtable();
        private static Hashtable _otherParamtypes = new Hashtable();

        private static Hashtable _Schema = new Hashtable();
        private static Hashtable _SchemaStrLen = new Hashtable();
        //private static string[] _OracleSchema;
        private static HybridDictionary myHyD;

        private Hashtable _paramtypes; //points to the param types hashtable

        #endregion

        #region " Constructors and Initialization "

        static DataFactory() //need equiv of Shared
        {
            // Setup the types used by the providers

            // SQL Server Types
            _sqlParamTypes.Clear();
            _sqlParamTypes.Add("BigInt", SqlDbType.BigInt);
            _sqlParamTypes.Add("Binary", SqlDbType.Binary);
            _sqlParamTypes.Add("Bit", SqlDbType.Bit);
            _sqlParamTypes.Add("VarChar", SqlDbType.VarChar);
            _sqlParamTypes.Add("Char", SqlDbType.Char);
            _sqlParamTypes.Add("DateTime", SqlDbType.DateTime);
            _sqlParamTypes.Add("Decimal", SqlDbType.Decimal);
            _sqlParamTypes.Add("Float", SqlDbType.Float);
            _sqlParamTypes.Add("Image", SqlDbType.Image);
            _sqlParamTypes.Add("Int", SqlDbType.Int);
            _sqlParamTypes.Add("Money", SqlDbType.Money);
            _sqlParamTypes.Add("NChar", SqlDbType.NChar);
            _sqlParamTypes.Add("NText", SqlDbType.NText);
            _sqlParamTypes.Add("Numeric", SqlDbType.Decimal);
            _sqlParamTypes.Add("NVarChar", SqlDbType.NVarChar);
            _sqlParamTypes.Add("Real", SqlDbType.Real);
            _sqlParamTypes.Add("SmallDateTime", SqlDbType.SmallDateTime);
            _sqlParamTypes.Add("SmallInt", SqlDbType.SmallInt);
            _sqlParamTypes.Add("SmallMoney", SqlDbType.SmallMoney);
            _sqlParamTypes.Add("Variant", SqlDbType.Variant);
            _sqlParamTypes.Add("Text", SqlDbType.Text);
            _sqlParamTypes.Add("Timestamp", SqlDbType.Timestamp);
            _sqlParamTypes.Add("TinyInt", SqlDbType.TinyInt);
            _sqlParamTypes.Add("UniqueIdentifier", SqlDbType.UniqueIdentifier);
            _sqlParamTypes.Add("VarBinary", SqlDbType.VarBinary);

            // Generics that can also be used
            _sqlParamTypes.Add("String", SqlDbType.NVarChar);
            _sqlParamTypes.Add("Integer", SqlDbType.Int);
            _sqlParamTypes.Add("Long", SqlDbType.BigInt);
            _sqlParamTypes.Add("TimeStamp", SqlDbType.Timestamp);
            _sqlParamTypes.Add("Clob", SqlDbType.VarChar);

            // OleDb types
            _oledbParamTypes.Clear();
            _oledbParamTypes.Add("BigInt", OleDbType.BigInt);
            _oledbParamTypes.Add("Binary", OleDbType.Binary);
            _oledbParamTypes.Add("Boolean", OleDbType.Boolean);
            _oledbParamTypes.Add("BSTR", OleDbType.BSTR);
            _oledbParamTypes.Add("Char", OleDbType.Char);
            _oledbParamTypes.Add("Currency", OleDbType.Currency);
            _oledbParamTypes.Add("Date", OleDbType.Date);
            _oledbParamTypes.Add("DBDate", OleDbType.DBDate);
            _oledbParamTypes.Add("DBTime", OleDbType.DBTime);
            _oledbParamTypes.Add("DBTimeStamp", OleDbType.DBTimeStamp);
            _oledbParamTypes.Add("Decimal", OleDbType.Decimal);
            _oledbParamTypes.Add("Double", OleDbType.Double);
            _oledbParamTypes.Add("Empty", OleDbType.Empty);
            _oledbParamTypes.Add("Error", OleDbType.Error);
            _oledbParamTypes.Add("Filetime", OleDbType.Filetime);
            _oledbParamTypes.Add("Float", OleDbType.Double);
            _oledbParamTypes.Add("Guid", OleDbType.Guid);
            _oledbParamTypes.Add("IDispatch", OleDbType.IDispatch);
            _oledbParamTypes.Add("Integer", OleDbType.Integer);
            _oledbParamTypes.Add("IUnknown", OleDbType.IUnknown);
            _oledbParamTypes.Add("LongVarBinary", OleDbType.LongVarBinary);
            _oledbParamTypes.Add("LongVarChar", OleDbType.LongVarChar);
            _oledbParamTypes.Add("LongVarWChar", OleDbType.LongVarWChar);
            _oledbParamTypes.Add("Numeric", OleDbType.Numeric);
            _oledbParamTypes.Add("PropVariant", OleDbType.PropVariant);
            _oledbParamTypes.Add("Single", OleDbType.Single);
            _oledbParamTypes.Add("SmallInt", OleDbType.SmallInt);
            _oledbParamTypes.Add("TinyInt", OleDbType.TinyInt);
            _oledbParamTypes.Add("UnsignedBigInt", OleDbType.UnsignedBigInt);
            _oledbParamTypes.Add("UnsignedInt", OleDbType.UnsignedInt);
            _oledbParamTypes.Add("UnsignedSmallInt", OleDbType.UnsignedSmallInt);
            _oledbParamTypes.Add("UnsignedTinyInt", OleDbType.UnsignedTinyInt);
            _oledbParamTypes.Add("VarBinary", OleDbType.VarBinary);
            _oledbParamTypes.Add("VarChar", OleDbType.VarChar);
            _oledbParamTypes.Add("Variant", OleDbType.Variant);
            _oledbParamTypes.Add("VarNumeric", OleDbType.VarNumeric);
            _oledbParamTypes.Add("VarWChar", OleDbType.VarWChar);
            _oledbParamTypes.Add("WChar", OleDbType.WChar);
            _oledbParamTypes.Add("Clob", OleDbType.VarChar);

            //// Generics that can also be used
            _oledbParamTypes.Add("String", OleDbType.VarWChar);
            _oledbParamTypes.Add("Int", OleDbType.Integer);
            _oledbParamTypes.Add("Long", OleDbType.BigInt);
            _oledbParamTypes.Add("Bit", OleDbType.Boolean);
            _oledbParamTypes.Add("NVarChar", OleDbType.VarWChar);
            _oledbParamTypes.Add("TimeStamp", OleDbType.DBTimeStamp);
            _oledbParamTypes.Add("DateTime", OleDbType.DBTimeStamp);

            // ODBC Types
            _odbcParamTypes.Clear();
            _odbcParamTypes.Add("BigInt", OdbcType.BigInt);
            _odbcParamTypes.Add("Binary", OdbcType.Binary);
            _odbcParamTypes.Add("Boolean", OdbcType.Bit);
            _odbcParamTypes.Add("Bit", OdbcType.Bit);
            _odbcParamTypes.Add("Char", OdbcType.Char);
            _odbcParamTypes.Add("Date", OdbcType.Date);
            _odbcParamTypes.Add("DateTime", OdbcType.DateTime);
            _odbcParamTypes.Add("Decimal", OdbcType.Decimal);
            _odbcParamTypes.Add("Double", OdbcType.Double);
            _odbcParamTypes.Add("Image", OdbcType.Image);
            _odbcParamTypes.Add("Int", OdbcType.Int);
            _odbcParamTypes.Add("NChar", OdbcType.NChar);
            _odbcParamTypes.Add("NText", OdbcType.NText);
            _odbcParamTypes.Add("Numeric", OdbcType.Numeric);
            _odbcParamTypes.Add("NVarChar", OdbcType.NVarChar);
            _odbcParamTypes.Add("Real", OdbcType.Real);
            _odbcParamTypes.Add("SmallDateTime", OdbcType.SmallDateTime);
            _odbcParamTypes.Add("SmallInt", OdbcType.SmallInt);
            _odbcParamTypes.Add("Text", OdbcType.Text);
            _odbcParamTypes.Add("Time", OdbcType.Time);
            _odbcParamTypes.Add("Timestamp", OdbcType.Timestamp);
            _odbcParamTypes.Add("TinyInt", OdbcType.TinyInt);
            _odbcParamTypes.Add("UniqueIdentifier", OdbcType.UniqueIdentifier);
            _odbcParamTypes.Add("VarBinary", OdbcType.VarBinary);
            _odbcParamTypes.Add("VarChar", OdbcType.VarChar);
            _odbcParamTypes.Add("Float", OdbcType.Double);
            _odbcParamTypes.Add("Int16", OdbcType.SmallInt);
            _odbcParamTypes.Add("Int32", OdbcType.Int);
            _odbcParamTypes.Add("Byte", OdbcType.Binary);

            _odbcParamTypes.Add("Integer", OdbcType.Int);
            _odbcParamTypes.Add("String", OdbcType.VarChar);
            _odbcParamTypes.Add("Long", OdbcType.BigInt);
            _odbcParamTypes.Add("TimeStamp", OdbcType.Timestamp);
            _odbcParamTypes.Add("Single", OdbcType.Real);

            // Oracle Types - Oracle.DataAccess.Client
            _oracleParamTypes.Clear();
            _oracleParamTypes.Add("BFile", OracleDbType.BFile);
            _oracleParamTypes.Add("Blob", OracleDbType.Blob);
            _oracleParamTypes.Add("Byte", OracleDbType.Byte);
            _oracleParamTypes.Add("Char", OracleDbType.Char);
            _oracleParamTypes.Add("DateTime", OracleDbType.Date);
            _oracleParamTypes.Add("Double", OracleDbType.Double);
            _oracleParamTypes.Add("Float", OracleDbType.Double);
            _oracleParamTypes.Add("Int16", OracleDbType.Int16);
            _oracleParamTypes.Add("Int32", OracleDbType.Int32);
            _oracleParamTypes.Add("IntervalDayToSecond", OracleDbType.IntervalDS);
            _oracleParamTypes.Add("IntervalYearToMonth", OracleDbType.IntervalYM);
            _oracleParamTypes.Add("LongRaw", OracleDbType.LongRaw);
            _oracleParamTypes.Add("LongVarChar", OracleDbType.NVarchar2);
            _oracleParamTypes.Add("NChar", OracleDbType.NChar);
            _oracleParamTypes.Add("NClob", OracleDbType.NClob);
            _oracleParamTypes.Add("Number", OracleDbType.Decimal);
            _oracleParamTypes.Add("NVarChar", OracleDbType.NVarchar2);
            _oracleParamTypes.Add("Raw", OracleDbType.Raw);
            _oracleParamTypes.Add("SByte", OracleDbType.Byte);
            _oracleParamTypes.Add("Timestamp", OracleDbType.TimeStamp);
            _oracleParamTypes.Add("TimestampLocal", OracleDbType.TimeStampLTZ);
            _oracleParamTypes.Add("TimestampWithTZ", OracleDbType.TimeStampTZ);
            _oracleParamTypes.Add("UInt16", OracleDbType.Int16);
            _oracleParamTypes.Add("UInt32", OracleDbType.Int32);
            _oracleParamTypes.Add("VarChar", OracleDbType.Varchar2);
            _oracleParamTypes.Add("Clob", OracleDbType.Clob);

            // Mapping the SQL Server data types to Oracle
            _oracleParamTypes.Add("Integer", OracleDbType.Int32);
            _oracleParamTypes.Add("String", OracleDbType.Varchar2);
            _oracleParamTypes.Add("Long", OracleDbType.Int32);
            _oracleParamTypes.Add("TinyInt", OracleDbType.Byte);
            _oracleParamTypes.Add("SmallInt", OracleDbType.Int16);
            _oracleParamTypes.Add("Int", OracleDbType.Int32);
            _oracleParamTypes.Add("Decimal", OracleDbType.Decimal);
            _oracleParamTypes.Add("Real", OracleDbType.Double);
            _oracleParamTypes.Add("Bit", OracleDbType.Int16);
            _oracleParamTypes.Add("Numeric", OracleDbType.Decimal);
          
            // Other provider types
            _otherParamtypes.Clear();
            _otherParamtypes.Add("string", DbType.String);
            _otherParamtypes.Add("integer", DbType.Int32);
            _otherParamtypes.Add("short", DbType.UInt16);
            _otherParamtypes.Add("boolean", DbType.Boolean);
            _otherParamtypes.Add("date", DbType.Date);
            _otherParamtypes.Add("datetime", DbType.DateTime);
            _otherParamtypes.Add("byte", DbType.Byte);
            _otherParamtypes.Add("currency", DbType.Currency);
            _otherParamtypes.Add("double", DbType.Double);
            _otherParamtypes.Add("decimal", DbType.Decimal);
            _otherParamtypes.Add("binary", DbType.Binary);
            _otherParamtypes.Add("guid", DbType.Guid);
            _otherParamtypes.Add("single", DbType.Single);

            myHyD = ColumnNamesUpperToMixed();
        }

        public DataFactory(string connect)
        {
            _initClass(connect);
            _createProviderTypes();
        }

        public DataFactory(string connect, string provider)
        {
            _initClass(connect);
            this.Provider = provider;
        }

        public static string[] GetProviders()
        {
            // List the providers that are supported
            string[] prov = new string[3];
            prov[0] = "SqlClient";
            prov[1] = "OleDb";
            prov[2] = "ODBC";
            prov[3] = "Oracle";
            return prov;
        }

        private void _createProviderTypes()
        {
            // Provider and connection string are set so instantiate the connection object

            switch (this.Provider)
            {
                case "SqlClient":
                    {
                        _conType = typeof(SqlConnection);
                        _comType = typeof(SqlCommand);
                        _drType = typeof(SqlDataReader);
                        _daType = typeof(SqlDataAdapter);
                        _parmType = typeof(SqlParameter);
                        _paramtypes = _sqlParamTypes;
                        break;
                    }
                case "OleDb":
                    {
                        _conType = typeof(OleDbConnection);
                        _comType = typeof(OleDbCommand);
                        _drType = typeof(OleDbDataReader);
                        _daType = typeof(OleDbDataAdapter);
                        _parmType = typeof(OleDbParameter);
                        _paramtypes = _oledbParamTypes;
                        break;
                    }
                case "ODBC":
                    {
                        _conType = typeof(OdbcConnection);
                        _comType = typeof(OdbcCommand);
                        _drType = typeof(OdbcDataReader);
                        _daType = typeof(OdbcDataAdapter);
                        _parmType = typeof(OdbcParameter);
                        _paramtypes = _odbcParamTypes;
                        break;
                    }
                case "Oracle":
                    {
                        _conType = typeof(OracleConnection);
                        _comType = typeof(OracleCommand);
                        _drType = typeof(OracleDataReader);
                        _daType = typeof(OracleDataAdapter);
                        _parmType = typeof(OracleParameter);
                        _paramtypes = _oracleParamTypes;
                        break;
                    }
                default:
                    {
                        _throwException("Invalid this.Provider (" + this.Provider + ")in _createProviderTypes()", null);
                        break;
                    }
            }
            // Create an instance of the connection object
            try
            {
                if (!(_conType == null))
                {
                    _connection = (IDbConnection)Activator.CreateInstance(_conType, false);
                    _connection.ConnectionString = _connect;

                    if (_connection.State == ConnectionState.Closed)
                    {
                        _connection.Open();
                        _connection.Close();
                    }
                }
            }
            catch (OracleException ex)
            {

                if (ex.ErrorCode == 12514)
                {
                    _throwException("Invalid Oracle Data Source or Server\n\n" +
                        "Unable to Connect to: " + _connect, ex);
                }
                else if (ex.ErrorCode == 12545)
                {
                    _throwException("The database is unavailable\n\n" +
                        "Unable to Connect to: " + _connect, ex);
                }
                else
                {
                    _throwException(ex.Message, ex);
                }
            }
            catch (Exception e)
            {
                _throwException("Could not connect with data source:  " + _connect, e);
            }
        }

        private void _initClass(string connect)
        {
            _connect = connect; //' Set the connection string

        }
        #endregion

        #region " Public Properties "


        public IDbConnection Connection
        {
            // Returns a reference to the connection
            get
            {
                return _connection;
            }
        }


        public string UserID
        {
            get
            {
                return _UserID;
            }
            set
            {
                _UserID = value;
            }
        }

        public string Password
        {
            get
            {
                return _Password;
            }
            set
            {
                _Password = value;
            }
        }

        public string DataSource
        {
            get
            {
                return _DataSource;
            }
            set
            {
                _DataSource = value;
            }
        }

        public string Provider
        {
            // Sets up the provider to use
            get
            {
                return _provider;
            }
            set
            {

                _provider = value;
                _createProviderTypes();
            }
        }

        public string GADSNGTableName
        {
            get
            {
                return _GADSNGTableName;
            }
            set
            {
                _GADSNGTableName = value;
            }
        }

        // Default procedure prefix, can be changed
        public static string ProcPrefix = "usp_"; //shared

        #endregion

        #region " Private Helpers "

        private IDbDataAdapter _setupDataAdapter(string statement, Array parms, IDbTransaction transaction)
        {
            IDbCommand com;
            IDbDataAdapter daCommand;
            IDataAdapter da;

            // Get the command
            com = _getStatement(statement, parms, true);
            if (!(transaction == null))
            {
                com.Transaction = transaction;
            }

            try
            {
                da = (IDataAdapter)Activator.CreateInstance(_daType, false);
                daCommand = (IDbDataAdapter)da;
                daCommand.SelectCommand = com;
                return daCommand;
            }
            catch (Exception e)
            {
                if (Provider == "SqlClient")
                {
                    // Get the command
                    string tmpStatement = statement.Replace("GADSNG.", "dbo.");

                    com = _getStatement(tmpStatement, parms, true);
                    if (!(transaction == null))
                    {
                        com.Transaction = transaction;
                    }

                    try
                    {
                        da = (IDataAdapter)Activator.CreateInstance(_daType, false);
                        daCommand = (IDbDataAdapter)da;
                        daCommand.SelectCommand = com;
                        return daCommand;
                    }
                    catch (Exception e2)
                    {
                        _throwException("Could not create command object", e2);
                    }
                }
                else
                {
                    _throwException("Could not create command object", e);
                }
            }
            return null;
        }

        private string GetTableName(string statement)
        {
            int _locateMe;
            int _locateEnd;
            int _baseLength;
            string _tableName;
            string _temp;
            string _tempMixedCase;
            _tempMixedCase = statement;
            _temp = statement.ToUpper();
            statement = _temp;

            _locateMe = 0;
            _locateEnd = 0;
            _baseLength = 0;

            if (statement.StartsWith("SELECT") | statement.StartsWith("DELETE"))
            {
                _locateEnd = statement.IndexOf("FROM (SELECT");

                if (_locateEnd != -1)
                {    // FROM (SELECT * FROM
                    _locateMe = statement.IndexOf("FROM", _locateEnd + 12);
                    _locateEnd = statement.IndexOf(" ", _locateMe + 5);
                    _baseLength = 5;
                }
                else
                {
                    _locateMe = statement.IndexOf("FROM");
                    _locateEnd = statement.IndexOf(" ", _locateMe + 5);
                    _baseLength = 5;
                }
               
            }
            else if (statement.StartsWith("INSERT"))
            {
                _locateMe = statement.IndexOf("INTO");
                _locateEnd = statement.IndexOf(" ", _locateMe + 5);
                _baseLength = 5;
            }
            else if (statement.StartsWith("UPDATE"))
            {
                _locateMe = statement.IndexOf("UPDATE");
                _locateEnd = statement.IndexOf(" ", _locateMe + 7);
                _baseLength = 7;
            }

            if (_locateEnd == -1)
            {
                _tableName = statement.Substring(_locateMe + _baseLength).Trim();
            }
            else
            {
                _tableName = statement.Substring((_locateMe + _baseLength), (_locateEnd - _locateMe - _baseLength)).Trim();
            }

            GADSNGTableName = _tableName;

            return _tableName;
        }

        private string _SetTableName(string statement)
        {
            if (statement.IndexOf("dbo.") > 0)
            {
                // the table is owned by dbo. ... so this statement has already been converted
                // to include dbo. as the owner so no need to reprocess; otherwise, you get strange SQL strings
                return statement;
            }

            int _locateMe;
            int _locateEnd;

            int _locateMe2;
            int _locateEnd2;

            int _locateMe3;
            int _locateEnd3;

            int _locateMeLOJ;

            int _baseline;
            string _tableName = statement;

            _locateEnd = 0;
            _locateMe = 0;
            _baseline = 0;

            if (statement.ToUpper().StartsWith("DELETE"))
            {
                // Delete does not permit the wildcard * in the statement as in DELETE * FROM...
                // Fixes any problems on my part with creating the DELETE statment

                statement = statement.Replace("* ", "");
            }

            if (Provider == "Oracle")
            {
                // required by DoesTableExist(string tableName)
                if (statement.ToUpper().IndexOf("ALL_TABLES") > 0)
                {
                    // statement = "select count(*) from all_tables where table_name = '" + tableName + "'";
                    // this will not work if "qualified" 
                    return statement;
                }

                _locateMe2 = 0;
                _locateEnd2 = 0;

                if (statement.ToUpper().StartsWith("SELECT"))
                {
                    _locateMe = statement.ToUpper().IndexOf("FROM");
                    _locateEnd = statement.IndexOf(" ", _locateMe + 5);
                    _baseline = 5;

                    // "SELECT MAX(Year) FROM PerformanceData WHERE (UtilityUnitCode = @UtilityUnitCode)"

                    if (_locateEnd != -1)
                    {
                        // This is an SELECT ... FROM ...IN (SELECT ... FROM ... IN (SELECT ... FROM ... ))
                        _locateMe2 = statement.ToUpper().IndexOf("FROM", _locateEnd);

                        if (_locateMe2 != -1)
                        {
                            _locateEnd2 = statement.IndexOf(" ", _locateMe2 + 5);

                            if (_locateEnd2 == -1)
                            {
                                _locateEnd2 = statement.IndexOf(")", _locateMe2 + 5);
                            }

                            statement = statement.Insert(_locateEnd2, "\"");
                            statement = statement.Insert(_locateMe2 + _baseline, "GADSNG.\"");

                            _locateMe3 = statement.ToUpper().IndexOf("FROM", _locateEnd2);

                            if (_locateMe3 != -1)
                            {
                                _locateEnd3 = statement.IndexOf(" ", _locateMe3 + 5);

                                statement = statement.Insert(_locateEnd3, "\"");
                                statement = statement.Insert(_locateMe3 + _baseline, "GADSNG.\"");
                            }
                        }


                        // NYISO IIFO 2017-05-09
                        _locateMeLOJ = statement.IndexOf("GADSNG.\"PeriodStart)\"");

                        if (_locateMeLOJ != -1)
                        {
                            statement = statement.Replace("GADSNG.\"PeriodStart)\"", "PeriodStart)");
                            statement = statement.Replace("GADSNG.\"PeriodEnd)\"", "PeriodEnd)");
                        }

                        _locateMeLOJ = statement.IndexOf("GADSNG.\"(SELECT\"");

                        if (_locateMeLOJ != -1)
                        {
                            statement = statement.Replace("GADSNG.\"(SELECT\"", "(SELECT");
                        }


                        _locateMeLOJ = statement.IndexOf("FROM Setup s LEFT OUTER JOIN DefaultCapacities t ON s.UtilityUnitCode = t.UtilityUnitCode ORDER BY UnitName");

                        if (_locateMeLOJ != -1)
                        {
                            statement = statement.Replace(
                                "FROM Setup s LEFT OUTER JOIN DefaultCapacities t ON s.UtilityUnitCode = t.UtilityUnitCode ORDER BY UnitName",
                                "FROM Setup s, GADSNG.\"DefaultCapacities\" t WHERE s.UtilityUnitCode = t.UtilityUnitCode(+) ORDER BY UnitName");
                        }
                        // FROM "GADSNG"."EventDetails" a LEFT OUTER JOIN [GADSNG]."EventRecords" r

                        _locateMeLOJ = statement.IndexOf("FROM EventDetails a LEFT OUTER JOIN EventRecords r");

                        if (_locateMeLOJ != -1)
                        {
                            //	statement = statement.Replace(
                            //		"FROM EventDetails a LEFT OUTER JOIN EventRecords r ON (a.UnitShortName = r.UnitShortName AND a.EventNumber = r.EventNumber AND a.EventType = r.EventType AND a.EventContribCode = r.EventContribCode AND a.EV_DateTime = r.EndDateTime)",
                            //		"FROM EventDetails a, GADSNG.\"EventRecords\" r ON (a.UnitShortName = r.UnitShortName(+) AND a.EventNumber = r.EventNumber(+) AND a.EventType = r.EventType(+) AND a.EventContribCode = r.EventContribCode(+) AND a.EV_DateTime = r.EndDateTime(+))");

                            statement = statement.Replace(
                                "FROM EventDetails a LEFT OUTER JOIN EventRecords r",
                                "FROM EventDetails a LEFT OUTER JOIN GADSNG.\"EventRecords\" r");
                        }

                        // INNER JOIN WebProcessTypes T
                        _locateMeLOJ = statement.IndexOf("INNER JOIN WebProcessTypes T");

                        if (_locateMeLOJ != -1)
                        {
                            statement = statement.Replace(
                                "INNER JOIN WebProcessTypes T",
                                "INNER JOIN GADSNG.\"WebProcessTypes\" T");
                        }

                        // INNER JOIN WebProcessStatus S
                        _locateMeLOJ = statement.IndexOf("INNER JOIN WebProcessStatus S");

                        if (_locateMeLOJ != -1)
                        {
                            statement = statement.Replace(
                                "INNER JOIN WebProcessStatus S",
                                "INNER JOIN GADSNG.\"WebProcessStatus\" S");
                        }

                        // FROM EventDetails a RIGHT JOIN EventRecords r
                        _locateMeLOJ = statement.IndexOf("FROM EventDetails a RIGHT JOIN EventRecords r");

                        if (_locateMeLOJ != -1)
                        {

                            statement = statement.Replace(
                                "FROM EventDetails a RIGHT JOIN EventRecords r",
                                "FROM EventDetails a RIGHT JOIN GADSNG.\"EventRecords\" r");
                        }





                        // LEFT OUTER JOIN EventRecords c
                        _locateMeLOJ = statement.IndexOf("LEFT OUTER JOIN EventRecords c");

                        if (_locateMeLOJ != -1)
                        {

                            statement = statement.Replace(
                                "LEFT OUTER JOIN EventRecords c",
                                "LEFT OUTER JOIN GADSNG.\"EventRecords\" c");
                        }

                        // WHERE UnitShortName IN (SELECT UnitShortName FROM ARUnitPerm WHERE
                        _locateMeLOJ = statement.IndexOf("WHERE UnitShortName IN (SELECT UnitShortName FROM ARUnitPerm WHERE");

                        if (_locateMeLOJ != -1)
                        {

                            statement = statement.Replace(
                                "WHERE UnitShortName IN (SELECT UnitShortName FROM ARUnitPerm WHERE",
                                "WHERE UnitShortName IN (SELECT UnitShortName FROM GADSNG.\"ARUnitPerm\" WHERE");
                        }

                    }
                }
                else if (statement.ToUpper().StartsWith("DELETE"))
                {
                    _locateMe = statement.ToUpper().IndexOf("FROM");
                    _locateEnd = statement.IndexOf(" ", _locateMe + 5);
                    _baseline = 5;

                    if (_locateEnd != -1)
                    {
                        // This is an DELETE FROM ...IN (SELECT ... FROM ... IN (SELECT ... FROM ... ))
                        _locateMe2 = statement.ToUpper().IndexOf("FROM", _locateEnd);

                        if (_locateMe2 != -1)
                        {
                            _locateEnd2 = statement.IndexOf(" ", _locateMe2 + 5);

                            statement = statement.Insert(_locateEnd2, "\"");
                            statement = statement.Insert(_locateMe2 + _baseline, "GADSNG.\"");

                            _locateMe3 = statement.ToUpper().IndexOf("FROM", _locateEnd2);

                            if (_locateMe3 != -1)
                            {
                                _locateEnd3 = statement.IndexOf(" ", _locateMe3 + 5);

                                statement = statement.Insert(_locateEnd3, "\"");
                                statement = statement.Insert(_locateMe3 + _baseline, "GADSNG.\"");
                            }
                        }
                    }
                }
                else if (statement.ToUpper().StartsWith("INSERT"))
                {
                    _locateMe = statement.ToUpper().IndexOf("INTO");
                    _locateEnd = statement.IndexOf(" ", _locateMe + 5);
                    _baseline = 5;
                    // This is an INSERT INTO ... SELECT ... FROM ...
                    _locateMe2 = statement.ToUpper().IndexOf("FROM");
                    _locateEnd2 = statement.IndexOf(" ", _locateMe2 + 5);

                    if (_locateMe2 != -1)
                    {
                        statement = statement.Insert(_locateEnd2, "\"");
                        statement = statement.Insert(_locateMe2 + _baseline, "GADSNG.\"");
                    }
                }
                else if (statement.ToUpper().StartsWith("UPDATE"))
                {
                    _locateMe = statement.ToUpper().IndexOf("UPDATE");
                    _locateEnd = statement.IndexOf(" ", _locateMe + 7);
                    _baseline = 7;
                    // This is an UPDATE table SET ... WHERE ... IN (SELECT ... FROM ...
                    _locateMe2 = statement.ToUpper().IndexOf("FROM");
                    _locateEnd2 = statement.IndexOf(" ", _locateMe2 + 5);

                    if (_locateMe2 != -1)
                    {
                        statement = statement.Insert((_locateEnd2), "\"");
                        statement = statement.Insert(_locateMe2 + 5, "GADSNG.\"");

                        _locateMe3 = statement.ToUpper().IndexOf("FROM", _locateEnd2);
                        _locateEnd3 = statement.IndexOf(" ", _locateMe3 + 5);

                        if (_locateMe3 != -1)
                        {
                            // This is an UPDATE table SET ... WHERE ... IN (SELECT ... FROM ... WHERE ... IN (SELECT ... FROM ...

                            statement = statement.Insert(_locateEnd3, "\"");
                            statement = statement.Insert(_locateMe3 + 5, "GADSNG.\"");
                        }
                    }
                }

                if (_locateEnd == -1)
                {
                    statement = statement + "\"";
                }
                else
                {
                    statement = statement.Insert(_locateEnd, "\"");
                }

                _tableName = statement.Insert(_locateMe + _baseline, "GADSNG.\"");

                _locateMeLOJ = statement.IndexOf("(SELECT\"");

                if (_locateMeLOJ != -1)
                {
                    _tableName = statement.Replace("(SELECT\"", "(SELECT");
                }

            }
            else if (Provider == "SqlClient")
            {
                // required by DoesTableExist(string tableName)
                if (statement.ToUpper().IndexOf("SYS.TABLES") > 0)
                {
                    // statement = "SELECT COUNT(*) FROM sys.tables WHERE name = '" + tableName + "'";
                    // this will not work if "qualified" 
                    return statement;
                }

                _locateMe2 = 0;
                _locateEnd2 = 0;

                if (statement.ToUpper().StartsWith("SELECT"))
                {
                    _locateMe = statement.ToUpper().IndexOf("FROM");
                    _locateEnd = statement.IndexOf(" ", _locateMe + 5);
                    _baseline = 5;

                    // "SELECT MAX(Year) FROM PerformanceData WHERE (UtilityUnitCode = @UtilityUnitCode)"

                    if (_locateEnd != -1)
                    {
                        // This is an SELECT ... FROM ...IN (SELECT ... FROM ... IN (SELECT ... FROM ... ))
                        _locateMe2 = statement.ToUpper().IndexOf("FROM", _locateEnd);

                        if (_locateMe2 != -1)
                        {
                            _locateEnd2 = statement.IndexOf(" ", _locateMe2 + 5);

                            if (_locateEnd2 == -1)
                            {
                                _locateEnd2 = statement.IndexOf(")", _locateMe2 + 5);
                            }

                            statement = statement.Insert(_locateEnd2, "\"");
                            statement = statement.Insert(_locateMe2 + _baseline, "GADSNG.\"");

                            _locateMe3 = statement.ToUpper().IndexOf("FROM", _locateEnd2);

                            if (_locateMe3 != -1)
                            {
                                _locateEnd3 = statement.IndexOf(" ", _locateMe3 + 5);

                                statement = statement.Insert(_locateEnd3, "\"");
                                statement = statement.Insert(_locateMe3 + _baseline, "GADSNG.\"");
                            }
                        }

                        _locateMeLOJ = statement.IndexOf("FROM Setup s LEFT OUTER JOIN DefaultCapacities t ON s.UtilityUnitCode = t.UtilityUnitCode ORDER BY UnitName");

                        if (_locateMeLOJ != -1)
                        {
                            statement = statement.Replace(
                               "FROM Setup s LEFT OUTER JOIN DefaultCapacities t ON s.UtilityUnitCode = t.UtilityUnitCode ORDER BY UnitName",
                               "FROM Setup s LEFT OUTER JOIN GADSNG.\"DefaultCapacities\" t ON s.UtilityUnitCode = t.UtilityUnitCode ORDER BY UnitName");
                        }
                        // FROM "GADSNG"."EventDetails" a LEFT OUTER JOIN [GADSNG]."EventRecords" r

                        _locateMeLOJ = statement.IndexOf("FROM EventDetails a LEFT OUTER JOIN EventRecords r");

                        if (_locateMeLOJ != -1)
                        {
                            //	statement = statement.Replace(
                            //		"FROM EventDetails a LEFT OUTER JOIN EventRecords r ON (a.UnitShortName = r.UnitShortName AND a.EventNumber = r.EventNumber AND a.EventType = r.EventType AND a.EventContribCode = r.EventContribCode AND a.EV_DateTime = r.EndDateTime)",
                            //		"FROM EventDetails a, GADSNG.\"EventRecords\" r ON (a.UnitShortName = r.UnitShortName(+) AND a.EventNumber = r.EventNumber(+) AND a.EventType = r.EventType(+) AND a.EventContribCode = r.EventContribCode(+) AND a.EV_DateTime = r.EndDateTime(+))");

                            statement = statement.Replace(
                               "FROM EventDetails a LEFT OUTER JOIN EventRecords r",
                               "FROM EventDetails a LEFT OUTER JOIN GADSNG.\"EventRecords\" r");
                        }

                        // FROM EventDetails a RIGHT JOIN EventRecords r

                        _locateMeLOJ = statement.IndexOf("FROM EventDetails a RIGHT JOIN EventRecords r");

                        if (_locateMeLOJ != -1)
                        {

                            statement = statement.Replace(
                                "FROM EventDetails a RIGHT JOIN EventRecords r",
                                "FROM EventDetails a RIGHT JOIN GADSNG.\"EventRecords\" r");
                        }
                    }
                }
                else if (statement.ToUpper().StartsWith("DELETE"))
                {
                    _locateMe = statement.ToUpper().IndexOf("FROM");
                    _locateEnd = statement.IndexOf(" ", _locateMe + 5);
                    _baseline = 5;

                    if (_locateEnd != -1)
                    {
                        // This is an DELETE FROM ...IN (SELECT ... FROM ... IN (SELECT ... FROM ... ))
                        _locateMe2 = statement.ToUpper().IndexOf("FROM", _locateEnd);

                        if (_locateMe2 != -1)
                        {
                            _locateEnd2 = statement.IndexOf(" ", _locateMe2 + 5);

                            statement = statement.Insert(_locateEnd2, "\"");
                            statement = statement.Insert(_locateMe2 + _baseline, "GADSNG.\"");

                            _locateMe3 = statement.ToUpper().IndexOf("FROM", _locateEnd2);

                            if (_locateMe3 != -1)
                            {
                                _locateEnd3 = statement.IndexOf(" ", _locateMe3 + 5);

                                statement = statement.Insert(_locateEnd3, "\"");
                                statement = statement.Insert(_locateMe3 + _baseline, "GADSNG.\"");
                            }
                        }
                    }
                }
                else if (statement.ToUpper().StartsWith("INSERT"))
                {
                    _locateMe = statement.ToUpper().IndexOf("INTO");
                    _locateEnd = statement.IndexOf(" ", _locateMe + 5);
                    _baseline = 5;
                    // This is an INSERT INTO ... SELECT ... FROM ...
                    _locateMe2 = statement.ToUpper().IndexOf("FROM");
                    _locateEnd2 = statement.IndexOf(" ", _locateMe2 + 5);

                    if (_locateMe2 != -1)
                    {
                        statement = statement.Insert(_locateEnd2, "\"");
                        statement = statement.Insert(_locateMe2 + _baseline, "GADSNG.\"");
                    }
                }
                else if (statement.ToUpper().StartsWith("UPDATE"))
                {
                    _locateMe = statement.ToUpper().IndexOf("UPDATE");
                    _locateEnd = statement.IndexOf(" ", _locateMe + 7);
                    _baseline = 7;
                    // This is an UPDATE table SET ... WHERE ... IN (SELECT ... FROM ...
                    _locateMe2 = statement.ToUpper().IndexOf("FROM");
                    _locateEnd2 = statement.IndexOf(" ", _locateMe2 + 5);

                    if (_locateMe2 != -1)
                    {
                        statement = statement.Insert((_locateEnd2), "\"");
                        statement = statement.Insert(_locateMe2 + 5, "GADSNG.\"");

                        _locateMe3 = statement.ToUpper().IndexOf("FROM", _locateEnd2);
                        _locateEnd3 = statement.IndexOf(" ", _locateMe3 + 5);

                        if (_locateMe3 != -1)
                        {
                            // This is an UPDATE table SET ... WHERE ... IN (SELECT ... FROM ... WHERE ... IN (SELECT ... FROM ...

                            statement = statement.Insert(_locateEnd3, "\"");
                            statement = statement.Insert(_locateMe3 + 5, "GADSNG.\"");
                        }
                    }
                }

                if (_locateEnd == -1)
                {
                    statement = statement + "\"";
                }
                else
                {
                    statement = statement.Insert(_locateEnd, "\"");
                }

                _tableName = statement.Insert(_locateMe + _baseline, "GADSNG.\"");

            }


            return _tableName;
        }

        private Statement _getStatementFromFile(string strTable, Array ParamList)
        {

            _Schema.Clear();
            _SchemaStrLen.Clear();

            Statement s = new Statement();
            string temp;

            if (strTable.ToUpper().StartsWith("DBO"))
            {
                if (strTable.StartsWith("dbo"))
                {
                    strTable = strTable.Replace("dbo.", "");
                }
                else
                {
                    strTable = strTable.Replace("DBO.", "");
                }

                strTable = strTable.Replace("\"", "");
            }
            else if (strTable.ToUpper().StartsWith("GADSNG"))
            {
                strTable = strTable.Replace("GADSNG.", "");
                strTable = strTable.Replace("\"", "");
            }

            switch (strTable.ToUpper())
            {
                // All case must be UPPERCASE

                case "CAUSECODES":
                    {
                        // TABLE CauseCodes
                        _Schema.Add("Level0", "SmallInt");
                        _Schema.Add("Level1", "SmallInt");
                        _Schema.Add("Level2", "SmallInt");
                        _Schema.Add("Level3", "SmallInt");
                        _Schema.Add("Level4", "SmallInt");
                        _Schema.Add("Level5", "SmallInt");
                        _Schema.Add("CauseCode", "SmallInt");
                        _Schema.Add("FossilSteam", "Bit");
                        _Schema.Add("Nuclear", "Bit");
                        _Schema.Add("Diesel", "Bit");
                        _Schema.Add("Hydro", "Bit");
                        _Schema.Add("GasTurbineSimpleCycle", "Bit");
                        _Schema.Add("JetEngineSimpleCycle", "Bit");
                        _Schema.Add("Misc", "Bit");
                        _Schema.Add("FluidizedBed", "Bit");
                        _Schema.Add("CombinedCycleCT", "Bit");
                        _Schema.Add("CombinedCycleJE", "Bit");
                        _Schema.Add("CombinedCycleSteam", "Bit");
                        _Schema.Add("Geothermal", "Bit");
                        _Schema.Add("MiscMulti", "Bit");
                        _Schema.Add("CombinedCycleCTSteam", "Bit");
                        _Schema.Add("CombinedCycleJESteam", "Bit");
                        _Schema.Add("CauseCodeDesc", "NVarChar");  // "80"
                        _Schema.Add("StartingYear", "SmallInt");
                        _Schema.Add("EndingYear", "SmallInt");
                        _Schema.Add("PlantMgtControl", "Bit");

                        _SchemaStrLen.Add("CauseCodeDesc", 80);
                        break;
                    }

                case "EVENTDATA01":
                    {
                        // TABLE EventData01
                        _Schema.Add("UtilityUnitCode", "Char"); // ( 6)
                        _Schema.Add("UnitShortName", "Char"); // (10)
                        _Schema.Add("Year", "SmallInt");
                        _Schema.Add("EventNumber", "SmallInt");
                        _Schema.Add("RevisionCard01", "Char"); // ( 1)
                        _Schema.Add("EventType", "Char"); // ( 2)
                        _Schema.Add("StartDateTime", "DateTime");
                        _Schema.Add("CarryOverLastYear", "Bit");
                        _Schema.Add("ChangeDateTime1", "DateTime");
                        _Schema.Add("ChangeInEventType1", "Char"); // ( 2)
                        _Schema.Add("ChangeDateTime2", "DateTime");
                        _Schema.Add("ChangeInEventType2", "Char"); // ( 2)
                        _Schema.Add("EndDateTime", "DateTime");
                        _Schema.Add("CarryOverNextYear", "Bit");
                        _Schema.Add("GrossAvailCapacity", "Decimal"); // Changed December 2005
                        _Schema.Add("NetAvailCapacity", "Decimal"); // Changed December 2005
                        _Schema.Add("CauseCode", "SmallInt");
                        _Schema.Add("CauseCodeExt", "Char"); // ( 2)
                        _Schema.Add("WorkStarted", "DateTime");
                        _Schema.Add("WorkEnded", "DateTime");
                        _Schema.Add("ContribCode", "TinyInt");
                        _Schema.Add("PrimaryAlert", "Bit");
                        _Schema.Add("ManhoursWorked", "SmallInt");
                        _Schema.Add("VerbalDesc1", "Char"); // (31)
                        _Schema.Add("RevisionCard02", "Char"); // ( 1)
                        _Schema.Add("VerbalDesc2", "Char"); // (55)
                        _Schema.Add("RevisionCard03", "Char"); // ( 1)
                        _Schema.Add("RevMonthCard01", "DateTime");
                        _Schema.Add("RevMonthCard02", "DateTime");
                        _Schema.Add("RevMonthCard03", "DateTime");
                        _Schema.Add("EditFlag", "Bit");
                        _Schema.Add("VerbalDesc86", "Char"); // (86)
                        _Schema.Add("VerbalDescFull", "VarChar"); // (4000)
                        _Schema.Add("FailureMechCode", "Char"); // ( 4)
                        _Schema.Add("TripMech", "Char"); // ( 1)
                        _Schema.Add("CumFiredHours", "Int");
                        _Schema.Add("CumEngineStarts", "Int");
                        _Schema.Add("DominantDerate", "Bit");
                        _Schema.Add("TimeStamp", "DateTime");
                        _Schema.Add("PJMIOCode", "TinyInt");
                        _Schema.Add("PJMLoadStatus", "Char"); // ( 1)
                        _Schema.Add("PJMStatusDate", "DateTime");

                        _SchemaStrLen.Add("UtilityUnitCode", 6);
                        _SchemaStrLen.Add("UnitShortName", 10);
                        _SchemaStrLen.Add("RevisionCard01", 1);
                        _SchemaStrLen.Add("EventType", 2);
                        _SchemaStrLen.Add("ChangeInEventType1", 2);
                        _SchemaStrLen.Add("ChangeInEventType2", 2);
                        _SchemaStrLen.Add("CauseCodeExt", 2);
                        _SchemaStrLen.Add("VerbalDesc1", 31);
                        _SchemaStrLen.Add("RevisionCard02", 1);
                        _SchemaStrLen.Add("VerbalDesc2", 55);
                        _SchemaStrLen.Add("RevisionCard03", 1);
                        _SchemaStrLen.Add("VerbalDesc86", 86);
                        _SchemaStrLen.Add("VerbalDescFull", 4000);
                        _SchemaStrLen.Add("FailureMechCode", 4);
                        _SchemaStrLen.Add("TripMech", 1);
                        _SchemaStrLen.Add("PJMLoadStatus", 1);

                        break;
                    }

                case "ANALYSISSETTINGS":
                    {
                        // TABLE AnalysisSettings
                        _Schema.Add("SectionName", "VarChar"); // (50)
                        _Schema.Add("KeyName", "VarChar"); // (50)
                        _Schema.Add("Value", "VarChar"); // (1000)

                        _SchemaStrLen.Add("SectionName", 50);
                        _SchemaStrLen.Add("KeyName", 50);
                        _SchemaStrLen.Add("Value", 1000);

                        break;
                    }
                case "EVENTDATA01AR":
                    {
                        // TABLE EventData01AR
                        _Schema.Add("UtilityUnitCode", "Char"); // ( 6)
                        _Schema.Add("UnitShortName", "Char"); // (10)
                        _Schema.Add("Year", "SmallInt");
                        _Schema.Add("EventNumber", "SmallInt");
                        _Schema.Add("RevisionCard01", "Char"); // ( 1)
                        _Schema.Add("EventType", "Char"); // ( 2)
                        _Schema.Add("StartDateTime", "DateTime");
                        _Schema.Add("CarryOverLastYear", "Bit");
                        _Schema.Add("ChangeDateTime1", "DateTime");
                        _Schema.Add("ChangeInEventType1", "Char"); // ( 2)
                        _Schema.Add("ChangeDateTime2", "DateTime");
                        _Schema.Add("ChangeInEventType2", "Char"); // ( 2)
                        _Schema.Add("EndDateTime", "DateTime");
                        _Schema.Add("CarryOverNextYear", "Bit");
                        _Schema.Add("GrossAvailCapacity", "Decimal"); // Changed December 2005
                        _Schema.Add("NetAvailCapacity", "Decimal"); // Changed December 2005
                        _Schema.Add("CauseCode", "SmallInt");
                        _Schema.Add("CauseCodeExt", "Char"); // ( 2)
                        _Schema.Add("WorkStarted", "DateTime");
                        _Schema.Add("WorkEnded", "DateTime");
                        _Schema.Add("ContribCode", "TinyInt");
                        _Schema.Add("PrimaryAlert", "Bit");
                        _Schema.Add("ManhoursWorked", "SmallInt");
                        _Schema.Add("RevisionCard02", "Char"); // ( 1)
                        _Schema.Add("RevisionCard03", "Char"); // ( 1)
                        _Schema.Add("RevMonthCard01", "DateTime");
                        _Schema.Add("RevMonthCard02", "DateTime");
                        _Schema.Add("RevMonthCard03", "DateTime");
                        _Schema.Add("VerbalDesc86", "Char"); // (86)
                        _Schema.Add("VerbalDescFull", "VarChar"); // (4000)
                        _Schema.Add("FailureMechCode", "Char"); // ( 4)
                        _Schema.Add("TripMech", "Char"); // ( 1)
                        _Schema.Add("CumFiredHours", "Int");
                        _Schema.Add("CumEngineStarts", "Int");
                        _Schema.Add("DominantDerate", "Bit");
                        _Schema.Add("TimeStamp", "DateTime");
                        _Schema.Add("PJMIOCode", "TinyInt");
                        _Schema.Add("Processed", "Bit");

                        _SchemaStrLen.Add("UtilityUnitCode", 6);
                        _SchemaStrLen.Add("UnitShortName", 10);
                        _SchemaStrLen.Add("RevisionCard01", 1);
                        _SchemaStrLen.Add("EventType", 2);
                        _SchemaStrLen.Add("ChangeInEventType1", 2);
                        _SchemaStrLen.Add("ChangeInEventType2", 2);
                        _SchemaStrLen.Add("CauseCodeExt", 2);
                        _SchemaStrLen.Add("RevisionCard02", 1);
                        _SchemaStrLen.Add("RevisionCard03", 1);
                        _SchemaStrLen.Add("VerbalDesc86", 86);
                        _SchemaStrLen.Add("VerbalDescFull", 4000);
                        _SchemaStrLen.Add("FailureMechCode", 4);
                        _SchemaStrLen.Add("TripMech", 1);

                        break;
                    }

                case "EVENTDATA02":
                    {
                        // Table EventData02
                        _Schema.Add("UtilityUnitCode", "Char"); // (6)
                        _Schema.Add("UnitShortName", "Char"); // (10)
                        _Schema.Add("Year", "SmallInt");
                        _Schema.Add("EventNumber", "SmallInt");
                        _Schema.Add("RevisionCardEven", "Char"); // (1)
                        _Schema.Add("EventType", "Char"); // (2)
                        _Schema.Add("CauseCode", "SmallInt");
                        _Schema.Add("CauseCodeExt", "Char"); // (2)
                        _Schema.Add("WorkStarted", "DateTime");
                        _Schema.Add("WorkEnded", "DateTime");
                        _Schema.Add("ContribCode", "TinyInt");
                        _Schema.Add("PrimaryAlert", "Bit");
                        _Schema.Add("ManhoursWorked", "SmallInt");
                        _Schema.Add("VerbalDesc1", "Char"); // (31)
                        _Schema.Add("EvenCardNumber", "TinyInt");
                        _Schema.Add("VerbalDesc2", "Char"); // (55)
                        _Schema.Add("RevisionCardOdd", "Char"); // ( 1)
                        _Schema.Add("RevMonthCardEven", "DateTime");
                        _Schema.Add("RevMonthCardOdd", "DateTime");
                        _Schema.Add("EditFlag", "Bit");
                        _Schema.Add("VerbalDesc86", "Char"); // (86)
                        _Schema.Add("VerbalDescFull", "VarChar"); // (4000)
                        _Schema.Add("FailureMechCode", "Char"); // ( 4)
                        _Schema.Add("TripMech", "Char"); // ( 1)
                        _Schema.Add("CumFiredHours", "Int");
                        _Schema.Add("CumEngineStarts", "Int");
                        _Schema.Add("TimeStamp", "DateTime");
                        _Schema.Add("PJMLoadStatus", "Char"); // ( 1)
                        _Schema.Add("PJMStatusDate", "DateTime");

                        _SchemaStrLen.Add("UtilityUnitCode", 6);
                        _SchemaStrLen.Add("UnitShortName", 10);
                        _SchemaStrLen.Add("RevisionCardEven", 1);
                        _SchemaStrLen.Add("EventType", 2);
                        _SchemaStrLen.Add("CauseCodeExt", 2);
                        _SchemaStrLen.Add("VerbalDesc1", 31);
                        _SchemaStrLen.Add("VerbalDesc2", 55);
                        _SchemaStrLen.Add("RevisionCardOdd", 1);
                        _SchemaStrLen.Add("VerbalDesc86", 86);
                        _SchemaStrLen.Add("VerbalDescFull", 4000);
                        _SchemaStrLen.Add("FailureMechCode", 4);
                        _SchemaStrLen.Add("TripMech", 1);
                        _SchemaStrLen.Add("PJMLoadStatus", 1);
                        break;
                    }

                case "EVENTDATA02AR":
                    {
                        // Table EventData02AR
                        _Schema.Add("UtilityUnitCode", "Char"); // (6)
                        _Schema.Add("UnitShortName", "Char"); // (10)
                        _Schema.Add("Year", "SmallInt");
                        _Schema.Add("EventNumber", "SmallInt");
                        _Schema.Add("RevisionCardEven", "Char"); // (1)
                        _Schema.Add("EventType", "Char"); // (2)
                        _Schema.Add("CauseCode", "SmallInt");
                        _Schema.Add("CauseCodeExt", "Char"); // (2)
                        _Schema.Add("WorkStarted", "DateTime");
                        _Schema.Add("WorkEnded", "DateTime");
                        _Schema.Add("ContribCode", "TinyInt");
                        _Schema.Add("PrimaryAlert", "Bit");
                        _Schema.Add("ManhoursWorked", "SmallInt");
                        _Schema.Add("EvenCardNumber", "TinyInt");
                        _Schema.Add("RevisionCardOdd", "Char"); // ( 1)
                        _Schema.Add("RevMonthCardEven", "DateTime");
                        _Schema.Add("RevMonthCardOdd", "DateTime");
                        _Schema.Add("VerbalDesc86", "Char"); // (86)
                        _Schema.Add("VerbalDescFull", "VarChar"); // (4000)
                        _Schema.Add("FailureMechCode", "Char"); // ( 4)
                        _Schema.Add("TripMech", "Char"); // ( 1)
                        _Schema.Add("CumFiredHours", "Int");
                        _Schema.Add("CumEngineStarts", "Int");
                        _Schema.Add("TimeStamp", "DateTime");
                        _Schema.Add("Processed", "Bit");

                        _SchemaStrLen.Add("UtilityUnitCode", 6);
                        _SchemaStrLen.Add("UnitShortName", 10);
                        _SchemaStrLen.Add("RevisionCardEven", 1);
                        _SchemaStrLen.Add("EventType", 2);
                        _SchemaStrLen.Add("CauseCodeExt", 2);
                        _SchemaStrLen.Add("RevisionCardOdd", 1);
                        _SchemaStrLen.Add("VerbalDesc86", 86);
                        _SchemaStrLen.Add("VerbalDescFull", 4000);
                        _SchemaStrLen.Add("FailureMechCode", 4);
                        _SchemaStrLen.Add("TripMech", 1);
                        break;
                    }

                case "PERFORMANCEDATA":
                    {
                        // Table PerformanceData
                        _Schema.Add("UtilityUnitCode", "Char"); // (6)
                        _Schema.Add("UnitShortName", "Char"); // (10)
                        _Schema.Add("Year", "SmallInt");
                        _Schema.Add("Period", "Char"); // (2)
                        _Schema.Add("RevisionCard1", "Char"); // (1)
                        _Schema.Add("GrossMaxCap", "Decimal"); // Changed December 2005 
                        _Schema.Add("GrossDepCap", "Decimal"); // Changed December 2005
                        _Schema.Add("GrossGen", "Decimal"); // Changed December 2005
                        _Schema.Add("NetMaxCap", "Decimal"); // Changed December 2005
                        _Schema.Add("NetDepCap", "Decimal"); // Changed December 2005
                        _Schema.Add("NetGen", "Decimal"); // Changed December 2005
                        _Schema.Add("TypUnitLoading", "TinyInt");
                        _Schema.Add("AttemptedStarts", "SmallInt");
                        _Schema.Add("ActualStarts", "SmallInt");
                        _Schema.Add("VerbalDesc", "VarChar"); // (25)
                        _Schema.Add("RevisionCard2", "Char"); // ( 1)
                        _Schema.Add("ServiceHours", "Decimal");
                        _Schema.Add("RSHours", "Decimal");
                        _Schema.Add("PumpingHours", "Decimal");
                        _Schema.Add("SynchCondHours", "Decimal");
                        _Schema.Add("PlannedOutageHours", "Decimal");
                        _Schema.Add("ForcedOutageHours", "Decimal");
                        _Schema.Add("MaintOutageHours", "Decimal");
                        _Schema.Add("ExtofSchedOutages", "Decimal");
                        _Schema.Add("PeriodHours", "Decimal");
                        _Schema.Add("InactiveHours", "Decimal"); // New December 2005
                        _Schema.Add("RevisionCard3", "Char"); // (1)
                        _Schema.Add("PriFuelCode", "Char"); // (2)
                        _Schema.Add("PriQtyBurned", "Decimal");
                        _Schema.Add("PriAvgHeatContent", "Int");
                        _Schema.Add("PriBtus", "Decimal");
                        _Schema.Add("PriPercentAsh", "Decimal");
                        _Schema.Add("PriPercentMoisture", "Decimal");
                        _Schema.Add("PriPercentSulfur", "Decimal");
                        _Schema.Add("PriPercentAlkalines", "Decimal");
                        _Schema.Add("PriGrindIndexVanad", "Decimal");
                        _Schema.Add("PriAshSoftTemp", "SmallInt");
                        _Schema.Add("SecFuelCode", "Char"); // (2)
                        _Schema.Add("SecQtyBurned", "Decimal");
                        _Schema.Add("SecAvgHeatContent", "Int");
                        _Schema.Add("SecBtus", "Decimal");
                        _Schema.Add("SecPercentAsh", "Decimal");
                        _Schema.Add("SecPercentMoisture", "Decimal");
                        _Schema.Add("SecPercentSulfur", "Decimal");
                        _Schema.Add("SecPercentAlkalines", "Decimal");
                        _Schema.Add("SecGrindIndexVanad", "Decimal");
                        _Schema.Add("SecAshSoftTemp", "SmallInt");
                        _Schema.Add("RevisionCard4", "Char"); // (1)
                        _Schema.Add("TerFuelCode", "Char"); // (2)
                        _Schema.Add("TerQtyBurned", "Decimal");
                        _Schema.Add("TerAvgHeatContent", "Int");
                        _Schema.Add("TerBtus", "Decimal");
                        _Schema.Add("TerPercentAsh", "Decimal");
                        _Schema.Add("TerPercentMoisture", "Decimal");
                        _Schema.Add("TerPercentSulfur", "Decimal");
                        _Schema.Add("TerPercentAlkalines", "Decimal");
                        _Schema.Add("TerGrindIndexVanad", "Decimal");
                        _Schema.Add("TerAshSoftTemp", "SmallInt");
                        _Schema.Add("QuaFuelCode", "Char"); // (2)
                        _Schema.Add("QuaQtyBurned", "Decimal");
                        _Schema.Add("QuaAvgHeatContent", "Int");
                        _Schema.Add("QuaBtus", "Decimal");
                        _Schema.Add("QuaPercentAsh", "Decimal");
                        _Schema.Add("QuaPercentMoisture", "Decimal");
                        _Schema.Add("QuaPercentSulfur", "Decimal");
                        _Schema.Add("QuaPercentAlkalines", "Decimal");
                        _Schema.Add("QuaGrindIndexVanad", "Decimal");
                        _Schema.Add("QuaAshSoftTemp", "SmallInt");
                        _Schema.Add("RevMonthCard1", "DateTime");
                        _Schema.Add("RevMonthCard2", "DateTime");
                        _Schema.Add("RevMonthCard3", "DateTime");
                        _Schema.Add("RevMonthCard4", "DateTime");
                        _Schema.Add("JOGrossMaxCap", "Decimal"); // Changed December 2005
                        _Schema.Add("JOGrossGen", "Decimal"); // Changed December 2005
                        _Schema.Add("JONetMaxCap", "Decimal"); // Changed December 2005
                        _Schema.Add("JONetGen", "Decimal"); // Changed December 2005
                        _Schema.Add("JOPriQtyBurned", "Decimal");
                        _Schema.Add("JOSecQtyBurned", "Decimal");
                        _Schema.Add("JOTerQtyBurned", "Decimal");
                        _Schema.Add("JOQuaQtyBurned", "Decimal");
                        _Schema.Add("TimeStamp", "DateTime");
                        _Schema.Add("PJMLoadStatus", "Char"); // ( 1)
                        _Schema.Add("PJMStatusDate", "DateTime");

                        _SchemaStrLen.Add("UtilityUnitCode", 6);
                        _SchemaStrLen.Add("UnitShortName", 10);
                        _SchemaStrLen.Add("Period", 2);
                        _SchemaStrLen.Add("RevisionCard1", 1);
                        _SchemaStrLen.Add("VerbalDesc", 25);
                        _SchemaStrLen.Add("RevisionCard2", 1);
                        _SchemaStrLen.Add("RevisionCard3", 1);
                        _SchemaStrLen.Add("PriFuelCode", 2);
                        _SchemaStrLen.Add("SecFuelCode", 2);
                        _SchemaStrLen.Add("RevisionCard4", 1);
                        _SchemaStrLen.Add("TerFuelCode", 2);
                        _SchemaStrLen.Add("QuaFuelCode", 2);
                        _SchemaStrLen.Add("PJMLoadStatus", 1);

                        break;
                    }

                case "PERFORMANCEDATAAR":
                    {
                        // Table PerformanceDataAR
                        _Schema.Add("UtilityUnitCode", "Char"); // (6)
                        _Schema.Add("UnitShortName", "Char"); // (10)
                        _Schema.Add("Year", "SmallInt");
                        _Schema.Add("Period", "Char"); // (2)
                        _Schema.Add("RevisionCard1", "Char"); // (1)
                        _Schema.Add("GrossMaxCap", "Decimal"); // Changed December 2005 
                        _Schema.Add("GrossDepCap", "Decimal"); // Changed December 2005
                        _Schema.Add("GrossGen", "Decimal"); // Changed December 2005
                        _Schema.Add("NetMaxCap", "Decimal"); // Changed December 2005
                        _Schema.Add("NetDepCap", "Decimal"); // Changed December 2005
                        _Schema.Add("NetGen", "Decimal"); // Changed December 2005
                        _Schema.Add("TypUnitLoading", "TinyInt");
                        _Schema.Add("AttemptedStarts", "SmallInt");
                        _Schema.Add("ActualStarts", "SmallInt");
                        _Schema.Add("VerbalDesc", "VarChar"); // (25)
                        _Schema.Add("RevisionCard2", "Char"); // ( 1)
                        _Schema.Add("ServiceHours", "Decimal");
                        _Schema.Add("RSHours", "Decimal");
                        _Schema.Add("PumpingHours", "Decimal");
                        _Schema.Add("SynchCondHours", "Decimal");
                        _Schema.Add("PlannedOutageHours", "Decimal");
                        _Schema.Add("ForcedOutageHours", "Decimal");
                        _Schema.Add("MaintOutageHours", "Decimal");
                        _Schema.Add("ExtofSchedOutages", "Decimal");
                        _Schema.Add("PeriodHours", "Decimal");
                        _Schema.Add("InactiveHours", "Decimal"); // New December 2005
                        _Schema.Add("RevisionCard3", "Char"); // (1)
                        _Schema.Add("PriFuelCode", "Char"); // (2)
                        _Schema.Add("PriQtyBurned", "Decimal");
                        _Schema.Add("PriAvgHeatContent", "Int");
                        _Schema.Add("PriBtus", "Decimal");
                        _Schema.Add("PriPercentAsh", "Decimal");
                        _Schema.Add("PriPercentMoisture", "Decimal");
                        _Schema.Add("PriPercentSulfur", "Decimal");
                        _Schema.Add("PriPercentAlkalines", "Decimal");
                        _Schema.Add("PriGrindIndexVanad", "Decimal");
                        _Schema.Add("PriAshSoftTemp", "SmallInt");
                        _Schema.Add("SecFuelCode", "Char"); // (2)
                        _Schema.Add("SecQtyBurned", "Decimal");
                        _Schema.Add("SecAvgHeatContent", "Int");
                        _Schema.Add("SecBtus", "Decimal");
                        _Schema.Add("SecPercentAsh", "Decimal");
                        _Schema.Add("SecPercentMoisture", "Decimal");
                        _Schema.Add("SecPercentSulfur", "Decimal");
                        _Schema.Add("SecPercentAlkalines", "Decimal");
                        _Schema.Add("SecGrindIndexVanad", "Decimal");
                        _Schema.Add("SecAshSoftTemp", "SmallInt");
                        _Schema.Add("RevisionCard4", "Char"); // (1)
                        _Schema.Add("TerFuelCode", "Char"); // (2)
                        _Schema.Add("TerQtyBurned", "Decimal");
                        _Schema.Add("TerAvgHeatContent", "Int");
                        _Schema.Add("TerBtus", "Decimal");
                        _Schema.Add("TerPercentAsh", "Decimal");
                        _Schema.Add("TerPercentMoisture", "Decimal");
                        _Schema.Add("TerPercentSulfur", "Decimal");
                        _Schema.Add("TerPercentAlkalines", "Decimal");
                        _Schema.Add("TerGrindIndexVanad", "Decimal");
                        _Schema.Add("TerAshSoftTemp", "SmallInt");
                        _Schema.Add("QuaFuelCode", "Char"); // (2)
                        _Schema.Add("QuaQtyBurned", "Decimal");
                        _Schema.Add("QuaAvgHeatContent", "Int");
                        _Schema.Add("QuaBtus", "Decimal");
                        _Schema.Add("QuaPercentAsh", "Decimal");
                        _Schema.Add("QuaPercentMoisture", "Decimal");
                        _Schema.Add("QuaPercentSulfur", "Decimal");
                        _Schema.Add("QuaPercentAlkalines", "Decimal");
                        _Schema.Add("QuaGrindIndexVanad", "Decimal");
                        _Schema.Add("QuaAshSoftTemp", "SmallInt");
                        _Schema.Add("RevMonthCard1", "DateTime");
                        _Schema.Add("RevMonthCard2", "DateTime");
                        _Schema.Add("RevMonthCard3", "DateTime");
                        _Schema.Add("RevMonthCard4", "DateTime");
                        _Schema.Add("JOGrossMaxCap", "Decimal"); // Changed December 2005
                        _Schema.Add("JOGrossGen", "Decimal"); // Changed December 2005
                        _Schema.Add("JONetMaxCap", "Decimal"); // Changed December 2005
                        _Schema.Add("JONetGen", "Decimal"); // Changed December 2005
                        _Schema.Add("JOPriQtyBurned", "Decimal");
                        _Schema.Add("JOSecQtyBurned", "Decimal");
                        _Schema.Add("JOTerQtyBurned", "Decimal");
                        _Schema.Add("JOQuaQtyBurned", "Decimal");
                        _Schema.Add("TimeStamp", "DateTime");
                        _Schema.Add("Processed", "Bit");

                        _SchemaStrLen.Add("UtilityUnitCode", 6);
                        _SchemaStrLen.Add("UnitShortName", 10);
                        _SchemaStrLen.Add("Period", 2);
                        _SchemaStrLen.Add("RevisionCard1", 1);
                        _SchemaStrLen.Add("VerbalDesc", 25);
                        _SchemaStrLen.Add("RevisionCard2", 1);
                        _SchemaStrLen.Add("RevisionCard3", 1);
                        _SchemaStrLen.Add("PriFuelCode", 2);
                        _SchemaStrLen.Add("SecFuelCode", 2);
                        _SchemaStrLen.Add("RevisionCard4", 1);
                        _SchemaStrLen.Add("TerFuelCode", 2);
                        _SchemaStrLen.Add("QuaFuelCode", 2);

                        break;
                    }

                case "PERFORMANCERECORDS":
                    {
                        // Table PerformanceRecords
                        _Schema.Add("UnitShortName", "Char"); // (10)
                        _Schema.Add("UtilityUnitCode", "Char"); // (6)
                        _Schema.Add("Year", "SmallInt");
                        _Schema.Add("Period", "Char"); // (2)
                        _Schema.Add("GrossMaxCap", "Decimal"); // Changed December 2005 
                        _Schema.Add("GrossDepCap", "Decimal"); // Changed December 2005
                        _Schema.Add("GrossGen", "Decimal"); // Changed December 2005
                        _Schema.Add("NetMaxCap", "Decimal"); // Changed December 2005
                        _Schema.Add("NetDepCap", "Decimal"); // Changed December 2005
                        _Schema.Add("NetGen", "Decimal"); // Changed December 2005
                        _Schema.Add("PriFuelCode", "Char"); // (2)
                        _Schema.Add("PriQtyBurned", "Decimal");
                        _Schema.Add("PriAvgHeatContent", "Int");
                        _Schema.Add("PriBtus", "Decimal");
                        _Schema.Add("PriPercentAsh", "Decimal");
                        _Schema.Add("PriPercentMoisture", "Decimal");
                        _Schema.Add("PriPercentSulfur", "Decimal");
                        _Schema.Add("PriPercentAlkalines", "Decimal");
                        _Schema.Add("PriGrindIndexVanad", "Decimal");
                        _Schema.Add("PriAshSoftTemp", "SmallInt");
                        _Schema.Add("SecFuelCode", "Char"); // (2)
                        _Schema.Add("SecQtyBurned", "Decimal");
                        _Schema.Add("SecAvgHeatContent", "Int");
                        _Schema.Add("SecBtus", "Decimal");
                        _Schema.Add("SecPercentAsh", "Decimal");
                        _Schema.Add("SecPercentMoisture", "Decimal");
                        _Schema.Add("SecPercentSulfur", "Decimal");
                        _Schema.Add("SecPercentAlkalines", "Decimal");
                        _Schema.Add("SecGrindIndexVanad", "Decimal");
                        _Schema.Add("SecAshSoftTemp", "SmallInt");
                        _Schema.Add("TerFuelCode", "Char"); // (2)
                        _Schema.Add("TerQtyBurned", "Decimal");
                        _Schema.Add("TerAvgHeatContent", "Int");
                        _Schema.Add("TerBtus", "Decimal");
                        _Schema.Add("TerPercentAsh", "Decimal");
                        _Schema.Add("TerPercentMoisture", "Decimal");
                        _Schema.Add("TerPercentSulfur", "Decimal");
                        _Schema.Add("TerPercentAlkalines", "Decimal");
                        _Schema.Add("TerGrindIndexVanad", "Decimal");
                        _Schema.Add("TerAshSoftTemp", "SmallInt");
                        _Schema.Add("QuaFuelCode", "Char"); // (2)
                        _Schema.Add("QuaQtyBurned", "Decimal");
                        _Schema.Add("QuaAvgHeatContent", "Int");
                        _Schema.Add("QuaBtus", "Decimal");
                        _Schema.Add("QuaPercentAsh", "Decimal");
                        _Schema.Add("QuaPercentMoisture", "Decimal");
                        _Schema.Add("QuaPercentSulfur", "Decimal");
                        _Schema.Add("QuaPercentAlkalines", "Decimal");
                        _Schema.Add("QuaGrindIndexVanad", "Decimal");
                        _Schema.Add("QuaAshSoftTemp", "SmallInt");
                        _Schema.Add("ServiceHoursCalc", "Decimal");
                        _Schema.Add("PeriodHours", "Decimal");
                        _Schema.Add("InactiveHours", "Decimal"); // New December 2005
                        _Schema.Add("NOF", "Float");
                        _Schema.Add("GOF", "Float");
                        _Schema.Add("NCF", "Float");
                        _Schema.Add("GCF", "Float");
                        _Schema.Add("NHR", "Float");
                        _Schema.Add("GHR", "Float");
                        _Schema.Add("TotalBtus", "Decimal");
                        _Schema.Add("AttemptedStarts", "Int");
                        _Schema.Add("ActualStarts", "Int");
                        _Schema.Add("StartingReliability", "Decimal");
                        _Schema.Add("PerfCalcDate", "DateTime");
                        _Schema.Add("ServiceHours", "Decimal");
                        _Schema.Add("RSHours", "Decimal");
                        _Schema.Add("PumpingHours", "Decimal");
                        _Schema.Add("SynchCondHours", "Decimal");
                        _Schema.Add("PlannedOutageHours", "Decimal");
                        _Schema.Add("ForcedOutageHours", "Decimal");
                        _Schema.Add("MaintOutageHours", "Decimal");
                        _Schema.Add("ExtofSchedOutages", "Decimal");
                        _Schema.Add("lQuarterlyData", "Bit");
                        _Schema.Add("JOGrossMaxCap", "Decimal"); // Changed December 2005
                        _Schema.Add("JOGrossGen", "Decimal"); // Changed December 2005
                        _Schema.Add("JONetMaxCap", "Decimal"); // Changed December 2005
                        _Schema.Add("JONetGen", "Decimal"); // Changed December 2005
                        _Schema.Add("JOPriQtyBurned", "Decimal");
                        _Schema.Add("JOSecQtyBurned", "Decimal");
                        _Schema.Add("JOTerQtyBurned", "Decimal");
                        _Schema.Add("JOQuaQtyBurned", "Decimal");
                        _Schema.Add("JOTotalBtus", "Decimal");
                        _Schema.Add("TimeStamp", "DateTime");
                        _Schema.Add("UnitAge", "Float");
                        _Schema.Add("G_ServiceHours", "Decimal");
                        _Schema.Add("G_PeriodHours", "Decimal");
                        _Schema.Add("G_ServiceHoursCalc", "Decimal");
                        _Schema.Add("N_ServiceHours", "Decimal");
                        _Schema.Add("N_PeriodHours", "Decimal");
                        _Schema.Add("N_ServiceHoursCalc", "Decimal");

                        _SchemaStrLen.Add("UnitShortName", 10);
                        _SchemaStrLen.Add("UtilityUnitCode", 6);
                        _SchemaStrLen.Add("Period", 2);
                        _SchemaStrLen.Add("PriFuelCode", 2);
                        _SchemaStrLen.Add("SecFuelCode", 2);
                        _SchemaStrLen.Add("TerFuelCode", 2);
                        _SchemaStrLen.Add("QuaFuelCode", 2);

                        break;
                    }
                case "SETUP":
                    {
                        // Table Setup
                        _Schema.Add("UtilityUnitCode", "Char"); // ( 6)
                        _Schema.Add("UnitName", "VarChar"); // (50)
                        _Schema.Add("UnitShortName", "Char"); // (10)
                        _Schema.Add("GrossMaxCapacity", "Decimal"); // Changed December 2005
                        _Schema.Add("NetMaxCapacity", "Decimal"); // Changed December 2005
                        _Schema.Add("PriFuelCode", "Char"); // ( 2)
                        _Schema.Add("PriHeatContent", "Int");
                        _Schema.Add("SecFuelCode", "Char"); // ( 2)
                        _Schema.Add("SecHeatContent", "Int");
                        _Schema.Add("UnitType", "VarChar"); // (50)
                        _Schema.Add("GrossDepCapacity", "Decimal"); // Changed December 2005
                        _Schema.Add("NetDepCapacity", "Decimal"); // Changed December 2005
                        _Schema.Add("TypUnitLoading", "TinyInt");
                        _Schema.Add("VerbalDesc", "Char"); // (25)
                        _Schema.Add("DaylightSavingTime", "Int");
                        _Schema.Add("JointOwnership", "Bit");
                        _Schema.Add("ExpandedReporting", "Bit");
                        _Schema.Add("InputDataRequirements", "TinyInt");
                        _Schema.Add("FuelQtyFormat", "TinyInt");
                        _Schema.Add("BtuOrHeatContent", "Bit");
                        _Schema.Add("FuelQualityShow", "Bit");
                        _Schema.Add("Perf02DecimalPlaces", "Bit");
                        _Schema.Add("BulkRS", "Bit");
                        _Schema.Add("EnglishOrMetric", "Bit");
                        _Schema.Add("ServiceHourMethod", "TinyInt");
                        _Schema.Add("CommercialDate", "DateTime");
                        _Schema.Add("RetirementDate", "DateTime");
                        _Schema.Add("GMCWeighting", "SmallInt");
                        _Schema.Add("NMCWeighting", "SmallInt");
                        _Schema.Add("PerformanceDate", "DateTime");
                        _Schema.Add("PerformanceStart", "DateTime");
                        _Schema.Add("Example3D", "Bit");
                        _Schema.Add("CheckStatus", "Char"); // ( 1)
                        _Schema.Add("NERC", "Bit");
                        _Schema.Add("NYISO", "Bit");
                        _Schema.Add("PJM", "Bit");
                        _Schema.Add("ISONE", "Bit");
                        _Schema.Add("UnitSelected", "Bit");
                        _Schema.Add("MaxCapReadOnly", "Bit");
                        _Schema.Add("WorkDetails", "Bit");
                        _Schema.Add("CopyToVerbDesc", "Bit");
                        _Schema.Add("CauseCodeExtDisplay", "Bit");
                        _Schema.Add("CauseCodeExtEditable", "Bit");
                        _Schema.Add("DepCapReadOnly", "Bit");
                        _Schema.Add("PumpingData", "Bit");
                        _Schema.Add("SynchCondData", "Bit");
                        _Schema.Add("GrossNetBoth", "TinyInt");
                        _Schema.Add("PJMStartsCount", "Bit");

                        _Schema.Add("CombinedCycleName", "VarChar"); // (50)
                        _Schema.Add("CombinedCycleShortName", "Char"); // (10)

                        /* Analysis & Reporting Granularity Flags */
                        _Schema.Add("Hourly", "Bit");
                        _Schema.Add("Daily", "Bit");
                        _Schema.Add("WeeklyMS", "Bit");
                        _Schema.Add("WeeklySS", "Bit");
                        _Schema.Add("FourWeeks", "Bit");
                        _Schema.Add("Monthly", "Bit");
                        _Schema.Add("Quarterly", "Bit");
                        _Schema.Add("Yearly", "Bit");
                        _Schema.Add("FixedN", "Bit");
                        _Schema.Add("FixedNValue", "SmallInt");
                        _Schema.Add("FixedByUser", "Bit");
                        _Schema.Add("FixedByUserFile", "VarChar"); // (50)
                        _Schema.Add("PeakPeriod", "Bit");
                        _Schema.Add("PeakPeriodSet", "VarChar"); // (50)

                        _Schema.Add("GenElect", "Bit"); // new December 2005
                        _Schema.Add("Siemens", "Bit"); // new December 2005
                        _Schema.Add("CAISO", "Bit"); // new December 2005
                        _Schema.Add("CEA", "Bit"); // new December 2005
                        _Schema.Add("MISO", "Bit"); // new December 2005
                        _Schema.Add("ISO1", "Bit"); // new December 2005
                        _Schema.Add("ISO2", "Bit"); // new December 2005
                        _Schema.Add("ISO3", "Bit"); // new December 2005
                        _Schema.Add("CHP", "Bit"); // new December 2005

                        _SchemaStrLen.Add("UtilityUnitCode", 6);
                        _SchemaStrLen.Add("UnitName", 50);
                        _SchemaStrLen.Add("UnitShortName", 10);
                        _SchemaStrLen.Add("PriFuelCode", 2);
                        _SchemaStrLen.Add("SecFuelCode", 2);
                        _SchemaStrLen.Add("UnitType", 50);
                        _SchemaStrLen.Add("VerbalDesc", 25);
                        _SchemaStrLen.Add("CheckStatus", 1);

                        _SchemaStrLen.Add("CombinedCycleName", 50);
                        _SchemaStrLen.Add("CombinedCycleShortName", 10);
                        _SchemaStrLen.Add("FixedByUserFile", 50);
                        _SchemaStrLen.Add("PeakPeriodSet", 50);

                        break;
                    }

                case "WRITESTATUS":
                    {
                        // Table WriteStatus
                        _Schema.Add("Organization", "VarChar"); // ( 50)
                        _Schema.Add("EventFile", "VarChar"); // (128)
                        _Schema.Add("PerfFile", "VarChar"); // (128)
                        _Schema.Add("OtherFile", "VarChar"); // (128)
                        _Schema.Add("WhenWrittenUTC", "DateTime");

                        _SchemaStrLen.Add("Organization", 50);
                        _SchemaStrLen.Add("EventFile", 128);
                        _SchemaStrLen.Add("PerfFile", 128);
                        _SchemaStrLen.Add("OtherFile", 128);

                        break;
                    }

                case "ANALYSISSTATUS":
                    {
                        // Table AnalysisStatus
                        _Schema.Add("Task", "Char"); // ( 10)
                        _Schema.Add("LastDateTime", "DateTime");

                        _SchemaStrLen.Add("Task", 10);

                        break;
                    }

                case "CAUSECODEEXTENSIONS":
                    {
                        // Table CauseCodeExtensions
                        // Code, CodeDescription, StartingCauseCode, EndingCauseCode
                        _Schema.Add("Code", "Char"); // (  2)
                        _Schema.Add("CodeDescription", "Char"); // (100)
                        _Schema.Add("StartingCauseCode", "SmallInt");
                        _Schema.Add("EndingCauseCode", "SmallInt");

                        _SchemaStrLen.Add("Code", 2);
                        _SchemaStrLen.Add("CodeDescription", 100);

                        break;
                    }

                case "NGGROUPS":
                    {
                        // Table NGGroups
                        _Schema.Add("GroupName", "VarChar"); // (50)
                        _Schema.Add("GroupID", "Int");
                        _Schema.Add("ARPermissions", "VarChar"); // (10)
                        _Schema.Add("YearLimit", "Int");
                        _Schema.Add("LoadASCII", "Bit");
                        _Schema.Add("LoadDBFs", "Bit");
                        _Schema.Add("WriteNERC", "Bit");
                        _Schema.Add("WriteNYISO", "Bit");
                        _Schema.Add("WritePJM", "Bit");
                        _Schema.Add("WriteISONE", "Bit");
                        _Schema.Add("WriteGenElect", "Bit"); // new December 2005
                        _Schema.Add("WriteSiemens", "Bit"); // new December 2005
                        _Schema.Add("WriteCAISO", "Bit"); // new December 2005
                        _Schema.Add("WriteCEA", "Bit"); // new December 2005
                        _Schema.Add("WriteMISO", "Bit"); // new December 2005
                        _Schema.Add("WriteISO1", "Bit"); // new December 2005
                        _Schema.Add("WriteISO2", "Bit"); // new December 2005
                        _Schema.Add("WriteISO3", "Bit"); // new December 2005

                        _SchemaStrLen.Add("GroupName", 50);
                        _SchemaStrLen.Add("APPermissions", 10);

                        break;
                    }

                case "ARGROUPS":
                    {
                        // Table ARGroups
                        _Schema.Add("GroupName", "VarChar"); // (50)
                        _Schema.Add("GroupShortName", "Char"); // (10)
                        _Schema.Add("GroupID", "Int");
                        _Schema.Add("ARPermissions", "Int");
                        _Schema.Add("GroupSelected", "Bit");

                        _Schema.Add("Hourly", "Bit");
                        _Schema.Add("Daily", "Bit");
                        _Schema.Add("WeeklyMS", "Bit");
                        _Schema.Add("WeeklySS", "Bit");
                        _Schema.Add("FourWeeks", "Bit");
                        _Schema.Add("Monthly", "Bit");
                        _Schema.Add("Quarterly", "Bit");
                        _Schema.Add("Yearly", "Bit");

                        _SchemaStrLen.Add("GroupName", 50);
                        _SchemaStrLen.Add("GroupShortName", 10);

                        break;
                    }

                case "NGUNITPERM":
                    {
                        // Table NGUnitPerm
                        _Schema.Add("UtilityUnitCode", "Char");
                        _Schema.Add("GroupID", "Int");

                        _SchemaStrLen.Add("UtilityUnitCode", 6);

                        break;
                    }

                case "ARUNITPERM":
                    {
                        // Table ARUnitPerm
                        _Schema.Add("UnitShortName", "Char");
                        _Schema.Add("GroupID", "Int");

                        _SchemaStrLen.Add("UnitShortName", 10);

                        break;
                    }

                case "NGUSERS":
                    {
                        // Table NGUsers
                        _Schema.Add("LoginID", "VarChar"); // (100)
                        _Schema.Add("Password", "VarChar"); // (500)
                        _Schema.Add("First_Name", "VarChar"); // ( 30)
                        _Schema.Add("Last_Name", "VarChar"); // ( 30)
                        _Schema.Add("Email_Address", "VarChar"); // (100)
                        _Schema.Add("Phone", "VarChar"); // ( 15)
                        _Schema.Add("LocationDescription", "VarChar"); // (100)

                        _SchemaStrLen.Add("LoginID", 100);
                        _SchemaStrLen.Add("Password", 500);
                        _SchemaStrLen.Add("First_Name", 30);
                        _SchemaStrLen.Add("Last_Name", 30);
                        _SchemaStrLen.Add("Email_Address", 100);
                        _SchemaStrLen.Add("Phone", 15);
                        _SchemaStrLen.Add("LocationDescription", 100);

                        break;
                    }

                case "FAVUNITS":
                    {
                        // Table FavUnits
                        _Schema.Add("LoginID", "VarChar"); // (100)
                        _Schema.Add("UnitName", "VarChar"); // ( 50)

                        _SchemaStrLen.Add("LoginID", 100);
                        _SchemaStrLen.Add("UnitName", 50);

                        break;
                    }

                case "FAVGROUPS":
                    {
                        // Table FavGroups
                        _Schema.Add("LoginID", "VarChar"); // (100)
                        _Schema.Add("GroupName", "VarChar"); // ( 50)

                        _SchemaStrLen.Add("LoginID", 100);
                        _SchemaStrLen.Add("GroupName", 50);

                        break;
                    }

                case "NERCXREF":
                    {
                        // ISO NE Only
                        _Schema.Add("UnitShortName", "Char"); //	10
                        _Schema.Add("UnitName", "VarChar"); //	50
                        _Schema.Add("NERCClassID", "SmallInt");
                        _Schema.Add("GADSMethod", "TinyInt");

                        _SchemaStrLen.Add("UnitShortName", 10);
                        _SchemaStrLen.Add("UnitName", 50);

                        break;
                    }

                case "OFCALCRESULTS":
                    {
                        // ISO NE Only
                        _Schema.Add("UnitShortName", "Char"); //	10
                        _Schema.Add("ISTge", "SmallInt");
                        _Schema.Add("CFNumerator", "Float");
                        _Schema.Add("CFDenominator", "Float");
                        _Schema.Add("ISTbefore", "SmallInt");
                        _Schema.Add("CCF", "Float");
                        _Schema.Add("ISTMissing", "SmallInt");
                        _Schema.Add("OFgm", "Float");
                        _Schema.Add("DateRange", "VarChar"); //	50
                        _Schema.Add("EndDate", "DateTime");
                        _Schema.Add("SingleMonth", "Float");

                        _SchemaStrLen.Add("UnitShortName", 10);
                        _SchemaStrLen.Add("DateRange", 50);

                        break;
                    }

                case "EFORDCALCRESULTS":
                    {
                        // ISO NE Only
                        _Schema.Add("UnitShortName", "Char"); //	10
                        _Schema.Add("ISTge", "SmallInt");
                        _Schema.Add("FL_Numerator", "Float");
                        _Schema.Add("FL_Denominator", "Float");
                        _Schema.Add("ISTbefore", "SmallInt");
                        _Schema.Add("CEFORd", "Float");
                        _Schema.Add("ISTMissing", "SmallInt");
                        _Schema.Add("EFORdgm", "Float");
                        _Schema.Add("DateRange", "VarChar"); //	50
                        _Schema.Add("EndDate", "DateTime");
                        _Schema.Add("SingleMonth", "Float");

                        _SchemaStrLen.Add("UnitShortName", 10);
                        _SchemaStrLen.Add("DateRange", 50);

                        break;
                    }

                case "NERCCLASSES":
                    {
                        // ISO NE Only
                        _Schema.Add("NERCClassID", "SmallInt");
                        _Schema.Add("ClassDesc", "VarChar"); //	80
                        _Schema.Add("CCF", "Float");
                        _Schema.Add("CEFORd", "Float");
                        _Schema.Add("EffectiveDate", "DateTime");

                        _SchemaStrLen.Add("ClassDesc", 80);

                        break;
                    }

                case "DEFAULTCAPACITIES":    // new December 2005
                    {
                        _Schema.Add("UtilityUnitCode", "Char"); // ( 6)
                        _Schema.Add("UnitShortName", "Char"); // (10)

                        _Schema.Add("JanGMC", "Float");
                        _Schema.Add("FebGMC", "Float");
                        _Schema.Add("MarGMC", "Float");
                        _Schema.Add("AprGMC", "Float");
                        _Schema.Add("MayGMC", "Float");
                        _Schema.Add("JunGMC", "Float");
                        _Schema.Add("JulGMC", "Float");
                        _Schema.Add("AugGMC", "Float");
                        _Schema.Add("SepGMC", "Float");
                        _Schema.Add("OctGMC", "Float");
                        _Schema.Add("NovGMC", "Float");
                        _Schema.Add("DecGMC", "Float");

                        _Schema.Add("JanNMC", "Float");
                        _Schema.Add("FebNMC", "Float");
                        _Schema.Add("MarNMC", "Float");
                        _Schema.Add("AprNMC", "Float");
                        _Schema.Add("MayNMC", "Float");
                        _Schema.Add("JunNMC", "Float");
                        _Schema.Add("JulNMC", "Float");
                        _Schema.Add("AugNMC", "Float");
                        _Schema.Add("SepNMC", "Float");
                        _Schema.Add("OctNMC", "Float");
                        _Schema.Add("NovNMC", "Float");
                        _Schema.Add("DecNMC", "Float");

                        _Schema.Add("JanGDC", "Float");
                        _Schema.Add("FebGDC", "Float");
                        _Schema.Add("MarGDC", "Float");
                        _Schema.Add("AprGDC", "Float");
                        _Schema.Add("MayGDC", "Float");
                        _Schema.Add("JunGDC", "Float");
                        _Schema.Add("JulGDC", "Float");
                        _Schema.Add("AugGDC", "Float");
                        _Schema.Add("SepGDC", "Float");
                        _Schema.Add("OctGDC", "Float");
                        _Schema.Add("NovGDC", "Float");
                        _Schema.Add("DecGDC", "Float");

                        _Schema.Add("JanNDC", "Float");
                        _Schema.Add("FebNDC", "Float");
                        _Schema.Add("MarNDC", "Float");
                        _Schema.Add("AprNDC", "Float");
                        _Schema.Add("MayNDC", "Float");
                        _Schema.Add("JunNDC", "Float");
                        _Schema.Add("JulNDC", "Float");
                        _Schema.Add("AugNDC", "Float");
                        _Schema.Add("SepNDC", "Float");
                        _Schema.Add("OctNDC", "Float");
                        _Schema.Add("NovNDC", "Float");
                        _Schema.Add("DecNDC", "Float");

                        _SchemaStrLen.Add("UtilityUnitCode", 6);
                        _SchemaStrLen.Add("UnitShortName", 10);

                        break;
                    }

                case "PERFDATA":
                    {
                        // ISO NE Only
                        _Schema.Add("UnitShortName", "Char"); //	10
                        _Schema.Add("PerfYear", "SmallInt");
                        _Schema.Add("ReportPeriod", "TinyInt");
                        _Schema.Add("RevisionCode", "Char"); //	1
                        _Schema.Add("NMC", "SmallInt");
                        _Schema.Add("NDC", "SmallInt");
                        _Schema.Add("NAGen", "Int");
                        _Schema.Add("Numerator", "Float");
                        _Schema.Add("Denominator", "Float");
                        _Schema.Add("CFForMonth", "Float");
                        _Schema.Add("ServiceHours", "SmallInt");
                        _Schema.Add("RSHours", "SmallInt");
                        _Schema.Add("PumpingHours", "SmallInt");
                        _Schema.Add("SynchCondHours", "SmallInt");
                        _Schema.Add("AvailableHours", "SmallInt");
                        _Schema.Add("POHours", "SmallInt");
                        _Schema.Add("FOHours", "SmallInt");
                        _Schema.Add("MOHours", "SmallInt");
                        _Schema.Add("SEHours", "SmallInt");
                        _Schema.Add("UnavailableHours", "SmallInt");
                        _Schema.Add("PeriodHours", "SmallInt");

                        _SchemaStrLen.Add("UnitShortName", 10);
                        _SchemaStrLen.Add("RevisionCode", 1);

                        break;
                    }

                case "NGUSERTOGROUP":
                    {
                        // Table NGUserToGroup
                        _Schema.Add("LoginID", "VarChar"); // (100)
                        _Schema.Add("GroupID", "Int");

                        _SchemaStrLen.Add("LoginID", 100);

                        break;
                    }

                case "ARUSERTOGROUP":
                    {
                        // Table ARUserToGroup
                        _Schema.Add("LoginID", "VarChar"); // (100)
                        _Schema.Add("GroupID", "Int");

                        _SchemaStrLen.Add("LoginID", 100);

                        break;
                    }

                case "CAUSECODEGROUPS":
                    {
                        _Schema.Add("BeginCauseCode", "SmallInt");
                        _Schema.Add("EndingCauseCode", "SmallInt");
                        _Schema.Add("GroupName", "Char"); // (10)
                        _Schema.Add("GroupLongName", "Char"); // (70)

                        _SchemaStrLen.Add("GroupName", 10);
                        _SchemaStrLen.Add("GroupLongName", 70);

                        break;
                    }

                case "SAVEDVERBALDESC":
                    {
                        // Table SavedVerbalDesc
                        _Schema.Add("LoginID", "VarChar"); // (100)
                        _Schema.Add("Description", "VarChar"); // (100)

                        _SchemaStrLen.Add("LoginID", 100);
                        _SchemaStrLen.Add("Description", 100);

                        break;
                    }

                case "ERRORS":
                    {
                        // Table Errors
                        _Schema.Add("UtilityUnitCode", "Char"); // ( 6)
                        _Schema.Add("Year", "SmallInt");
                        _Schema.Add("Period", "Char"); // ( 2)
                        _Schema.Add("EventNumber", "SmallInt");
                        _Schema.Add("EventEvenCardNo", "TinyInt");
                        _Schema.Add("ErrorMessage", "VarChar"); // (80)
                        _Schema.Add("ErrorSeverity", "Char"); // ( 1) W-warning / E-error / M-missing
                        _Schema.Add("UnitShortName", "Char"); // (10)

                        _SchemaStrLen.Add("UtilityUnitCode", 6);
                        _SchemaStrLen.Add("Period", 2);
                        _SchemaStrLen.Add("ErrorMessage", 80);
                        _SchemaStrLen.Add("ErrorSeverity", 1);
                        _SchemaStrLen.Add("UnitShortName", 10);

                        break;
                    }

                case "EVENTRECORDS":
                    {
                        _Schema.Add("UnitShortName", "Char"); // (10)
                        _Schema.Add("EventNumber", "SmallInt");
                        _Schema.Add("EventType", "VarChar"); // ( 2)
                        _Schema.Add("StartDateTime", "DateTime");
                        _Schema.Add("EndDateTime", "DateTime");
                        _Schema.Add("EventContribCode", "TinyInt");
                        _Schema.Add("EquipGroupName", "VarChar"); // (10)
                        _Schema.Add("CauseCode", "SmallInt");
                        _Schema.Add("CauseCodeExt", "VarChar"); // ( 2)
                        _Schema.Add("VerbalDesc86", "VarChar"); // (86)
                        _Schema.Add("VerbalDescFull", "VarChar"); // (4000)
                        _Schema.Add("ClockHours", "Decimal");
                        _Schema.Add("CalcHours", "Decimal");
                        _Schema.Add("EquivHours", "Decimal");
                        _Schema.Add("WorkStarted", "DateTime");
                        _Schema.Add("WorkEnded", "DateTime");
                        _Schema.Add("GrossAvailCapacity", "Decimal"); // Changed December 2005
                        _Schema.Add("NetAvailCapacity", "Decimal"); // Changed December 2005
                        _Schema.Add("ManhoursWorked", "SmallInt");
                        _Schema.Add("ContribCode", "TinyInt");
                        _Schema.Add("EquivMWhrs", "Decimal");
                        _Schema.Add("lFilter", "Bit");
                        _Schema.Add("ExtensionType", "NVarChar"); // ( 2)
                        _Schema.Add("CarryOverLastYear", "Bit");
                        _Schema.Add("CarryOverNextYear", "Bit");
                        _Schema.Add("FailureMechCode", "Char"); // ( 4)
                        _Schema.Add("TripMech", "Char"); // ( 1)
                        _Schema.Add("CumFiredHours", "Int");
                        _Schema.Add("CumEngineStarts", "Int");
                        _Schema.Add("DominantDerate", "Bit");
                        _Schema.Add("PlantMgtControl", "Bit");
                        _Schema.Add("PJMIOCode", "TinyInt");
                        _Schema.Add("DeratingAmount", "Decimal");  // new December 2005

                        _SchemaStrLen.Add("UnitShortName", 10);
                        _SchemaStrLen.Add("EventType", 2);
                        _SchemaStrLen.Add("EquipGroupName", 10);
                        _SchemaStrLen.Add("CauseCodeExt", 2);
                        _SchemaStrLen.Add("VerbalDesc86", 86);
                        _SchemaStrLen.Add("VerbalDescFull", 4000);
                        _SchemaStrLen.Add("ExtensionType", 2);
                        _SchemaStrLen.Add("FailureMechCode", 4);
                        _SchemaStrLen.Add("TripMech", 1);

                        break;
                    }

                case "EVENTDETAILS":
                    {
                        _Schema.Add("UnitShortName", "Char"); // (10)
                        _Schema.Add("UtilityUnitCode", "Char"); // ( 6)
                        _Schema.Add("EventNumber", "SmallInt");
                        _Schema.Add("EventType", "VarChar"); // ( 2)
                        _Schema.Add("TL_DateTime", "DateTime");
                        _Schema.Add("ContribCode", "TinyInt");
                        _Schema.Add("EventContribCode", "TinyInt");
                        _Schema.Add("EquipGroupName", "VarChar"); // (10)
                        _Schema.Add("CauseCode", "SmallInt");
                        _Schema.Add("CauseCodeExt", "VarChar"); // ( 2)
                        _Schema.Add("CalcHours", "Decimal");
                        _Schema.Add("EquivHours", "Decimal");
                        _Schema.Add("EV_DateTime", "DateTime");
                        _Schema.Add("EquivMWhrs", "Decimal");
                        _Schema.Add("EUFDH_RS", "Decimal");
                        _Schema.Add("E_EUFDH_RS", "Decimal");
                        _Schema.Add("ExtensionType", "NVarChar"); // ( 2)
                        _Schema.Add("lFilter", "Bit");
                        _Schema.Add("Granularity", "VarChar"); // (50)
                        _Schema.Add("PlantMgtControl", "Bit");
                        _Schema.Add("PJMIOCode", "TinyInt");
                        _Schema.Add("EPDH_RS", "Decimal");
                        _Schema.Add("E_EPDH_RS", "Decimal");
                        _Schema.Add("EMDH_RS", "Decimal");
                        _Schema.Add("E_EMDH_RS", "Decimal");

                        _SchemaStrLen.Add("UnitShortName", 10);
                        _SchemaStrLen.Add("UtilityUnitCode", 6);
                        _SchemaStrLen.Add("EventType", 2);
                        _SchemaStrLen.Add("EquipGroupName", 10);
                        _SchemaStrLen.Add("CauseCodeExt", 2);
                        _SchemaStrLen.Add("ExtensionType", 2);
                        _SchemaStrLen.Add("Granularity", 50);

                        break;
                    }

                case "EVENTHOURS":
                    {
                        // Table EventHours
                        _Schema.Add("UnitShortName", "Char"); // (10)
                        _Schema.Add("TL_DateTime", "DateTime");
                        _Schema.Add("PO", "Float");
                        _Schema.Add("PO_SE", "Float");
                        _Schema.Add("MO", "Float");
                        _Schema.Add("MO_SE", "Float");
                        _Schema.Add("SF", "Float");
                        _Schema.Add("U1", "Float");
                        _Schema.Add("U2", "Float");
                        _Schema.Add("U3", "Float");
                        _Schema.Add("D1", "Float");
                        _Schema.Add("D2", "Float");
                        _Schema.Add("D3", "Float");
                        _Schema.Add("D4", "Float");
                        _Schema.Add("D4_DE", "Float");
                        _Schema.Add("PD", "Float");
                        _Schema.Add("PD_DE", "Float");
                        _Schema.Add("RS", "Float");
                        _Schema.Add("NC", "Float");
                        _Schema.Add("EUFDH_RS", "Float");
                        _Schema.Add("EPDH_RS", "Float");
                        _Schema.Add("EMDH_RS", "Float");
                        _Schema.Add("SH", "Float");
                        _Schema.Add("PH", "Float");
                        _Schema.Add("ESEDH", "Float");
                        _Schema.Add("AH", "Float");
                        _Schema.Add("POF", "Float");
                        _Schema.Add("UOF", "Float");
                        _Schema.Add("FOF", "Float");
                        _Schema.Add("MOF", "Float");
                        _Schema.Add("SOF", "Float");
                        _Schema.Add("UF", "Float");
                        _Schema.Add("AF", "Float");
                        _Schema.Add("SEF", "Float");
                        _Schema.Add("SDF", "Float");
                        _Schema.Add("UDF", "Float");
                        _Schema.Add("EUF", "Float");
                        _Schema.Add("EAF", "Float");
                        _Schema.Add("EMOF", "Float");
                        _Schema.Add("EPOF", "Float");
                        _Schema.Add("EFOF", "Float");
                        _Schema.Add("ESOF", "Float");
                        _Schema.Add("EUOF", "Float");
                        _Schema.Add("EPOR", "Float");
                        _Schema.Add("EMOR", "Float");
                        _Schema.Add("EUOR", "Float");
                        _Schema.Add("FORate", "Float");
                        _Schema.Add("EFOR", "Float");
                        _Schema.Add("E_PO", "Float");
                        _Schema.Add("E_PO_SE", "Float");
                        _Schema.Add("E_MO", "Float");
                        _Schema.Add("E_MO_SE", "Float");
                        _Schema.Add("E_SF", "Float");
                        _Schema.Add("E_U1", "Float");
                        _Schema.Add("E_U2", "Float");
                        _Schema.Add("E_U3", "Float");
                        _Schema.Add("E_D1", "Float");
                        _Schema.Add("E_D2", "Float");
                        _Schema.Add("E_D3", "Float");
                        _Schema.Add("E_D4", "Float");
                        _Schema.Add("E_D4_DE", "Float");
                        _Schema.Add("E_PD", "Float");
                        _Schema.Add("E_PD_DE", "Float");
                        _Schema.Add("E_RS", "Float");
                        _Schema.Add("E_NC", "Float");
                        _Schema.Add("E_EUFDH_RS", "Float");
                        _Schema.Add("E_EPDH_RS", "Float");
                        _Schema.Add("E_EMDH_RS", "Float");
                        _Schema.Add("E_SH", "Float");
                        _Schema.Add("E_PH", "Float");
                        _Schema.Add("E_ESEDH", "Float");
                        _Schema.Add("E_AH", "Float");
                        _Schema.Add("PumpingHours", "Float");
                        _Schema.Add("SynchHours", "Float");
                        _Schema.Add("E_PumpingHours", "Float");
                        _Schema.Add("E_SynchHours", "Float");
                        _Schema.Add("Granularity", "VarChar"); // (50)
                        _Schema.Add("PlantMgtControl", "Bit");
                        _Schema.Add("UnitAge", "Float");
                        _Schema.Add("AH_OMC", "Float");
                        _Schema.Add("FOH_OMC", "Float");
                        _Schema.Add("EFDH_OMC", "Float");
                        _Schema.Add("E_AH_OMC", "Float");
                        _Schema.Add("E_FOH_OMC", "Float");
                        _Schema.Add("E_EFDH_OMC", "Float");
                        _Schema.Add("NPO", "Int");  // new December 2005
                        _Schema.Add("NUO", "Int");  // new December 2005
                        _Schema.Add("NFO", "Int");  // new December 2005
                        _Schema.Add("NMO", "Int");  // new December 2005
                        _Schema.Add("EAFxs", "Float");  // new December 2005
                        _Schema.Add("IR", "Float");  // new December 2005
                        _Schema.Add("MB", "Float");  // new December 2005
                        _Schema.Add("RU", "Float");  // new December 2005

                        _SchemaStrLen.Add("UnitShortName", 10);
                        _SchemaStrLen.Add("Granularity", 50);

                        break;
                    }

                case "FORECASTEH":
                    {
                        // Table ForecastEH  Added May 2013
                        _Schema.Add("UnitShortName", "Char"); // (10)
                        _Schema.Add("TL_DateTime", "DateTime");
                        _Schema.Add("FOH", "Float");
                        _Schema.Add("SOH", "Float");
                        _Schema.Add("EFDH", "Float");
                        _Schema.Add("ESDH", "Float");
                        _Schema.Add("RS", "Float");
                        _Schema.Add("EUFDH_RS", "Float");
                        _Schema.Add("SH", "Float");
                        _Schema.Add("PH", "Float");
                        _Schema.Add("AttemptedStarts", "Int");
                        _Schema.Add("ActualStarts", "Int");
                        _Schema.Add("NFO", "Int");
                        _Schema.Add("MaxCap", "Decimal");

                        _SchemaStrLen.Add("UnitShortName", 10);

                        break;
                    }
                case "PEAKPERIODS":
                    {
                        _Schema.Add("SetName", "VarChar"); // (50)
                        _Schema.Add("StartingDateTime", "DateTime");
                        _Schema.Add("EndingDateTime", "DateTime");
                        _Schema.Add("Discard", "Bit");

                        _SchemaStrLen.Add("SetName", 50);

                        break;
                    }

                case "EFORD":
                    {
                        // Table EFORd
                        _Schema.Add("UnitShortName", "Char"); // (10)
                        _Schema.Add("TL_DateTime", "DateTime");
                        _Schema.Add("Granularity", "VarChar"); // (50)

                        _Schema.Add("PO", "Float");
                        _Schema.Add("PO_SE", "Float");
                        _Schema.Add("MO", "Float");
                        _Schema.Add("MO_SE", "Float");
                        _Schema.Add("SF", "Float");
                        _Schema.Add("U1", "Float");
                        _Schema.Add("U2", "Float");
                        _Schema.Add("U3", "Float");
                        _Schema.Add("D1", "Float");
                        _Schema.Add("D2", "Float");
                        _Schema.Add("D3", "Float");
                        _Schema.Add("D4", "Float");
                        _Schema.Add("D4_DE", "Float");
                        _Schema.Add("PD", "Float");
                        _Schema.Add("PD_DE", "Float");
                        _Schema.Add("RS", "Float");
                        _Schema.Add("EUFDH_RS", "Float");
                        _Schema.Add("SH", "Float");
                        _Schema.Add("PH", "Float");
                        _Schema.Add("ESEDH", "Float");
                        _Schema.Add("AH", "Float");

                        _Schema.Add("E_PO", "Float");
                        _Schema.Add("E_PO_SE", "Float");
                        _Schema.Add("E_MO", "Float");
                        _Schema.Add("E_MO_SE", "Float");
                        _Schema.Add("E_SF", "Float");
                        _Schema.Add("E_U1", "Float");
                        _Schema.Add("E_U2", "Float");
                        _Schema.Add("E_U3", "Float");
                        _Schema.Add("E_D1", "Float");
                        _Schema.Add("E_D2", "Float");
                        _Schema.Add("E_D3", "Float");
                        _Schema.Add("E_D4", "Float");
                        _Schema.Add("E_D4_DE", "Float");
                        _Schema.Add("E_PD", "Float");
                        _Schema.Add("E_PD_DE", "Float");
                        _Schema.Add("E_RS", "Float");
                        _Schema.Add("E_EUFDH_RS", "Float");
                        _Schema.Add("E_SH", "Float");
                        _Schema.Add("E_PH", "Float");
                        _Schema.Add("E_ESEDH", "Float");
                        _Schema.Add("E_AH", "Float");

                        _Schema.Add("FOCount", "Int");
                        _Schema.Add("RSCount", "Int");
                        _Schema.Add("ActualStartsCount", "Int");
                        _Schema.Add("AttemptedStartsCount", "Int");
                        _Schema.Add("AttemptedStarts", "Int");
                        _Schema.Add("ActualStarts", "Int");

                        _Schema.Add("StartingReliability", "Float");

                        _Schema.Add("GrossMaxCap", "Decimal"); // Changed December 2005
                        _Schema.Add("NetMaxCap", "Decimal"); // Changed December 2005

                        _Schema.Add("PumpingHours", "Float");
                        _Schema.Add("SynchCondHours", "Float");
                        _Schema.Add("E_PumpingHours", "Float");
                        _Schema.Add("E_SynchCondHours", "Float");

                        _Schema.Add("GMC_Weight", "Int");
                        _Schema.Add("NMC_Weight", "Int");
                        _Schema.Add("ServiceHourMethod", "Int");

                        _Schema.Add("DEFOR", "Float");
                        _Schema.Add("DFOR", "Float");
                        _Schema.Add("FP", "Float");
                        _Schema.Add("FF_ID", "Float");
                        _Schema.Add("FF_IT", "Float");
                        _Schema.Add("FF_IR", "Float");
                        _Schema.Add("FF", "Float");
                        _Schema.Add("FF_D", "Float");
                        _Schema.Add("FF_T", "Float");
                        _Schema.Add("FF_R", "Float");
                        _Schema.Add("FL_Numerator", "Float");
                        _Schema.Add("FL_Denominator", "Float");
                        _Schema.Add("FL_FORdNumerator", "Float");

                        _Schema.Add("FOCount_OMC", "Int");
                        _Schema.Add("FOH_OMC", "Float");
                        _Schema.Add("AH_OMC", "Float");
                        _Schema.Add("EFDH_OMC", "Float");
                        _Schema.Add("E_FOH_OMC", "Float");
                        _Schema.Add("E_AH_OMC", "Float");
                        _Schema.Add("E_EFDH_OMC", "Float");

                        _SchemaStrLen.Add("UnitShortName", 10);
                        _SchemaStrLen.Add("Granularity", 50);

                        break;
                    }

                case "EFORDTOTAL":
                    {
                        // Table EFORdTotal
                        _Schema.Add("UnitShortName", "Char"); // (10)
                        _Schema.Add("UnitName", "VarChar"); // (50)
                        _Schema.Add("UtilityUnitCode", "Char"); // ( 6)

                        _Schema.Add("PeriodStart", "DateTime");
                        _Schema.Add("PeriodEnd", "DateTime");

                        _Schema.Add("SF", "Float");
                        _Schema.Add("U1", "Float");
                        _Schema.Add("U2", "Float");
                        _Schema.Add("U3", "Float");
                        _Schema.Add("D1", "Float");
                        _Schema.Add("D2", "Float");
                        _Schema.Add("D3", "Float");
                        _Schema.Add("RS", "Float");
                        _Schema.Add("EUFDH_RS", "Float");
                        _Schema.Add("SH", "Float");
                        _Schema.Add("AH", "Float");

                        _Schema.Add("FOCount", "Int");
                        _Schema.Add("RSCount", "Int");
                        _Schema.Add("ActualStartsCount", "Int");
                        _Schema.Add("AttemptedStartsCount", "Int");
                        _Schema.Add("AttemptedStarts", "Int");
                        _Schema.Add("ActualStarts", "Int");

                        _Schema.Add("ServiceHourMethod", "Int");

                        _Schema.Add("DEFOR", "Float");
                        _Schema.Add("DFOR", "Float");
                        _Schema.Add("FL_Numerator", "Float");
                        _Schema.Add("FL_Denominator", "Float");
                        _Schema.Add("FL_FORdNumerator", "Float");

                        _SchemaStrLen.Add("UnitShortName", 10);
                        _SchemaStrLen.Add("UnitName", 50);
                        _SchemaStrLen.Add("UtilityUnitCode", 6);

                        break;
                    }

                case "AUDITPERFORMANCEDATA":  // new December 2005
                    {
                        // Table PerformanceData
                        _Schema.Add("UtilityUnitCode", "Char"); // (6)
                        _Schema.Add("UnitShortName", "Char"); // (10)
                        _Schema.Add("Year", "SmallInt");
                        _Schema.Add("Period", "Char"); // (2)
                        _Schema.Add("RevisionCard1", "Char"); // (1)					
                        _Schema.Add("RevisionCard2", "Char"); // (1)
                        _Schema.Add("RevisionCard3", "Char"); // (1)					
                        _Schema.Add("RevisionCard4", "Char"); // (1)					
                        _Schema.Add("RevMonthCard1", "DateTime");
                        _Schema.Add("RevMonthCard2", "DateTime");
                        _Schema.Add("RevMonthCard3", "DateTime");
                        _Schema.Add("RevMonthCard4", "DateTime");
                        _Schema.Add("LoginID", "VarChar"); // (100)
                        _Schema.Add("TimeStamp", "DateTime");

                        _SchemaStrLen.Add("UtilityUnitCode", 6);
                        _SchemaStrLen.Add("UnitShortName", 10);
                        _SchemaStrLen.Add("Period", 2);
                        _SchemaStrLen.Add("RevisionCard1", 1);
                        _SchemaStrLen.Add("RevisionCard2", 1);
                        _SchemaStrLen.Add("RevisionCard3", 1);
                        _SchemaStrLen.Add("RevisionCard4", 1);
                        _SchemaStrLen.Add("LoginID", 100);

                        break;
                    }

                case "AUDITEVENTDATA01":  // new December 2005
                    {
                        // TABLE EventData01
                        _Schema.Add("UtilityUnitCode", "Char"); // ( 6)
                        _Schema.Add("UnitShortName", "Char"); // (10)
                        _Schema.Add("Year", "SmallInt");
                        _Schema.Add("EventNumber", "SmallInt");
                        _Schema.Add("RevisionCard01", "Char"); // ( 1)
                        _Schema.Add("RevisionCard02", "Char"); // ( 1)
                        _Schema.Add("RevisionCard03", "Char"); // ( 1)
                        _Schema.Add("RevMonthCard01", "DateTime");
                        _Schema.Add("RevMonthCard02", "DateTime");
                        _Schema.Add("RevMonthCard03", "DateTime");
                        _Schema.Add("LoginID", "VarChar"); // (100)
                        _Schema.Add("TimeStamp", "DateTime");

                        _SchemaStrLen.Add("UtilityUnitCode", 6);
                        _SchemaStrLen.Add("UnitShortName", 10);
                        _SchemaStrLen.Add("RevisionCard01", 1);
                        _SchemaStrLen.Add("RevisionCard02", 1);
                        _SchemaStrLen.Add("RevisionCard03", 1);
                        _SchemaStrLen.Add("LoginID", 100);

                        break;
                    }

                case "AUDITEVENTDATA02":  // new December 2005
                    {
                        // Table EventData02
                        _Schema.Add("UtilityUnitCode", "Char"); // (6)
                        _Schema.Add("UnitShortName", "Char"); // (10)
                        _Schema.Add("Year", "SmallInt");
                        _Schema.Add("EventNumber", "SmallInt");
                        _Schema.Add("RevisionCardEven", "Char"); // (1)
                        _Schema.Add("EvenCardNumber", "TinyInt");
                        _Schema.Add("RevisionCardOdd", "Char"); // (1)
                        _Schema.Add("RevMonthCardEven", "DateTime");
                        _Schema.Add("RevMonthCardOdd", "DateTime");
                        _Schema.Add("LoginID", "VarChar"); // (100)
                        _Schema.Add("TimeStamp", "DateTime");

                        _SchemaStrLen.Add("UtilityUnitCode", 6);
                        _SchemaStrLen.Add("UnitShortName", 10);
                        _SchemaStrLen.Add("RevisionCardEven", 1);
                        _SchemaStrLen.Add("RevisionCardOdd", 1);
                        _SchemaStrLen.Add("LoginID", 100);

                        break;
                    }

                case "AVAILFACTORCALCTYPE":  // new February 2007 for ISO-NE
                    {
                        // ISO NE Only
                        _Schema.Add("UtilityUnitCode", "Char"); // (6)
                        _Schema.Add("UnitName", "VarChar"); // (50)
                        _Schema.Add("UnitShortName", "Char"); // (10)
                        _Schema.Add("AFCType", "VarChar"); // (50)
                        _Schema.Add("EffStartDate", "DateTime");
                        _Schema.Add("EffEndDate", "DateTime");
                        _Schema.Add("UserID", "VarChar"); // (100)
                        _Schema.Add("TimeStamp", "DateTime");

                        _SchemaStrLen.Add("UtilityUnitCode", 6);
                        _SchemaStrLen.Add("UnitName", 50);
                        _SchemaStrLen.Add("UnitShortName", 10);
                        _SchemaStrLen.Add("AFCType", 50);
                        _SchemaStrLen.Add("UserID", 100);

                        break;
                    }

                case "AVAILFACTORCALCTYPEARCHIVE":  // new February 2007 for ISO-NE
                    {
                        // ISO NE Only
                        _Schema.Add("UtilityUnitCode", "Char"); // (6)
                        _Schema.Add("UnitName", "VarChar"); // (50)
                        _Schema.Add("UnitShortName", "Char"); // (10)
                        _Schema.Add("AFCType", "VarChar"); // (50)
                        _Schema.Add("EffStartDate", "DateTime");
                        _Schema.Add("EffEndDate", "DateTime");
                        _Schema.Add("UserID", "VarChar"); // (100)
                        _Schema.Add("TimeStamp", "DateTime");
                        _Schema.Add("ArchiveUserID", "VarChar"); // (100)
                        _Schema.Add("ArchiveTimeStamp", "DateTime");
                        _Schema.Add("ChangeType", "VarChar"); // (50)

                        _SchemaStrLen.Add("UtilityUnitCode", 6);
                        _SchemaStrLen.Add("UnitName", 50);
                        _SchemaStrLen.Add("UnitShortName", 10);
                        _SchemaStrLen.Add("AFCType", 50);
                        _SchemaStrLen.Add("UserID", 100);
                        _SchemaStrLen.Add("ArchiveUserID", 100);
                        _SchemaStrLen.Add("ChangeType", 50);

                        break;
                    }

                case "FCMWEIGHTINGFACTOR":  // new February 2007 for ISO-NE
                    {
                        // ISO NE Only
                        _Schema.Add("WtgFactorDate", "DateTime");
                        _Schema.Add("WtgFactorHourHH", "Char"); // (3) will be something like 01, 02X for DST extra hour
                        _Schema.Add("WtgFactor", "TinyInt");
                        _Schema.Add("TimeStamp", "DateTime");
                        _Schema.Add("UserID", "VarChar"); // (100)

                        _SchemaStrLen.Add("WtgFactorHourHH", 3);
                        _SchemaStrLen.Add("UserID", 100);

                        break;
                    }

                case "FCMWEIGHTINGFACTORARCHIVE":  // new February 2007 for ISO-NE
                    {
                        // ISO NE Only
                        _Schema.Add("WtgFactorDate", "DateTime");
                        _Schema.Add("WtgFactorHourHH", "Char"); // (3) will be something like 01, 02X for DST extra hour
                        _Schema.Add("WtgFactor", "TinyInt");
                        _Schema.Add("TimeStamp", "DateTime");
                        _Schema.Add("UserID", "VarChar"); // (100)
                        _Schema.Add("ArchiveTimeStamp", "DateTime");
                        _Schema.Add("ArchiveUserID", "VarChar"); // (100)

                        _SchemaStrLen.Add("WtgFactorHourHH", 3);
                        _SchemaStrLen.Add("UserID", 100);
                        _SchemaStrLen.Add("ArchiveUserID", 100);

                        break;
                    }

                case "DELISTSTATUS":
                    {
                        // ISO NE Only
                        _Schema.Add("UtilityUnitCode", "Char"); // (6)
                        _Schema.Add("UnitName", "VarChar"); // (50)
                        _Schema.Add("UnitShortName", "Char"); // (10)
                        _Schema.Add("DelistMonth", "Char"); // (7)
                        _Schema.Add("DelistStatus", "Char"); // (10) 
                        _Schema.Add("SCC", "Decimal");
                        _Schema.Add("DelistMW", "Decimal");
                        _Schema.Add("UserID", "VarChar"); // (100) 
                        _Schema.Add("TimeStamp", "DateTime");

                        _SchemaStrLen.Add("UtilityUnitCode", 6);
                        _SchemaStrLen.Add("UnitName", 50);
                        _SchemaStrLen.Add("UnitShortName", 10);
                        _SchemaStrLen.Add("DelistMonth", 7);
                        _SchemaStrLen.Add("DelistStatus", 10);
                        _SchemaStrLen.Add("UserID", 100);

                        break;
                    }

                case "DELISTSTATUSARCHIVE":
                    {
                        // ISO NE Only
                        _Schema.Add("UtilityUnitCode", "Char"); // (6)
                        _Schema.Add("UnitName", "VarChar"); // (50)
                        _Schema.Add("UnitShortName", "Char"); // (10)
                        _Schema.Add("DelistMonth", "Char"); // (7)
                        _Schema.Add("DelistStatus", "Char"); // (10) 
                        _Schema.Add("SCC", "Decimal");
                        _Schema.Add("DelistMW", "Decimal");
                        _Schema.Add("UserID", "VarChar"); // (100) 
                        _Schema.Add("TimeStamp", "DateTime");
                        _Schema.Add("ArchiveUserID", "VarChar"); // (100)
                        _Schema.Add("ArchiveTimeStamp", "DateTime");

                        _SchemaStrLen.Add("UtilityUnitCode", 6);
                        _SchemaStrLen.Add("UnitName", 50);
                        _SchemaStrLen.Add("UnitShortName", 10);
                        _SchemaStrLen.Add("DelistMonth", 7);
                        _SchemaStrLen.Add("DelistStatus", 10);
                        _SchemaStrLen.Add("UserID", 100);
                        _SchemaStrLen.Add("ArchiveUserID", 100);

                        break;
                    }

                case "SHMETHOD":  // new February 2007 for ISO-NE
                    {
                        // ISO NE Only
                        _Schema.Add("UtilityUnitCode", "Char"); // (6)
                        _Schema.Add("UnitName", "VarChar"); // (50)
                        _Schema.Add("UnitShortName", "Char"); // (10)
                        _Schema.Add("ServiceHourMethod", "TinyInt");
                        _Schema.Add("EffStartDate", "DateTime");
                        _Schema.Add("EffEndDate", "DateTime");
                        _Schema.Add("UserID", "VarChar"); // (100)
                        _Schema.Add("TimeStamp", "DateTime");
                        _Schema.Add("ServiceHourMethodText", "VarChar");

                        _SchemaStrLen.Add("UtilityUnitCode", 6);
                        _SchemaStrLen.Add("UnitName", 50);
                        _SchemaStrLen.Add("UnitShortName", 10);
                        _SchemaStrLen.Add("UserID", 100);
                        _SchemaStrLen.Add("ServiceHourMethodText", 100);

                        break;
                    }

                case "SHMETHODARCHIVE":  // new February 2007 for ISO-NE
                    {
                        // ISO NE Only
                        _Schema.Add("UtilityUnitCode", "Char"); // (6)
                        _Schema.Add("UnitName", "VarChar"); // (50)
                        _Schema.Add("UnitShortName", "Char"); // (10)
                        _Schema.Add("ServiceHourMethod", "TinyInt");
                        _Schema.Add("EffStartDate", "DateTime");
                        _Schema.Add("EffEndDate", "DateTime");
                        _Schema.Add("UserID", "VarChar"); // (100)
                        _Schema.Add("TimeStamp", "DateTime");
                        _Schema.Add("ArchiveUserID", "VarChar"); // (100)
                        _Schema.Add("ArchiveTimeStamp", "DateTime");
                        _Schema.Add("ChangeType", "VarChar"); // (50)
                        _Schema.Add("ServiceHourMethodText", "VarChar");

                        _SchemaStrLen.Add("UtilityUnitCode", 6);
                        _SchemaStrLen.Add("UnitName", 50);
                        _SchemaStrLen.Add("UnitShortName", 10);
                        _SchemaStrLen.Add("UserID", 100);
                        _SchemaStrLen.Add("ArchiveUserID", 100);
                        _SchemaStrLen.Add("ChangeType", 50);
                        _SchemaStrLen.Add("ServiceHourMethodText", 100);

                        break;
                    }

                case "FCMMONTHLY":  // new February 2007 for ISO-NE
                    {
                        // ISO NE Only
                        _Schema.Add("UnitShortName", "Char");
                        _Schema.Add("FYearMonth", "Char");
                        _Schema.Add("DelistStatus", "Char");
                        _Schema.Add("SCC", "Float");
                        _Schema.Add("DelistMW", "Float");
                        _Schema.Add("DelistedMW", "Float");
                        _Schema.Add("SubPeriod", "TinyInt");
                        _Schema.Add("MaxDateInSubPeriod", "Bit");
                        _Schema.Add("WFOH", "Float");
                        _Schema.Add("AH", "Float");
                        _Schema.Add("WSH", "Float");
                        _Schema.Add("WEFOH", "Float");
                        _Schema.Add("FOH", "Float");
                        _Schema.Add("FOCount", "Int");
                        _Schema.Add("RSH", "Float");
                        _Schema.Add("AttemptedStarts", "Int");
                        _Schema.Add("ActualStarts", "Int");
                        _Schema.Add("SH", "Float");
                        _Schema.Add("EFOH", "Float");
                        _Schema.Add("Period", "VarChar");
                        _Schema.Add("RunTimeStamp", "Char");
                        _Schema.Add("RunDesc", "VarChar");
                        _Schema.Add("Approved", "Bit");

                        _SchemaStrLen.Add("UnitShortName", 10);
                        _SchemaStrLen.Add("FYearMonth", 6);
                        _SchemaStrLen.Add("DelistStatus", 10);
                        _SchemaStrLen.Add("Period", 50);
                        _SchemaStrLen.Add("RunTimeStamp", 14);
                        _SchemaStrLen.Add("RunDesc", 50);

                        break;
                    }

                case "GADS_STARTS": //
                    {
                        // TVA Only
                        _Schema.Add("UtilityUnitCode", "Char");
                        _Schema.Add("Year", "SmallInt");
                        _Schema.Add("Period", "SmallInt");
                        _Schema.Add("Hot", "SmallInt");
                        _Schema.Add("Warm", "SmallInt");
                        _Schema.Add("Cold", "SmallInt");

                        _SchemaStrLen.Add("UtilityUnitCode", 6);

                        break;

                    }

                case "FCMSUBPERIOD":  // new February 2007 for ISO-NE
                    {
                        // ISO NE Only
                        _Schema.Add("UnitShortName", "Char");
                        _Schema.Add("Period", "VarChar");
                        _Schema.Add("SubPeriod", "TinyInt");
                        _Schema.Add("WFOH", "Float");
                        _Schema.Add("WSH", "Float");
                        _Schema.Add("DelistedMW", "Float");
                        _Schema.Add("SCC", "Float");
                        _Schema.Add("WEFOH", "Float");
                        _Schema.Add("FOH", "Float");
                        _Schema.Add("FOCount", "Int");
                        _Schema.Add("r", "Float");
                        _Schema.Add("Invr", "Float");
                        _Schema.Add("RSH", "Float");
                        _Schema.Add("AttemptedStarts", "Int");
                        _Schema.Add("T", "Float");
                        _Schema.Add("InvT", "Float");
                        _Schema.Add("SH", "Float");
                        _Schema.Add("ActualStarts", "Int");
                        _Schema.Add("D", "Float");
                        _Schema.Add("InvD", "Float");
                        _Schema.Add("fFactor", "Float");
                        _Schema.Add("AH", "Float");
                        _Schema.Add("fpFactor", "Float");
                        _Schema.Add("WFOHd", "Float");
                        _Schema.Add("FCMEFORdNumerator", "Float");
                        _Schema.Add("FCMEFORdDenominator", "Float");
                        _Schema.Add("NonDelistedFCMEFORd", "Float");
                        _Schema.Add("FOHd", "Float");
                        _Schema.Add("EFOH", "Float");
                        _Schema.Add("StdEFORdNumerator", "Float");
                        _Schema.Add("StdEFORdDenominator", "Float");
                        _Schema.Add("StdEFORd", "Float");
                        _Schema.Add("FCMEFORd", "Float");
                        _Schema.Add("SubPeriodLength", "Int");
                        _Schema.Add("LengthAvgEFORd", "Float");
                        _Schema.Add("RunTimeStamp", "Char");
                        _Schema.Add("RunDesc", "VarChar");
                        _Schema.Add("Approved", "Bit");

                        _SchemaStrLen.Add("UnitShortName", 10);
                        _SchemaStrLen.Add("Period", 50);
                        _SchemaStrLen.Add("RunTimeStamp", 14);
                        _SchemaStrLen.Add("RunDesc", 50);

                        break;
                    }

                case "FCMPERIOD":  // new February 2007 for ISO-NE
                    {
                        _Schema.Add("UnitShortName", "Char");
                        _Schema.Add("Period", "VarChar");
                        _Schema.Add("CapPeriodLength", "Int");
                        _Schema.Add("FCMEFORd", "Float");
                        _Schema.Add("IST", "Int");
                        _Schema.Add("TWFCMEFORd", "Float");
                        _Schema.Add("TWEFORdCAvg", "Float");
                        _Schema.Add("FinalFCMEFORd", "Float");
                        _Schema.Add("RunTimeStamp", "Char");
                        _Schema.Add("RunDesc", "VarChar");
                        _Schema.Add("Approved", "Bit");

                        _SchemaStrLen.Add("UnitShortName", 10);
                        _SchemaStrLen.Add("Period", 50);
                        _SchemaStrLen.Add("RunTimeStamp", 14);
                        _SchemaStrLen.Add("RunDesc", 50);

                        break;
                    }

                case "FCMPRIORAPPROVEDRUNS":  // new February 2008 for ISO-NE
                    {
                        _Schema.Add("Period", "VarChar");
                        _Schema.Add("RunTimeStamp", "Char");
                        _Schema.Add("RunDesc", "VarChar");

                        _SchemaStrLen.Add("Period", 50);
                        _SchemaStrLen.Add("RunTimeStamp", 14);
                        _SchemaStrLen.Add("RunDesc", 50);

                        break;
                    }

                case "FCMEFORD":  // new February 2007 for ISO-NE
                    {
                        // ISO NE Only
                        _Schema.Add("UnitShortName", "Char");
                        _SchemaStrLen.Add("UnitShortName", 10);

                        break;
                    }

                case "FCMERRORS":  // new February 2007 for ISO-NE
                    {
                        // ISO NE Only
                        _Schema.Add("UnitShortName", "Char");
                        _SchemaStrLen.Add("UnitShortName", 10);

                        break;
                    }

                case "FCMEVENTHOURS":  // new February 2007 for ISO-NE
                    {
                        // ISO NE Only
                        _Schema.Add("UnitShortName", "Char");
                        _SchemaStrLen.Add("UnitShortName", 10);

                        break;
                    }

                case "FCMEVENTHOURSTEMP":  // new February 2007 for ISO-NE
                    {
                        // ISO NE Only
                        _Schema.Add("UnitShortName", "Char");
                        _SchemaStrLen.Add("UnitShortName", 10);

                        break;
                    }

                case "FCMFFACTOR":  // new February 2007 for ISO-NE
                    {
                        // ISO NE Only
                        _Schema.Add("UnitShortName", "Char");
                        _SchemaStrLen.Add("UnitShortName", 10);

                        break;
                    }

                case "TIMEMAST":  // new February 2007 for ISO-NE
                    {
                        // ISO NE Only
                        _Schema.Add("sXML", "NText");
                        _Schema.Add("UtilityUnitCode", "Char"); // ( 6)

                        _SchemaStrLen.Add("UtilityUnitCode", 6);

                        break;
                    }

                case "TIMEMASTARCHIVE":  // new February 2007 for ISO-NE
                    {
                        // ISO NE Only
                        _Schema.Add("sXML", "NText");
                        _Schema.Add("UtilityUnitCode", "Char"); // ( 6)

                        _SchemaStrLen.Add("UtilityUnitCode", 6);

                        break;
                    }

                case "FCMFINALEFORDRPT":  // new February 2007 for ISO-NE
                    {
                        // ObligMonth, UnitShortName, UnitName, AFCType, ClassAvgEFORd, IST, FinalEFORd, RunTimeStamp

                        _Schema.Add("ObligMonth", "Char");
                        _Schema.Add("UnitName", "VarChar"); // (50)
                        _Schema.Add("UnitShortName", "Char");
                        _Schema.Add("AFCType", "VarChar"); // (50)
                        _Schema.Add("ClassAvgEFORd", "Float");
                        _Schema.Add("IST", "Int");
                        _Schema.Add("FinalEFORd", "Float");
                        _Schema.Add("FinalEFORdWCAvg", "Float");
                        _Schema.Add("RunTimeStamp", "Char");

                        _SchemaStrLen.Add("ObligMonth", 7);
                        _SchemaStrLen.Add("UnitName", 50);
                        _SchemaStrLen.Add("UnitShortName", 10);
                        _SchemaStrLen.Add("AFCType", 50);
                        _SchemaStrLen.Add("RunTimeStamp", 14);

                        break;
                    }

                case "CFECAUSECODETOPMSYS":  // new December 2007 for CFE
                    {
                        _Schema.Add("CauseCode", "SmallInt");
                        _Schema.Add("PMSYS", "SmallInt");
                        _Schema.Add("PMSYSDescription", "VarChar"); // (150)

                        _SchemaStrLen.Add("PMSYSDescription", 150);

                        break;
                    }

                case "CFEEQUIPOSR3":  // new December 2007 for CFE
                    {
                        _Schema.Add("CCCC", "Int");
                        _Schema.Add("U", "SmallInt");
                        _Schema.Add("SS", "SmallInt");
                        _Schema.Add("TTTT", "Int");
                        _Schema.Add("NNNN", "Int");
                        _Schema.Add("EquipmentDesc", "VarChar"); // (75)

                        _SchemaStrLen.Add("EquipmentDesc", 75);

                        break;
                    }

                case "CFEUNITXREF":  // new December 2007 for CFE
                    {
                        _Schema.Add("UtilityUnitCode", "Char"); // ( 6)
                        _Schema.Add("UnitName", "VarChar"); // (50)
                        _Schema.Add("UnitShortName", "Char"); // (10)
                        _Schema.Add("CentralASARE", "Int");
                        _Schema.Add("Unit", "SmallInt");

                        _SchemaStrLen.Add("UtilityUnitCode", 6);
                        _SchemaStrLen.Add("UnitName", 50);
                        _SchemaStrLen.Add("UnitShortName", 10);

                        break;
                    }

                case "CFESAP":  // new December 2007 for CFE
                    {
                        // TABLE CFESAP
                        _Schema.Add("UtilityUnitCode", "Char"); // (  6)
                        _Schema.Add("UnitShortName", "Char"); // ( 10)
                        _Schema.Add("Year", "SmallInt");
                        _Schema.Add("EventNumber", "SmallInt");
                        _Schema.Add("EvenCardNumber", "TinyInt");
                        _Schema.Add("RevisionCardEven", "Char"); // (  1)
                        _Schema.Add("TechnicalLocation", "VarChar"); // ( 25)
                        _Schema.Add("EquipmentDescription", "VarChar"); // (150)
                        _Schema.Add("SS", "SmallInt");
                        _Schema.Add("TTTT", "Int");

                        _SchemaStrLen.Add("UtilityUnitCode", 6);
                        _SchemaStrLen.Add("UnitShortName", 10);
                        _SchemaStrLen.Add("RevisionCardEven", 1);
                        _SchemaStrLen.Add("TechnicalLocation", 25);
                        _SchemaStrLen.Add("EquipmentDescription", 150);

                        break;
                    }

                case "CUSTOMEVENTDATA":
                    {
                        // Table CustomEventData
                        _Schema.Add("UtilityUnitCode", "Char"); // (6)
                        _Schema.Add("UnitShortName", "Char"); // (10)
                        _Schema.Add("Year", "SmallInt");
                        _Schema.Add("EventNumber", "SmallInt");
                        _Schema.Add("CustomField1", "VarChar"); // (255)
                        _Schema.Add("CustomField2", "VarChar"); // (255)
                        _Schema.Add("CustomListbox", "VarChar"); // (255)
                        _Schema.Add("CardNumber", "TinyInt");

                        _SchemaStrLen.Add("UtilityUnitCode", 6);
                        _SchemaStrLen.Add("UnitShortName", 10);
                        _SchemaStrLen.Add("CustomField1", 255);
                        _SchemaStrLen.Add("CustomField2", 255);
                        _SchemaStrLen.Add("CustomListbox", 255);

                        break;
                    }

                case "CLIENTCR":
                    {
                        // Table ClientCR

                        _Schema.Add("ReportFileName", "VarChar"); // (255)
                        _Schema.Add("Description", "VarChar"); // (255)

                        _SchemaStrLen.Add("ReportFileName", 256);
                        _SchemaStrLen.Add("Description", 100);

                        break;
                    }

                case "CUSTOMPERFDATA":
                    {
                        // Table CustomPerfData
                        _Schema.Add("UtilityUnitCode", "Char"); // (6)
                        _Schema.Add("UnitShortName", "Char"); // (10)
                        _Schema.Add("Year", "SmallInt");
                        _Schema.Add("Period", "Char"); // (2)
                        _Schema.Add("PerfCalcDate", "DateTime");
                        _Schema.Add("Field01", "Decimal");
                        _Schema.Add("Field02", "Decimal");
                        _Schema.Add("Field03", "Decimal");
                        _Schema.Add("Field04", "Decimal");
                        _Schema.Add("Field05", "Decimal");
                        _Schema.Add("Field06", "Decimal");
                        _Schema.Add("Field07", "Decimal");
                        _Schema.Add("Field08", "Decimal");
                        _Schema.Add("Field09", "Decimal");
                        _Schema.Add("Field10", "Decimal");

                        _SchemaStrLen.Add("UtilityUnitCode", 6);
                        _SchemaStrLen.Add("UnitShortName", 10);
                        _SchemaStrLen.Add("Period", 2);

                        break;
                    }

                case "NYISOIIFO":
                    {
                        // Table NYISOIIFO
                        _Schema.Add("UnitShortName", "Char"); // (10)
                        _Schema.Add("UtilityUnitCode", "Char"); // ( 6)
                        _Schema.Add("PeriodStart", "DateTime");
                        _Schema.Add("PeriodEnd", "DateTime");
                        _Schema.Add("IIFOH", "Float");

                        _SchemaStrLen.Add("UnitShortName", 10);
                        _SchemaStrLen.Add("UtilityUnitCode", 6);

                        break;
                    }

                default:
                    {
                        break;
                    }
            }
            s.Parms = new ArrayList();
            s.CommandType = CommandType.Text;

            s.SQL = "Sql";
            s.Name = "name";
            if (ParamList != null)
            {
                for (int i = 0; i <= ParamList.GetUpperBound(0); i++)
                {   // THE ABOVE "< ParamList.GetUpperBound(0)" REQUIRES THAT THE ARRAY BE 1 BIGGER THAN REQUIRED OR
                    // CHANGE TO "<= ParamList.GetUpperBound(0)" !!!!
                    // Console.WriteLine("\t{0}:\t{1}", myEnumerator.Key, myEnumerator.Value);

                    Parm p = new Parm()
                    {
                        Name = "name"
                    };
                    if (ParamList.GetValue(i, 0) == null)
                    {
                        break;
                    }
                    p.SQLName = ParamList.GetValue(i, 0).ToString();

                    p.Name = ParamList.GetValue(i, 1).ToString().Substring(0, ParamList.GetValue(i, 1).ToString().IndexOf(','));
                    p.stringValue = ParamList.GetValue(i, 1).ToString().Substring(ParamList.GetValue(i, 1).ToString().IndexOf(',') + 2);
                    //				p.SQLName = myEnumerator.Key.ToString();
                    //				p.Name = myEnumerator.Value.ToString().Substring(0,myEnumerator.Value.ToString().IndexOf(','));
                    //				p.stringValue = myEnumerator.Value.ToString().Substring(myEnumerator.Value.ToString().IndexOf(',') + 2);
                    //				
                    if (_Schema.ContainsKey(p.Name))
                    {
                        p.Type = _Schema[p.Name].ToString();
                    }
                    else
                    {
                        _throwException("Invalid p.Name [" + p.Name + "] in statement " + p.SQLName, null);
                    }

                    if (_SchemaStrLen.ContainsKey(p.Name))
                    {
                        temp = _SchemaStrLen[p.Name].ToString();
                    }
                    else
                    {
                        temp = null;
                    }

                    if (!(temp == null))
                    {
                        p.maxLength = Convert.ToInt32(temp);
                    }

                    temp = null;

                    temp = p.SQLName;
                    if (!(temp == null))
                    {
                        p.SourceColumn = p.Name;
                    }
                    p.Direction = ParameterDirection.Input;
                    s.Parms.Add(p);

                }
            }
            //if (this.Provider == "Oracle")
            //{
            //    _OracleSchema = new string[_Schema.Count];
            //    _Schema.Keys.CopyTo(_OracleSchema, 0);
            //    IComparer myComparer = new myReverserClass();
            //    Array.Sort(_OracleSchema, myComparer);
            //}

            _Schema.Clear();
            _SchemaStrLen.Clear();

            return s;
        }

        /// <summary>
        ///	This creates the 
        /// </summary>
        /// <param name="statement"></param>
        /// <param name="parms"></param>
        /// <param name="setParmValue"></param>
        /// <returns></returns>

        private IDbCommand _getStatement(string statement, Array parms, bool setParmValue)
        {
            //
            Statement s;
            string tableName;
            string strTemp;

            //			// Test

            //			This method has a fatal flaw in that it takes a valid string like "@CauseCode" and converts it
            //			correctly to "?", but it also takes "@CauseCodeDesc" and converts it to "?Desc" since the first
            //			part was replaced earlier.

            //			string l_parameter;
            //			string l_result;
            //			string a_sqlStatement;
            //			a_sqlStatement = statement;
            //			l_result = a_sqlStatement;
            //			// first find all parameters
            //			Regex System.Text.RegularExpressions.Regex = new Regex(@"[@]\w+(?=\s|$|[,]|[)])");
            //			MatchCollection l_matches = System.Text.RegularExpressions.Regex.Matches(a_sqlStatement);
            //
            //			foreach (Match l_match in l_matches)
            //			{
            //				l_parameter = l_match.ToString();
            //				// make additional checks of the parameter
            //				// check for things such as @@IDENTITY
            //				if (l_result.IndexOf("@" + l_parameter) == -1)
            //				{
            //					l_result = l_result.Replace(l_parameter, "?");
            //				}
            //			}

            //			// Test

            IDbCommand com;
            IDbDataParameter newParm;

            tableName = GADSNGTableName;

            s = _getStatementFromFile(tableName, parms);


            // Build the command, add the parameters
            com = (IDbCommand)Activator.CreateInstance(_comType, false);

            strTemp = statement;

            if (Provider == "Oracle")
            {

                /* Parameter Binding by Name
                 * Both Microsoft OracleClient and ODP.NET use the colon to bind parameters and bind parameters by name. 
                 * While ODP.NET supports binding by name, it binds by position by default. To match Microsoft OracleClient's 
                 * parameter name binding behavior, add the following line to your code after each OracleCommand instantiation that you bind parameters to:

                 * OracleCommand.BindByName = true; 
                 * 
                 * This isn't made entirely clear on the Oracle documentation, but there is an additional property, BindByName, on the OracleCommand object, which must be set to true in order to bind parameters by name:
                 * 
                 *OracleCommand command = new OracleCommand(query, connection) 
                 *   { CommandType = CommandType.Text, BindByName = true }; 
                 * 
                 */

                //TODO uncomment the line below if using Oracle.DataAccess.Client
                com.GetType().GetProperty("BindByName").SetValue(com, true, null);

                strTemp = statement.Replace("@", ":");

                if (strTemp.IndexOf("(+)") == -1)
                {   // the syntax for a LEFT OUTER JOIN includes a (+) which must be left intact

                    // Oracle's string concatenation is || so 
                    // make sure that you don't use "+" in SQL statements for numeric field additions
                    strTemp = strTemp.Replace("+", "||");

                }

                // SQL Server uses SUBSTRING() while Oracle uses SUBSTR()1
                strTemp = strTemp.Replace("SUBSTRING(", "SUBSTR(");

                //for (int i = 0; i < _OracleSchema.Length; i++)
                //{
                //    if (strTemp.IndexOf(_OracleSchema[i]) > 0)
                //    {
                //        // NERC is already upper
                //        strTemp = strTemp.Replace(_OracleSchema[i], "\"" + _OracleSchema[i] + "\"");
                //    }
                //    else
                //    {
                //        if (strTemp.IndexOf(_OracleSchema[i].ToUpper()) > 0)
                //        {
                //            strTemp = strTemp.Replace(_OracleSchema[i].ToUpper(), "\"" + _OracleSchema[i] + "\"");
                //        }
                //    }
                //}

                //strTemp = strTemp.Replace("\"\"PJM\"StartsCount\"","\"PJMStartsCount\"");  //	"UPDATE GADSNG.\"Setup\" SET \"\"PJM\"StartsCount\" = 0 WHERE \"\"PJM\"StartsCount\" IS NULL"
                //strTemp = strTemp.Replace("\"\"FixedByUser\"File\"", "\"FixedByUserFile\"");  // \"\"FixedByUser\"File"
                //strTemp = strTemp.Replace("\"\"PeakPeriod\"Set\"", "\"PeakPeriodSet\"");  // \"\"PeakPeriod\"Set"
            }
            else if (Provider == "OleDb" || Provider == "ODBC")
            {
                int _beginning;
                int _endBlank;
                int _endRightParen;
                int _end;

                _beginning = 1;

                while (_beginning > 0)
                {
                    _beginning = statement.IndexOf("@");

                    if (_beginning > 0)
                    {
                        _endBlank = statement.IndexOf(" ", _beginning);
                        _endRightParen = statement.IndexOf(")", _beginning);

                        if (_endRightParen == -1 && _endBlank > -1)
                        {
                            _end = _endBlank;
                        }
                        else if ((_endBlank > _endRightParen) || _endBlank == -1)
                        {
                            if (_endRightParen == -1)
                            {
                                _endRightParen = statement.Length;
                            }

                            _end = _endRightParen;
                        }
                        else
                        {
                            _end = _endBlank;
                        }

                        strTemp = statement.Remove(_beginning, (_end - _beginning));
                        statement = strTemp;
                        if (statement.ToUpper().StartsWith("INSERT"))
                        {
                            strTemp = statement.Insert(_beginning, "?,");
                        }
                        else if (statement.ToUpper().StartsWith("UPDATE"))
                        {
                            if (_beginning < statement.ToUpper().IndexOf("WHERE"))
                            {
                                strTemp = statement.Insert(_beginning, "?,");
                            }
                            else
                            {
                                strTemp = statement.Insert(_beginning, "?");
                            }
                        }
                        else
                        {
                            strTemp = statement.Insert(_beginning, "?");
                        }
                        statement = strTemp;
                    }
                }

                if (statement.ToUpper().StartsWith("INSERT") | statement.ToUpper().StartsWith("UPDATE"))
                {
                    if (statement.LastIndexOf("?,") > 0)
                    {
                        strTemp = statement.Remove(statement.LastIndexOf("?,") + 1, 1);
                        strTemp = strTemp.Replace(" Year,", " [Year],");
                        strTemp = strTemp.Replace("TimeStamp", "[TimeStamp]");
                        strTemp = strTemp.Replace(" Password ", " [Password] ");
                        strTemp = strTemp.Replace(" Password, ", " [Password], ");
                        strTemp = strTemp.Replace("(Year =", "([Year] =");
                        strTemp = strTemp.Replace(", Year =", ", [Year] =");
                        // some insert statements were ending up with (TimeStamp >= ?,) -- notice the comma after the ?
                        // which needs to be (TimeStamp >= ?)
                        strTemp = strTemp.Replace("?,)", "?)");
                        strTemp = strTemp.Replace("?, AND ", "? AND ");
                        strTemp = strTemp.Replace("?, OR ", "? OR ");
                        statement = strTemp;
                    }
                }

            }
            else
            {
                strTemp = statement;
                //				if (Provider == "SqlClient")
                //					com.CommandTimeout = 0;
            }

            /*
             * IDbCommand.CommandTimeout Property
             * 
             * Gets or sets the wait time before terminating the attempt to execute a command and 
             * generating an error.
             * 
             * The time (in seconds) to wait for the command to execute. The default value is 30 seconds.
             * 
             * 60 seconds may keep large database calls from timing out
             * 
             * Unlike the Command object in the other .NET Framework data providers (SQL Server, OLE DB, and ODBC), 
             * OracleCommand does not support a CommandTimeout property. 
             * 
             * Setting a command timeout has no effect and value returned is always zero.
            */
            com.CommandTimeout = 0;
            com.CommandText = strTemp;
            com.CommandType = CommandType.Text;

            // Now add the parameters
            foreach (Parm p in s.Parms)
            {
                object[] args = new object[2];
                if (Provider == "Oracle" | Provider == "OleDb" | Provider == "ODBC")
                {
                    args[0] = p.SQLName.Replace("@", "");

                    //					if (Provider == "OleDb")
                    //					{
                    //						string temp;
                    //						temp = p.SQLName.Replace("@","");
                    //						args[0] = temp.Replace("TimeStamp", "[TimeStamp]");
                    //					}
                }
                else
                {
                    args[0] = p.SQLName;
                }
                // Get the type
                if (_paramtypes.ContainsKey(p.Type))
                {
                    args[1] = _paramtypes[p.Type];
                }
                else
                {
                    _throwException("Invalid type [" + p.Type + "] in statement " + statement, null);
                }
                // Create the parameter object
                newParm = (IDbDataParameter)Activator.CreateInstance(_parmType, args);
                // Set its properties
                newParm.Direction = p.Direction;
                if (!(p.SourceColumn == null))
                {
                    newParm.SourceColumn = p.SourceColumn;
                }

                if (p.maxLength > 0)
                {
                    newParm.Size = p.maxLength;
                }

                com.Parameters.Add(newParm);
                // strTemp = p.SQLName.Replace("@","");
                IDataParameter d;
                if (Provider == "Oracle" | Provider == "OleDb" | Provider == "ODBC")
                {
                    d = (IDataParameter)com.Parameters[p.SQLName.Replace("@", "")];
                }
                else
                {
                    d = (IDataParameter)com.Parameters[p.SQLName];
                }

                switch (p.Type.ToUpper())
                {
                    case "SMALLINT":
                        {
                            if (p.stringValue == "")
                            {
                                d.Value = DBNull.Value;
                            }
                            else
                            {
                                d.Value = Convert.ToInt16(p.stringValue);
                            }
                            break;
                        }

                    case "BIGINT":
                        {
                            if (p.stringValue == "")
                            {
                                d.Value = DBNull.Value;
                            }
                            else
                            {
                                d.Value = Convert.ToInt64(p.stringValue);
                            }
                            break;
                        }

                    case "BIT":
                        {
                            if (Provider == "Oracle")
                            {
                                if ((p.stringValue.ToUpper() == "TRUE") | (p.stringValue.ToUpper() == "1"))
                                {
                                    d.Value = 1;
                                }
                                else
                                {
                                    d.Value = 0;
                                }
                            }
                            else if (Provider == "ODBC")
                            {
                                if ((p.stringValue.ToUpper() == "TRUE") | (p.stringValue.ToUpper() == "1") | (p.stringValue.ToUpper() == "-1"))
                                {
                                    d.Value = "True";
                                }
                                else
                                {
                                    d.Value = "False";
                                }
                            }
                            else if (Provider == "OleDb")
                            {
                                if ((p.stringValue.ToUpper() == "TRUE") | (p.stringValue.ToUpper() == "1") | (p.stringValue.ToUpper() == "-1"))
                                {
                                    d.Value = "True";
                                }
                                else
                                {
                                    d.Value = "False";
                                }
                            }
                            else
                            {
                                if (p.stringValue == "")
                                {
                                    d.Value = DBNull.Value;
                                }
                                else
                                {
                                    if (p.stringValue == "0")
                                    {
                                        d.Value = Convert.ToBoolean(0);
                                    }
                                    else if (p.stringValue == "1")
                                    {
                                        d.Value = Convert.ToBoolean(1);
                                    }
                                    else if (p.stringValue.ToUpper() == "TRUE")
                                    {
                                        d.Value = Convert.ToBoolean(p.stringValue);
                                    }
                                    else if (p.stringValue.ToUpper() == "FALSE")
                                    {
                                        d.Value = Convert.ToBoolean(p.stringValue);
                                    }
                                    else
                                    {
                                        d.Value = Convert.ToBoolean(0);
                                    }
                                }
                            }
                            break;
                        }
                    case "DATETIME":
                        {
                            if (p.stringValue == "")
                            {
                                //							d.Value = null;
                                d.Value = DBNull.Value;
                            }
                            else
                            {
                                d.Value = Convert.ToDateTime(p.stringValue);
                            }
                            break;
                        }
                    case "DECIMAL":
                        {
                            if (p.stringValue == "")
                            {
                                d.Value = DBNull.Value;
                            }
                            else
                            {
                                if (p.stringValue.IndexOf("E") > 0)
                                {
                                    d.Value = Convert.ToDecimal(Convert.ToDouble(p.stringValue));
                                }
                                else
                                {
                                    d.Value = Convert.ToDecimal(p.stringValue);
                                }
                            }
                            break;
                        }
                    case "FLOAT":
                        {
                            if (p.stringValue == "")
                            {
                                d.Value = DBNull.Value;
                            }
                            else
                            {
                                d.Value = Convert.ToDouble(p.stringValue);
                            }
                            break;
                        }
                    case "INT":
                        {
                            if (p.stringValue == "")
                            {
                                d.Value = DBNull.Value;
                            }
                            else
                            {
                                d.Value = Convert.ToInt32(p.stringValue);
                            }
                            break;
                        }
                    case "MONEY":
                        {
                            if (p.stringValue == "")
                            {
                                d.Value = DBNull.Value;
                            }
                            else
                            {
                                d.Value = Convert.ToDecimal(p.stringValue);
                            }
                            break;
                        }
                    case "NUMERIC":
                        {
                            if (p.stringValue == "")
                            {
                                d.Value = DBNull.Value;
                            }
                            else
                            {
                                d.Value = Convert.ToDecimal(p.stringValue);
                            }
                            break;
                        }
                    case "REAL":
                        {
                            if (p.stringValue == "")
                            {
                                d.Value = DBNull.Value;
                            }
                            else
                            {
                                d.Value = Convert.ToSingle(p.stringValue);
                            }
                            break;
                        }
                    case "SMALLDATETIME":
                        {
                            if (p.stringValue == "")
                            {
                                d.Value = DBNull.Value;
                            }
                            else
                            {
                                d.Value = Convert.ToDateTime(p.stringValue);
                            }
                            break;
                        }
                    case "SMALLMONEY":
                        {
                            if (p.stringValue == "")
                            {
                                d.Value = DBNull.Value;
                            }
                            else
                            {
                                d.Value = Convert.ToDecimal(p.stringValue);
                            }
                            break;
                        }
                    case "TIMESTAMP":
                        {
                            if (p.stringValue == "")
                            {
                                d.Value = DBNull.Value;
                            }
                            else
                            {
                                d.Value = p.stringValue;
                            }
                            break;
                        }
                    case "TINYINT":
                        {
                            if (p.stringValue == "")
                            {
                                d.Value = DBNull.Value;
                            }
                            else
                            {
                                d.Value = Convert.ToByte(p.stringValue);
                            }
                            break;
                        }
                    case "INTEGER":
                        {
                            if (p.stringValue == "")
                            {
                                d.Value = DBNull.Value;
                            }
                            else
                            {
                                d.Value = Convert.ToInt32(p.stringValue);
                            }
                            break;
                        }
                    case "LONG":
                        {
                            if (p.stringValue == "")
                            {
                                d.Value = DBNull.Value;
                            }
                            else
                            {
                                d.Value = Convert.ToInt64(p.stringValue);
                            }
                            break;
                        }
                    default:
                        {
                            d.Value = p.stringValue;
                            break;
                        }
                }

            }

            s.Command = com;
            com.Connection = _connection;
            return com;
        }


        private void _throwException(string message, Exception innerException)
        {
            DataFactoryException newMine = new DataFactoryException(message, innerException);

            // Throw
            throw newMine;
        }


        #endregion

        #region " ColumnNamesUpperToMixed "

        public static HybridDictionary ColumnNamesUpperToMixed()
        {
            HybridDictionary myHyD = new HybridDictionary
            {
                { "ACTUALSTARTS", "ActualStarts" },
                { "ACTUALSTARTSCOUNT", "ActualStartsCount" },
                { "APRGDC", "AprGDC" },
                { "APRGMC", "AprGMC" },
                { "APRNDC", "AprNDC" },
                { "APRNMC", "AprNMC" },
                { "ARPERMISSIONS", "ARPermissions" },
                { "ATTEMPTEDSTARTS", "AttemptedStarts" },
                { "ATTEMPTEDSTARTSCOUNT", "AttemptedStartsCount" },
                { "AUGGDC", "AugGDC" },
                { "AUGGMC", "AugGMC" },
                { "AUGNDC", "AugNDC" },
                { "AUGNMC", "AugNMC" },
                { "BEGINCAUSECODE", "BeginCauseCode" },
                { "BTUORHEATCONTENT", "BtuOrHeatContent" },
                { "BULKRS", "BulkRS" },
                { "CALCHOURS", "CalcHours" },
                { "CARDNUMBER", "CardNumber" },
                { "CARRYOVERLASTYEAR", "CarryOverLastYear" },
                { "CARRYOVERNEXTYEAR", "CarryOverNextYear" },
                { "CAUSECODE", "CauseCode" },
                { "CAUSECODEDESC", "CauseCodeDesc" },
                { "CAUSECODEEXT", "CauseCodeExt" },
                { "CAUSECODEEXTDISPLAY", "CauseCodeExtDisplay" },
                { "CAUSECODEEXTEDITABLE", "CauseCodeExtEditable" },
                { "CHANGEDATETIME1", "ChangeDateTime1" },
                { "CHANGEDATETIME2", "ChangeDateTime2" },
                { "CHANGEINEVENTTYPE1", "ChangeInEventType1" },
                { "CHANGEINEVENTTYPE2", "ChangeInEventType2" },
                { "CHECKSTATUS", "CheckStatus" },
                { "CLOCKHOURS", "ClockHours" },
                { "CODE", "Code" },
                { "CODEDESCRIPTION", "CodeDescription" },
                { "COMBINEDCYCLECT", "CombinedCycleCT" },
                { "COMBINEDCYCLECTSTEAM", "CombinedCycleCTSteam" },
                { "COMBINEDCYCLEJE", "CombinedCycleJE" },
                { "COMBINEDCYCLEJESTEAM", "CombinedCycleJESteam" },
                { "COMBINEDCYCLENAME", "CombinedCycleName" },
                { "COMBINEDCYCLESHORTNAME", "CombinedCycleShortName" },
                { "COMBINEDCYCLESTEAM", "CombinedCycleSteam" },
                { "COMMERCIALDATE", "CommercialDate" },
                { "CONTRIBCODE", "ContribCode" },
                { "COPYTOVERBDESC", "CopyToVerbDesc" },
                { "CUMENGINESTARTS", "CumEngineStarts" },
                { "CUMFIREDHOURS", "CumFiredHours" },
                { "CUSTOMFIELD1", "CustomField1" },
                { "CUSTOMFIELD2", "CustomField2" },
                { "CUSTOMLISTBOX", "CustomListbox" },
                { "DAILY", "Daily" },
                { "DAYLIGHTSAVINGTIME", "DaylightSavingTime" },
                { "DECGDC", "DecGDC" },
                { "DECGMC", "DecGMC" },
                { "DECNDC", "DecNDC" },
                { "DECNMC", "DecNMC" },
                { "DEPCAPREADONLY", "DepCapReadOnly" },
                { "DERATINGAMOUNT", "DeratingAmount" },
                { "DESCRIPTION", "Description" },
                { "DESIGNFLOWE", "DesignFlowE" },
                { "DESIGNFLOWP", "DesignFlowP" },
                { "DESIGNPRESSURE", "DesignPressure" },
                { "DESIGNQUALITY", "DesignQuality" },
                { "DESIGNTEMPERATURE", "DesignTemperature" },
                { "DIESEL", "Diesel" },
                { "DISCARD", "Discard" },
                { "DOMINANTDERATE", "DominantDerate" },
                { "E_PUMPINGHOURS", "E_PumpingHours" },
                { "E_SYNCHCONDHOURS", "E_SynchCondHours" },
                { "E_SYNCHHOURS", "E_SynchHours" },
                { "EAFXS", "EAFxs" },
                { "EDITFLAG", "EditFlag" },
                { "EFFENDDATE", "EffEndDate" },
                { "EFFSTARTDATE", "EffStartDate" },
                { "EMAIL_ADDRESS", "Email_Address" },
                { "ENDDATETIME", "EndDateTime" },
                { "ENDINGCAUSECODE", "EndingCauseCode" },
                { "ENDINGDATETIME", "EndingDateTime" },
                { "ENDINGYEAR", "EndingYear" },
                { "ENGLISHORMETRIC", "EnglishOrMetric" },
                { "EQUIPGROUPNAME", "EquipGroupName" },
                { "EQUIVHOURS", "EquivHours" },
                { "EQUIVMWHRS", "EquivMWhrs" },
                { "ERRORMESSAGE", "ErrorMessage" },
                { "ERRORSEVERITY", "ErrorSeverity" },
                { "EV_DATETIME", "EV_DateTime" },
                { "EVENCARDNUMBER", "EvenCardNumber" },
                { "EVENTCONTRIBCODE", "EventContribCode" },
                { "EVENTEVENCARDNO", "EventEvenCardNo" },
                { "EVENTFILE", "EventFile" },
                { "EVENTNUMBER", "EventNumber" },
                { "EVENTTYPE", "EventType" },
                { "EXAMPLE3D", "Example3D" },
                { "EXPANDEDREPORTING", "ExpandedReporting" },
                { "EXTENSIONTYPE", "ExtensionType" },
                { "EXTOFSCHEDOUTAGES", "ExtofSchedOutages" },
                { "FAILUREMECHCODE", "FailureMechCode" },
                { "FEBGDC", "FebGDC" },
                { "FEBGMC", "FebGMC" },
                { "FEBNDC", "FebNDC" },
                { "FEBNMC", "FebNMC" },
                { "FIRST_NAME", "First_Name" },
                { "FIXEDBYUSER", "FixedByUser" },
                { "FIXEDBYUSERFILE", "FixedByUserFile" },
                { "FIXEDN", "FixedN" },
                { "FIXEDNVALUE", "FixedNValue" },
                { "FL_DENOMINATOR", "FL_Denominator" },
                { "FL_FORDNUMERATOR", "FL_FORdNumerator" },
                { "FL_NUMERATOR", "FL_Numerator" },
                { "FLOWE", "FlowE" },
                { "FLOWP", "FlowP" },
                { "FLUIDIZEDBED", "FluidizedBed" },
                { "FOCOUNT", "FOCount" },
                { "FOCOUNT_OMC", "FOCount_OMC" },
                { "FORATE", "FORate" },
                { "FORCEDOUTAGEHOURS", "ForcedOutageHours" },
                { "FOSSILSTEAM", "FossilSteam" },
                { "FOURWEEKS", "FourWeeks" },
                { "FUELQTYFORMAT", "FuelQtyFormat" },
                { "FUELQUALITYSHOW", "FuelQualityShow" },
                { "G_PERIODHOURS", "G_PeriodHours" },
                { "G_SERVICEHOURS", "G_ServiceHours" },
                { "G_SERVICEHOURSCALC", "G_ServiceHoursCalc" },
                { "GASTURBINESIMPLECYCLE", "GasTurbineSimpleCycle" },
                { "GENELECT", "GenElect" },
                { "GEOTHERMAL", "Geothermal" },
                { "GMC_WEIGHT", "GMC_Weight" },
                { "GMCWEIGHTING", "GMCWeighting" },
                { "GRANULARITY", "Granularity" },
                { "GROSSAVAILCAPACITY", "GrossAvailCapacity" },
                { "GROSSDEPCAP", "GrossDepCap" },
                { "GROSSDEPCAPACITY", "GrossDepCapacity" },
                { "GROSSGEN", "GrossGen" },
                { "GROSSMAXCAP", "GrossMaxCap" },
                { "GROSSMAXCAPACITY", "GrossMaxCapacity" },
                { "GROSSNETBOTH", "GrossNetBoth" },
                { "GROUPID", "GroupID" },
                { "GROUPLONGNAME", "GroupLongName" },
                { "GROUPNAME", "GroupName" },
                { "GROUPSELECTED", "GroupSelected" },
                { "GROUPSHORTNAME", "GroupShortName" },
                { "H1TORS", "H1toRS" },
                { "H1TOSD", "H1toSD" },
                { "H1TOSO", "H1toSO" },
                { "H2TORS", "H2toRS" },
                { "H2TOSD", "H2toSD" },
                { "H2TOSO", "H2toSO" },
                { "H3TORS", "H3toRS" },
                { "H3TOSD", "H3toSD" },
                { "H3TOSO", "H3toSO" },
                { "H4TORS", "H4toRS" },
                { "H4TOSD", "H4toSD" },
                { "H4TOSO", "H4toSO" },
                { "HOURLY", "Hourly" },
                { "HYDRO", "Hydro" },
                { "IGNOREAC", "IgnoreAC" },
                { "INACTIVEHOURS", "InactiveHours" },
                { "INPUTDATAREQUIREMENTS", "InputDataRequirements" },
                { "JANGDC", "JanGDC" },
                { "JANGMC", "JanGMC" },
                { "JANNDC", "JanNDC" },
                { "JANNMC", "JanNMC" },
                { "JETENGINESIMPLECYCLE", "JetEngineSimpleCycle" },
                { "JOGROSSGEN", "JOGrossGen" },
                { "JOGROSSMAXCAP", "JOGrossMaxCap" },
                { "JOINTOWNERSHIP", "JointOwnership" },
                { "JONETGEN", "JONetGen" },
                { "JONETMAXCAP", "JONetMaxCap" },
                { "JOPRIQTYBURNED", "JOPriQtyBurned" },
                { "JOQUAQTYBURNED", "JOQuaQtyBurned" },
                { "JOSECQTYBURNED", "JOSecQtyBurned" },
                { "JOTERQTYBURNED", "JOTerQtyBurned" },
                { "JOTOTALBTUS", "JOTotalBtus" },
                { "JULGDC", "JulGDC" },
                { "JULGMC", "JulGMC" },
                { "JULNDC", "JulNDC" },
                { "JULNMC", "JulNMC" },
                { "JUNGDC", "JunGDC" },
                { "JUNGMC", "JunGMC" },
                { "JUNNDC", "JunNDC" },
                { "JUNNMC", "JunNMC" },
                { "KEYNAME", "KeyName" },
                { "LAST_NAME", "Last_Name" },
                { "LASTDATETIME", "LastDateTime" },
                { "LEVEL0", "Level0" },
                { "LEVEL1", "Level1" },
                { "LEVEL2", "Level2" },
                { "LEVEL3", "Level3" },
                { "LEVEL4", "Level4" },
                { "LEVEL5", "Level5" },
                { "LFILTER", "lFilter" },
                { "LISTBOXVALUES", "ListboxValues" },
                { "LOADASCII", "LoadASCII" },
                { "LOADDBFS", "LoadDBFs" },
                { "LOCATIONDESCRIPTION", "LocationDescription" },
                { "LOGINID", "LoginID" },
                { "LQUARTERLYDATA", "lQuarterlyData" },
                { "MAINTOUTAGEHOURS", "MaintOutageHours" },
                { "MANHOURSWORKED", "ManhoursWorked" },
                { "MARGDC", "MarGDC" },
                { "MARGMC", "MarGMC" },
                { "MARNDC", "MarNDC" },
                { "MARNMC", "MarNMC" },
                { "MAXCAPREADONLY", "MaxCapReadOnly" },
                { "MAYGDC", "MayGDC" },
                { "MAYGMC", "MayGMC" },
                { "MAYNDC", "MayNDC" },
                { "MAYNMC", "MayNMC" },
                { "MISC", "Misc" },
                { "MISCMULTI", "MiscMulti" },
                { "MONTHLY", "Monthly" },
                { "MWE", "MWe" },
                { "N_PERIODHOURS", "N_PeriodHours" },
                { "N_SERVICEHOURS", "N_ServiceHours" },
                { "N_SERVICEHOURSCALC", "N_ServiceHoursCalc" },
                { "NETAVAILCAPACITY", "NetAvailCapacity" },
                { "NETDEPCAP", "NetDepCap" },
                { "NETDEPCAPACITY", "NetDepCapacity" },
                { "NETGEN", "NetGen" },
                { "NETMAXCAP", "NetMaxCap" },
                { "NETMAXCAPACITY", "NetMaxCapacity" },
                { "NMC_WEIGHT", "NMC_Weight" },
                { "NMCWEIGHTING", "NMCWeighting" },
                { "NOVGDC", "NovGDC" },
                { "NOVGMC", "NovGMC" },
                { "NOVNDC", "NovNDC" },
                { "NOVNMC", "NovNMC" },
                { "NUCLEAR", "Nuclear" },
                { "OCTGDC", "OctGDC" },
                { "OCTGMC", "OctGMC" },
                { "OCTNDC", "OctNDC" },
                { "OCTNMC", "OctNMC" },
                { "ORGANIZATION", "Organization" },
                { "OTHERFILE", "OtherFile" },
                { "PASSWORD", "Password" },
                { "PEAKPERIOD", "PeakPeriod" },
                { "PEAKPERIODSET", "PeakPeriodSet" },
                { "PERF02DECIMALPLACES", "Perf02DecimalPlaces" },
                { "PERFCALCDATE", "PerfCalcDate" },
                { "PERFFILE", "PerfFile" },
                { "PERFORMANCEDATE", "PerformanceDate" },
                { "PERFORMANCESTART", "PerformanceStart" },
                { "PERIOD", "Period" },
                { "PERIODEND", "PeriodEnd" },
                { "PERIODHOURS", "PeriodHours" },
                { "PERIODSTART", "PeriodStart" },
                { "PHONE", "Phone" },
                { "PJMIOCODE", "PJMIOCode" },
                { "PJMLOADSTATUS", "PJMLoadStatus" },
                { "PJMSTARTSCOUNT", "PJMStartsCount" },
                { "PJMSTATUSDATE", "PJMStatusDate" },
                { "PLANNEDOUTAGEHOURS", "PlannedOutageHours" },
                { "PLANTMGTCONTROL", "PlantMgtControl" },
                { "PRESSURE", "Pressure" },
                { "PRIASHSOFTTEMP", "PriAshSoftTemp" },
                { "PRIAVGHEATCONTENT", "PriAvgHeatContent" },
                { "PRIBTUS", "PriBtus" },
                { "PRIFUELCODE", "PriFuelCode" },
                { "PRIGRINDINDEXVANAD", "PriGrindIndexVanad" },
                { "PRIHEATCONTENT", "PriHeatContent" },
                { "PRIMARYALERT", "PrimaryAlert" },
                { "PRIPERCENTALKALINES", "PriPercentAlkalines" },
                { "PRIPERCENTASH", "PriPercentAsh" },
                { "PRIPERCENTMOISTURE", "PriPercentMoisture" },
                { "PRIPERCENTSULFUR", "PriPercentSulfur" },
                { "PRIQTYBURNED", "PriQtyBurned" },
                { "PROCESSED", "Processed" },
                { "PUMPINGDATA", "PumpingData" },
                { "PUMPINGHOURS", "PumpingHours" },
                { "QUAASHSOFTTEMP", "QuaAshSoftTemp" },
                { "QUAAVGHEATCONTENT", "QuaAvgHeatContent" },
                { "QUABTUS", "QuaBtus" },
                { "QUAFUELCODE", "QuaFuelCode" },
                { "QUAGRINDINDEXVANAD", "QuaGrindIndexVanad" },
                { "QUALITY", "Quality" },
                { "QUAPERCENTALKALINES", "QuaPercentAlkalines" },
                { "QUAPERCENTASH", "QuaPercentAsh" },
                { "QUAPERCENTMOISTURE", "QuaPercentMoisture" },
                { "QUAPERCENTSULFUR", "QuaPercentSulfur" },
                { "QUAQTYBURNED", "QuaQtyBurned" },
                { "QUARTERLY", "Quarterly" },
                { "REPORTFILENAME", "ReportFileName" },
                { "RETIREMENTDATE", "RetirementDate" },
                { "REVISIONCARD01", "RevisionCard01" },
                { "REVISIONCARD02", "RevisionCard02" },
                { "REVISIONCARD03", "RevisionCard03" },
                { "REVISIONCARD1", "RevisionCard1" },
                { "REVISIONCARD2", "RevisionCard2" },
                { "REVISIONCARD3", "RevisionCard3" },
                { "REVISIONCARD4", "RevisionCard4" },
                { "REVISIONCARDEVEN", "RevisionCardEven" },
                { "REVISIONCARDODD", "RevisionCardOdd" },
                { "REVMONTHCARD01", "RevMonthCard01" },
                { "REVMONTHCARD02", "RevMonthCard02" },
                { "REVMONTHCARD03", "RevMonthCard03" },
                { "REVMONTHCARD1", "RevMonthCard1" },
                { "REVMONTHCARD2", "RevMonthCard2" },
                { "REVMONTHCARD3", "RevMonthCard3" },
                { "REVMONTHCARD4", "RevMonthCard4" },
                { "REVMONTHCARDEVEN", "RevMonthCardEven" },
                { "REVMONTHCARDODD", "RevMonthCardOdd" },
                { "RSCOUNT", "RSCount" },
                { "RSHOURS", "RSHours" },
                { "RSTOH1", "RStoH1" },
                { "RSTOH2", "RStoH2" },
                { "RSTOH3", "RStoH3" },
                { "RSTOH4", "RStoH4" },
                { "RSTOSD", "RStoSD" },
                { "RSTOSO", "RStoSO" },
                { "SDTOH1", "SDtoH1" },
                { "SDTOH2", "SDtoH2" },
                { "SDTOH3", "SDtoH3" },
                { "SDTOH4", "SDtoH4" },
                { "SDTORS", "SDtoRS" },
                { "SDTOSO", "SDtoSO" },
                { "SECASHSOFTTEMP", "SecAshSoftTemp" },
                { "SECAVGHEATCONTENT", "SecAvgHeatContent" },
                { "SECBTUS", "SecBtus" },
                { "SECFUELCODE", "SecFuelCode" },
                { "SECGRINDINDEXVANAD", "SecGrindIndexVanad" },
                { "SECHEATCONTENT", "SecHeatContent" },
                { "SECPERCENTALKALINES", "SecPercentAlkalines" },
                { "SECPERCENTASH", "SecPercentAsh" },
                { "SECPERCENTMOISTURE", "SecPercentMoisture" },
                { "SECPERCENTSULFUR", "SecPercentSulfur" },
                { "SECQTYBURNED", "SecQtyBurned" },
                { "SECTIONNAME", "SectionName" },
                { "SEPGDC", "SepGDC" },
                { "SEPGMC", "SepGMC" },
                { "SEPNDC", "SepNDC" },
                { "SEPNMC", "SepNMC" },
                { "SERVICEHOURMETHOD", "ServiceHourMethod" },
                { "SERVICEHOURMETHODTEXT", "ServiceHourMethodText" },
                { "SERVICEHOURS", "ServiceHours" },
                { "SERVICEHOURSCALC", "ServiceHoursCalc" },
                { "SETNAME", "SetName" },
                { "SIEMENS", "Siemens" },
                { "SOTOH1", "SOtoH1" },
                { "SOTOH2", "SOtoH2" },
                { "SOTOH3", "SOtoH3" },
                { "SOTOH4", "SOtoH4" },
                { "SOTORS", "SOtoRS" },
                { "SOTOSD", "SOtoSD" },
                { "STARTDATETIME", "StartDateTime" },
                { "STARTINGCAUSECODE", "StartingCauseCode" },
                { "STARTINGDATETIME", "StartingDateTime" },
                { "STARTINGRELIABILITY", "StartingReliability" },
                { "STARTINGYEAR", "StartingYear" },
                { "STREAM", "Stream" },
                { "SYNCHCONDDATA", "SynchCondData" },
                { "SYNCHCONDHOURS", "SynchCondHours" },
                { "SYNCHHOURS", "SynchHours" },
                { "TASK", "Task" },
                { "TEMPERATURE", "Temperature" },
                { "TERASHSOFTTEMP", "TerAshSoftTemp" },
                { "TERAVGHEATCONTENT", "TerAvgHeatContent" },
                { "TERBTUS", "TerBtus" },
                { "TERFUELCODE", "TerFuelCode" },
                { "TERGRINDINDEXVANAD", "TerGrindIndexVanad" },
                { "TERPERCENTALKALINES", "TerPercentAlkalines" },
                { "TERPERCENTASH", "TerPercentAsh" },
                { "TERPERCENTMOISTURE", "TerPercentMoisture" },
                { "TERPERCENTSULFUR", "TerPercentSulfur" },
                { "TERQTYBURNED", "TerQtyBurned" },
                { "TIMESTAMP", "TimeStamp" },
                { "TL_DATETIME", "TL_DateTime" },
                { "TOTALBTUS", "TotalBtus" },
                { "TRIPMECH", "TripMech" },
                { "TYPUNITLOADING", "TypUnitLoading" },
                { "UNITAGE", "UnitAge" },
                { "UNITNAME", "UnitName" },
                { "UNITSELECTED", "UnitSelected" },
                { "UNITSHORTNAME", "UnitShortName" },
                { "UNITTYPE", "UnitType" },
                { "USERID", "UserID" },
                { "UTILITYUNITCODE", "UtilityUnitCode" },
                { "VALUE", "Value" },
                { "VERBALDESC", "VerbalDesc" },
                { "VERBALDESC1", "VerbalDesc1" },
                { "VERBALDESC2", "VerbalDesc2" },
                { "VERBALDESC86", "VerbalDesc86" },
                { "VERBALDESCFULL", "VerbalDescFull" },
                { "WEEKLYMS", "WeeklyMS" },
                { "WEEKLYSS", "WeeklySS" },
                { "WHENWRITTENUTC", "WhenWrittenUTC" },
                { "WORKDETAILS", "WorkDetails" },
                { "WORKENDED", "WorkEnded" },
                { "WORKSTARTED", "WorkStarted" },
                { "WRITECAISO", "WriteCAISO" },
                { "WRITECEA", "WriteCEA" },
                { "WRITEGENELECT", "WriteGenElect" },
                { "WRITEISO1", "WriteISO1" },
                { "WRITEISO2", "WriteISO2" },
                { "WRITEISO3", "WriteISO3" },
                { "WRITEISONE", "WriteISONE" },
                { "WRITEMISO", "WriteMISO" },
                { "WRITENERC", "WriteNERC" },
                { "WRITENYISO", "WriteNYISO" },
                { "WRITEPJM", "WritePJM" },
                { "WRITESIEMENS", "WriteSiemens" },
                { "YEAR", "Year" },
                { "YEARLIMIT", "YearLimit" },
                { "YEARLY", "Yearly" }
            };
            return myHyD;

        }

        #endregion

        #region " DoesTableExist "

        public int DoesTableExist(string tableName)
        {

            int i = 0;
            int j = 0;
            int iReturn = -1;  // DEFAULT:  table does NOT exist
            System.Object objReturn = null;
            string statement = "SELECT COUNT(*) FROM " + tableName;

            try
            {

                objReturn = _executeScalar(statement, null, out j, null);
                i = Convert.ToInt32(objReturn);

                if (i >= 0)
                {
                    // table exists even if empty
                    iReturn = 1;
                }
                else
                {
                    // table does NOT exist 
                    iReturn = -1;
                }
            }
            catch
            {
                // table does NOT exist
                iReturn = -1;
            }

            return iReturn;
        }

        #endregion

        #region " GetDataSet overloads "


        public DataSet GetDataSet(string statement, Array parms)
        {
            // Overloaded method
            return _getDataSet(statement, parms, null);
        }


        public DataSet GetDataSet(string statement, Array parms, IDbTransaction transaction)
        {
            // Overloaded method
            return _getDataSet(statement, parms, transaction);
        }


        private DataSet _getDataSet(string statement, Array parms, IDbTransaction transaction)
        {
            string _tableName;

            // Returns a DataSet given the statement

            _tableName = GetTableName(statement);
            statement = _SetTableName(statement);
            IDataAdapter da;
            DataSet ds = new DataSet();
            da = _setupDataAdapter(statement, parms, transaction);

            try
            {
                // Fill and return
                da.MissingMappingAction = MissingMappingAction.Passthrough;
                da.MissingSchemaAction = MissingSchemaAction.AddWithKey;
                //da.FillSchema(ds, System.Data.SchemaType.Source);
                da.Fill(ds);
                ds.DataSetName = "dsGADSNG";
                ds.Tables["Table"].TableName = _tableName;
                ds.ExtendedProperties.Add("Creator", "DataFactory class");
                ds.ExtendedProperties.Add("TimeCreated", DateTime.Now.ToShortTimeString());
                ds.ExtendedProperties.Add("Statement", statement);
                ds.ExtendedProperties.Add("Parameters", parms);

                if (Provider == "Oracle")
                {
                    // Oracle column names can be returned in all UPPERCASE which causes the XAML path= to fail
                    foreach (DataColumn myCols in ds.Tables[_tableName].Columns)
                    {
                        if (myHyD.Contains(myCols.ColumnName))
                        {
                            myCols.ColumnName = myHyD[(string)myCols.ColumnName.Trim()].ToString();
                        }
                    }
                }

                return ds;
            }
            catch (Exception e)
            {
                if (Provider == "SqlClient")
                {
                    try
                    {
                        string tmpStatement = statement.Replace("GADSNG.", "dbo.");
                        da = _setupDataAdapter(tmpStatement, parms, transaction);

                        // Fill and return
                        da.MissingMappingAction = MissingMappingAction.Passthrough;
                        da.MissingSchemaAction = MissingSchemaAction.AddWithKey;
                        da.Fill(ds);
                        ds.DataSetName = "dsGADSNG";
                        ds.Tables["Table"].TableName = _tableName;
                        ds.ExtendedProperties.Add("Creator", "DataFactory class");
                        ds.ExtendedProperties.Add("TimeCreated", DateTime.Now.ToShortTimeString());
                        ds.ExtendedProperties.Add("Statement", statement);
                        ds.ExtendedProperties.Add("Parameters", parms);
                        return ds;

                    }
                    catch (Exception e2)
                    {
                        _throwException("Could not fill DataSet", e2);
                    }
                }
                else
                {
                    _throwException("Could not fill DataSet", e);
                }
            }
            return ds;
        }


        #endregion

        #region " GetDataAdapter overloads "

        public DbDataAdapter GetDataAdapter(ref string statement, Array parms)
        {
            // Overloaded method
            return _getDataAdapter(ref statement, parms, null);
        }


        public DbDataAdapter GetDataAdapter(ref string statement, Array parms, IDbTransaction transaction)
        {
            // Overloaded method
            return _getDataAdapter(ref statement, parms, transaction);
        }


        private DbDataAdapter _getDataAdapter(ref string statement, Array parms, IDbTransaction transaction)
        {
            string _tableName;

            // Returns a DataSet given the statement

            _tableName = GetTableName(statement);
            statement = _SetTableName(statement);
            DbDataAdapter da;

            da = (DbDataAdapter)_setupDataAdapter(statement, parms, transaction);

            try
            {
                // Fill and return
                da.MissingMappingAction = MissingMappingAction.Passthrough;
                da.MissingSchemaAction = MissingSchemaAction.AddWithKey;

                return da;
            }
            catch (Exception e)
            {
                if (Provider == "SqlClient")
                {
                    try
                    {
                        string tmpStatement = statement.Replace("GADSNG.", "dbo.");
                        da = (DbDataAdapter)_setupDataAdapter(tmpStatement, parms, transaction);
                        // Fill and return
                        da.MissingMappingAction = MissingMappingAction.Passthrough;
                        da.MissingSchemaAction = MissingSchemaAction.AddWithKey;

                        return da;
                    }
                    catch (Exception e2)
                    {
                        _throwException("Could not create DbDataAdapter", e2);
                    }
                }
                else
                {
                    _throwException("Could not create DbDataAdapter", e);
                }
            }
            return da;
        }

        #endregion

        #region " GetIDataAdapter overloads "

        public IDataAdapter GetIDataAdapter(ref string statement, Array parms)
        {
            // Overloaded method
            return _getIDataAdapter(ref statement, parms, null);
        }


        public IDataAdapter GetIDataAdapter(ref string statement, Array parms, IDbTransaction transaction)
        {
            // Overloaded method
            return _getIDataAdapter(ref statement, parms, transaction);
        }


        private IDataAdapter _getIDataAdapter(ref string statement, Array parms, IDbTransaction transaction)
        {
            string _tableName;

            // Returns a DataSet given the statement

            _tableName = GetTableName(statement);
            statement = _SetTableName(statement);
            IDataAdapter da;

            da = (IDataAdapter)_setupDataAdapter(statement, parms, transaction);

            try
            {
                // Fill and return
                da.MissingMappingAction = MissingMappingAction.Passthrough;
                da.MissingSchemaAction = MissingSchemaAction.AddWithKey;

                return da;
            }
            catch (Exception e)
            {
                if (Provider == "SqlClient")
                {
                    try
                    {
                        string tmpStatement = statement.Replace("GADSNG.", "dbo.");
                        da = (IDataAdapter)_setupDataAdapter(tmpStatement, parms, transaction);

                        // Fill and return
                        da.MissingMappingAction = MissingMappingAction.Passthrough;
                        da.MissingSchemaAction = MissingSchemaAction.AddWithKey;

                        return da;
                    }
                    catch (Exception e2)
                    {
                        _throwException("Could not create IDataAdapter", e2);
                    }
                }
                else
                {
                    _throwException("Could not create IDataAdapter", e);
                }
            }
            return da;
        }

        #endregion

        #region " GetDataTable overloads "

        public DataTable GetDataTable(string statement, Array parms)
        {
            // Overloaded method
            return _getDataTable(statement, parms, null);
        }


        public DataTable GetDataTable(string statement, Array parms, IDbTransaction transaction)
        {
            // Overloaded method
            return _getDataTable(statement, parms, transaction);
        }


        private DataTable _getDataTable(string statement, Array parms, IDbTransaction transaction)
        {
            string _tableName;

            // Returns a DataSet given the statement

            _tableName = GetTableName(statement);
            statement = _SetTableName(statement);

            // Return a DataTable given the statement
            DbDataAdapter da;

            //Dim ds As New DataSet()
            DataTable dt = new DataTable(_tableName);

            da = (DbDataAdapter)_setupDataAdapter(statement, parms, transaction);

            try
            {
                // Fill and return
                da.MissingMappingAction = MissingMappingAction.Passthrough;
                da.MissingSchemaAction = MissingSchemaAction.AddWithKey;
                da.Fill(dt);
                dt.ExtendedProperties.Add("Creator", "DataFactory class");
                dt.ExtendedProperties.Add("TimeCreated", DateTime.Now.ToShortTimeString());
                dt.ExtendedProperties.Add("Statement", statement);
                dt.ExtendedProperties.Add("Parameters", parms);

                if (Provider == "Oracle")
                {
                    // Oracle column names can be returned in all UPPERCASE which causes the XAML path= to fail
                    foreach (DataColumn myCols in dt.Columns)
                    {
                        if (myHyD.Contains(myCols.ColumnName))
                        {
                            myCols.ColumnName = myHyD[(string)myCols.ColumnName.Trim()].ToString();
                        }
                    }
                }

                return dt;
            }
            catch (Exception e)
            {
                if (Provider == "SqlClient")
                {
                    string tmpStatement = statement.Replace("GADSNG.", "dbo.");

                    da = (DbDataAdapter)_setupDataAdapter(tmpStatement, parms, transaction);

                    try
                    {
                        // Fill and return
                        da.MissingMappingAction = MissingMappingAction.Passthrough;
                        da.MissingSchemaAction = MissingSchemaAction.AddWithKey;
                        da.Fill(dt);
                        dt.ExtendedProperties.Add("Creator", "DataFactory class");
                        dt.ExtendedProperties.Add("TimeCreated", DateTime.Now.ToShortTimeString());
                        dt.ExtendedProperties.Add("Statement", tmpStatement);
                        dt.ExtendedProperties.Add("Parameters", parms);

                        return dt;
                    }
                    catch (Exception e2)
                    {
                        _throwException("Could not fill DataTable for statement " + statement, e2);
                    }
                }
                else
                {
                    _throwException("Could not fill DataTable for statement " + statement, e);
                }
            }
            return dt;
        }

        #endregion

        #region " ExecuteDataReader overloads "

        public IDataReader ExecuteDataReader(string statement, Array parms, params CommandBehavior[] behaviors) //help here	  //behaviors()
        {
            // Overloaded signature
            return _executeDataReader(statement, parms, null, behaviors);
        }


        public IDataReader ExecuteDataReader(string statement, Array parms, IDbTransaction transaction, params CommandBehavior[] behaviors)	 //behaviors()
        {
            // Overloaded signature
            return _executeDataReader(statement, parms, transaction, behaviors);
        }


        private IDataReader _executeDataReader(string statement, Array parms, IDbTransaction transaction, params CommandBehavior[] behaviors) //behaviors()
        {
            string _tableName;

            // Returns a DataSet given the statement

            _tableName = GetTableName(statement);
            statement = _SetTableName(statement);

            // Return a  data reader given the statement and parms
            IDbCommand com;
            IDataReader dr;
            CommandBehavior behavior = CommandBehavior.Default;

            // Get the command
            com = _getStatement(statement, parms, true);
            if (!(transaction == null))
            {
                com.Transaction = transaction;
            }

            try
            {
                // Setup the command behaviors
                if (!(behaviors == null))
                {
                    foreach (CommandBehavior b in behaviors)
                    {
                        behavior = behavior | b;
                    }
                }

                if (com.Connection.State == ConnectionState.Closed)
                {
                    com.Connection.Open();
                }
                dr = com.ExecuteReader(behavior);
                return dr;
            }
            catch (Exception e)
            {
                if (Provider == "SqlClient")
                {
                    try
                    {
                        string tmpStatement = statement.Replace("GADSNG.", "dbo.");
                        com = _getStatement(tmpStatement, parms, true);

                        if (!(transaction == null))
                        {
                            com.Transaction = transaction;
                        }
                        // Setup the command behaviors
                        if (!(behaviors == null))
                        {
                            foreach (CommandBehavior b in behaviors)
                            {
                                behavior = behavior | b;
                            }
                        }

                        if (com.Connection.State == ConnectionState.Closed)
                        {
                            com.Connection.Open();
                        }
                        dr = com.ExecuteReader(behavior);
                        return dr;
                    }
                    catch (Exception e2)
                    {
                        _throwException("Could not create data reader from statement " + statement, e2);
                    }
                }
                else
                {
                    _throwException("Could not create data reader from statement " + statement, e);
                }
            }
            return null;
        }


        #endregion

        #region " ExecuteStoredProcedure(string spName, string strXML) "

        public int ExecuteStoredProcedure(string spName, string strXML)
        {
            // the spName should have the following format:  GADSNG.xxxxxx since the GADSNG is required as the owner

            // This is written for performing bulk inserts using OpenXML with the SQLCLIENT provider
            // the parameter name in the Stored Procedure MUST BE "@sXML" and its type should be NText

            // THIS IS CODED ONLY FOR SQL SERVER ... IF THEY WANT ORACLE, THEN THIS NEEDS TO BE RE-DONE

            // Execute the command and do not return a result set

            // This needs to be removed from the XML and replaced with <TimeMast> only
            // <TimeMast xmlns=\"http://tempuri.org/TimeMast.xsd\">

            int intAt;
            intAt = strXML.IndexOf(">") + 1;
            strXML = "<TimeMast>" + strXML.Substring(intAt);

            string sConn = _connection.ConnectionString;

            //_Connect = "data source=" & _DataSource & ";initial catalog=GADSNG;User ID=" & _UserID & ";Password=" & _Password & ";Connect Timeout=60;Min Pool Size=1;"
            //sConn = "user id=GADSNG;password=GADSNG;Database=GADSNG;Server=Unit713";

            sConn = "user id=" + _UserID + ";password=" + _Password + ";Database=GADSNG;Server=" + _DataSource;
            SqlConnection objCon = new SqlConnection(sConn);

            SqlCommand objCom = new SqlCommand()
            {
                CommandType = CommandType.StoredProcedure,
                CommandText = spName
            };
            string statement = spName;
            objCom.Connection = objCon;

            SqlParameter sparam = new SqlParameter()
            {
                ParameterName = "@sXML",
                SqlDbType = SqlDbType.NText
            };
            objCom.Parameters.Add(sparam);

            sparam.Value = strXML;

            int intRows = 0;

            try
            {
                if (objCon.State == ConnectionState.Closed)
                {
                    objCon.Open();
                }

                intRows = objCom.ExecuteNonQuery();

                return intRows;
            }
            catch (SqlException e)
            {
                string strErrors;

                SqlErrorCollection myErrors = e.Errors;
                strErrors = "Class: " + e.Class.ToString() + "\n" +
                    "Error # " + e.Number.ToString() + ": " + e.Message.ToString() + " on line " + e.LineNumber.ToString() + "\n" +
                    "Error reported by " + e.Source.ToString() + " while connected to " + e.Server.ToString() + "\n" +
                    "Neither record was written to database." + "\n\n" +
                    "Errors collection contains:";

                for (int i = 0; i < myErrors.Count; i++)
                {
                    strErrors += "\n\nClass: " + myErrors[i].Class.ToString() + "\n" +
                        "Error #" + myErrors[i].Number.ToString() + ": " + myErrors[i].Message.ToString() + " on line " + myErrors[i].LineNumber.ToString() + ".\n" +
                        "Error reported by " + myErrors[i].Source.ToString() + " while connected to " + myErrors[i].Server.ToString();
                }
                //				MessageBox.Show(strErrors,"Error in _executeNonQuery");
                _throwException("Could not invoke ExecuteNonQuery for statement" + statement + "\n" + strErrors, e);
            }
            catch (OleDbException e)
            {
                string strErrors;
                strErrors = "";

                for (int i = 0; i < e.Errors.Count; i++)
                {
                    strErrors += "Index #" + i + "\n\n" +
                        "Message: " + e.Errors[i].Message + "\n\n" +
                        "Native: " + e.Errors[i].NativeError.ToString() + "\n\n" +
                        "Source: " + e.Errors[i].Source + "\n\n" +
                        "SQL: " + e.Errors[i].SQLState + "\n";
                }

                //				MessageBox.Show(strErrors,"Error in _executeNonQuery");
                _throwException("Could not invoke ExecuteNonQuery for statement " + statement + "\n" + strErrors, e);
            }
            catch (OdbcException e)
            {
                string strErrors;
                strErrors = "";

                for (int i = 0; i < e.Errors.Count; i++)
                {
                    strErrors += "Index #" + i + "\n\n" +
                        "Message: " + e.Errors[i].Message + "\n\n" +
                        "Native: " + e.Errors[i].NativeError.ToString() + "\n\n" +
                        "Source: " + e.Errors[i].Source + "\n\n" +
                        "SQL: " + e.Errors[i].SQLState + "\n\n";
                }

                //				MessageBox.Show(strErrors,"Error in _executeNonQuery");
                _throwException("Could not invoke ExecuteNonQuery for statement " + statement + "\n" + strErrors, e);
            }
            catch (OracleException e)
            {
                string strErrors = "";

                strErrors += "Message: " + e.Message.ToString() + "\n" +
                    "NativeError: " + e.ErrorCode.ToString() + "\n" +
                    "Source: " + e.Source.ToString() + "\n" +
                    "SQLState: " + e.TargetSite.ToString() + "\n";

                _throwException("Could not invoke ExecuteNonQuery for statement " + statement + "\n" + strErrors, e);
            }
            catch (Exception e)
            {
                _throwException("Could not invoke ExecuteNonQuery for statement " + statement, e);
            }
            finally
            {

                objCon.Close();
            }
            return 0;
        }

        #endregion

        #region " ExecuteNonQuery overloads "

        public int ExecuteNonQuery(string statement, Array parms)
        {
            // Overloaded method
            return _executeNonQuery(statement, parms, null, out int i, out object o);
        }


        public int ExecuteNonQuery(string statement, Array parms, out int returnVal)
        {
            // Overloaded method
            object o = new object();
            return _executeNonQuery(statement, parms, null, out returnVal, out o);
        }


        public int ExecuteNonQuery(string statement, Array parms, out int returnVal, IDbTransaction transaction)
        {
            // Overloaded method
            object o = new object();
            return _executeNonQuery(statement, parms, transaction, out returnVal, out o);
        }


        public int ExecuteNonQuery(string statement, Array parms, out int returnVal, IDbTransaction transaction, out object outputParms)
        {
            // Overloaded method
            return _executeNonQuery(statement, parms, transaction, out returnVal, out outputParms);
        }


        private int _executeNonQuery(string statement, Array parms, IDbTransaction transaction, out int returnVal, out object outputParms)
        {
            string _tableName;

            // Returns a DataSet given the statement

            _tableName = GetTableName(statement);
            statement = _SetTableName(statement);

            // Execute the command and do not return a result set
            returnVal = 0;
            outputParms = new Object();

            IDbCommand com;
            int rows;
            ArrayList outList = new ArrayList();
            bool leaveOpen = false;

            // Get the command
            com = _getStatement(statement, parms, true);
            if (!(transaction == null))
            {
                com.Transaction = transaction;
            }

            try
            {
                if (com.Connection.State == ConnectionState.Closed)
                {
                    com.Connection.Open();
                }
                else
                {
                    leaveOpen = true;
                }

                statement = com.CommandText.ToString();
                //				statement = com.CommandText.ToString().Replace("TimeStamp","[TimeStamp]");
                com.CommandType = CommandType.Text;
                rows = com.ExecuteNonQuery();

                // Read the return values and output parameters
                foreach (IDataParameter p in com.Parameters)
                {
                    if (p.Direction == ParameterDirection.ReturnValue)
                    {
                        returnVal = Convert.ToInt32(p.Value);
                    }
                    else if (p.Direction != ParameterDirection.Input)
                    {
                        outList.Add(p.Value);
                    }
                }
                outputParms = outList.ToArray();
                return rows;
            }
            catch (SqlException e)
            {
                if (Provider == "SqlClient")
                {
                    string tmpStatement = statement.Replace("GADSNG.", "dbo.");

                    com = _getStatement(tmpStatement, parms, true);
                    if (!(transaction == null))
                    {
                        com.Transaction = transaction;
                    }
                    try
                    {
                        if (com.Connection.State == ConnectionState.Closed)
                        {
                            com.Connection.Open();
                        }
                        else
                        {
                            leaveOpen = true;
                        }

                        statement = com.CommandText.ToString();
                        //				statement = com.CommandText.ToString().Replace("TimeStamp","[TimeStamp]");
                        com.CommandType = CommandType.Text;
                        rows = com.ExecuteNonQuery();

                        // Read the return values and output parameters
                        foreach (IDataParameter p in com.Parameters)
                        {
                            if (p.Direction == ParameterDirection.ReturnValue)
                            {
                                returnVal = Convert.ToInt32(p.Value);
                            }
                            else if (p.Direction != ParameterDirection.Input)
                            {
                                outList.Add(p.Value);
                            }
                        }
                        outputParms = outList.ToArray();
                        return rows;
                    }
                    catch (Exception e2)
                    {
                        _throwException("Could not invoke ExecuteNonQuery for statement " + tmpStatement, e2);
                    }
                }
                else
                {
                    string strErrors;

                    SqlErrorCollection myErrors = e.Errors;
                    strErrors = "Class: " + e.Class.ToString() + "\n" +
                    "Error # " + e.Number.ToString() + ": " + e.Message.ToString() + " on line " + e.LineNumber.ToString() + "\n" +
                    "Error reported by " + e.Source.ToString() + " while connected to " + e.Server.ToString() + "\n" +
                    "Neither record was written to database." + "\n\n" +
                    "Errors collection contains:";

                    for (int i = 0; i < myErrors.Count; i++)
                    {
                        strErrors += "\n\nClass: " + myErrors[i].Class.ToString() + "\n" +
                        "Error #" + myErrors[i].Number.ToString() + ": " + myErrors[i].Message.ToString() + " on line " + myErrors[i].LineNumber.ToString() + ".\n" +
                        "Error reported by " + myErrors[i].Source.ToString() + " while connected to " + myErrors[i].Server.ToString();
                    }
                    //				MessageBox.Show(strErrors,"Error in _executeNonQuery");
                    _throwException("Could not invoke ExecuteNonQuery for statement" + statement + "\n" + strErrors, e);
                }
            }
            catch (OleDbException e)
            {
                string strErrors;
                strErrors = "";

                for (int i = 0; i < e.Errors.Count; i++)
                {
                    strErrors += "Index #" + i + "\n\n" +
                        "Message: " + e.Errors[i].Message + "\n\n" +
                        "Native: " + e.Errors[i].NativeError.ToString() + "\n\n" +
                        "Source: " + e.Errors[i].Source + "\n\n" +
                        "SQL: " + e.Errors[i].SQLState + "\n";
                }

                //				MessageBox.Show(strErrors,"Error in _executeNonQuery");
                _throwException("Could not invoke ExecuteNonQuery for statement " + statement + "\n" + strErrors, e);
            }
            catch (OdbcException e)
            {
                string strErrors;
                strErrors = "";

                for (int i = 0; i < e.Errors.Count; i++)
                {
                    strErrors += "Index #" + i + "\n\n" +
                        "Message: " + e.Errors[i].Message + "\n\n" +
                        "Native: " + e.Errors[i].NativeError.ToString() + "\n\n" +
                        "Source: " + e.Errors[i].Source + "\n\n" +
                        "SQL: " + e.Errors[i].SQLState + "\n\n";
                }

                //				MessageBox.Show(strErrors,"Error in _executeNonQuery");
                _throwException("Could not invoke ExecuteNonQuery for statement " + statement + "\n" + strErrors, e);
            }
            catch (OracleException e)
            {
                string strErrors = "";

                strErrors += "Message: " + e.Message.ToString() + "\n" +
                    "NativeError: " + e.ErrorCode.ToString() + "\n" +
                    "Source: " + e.Source.ToString() + "\n" +
                    "SQLState: " + e.TargetSite.ToString() + "\n";

                _throwException("Could not invoke ExecuteNonQuery for statement " + statement + "\n" + strErrors, e);
            }
            catch (Exception e)
            {
                _throwException("Could not invoke ExecuteNonQuery for statement " + statement, e);
            }
            finally
            {
                if (!(leaveOpen))
                {
                    com.Connection.Close();
                }
            }
            return 0;
        }

        #endregion

        #region " ExecuteScalar overloads "

        public System.Object ExecuteScalar(string statement, Array parms)
        {
            // Overloaded method
            return _executeScalar(statement, parms, out int i, null);
        }


        public System.Object ExecuteScalar(string statement, Array parms, out int returnVal)
        {
            // Overloaded method
            return _executeScalar(statement, parms, out returnVal, null);
        }


        public System.Object ExecuteScalar(string statement, Array parms, out int returnVal, IDbTransaction transaction)
        {
            // Overloaded method
            return _executeScalar(statement, parms, out returnVal, transaction);
        }


        private System.Object _executeScalar(string statement, Array parms, out int returnVal, IDbTransaction transaction)
        {
            string _tableName;

            // Returns a DataSet given the statement

            _tableName = GetTableName(statement);
            statement = _SetTableName(statement);

            // Return a single value using ExecuteScalar

            returnVal = 0;

            IDbCommand com;
            object val;
            bool leaveOpen = false;
            // Get the command
            com = _getStatement(statement, parms, true);
            if (!(transaction == null))
            {
                com.Transaction = transaction;
            }
            try
            {
                if (com.Connection.State == ConnectionState.Closed)
                {
                    com.Connection.Open();
                }
                else
                {
                    leaveOpen = true;
                }
                val = com.ExecuteScalar();
                foreach (IDataParameter p in com.Parameters)
                {
                    if (p.Direction == ParameterDirection.ReturnValue)
                    {
                        returnVal = Convert.ToInt32(p.Value);
                        break;
                    }
                }
                return val;
            }
            catch (Exception e)
            {
                if (Provider == "SqlClient")
                {
                    string tmpStatement = statement.Replace("GADSNG.", "dbo.");

                    com = _getStatement(tmpStatement, parms, true);
                    if (!(transaction == null))
                    {
                        com.Transaction = transaction;
                    }
                    try
                    {
                        if (com.Connection.State == ConnectionState.Closed)
                        {
                            com.Connection.Open();
                        }
                        else
                        {
                            leaveOpen = true;
                        }
                        val = com.ExecuteScalar();
                        foreach (IDataParameter p in com.Parameters)
                        {
                            if (p.Direction == ParameterDirection.ReturnValue)
                            {
                                returnVal = Convert.ToInt32(p.Value);
                                break;
                            }
                        }
                        return val;
                    }
                    catch (Exception e2)
                    {
                        _throwException("Failed to execute ExecuteScalar method for statement " + tmpStatement, e2);
                    }
                }
                else
                {
                    _throwException("Failed to execute ExecuteScalar method for statement " + statement, e);
                }
            }
            finally
            {
                if (!(leaveOpen))
                {
                    com.Connection.Close();
                }
            }
            return 0;
        }


        #endregion

        #region " Other public methods "

        public IDbDataAdapter CreateDataAdapter(string tableName, string selectStatement, string insertStatement, string updateStatement, string deleteStatement, IDbTransaction transaction)
        {

            // Create a data adapter that they can use themselves or with the SyncDataSet method
            // The user will have to setup the tablemappings themselves, this will default to the name of the 
            // parameters as found in the statement XML file

            IDbCommand selCom, insCom, updCom, delCom;
            IDbDataAdapter da;
            GADSNGTableName = tableName;
            string[,] parms = new string[3, 2];
            try
            {
                selCom = _getStatement(selectStatement, null, false);

                parms.SetValue("@GroupName", 0, 0);
                parms.SetValue("GroupName, Fluegge", 0, 1);
                parms.SetValue("@GroupID", 1, 0);
                parms.SetValue("GroupID, 9999", 1, 1);
                parms.SetValue("@ARPermissions", 2, 0);
                parms.SetValue("ARPermissions, Test", 2, 1);

                insCom = _getStatement(insertStatement, parms, false);
                //				insCom.Parameters.Add(New SqlParameter("@GroupName", System.Data.SqlDbType.VarChar, 50, "GroupName"));
                //				insCom.Parameters.Add(New System.Data.SqlClient.SqlParameter("@GroupID", System.Data.SqlDbType.Int, 4, "GroupID"));
                //				insCom.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ARPermissions", System.Data.SqlDbType.VarChar, 10, "ARPermissions"));
                //				

                updCom = _getStatement(updateStatement, null, false);
                delCom = _getStatement(deleteStatement, null, false);

                if (!(transaction == null))
                {
                    selCom.Transaction = transaction;
                    insCom.Transaction = transaction;
                    updCom.Transaction = transaction;
                    delCom.Transaction = transaction;
                }

                da = (IDbDataAdapter)Activator.CreateInstance(_daType, false);
                da.SelectCommand = selCom;
                da.InsertCommand = insCom;
                da.UpdateCommand = updCom;
                da.DeleteCommand = delCom;

                return da;
            }
            catch (Exception e)
            {
                if (Provider == "SqlClient")
                {
                    try
                    {
                        string tmpselectStatement = selectStatement.Replace("GADSNG.", "dbo.");

                        selCom = _getStatement(tmpselectStatement, null, false);

                        parms.SetValue("@GroupName", 0, 0);
                        parms.SetValue("GroupName, Fluegge", 0, 1);
                        parms.SetValue("@GroupID", 1, 0);
                        parms.SetValue("GroupID, 9999", 1, 1);
                        parms.SetValue("@ARPermissions", 2, 0);
                        parms.SetValue("ARPermissions, Test", 2, 1);

                        string tmpinsertStatement = insertStatement.Replace("GADSNG.", "dbo.");

                        insCom = _getStatement(tmpinsertStatement, parms, false);
                        //				insCom.Parameters.Add(New SqlParameter("@GroupName", System.Data.SqlDbType.VarChar, 50, "GroupName"));
                        //				insCom.Parameters.Add(New System.Data.SqlClient.SqlParameter("@GroupID", System.Data.SqlDbType.Int, 4, "GroupID"));
                        //				insCom.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ARPermissions", System.Data.SqlDbType.VarChar, 10, "ARPermissions"));
                        //				

                        string tmpupdateStatement = updateStatement.Replace("GADSNG.", "dbo.");
                        string tmpdeleteStatement = deleteStatement.Replace("GADSNG.", "dbo.");

                        updCom = _getStatement(tmpupdateStatement, null, false);
                        delCom = _getStatement(tmpdeleteStatement, null, false);

                        if (!(transaction == null))
                        {
                            selCom.Transaction = transaction;
                            insCom.Transaction = transaction;
                            updCom.Transaction = transaction;
                            delCom.Transaction = transaction;
                        }

                        da = (IDbDataAdapter)Activator.CreateInstance(_daType, false);
                        da.SelectCommand = selCom;
                        da.InsertCommand = insCom;
                        da.UpdateCommand = updCom;
                        da.DeleteCommand = delCom;

                        return da;
                    }
                    catch (Exception e2)
                    {
                        _throwException("Cannot create a data adapter (CreateDataAdapter)", e2);
                    }
                }
                else
                {
                    _throwException("Cannot create a data adapter (CreateDataAdapter)", e);
                }
            }
            return null;
        }


        public DataSet SyncDataSet(DataSet ds, IDbDataAdapter da)
        {
            // Synchronize the DataSet with the data store using the data adapter (for completeness only)
            try
            {
                da.Update(ds);
            }
            catch (Exception e)
            {
                _throwException("Could not sync the DataSet", e);
            }
            return null;

        }


        public IDbTransaction BeginTransaction(IsolationLevel iso)
        {
            // Start a new transaction WARNING. This opens the connection
            try
            {
                // Open the connection if it is close
                if (_connection.State == ConnectionState.Closed)
                {
                    _connection.Open();
                }

                return _connection.BeginTransaction(iso);
            }
            catch (Exception e)
            {
                _throwException("Could not begin the transaction", e);
            }
            return null;
        }

        #endregion
    }

    #region " Structures "

    // Holds the statements
    struct Statement
    {
        public string Name;
        public string SQL;
        public CommandType CommandType;
        public ArrayList Parms;
        public IDbCommand Command;
    }


    // Holds the parameters
    struct Parm
    {
        public string Name;
        public string SQLName;
        public string Type;
        public ParameterDirection Direction;
        public int maxLength;
        public string SourceColumn;
        public string stringValue;
    }

    #endregion

    #region " Exception Class "

    // Exception Class
    [Serializable()]
    public class DataFactoryException : BaseApplicationException
    {

        // Default constructor
        public DataFactoryException()
            : base()
        {
        }

        // Constructor with exception message
        public DataFactoryException(string message)
            : base(message)
        {
        }

        // Constructor with message and inner exception
        public DataFactoryException(string message, Exception originalException)
            : base(message, originalException)
        {
        }

        // Protected constructor with de-serialize data
        protected DataFactoryException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        // Add custom members here
    }

    #endregion

    public class myReverserClass : IComparer
    {

        // Calls CaseInsensitiveComparer.Compare with the parameters reversed.
        int IComparer.Compare(Object x, Object y)
        {
            return ((new CaseInsensitiveComparer()).Compare(y, x));
        }

    }


}
