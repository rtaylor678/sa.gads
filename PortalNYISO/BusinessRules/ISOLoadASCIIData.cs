using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Data;
using System.Xml;
using System.Windows.Forms;
using System.Collections;
using System.Collections.Specialized;
using BusinessLayer;
using BusinessLayer.BusinessLayer;
using GADSNG.Base;
using Microsoft.ApplicationBlocks.ExceptionManagement;

/*
 ===  Class Name:   ISOLoadASCIIData  ========================================
	Description:   Loads GADS 80/82-column ASCII data
	
		Project:   GADS OS
   Date Created:   2002.12.13
	Code Copyright � 2002 by The Outercurve Foundation, All Rights Reserved.

	  Revision History
   Who     Date		CodeVersion - Comments
   RMF  04/26/2007   Added the new January 2006 05 and 07 record layouts

	Discussion:
   

 ==========================================================================
*/

namespace GADSDataInterfaces
{
    /// <summary>
    /// Summary description for ISOLoadASCIIData.
    /// </summary>
    public class ISOLoadASCIIData
    {
        static Regex cfe_regex = new Regex(@"^[-+]?\d*$");

        public ISOLoadASCIIData()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        /// <summary>
        /// ISOLoadASCIIFile loads in both Performance and Event GADS 80/82-column ASCII data
        /// </summary>
        /// <param name="strFileName"></param>
        /// <returns>strFileName with load status</returns>
        public static string ISOLoadASCIIFile(string strFileName, string connect, string myUser, GADSNGBusinessLayer blConnect, int YearLimit)
        {
            string strRecord;
            strRecord = "";
            string strCardType;
            string strTemp1;
            string strTemp2;
            int nFormat = 0;
            bool lResult;
            bool lRowNew;
            //int intStopLoop;
            //intStopLoop = 0;
            long longCounter;
            longCounter = 0;
            //double douBtus1 = 0;
            //double douBtus2 = 0;
            string strErrorMessage;
            string strMessage;
            strErrorMessage = string.Empty;
            strMessage = string.Empty;
            //			IDataReader myReader;

            System.DateTime MyDateTime;
            System.DateTime MyUTCDateTime;
            MyUTCDateTime = System.DateTime.UtcNow;

            if (connect == string.Empty)
            {
                connect = "DataEntry";
            }

            // GADSNGBusinessLayer blConnect = null;

            //try
            //{
            //blConnect = new GADSNGBusinessLayer(connect);
            //}
            //catch (Exception ex)
            //{
            //    // publish Exception using ExceptionManager
            //    //ExceptionManager.Publish(ex);
            //    Application.DoEvents();
            //    return "EXCEPTION:  " + ex.ToString();
            //}

            ArrayList alDeleteEvent01 = new ArrayList();
            ArrayList alDeleteEvent02 = new ArrayList();
            ArrayList alDeleteEvent03 = new ArrayList();
            ArrayList alDeleteEvent04 = new ArrayList();
            ArrayList alDeleteEvent05 = new ArrayList();

            Setup.SetupDataTable dtSetup = null;

            try
            {
                dtSetup = blConnect.GetSelectedUtilityUnitData("ALL", myUser);
            }
            catch (Exception ex)
            {
                // publish Exception using ExceptionManager
                //ExceptionManager.Publish(ex);
                //Application.DoEvents();
                return "EXCEPTION:  " + ex.ToString();
            }

            Setup.SetupRow drSetup;

            //			try
            //			{
            //				myReader = blConnect.GetSHMethodReader();
            //				//"SELECT UnitShortName, UnitName, ServiceHourMethod, EffStartDate, EffEndDate FROM SHMethod ORDER BY UnitShortName, UnitName"
            //				while (myReader.Read())
            //				{
            //					if (! myReader.IsDBNull(3))
            //					{
            //						if (myReader.GetByte(2) == 2) 
            //						{
            //							drNERCXRef = dtNERCXRef.NewNERCXRefRow;
            //
            //							if (myReader.IsDBNull(0))
            //								drNERCXRef.UnitShortName = String.Empty;
            //							else
            //								drNERCXRef.UnitShortName = myReader.GetString(0).Trim();
            //	                        
            //							if (myReader.IsDBNull(1))
            //								drNERCXRef.UnitName = String.Empty;
            //							else
            //								drNERCXRef.UnitName = myReader.GetString(1).Trim;
            //	                        
            //							if (myReader.IsDBNull(2))
            //								drNERCXRef.GADSMethod = 0;
            //							else
            //								drNERCXRef.GADSMethod = myReader.GetByte(2);
            //	                      
            //							drNERCXRef.NERCClassID = -1;
            //
            //							dtNERCXRef.AddNERCXRefRow(drNERCXRef);
            //						}
            //					}
            //				}
            //			}
            //			catch (Exception ex)
            //			{
            //				strError = ex.ToString;
            //			}
            //			finally
            //			{
            //				myReader.Close();
            //				myReader.Dispose();
            //			}

            //			string strMyTest;

            //			foreach (Setup.SetupRow drSetup in dtSetup.Rows)
            //			{
            //				strMyTest = drSetup.UtilityUnitCode;
            //				strMyTest = drSetup.UnitName;
            //			}

            Event01 dsEvent01 = new Event01();
            Event01.EventData01DataTable tblEvent01 = dsEvent01.EventData01;
            Event01.EventData01Row rowEvent01;

            Event0299 dsEvent02 = new Event0299();
            Event0299.EventData02DataTable tblEvent02 = dsEvent02.EventData02;
            //Event0299.EventData02Row rowEvent02;

            Performance dsPerformance = new Performance();
            Performance.PerformanceDataDataTable tblPerformance = dsPerformance.PerformanceData;
            Performance.PerformanceDataRow rowPerformance;

            string dateString;

            Regex l_regex = new Regex("[0-9]{2}");
            Regex dt_regex = new Regex("[0-9]{8}");
            Regex Exp_regex = new Regex("[F][0-9][0-9][0][AM][ 0-9]{6}[ 0-9]{5}");
            Regex FM_regex = new Regex("[F][0-9][0-9][0]");
            Regex d_regex = new Regex("[A-R{}]");

            // "Key" data variables
            byte intRecordCode;
            //			int intUtilityCode;
            //			int intUnitCode;
            string strUtilityUnitCode;
            short intReportingYear;
            string strRevisionCode;
            string strCardNo;
            //byte intCardNo;

            // used to determine "revision to prior month's data"
            int intRevisionMonth;

            // Performance data variables
            string strPeriod;

            int intGMC = 0;
            int intGDC = 0;
            int intGAG = 0;
            int intNMC = 0;
            int intNDC = 0;
            int intNAG = 0;

            decimal fpGMC = 0;
            decimal fpGDC = 0;
            decimal fpGAG = 0;
            decimal fpNMC = 0;
            decimal fpNDC = 0;
            decimal fpNAG = 0;

            byte intTypicalUnitLoading = 0;
            int intAttemptedStarts = 0;
            int intActualStarts = 0;
            string strTULDescription = string.Empty;

            int intUnitServiceHours = 0;
            int intRSHours = 0;
            int intPumpingHours = 0;
            int intSynchCondensingHours = 0;
            int intAvailableHours = 0;
            int intPlannedOutageHours = 0;
            int intForcedOutageHours = 0;
            int intMaintenanceOutageHours = 0;
            int intScheduledOutageExtensions = 0;
            int intUnavailableHours = 0;
            int intPeriodHours = 0;
            int intInactiveHours = 0;

            decimal fpUnitServiceHours = 0;
            decimal fpRSHours = 0;
            decimal fpPumpingHours = 0;
            decimal fpSynchCondensingHours = 0;
            decimal fpAvailableHours = 0;
            decimal fpPlannedOutageHours = 0;
            decimal fpForcedOutageHours = 0;
            decimal fpMaintenanceOutageHours = 0;
            decimal fpScheduledOutageExtensions = 0;
            decimal fpUnavailableHours = 0;
            decimal fpPeriodHours = 0;
            decimal fpInactiveHours = 0;

            string strPrimaryFuelCode = string.Empty;
            //double dblPrimaryQuantityBurned = 0;
            //int intPrimaryHeatContent = 0;
            //float fpPrimaryAsh = 0;
            //float fpPrimaryMoisture = 0;
            //float fpPrimarySulfur = 0;
            //float fpPrimaryAlkalines = 0;
            //float fpPrimaryGIVNP = 0;
            //int intPrimaryAshSofteningTemp = 0;

            string strSecondaryFuelCode = string.Empty;
            //double dblSecondaryQuantityBurned = 0;
            //int intSecondaryHeatContent = 0;
            //float fpSecondaryAsh = 0;
            //float fpSecondaryMoisture = 0;
            //float fpSecondarySulfur = 0;
            //float fpSecondaryAlkalines = 0;
            //float fpSecondaryGIVNP = 0;
            //int intSecondaryAshSofteningTemp = 0;

            string strTertiaryFuelCode = string.Empty;
            //double dblTertiaryQuantityBurned = 0;
            //int intTertiaryHeatContent = 0;
            //float fpTertiaryAsh = 0;
            //float fpTertiaryMoisture = 0;
            //float fpTertiarySulfur = 0;
            //float fpTertiaryAlkalines = 0;
            //float fpTertiaryGIVNP = 0;
            //int intTertiaryAshSofteningTemp = 0;

            string strQuaternaryFuelCode = string.Empty;
            //double dblQuaternaryQuantityBurned = 0;
            //int intQuaternaryHeatContent = 0;
            //float fpQuaternaryAsh = 0;
            //float fpQuaternaryMoisture = 0;
            //float fpQuaternarySulfur = 0;
            //float fpQuaternaryAlkalines = 0;
            //float fpQuaternaryGIVNP = 0;
            //int intQuaternaryAshSofteningTemp = 0;

            // Event data variables
            short intEventNumber;
            string strEventType;

            string strStartOfEvent;
            string strFirstChangeOfEvent;
            string strEventType1;
            string strSecondChangeOfEvent;
            string strEventType2;
            string strEndOfEvent;
            int intGAC = 0;
            int intNAC = 0;
            decimal fpGAC = 0;
            decimal fpNAC = 0;
            string strDominantDerate = string.Empty;  // D in column 65

            short intCauseCode;
            string strAmplificationCode;  // new field to mimic CEA code
            string strCauseCodeExt;

            string strTimeWorkStarted;
            string strTimeWorkEnded;
            byte intEventContribCode;
            string strPrimaryAlert;
            short intManhoursWorked;
            string strVerbalDescription;  // used for both parts of the verbal description
            string strFailureMech;
            string strExpandedData;

            lResult = true;
            bool lJO;
            lJO = false; // When loading from the ASCII data, we cannot load Joint Ownership data since it's not there

            StreamReader StreamToDisplay = null;

            try  // Open file and trap any errors using handler
            {
                StreamToDisplay = new StreamReader(strFileName);

                while (StreamToDisplay.Peek() > -1)
                {
                    strRecord = StreamToDisplay.ReadLine().Trim();

                    //Application.DoEvents();
                    //intStopLoop++;
                    longCounter++;

                    //					strMessage = " \n\nAt Record " + longCounter.ToString() + " \n\n[" + strRecord +"]";
                    strMessage = " At Record " + longCounter.ToString() + " [" + strRecord + "]";
                    strErrorMessage = strMessage;

                    //if (intStopLoop > 50)
                    //{
                    //    intStopLoop = 0;

                    //    if (System.IO.File.Exists("DEStop.txt") == true)
                    //    {
                    //        StreamToDisplay.Close();

                    //        System.IO.File.Delete("DEStop.txt");

                    //        return strFileName + " -- Loading Aborted";
                    //    }

                    //}

                    strRecord = strRecord.Replace('*', ' ');

                    if (strRecord.StartsWith("05") && strRecord.EndsWith("04"))
                    {
                        continue;
                    }

                    if ((strRecord.StartsWith("07") && !(strRecord.EndsWith("01") || strRecord.EndsWith("02"))))
                    {
                        continue;
                    }
                    
                    if (strRecord.Length >= 13) // have to assume that you have only the "key" data columns for 80-col record (minimum)
                    {
                        strCardType = strRecord.Substring(0, 2);

                        if (strCardType == "97")
                        {
                            if (strRecord.Length == 80 || strRecord.Length == 15)
                            {
                                nFormat = -2;
                            }
                            else if (strRecord.Length == 82 || strRecord.Length == 17)
                            {
                                nFormat = 0;
                            }
                            else
                            {
                                lResult = false;
                                break;
                            }
                        }
                        else if (strCardType == "07")
                        {
                            if (strRecord.Length == 82 || strRecord.Length == 17)
                            {
                                nFormat = 0;
                            }
                            else
                            {
                                if (strRecord.EndsWith("02") && strRecord.Length > 82)
                                {
                                    string strTemp = strRecord;
                                    strRecord = strTemp.Substring(0, 80) + "02";
                                }
                                else
                                {
                                    lResult = false;
                                    break;
                                }
                            }
                        }
                        else if (strCardType == "95")
                        {
                            if (strRecord.Length == 80 || strRecord.Length == 13)
                            {
                                nFormat = -2;
                            }
                            else if (strRecord.Length == 82 || strRecord.Length == 15)
                            {
                                nFormat = 0;
                            }
                            else
                            {
                                lResult = false;
                                break;
                            }
                        }
                        else if (strCardType == "05")
                        {
                            if (strRecord.Length == 125 || strRecord.Length == 15)
                            {
                                nFormat = 0;
                            }
                            else
                            {
                                lResult = false;
                                break;
                            }

                        }
                        else
                        {
                            lResult = false;
                            break;
                        }

                        #region Performance Record
                        if (strCardType == "95" || strCardType == "05")
                        {
                            // Performance Record

                            intRecordCode = Convert.ToByte(95);
                            //							intUtilityCode     = Convert.ToInt32(CheckForEmpty(strRecord.Substring(2,3)));
                            //							intUnitCode        = Convert.ToInt32(CheckForEmpty(strRecord.Substring(5,3)));
                            strUtilityUnitCode = strRecord.Substring(2, 6);
                            drSetup = dtSetup.FindByUtilityUnitCode(strUtilityUnitCode);

                            if (drSetup == null)
                            {
                                strUtilityUnitCode = "000000";
                            }

                            if (nFormat == -2)
                            {
                                // Below is 80-column data format
                                intReportingYear = Convert.ToInt16(CheckForEmpty(strRecord.Substring(8, 2)));

                                if (intReportingYear < 80)
                                {
                                    intReportingYear += 2000;
                                }
                                else
                                {
                                    intReportingYear += 1900;
                                }
                            }
                            else
                            {	// Below is 82-column data format
                                intReportingYear = Convert.ToInt16(CheckForEmpty(strRecord.Substring(8, 4)));
                            }

                            if (intReportingYear < YearLimit)
                            {
                                continue;
                            }

                            strPeriod = strRecord.Substring(12 + nFormat, 2);
                            if (strPeriod.ToUpper().StartsWith("X") & intReportingYear < 1994)
                            {
                                intRevisionMonth = 3 * Convert.ToInt32(CheckForEmpty(strPeriod.Substring(1, 1)));
                            }
                            else if (strPeriod.ToUpper().StartsWith("X") & intReportingYear >= 1994)
                            {
                                continue;
                            }
                            else
                            {
                                intRevisionMonth = Convert.ToInt32(CheckForEmpty(strPeriod));
                                strPeriod = strPeriod.Replace(' ', '0');
                            }

                            strRevisionCode = strRecord.Substring(14 + nFormat, 1);
                            strRevisionCode = strRevisionCode.Replace(' ', '0');

                            if (strRecord.EndsWith("01") & strRecord.Length >= 80)
                            {
                                // Performance 01 card

                                if (strCardType == "95")
                                {
                                    //intGMC = Convert.ToInt32(CheckForEmpty(strRecord.Substring(15 + nFormat, 4)));
                                    //intGDC = Convert.ToInt32(CheckForEmpty(strRecord.Substring(19 + nFormat, 4)));
                                    //intGAG = Convert.ToInt32(CheckForEmpty(strRecord.Substring(23 + nFormat, 7)));
                                    intGMC = 0;
                                    intGDC = 0;
                                    intGAG = 0;
                                    intNMC = Convert.ToInt32(CheckForEmpty(strRecord.Substring(30 + nFormat, 4)));
                                    intNDC = Convert.ToInt32(CheckForEmpty(strRecord.Substring(34 + nFormat, 4)));

                                    //if (intGMC == 0 & intGDC > 0)
                                    //{
                                    //    intGMC = intGDC;
                                    //}
                                    //else if (intGDC == 0 & intGMC > 0)
                                    //{
                                    //    intGDC = intGMC;
                                    //}

                                    if (intNMC == 0 & intNDC > 0)
                                    {
                                        intNMC = intNDC;
                                    }
                                    else if (intNDC == 0 & intNMC > 0)
                                    {
                                        intNDC = intNMC;
                                    }

                                    // This deals with the extra character NERC inserts into the Net Generation
                                    // data field when they send ASCII files back

                                    strTemp1 = strRecord.Substring(38 + nFormat, 7);
                                    strTemp2 = strTemp1.Substring(strTemp1.Length - 1, 1).ToUpper();

                                    //								if ((strTemp2.CompareTo("A") >= 0 & strTemp2.CompareTo("R") <= 0 ) || strTemp2 == "{" || strTemp2 == "}")

                                    if (d_regex.IsMatch(strTemp2))
                                        intNAG = Convert.ToInt32(Decode(strTemp1));
                                    else
                                        intNAG = Convert.ToInt32(CheckForEmpty(strTemp1));

                                    intTypicalUnitLoading = Convert.ToByte(CheckForEmpty(strRecord.Substring(45 + nFormat, 1)));
                                    strTULDescription = strRecord.Substring(55 + nFormat, 25);
                                    strTULDescription = strTULDescription.Replace("'", " ");
                                    strTULDescription = strTULDescription.Replace(",", " ");

                                    if (intTypicalUnitLoading == 6 & strTULDescription.Trim() == string.Empty)
                                    {
                                        // Added for GADS OS Portal Changes
                                        strTULDescription = "NA (GADS OS)";
                                    }

                                    intAttemptedStarts = Convert.ToInt32(CheckForEmpty(strRecord.Substring(46 + nFormat, 3)));
                                    intActualStarts = Convert.ToInt32(CheckForEmpty(strRecord.Substring(49 + nFormat, 3)));
                                }
                                else if (strCardType == "05")
                                {
                                    //fpGMC = Convert.ToDecimal(CheckForEmpty(strRecord.Substring(15, 6))) / 100;
                                    //fpGDC = Convert.ToDecimal(CheckForEmpty(strRecord.Substring(21, 6))) / 100;
                                    //fpGAG = Convert.ToDecimal(CheckForEmpty(strRecord.Substring(27, 9))) / 100;
                                    fpGMC = 0M;
                                    fpGDC = 0M;
                                    fpGAG = 0M;
                                    fpNMC = Convert.ToDecimal(CheckForEmpty(strRecord.Substring(36, 6))) / 100;
                                    fpNDC = Convert.ToDecimal(CheckForEmpty(strRecord.Substring(42, 6))) / 100;

                                    //if (fpGMC == 0 & fpGDC > 0)
                                    //{
                                    //    fpGMC = fpGDC;
                                    //}
                                    //else if (fpGDC == 0 & fpGMC > 0)
                                    //{
                                    //    fpGDC = fpGMC;
                                    //}

                                    if (fpNMC == 0 & fpNDC > 0)
                                    {
                                        fpNMC = fpNDC;
                                    }
                                    else if (fpNDC == 0 & fpNMC > 0)
                                    {
                                        fpNDC = fpNMC;
                                    }

                                    // This deals with the extra character NERC inserts into the Net Generation
                                    // data field when they send ASCII files back

                                    strTemp1 = strRecord.Substring(48, 9);
                                    strTemp2 = strTemp1.Substring(strTemp1.Length - 1, 1).ToUpper();

                                    //  if ((strTemp2.CompareTo("A") >= 0 & strTemp2.CompareTo("R") <= 0 ) || strTemp2 == "{" || strTemp2 == "}")

                                    if (d_regex.IsMatch(strTemp2))
                                        fpNAG = Convert.ToDecimal(Decode(strTemp1));
                                    else
                                        fpNAG = Convert.ToDecimal(CheckForEmpty(strTemp1));

                                    fpNAG = fpNAG / 100;

                                    intTypicalUnitLoading = Convert.ToByte(CheckForEmpty(strRecord.Substring(57, 1)));
                                    strTULDescription = strRecord.Substring(98, 25);
                                    strTULDescription = strTULDescription.Replace("'", " ");
                                    strTULDescription = strTULDescription.Replace(",", " ");

                                    if (intTypicalUnitLoading == 6 & strTULDescription.Trim() == string.Empty)
                                    {
                                        // Added for GADS OS Portal Changes
                                        strTULDescription = "NA (GADS OS)";
                                    }

                                    intAttemptedStarts = Convert.ToInt32(CheckForEmpty(strRecord.Substring(58, 3)));
                                    intActualStarts = Convert.ToInt32(CheckForEmpty(strRecord.Substring(61, 3)));
                                }

                                // Performance 01 card loading to the dataset
                                rowPerformance = tblPerformance.FindByUtilityUnitCodeYearPeriod(strUtilityUnitCode, intReportingYear, strPeriod);

                                lRowNew = false;

                                if (rowPerformance == null)
                                {
                                    // if NOT, create a new datarow
                                    rowPerformance = tblPerformance.NewPerformanceDataRow();
                                    rowPerformance.RevisionCard1 = "0";
                                    rowPerformance.RevMonthCard1 = MyUTCDateTime;
                                    rowPerformance.RevisionCard2 = "*";
                                    rowPerformance.RevMonthCard2 = MyUTCDateTime;
                                    rowPerformance.ServiceHours = 0;
                                    rowPerformance.RSHours = 0;
                                    rowPerformance.PumpingHours = 0;
                                    rowPerformance.SynchCondHours = 0;
                                    rowPerformance.PlannedOutageHours = 0;
                                    rowPerformance.ForcedOutageHours = 0;
                                    rowPerformance.MaintOutageHours = 0;
                                    rowPerformance.ExtofSchedOutages = 0;
                                    rowPerformance.PeriodHours = 0;
                                    rowPerformance.InactiveHours = 0;
                                    rowPerformance.PriFuelCode = "  ";
                                    rowPerformance.RevisionCard3 = "*";
                                    rowPerformance.RevMonthCard3 = MyUTCDateTime;
                                    rowPerformance.RevisionCard4 = "*";
                                    rowPerformance.RevMonthCard4 = MyUTCDateTime;
                                    lRowNew = true;
                                }

                                rowPerformance.UtilityUnitCode = strUtilityUnitCode;
                                rowPerformance.Year = intReportingYear;
                                rowPerformance.Period = strPeriod;

                                if (String.Compare(strRevisionCode, rowPerformance.RevisionCard1) >= 0)
                                {
                                    rowPerformance.RevisionCard1 = strRevisionCode;
                                    rowPerformance.RevMonthCard1 = MyUTCDateTime;
                                    if (strCardType == "95")
                                    {
                                        rowPerformance.GrossMaxCap = Convert.ToDecimal(intGMC);
                                        rowPerformance.GrossDepCap = Convert.ToDecimal(intGDC);
                                        rowPerformance.GrossGen = Convert.ToDecimal(intGAG);
                                        rowPerformance.NetMaxCap = Convert.ToDecimal(intNMC);
                                        rowPerformance.NetDepCap = Convert.ToDecimal(intNDC);
                                        rowPerformance.NetGen = Convert.ToDecimal(intNAG);
                                    }
                                    else if (strCardType == "05")
                                    {
                                        rowPerformance.GrossMaxCap = fpGMC;
                                        rowPerformance.GrossDepCap = fpGDC;
                                        rowPerformance.GrossGen = fpGAG;
                                        rowPerformance.NetMaxCap = fpNMC;
                                        rowPerformance.NetDepCap = fpNDC;
                                        rowPerformance.NetGen = fpNAG;
                                    }
                                    rowPerformance.TypUnitLoading = intTypicalUnitLoading;
                                    rowPerformance.AttemptedStarts = Convert.ToInt16(intAttemptedStarts);
                                    rowPerformance.ActualStarts = Convert.ToInt16(intActualStarts);
                                    rowPerformance.VerbalDesc = strTULDescription;
                                }

                                if (lRowNew)
                                {
                                    tblPerformance.AddPerformanceDataRow(rowPerformance);
                                }

                                dsPerformance.AcceptChanges();

                            }
                            else if (strRecord.EndsWith("02") & strRecord.Length >= 80)
                            {
                                // Performance 02 card
                                if (strCardType == "95")
                                {
                                    intUnitServiceHours = Convert.ToInt32(CheckForEmpty(strRecord.Substring(15 + nFormat, 4)));
                                    intRSHours = Convert.ToInt32(CheckForEmpty(strRecord.Substring(19 + nFormat, 4)));
                                    intPumpingHours = Convert.ToInt32(CheckForEmpty(strRecord.Substring(23 + nFormat, 4)));
                                    intSynchCondensingHours = Convert.ToInt32(CheckForEmpty(strRecord.Substring(27 + nFormat, 4)));
                                    intAvailableHours = Convert.ToInt32(CheckForEmpty(strRecord.Substring(31 + nFormat, 4)));
                                    intPlannedOutageHours = Convert.ToInt32(CheckForEmpty(strRecord.Substring(35 + nFormat, 4)));
                                    intForcedOutageHours = Convert.ToInt32(CheckForEmpty(strRecord.Substring(39 + nFormat, 4)));
                                    intMaintenanceOutageHours = Convert.ToInt32(CheckForEmpty(strRecord.Substring(43 + nFormat, 4)));
                                    intScheduledOutageExtensions = Convert.ToInt32(CheckForEmpty(strRecord.Substring(47 + nFormat, 4)));
                                    intUnavailableHours = Convert.ToInt32(CheckForEmpty(strRecord.Substring(51 + nFormat, 4)));
                                    intPeriodHours = Convert.ToInt32(CheckForEmpty(strRecord.Substring(55 + nFormat, 4)));
                                    intInactiveHours = Convert.ToInt32(CheckForEmpty(strRecord.Substring(59 + nFormat, 4)));
                                }
                                else if (strCardType == "05")
                                {
                                    fpUnitServiceHours = Convert.ToDecimal(CheckForEmpty(strRecord.Substring(15, 5))) / 100;
                                    fpRSHours = Convert.ToDecimal(CheckForEmpty(strRecord.Substring(20, 5))) / 100;
                                    fpPumpingHours = Convert.ToDecimal(CheckForEmpty(strRecord.Substring(25, 5))) / 100;
                                    fpSynchCondensingHours = Convert.ToDecimal(CheckForEmpty(strRecord.Substring(30, 5))) / 100;
                                    fpAvailableHours = Convert.ToDecimal(CheckForEmpty(strRecord.Substring(35, 5))) / 100;
                                    fpPlannedOutageHours = Convert.ToDecimal(CheckForEmpty(strRecord.Substring(40, 5))) / 100;
                                    fpForcedOutageHours = Convert.ToDecimal(CheckForEmpty(strRecord.Substring(45, 5))) / 100;
                                    fpMaintenanceOutageHours = Convert.ToDecimal(CheckForEmpty(strRecord.Substring(50, 5))) / 100;
                                    fpScheduledOutageExtensions = Convert.ToDecimal(CheckForEmpty(strRecord.Substring(55, 5))) / 100;
                                    fpUnavailableHours = Convert.ToDecimal(CheckForEmpty(strRecord.Substring(60, 5))) / 100;
                                    fpPeriodHours = Convert.ToDecimal(CheckForEmpty(strRecord.Substring(65, 5))) / 100;
                                    fpInactiveHours = Convert.ToDecimal(CheckForEmpty(strRecord.Substring(70, 5))) / 100;
                                }
                                // Performance 02 card loading to the dataset
                                rowPerformance = tblPerformance.FindByUtilityUnitCodeYearPeriod(strUtilityUnitCode, intReportingYear, strPeriod);

                                lRowNew = false;

                                if (rowPerformance == null)
                                {
                                    // if NOT, create a new datarow
                                    rowPerformance = tblPerformance.NewPerformanceDataRow();
                                    rowPerformance.RevisionCard1 = "*";
                                    rowPerformance.RevMonthCard1 = MyUTCDateTime;
                                    rowPerformance.RevisionCard2 = "0";
                                    rowPerformance.RevMonthCard2 = MyUTCDateTime;
                                    rowPerformance.ServiceHours = 0;
                                    rowPerformance.RSHours = 0;
                                    rowPerformance.PumpingHours = 0;
                                    rowPerformance.SynchCondHours = 0;
                                    rowPerformance.PlannedOutageHours = 0;
                                    rowPerformance.ForcedOutageHours = 0;
                                    rowPerformance.MaintOutageHours = 0;
                                    rowPerformance.ExtofSchedOutages = 0;
                                    rowPerformance.PeriodHours = 0;
                                    rowPerformance.InactiveHours = 0;
                                    rowPerformance.PriFuelCode = "  ";
                                    rowPerformance.RevisionCard3 = "*";
                                    rowPerformance.RevMonthCard3 = MyUTCDateTime;
                                    rowPerformance.RevisionCard4 = "*";
                                    rowPerformance.RevMonthCard4 = MyUTCDateTime;
                                    lRowNew = true;
                                }

                                rowPerformance.UtilityUnitCode = strUtilityUnitCode;
                                rowPerformance.Year = intReportingYear;
                                rowPerformance.Period = strPeriod;

                                if (String.Compare(strRevisionCode, rowPerformance.RevisionCard2) >= 0)
                                {
                                    if (strCardType == "95")
                                    {
                                        rowPerformance.RevisionCard2 = strRevisionCode;
                                        rowPerformance.RevMonthCard2 = MyUTCDateTime;
                                        rowPerformance.ServiceHours = Convert.ToDecimal(intUnitServiceHours);
                                        rowPerformance.RSHours = Convert.ToDecimal(intRSHours);
                                        rowPerformance.PumpingHours = Convert.ToDecimal(intPumpingHours);
                                        rowPerformance.SynchCondHours = Convert.ToDecimal(intSynchCondensingHours);
                                        rowPerformance.PlannedOutageHours = Convert.ToDecimal(intPlannedOutageHours);
                                        rowPerformance.ForcedOutageHours = Convert.ToDecimal(intForcedOutageHours);
                                        rowPerformance.MaintOutageHours = Convert.ToDecimal(intMaintenanceOutageHours);
                                        rowPerformance.ExtofSchedOutages = Convert.ToDecimal(intScheduledOutageExtensions);
                                        rowPerformance.PeriodHours = System.Math.Round(Convert.ToDecimal(intPeriodHours) + Convert.ToDecimal(intInactiveHours), 0);
                                        rowPerformance.InactiveHours = Convert.ToDecimal(intInactiveHours);
                                    }
                                    else if (strCardType == "05")
                                    {
                                        rowPerformance.RevisionCard2 = strRevisionCode;
                                        rowPerformance.RevMonthCard2 = MyUTCDateTime;
                                        rowPerformance.ServiceHours = fpUnitServiceHours;
                                        rowPerformance.RSHours = fpRSHours;
                                        rowPerformance.PumpingHours = fpPumpingHours;
                                        rowPerformance.SynchCondHours = fpSynchCondensingHours;
                                        rowPerformance.PlannedOutageHours = fpPlannedOutageHours;
                                        rowPerformance.ForcedOutageHours = fpForcedOutageHours;
                                        rowPerformance.MaintOutageHours = fpMaintenanceOutageHours;
                                        rowPerformance.ExtofSchedOutages = fpScheduledOutageExtensions;
                                        rowPerformance.PeriodHours = System.Math.Round(fpPeriodHours + fpInactiveHours, 0);
                                        rowPerformance.InactiveHours = fpInactiveHours;
                                    }

                                    decimal mySH = System.Math.Round((rowPerformance.PeriodHours - (rowPerformance.RSHours + rowPerformance.PumpingHours 
                                        + rowPerformance.SynchCondHours + rowPerformance.PlannedOutageHours + rowPerformance.ForcedOutageHours 
                                        + rowPerformance.MaintOutageHours + rowPerformance.ExtofSchedOutages)),2);

                                    if (System.Math.Abs(System.Math.Round((rowPerformance.ServiceHours - mySH),2)) > 0.00M)
                                    {
                                        // J. Pratico Wed 8/13/2014 4:54 PM email
                                        //When it comes down to EFORd calculations, the fact that these number don�t total to the PERIOD HOURS isn�t critical.  
                                        //I believe our software pulls the critical statistics from the events.  Would it be possible to allow the totals for (AH, UAH, PH) 
                                        //to be off by 0.01 hours and not cause an error in the software? This MP sends in data for 53 individual units.
                                        //The PORTAL found more than 75 errors of this type in their data.
                                        if (System.Math.Abs(rowPerformance.ServiceHours - mySH) <= 0.0149M)
                                        {
                                            rowPerformance.ServiceHours =  mySH;
                                        }
                                    }
                                }

                                if (lRowNew)
                                {
                                    tblPerformance.AddPerformanceDataRow(rowPerformance);
                                }

                                dsPerformance.AcceptChanges();
                            }
                            else if (strRecord.EndsWith("03") & strRecord.Length >= 80)
                            {
                                // Performance 03 card
                                strPrimaryFuelCode = strRecord.Substring(15 + nFormat, 2).ToUpper();
                                //dblPrimaryQuantityBurned = 0.01 * Convert.ToDouble(CheckForEmpty(strRecord.Substring(17 + nFormat, 7)));
                                //intPrimaryHeatContent = Convert.ToInt32(CheckForEmpty(strRecord.Substring(24 + nFormat, 6)));
                                //fpPrimaryAsh = 0.1F * Convert.ToSingle(CheckForEmpty(strRecord.Substring(30 + nFormat, 3)));
                                //fpPrimaryMoisture = 0.1F * Convert.ToSingle(CheckForEmpty(strRecord.Substring(33 + nFormat, 3)));
                                //fpPrimarySulfur = 0.1F * Convert.ToSingle(CheckForEmpty(strRecord.Substring(36 + nFormat, 2)));
                                //fpPrimaryAlkalines = 0.1F * Convert.ToSingle(CheckForEmpty(strRecord.Substring(38 + nFormat, 3)));
                                //dblPrimaryQuantityBurned = 0.0;
                                //intPrimaryHeatContent = 0;
                                //fpPrimaryAsh = 0F;
                                //fpPrimaryMoisture = 0F;
                                //fpPrimarySulfur = 0F;
                                //fpPrimaryAlkalines = 0F;
                                //fpPrimaryGIVNP = 0F;
                                //if (strPrimaryFuelCode == "CC" || strPrimaryFuelCode == "LI")
                                //{
                                //    fpPrimaryGIVNP = Convert.ToSingle(CheckForEmpty(strRecord.Substring(41 + nFormat, 3)));
                                //}
                                //else if (strPrimaryFuelCode == "OO" || strPrimaryFuelCode == "KE" || strPrimaryFuelCode == "JP" || strPrimaryFuelCode == "DI")
                                //{
                                //    fpPrimaryGIVNP = 0.1F * Convert.ToSingle(CheckForEmpty(strRecord.Substring(41 + nFormat, 3)));
                                //}

                                //intPrimaryAshSofteningTemp = Convert.ToInt32(CheckForEmpty(strRecord.Substring(44 + nFormat, 4)));

                                //douBtus1 = CalcBtus(strPrimaryFuelCode, dblPrimaryQuantityBurned, intPrimaryHeatContent);
                                //intPrimaryAshSofteningTemp = 0;

                                //douBtus1 = 0;

                                //if (strCardType == "95")
                                //{
                                //    strSecondaryFuelCode = strRecord.Substring(48 + nFormat, 2).ToUpper();
                                //    dblSecondaryQuantityBurned = 0.01 * Convert.ToDouble(CheckForEmpty(strRecord.Substring(50 + nFormat, 6)));
                                //    intSecondaryHeatContent = Convert.ToInt32(CheckForEmpty(strRecord.Substring(56 + nFormat, 6)));
                                //    fpSecondaryAsh = 0.1F * Convert.ToSingle(CheckForEmpty(strRecord.Substring(62 + nFormat, 3)));
                                //    fpSecondaryMoisture = 0.1F * Convert.ToSingle(CheckForEmpty(strRecord.Substring(65 + nFormat, 3)));
                                //    fpSecondarySulfur = 0.1F * Convert.ToSingle(CheckForEmpty(strRecord.Substring(68 + nFormat, 2)));
                                //    fpSecondaryAlkalines = 0.1F * Convert.ToSingle(CheckForEmpty(strRecord.Substring(70 + nFormat, 3)));
                                //    fpSecondaryGIVNP = Convert.ToSingle(CheckForEmpty(strRecord.Substring(73 + nFormat, 3)));
                                //    intSecondaryAshSofteningTemp = Convert.ToInt32(CheckForEmpty(strRecord.Substring(76 + nFormat, 4)));

                                //    douBtus2 = CalcBtus(strSecondaryFuelCode, dblSecondaryQuantityBurned, intSecondaryHeatContent);
                                //}
                                //else if (strCardType == "05")
                                //{
                                //    strSecondaryFuelCode = strRecord.Substring(69, 2).ToUpper();
                                //    dblSecondaryQuantityBurned = 0.01 * Convert.ToDouble(CheckForEmpty(strRecord.Substring(71, 7)));
                                //    intSecondaryHeatContent = Convert.ToInt32(CheckForEmpty(strRecord.Substring(78, 6)));
                                //    fpSecondaryAsh = 0.1F * Convert.ToSingle(CheckForEmpty(strRecord.Substring(84, 3)));
                                //    fpSecondaryMoisture = 0.1F * Convert.ToSingle(CheckForEmpty(strRecord.Substring(87, 3)));
                                //    fpSecondarySulfur = 0.1F * Convert.ToSingle(CheckForEmpty(strRecord.Substring(90, 2)));
                                //    fpSecondaryAlkalines = 0.1F * Convert.ToSingle(CheckForEmpty(strRecord.Substring(92, 3)));
                                //    fpSecondaryGIVNP = Convert.ToSingle(CheckForEmpty(strRecord.Substring(95, 3)));
                                //    intSecondaryAshSofteningTemp = Convert.ToInt32(CheckForEmpty(strRecord.Substring(98, 4)));

                                //    douBtus2 = CalcBtus(strSecondaryFuelCode, dblSecondaryQuantityBurned, intSecondaryHeatContent);
                                //}

                                //if (strSecondaryFuelCode == "CC" || strSecondaryFuelCode == "LI")
                                //{
                                //    //fpSecondaryGIVNP =        fpSecondaryGIVNP;
                                //}
                                //else if (strSecondaryFuelCode == "OO" || strSecondaryFuelCode == "KE" || strSecondaryFuelCode == "JP" || strSecondaryFuelCode == "DI")
                                //{
                                //    fpSecondaryGIVNP = 0.1F * fpSecondaryGIVNP;
                                //}

                                // Performance 03 card loading to the dataset
                                rowPerformance = tblPerformance.FindByUtilityUnitCodeYearPeriod(strUtilityUnitCode, intReportingYear, strPeriod);

                                lRowNew = false;

                                if (rowPerformance == null)
                                {
                                    // if NOT, create a new datarow
                                    rowPerformance = tblPerformance.NewPerformanceDataRow();
                                    rowPerformance.RevisionCard1 = "*";
                                    rowPerformance.RevMonthCard1 = MyUTCDateTime;
                                    rowPerformance.RevisionCard2 = "*";
                                    rowPerformance.RevMonthCard2 = MyUTCDateTime;
                                    rowPerformance.ServiceHours = 0;
                                    rowPerformance.RSHours = 0;
                                    rowPerformance.PumpingHours = 0;
                                    rowPerformance.SynchCondHours = 0;
                                    rowPerformance.PlannedOutageHours = 0;
                                    rowPerformance.ForcedOutageHours = 0;
                                    rowPerformance.MaintOutageHours = 0;
                                    rowPerformance.ExtofSchedOutages = 0;
                                    rowPerformance.PeriodHours = 0;
                                    rowPerformance.InactiveHours = 0;
                                    rowPerformance.PriFuelCode = "  ";
                                    rowPerformance.RevisionCard3 = "0";
                                    rowPerformance.RevMonthCard3 = MyUTCDateTime;
                                    rowPerformance.RevisionCard4 = "*";
                                    rowPerformance.RevMonthCard4 = MyUTCDateTime;
                                    lRowNew = true;
                                }

                                rowPerformance.UtilityUnitCode = strUtilityUnitCode;
                                rowPerformance.Year = intReportingYear;
                                rowPerformance.Period = strPeriod;

                                if (String.Compare(strRevisionCode, rowPerformance.RevisionCard3) >= 0)
                                {
                                    rowPerformance.RevisionCard3 = strRevisionCode;
                                    rowPerformance.RevMonthCard3 = MyUTCDateTime; ;

                                    rowPerformance.PriFuelCode = strPrimaryFuelCode;
                                    //rowPerformance.PriQtyBurned = Convert.ToDecimal(dblPrimaryQuantityBurned);
                                    //rowPerformance.PriAvgHeatContent = intPrimaryHeatContent;
                                    //rowPerformance.PriBtus = Convert.ToDecimal(douBtus1);
                                    //rowPerformance.PriPercentAsh = Convert.ToDecimal(fpPrimaryAsh);
                                    //rowPerformance.PriPercentMoisture = Convert.ToDecimal(fpPrimaryMoisture);
                                    //rowPerformance.PriPercentSulfur = Convert.ToDecimal(fpPrimarySulfur);
                                    //rowPerformance.PriPercentAlkalines = Convert.ToDecimal(fpPrimaryAlkalines);
                                    //rowPerformance.PriGrindIndexVanad = Convert.ToDecimal(fpPrimaryGIVNP);
                                    //rowPerformance.PriAshSoftTemp = Convert.ToInt16(intPrimaryAshSofteningTemp);

                                    //rowPerformance.SecFuelCode = strSecondaryFuelCode;
                                    //rowPerformance.SecQtyBurned = Convert.ToDecimal(dblSecondaryQuantityBurned);
                                    //rowPerformance.SecAvgHeatContent = intSecondaryHeatContent;
                                    //rowPerformance.SecBtus = Convert.ToDecimal(douBtus2);
                                    //rowPerformance.SecPercentAsh = Convert.ToDecimal(fpSecondaryAsh);
                                    //rowPerformance.SecPercentMoisture = Convert.ToDecimal(fpSecondaryMoisture);
                                    //rowPerformance.SecPercentSulfur = Convert.ToDecimal(fpSecondarySulfur);
                                    //rowPerformance.SecPercentAlkalines = Convert.ToDecimal(fpSecondaryAlkalines);
                                    //rowPerformance.SecGrindIndexVanad = Convert.ToDecimal(fpSecondaryGIVNP);
                                    //rowPerformance.SecAshSoftTemp = Convert.ToInt16(intSecondaryAshSofteningTemp);
                                    rowPerformance.PriQtyBurned = 0;
                                    rowPerformance.PriAvgHeatContent = 0;
                                    rowPerformance.PriBtus = 0;
                                    rowPerformance.PriPercentAsh = 0;
                                    rowPerformance.PriPercentMoisture = 0;
                                    rowPerformance.PriPercentSulfur = 0;
                                    rowPerformance.PriPercentAlkalines = 0;
                                    rowPerformance.PriGrindIndexVanad = 0;
                                    rowPerformance.PriAshSoftTemp = 0;

                                    rowPerformance.SecFuelCode = "";
                                    rowPerformance.SecQtyBurned = 0;
                                    rowPerformance.SecAvgHeatContent = 0;
                                    rowPerformance.SecBtus = 0;
                                    rowPerformance.SecPercentAsh = 0;
                                    rowPerformance.SecPercentMoisture = 0;
                                    rowPerformance.SecPercentSulfur = 0;
                                    rowPerformance.SecPercentAlkalines = 0;
                                    rowPerformance.SecGrindIndexVanad = 0;
                                    rowPerformance.SecAshSoftTemp = 0;
                                }

                                if (lRowNew)
                                {
                                    tblPerformance.AddPerformanceDataRow(rowPerformance);
                                }

                                dsPerformance.AcceptChanges();

                            }
                            else if (strRecord.EndsWith("04") & strRecord.Length >= 80)
                            {
                                //// Performance 04 card
                                //strTertiaryFuelCode = strRecord.Substring(15 + nFormat, 2).ToUpper();
                                //dblTertiaryQuantityBurned = 0.01 * Convert.ToDouble(CheckForEmpty(strRecord.Substring(17 + nFormat, 7)));
                                //intTertiaryHeatContent = Convert.ToInt32(CheckForEmpty(strRecord.Substring(24 + nFormat, 6)));
                                //fpTertiaryAsh = 0.1F * Convert.ToSingle(CheckForEmpty(strRecord.Substring(30 + nFormat, 3)));
                                //fpTertiaryMoisture = 0.1F * Convert.ToSingle(CheckForEmpty(strRecord.Substring(33 + nFormat, 3)));
                                //fpTertiarySulfur = 0.1F * Convert.ToSingle(CheckForEmpty(strRecord.Substring(36 + nFormat, 2)));
                                //fpTertiaryAlkalines = 0.1F * Convert.ToSingle(CheckForEmpty(strRecord.Substring(38 + nFormat, 3)));

                                //if (strTertiaryFuelCode == "CC" || strTertiaryFuelCode == "LI")
                                //{
                                //    fpTertiaryGIVNP = Convert.ToSingle(CheckForEmpty(strRecord.Substring(41 + nFormat, 3)));
                                //}
                                //else if (strTertiaryFuelCode == "OO" || strTertiaryFuelCode == "KE" || strTertiaryFuelCode == "JP" || strTertiaryFuelCode == "DI")
                                //{
                                //    fpTertiaryGIVNP = 0.1F * Convert.ToSingle(CheckForEmpty(strRecord.Substring(41 + nFormat, 3)));
                                //}

                                //intTertiaryAshSofteningTemp = Convert.ToInt32(CheckForEmpty(strRecord.Substring(44 + nFormat, 4)));

                                //douBtus1 = CalcBtus(strTertiaryFuelCode, dblTertiaryQuantityBurned, intTertiaryHeatContent);

                                //if (strCardType == "95")
                                //{
                                //    strQuaternaryFuelCode = strRecord.Substring(48 + nFormat, 2).ToUpper();
                                //    dblQuaternaryQuantityBurned = 0.01 * Convert.ToDouble(CheckForEmpty(strRecord.Substring(50 + nFormat, 6)));
                                //    intQuaternaryHeatContent = Convert.ToInt32(CheckForEmpty(strRecord.Substring(56 + nFormat, 6)));
                                //    fpQuaternaryAsh = 0.1F * Convert.ToSingle(CheckForEmpty(strRecord.Substring(62 + nFormat, 3)));
                                //    fpQuaternaryMoisture = 0.1F * Convert.ToSingle(CheckForEmpty(strRecord.Substring(65 + nFormat, 3)));
                                //    fpQuaternarySulfur = 0.1F * Convert.ToSingle(CheckForEmpty(strRecord.Substring(68 + nFormat, 2)));
                                //    fpQuaternaryAlkalines = 0.1F * Convert.ToSingle(CheckForEmpty(strRecord.Substring(70 + nFormat, 3)));
                                //    fpQuaternaryGIVNP = Convert.ToSingle(CheckForEmpty(strRecord.Substring(73 + nFormat, 3)));
                                //    intQuaternaryAshSofteningTemp = Convert.ToInt32(CheckForEmpty(strRecord.Substring(76 + nFormat, 4)));

                                //    douBtus2 = CalcBtus(strQuaternaryFuelCode, dblQuaternaryQuantityBurned, intQuaternaryHeatContent);
                                //}
                                //else if (strCardType == "05")
                                //{
                                //    strQuaternaryFuelCode = strRecord.Substring(69, 2).ToUpper();
                                //    dblQuaternaryQuantityBurned = 0.01 * Convert.ToDouble(CheckForEmpty(strRecord.Substring(71, 7)));
                                //    intQuaternaryHeatContent = Convert.ToInt32(CheckForEmpty(strRecord.Substring(78, 6)));
                                //    fpQuaternaryAsh = 0.1F * Convert.ToSingle(CheckForEmpty(strRecord.Substring(84, 3)));
                                //    fpQuaternaryMoisture = 0.1F * Convert.ToSingle(CheckForEmpty(strRecord.Substring(87, 3)));
                                //    fpQuaternarySulfur = 0.1F * Convert.ToSingle(CheckForEmpty(strRecord.Substring(90, 2)));
                                //    fpQuaternaryAlkalines = 0.1F * Convert.ToSingle(CheckForEmpty(strRecord.Substring(92, 3)));
                                //    fpQuaternaryGIVNP = Convert.ToSingle(CheckForEmpty(strRecord.Substring(95, 3)));
                                //    intQuaternaryAshSofteningTemp = Convert.ToInt32(CheckForEmpty(strRecord.Substring(98, 4)));

                                //    douBtus2 = CalcBtus(strQuaternaryFuelCode, dblQuaternaryQuantityBurned, intQuaternaryHeatContent);
                                //}

                                //if (strQuaternaryFuelCode == "CC" || strQuaternaryFuelCode == "LI")
                                //{
                                //    //fpQuaternaryGIVNP =        fpQuaternaryGIVNP;
                                //}
                                //else if (strQuaternaryFuelCode == "OO" || strQuaternaryFuelCode == "KE" || strQuaternaryFuelCode == "JP" || strQuaternaryFuelCode == "DI")
                                //{
                                //    fpQuaternaryGIVNP = 0.1F * fpQuaternaryGIVNP;
                                //}

                                //// Performance 04 card loading to the dataset
                                //rowPerformance = tblPerformance.FindByUtilityUnitCodeYearPeriod(strUtilityUnitCode, intReportingYear, strPeriod);

                                //lRowNew = false;

                                //if (rowPerformance == null)
                                //{
                                //    // if NOT, create a new datarow
                                //    rowPerformance = tblPerformance.NewPerformanceDataRow();
                                //    rowPerformance.RevisionCard1 = "*";
                                //    rowPerformance.RevMonthCard1 = MyUTCDateTime;
                                //    rowPerformance.RevisionCard2 = "*";
                                //    rowPerformance.RevMonthCard2 = MyUTCDateTime;
                                //    rowPerformance.ServiceHours = 0;
                                //    rowPerformance.RSHours = 0;
                                //    rowPerformance.PumpingHours = 0;
                                //    rowPerformance.SynchCondHours = 0;
                                //    rowPerformance.PlannedOutageHours = 0;
                                //    rowPerformance.ForcedOutageHours = 0;
                                //    rowPerformance.MaintOutageHours = 0;
                                //    rowPerformance.ExtofSchedOutages = 0;
                                //    rowPerformance.PeriodHours = 0;
                                //    rowPerformance.InactiveHours = 0;
                                //    rowPerformance.PriFuelCode = "  ";
                                //    rowPerformance.RevisionCard3 = "*";
                                //    rowPerformance.RevMonthCard3 = MyUTCDateTime;
                                //    rowPerformance.RevisionCard4 = "0";
                                //    rowPerformance.RevMonthCard4 = MyUTCDateTime;
                                //    lRowNew = true;
                                //}

                                //rowPerformance.UtilityUnitCode = strUtilityUnitCode;
                                //rowPerformance.Year = intReportingYear;
                                //rowPerformance.Period = strPeriod;

                                //if (String.Compare(strRevisionCode, rowPerformance.RevisionCard4) >= 0)
                                //{
                                //    rowPerformance.RevisionCard4 = strRevisionCode;
                                //    rowPerformance.RevMonthCard4 = MyUTCDateTime;

                                //    rowPerformance.TerFuelCode = strTertiaryFuelCode;
                                //    rowPerformance.TerQtyBurned = Convert.ToDecimal(dblTertiaryQuantityBurned);
                                //    rowPerformance.TerAvgHeatContent = intTertiaryHeatContent;
                                //    rowPerformance.TerBtus = Convert.ToDecimal(douBtus1);
                                //    rowPerformance.TerPercentAsh = Convert.ToDecimal(fpTertiaryAsh);
                                //    rowPerformance.TerPercentMoisture = Convert.ToDecimal(fpTertiaryMoisture);
                                //    rowPerformance.TerPercentSulfur = Convert.ToDecimal(fpTertiarySulfur);
                                //    rowPerformance.TerPercentAlkalines = Convert.ToDecimal(fpTertiaryAlkalines);
                                //    rowPerformance.TerGrindIndexVanad = Convert.ToDecimal(fpTertiaryGIVNP);
                                //    rowPerformance.TerAshSoftTemp = Convert.ToInt16(intTertiaryAshSofteningTemp);

                                //    rowPerformance.QuaFuelCode = strQuaternaryFuelCode;
                                //    rowPerformance.QuaQtyBurned = Convert.ToDecimal(dblQuaternaryQuantityBurned);
                                //    rowPerformance.QuaAvgHeatContent = intQuaternaryHeatContent;
                                //    rowPerformance.QuaBtus = Convert.ToDecimal(douBtus2);
                                //    rowPerformance.QuaPercentAsh = Convert.ToDecimal(fpQuaternaryAsh);
                                //    rowPerformance.QuaPercentMoisture = Convert.ToDecimal(fpQuaternaryMoisture);
                                //    rowPerformance.QuaPercentSulfur = Convert.ToDecimal(fpQuaternarySulfur);
                                //    rowPerformance.QuaPercentAlkalines = Convert.ToDecimal(fpQuaternaryAlkalines);
                                //    rowPerformance.QuaGrindIndexVanad = Convert.ToDecimal(fpQuaternaryGIVNP);
                                //    rowPerformance.QuaAshSoftTemp = Convert.ToInt16(intQuaternaryAshSofteningTemp);
                                //}

                                //if (lRowNew)
                                //{
                                //    tblPerformance.AddPerformanceDataRow(rowPerformance);
                                //}

                                //dsPerformance.AcceptChanges();

                            }
                            else
                            {
                                // Performance record to DELETE ALL records for period

                                if (strRevisionCode.ToUpper() == "X")
                                {
                                    /*
                                     * ORIGINAL -
                                     * 
                                     * To delete a single card (other than 01 card), repeat columns 1-12 (80-column format)
                                     * as previously reported, enter an "X" in column 13 and enter the card number of the
                                     * record to be deleted in columns 79 and 80.
                                     * 
                                     * To delete an entire report, repeat columns 1-12 (80-column format) as previously reported
                                     * and enter an "X" in column 13.  Leave columns 79 and 80 blank.  ALL records (cards) for
                                     * that report period will be deleted.
                                     * 
                                     * October 2002 -
                                     * 
                                     * To delete data from one or more data fields, GADS recommends that you resubmit the entire
                                     * data set -- year-to-date -- for that unit (or all units you report) to GADS.  This 
                                     * procedure will insure that both you and the GADS database have the same records on file.
                                     * You have the option to find the record that has the highest revision code and then
                                     * increase this number by 1 or set all revision codes back to zero.
                                     * 
                                    */
                                    // Code to add only utility code, unit code, year, month, rev code X and period to dataset

                                    rowPerformance = tblPerformance.FindByUtilityUnitCodeYearPeriod(strUtilityUnitCode, intReportingYear, strPeriod);

                                    lRowNew = false;

                                    if (rowPerformance == null)
                                    {
                                        // if NOT, create a new datarow
                                        rowPerformance = tblPerformance.NewPerformanceDataRow();
                                        lRowNew = true;
                                        rowPerformance.UtilityUnitCode = strUtilityUnitCode;
                                        rowPerformance.Year = intReportingYear;
                                        rowPerformance.Period = strPeriod;
                                    }

                                    rowPerformance.RevisionCard1 = "X";
                                    rowPerformance.RevMonthCard1 = MyUTCDateTime;
                                    rowPerformance.GrossMaxCap = 0;
                                    rowPerformance.GrossDepCap = 0;
                                    rowPerformance.GrossGen = 0;
                                    rowPerformance.NetMaxCap = 0;
                                    rowPerformance.NetDepCap = 0;
                                    rowPerformance.NetGen = 0;
                                    rowPerformance.TypUnitLoading = 0;
                                    rowPerformance.AttemptedStarts = 0;
                                    rowPerformance.ActualStarts = 0;
                                    rowPerformance.VerbalDesc = "";
                                    rowPerformance.RevisionCard2 = "X";
                                    rowPerformance.RevMonthCard2 = MyUTCDateTime;
                                    rowPerformance.ServiceHours = 0;
                                    rowPerformance.RSHours = 0;
                                    rowPerformance.PumpingHours = 0;
                                    rowPerformance.SynchCondHours = 0;
                                    rowPerformance.PlannedOutageHours = 0;
                                    rowPerformance.ForcedOutageHours = 0;
                                    rowPerformance.MaintOutageHours = 0;
                                    rowPerformance.ExtofSchedOutages = 0;
                                    rowPerformance.PeriodHours = 0;
                                    rowPerformance.InactiveHours = 0;
                                    rowPerformance.PriFuelCode = "  ";
                                    rowPerformance.SecFuelCode = "  ";
                                    rowPerformance.TerFuelCode = "  ";
                                    rowPerformance.QuaFuelCode = "  ";
                                    rowPerformance.RevisionCard3 = "X";
                                    rowPerformance.RevMonthCard3 = MyUTCDateTime;
                                    rowPerformance.RevisionCard4 = "X";
                                    rowPerformance.RevMonthCard4 = MyUTCDateTime;

                                    if (lRowNew)
                                    {
                                        tblPerformance.AddPerformanceDataRow(rowPerformance);
                                    }

                                    dsPerformance.AcceptChanges();
                                }
                            }
                        }
                        #endregion

                        #region Event Record
                        else if (strCardType == "97" || strCardType == "07")
                        {
                            // Event Record

                            intRecordCode = Convert.ToByte(97);
                            //							intUtilityCode     = Convert.ToInt32(CheckForEmpty(strRecord.Substring(2,3)));
                            //							intUnitCode        = Convert.ToInt32(CheckForEmpty(strRecord.Substring(5,3)));
                            strUtilityUnitCode = strRecord.Substring(2, 6);

                            drSetup = dtSetup.FindByUtilityUnitCode(strUtilityUnitCode);

                            if (drSetup == null)
                            {
                                strUtilityUnitCode = "000000";
                            }

                            if (nFormat == -2)
                            {
                                // Below is 80-column data format
                                intReportingYear = Convert.ToInt16(CheckForEmpty(strRecord.Substring(8, 2)));

                                if (intReportingYear < 80)
                                {
                                    intReportingYear += 2000;
                                }
                                else
                                {
                                    intReportingYear += 1900;
                                }
                            }
                            else
                            {	// Below is 82-column data format
                                intReportingYear = Convert.ToInt16(CheckForEmpty(strRecord.Substring(8, 4)));
                            }

                            if (intReportingYear < YearLimit)
                            {
                                continue;
                            }

                            intEventNumber = Convert.ToInt16(CheckForEmpty(strRecord.Substring(12 + nFormat, 4)));

                            strRevisionCode = strRecord.Substring(16 + nFormat, 1);
                            strRevisionCode = strRevisionCode.Replace(' ', '0');

                            if (strRecord.Length != 17 && strRecord.Length != 15)
                            {
                                strEventType = strRecord.Substring(17 + nFormat, 2);

                                if (strEventType.Trim() == string.Empty)
                                {
                                    // if the Event Type is left blank, make it NC ... this was a problem at ISO-NE
                                    //                                    strEventType = "NC";
                                    strEventType = "XX";
                                    strRevisionCode = "X";
                                }
                            }

                            if (strRecord.Length == 17 || strRecord.Length == 15)
                            {
                                // Event Record to be deleted

                                /* Originally:
                                 * 
                                 * To delete a single card (other than 01), repeat columns 1-14 as previously reported,
                                 * enter an "X" in column 15 and enter the card number of the record to be deleted in
                                 * columns 79-80.
                                 * 
                                 * To delete an entire event, repeat columns 1-14 as previously reported and enter an 
                                 * "X" in column 15.  Leave columns 79 and 80 blank.  ALL records (cards) entered for
                                 * this event will be deleted.
                                 * 
                                 * October 2002 -
                                 * 
                                 * To delete data from one or more data fields, GADS recommends that you resubmit the entire
                                 * data set -- year-to-date -- for that unit (or all units you report) to GADS.  This 
                                 * procedure will insure that both you and the GADS database have the same records on file.
                                 * You have the option to find the record that has the highest revision code and then
                                 * increase this number by 1 or set all revision codes back to zero.
                                 * 
                                */

                                if (strRevisionCode.ToUpper() == "X")
                                {
                                    alDeleteEvent01.Add(
                                        "(UtilityUnitCode = '" + strUtilityUnitCode + "') AND " +
                                        "(Year = " + intReportingYear.ToString() + ") AND " +
                                        "(EventNumber = " + intEventNumber.ToString() + ")");
                                }

                            }
                            else
                            {
                                // full 80 or 82 column record

                                strEventType = strRecord.Substring(17 + nFormat, 2);

                                if (strEventType.Trim() == string.Empty)
                                {
                                    // if the Event Type is left blank, make it NC ... this was a problem at ISO-NE
                                    //                                    strEventType = "NC";
                                    strEventType = "XX";
                                    strRevisionCode = "X";
                                }

                                if (strRevisionCode.ToUpper() == "X")
                                {
                                    /* Originally:
                                     * 
                                     * To delete a single card (other than 01), repeat columns 1-14 as previously reported,
                                     * enter an "X" in column 15 and enter the card number of the record to be deleted in
                                     * columns 79-80.
                                     * 
                                     * To delete an entire event, repeat columns 1-14 as previously reported and enter an 
                                     * "X" in column 15.  Leave columns 79 and 80 blank.  ALL records (cards) entered for
                                     * this event will be deleted.
                                     * 
                                     */
                                    if (strRecord.EndsWith("01") || strRecord.EndsWith(" 1"))
                                    {
                                        // You cannot delete the single 01 card
                                    }
                                    else if (strRecord.EndsWith("02") || strRecord.EndsWith(" 2"))
                                    {
                                        alDeleteEvent02.Add(
                                            "(UtilityUnitCode = '" + strUtilityUnitCode + "') AND " +
                                            "(Year = " + intReportingYear.ToString() + ")  AND " +
                                            "(EventNumber = " + intEventNumber.ToString() + ")");
                                    }
                                    else if (strRecord.EndsWith("03") || strRecord.EndsWith(" 3"))
                                    {
                                        alDeleteEvent03.Add(
                                            "(UtilityUnitCode = '" + strUtilityUnitCode + "') AND " +
                                            "(Year = " + intReportingYear.ToString() + ")  AND " +
                                            "(EventNumber = " + intEventNumber.ToString() + ")");
                                    }
                                    else
                                    {
                                        strTemp1 = strRecord.Substring(strRecord.Length - 2, 2);
                                        strTemp1 = strTemp1.Replace(" ", "0");
                                        strCardNo = strTemp1;

                                        if (Even(Convert.ToInt16(strCardNo)))
                                        {
                                            // Even numbered Event card between 04 and 99
                                            alDeleteEvent04.Add(
                                                "(UtilityUnitCode = '" + strUtilityUnitCode + "') AND " +
                                                "(Year = " + intReportingYear.ToString() + ")  AND " +
                                                "(EventNumber = " + intEventNumber.ToString() + ")  AND " +
                                                "(EvenCardNumber = " + strCardNo + ")");
                                        }
                                        else
                                        {
                                            // Odd numbered Event card between 04 and 99

                                            strCardNo = Convert.ToString(Convert.ToInt16(strTemp1) - 1);

                                            alDeleteEvent05.Add(
                                                "(UtilityUnitCode = '" + strUtilityUnitCode + "') AND " +
                                                "(Year = " + intReportingYear.ToString() + ")  AND " +
                                                "(EventNumber = " + intEventNumber.ToString() + ")  AND " +
                                                "(EvenCardNumber = " + strCardNo + ")");
                                        }
                                    }
                                }
                                else if (strRecord.EndsWith("01") || strRecord.EndsWith(" 1"))
                                {
                                    // Event 01 record

                                    strStartOfEvent = strRecord.Substring(19 + nFormat, 8);
                                    if (strStartOfEvent.Trim() != "")
                                    {
                                        // The field has numbers in it so replace spaces with 0
                                        strStartOfEvent = strStartOfEvent.Replace(" ", "0");
                                    }

                                    if (intReportingYear >= 2012)
                                    {
                                        // OATI has put their own "key" in the 20 blank spaces - so ignore
                                        strFirstChangeOfEvent = string.Empty;
                                        strEventType1 = string.Empty;
                                        strSecondChangeOfEvent = string.Empty;
                                        strEventType2 = string.Empty;
                                    }
                                    else
                                    {
                                        strFirstChangeOfEvent = strRecord.Substring(27 + nFormat, 8);

                                        if (strFirstChangeOfEvent.CompareTo("00000000") == 0)
                                        {
                                            // The field is 0-filled so replace with blanks
                                            strFirstChangeOfEvent = "        ";
                                        }
                                        if (strFirstChangeOfEvent.Trim() != "")
                                        {
                                            // The field has numbers in it so replace spaces with 0
                                            strFirstChangeOfEvent = strFirstChangeOfEvent.Replace(" ", "0");
                                        }

                                        strEventType1 = strRecord.Substring(35 + nFormat, 2);

                                        strSecondChangeOfEvent = strRecord.Substring(37 + nFormat, 8);

                                        if (strSecondChangeOfEvent.CompareTo("00000000") == 0)
                                        {
                                            // The field is 0-filled so replace with blanks
                                            strSecondChangeOfEvent = "        ";
                                        }
                                        if (strSecondChangeOfEvent.Trim() != "")
                                        {
                                            // The field has numbers in it so replace spaces with 0
                                            strSecondChangeOfEvent = strSecondChangeOfEvent.Replace(" ", "0");
                                        }

                                        strEventType2 = strRecord.Substring(45 + nFormat, 2);
                                    }

                                    strEndOfEvent = strRecord.Substring(47 + nFormat, 8);

                                    if (strEndOfEvent.CompareTo("00000000") == 0)
                                    {
                                        // The field is 0-filled so replace with blanks
                                        strEndOfEvent = "        ";
                                    }
                                    if (strEndOfEvent.Trim() != "")
                                    {
                                        // The field has numbers in it so replace spaces with 0
                                        strEndOfEvent = strEndOfEvent.Replace(" ", "0");
                                    }

                                    if (strEndOfEvent.Trim() == "")
                                    {
                                        // Setting the Revision Month to the best estimate using the "end of event"

                                        if (strSecondChangeOfEvent.Trim() == "")
                                        {
                                            if (strFirstChangeOfEvent.Trim() == "")
                                            {
                                                intRevisionMonth = Convert.ToInt32(CheckForEmpty(strStartOfEvent.Substring(0, 2)));
                                            }
                                            else
                                            {
                                                intRevisionMonth = Convert.ToInt32(CheckForEmpty(strFirstChangeOfEvent.Substring(0, 2)));
                                            }
                                        }
                                        else
                                        {
                                            intRevisionMonth = Convert.ToInt32(CheckForEmpty(strSecondChangeOfEvent.Substring(0, 2)));
                                        }
                                    }
                                    else
                                    {
                                        intRevisionMonth = Convert.ToInt32(CheckForEmpty(strEndOfEvent.Substring(0, 2)));
                                    }

                                    if (strCardType == "97")
                                    {
                                        //intGAC = Convert.ToInt32(CheckForEmpty(strRecord.Substring(55 + nFormat, 4)));
                                        intGAC = 0;
                                        intNAC = Convert.ToInt32(CheckForEmpty(strRecord.Substring(59 + nFormat, 4)));
                                        //strDominantDerate = strRecord.Substring(64 + nFormat, 1);
                                        strDominantDerate = " ";
                                    }
                                    else if (strCardType == "07")
                                    {
                                        //fpGAC = Convert.ToDecimal(CheckForEmpty(strRecord.Substring(55, 6))) / 100;
                                        fpGAC = 0;
                                        fpNAC = Convert.ToDecimal(CheckForEmpty(strRecord.Substring(61, 6))) / 100;
                                        //strDominantDerate = strRecord.Substring(68, 1);
                                        strDominantDerate = " ";
                                    }
                                    // only Deratings and NCs are "allowed" to have non-zero ACs

                                    switch (strEventType)
                                    {
                                        case "U1":
                                            intGAC = 0;
                                            intNAC = 0;
                                            fpGAC = 0;
                                            fpNAC = 0;
                                            break;
                                        case "U2":
                                            intGAC = 0;
                                            intNAC = 0;
                                            fpGAC = 0;
                                            fpNAC = 0;
                                            break;
                                        case "U3":
                                            intGAC = 0;
                                            intNAC = 0;
                                            fpGAC = 0;
                                            fpNAC = 0;
                                            break;
                                        case "SF":
                                            intGAC = 0;
                                            intNAC = 0;
                                            fpGAC = 0;
                                            fpNAC = 0;
                                            break;
                                        case "PO":
                                            intGAC = 0;
                                            intNAC = 0;
                                            fpGAC = 0;
                                            fpNAC = 0;
                                            break;
                                        case "MO":
                                            intGAC = 0;
                                            intNAC = 0;
                                            fpGAC = 0;
                                            fpNAC = 0;
                                            break;
                                        case "SE":
                                            intGAC = 0;
                                            intNAC = 0;
                                            fpGAC = 0;
                                            fpNAC = 0;
                                            break;
                                        case "ME":
                                            intGAC = 0;
                                            intNAC = 0;
                                            fpGAC = 0;
                                            fpNAC = 0;
                                            break;
                                        case "PE":
                                            intGAC = 0;
                                            intNAC = 0;
                                            fpGAC = 0;
                                            fpNAC = 0;
                                            break;
                                        case "RS":
                                            intGAC = 0;
                                            intNAC = 0;
                                            fpGAC = 0;
                                            fpNAC = 0;
                                            break;
                                        case "PU":
                                            intGAC = 0;
                                            intNAC = 0;
                                            fpGAC = 0;
                                            fpNAC = 0;
                                            break;
                                        case "CO":
                                            intGAC = 0;
                                            intNAC = 0;
                                            fpGAC = 0;
                                            fpNAC = 0;
                                            break;
                                        case "IR":
                                            intGAC = 0;
                                            intNAC = 0;
                                            fpGAC = 0;
                                            fpNAC = 0;
                                            break;
                                        case "MB":
                                            intGAC = 0;
                                            intNAC = 0;
                                            fpGAC = 0;
                                            fpNAC = 0;
                                            break;
                                        case "RU":
                                            intGAC = 0;
                                            intNAC = 0;
                                            fpGAC = 0;
                                            fpNAC = 0;
                                            break;
                                        default:
                                            // either a derating or an NC
                                            break;
                                    }

                                    // Code to load Event 01 data to dataset

                                    // Check to see if this data is already in the dataset
                                    rowEvent01 = tblEvent01.FindByUtilityUnitCodeYearEventNumber(strUtilityUnitCode, intReportingYear, intEventNumber);

                                    lRowNew = false;

                                    if (rowEvent01 == null)
                                    {
                                        // if NOT, create a new datarow

                                        rowEvent01 = tblEvent01.NewEventData01Row();
                                        rowEvent01.CauseCode = 0;
                                        rowEvent01.CauseCodeExt = "  ";
                                        rowEvent01.ContribCode = 0;
                                        rowEvent01.VerbalDesc1 = "";
                                        rowEvent01.RevisionCard01 = "0";
                                        rowEvent01.RevisionCard02 = "*";
                                        rowEvent01.RevisionCard03 = "*";
                                        rowEvent01.RevMonthCard02 = MyUTCDateTime;
                                        rowEvent01.RevMonthCard03 = MyUTCDateTime;
                                        lRowNew = true;

                                    }

                                    rowEvent01.UtilityUnitCode = strUtilityUnitCode;
                                    rowEvent01.Year = intReportingYear;
                                    rowEvent01.EventNumber = intEventNumber;

                                    if (String.Compare(strRevisionCode, rowEvent01.RevisionCard01) >= 0)
                                    {
                                        rowEvent01.RevisionCard01 = strRevisionCode;
                                        rowEvent01.EventType = strEventType;

                                        if (dt_regex.IsMatch(strStartOfEvent))
                                        {
                                            // create the Start of Event date/time
                                            if (strStartOfEvent.Substring(4, 4) == "2400")
                                            {
                                                dateString = strStartOfEvent.Substring(0, 2) + @"/" + strStartOfEvent.Substring(2, 2) + @"/" + intReportingYear.ToString() +
                                                    " 23:59:59.999";
                                            }
                                            else
                                            {
                                                dateString = strStartOfEvent.Substring(0, 2) + @"/" + strStartOfEvent.Substring(2, 2) + @"/" + intReportingYear.ToString() + " " +
                                                    strStartOfEvent.Substring(4, 2) + ":" + strStartOfEvent.Substring(6, 2);
                                            }

                                            if (strStartOfEvent == "01010000")
                                            {
                                                rowEvent01.CarryOverLastYear = true;
                                            }
                                            else
                                            {
                                                rowEvent01.CarryOverLastYear = false;
                                            }

                                            MyDateTime = System.DateTime.Parse(dateString);
                                            rowEvent01.StartDateTime = MyDateTime;
                                            rowEvent01.RevMonthCard01 = MyUTCDateTime;

                                        }
                                        else
                                        {
                                            // create the Start of Event date/time
                                            if (strStartOfEvent.ToUpper() == "010100XX")
                                            {
                                                dateString = @"01/01/" + intReportingYear.ToString() + " 00:00";
                                                MyDateTime = System.DateTime.Parse(dateString);
                                                rowEvent01.StartDateTime = MyDateTime;
                                                rowEvent01.RevMonthCard01 = MyUTCDateTime;
                                                rowEvent01.CarryOverLastYear = true;
                                            }
                                            else if (strStartOfEvent.ToUpper().EndsWith("XX"))
                                            {
                                                try
                                                {
                                                    dateString = strStartOfEvent.Substring(0, 2) + @"/" + strStartOfEvent.Substring(2, 2) + @"/" + intReportingYear.ToString() + " " +
                                                        strStartOfEvent.Substring(4, 2) + ":00";

                                                    rowEvent01.CarryOverLastYear = false;

                                                    MyDateTime = System.DateTime.Parse(dateString);
                                                    rowEvent01.StartDateTime = MyDateTime;
                                                    rowEvent01.RevMonthCard01 = MyUTCDateTime;
                                                }
                                                catch
                                                {
                                                    rowEvent01.CarryOverLastYear = false;
                                                }

                                            }
                                            else
                                            {
                                                rowEvent01.CarryOverLastYear = false;
                                            }
                                        }

                                        if (strFirstChangeOfEvent != "00000000" && strFirstChangeOfEvent.Trim() != string.Empty)
                                        {
                                            if (dt_regex.IsMatch(strFirstChangeOfEvent))
                                            {
                                                // create the First Change of Event date/time
                                                if (strFirstChangeOfEvent.Substring(4, 4) == "2400")
                                                {
                                                    dateString = strFirstChangeOfEvent.Substring(0, 2) + @"/" + strFirstChangeOfEvent.Substring(2, 2) + @"/" + intReportingYear.ToString() +
                                                        " 23:59:59.999";
                                                }
                                                else
                                                {
                                                    dateString = strFirstChangeOfEvent.Substring(0, 2) + @"/" + strFirstChangeOfEvent.Substring(2, 2) + @"/" + intReportingYear.ToString() + " " +
                                                        strFirstChangeOfEvent.Substring(4, 2) + ":" + strFirstChangeOfEvent.Substring(6, 2);
                                                }

                                                MyDateTime = System.DateTime.Parse(dateString);
                                                rowEvent01.ChangeDateTime1 = MyDateTime;
                                                rowEvent01.RevMonthCard01 = MyUTCDateTime;
                                            }
                                        }
                                        rowEvent01.ChangeInEventType1 = strEventType1;

                                        if (strSecondChangeOfEvent != "00000000" && strSecondChangeOfEvent.Trim() != string.Empty)
                                        {
                                            if (dt_regex.IsMatch(strSecondChangeOfEvent))
                                            {
                                                // create the Second Change of Event date/time
                                                if (strSecondChangeOfEvent.Substring(4, 4) == "2400")
                                                {
                                                    dateString = strSecondChangeOfEvent.Substring(0, 2) + @"/" + strSecondChangeOfEvent.Substring(2, 2) + @"/" + intReportingYear.ToString() +
                                                        " 23:59:59.999";
                                                }
                                                else
                                                {
                                                    dateString = strSecondChangeOfEvent.Substring(0, 2) + @"/" + strSecondChangeOfEvent.Substring(2, 2) + @"/" + intReportingYear.ToString() + " " +
                                                        strSecondChangeOfEvent.Substring(4, 2) + ":" + strSecondChangeOfEvent.Substring(6, 2);
                                                }

                                                MyDateTime = System.DateTime.Parse(dateString);
                                                rowEvent01.ChangeDateTime2 = MyDateTime;
                                                rowEvent01.RevMonthCard01 = MyUTCDateTime;
                                            }
                                        }

                                        rowEvent01.ChangeInEventType2 = strEventType2;

                                        if (dt_regex.IsMatch(strEndOfEvent))
                                        {
                                            // create the End of Event date/time

                                            if (strEndOfEvent == "12312400")
                                            {
                                                dateString = @"12/31/" + intReportingYear.ToString() + " 23:59:59.999";
                                                rowEvent01.CarryOverNextYear = true;
                                            }
                                            else
                                            {
                                                if (strEndOfEvent.Substring(4, 4) == "2400")
                                                {
                                                    dateString = strEndOfEvent.Substring(0, 2) + @"/" + strEndOfEvent.Substring(2, 2) + @"/" + intReportingYear.ToString() +
                                                        " 23:59:59.999";
                                                }
                                                else
                                                {
                                                    dateString = strEndOfEvent.Substring(0, 2) + @"/" + strEndOfEvent.Substring(2, 2) + @"/" + intReportingYear.ToString() + " " +
                                                        strEndOfEvent.Substring(4, 2) + ":" + strEndOfEvent.Substring(6, 2);
                                                }

                                                rowEvent01.CarryOverNextYear = false;

                                            }

                                            MyDateTime = System.DateTime.Parse(dateString);
                                            if (MyDateTime < System.DateTime.Parse(System.DateTime.Now.Month.ToString() + "/01/" + System.DateTime.Now.Year.ToString()))
                                            { 
                                                rowEvent01.EndDateTime = MyDateTime;
                                            } 
                                            rowEvent01.RevMonthCard01 = MyUTCDateTime;

                                        }
                                        else
                                        {
                                            // Invalid EndOfEvent date/time -- could be 123124XX

                                            // create the End of Event date/time

                                            if (strEndOfEvent.ToUpper() == "123124XX")
                                            {
                                                dateString = @"12/31/" + intReportingYear.ToString() + " 23:59:59.999";
                                                MyDateTime = System.DateTime.Parse(dateString);
                                                if (MyDateTime < System.DateTime.Parse(System.DateTime.Now.Month.ToString() + "/01/" + System.DateTime.Now.Year.ToString()))
                                                {
                                                    rowEvent01.EndDateTime = MyDateTime;
                                                    rowEvent01.CarryOverNextYear = true;
                                                } 
                                                rowEvent01.RevMonthCard01 = MyUTCDateTime;                                               
                                            }
                                            else if (strEndOfEvent.ToUpper().EndsWith("24XX") && strEndOfEvent.StartsWith("12"))
                                            {
                                                // TXU had an end of event as 123024XX - I'm going to assume that 
                                                // anything that ends with 24XX and is in the month of December 
                                                // is intended to be the end of the year - 
                                                // why else would you put XX as the "Minutes"?

                                                try
                                                {
                                                    dateString = @"12/31/" + intReportingYear.ToString() + " 23:59:59.999";
                                                    MyDateTime = System.DateTime.Parse(dateString);
                                                    if (MyDateTime < System.DateTime.Parse(System.DateTime.Now.Month.ToString() + "/01/" + System.DateTime.Now.Year.ToString()))
                                                    {
                                                        rowEvent01.EndDateTime = MyDateTime;
                                                        rowEvent01.CarryOverNextYear = true;
                                                    } 
                                                    rowEvent01.RevMonthCard01 = MyUTCDateTime;                                                   
                                                }
                                                catch
                                                {
                                                    rowEvent01.CarryOverNextYear = false;
                                                }
                                            }
                                            else if (strEndOfEvent.ToUpper().EndsWith("24XX"))
                                            {
                                                // It is some month other than December with a 24XX as the time
                                                // Assume badly screwed up so change it to the correct date, but end it
                                                // at midnight

                                                try
                                                {
                                                    dateString = strEndOfEvent.Substring(0, 2) + @"/" + strEndOfEvent.Substring(2, 2) + @"/" + intReportingYear.ToString() +
                                                        " 23:59:59.999";

                                                    MyDateTime = System.DateTime.Parse(dateString);
                                                    if (MyDateTime < System.DateTime.Parse(System.DateTime.Now.Month.ToString() + "/01/" + System.DateTime.Now.Year.ToString()))
                                                    {
                                                        rowEvent01.EndDateTime = MyDateTime;
                                                    } 
                                                    rowEvent01.RevMonthCard01 = MyUTCDateTime;
                                                    rowEvent01.CarryOverNextYear = false;
                                                }
                                                catch
                                                {
                                                    rowEvent01.CarryOverNextYear = false;
                                                }
                                            }
                                            else
                                            {
                                                // NOTE:  if none of the above leave it blank
                                                rowEvent01.CarryOverNextYear = false;
                                            }
                                        }

                                        if (strCardType == "97")
                                        {
                                            rowEvent01.GrossAvailCapacity = Convert.ToDecimal(intGAC);
                                            rowEvent01.NetAvailCapacity = Convert.ToDecimal(intNAC);
                                        }
                                        else if (strCardType == "07")
                                        {
                                            rowEvent01.GrossAvailCapacity = fpGAC;
                                            rowEvent01.NetAvailCapacity = fpNAC;
                                        }

                                        if (strDominantDerate.ToUpper() == "D")
                                        {
                                            //rowEvent01.DominantDerate = true;
                                            rowEvent01.DominantDerate = false;
                                        }
                                        else
                                        {
                                            rowEvent01.DominantDerate = false;
                                        }
                                    }

                                    if (lRowNew)
                                    {
                                        tblEvent01.AddEventData01Row(rowEvent01);
                                    }

                                    dsEvent01.AcceptChanges();

                                }
                                else if (strRecord.EndsWith("02") || strRecord.EndsWith(" 2"))
                                {
                                    // Event 02 record
                                    intCauseCode = Convert.ToInt16(CheckForEmpty(strRecord.Substring(19 + nFormat, 4)));

                                    switch (strEventType)
                                    {
                                        case "IR":
                                            intCauseCode = 2;
                                            break;
                                        case "MB":
                                            intCauseCode = 9991;
                                            break;
                                        case "RU":
                                            intCauseCode = 9990;
                                            break;
                                        case "RS":
                                            intCauseCode = 0;
                                            break;
                                        case "CO":
                                            intCauseCode = 0;
                                            break;
                                        case "PU":
                                            intCauseCode = 0;
                                            break;
                                        default:
                                            // either a derating or an NC
                                            break;
                                    }

                                    strCauseCodeExt = strRecord.Substring(23 + nFormat, 2);
                                    strAmplificationCode = strRecord.Substring(23 + nFormat, 1);   // new field to mimic CEA code
                                    //strTimeWorkStarted = strRecord.Substring(25 + nFormat, 8);
                                    //strTimeWorkEnded = strRecord.Substring(33 + nFormat, 8);
                                    strTimeWorkStarted = "        ";
                                    strTimeWorkEnded = "        ";
                                    intEventContribCode = Convert.ToByte(CheckForEmpty(strRecord.Substring(43 + nFormat, 1)));
                                    if (intEventContribCode == 0)
                                    {
                                        intEventContribCode = 1;
                                    }
                                    //strPrimaryAlert = strRecord.Substring(44 + nFormat, 1);
                                    strPrimaryAlert = " ";

                                    //intManhoursWorked = Convert.ToInt16(CheckForEmpty(strRecord.Substring(45 + nFormat, 4)));
                                    intManhoursWorked = 0;

                                    //strVerbalDescription = strRecord.Substring(49 + nFormat, 31);  // used for both parts of the verbal description
                                    //strVerbalDescription = strVerbalDescription.Replace("'", " ");
                                    //strVerbalDescription = strVerbalDescription.Replace(",", " ");
                                    strVerbalDescription = "NA";

                                    //strFailureMech = strRecord.Substring(49 + nFormat, 4);  // Failure Mechanism -- ALL Even numbered records
                                    //strExpandedData = strRecord.Substring(49 + nFormat, 16);  // Expanded Data Reporting -- Contribution Code 1 ONLY
                                    strFailureMech = "";  // Failure Mechanism -- ALL Even numbered records
                                    strExpandedData = "";  // Expanded Data Reporting -- Contribution Code 1 ONLY

                                    // Code to load Event 02 card data to dataset

                                    // Check to see if this data is already in the dataset
                                    rowEvent01 = tblEvent01.FindByUtilityUnitCodeYearEventNumber(strUtilityUnitCode, intReportingYear, intEventNumber);

                                    lRowNew = false;

                                    if (rowEvent01 == null)
                                    {
                                        // if NOT, create a new datarow

                                        rowEvent01 = tblEvent01.NewEventData01Row();
                                        rowEvent01.StartDateTime = MyUTCDateTime;
                                        rowEvent01.CauseCode = 0;
                                        rowEvent01.CauseCodeExt = "  ";
                                        rowEvent01.ContribCode = 0;
                                        rowEvent01.VerbalDesc1 = "";
                                        rowEvent01.VerbalDesc86 = "";
                                        rowEvent01.RevisionCard01 = "*";
                                        rowEvent01.RevisionCard02 = "0";
                                        rowEvent01.RevisionCard03 = "*";
                                        rowEvent01.RevMonthCard01 = MyUTCDateTime;
                                        rowEvent01.RevMonthCard03 = MyUTCDateTime;
                                        lRowNew = true;

                                    }

                                    rowEvent01.UtilityUnitCode = strUtilityUnitCode;
                                    rowEvent01.Year = intReportingYear;
                                    rowEvent01.EventNumber = intEventNumber;

                                    if (String.Compare(strRevisionCode, rowEvent01.RevisionCard02) >= 0)
                                    {
                                        rowEvent01.RevisionCard02 = strRevisionCode;
                                        rowEvent01.EventType = strEventType;
                                        rowEvent01.RevMonthCard02 = rowEvent01.RevMonthCard01;

                                        rowEvent01.CauseCode = intCauseCode;
                                        rowEvent01.CauseCodeExt = strCauseCodeExt;

                                        if (strTimeWorkStarted != "00000000" && strTimeWorkStarted.Trim() != string.Empty)
                                        {
                                            if (dt_regex.IsMatch(strTimeWorkStarted))
                                            {
                                                // create the Time Work Started date/time
                                                if (strTimeWorkStarted.Substring(4, 4) == "2400")
                                                {
                                                    dateString = strTimeWorkStarted.Substring(0, 2) + @"/" + strTimeWorkStarted.Substring(2, 2) + @"/" + intReportingYear.ToString() +
                                                        " 23:59:59.999";
                                                }
                                                else
                                                {
                                                    dateString = strTimeWorkStarted.Substring(0, 2) + @"/" + strTimeWorkStarted.Substring(2, 2) + @"/" + intReportingYear.ToString() + " " +
                                                        strTimeWorkStarted.Substring(4, 2) + ":" + strTimeWorkStarted.Substring(6, 2);
                                                }

                                                MyDateTime = System.DateTime.Parse(dateString);
                                                rowEvent01.WorkStarted = MyDateTime;
                                                rowEvent01.RevMonthCard02 = MyUTCDateTime;
                                            }
                                            else
                                            {
                                                // create the Time Work Started date/time

                                                if (strTimeWorkStarted.Substring(6, 2).ToUpper() == "XX")
                                                {
                                                    dateString = strTimeWorkStarted.Substring(0, 2) + @"/" + strTimeWorkStarted.Substring(2, 2) + @"/" + intReportingYear.ToString() + " " +
                                                        strTimeWorkStarted.Substring(4, 2) + ":00";

                                                    MyDateTime = System.DateTime.Parse(dateString);
                                                    rowEvent01.WorkStarted = MyDateTime;
                                                    rowEvent01.RevMonthCard02 = MyUTCDateTime;

                                                }
                                            }
                                        }

                                        if (strTimeWorkEnded != "00000000" && strTimeWorkEnded.Trim() != string.Empty)
                                        {
                                            if (dt_regex.IsMatch(strTimeWorkEnded))
                                            {
                                                // create the Time Work Ended date/time

                                                // Invalid date/time -- could be 123124XX

                                                if (strTimeWorkEnded == "12312400")
                                                {
                                                    dateString = @"12/31/" + intReportingYear.ToString() + " 23:59:59.999";

                                                }
                                                else
                                                {
                                                    if (strTimeWorkEnded.Substring(4, 4) == "2400")
                                                    {
                                                        dateString = strTimeWorkEnded.Substring(0, 2) + @"/" + strTimeWorkEnded.Substring(2, 2) + @"/" + intReportingYear.ToString() +
                                                            " 23:59:59.999";
                                                    }
                                                    else
                                                    {
                                                        dateString = strTimeWorkEnded.Substring(0, 2) + @"/" + strTimeWorkEnded.Substring(2, 2) + @"/" + intReportingYear.ToString() + " " +
                                                            strTimeWorkEnded.Substring(4, 2) + ":" + strTimeWorkEnded.Substring(6, 2);
                                                    }
                                                }

                                                MyDateTime = System.DateTime.Parse(dateString);
                                                rowEvent01.WorkEnded = MyDateTime;
                                                rowEvent01.RevMonthCard02 = MyUTCDateTime;

                                            }
                                            else
                                            {
                                                // create the Time Work Ended date/time

                                                if (strTimeWorkEnded.ToUpper() == "123124XX")
                                                {
                                                    dateString = @"12/31/" + intReportingYear.ToString() + " 23:59:59.999";

                                                    MyDateTime = System.DateTime.Parse(dateString);
                                                    rowEvent01.WorkEnded = MyDateTime;
                                                    rowEvent01.RevMonthCard02 = MyUTCDateTime;
                                                }

                                            }
                                        }

                                        // For Card 02 Contribution Code must always be a 1
                                        rowEvent01.ContribCode = 1;

                                        if (strPrimaryAlert.ToUpper() == "X")
                                        {
                                            rowEvent01.PrimaryAlert = true;
                                        }
                                        else
                                        {
                                            rowEvent01.PrimaryAlert = false;
                                        }
                                        rowEvent01.ManhoursWorked = intManhoursWorked;

                                        if (Exp_regex.IsMatch(strExpandedData))
                                        {
                                            // Expanded Data Reporting -- Contribution Code 1 ONLY
                                            strVerbalDescription = strRecord.Substring(65 + nFormat, 15);
                                            strVerbalDescription = strVerbalDescription.Replace("'", " ");
                                            strVerbalDescription = strVerbalDescription.Replace(",", " ");

                                            rowEvent01.FailureMechCode = strExpandedData.Substring(0, 4);
                                            rowEvent01.TripMech = strExpandedData.Substring(4, 1);
                                            rowEvent01.CumFiredHours = Convert.ToInt32(strExpandedData.Substring(5, 6));
                                            rowEvent01.CumEngineStarts = Convert.ToInt32(strExpandedData.Substring(11, 5));
                                        }
                                        else if (FM_regex.IsMatch(strFailureMech))
                                        {
                                            strVerbalDescription = strRecord.Substring(53 + nFormat, 27);
                                            strVerbalDescription = strVerbalDescription.Replace("'", " ");
                                            strVerbalDescription = strVerbalDescription.Replace(",", " ");

                                            rowEvent01.FailureMechCode = strFailureMech;
                                        }

                                        strVerbalDescription = strVerbalDescription.Replace("'", " ");
                                        strVerbalDescription = strVerbalDescription.Replace(",", " ");

                                        rowEvent01.VerbalDesc1 = strVerbalDescription;
                                        strTemp1 = strVerbalDescription.PadLeft(31, ' ');

                                        if (rowEvent01.IsVerbalDesc86Null())
                                        {
                                            rowEvent01.VerbalDesc86 = strTemp1;
                                        }
                                        else
                                        {
                                            if (rowEvent01.VerbalDesc86.Trim() == string.Empty)
                                            {
                                                rowEvent01.VerbalDesc86 = strTemp1;
                                            }
                                            else
                                            {
                                                strVerbalDescription = rowEvent01.VerbalDesc86.Substring(31).Replace("'", " ");
                                                strVerbalDescription = strVerbalDescription.Replace(",", " ");

                                                rowEvent01.VerbalDesc86 = strTemp1 + strVerbalDescription;
                                            }
                                        }
                                    }

                                    if (lRowNew)
                                    {
                                        tblEvent01.AddEventData01Row(rowEvent01);
                                    }

                                    dsEvent01.AcceptChanges();

                                }
                                else if (strRecord.EndsWith("03") || strRecord.EndsWith(" 3"))
                                {
                                    //// Event 03 record

                                    //strVerbalDescription = strRecord.Substring(25 + nFormat, 55);  // used for both parts of the verbal description
                                    //strVerbalDescription = strVerbalDescription.Replace("'", " ");
                                    //strVerbalDescription = strVerbalDescription.Replace(",", " ");

                                    //// Code here to load Event 03 card data to dataset

                                    //// Check to see if this data is already in the dataset
                                    //rowEvent01 = tblEvent01.FindByUtilityUnitCodeYearEventNumber(strUtilityUnitCode, intReportingYear, intEventNumber);

                                    //lRowNew = false;

                                    //if (rowEvent01 == null)
                                    //{
                                    //    // if NOT, create a new datarow

                                    //    rowEvent01 = tblEvent01.NewEventData01Row();
                                    //    rowEvent01.StartDateTime = MyUTCDateTime;
                                    //    rowEvent01.CauseCode = 0;
                                    //    rowEvent01.CauseCodeExt = "  ";
                                    //    rowEvent01.ContribCode = 0;
                                    //    rowEvent01.VerbalDesc1 = "";
                                    //    rowEvent01.VerbalDesc86 = "";
                                    //    rowEvent01.RevisionCard01 = "*";
                                    //    rowEvent01.RevisionCard02 = "*";
                                    //    rowEvent01.RevisionCard03 = "0";
                                    //    rowEvent01.RevMonthCard01 = MyUTCDateTime;
                                    //    rowEvent01.RevMonthCard02 = MyUTCDateTime;
                                    //    lRowNew = true;
                                    //}

                                    //rowEvent01.UtilityUnitCode = strUtilityUnitCode;
                                    //rowEvent01.Year = intReportingYear;
                                    //rowEvent01.EventNumber = intEventNumber;

                                    //if (rowEvent01.IsRevisionCard03Null())
                                    //{
                                    //    rowEvent01.RevisionCard03 = "0";
                                    //}

                                    //if (String.Compare(strRevisionCode, rowEvent01.RevisionCard03) >= 0)
                                    //{
                                    //    rowEvent01.RevisionCard03 = strRevisionCode;
                                    //    rowEvent01.EventType = strEventType;
                                    //    rowEvent01.RevMonthCard03 = rowEvent01.RevMonthCard01;

                                    //    rowEvent01.VerbalDesc2 = strVerbalDescription;

                                    //    if (rowEvent01.IsVerbalDesc86Null())
                                    //    {
                                    //        rowEvent01.VerbalDesc86 = strVerbalDescription.PadLeft(86, ' ');
                                    //    }
                                    //    else
                                    //    {
                                    //        if (rowEvent01.VerbalDesc86.Trim() == string.Empty)
                                    //        {
                                    //            rowEvent01.VerbalDesc86 = strVerbalDescription.PadLeft(86, ' ');
                                    //        }
                                    //        else
                                    //        {
                                    //            strVerbalDescription = rowEvent01.VerbalDesc86.Substring(0, 31).Replace("'", " ") + strVerbalDescription;
                                    //            strVerbalDescription = strVerbalDescription.Replace(",", " ");

                                    //            rowEvent01.VerbalDesc86 = strVerbalDescription;
                                    //        }
                                    //    }
                                    //}

                                    //if (lRowNew)
                                    //{
                                    //    tblEvent01.AddEventData01Row(rowEvent01);
                                    //}

                                    //dsEvent01.AcceptChanges();
                                }
                                else
                                {
                                    //// Event 04-99 records
                                    //strTemp1 = strRecord.Substring(strRecord.Length - 2, 2);
                                    //strTemp1 = strTemp1.Replace(" ", "0");

                                    //if (!l_regex.IsMatch(strTemp1) || strTemp1.ToString() == "00")
                                    //{
                                    //    // makes sure that the card number is two digits and not something strange
                                    //    lResult = false;
                                    //    break;
                                    //}

                                    //strCardNo = strTemp1;
                                    //intCardNo = Convert.ToByte(strCardNo);

                                    //if (Even(Convert.ToInt32(CheckForEmpty(strTemp1))))
                                    //{
                                    //    // "even" numbered Event record

                                    //    intCauseCode = Convert.ToInt16(CheckForEmpty(strRecord.Substring(19 + nFormat, 4)));
                                    //    strCauseCodeExt = strRecord.Substring(23 + nFormat, 2);
                                    //    strAmplificationCode = strRecord.Substring(23 + nFormat, 1);   // new field to mimic CEA code
                                    //    strTimeWorkStarted = strRecord.Substring(25 + nFormat, 8);
                                    //    strTimeWorkEnded = strRecord.Substring(33 + nFormat, 8);
                                    //    intEventContribCode = Convert.ToByte(CheckForEmpty(strRecord.Substring(43 + nFormat, 1)));
                                    //    if (intEventContribCode == 0)
                                    //    {
                                    //        intEventContribCode = 3;
                                    //    }
                                    //    strPrimaryAlert = strRecord.Substring(44 + nFormat, 1);
                                    //    intManhoursWorked = Convert.ToInt16(CheckForEmpty(strRecord.Substring(45 + nFormat, 4)));
                                    //    strVerbalDescription = strRecord.Substring(49 + nFormat, 31);  // used for both parts of the verbal description
                                    //    strVerbalDescription = strVerbalDescription.Replace("'", " ");
                                    //    strVerbalDescription = strVerbalDescription.Replace(",", " ");
                                    //    strFailureMech = strRecord.Substring(49 + nFormat, 4);  // Failure Mechanism -- ALL Even numbered records
                                    //    strExpandedData = strRecord.Substring(49 + nFormat, 16);  // Expanded Data Reporting -- Contribution Code 1 ONLY

                                    //    // Code here to load Event 04-99 even-numbered card data to dataset

                                    //    // Check to see if this data is already in the dataset

                                    //    rowEvent02 = tblEvent02.FindByUtilityUnitCodeYearEventNumberEvenCardNumber(strUtilityUnitCode, intReportingYear, intEventNumber, intCardNo);

                                    //    lRowNew = false;

                                    //    if (rowEvent02 == null)
                                    //    {
                                    //        // if NOT, create a new datarow

                                    //        rowEvent02 = tblEvent02.NewEventData02Row();
                                    //        rowEvent02.CauseCode = 0;
                                    //        rowEvent02.CauseCodeExt = "  ";
                                    //        rowEvent02.ContribCode = 0;
                                    //        rowEvent02.VerbalDesc1 = "";
                                    //        rowEvent02.RevisionCardEven = "0";
                                    //        rowEvent02.RevisionCardOdd = "*";
                                    //        rowEvent02.RevMonthCardEven = MyUTCDateTime;
                                    //        rowEvent02.RevMonthCardOdd = MyUTCDateTime;
                                    //        lRowNew = true;

                                    //    }

                                    //    rowEvent02.UtilityUnitCode = strUtilityUnitCode;
                                    //    rowEvent02.Year = intReportingYear;
                                    //    rowEvent02.EventNumber = intEventNumber;

                                    //    if (String.Compare(strRevisionCode, rowEvent02.RevisionCardEven) >= 0)
                                    //    {
                                    //        rowEvent02.RevisionCardEven = strRevisionCode;
                                    //        rowEvent02.EventType = strEventType;
                                    //        rowEvent02.RevMonthCardEven = MyUTCDateTime;

                                    //        rowEvent02.CauseCode = intCauseCode;
                                    //        rowEvent02.CauseCodeExt = strCauseCodeExt;

                                    //        if (strTimeWorkStarted != "00000000" && strTimeWorkStarted.Trim() != string.Empty)
                                    //        {
                                    //            if (dt_regex.IsMatch(strTimeWorkStarted))
                                    //            {
                                    //                // create the Time Work Started date/time
                                    //                if (strTimeWorkStarted.Substring(4, 4) == "2400")
                                    //                {
                                    //                    dateString = strTimeWorkStarted.Substring(0, 2) + @"/" + strTimeWorkStarted.Substring(2, 2) + @"/" + intReportingYear.ToString() +
                                    //                        " 23:59:59.999";
                                    //                }
                                    //                else
                                    //                {
                                    //                    dateString = strTimeWorkStarted.Substring(0, 2) + @"/" + strTimeWorkStarted.Substring(2, 2) + @"/" + intReportingYear.ToString() + " " +
                                    //                        strTimeWorkStarted.Substring(4, 2) + ":" + strTimeWorkStarted.Substring(6, 2);
                                    //                }

                                    //                MyDateTime = System.DateTime.Parse(dateString);
                                    //                rowEvent02.WorkStarted = MyDateTime;
                                    //                rowEvent02.RevMonthCardEven = MyUTCDateTime;
                                    //            }
                                    //            else
                                    //            {
                                    //                // create the Time Work Started date/time

                                    //                if (strTimeWorkStarted.Substring(6, 2).ToUpper() == "XX")
                                    //                {
                                    //                    dateString = strTimeWorkStarted.Substring(0, 2) + @"/" + strTimeWorkStarted.Substring(2, 2) + @"/" + intReportingYear.ToString() + " " +
                                    //                        strTimeWorkStarted.Substring(4, 2) + ":00";

                                    //                    MyDateTime = System.DateTime.Parse(dateString);
                                    //                    rowEvent02.WorkStarted = MyDateTime;
                                    //                    rowEvent02.RevMonthCardEven = MyUTCDateTime;

                                    //                }
                                    //            }
                                    //        }

                                    //        if (strTimeWorkEnded != "00000000" && strTimeWorkEnded.Trim() != string.Empty)
                                    //        {
                                    //            if (dt_regex.IsMatch(strTimeWorkEnded))
                                    //            {
                                    //                // create the Time Work Ended date/time

                                    //                // Invalid date/time -- could be 123124XX

                                    //                if (strTimeWorkEnded == "12312400")
                                    //                {
                                    //                    dateString = @"12/31/" + intReportingYear.ToString() + " 23:59:59.999";

                                    //                }
                                    //                else
                                    //                {
                                    //                    if (strTimeWorkEnded.Substring(4, 4) == "2400")
                                    //                    {
                                    //                        dateString = strTimeWorkEnded.Substring(0, 2) + @"/" + strTimeWorkEnded.Substring(2, 2) + @"/" + intReportingYear.ToString() +
                                    //                            " 23:59:59.999";
                                    //                    }
                                    //                    else
                                    //                    {
                                    //                        dateString = strTimeWorkEnded.Substring(0, 2) + @"/" + strTimeWorkEnded.Substring(2, 2) + @"/" + intReportingYear.ToString() + " " +
                                    //                            strTimeWorkEnded.Substring(4, 2) + ":" + strTimeWorkEnded.Substring(6, 2);
                                    //                    }
                                    //                }

                                    //                MyDateTime = System.DateTime.Parse(dateString);
                                    //                rowEvent02.WorkEnded = MyDateTime;
                                    //                rowEvent02.RevMonthCardEven = MyUTCDateTime;

                                    //            }
                                    //            else
                                    //            {
                                    //                // create the Time Work Ended date/time

                                    //                if (strTimeWorkEnded.ToUpper() == "123124XX")
                                    //                {
                                    //                    dateString = @"12/31/" + intReportingYear.ToString() + " 23:59:59.999";

                                    //                    MyDateTime = System.DateTime.Parse(dateString);
                                    //                    rowEvent02.WorkEnded = MyDateTime;
                                    //                    rowEvent02.RevMonthCardEven = MyUTCDateTime;
                                    //                }

                                    //            }
                                    //        }

                                    //        rowEvent02.ContribCode = intEventContribCode;

                                    //        if (strPrimaryAlert.ToUpper() == "X")
                                    //        {
                                    //            rowEvent02.PrimaryAlert = true;
                                    //        }
                                    //        else
                                    //        {
                                    //            rowEvent02.PrimaryAlert = false;
                                    //        }
                                    //        rowEvent02.ManhoursWorked = intManhoursWorked;

                                    //        if (Exp_regex.IsMatch(strExpandedData))
                                    //        {
                                    //            if (intEventContribCode == 1)
                                    //            {
                                    //                // Expanded Data Reporting -- Contribution Code 1 ONLY
                                    //                strVerbalDescription = strRecord.Substring(65 + nFormat, 15);
                                    //                strVerbalDescription = strVerbalDescription.Replace("'", " ");
                                    //                strVerbalDescription = strVerbalDescription.Replace(",", " ");
                                    //                rowEvent02.FailureMechCode = strExpandedData.Substring(0, 4);
                                    //                rowEvent02.TripMech = strExpandedData.Substring(4, 1);
                                    //                rowEvent02.CumFiredHours = Convert.ToInt32(strExpandedData.Substring(5, 6));
                                    //                rowEvent02.CumEngineStarts = Convert.ToInt32(strExpandedData.Substring(11, 5));
                                    //            }
                                    //            else
                                    //            {
                                    //                strVerbalDescription = strRecord.Substring(53 + nFormat, 27);
                                    //                strVerbalDescription = strVerbalDescription.Replace("'", " ");
                                    //                strVerbalDescription = strVerbalDescription.Replace(",", " ");
                                    //                rowEvent02.FailureMechCode = strFailureMech;
                                    //            }
                                    //        }
                                    //        else if (FM_regex.IsMatch(strFailureMech))
                                    //        {
                                    //            strVerbalDescription = strRecord.Substring(53 + nFormat, 27);
                                    //            strVerbalDescription = strVerbalDescription.Replace("'", " ");
                                    //            strVerbalDescription = strVerbalDescription.Replace(",", " ");
                                    //            rowEvent02.FailureMechCode = strFailureMech;
                                    //        }

                                    //        strVerbalDescription = strVerbalDescription.Replace("'", " ");
                                    //        strVerbalDescription = strVerbalDescription.Replace(",", " ");
                                    //        rowEvent02.VerbalDesc1 = strVerbalDescription;
                                    //        strTemp1 = strVerbalDescription.PadLeft(31, ' ');

                                    //        if (rowEvent02.IsVerbalDesc86Null())
                                    //        {
                                    //            rowEvent02.VerbalDesc86 = strTemp1;
                                    //        }
                                    //        else
                                    //        {
                                    //            if (rowEvent02.VerbalDesc86.Trim() == string.Empty)
                                    //            {
                                    //                rowEvent02.VerbalDesc86 = strTemp1;
                                    //            }
                                    //            else
                                    //            {
                                    //                strVerbalDescription = rowEvent02.VerbalDesc86.Substring(31).Replace("'", " ");
                                    //                strVerbalDescription = strVerbalDescription.Replace(",", " ");
                                    //                rowEvent02.VerbalDesc86 = strTemp1 + strVerbalDescription;
                                    //            }
                                    //        }

                                    //        rowEvent02.EvenCardNumber = intCardNo;
                                    //    }

                                    //    if (lRowNew)
                                    //    {
                                    //        tblEvent02.AddEventData02Row(rowEvent02);
                                    //    }

                                    //    dsEvent02.AcceptChanges();
                                    //}
                                    //else
                                    //{
                                    //    // "odd" numbered Event records

                                    //    intCauseCode = Convert.ToInt16(CheckForEmpty(strRecord.Substring(19 + nFormat, 4)));
                                    //    strCauseCodeExt = strRecord.Substring(23 + nFormat, 2);
                                    //    strAmplificationCode = strRecord.Substring(23 + nFormat, 1);   // new field to mimic CEA code
                                    //    strVerbalDescription = strRecord.Substring(25 + nFormat, 55);  // used for both parts of the verbal description
                                    //    strVerbalDescription = strVerbalDescription.Replace("'", " ");
                                    //    strVerbalDescription = strVerbalDescription.Replace(",", " ");
                                    //    // Code to load Event 04-99 odd-numbered card data to dataset

                                    //    // Check to see if this data is already in the dataset
                                    //    intCardNo -= 1;
                                    //    rowEvent02 = tblEvent02.FindByUtilityUnitCodeYearEventNumberEvenCardNumber(strUtilityUnitCode, intReportingYear, intEventNumber, intCardNo);

                                    //    lRowNew = false;

                                    //    if (rowEvent02 == null)
                                    //    {
                                    //        // if NOT, create a new datarow

                                    //        rowEvent02 = tblEvent02.NewEventData02Row();
                                    //        rowEvent02.CauseCode = 0;
                                    //        rowEvent02.CauseCodeExt = "  ";
                                    //        rowEvent02.ContribCode = 0;
                                    //        rowEvent02.VerbalDesc1 = "";
                                    //        rowEvent02.RevisionCardEven = "*";
                                    //        rowEvent02.RevisionCardOdd = "0";
                                    //        rowEvent02.RevMonthCardEven = MyUTCDateTime;
                                    //        rowEvent02.RevMonthCardOdd = MyUTCDateTime;
                                    //        lRowNew = true;

                                    //    }

                                    //    rowEvent02.UtilityUnitCode = strUtilityUnitCode;
                                    //    rowEvent02.Year = intReportingYear;
                                    //    rowEvent02.EventNumber = intEventNumber;

                                    //    if (String.Compare(strRevisionCode, rowEvent02.RevisionCardOdd) >= 0)
                                    //    {
                                    //        rowEvent02.RevisionCardOdd = strRevisionCode;
                                    //        rowEvent02.EventType = strEventType;
                                    //        rowEvent02.RevMonthCardOdd = MyUTCDateTime;

                                    //        rowEvent02.CauseCode = intCauseCode;
                                    //        rowEvent02.CauseCodeExt = strCauseCodeExt;

                                    //        strVerbalDescription = strVerbalDescription.Replace("'", " ");
                                    //        strVerbalDescription = strVerbalDescription.Replace(",", " ");
                                    //        rowEvent02.VerbalDesc2 = strVerbalDescription;

                                    //        if (rowEvent02.IsVerbalDesc86Null())
                                    //        {
                                    //            rowEvent02.VerbalDesc86 = strVerbalDescription.PadLeft(86, ' ');
                                    //        }
                                    //        else
                                    //        {
                                    //            if (rowEvent02.VerbalDesc86.Trim() == string.Empty)
                                    //            {
                                    //                rowEvent02.VerbalDesc86 = strVerbalDescription.PadLeft(86, ' ');
                                    //            }
                                    //            else
                                    //            {
                                    //                strVerbalDescription = rowEvent02.VerbalDesc86.Substring(0, 31).Replace("'", " ") + strVerbalDescription;
                                    //                strVerbalDescription = strVerbalDescription.Replace(",", " ");
                                    //                rowEvent02.VerbalDesc86 = strVerbalDescription;
                                    //            }
                                    //        }

                                    //        rowEvent02.EvenCardNumber = Convert.ToByte(intCardNo);
                                    //    }

                                    //    if (lRowNew)
                                    //    {

                                    //        tblEvent02.AddEventData02Row(rowEvent02);

                                    //    }

                                    //    dsEvent02.AcceptChanges();
                                    //}
                                }
                            }
                        }
                        #endregion
                    }
                }

                #region Loading to database

                strErrorMessage = "\n\nASCII file has been completely read";
                //Application.DoEvents();

                if (System.IO.File.Exists("DEStop.txt") == true)
                {
                    StreamToDisplay.Close();

                    System.IO.File.Delete("DEStop.txt");

                    return strFileName + " -- Loading Aborted";
                }

                //Application.DoEvents();

                // Processing of Event Data

                if (!lResult)
                {
                    return strFileName + " -- ERROR in file format" + "\n\n" + strMessage;
                }

                HybridDictionary myHyD = new HybridDictionary();

                foreach (Event01.EventData01Row r01 in dsEvent01.Tables[0].Rows)
                {
                    if (!myHyD.Contains(r01.UtilityUnitCode))
                    {
                        myHyD.Add(r01.UtilityUnitCode, r01.Year);
                    }
                }

                int myDel = 0;

                foreach (DictionaryEntry de in myHyD)
                {
                    myDel = blConnect.DeleteRecords("DELETE FROM EventData01 WHERE UtilityUnitCode = '" + de.Key.ToString() + "' AND Year = " + de.Value.ToString());
                    blConnect.UpdateErrorCheckStatus(de.Key.ToString(), "R");
                }
                myHyD.Clear();

                lResult = blConnect.LoadEvent01(dsEvent01, true);
                if (!lResult)
                    return strFileName + " -- ERROR in loading Event 1-3";

                dsEvent01.Dispose();

                //Application.DoEvents();

                //lResult = blConnect.LoadEvent0499(dsEvent02, true);
                //if (!lResult)
                //    return strFileName + " -- ERROR in loading Event 4-99";

                dsEvent02.Dispose();

                //Application.DoEvents();

                lResult = blConnect.DeleteEventRecords(alDeleteEvent02);
                if (!lResult)
                    return strFileName + " -- ERROR in loading (Del2)";

                alDeleteEvent02.Clear();

                //Application.DoEvents();

                lResult = blConnect.DeleteEvent03Records(alDeleteEvent03);
                if (!lResult)
                    return strFileName + " -- ERROR in loading (Del3)";

                alDeleteEvent03.Clear();

                //Application.DoEvents();

                lResult = blConnect.DeleteEvent04Records(alDeleteEvent04);
                if (!lResult)
                    return strFileName + " -- ERROR in loading (Del4)";

                alDeleteEvent04.Clear();

                //Application.DoEvents();

                lResult = blConnect.DeleteEvent05Records(alDeleteEvent05);
                if (!lResult)
                    return strFileName + " -- ERROR in loading (Del5)";

                alDeleteEvent05.Clear();

                //Application.DoEvents();

                lResult = blConnect.DeleteEvents(alDeleteEvent01);
                if (!lResult)
                    return strFileName + " -- ERROR in loading (Del1)";

                alDeleteEvent01.Clear();

                //Application.DoEvents();

                // Processing of Performance Data

                lJO = false;

                foreach (Performance.PerformanceDataRow r01 in dsPerformance.Tables[0].Rows)
                {
                    if (!myHyD.Contains(r01.UtilityUnitCode))
                    {
                        myHyD.Add(r01.UtilityUnitCode, r01.Year);
                    }
                }

                myDel = 0;

                foreach (DictionaryEntry de in myHyD)
                {
                    myDel = blConnect.DeleteRecords("DELETE FROM PerformanceData WHERE UtilityUnitCode = '" + de.Key.ToString() + "' AND Year = " + de.Value.ToString());
                    blConnect.UpdateErrorCheckStatus(de.Key.ToString(), "R");
                }

                myHyD.Clear();

                lResult = blConnect.LoadPerformance(dsPerformance, lJO, false);
                if (!lResult)
                    return strFileName + " -- ERROR in loading Performance";

                dsPerformance.Dispose();

                tblEvent01.Dispose();
                tblEvent02.Dispose();
                tblPerformance.Dispose();

                //Application.DoEvents();

                #endregion

            }
            catch (Exception e)
            {
                lResult = false;
                // publish Exception using ExceptionManager
                //ExceptionManager.Publish( e );
                //Application.DoEvents();
                strErrorMessage = e.Message + strErrorMessage;
                //				MessageBox.Show(strErrorMessage);
                //				Application.DoEvents();
            }
            finally
            {
                dsPerformance.Dispose();
                dsEvent01.Dispose();
                dsEvent02.Dispose();
                StreamToDisplay.Close();
                //Application.DoEvents();
            }

            if (lResult)
                return strFileName + " -- OK";
            else
                return strFileName + " -- ERROR in loading " + strErrorMessage;
        }

        #region CalcBtus
        private static double CalcBtus(string strFuelCode, double douQtyBurned, int int32HeatContent)
        {
            //double douConversionFactor;
            //douConversionFactor = 1.00;
            return 0.0;
            //if (strFuelCode == "NU")
            //{
            //    return 0.0;
            //}

            //switch (strFuelCode)
            //{
            //    case "CC":
            //        douConversionFactor = 1000.0 * 2000.0;
            //        break;
            //    case "LI":
            //        douConversionFactor = 1000.0 * 2000.0;
            //        break;
            //    case "OS":
            //        douConversionFactor = 1000.0 * 2000.0;
            //        break;
            //    case "BM":
            //        douConversionFactor = 1000.0 * 2000.0;
            //        break;
            //    case "PC":
            //        douConversionFactor = 1000.0 * 2000.0;
            //        break;
            //    case "OO":
            //        douConversionFactor = 1000.0 * 42.0;
            //        break;
            //    case "DI":
            //        douConversionFactor = 1000.0 * 42.0;
            //        break;
            //    case "KE":
            //        douConversionFactor = 1000.0 * 42.0;
            //        break;
            //    case "JP":
            //        douConversionFactor = 1000.0 * 42.0;
            //        break;
            //    case "OL":
            //        douConversionFactor = 1000.0 * 42.0;
            //        break;
            //    case "GG":
            //        douConversionFactor = 1000000.0;
            //        break;
            //    case "PR":
            //        douConversionFactor = 1000000.0;
            //        break;
            //    case "SL":
            //        douConversionFactor = 1000000.0;
            //        break;
            //    case "OG":
            //        douConversionFactor = 1000000.0;
            //        break;
            //    default:
            //        // PE, WD, WA, GE, WM, SO, WH
            //        douConversionFactor = 1.0;
            //        break;
            //}

            //return douConversionFactor * douQtyBurned * int32HeatContent;

        }
        #endregion

        #region Decode
        private static string Decode(string strNERCCode)
        {
            // Performance 01 record data from NERC contains a special character in column 43 (80-column data records)
            // or in column 45 (82-column data record) for Net Actual Generation that is invalid under a "normal"
            // processing of the performance data.  This "corrects" the data received from NERC.

            string cOnes;
            string strSign;
            strSign = "";

            switch (strNERCCode.Substring(strNERCCode.Length - 1, 1))
            {
                case "{":
                    cOnes = "0";
                    break;
                case "}":
                    cOnes = "0";
                    break;
                case "A":
                    cOnes = "1";
                    break;
                case "J":
                    strSign = "-";
                    cOnes = "1";
                    break;
                case "B":
                    cOnes = "2";
                    break;
                case "K":
                    strSign = "-";
                    cOnes = "2";
                    break;
                case "C":
                    cOnes = "3";
                    break;
                case "L":
                    strSign = "-";
                    cOnes = "3";
                    break;
                case "D":
                    cOnes = "4";
                    break;
                case "M":
                    strSign = "-";
                    cOnes = "4";
                    break;
                case "E":
                    cOnes = "5";
                    break;
                case "N":
                    strSign = "-";
                    cOnes = "5";
                    break;
                case "F":
                    cOnes = "6";
                    break;
                case "O":
                    strSign = "-";
                    cOnes = "6";
                    break;
                case "G":
                    cOnes = "7";
                    break;
                case "P":
                    strSign = "-";
                    cOnes = "7";
                    break;
                case "H":
                    cOnes = "8";
                    break;
                case "Q":
                    strSign = "-";
                    cOnes = "8";
                    break;
                case "I":
                    cOnes = "9";
                    break;
                case "R":
                    strSign = "-";
                    cOnes = "9";
                    break;
                default:
                    return strNERCCode;
            }

            StringBuilder MyStringBuilder = new StringBuilder(strSign + strNERCCode.Substring(0, strNERCCode.Length - 1));
            return MyStringBuilder.Append(cOnes).ToString();

        }
        #endregion

        #region Even
        /// <summary>
        /// Determines if the integer input is even or odd
        /// </summary>
        private static bool Even(int intValue)
        {
            if (intValue % 2 == 0)
                return true;
            else
                return false;
        }
        #endregion

        #region CheckForEmpty
        /// <summary>
        /// Converts an "empty" string to include at least one 0 for converting to a number
        /// </summary>
        private static string CheckForEmpty(string numberString)
        {
            string strTemp;
            strTemp = numberString.Trim();

            if (strTemp == string.Empty)
            {
                return "0";
            }
            else if (strTemp == "-")
            {
                return "0";
            }
            else if (cfe_regex.IsMatch(strTemp))
            {
                return numberString;
            }
            else
            {
                string strTemp2;
                strTemp2 = strTemp.Replace(" ", "0");

                if (cfe_regex.IsMatch(strTemp2))
                {
                    return strTemp2;
                }
                else
                {
                    return "0";
                }
            }
        }
        #endregion
    }
}
