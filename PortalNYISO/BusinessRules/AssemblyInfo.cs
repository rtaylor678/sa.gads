using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

//
// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
//
[assembly: AssemblyTitle("GADSNGDE ASCII File Interface")]
[assembly: AssemblyDescription("Data Entry ASCII file interface")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("GADS Open Source Project")]
[assembly: AssemblyProduct("GADS Data Entry")]
[assembly: AssemblyCopyright("Copyright � 2011 by The Outercurve Foundation, All Rights Reserved.")]
[assembly: AssemblyTrademark("GADS OS is a service mark of GADS Open Source Project")]
[assembly: AssemblyCulture("")]		

//
// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:

[assembly: AssemblyVersion("12.0.0.0")]
[assembly: AssemblyFileVersionAttribute("16.1.8.0")]
[assembly: ComVisibleAttribute(false)]
[assembly: GuidAttribute("0A799FE1-87CE-4215-9753-803D346920A7")]
