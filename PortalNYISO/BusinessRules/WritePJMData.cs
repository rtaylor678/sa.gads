using System;
using System.Collections;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Data;
using System.Data.OleDb;
using System.Data.Common;
using System.Xml;
using System.Windows.Forms;
using GADSNG.Base;
using BusinessLayer;
using BusinessLayer.BusinessLayer;

namespace GADSDataInterfaces
{
	/// <summary>
	/// Summary description for WritePJMData.
	/// </summary>
	public class WritePJMData
	{

		protected string WriteFileType;

		public WritePJMData()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		/// <summary>
		/// This function writes out either the NERC or NYISO GADS ASCII data file
		/// </summary>
		/// <param name="strFileName"></param>
		/// <param name="intChoice"></param>
		/// <param name="intYear"></param>
		/// <param name="intPeriod"></param>
		/// <param name="strWho"></param>
		/// <param name="intNoOfColumns"></param>
		/// <returns>A status string related to the output file creation</returns>
		/// 

#region WritePJMFile

		public static string WritePJMFile(string strFileName, int intYear, int intPeriod, IEnumerable aUnits, string connect, GADSNGBusinessLayer blConnect)
		{
			DataTable dtPerfChanges;			
			int i;
			
			System.Collections.IEnumerator myEnumerator = aUnits.GetEnumerator();
			
			string stringFolder;
			string stringFileName;
			string stringPerf95File;
			string stringPerf99File;
			string stringEvent97File;
			string stringException;
			bool lError;
			lError = false;

			bool lInternalOnly;

			stringFileName = "Invalid file name";

			stringPerf95File = strFileName;
			stringPerf99File = strFileName;
			stringEvent97File = strFileName;

			i = strFileName.LastIndexOf(@"\");

			string strTestFile;
			strTestFile = "Test.txt";

			if (i > 0)
			{
				// strFileName has at least one \ -- minimum should be x:\xxxevtnn.dbf

				// typically stringFolder will be x:\yyyy\zzzzz\
				stringFolder  = strFileName.Substring(0, i);

				if (Directory.Exists(stringFolder) == false)
				{
					return "ERROR -- " + stringFolder + " is not a valid directory";
				}

				if (stringFolder.EndsWith(@"\") == false)
				{
					stringFolder += @"\";
				}
				
				strTestFile = stringFolder + "Test.Txt";
			}
			else
			{
				i = strFileName.LastIndexOf(@":");

				if (i > 0)
				{
					stringFolder  = strFileName.Substring(0, i) + @":\";
					
					if (Directory.Exists(stringFolder) == false)
					{
						return "ERROR -- " + stringFolder + " is not a valid directory";
					}
				
					strTestFile = stringFolder + "Test.Txt";
				}
			}

			// This creates a "temporary" TestFileWriter so that the Finally use compiles
			StreamWriter TestFileWriter = new StreamWriter(strTestFile);
			TestFileWriter.WriteLine("Testing on " + System.DateTime.Now.ToLongDateString());
			TestFileWriter.Close();

			try
			{	
				// Test to see if the entered strFileName is a valid file name
				TestFileWriter = new StreamWriter(strFileName,false);
				TestFileWriter.WriteLine("Testing on " + System.DateTime.Now.ToLongDateString());			
			}
			catch (ArgumentException e)
			{
				lError = true;
				stringException = e.ToString();
				stringFileName  = "Missing File Name";
			}
			catch (DirectoryNotFoundException e)
			{
				lError = true;
				stringException = e.ToString();
				stringFileName  = "Directory not found";
			}
			catch (IOException e)
			{
				lError = true;
				stringException = e.ToString();
				stringFileName  = "Invalid Syntax";
			}
			catch (Exception e)
			{
				lError = true;
				stringException = e.ToString();
				stringFileName = "General Exception or Invalid file name";
			}
			finally
			{
				TestFileWriter.Close();
			}
			
			if (lError == true)
			{
				return "ERROR -- " + stringFileName;
			}
		

			i = strFileName.LastIndexOf(@"\");

			if (i > 0)
			{
				// strFileName has at least one \ -- minimum should be x:\xxxevtnn.dbf

				// stringFileName will be *.*
				stringFileName = strFileName.Substring(i + 1);

				// typically stringFolder will be x:\yyyy\zzzzz\
				stringFolder  = strFileName.Substring(0, i);

				if (Directory.Exists(stringFolder) == false)
				{
					return "ERROR -- " + stringFolder + " is not a valid directory.";
				}

				if (stringFolder.EndsWith(@"\") == false)
				{
					stringFolder += @"\";
				}
				stringPerf95File = stringFolder + "Card95_" + stringFileName;
				stringPerf99File = stringFolder + "Card99_" + stringFileName;
				stringEvent97File = stringFolder + "Card97_" + stringFileName;
			}
			else if(strFileName.LastIndexOf(@":") > 0)
			{
				i = strFileName.LastIndexOf(@":");

				stringFileName = strFileName.Substring(i + 1);

				stringFolder  = strFileName.Substring(0, i) + @":\";
				
				if (Directory.Exists(stringFolder) == false)
				{
					return "ERROR -- " + stringFolder + " is not a valid directory.";
				}
			
				stringPerf95File = stringFolder + "Card95_" + stringFileName;
				stringPerf99File = stringFolder + "Card99_" + stringFileName;
				stringEvent97File = stringFolder + "Card97_" + stringFileName;	
			}
			else
			{
				stringFileName = strFileName;
				stringFolder = "";
				stringPerf95File = "Card95_" + strFileName;
				stringPerf99File = "Card99_" + strFileName;
				stringEvent97File = "Card97_" + strFileName;
			}
						
			if (stringPerf95File.ToUpper().EndsWith(".TXT"))
			{
				stringPerf95File = stringPerf95File.Replace(".TXT",".CSV");
				stringPerf95File = stringPerf95File.Replace(".txt",".csv");
			}

			if (stringEvent97File.ToUpper().EndsWith(".TXT"))
			{
				stringEvent97File = stringEvent97File.Replace(".TXT",".CSV");
				stringEvent97File = stringEvent97File.Replace(".txt",".csv");
			}

			if (stringPerf99File.ToUpper().EndsWith(".TXT"))
			{
				stringPerf99File = stringPerf99File.Replace(".TXT",".CSV");
				stringPerf99File = stringPerf99File.Replace(".txt",".csv");
			}
			
			StreamWriter Event97FileWriter = null;
			try
			{
				Event97FileWriter = new StreamWriter(stringEvent97File,false);
			}
			catch (System.IO.IOException eIO)
			{
				stringException = eIO.ToString();
				return "ERROR -- " + stringEvent97File + " is being used by another application";
			}
			catch (Exception e)
			{
				stringException = e.ToString();
				return "ERROR -- Could not create " + stringEvent97File;
				// throw(e);
			}
			Event97FileWriter.AutoFlush = true;
		
			StreamWriter Perf95FileWriter = null;
			try
			{
				Perf95FileWriter = new StreamWriter(stringPerf95File,false);
			}
			catch (System.IO.IOException eIO)
			{
				stringException = eIO.ToString();
				return "ERROR -- " + stringPerf95File + " is being used by another application";
			}
			catch (Exception e)
			{
				stringException = e.ToString();
				return "ERROR -- Could not create " + stringPerf95File;
				// throw(e);
			}
			Perf95FileWriter.AutoFlush = true;

			StreamWriter Perf99FileWriter = null;
			try
			{
				Perf99FileWriter = new StreamWriter(stringPerf99File,false);
			}
			catch (System.IO.IOException eIO)
			{
				stringException = eIO.ToString();
				return "ERROR -- " + stringPerf99File + " is being used by another application";
			}
			catch (Exception e)
			{
				stringException = e.ToString();
				return "ERROR -- Could not create " + stringPerf99File;
				// throw(e);
			}
			Perf99FileWriter.AutoFlush = true;

			StringBuilder Perf95StringBuilder;
			StringBuilder Perf99StringBuilder;
			StringBuilder Event97StringBuilder;

			Perf95StringBuilder = new StringBuilder(0, 1024);
			Perf99StringBuilder = new StringBuilder(0, 1024);
			Event97StringBuilder = new StringBuilder(0, 1024);

			Perf95StringBuilder.Length = 0;
			string Perf95Header = "95,UtilityCode,UnitCode,Year,Month,ReportRevCode," +
								  "NMC,NDC,NAG,LoadChar,AttStarts,ActStarts,LoadDesc," + 
								  "SH,RSH,PmpHrs,SynCondHrs";
			Perf95StringBuilder.Insert(0,Perf95Header);
			Perf95FileWriter.WriteLine(Perf95StringBuilder.ToString());

			Perf99StringBuilder.Length = 0;
			string Perf99Header = "99,UtilityCode,UnitCode,Year,Month,RevCode," +
								  "PriFuelCode,PriQtyBurned,PriAvgHeatContent,PriPercentAsh,PriPercentMoisture,PriPercentSulfur," +
								  "SecFuelCode,SecQtyBurned,SecAvgHeatContent,SecPercentAsh,SecPercentMoisture,SecPercentSulfur," + 
								  "TerFuelCode,TerQtyBurned,TerAvgHeatContent,TerPercentAsh,TerPercentMoisture,TerPercentSulfur," + 
								  "QuaFuelCode,QuaQtyBurned,QuaAvgHeatContent,QuaPercentAsh,QuaPercentMoisture,QuaPercentSulfur";
			Perf99StringBuilder.Insert(0,Perf99Header);
			Perf99FileWriter.WriteLine(Perf99StringBuilder.ToString());

			Event97StringBuilder.Length = 0;
			string Event97Header = "97,UtilityCode,UnitCode,Year,EventNumber,EventIndex,RevCode," +
								   "EventType,IOCode,StartOfEvent,EndOfEvent,AvailCapacity,CauseCode," + 
								   "ContribCode,WorkStarted,WorkCompleted,MHWorked,VerbalDesc,FailCode,AmpCode";
			Event97StringBuilder.Insert(0,Event97Header);
			Event97FileWriter.WriteLine(Event97StringBuilder.ToString());

			bool lResult;
			string strUnit;
			string strTemp;

			Regex l_regex = new Regex("[0-9]{2}");
			Regex dt_regex = new Regex("[0-9]{8}");

			//			string connect = "DataEntry";
            // GADSNGBusinessLayer blConnect = new GADSNGBusinessLayer(connect);			

			lResult = true;

			Performance.PerformanceDataDataTable dtPerformance;
	
			AllEventData dsEvents = new AllEventData();

			try
			{
				// Process this unit's data

				while (myEnumerator.MoveNext())
				{
                    //Application.DoEvents();	

					strUnit = myEnumerator.Current.ToString();

					// get the performance and event datatables

					dtPerformance = blConnect.GetPerformanceData(strUnit, intYear);			
					dsEvents = blConnect.GetBothEventSets(strUnit, intYear);
											
					/*
					* ORIGINAL -
					* 
					* To delete a single card (other than 01 card), repeat columns 1-12 (80-column format)
					* as previously reported, enter an "X" in column 13 and enter the card number of the
					* record to be deleted in columns 79 and 80.
					* 
					* To delete an entire report, repeat columns 1-12 (80-column format) as previously reported
					* and enter an "X" in column 13.  Leave columns 79 and 80 blank.  ALL records (cards) for
					* that report period will be deleted.
					* 
					* October 2002 -
					* 
					* To delete data from one or more data fields, GADS recommends that you resubmit the entire
					* data set -- year-to-date -- for that unit (or all units you report) to GADS.  This 
					* procedure will insure that both you and the GADS database have the same records on file.
					* You have the option to find the record that has the highest revision code and then
					* increase this number by 1 or set all revision codes back to zero.
					* 
					*/
					
					#region Card 95 and 99 Data
					try
					{
						foreach (Performance.PerformanceDataRow rowPerf in dtPerformance.Rows)
						{
                            //Application.DoEvents();

							if (Convert.ToInt32(rowPerf.Period) == intPeriod)
							{

								/*
									* ----------------------------------
									*  Card 95 data
									* ----------------------------------
								*/

								Perf95StringBuilder.Length = 0;
								Perf95StringBuilder.Insert(0,95);
								// -----------------------------
								Perf95StringBuilder.Append(",");
								// -----------------------------

								Perf95StringBuilder.Append(rowPerf.UtilityUnitCode.PadLeft(6,'0').Substring(0,3));
								// -----------------------------
								Perf95StringBuilder.Append(",");
								// -----------------------------

								Perf95StringBuilder.Append(rowPerf.UtilityUnitCode.PadLeft(6,'0').Substring(3,3));
								// -----------------------------
								Perf95StringBuilder.Append(",");
								// -----------------------------

								Perf95StringBuilder.Append(rowPerf.Year.ToString());
								// -----------------------------
								Perf95StringBuilder.Append(",");
								// -----------------------------
							
								Perf95StringBuilder.Append(rowPerf.Period);
								// -----------------------------
								Perf95StringBuilder.Append(",");
								// -----------------------------
							
								/*
								 * Enter 0 or blank if this is the original report
								 * 
								 * Enter R to replace a previously submitted event.  To change any single entry or series of entries it is necessary to resubmit
								 * all data previously reported.  The R stands for REPLACE (not REVISE).  The system will take the new record and completely replace the 
								 * old one.  Columns 1-14 will be used to locate the previously submitted record.
								 * 
								 * Enter D to DELETE an ENTIRE record and repeat columns1-14 as previously reported.
								 * 
								 */

							
								Perf95StringBuilder.Append("0");
								// -----------------------------
								Perf95StringBuilder.Append(",");
								// -----------------------------
								
								if (rowPerf.IsNetMaxCapNull() == false)
								{
									if (rowPerf.NetMaxCap > 0)
										Perf95StringBuilder.Append(System.Math.Round(rowPerf.NetMaxCap,2).ToString("####.00"));
									else
										Perf95StringBuilder.Append(" ");
								}
								else
								{
									Perf95StringBuilder.Append(" ");
								}

								// -----------------------------
								Perf95StringBuilder.Append(",");
								// -----------------------------

								if (rowPerf.IsNetDepCapNull() == false)
								{
									if (rowPerf.NetDepCap > 0)
										Perf95StringBuilder.Append(System.Math.Round(rowPerf.NetDepCap,2).ToString("####.00"));
									else
										Perf95StringBuilder.Append(" ");
								}
								else
								{
									Perf95StringBuilder.Append(" ");
								}

								// -----------------------------
								Perf95StringBuilder.Append(",");
								// -----------------------------

								if (rowPerf.IsNetGenNull() == false)
								{
									if (rowPerf.NetMaxCap == 0 & rowPerf.NetDepCap == 0 & rowPerf.NetGen == 0)
										Perf95StringBuilder.Append(" ");
									else
										Perf95StringBuilder.Append(System.Math.Round(rowPerf.NetGen,2).ToString("#######.00"));
								}
								else
								{
									if (rowPerf.NetMaxCap == 0 & rowPerf.NetDepCap == 0)
										Perf95StringBuilder.Append(" ");
									else
										Perf95StringBuilder.Append("0");
								}
	
								// -----------------------------
								Perf95StringBuilder.Append(",");
								// -----------------------------

								if (rowPerf.IsTypUnitLoadingNull() == false)
									Perf95StringBuilder.Append(rowPerf.TypUnitLoading.ToString());
								else
									Perf95StringBuilder.Append(" ");
								
								// -----------------------------
								Perf95StringBuilder.Append(",");
								// -----------------------------

//								Perf95StringBuilder.Append(String.Empty.PadLeft(1,' '));

								if (rowPerf.IsAttemptedStartsNull() == false)
									Perf95StringBuilder.Append(rowPerf.AttemptedStarts.ToString().PadLeft(3,' '));
								else
									Perf95StringBuilder.Append(" ");
								
								// -----------------------------
								Perf95StringBuilder.Append(",");
								// -----------------------------

								if (rowPerf.IsActualStartsNull() == false)
									Perf95StringBuilder.Append(rowPerf.ActualStarts.ToString().PadLeft(3,' '));
								else
									Perf95StringBuilder.Append(" ");
								
								// -----------------------------
								Perf95StringBuilder.Append(",");
								// -----------------------------
							
//								Perf95StringBuilder.Append(String.Empty.PadLeft(3,' '));

								if (rowPerf.IsVerbalDescNull() == false)
								{
									strTemp = rowPerf.VerbalDesc.ToUpper().Replace(","," ").PadRight(19,' ').Substring(0,19);

									if (strTemp.Trim() == string.Empty)
										Perf95StringBuilder.Append(" ");
									else
										Perf95StringBuilder.Append(strTemp.Trim());
								}
								else
									Perf95StringBuilder.Append(" ");
								
								// -----------------------------
								Perf95StringBuilder.Append(",");
								// -----------------------------
					
								// PJM requires 2 decimal places for the hours, but NERC/NYISO only want whole hours

								Perf95StringBuilder.Append(Decimal.Round(rowPerf.ServiceHours,   2).ToString("###.00"));
								// -----------------------------
								Perf95StringBuilder.Append(",");
								// -----------------------------

								Perf95StringBuilder.Append(Decimal.Round(rowPerf.RSHours,        2).ToString("###.00"));
								// -----------------------------
								Perf95StringBuilder.Append(",");
								// -----------------------------

								Perf95StringBuilder.Append(Decimal.Round(rowPerf.PumpingHours,   2).ToString("###.00"));
								// -----------------------------
								Perf95StringBuilder.Append(",");
								// -----------------------------

								Perf95StringBuilder.Append(Decimal.Round(rowPerf.SynchCondHours, 2).ToString("###.00"));
		
								Perf95FileWriter.WriteLine(Perf95StringBuilder.ToString());
								rowPerf.PJMLoadStatus = "S";
								rowPerf.PJMStatusDate = System.DateTime.UtcNow;

								/*
								* ----------------------------------
								*  Performance 03/04 records
								* ----------------------------------
								*/
														
								Perf99StringBuilder.Length = 0;
								Perf99StringBuilder.Insert(0,99);
								// -----------------------------
								Perf99StringBuilder.Append(",");
								// -----------------------------

								Perf99StringBuilder.Append(rowPerf.UtilityUnitCode.PadLeft(6,'0').Substring(0,3));
								// -----------------------------
								Perf99StringBuilder.Append(",");
								// -----------------------------

								Perf99StringBuilder.Append(rowPerf.UtilityUnitCode.PadLeft(6,'0').Substring(3,3));
								// -----------------------------
								Perf99StringBuilder.Append(",");
								// -----------------------------

								Perf99StringBuilder.Append(rowPerf.Year.ToString());
								// -----------------------------
								Perf99StringBuilder.Append(",");
								// -----------------------------
							
								Perf99StringBuilder.Append(rowPerf.Period);
								// -----------------------------
								Perf99StringBuilder.Append(",");
								// -----------------------------

								/*
								 * Enter 0 or blank if this is the original report
								 * 
								 * Enter R to replace a previously submitted event.  To change any single entry or series of entries it is necessary to resubmit
								 * all data previously reported.  The R stands for REPLACE (not REVISE).  The system will take the new record and completely replace the 
								 * old one.  Columns 1-14 will be used to locate the previously submitted record.
								 * 
								 * Enter D to DELETE an ENTIRE record and repeat columns1-14 as previously reported.
								 * 
								 */

								Perf99StringBuilder.Append("0");
								// -----------------------------
								Perf99StringBuilder.Append(",");
								// -----------------------------

								// Primary Fuel Data

								Perf99StringBuilder.Append(rowPerf.PriFuelCode);
								// -----------------------------
								Perf99StringBuilder.Append(",");
								// -----------------------------

								if (rowPerf.PriFuelCode.ToUpper() == "NU")
								{
									// Leave columns 18-24 blank when reporting data for Nuclear units
									Perf99StringBuilder.Append(" ");
								}
								else
								{
									if (rowPerf.IsPriQtyBurnedNull() == false)
										Perf99StringBuilder.Append(Decimal.Round(rowPerf.PriQtyBurned,2).ToString("#####.00"));
									else
										Perf99StringBuilder.Append(" ");
								}

								// -----------------------------
								Perf99StringBuilder.Append(",");
								// -----------------------------


								if (rowPerf.IsPriAvgHeatContentNull() == false)
									Perf99StringBuilder.Append(rowPerf.PriAvgHeatContent.ToString());
								else
									Perf99StringBuilder.Append(" ");

								// -----------------------------
								Perf99StringBuilder.Append(",");
								// -----------------------------
							
								if (rowPerf.IsPriPercentAshNull() == false && rowPerf.PriPercentAsh > 0)
									Perf99StringBuilder.Append(Decimal.Round(rowPerf.PriPercentAsh,1).ToString("##.0"));
								else
									Perf99StringBuilder.Append(" ");

								// -----------------------------
								Perf99StringBuilder.Append(",");
								// -----------------------------

								if (rowPerf.IsPriPercentMoistureNull() == false && rowPerf.PriPercentMoisture > 0)
									Perf99StringBuilder.Append(Decimal.Round(rowPerf.PriPercentMoisture,1).ToString("##.0"));
								else
									Perf99StringBuilder.Append(" ");
								
								// -----------------------------
								Perf99StringBuilder.Append(",");
								// -----------------------------

								if (rowPerf.IsPriPercentSulfurNull() == false && rowPerf.PriPercentSulfur > 0)
									Perf99StringBuilder.Append(Decimal.Round(rowPerf.PriPercentSulfur,1).ToString("0.0"));
								else
									Perf99StringBuilder.Append(" ");
								
								// -----------------------------
								Perf99StringBuilder.Append(",");
								// -----------------------------
							

								// Secondary Fuel Data

								if (rowPerf.IsSecFuelCodeNull() == false)
								{
									if (rowPerf.SecFuelCode.Trim() != String.Empty)
									{
									
										Perf99StringBuilder.Append(rowPerf.SecFuelCode);
										// -----------------------------
										Perf99StringBuilder.Append(",");
										// -----------------------------
											
										if (rowPerf.IsSecQtyBurnedNull() == false)
											Perf99StringBuilder.Append(Decimal.Round(rowPerf.SecQtyBurned,2).ToString("####.00"));
										else
											Perf99StringBuilder.Append(" ");
										
										// -----------------------------
										Perf99StringBuilder.Append(",");
										// -----------------------------

										if (rowPerf.IsSecAvgHeatContentNull() == false)
											Perf99StringBuilder.Append(rowPerf.SecAvgHeatContent.ToString());
										else
											Perf99StringBuilder.Append(" ");
										
										// -----------------------------
										Perf99StringBuilder.Append(",");
										// -----------------------------

										if (rowPerf.IsSecPercentAshNull() == false && rowPerf.SecPercentAsh > 0)
											Perf99StringBuilder.Append(Decimal.Round(rowPerf.SecPercentAsh,1).ToString("##.0"));
										else
											Perf99StringBuilder.Append(" ");
										
										// -----------------------------
										Perf99StringBuilder.Append(",");
										// -----------------------------

										if (rowPerf.IsSecPercentMoistureNull() == false && rowPerf.SecPercentMoisture > 0)
											Perf99StringBuilder.Append(Decimal.Round(rowPerf.SecPercentMoisture,1).ToString("##.0"));
										else
											Perf99StringBuilder.Append(" ");
										
										// -----------------------------
										Perf99StringBuilder.Append(",");
										// -----------------------------

										if (rowPerf.IsSecPercentSulfurNull() == false && rowPerf.SecPercentSulfur > 0)
											Perf99StringBuilder.Append(Decimal.Round(rowPerf.SecPercentSulfur,1).ToString("0.0"));
										else
											Perf99StringBuilder.Append(" ");
							 
										// -----------------------------
										Perf99StringBuilder.Append(",");
										// -----------------------------	
									}
									else
									{
										Perf99StringBuilder.Append(",,,,,,");
									}
								}
								else
								{
									Perf99StringBuilder.Append(",,,,,,");
								}

																	
								/*
								* ----------------------------------
								*  Performance 04 record
								* ----------------------------------
								*/
								if (rowPerf.RevisionCard4.ToUpper() != "X")
								{
									if (rowPerf.IsTerFuelCodeNull() == false)
									{
										if (rowPerf.TerFuelCode.Trim() != String.Empty)
										{
											
											// Tertiary Fuel Data

											Perf99StringBuilder.Append(rowPerf.TerFuelCode);
											// -----------------------------
											Perf99StringBuilder.Append(",");
											// -----------------------------	

											if (rowPerf.IsTerQtyBurnedNull() == false)
												Perf99StringBuilder.Append(Decimal.Round(rowPerf.TerQtyBurned,2).ToString("####.00"));
											else
												Perf99StringBuilder.Append(" ");
											
											// -----------------------------
											Perf99StringBuilder.Append(",");
											// -----------------------------	

											if (rowPerf.IsTerAvgHeatContentNull() == false)
												Perf99StringBuilder.Append(rowPerf.TerAvgHeatContent.ToString());
											else
												Perf99StringBuilder.Append(" ");

											// -----------------------------
											Perf99StringBuilder.Append(",");
											// -----------------------------	
								
											if (rowPerf.IsTerPercentAshNull() == false && rowPerf.TerPercentAsh > 0)
												Perf99StringBuilder.Append(Decimal.Round(rowPerf.TerPercentAsh,1).ToString("##.0"));
											else
												Perf99StringBuilder.Append(" ");

											// -----------------------------
											Perf99StringBuilder.Append(",");
											// -----------------------------

											if (rowPerf.IsTerPercentMoistureNull() == false && rowPerf.TerPercentMoisture > 0)
												Perf99StringBuilder.Append(Decimal.Round(rowPerf.TerPercentMoisture,1).ToString("##.0"));
											else
												Perf99StringBuilder.Append(" ");

											// -----------------------------
											Perf99StringBuilder.Append(",");
											// -----------------------------

											if (rowPerf.IsTerPercentSulfurNull() == false && rowPerf.TerPercentSulfur > 0)
												Perf99StringBuilder.Append(Decimal.Round(rowPerf.TerPercentSulfur,1).ToString("0.0"));
											else
												Perf99StringBuilder.Append(" ");

											// -----------------------------
											Perf99StringBuilder.Append(",");
											// -----------------------------

											// Quaternary Fuel Data

											if (rowPerf.IsQuaFuelCodeNull() == false)
											{
												if (rowPerf.QuaFuelCode.Trim() != String.Empty)
												{	
													if (rowPerf.IsQuaFuelCodeNull() == false)
														Perf99StringBuilder.Append(rowPerf.QuaFuelCode);
													else
														Perf99StringBuilder.Append(" ");

													// -----------------------------
													Perf99StringBuilder.Append(",");
													// -----------------------------

													if (rowPerf.IsQuaQtyBurnedNull() == false)
														Perf99StringBuilder.Append(Decimal.Round(rowPerf.QuaQtyBurned,2).ToString("####.00"));
													else
														Perf99StringBuilder.Append(" ");

													// -----------------------------
													Perf99StringBuilder.Append(",");
													// -----------------------------

													if (rowPerf.IsQuaAvgHeatContentNull() == false)
														Perf99StringBuilder.Append(rowPerf.QuaAvgHeatContent.ToString());
													else
														Perf99StringBuilder.Append(" ");
													
													// -----------------------------
													Perf99StringBuilder.Append(",");
													// -----------------------------

													if (rowPerf.IsQuaPercentAshNull() == false && rowPerf.QuaPercentAsh > 0)
														Perf99StringBuilder.Append(Decimal.Round(rowPerf.QuaPercentAsh,1).ToString("##.0"));
													else
														Perf99StringBuilder.Append(" ");

													// -----------------------------
													Perf99StringBuilder.Append(",");
													// -----------------------------

													if (rowPerf.IsQuaPercentMoistureNull() == false && rowPerf.QuaPercentMoisture > 0)
														Perf99StringBuilder.Append(Decimal.Round(rowPerf.QuaPercentMoisture,1).ToString("##.0"));
													else
														Perf99StringBuilder.Append(" ");
													
													// -----------------------------
													Perf99StringBuilder.Append(",");
													// -----------------------------

													if (rowPerf.IsQuaPercentSulfurNull() == false && rowPerf.QuaPercentSulfur > 0)
														Perf99StringBuilder.Append(Decimal.Round(rowPerf.QuaPercentSulfur,1).ToString("0.0"));
													else
														Perf99StringBuilder.Append("");

													// -----------------------------
													Perf99StringBuilder.Append(",");
													// -----------------------------

												}
												else
												{
													Perf99StringBuilder.Append(",,,,,,");
												}
											}
											else
											{
												Perf99StringBuilder.Append(",,,,,,");
											}											
										}
									}
									else
									{
										Perf99StringBuilder.Append(",,,,,,");
										Perf99StringBuilder.Append(",,,,,,");
									}
								}
								else
								{
									// rowPerf.RevisionCard4.ToUpper() == "X"

									Perf99StringBuilder.Append(",,,,,,");
									Perf99StringBuilder.Append(",,,,,,");
								}

								Perf99FileWriter.WriteLine(Perf99StringBuilder.ToString());	
								rowPerf.PJMLoadStatus = "S";
								rowPerf.PJMStatusDate = System.DateTime.UtcNow;
							}	
						}					
					}
					catch (System.Data.StrongTypingException e)
					{
						stringException = e.ToString();
						lResult = false;
						// MessageBox.Show(e.ToString());
					}

					#endregion

					#region Card 97 Data
					try
					{
						foreach (AllEventData.EventData01Row EventDriver in dsEvents.Tables["EventData01"].Rows)
						{
                            //Application.DoEvents();

							if (EventDriver.EventType == "NC" || 
								EventDriver.EventType == "PU" || 
								EventDriver.EventType == "CO" || 
								EventDriver.CauseCode == 7777 || 
								(EventDriver.CauseCode >= 9180 && EventDriver.CauseCode <= 9199) ||
								(EventDriver.StartDateTime.Month != intPeriod && EventDriver.IsEndDateTimeNull()) || 
								EventDriver.RevisionCard01 == "X")
							{
								// Non-curtailing Events are not going to be included in the output file
								// the other half of the if() is to say if it starts in some other period and is still open-ended then skip
								// do not include deleted event records in transmittal file
						
								lInternalOnly = true;
							}
							else
							{
								lInternalOnly = false;
							}

							if (lInternalOnly == false)
							{
								if (EventDriver.StartDateTime.Month == intPeriod || (EventDriver.StartDateTime.Month < intPeriod && !EventDriver.IsEndDateTimeNull()))
								{
									if (EventDriver.StartDateTime.Month == intPeriod || (EventDriver.StartDateTime.Month < intPeriod && EventDriver.EndDateTime.Month == intPeriod))
									{
										// The Event either starts in this month or starts in a prior month and is still open

										// Monthly - intPeriod = specific month

										// process the 01 and 02/03 cards 


										Event97StringBuilder.Length = 0;
										Event97StringBuilder.Insert(0,97);
										Event97StringBuilder.Append(",");
										// -----------------------------

										Event97StringBuilder.Append(EventDriver.UtilityUnitCode.PadLeft(6,'0').Substring(0,3));
										Event97StringBuilder.Append(",");
										// -----------------------------

										Event97StringBuilder.Append(EventDriver.UtilityUnitCode.PadLeft(6,'0').Substring(3,3));
										Event97StringBuilder.Append(",");
										// -----------------------------

										Event97StringBuilder.Append(EventDriver.Year.ToString());
										Event97StringBuilder.Append(",");
										// -----------------------------

										Event97StringBuilder.Append(EventDriver.EventNumber.ToString().PadLeft(4,'0'));
										Event97StringBuilder.Append(",");
										// -----------------------------

										// Event Index
										Event97StringBuilder.Append("01");
										Event97StringBuilder.Append(",");
										// -----------------------------

										/*
												* Enter 0 or blank if this is the original report
												* 
												* Enter R to replace a previously submitted event.  To change any single entry or series of entries it is necessary to resubmit
												* all data previously reported.  The R stands for REPLACE (not REVISE).  The system will take the new record and completely replace the 
												* old one.  Columns 1-14 will be used to locate the previously submitted record.
												* 
												* Enter D to DELETE an ENTIRE record and repeat columns1-14 as previously reported.
												* 
										*/
										if (EventDriver.StartDateTime.Month < intPeriod && EventDriver.EndDateTime.Month == intPeriod)
											Event97StringBuilder.Append("R");	
										else
											Event97StringBuilder.Append(0);

										Event97StringBuilder.Append(",");
										// -----------------------------


										// If index number is 2-48, leave event type blank
										Event97StringBuilder.Append(EventDriver.EventType);
										Event97StringBuilder.Append(",");
										// -----------------------------

										// IO (Interconnection Office) Code
										// If index number is 2-48, leave IO Code blank
										// The only valid IO Code allowed by the new PJM system is 0 or 9
										//    0 - means no IO Code is being submitted
										//    9 - means an outage or derating of a periodic routine nature (e.g., condenser cleaning, deslagging, etc) which started and ended
										//        during a single off peak period (2200-0800) (MO, SE, U1, U2, U3, D4, DE, D1, D2, D3)
										if (EventDriver.IsPJMIOCodeNull())
										{
											Event97StringBuilder.Append(" 0");
										}
										else
										{
											Event97StringBuilder.Append(" 0");
										}
										Event97StringBuilder.Append(",");
										// -----------------------------
							
										Event97StringBuilder.Append(EventDriver.StartDateTime.ToString("MM/dd/yyyy HH:mm"));
										Event97StringBuilder.Append(",");
										// -----------------------------
						
										if (EventDriver.IsEndDateTimeNull() == true)
										{
											// The End of Event date/time is blank (<null>)
											Event97StringBuilder.Append(" ");
										}
										else
										{
											// End of Event date/time is NOT empty
											if (EventDriver.EndDateTime.Month > intPeriod)
												Event97StringBuilder.Append(" ");
											else
												Event97StringBuilder.Append(EventDriver.EndDateTime.ToString("MM/dd/yyyy HH:mm"));
										}	
										Event97StringBuilder.Append(",");
										// -----------------------------

										if (EventDriver.IsNetAvailCapacityNull() == false && EventDriver.NetAvailCapacity > 0)
											Event97StringBuilder.Append(Math.Round(EventDriver.NetAvailCapacity,2).ToString("####.00"));
										else
											Event97StringBuilder.Append("0");
										
										Event97StringBuilder.Append(",");
										// -----------------------------
							
										Event97StringBuilder.Append(EventDriver.CauseCode.ToString().PadLeft(4, '0'));
										Event97StringBuilder.Append(",");
										// -----------------------------

										Event97StringBuilder.Append(EventDriver.ContribCode.ToString().PadLeft(1,' '));
										Event97StringBuilder.Append(",");
										// -----------------------------

										if (EventDriver.IsWorkStartedNull() == true || EventDriver.EventType == "RS")
										{
											// The Time Work Started date/time is blank (<null>)
											Event97StringBuilder.Append(" ");
										}
										else
										{
											if (EventDriver.WorkStarted > EventDriver.StartDateTime)
											{
												if (EventDriver.IsEndDateTimeNull())
												{
													// Time Work Started date/time is NOT empty
													Event97StringBuilder.Append(EventDriver.WorkStarted.ToString("MM/dd/yyyy HH:mm"));
												}
												else
												{
													if (EventDriver.WorkStarted < EventDriver.EndDateTime)
													{
														// Time Work Started date/time is NOT empty
														Event97StringBuilder.Append(EventDriver.WorkStarted.ToString("MM/dd/yyyy HH:mm"));
													}
													else
													{
														Event97StringBuilder.Append(" ");
													}
												}
											}
											else
											{
												Event97StringBuilder.Append(" ");
											}							
										}

										Event97StringBuilder.Append(",");
										// -----------------------------
									
										if (EventDriver.IsWorkEndedNull() == true || EventDriver.EventType == "RS")
										{
											// The Time Work Ended date/time is blank (<null>)
											Event97StringBuilder.Append(" ");
										}
										else
										{
											if (EventDriver.WorkEnded > EventDriver.StartDateTime)
											{
												if (EventDriver.IsEndDateTimeNull())
												{
													// Fill in the Time Work Ended date/time	
													Event97StringBuilder.Append(EventDriver.WorkEnded.ToString("MM/dd/yyyy HH:mm"));
												}
												else
												{
													if (EventDriver.WorkEnded < EventDriver.EndDateTime)
													{
														// Fill in the Time Work Ended date/time	
														Event97StringBuilder.Append(EventDriver.WorkEnded.ToString("MM/dd/yyyy HH:mm"));
													}
													else
													{
														Event97StringBuilder.Append(" ");
													}
												}
											}
											else
											{
												Event97StringBuilder.Append(" ");
											}
										}

										Event97StringBuilder.Append(",");
										// -----------------------------

										if (EventDriver.EventType == "RS")
											Event97StringBuilder.Append(" ");
										else
										{
											if (EventDriver.IsManhoursWorkedNull() == false && EventDriver.ManhoursWorked > 0)
												Event97StringBuilder.Append(EventDriver.ManhoursWorked.ToString().PadLeft(4,' '));
											else
												Event97StringBuilder.Append(" ");
										}

										Event97StringBuilder.Append(",");
										// -----------------------------

										Event97StringBuilder.Append(EventDriver.VerbalDesc86.ToUpper().Replace(","," "));
																	
										// -----------------------------
										
										// this became a requirement in March 2010 - RMF
										Event97StringBuilder.Append(",");

										if (EventDriver.IsFailureMechCodeNull() == false)
											Event97StringBuilder.Append(EventDriver.FailureMechCode);
										else
											Event97StringBuilder.Append(" ");

										Event97StringBuilder.Append(",");

										if (EventDriver.IsCauseCodeExtNull() == false)
											Event97StringBuilder.Append(EventDriver.CauseCodeExt);
										else
											Event97StringBuilder.Append(" ");

										Event97FileWriter.WriteLine(Event97StringBuilder.ToString());
										EventDriver.PJMLoadStatus = "S";
										EventDriver.PJMStatusDate = System.DateTime.UtcNow;


										if (!(EventDriver.StartDateTime.Month < intPeriod && EventDriver.EndDateTime.Month == intPeriod))
										{

											// The only thing you can revised on an open-ended event is the End of Event Date/time -- the rest you cannot change
											
											if (EventDriver.EventType == "D1" || EventDriver.EventType == "D2" || EventDriver.EventType == "D3" ||
												EventDriver.EventType == "D4" || EventDriver.EventType == "PD" || EventDriver.EventType == "DE" ||
												EventDriver.EventType == "RS" || EventDriver.EventType == "DP" || EventDriver.EventType == "DM" ||
												(EventDriver.StartDateTime.Month < intPeriod && EventDriver.EndDateTime.Month == intPeriod))
											{
												// deratings may only have index number 1 for each event number
												// RS is not included in the list to have additional cards 02-48
												// Level 1 Error Message 110
												// The only thing you can change on a prior open event is the end of event date time
											}
											else
											{
												// Additional cards 02-48 may be included to indicate other components worked on during a full outage
												// (PO, MO, U1, U2, U3, SF, SE) event
												// process the 04/05 - 98/99 cards

												foreach (AllEventData.EventData02Row EventDetail in EventDriver.GetChildRows("AdditionalWork"))
												{
													Event97StringBuilder.Length = 0;
													Event97StringBuilder.Insert(0,97);
													Event97StringBuilder.Append(",");
													// -----------------------------

													Event97StringBuilder.Append(EventDetail.UtilityUnitCode.PadLeft(6,'0').Substring(0,3));
													Event97StringBuilder.Append(",");
													// -----------------------------

													Event97StringBuilder.Append(EventDetail.UtilityUnitCode.PadLeft(6,'0').Substring(3,3));
													Event97StringBuilder.Append(",");
													// -----------------------------

													Event97StringBuilder.Append(EventDetail.Year.ToString());
													Event97StringBuilder.Append(",");
													// -----------------------------

													Event97StringBuilder.Append(EventDetail.EventNumber.ToString().PadLeft(4,'0'));
													Event97StringBuilder.Append(",");
													// -----------------------------

													// Event Index
													Event97StringBuilder.Append((EventDetail.EvenCardNumber/2).ToString().PadLeft(2, '0'));
													Event97StringBuilder.Append(",");
													// -----------------------------

													/*
														* Enter 0 or blank if this is the original report
														* 
														* Enter R to replace a previously submitted event.  To change any single entry or series of entries it is necessary to resubmit
														* all data previously reported.  The R stands for REPLACE (not REVISE).  The system will take the new record and completely replace the 
														* old one.  Columns 1-14 will be used to locate the previously submitted record.
														* 
														* Enter D to DELETE an ENTIRE record and repeat columns1-14 as previously reported.
														* 
														*/
													Event97StringBuilder.Append(0);	
													Event97StringBuilder.Append(",");
													// -----------------------------

													// If index number is 2-48, leave event type blank
													Event97StringBuilder.Append(" ");
													Event97StringBuilder.Append(",");
													// -----------------------------

													// IO (Interconnection Office) Code
													// If index number is 2-48, leave IO Code blank
													Event97StringBuilder.Append(" ");
													Event97StringBuilder.Append(",");
													// -----------------------------

													// Start Date and Time -- for Index Number 2-48 leave blank
													Event97StringBuilder.Append(" ");
													Event97StringBuilder.Append(",");
													// -----------------------------
									
													// End Date and Time -- for Index Number 2-48 leave blank
													Event97StringBuilder.Append(" ");
													Event97StringBuilder.Append(",");
													// -----------------------------

													// Net Available Capacity -- for Index Number 2-48 leave blank
													Event97StringBuilder.Append(" ");
													Event97StringBuilder.Append(",");
													// -----------------------------

													Event97StringBuilder.Append(EventDetail.CauseCode.ToString().PadLeft(4, '0'));
													Event97StringBuilder.Append(",");
													// -----------------------------

													Event97StringBuilder.Append(EventDetail.ContribCode.ToString().PadLeft(1,' '));
													Event97StringBuilder.Append(",");
													// -----------------------------

													if (EventDetail.IsWorkStartedNull() == true)
													{
														// The Time Work Started date/time is blank (<null>)
														Event97StringBuilder.Append(String.Empty.PadLeft(12,' '));
													}
													else
													{
														if (EventDetail.WorkStarted > EventDriver.StartDateTime)
														{
															if (EventDriver.IsEndDateTimeNull())
															{
																// Time Work Started date/time is NOT empty
																// Fill in the Time Work Started date/time	
																Event97StringBuilder.Append(EventDetail.WorkStarted.ToString("MM/dd/yyyy HH:mm"));
															}
															else
															{
																if (EventDetail.WorkStarted < EventDriver.EndDateTime)
																{
																	// Time Work Started date/time is NOT empty
																	// Fill in the Time Work Started date/time	
																	Event97StringBuilder.Append(EventDetail.WorkStarted.ToString("MM/dd/yyyy HH:mm"));
																}
																else
																{
																	Event97StringBuilder.Append(" ");
																}
															}
														}
														else
														{
															// According to Gary Schuck the PJM edit checks require that the Work Started be greater than the Start of Event
															// and cannot be equal to or less than the event start date/time
															Event97StringBuilder.Append(" ");
														}
														
													}
													Event97StringBuilder.Append(",");
													// -----------------------------
										
													if (EventDetail.IsWorkEndedNull() == true)
													{
														// The Time Work Ended date/time is blank (<null>)
														Event97StringBuilder.Append(" ");
													}
													else
													{
														if (EventDetail.WorkEnded > EventDriver.StartDateTime)
														{
															if (EventDriver.IsEndDateTimeNull())
															{
																// Time Work Ended date/time is NOT empty
																// Fill in the Time Work Ended date/time
																Event97StringBuilder.Append(EventDetail.WorkEnded.ToString("MM/dd/yyyy HH:mm"));		
															}
															else
															{
																if (EventDetail.WorkEnded < EventDriver.EndDateTime)
																{
																	// Time Work Ended date/time is NOT empty
																	// Fill in the Time Work Ended date/time
																	Event97StringBuilder.Append(EventDetail.WorkEnded.ToString("MM/dd/yyyy HH:mm"));		
																}
																else
																{
																	Event97StringBuilder.Append(" ");
																}
															}
														}
														else
														{
															Event97StringBuilder.Append(" ");
														}
													}

													Event97StringBuilder.Append(",");
													// -----------------------------

													if (EventDetail.IsManhoursWorkedNull() == false && EventDetail.ManhoursWorked > 0)
													{
														Event97StringBuilder.Append(EventDetail.ManhoursWorked.ToString());
													}
													else
													{
														Event97StringBuilder.Append(" ");
													}
													Event97StringBuilder.Append(",");
													// -----------------------------
									
													Event97StringBuilder.Append(EventDetail.VerbalDesc86.ToUpper().Replace(","," "));

													// this became a requirement in March 2010 - RMF
													Event97StringBuilder.Append(",");

													if (EventDetail.IsFailureMechCodeNull() == false)
														Event97StringBuilder.Append(EventDetail.FailureMechCode);
													else
														Event97StringBuilder.Append(" ");

													Event97StringBuilder.Append(",");
													
													if (EventDetail.IsCauseCodeExtNull() == false)
														Event97StringBuilder.Append(EventDetail.CauseCodeExt);
													else
														Event97StringBuilder.Append(" ");
							
													Event97FileWriter.WriteLine(Event97StringBuilder.ToString());
													EventDetail.PJMLoadStatus = "S";
													EventDetail.PJMStatusDate = System.DateTime.UtcNow;

													// ========================================================================================
							
												}
											}
										}
									}
								}
							}
						}
					}
					
					catch (System.Data.StrongTypingException e)
					{
						stringException = e.ToString();
						lResult = false;
						// MessageBox.Show(e.ToString());
					}

					#endregion

					if (lResult)
					{
						//TODO Insert the updates to Performance and Event databases for the change in PJMLoadStatus

						dtPerfChanges = dtPerformance.GetChanges();
						lResult = blConnect.LoadPJMPerformance(dtPerfChanges);

						if (lResult)
						{
							lResult = blConnect.LoadPJMEvents(dsEvents);
						}
					}
				}		
			}
			catch (Exception e)
			{
				stringException = e.ToString();
				lResult = false;
				// throw(e);
			}
			finally
			{
				Perf95FileWriter.Close();
				Perf99FileWriter.Close();
				Event97FileWriter.Close();
				blConnect.WriteStatusUTC("PJM", stringEvent97File, stringPerf95File, stringPerf99File);
			}
			if (lResult)
			{
				return "OK -- " + strFileName;
			}
			else
			{
				return "ERROR -- " + strFileName;
			}
		}

		
#endregion

		public string FileType
		{
			get
			{
				return WriteFileType;
			}
			set
			{
				WriteFileType = value;
			}
		}


#region MarkFinalPJMData

        public static string MarkFinalPJMData(int intYear, int intPeriod, IEnumerable aUnits, string connect, GADSNGBusinessLayer blConnect)
		{
			DataTable dtPerfChanges;			
			System.Collections.IEnumerator myEnumerator = aUnits.GetEnumerator();
			string stringException;
	
			bool lResult;
			string strUnit;
			
			//			string connect = "DataEntry";
            // GADSNGBusinessLayer blConnect = new GADSNGBusinessLayer(connect);	
			
			lResult = true;

			Performance.PerformanceDataDataTable dtPerformance;
	
			AllEventData dsEvents = new AllEventData();

			try
			{
				// Process this unit's data
				
				while (myEnumerator.MoveNext())
				{
                    //Application.DoEvents();	

					strUnit = myEnumerator.Current.ToString();

					// get the performance and event datatables

					dtPerformance = blConnect.GetPerformanceData(strUnit, intYear);			
					dsEvents = blConnect.GetBothEventSets(strUnit, intYear);
											
					try
					{
						foreach (Performance.PerformanceDataRow rowPerf in dtPerformance.Rows)
						{
                            //Application.DoEvents();

							if (Convert.ToInt32(rowPerf.Period) == intPeriod)
							{
								if (rowPerf.PJMLoadStatus == "A")
								{
									rowPerf.PJMLoadStatus = "F";
									rowPerf.PJMStatusDate = System.DateTime.UtcNow;						
								}
							}	
						}					
					}
					catch (System.Data.StrongTypingException e)
					{
						stringException = e.ToString();
						lResult = false;
					}

					try
					{
						foreach (AllEventData.EventData01Row EventDriver in dsEvents.Tables["EventData01"].Rows)
						{
                            //Application.DoEvents();

							if (EventDriver.EventType == "NC" || EventDriver.EventType == "PU" || EventDriver.EventType == "CO" || (EventDriver.StartDateTime.Month != intPeriod && EventDriver.IsEndDateTimeNull()))
							{
								// Non-curtailing Events are not going to be included in the output file
								// the other half of the if() is to say if it starts in some other period and is still open-ended then skip
							}
							else
							{
								if (EventDriver.StartDateTime.Month == intPeriod || (EventDriver.StartDateTime.Month < intPeriod && !EventDriver.IsEndDateTimeNull()))
								{
									if (EventDriver.StartDateTime.Month == intPeriod || (EventDriver.StartDateTime.Month < intPeriod && EventDriver.EndDateTime.Month == intPeriod))
									{
										if (EventDriver.IsPJMLoadStatusNull()) EventDriver.PJMLoadStatus = " ";

										if (EventDriver.PJMLoadStatus == "A")
										{
											if (!(EventDriver.StartDateTime.Month == intPeriod && EventDriver.IsEndDateTimeNull()))
											{
												EventDriver.PJMLoadStatus = "F";
												EventDriver.PJMStatusDate = System.DateTime.UtcNow;
											}
										}
										
										foreach (AllEventData.EventData02Row EventDetail in EventDriver.GetChildRows("AdditionalWork"))
										{	
											if (EventDriver.PJMLoadStatus == "A")
											{
												EventDetail.PJMLoadStatus = "F";
												EventDetail.PJMStatusDate = System.DateTime.UtcNow;
											}
										}
									}
								}
							}
						}
					}
					
					catch (System.Data.StrongTypingException e)
					{
						stringException = e.ToString();
						lResult = false;
					}

					if (lResult)
					{
						dtPerfChanges = dtPerformance.GetChanges();
						lResult = blConnect.LoadPJMPerformance(dtPerfChanges);

						if (lResult)
						{
							lResult = blConnect.LoadPJMEvents(dsEvents);
						}
					}
				}		
			}
			catch (Exception e)
			{
				stringException = e.ToString();
				lResult = false;
				// throw(e);
			}
			
			if (lResult)
			{
				return "Units successfully marked as Final";
			}
			else
			{
				return "ERROR -- Units NOT successfully marked as Final";
			}
		}

#endregion

#region AcceptPJMData

        public static string AcceptPJMData(int intYear, int intPeriod, IEnumerable aUnits, string connect, GADSNGBusinessLayer blConnect)
		{
			DataTable dtPerfChanges;			
			System.Collections.IEnumerator myEnumerator = aUnits.GetEnumerator();
			string stringException;
	
			bool lResult;
			string strUnit;
			
			//			string connect = "DataEntry";
            // GADSNGBusinessLayer blConnect = new GADSNGBusinessLayer(connect);	
			
			lResult = true;

			Performance.PerformanceDataDataTable dtPerformance;
	
			AllEventData dsEvents = new AllEventData();

			try
			{
				// Process this unit's data
				
				while (myEnumerator.MoveNext())
				{
                    //Application.DoEvents();	

					strUnit = myEnumerator.Current.ToString();

					// get the performance and event datatables

					dtPerformance = blConnect.GetPerformanceData(strUnit, intYear);			
					dsEvents = blConnect.GetBothEventSets(strUnit, intYear);
											
					try
					{
						foreach (Performance.PerformanceDataRow rowPerf in dtPerformance.Rows)
						{
                            //Application.DoEvents();

							if (Convert.ToInt32(rowPerf.Period) == intPeriod)
							{
								if (rowPerf.PJMLoadStatus == "S" || rowPerf.PJMLoadStatus == "R")
								{
									rowPerf.PJMLoadStatus = "A";
									rowPerf.PJMStatusDate = System.DateTime.UtcNow;						
								}
							}	
						}					
					}
					catch (System.Data.StrongTypingException e)
					{
						stringException = e.ToString();
						lResult = false;
					}

					try
					{
						foreach (AllEventData.EventData01Row EventDriver in dsEvents.Tables["EventData01"].Rows)
						{
                            //Application.DoEvents();

							if (EventDriver.EventType == "NC" || EventDriver.EventType == "PU" || EventDriver.EventType == "CO" || (EventDriver.StartDateTime.Month != intPeriod && EventDriver.IsEndDateTimeNull()))
							{
								// Non-curtailing Events are not going to be included in the output file
								// the other half of the if() is to say if it starts in some other period and is still open-ended then skip
							}
							else
							{
								if (EventDriver.StartDateTime.Month == intPeriod || (EventDriver.StartDateTime.Month < intPeriod && !EventDriver.IsEndDateTimeNull()))
								{
									if (EventDriver.StartDateTime.Month == intPeriod || (EventDriver.StartDateTime.Month < intPeriod && EventDriver.EndDateTime.Month == intPeriod))
									{
										if (EventDriver.IsPJMLoadStatusNull()) EventDriver.PJMLoadStatus = " ";

										if (EventDriver.PJMLoadStatus == "S" || EventDriver.PJMLoadStatus == "R")
										{
											EventDriver.PJMLoadStatus = "A";
											EventDriver.PJMStatusDate = System.DateTime.UtcNow;
										}
										
										foreach (AllEventData.EventData02Row EventDetail in EventDriver.GetChildRows("AdditionalWork"))
										{	
											if (EventDriver.PJMLoadStatus == "S" || EventDriver.PJMLoadStatus == "R")
											{
												EventDetail.PJMLoadStatus = "A";
												EventDetail.PJMStatusDate = System.DateTime.UtcNow;
											}
										}
									}
								}
							}
						}
					}
					
					catch (System.Data.StrongTypingException e)
					{
						stringException = e.ToString();
						lResult = false;
					}

					if (lResult)
					{
	
						dtPerfChanges = dtPerformance.GetChanges();
						lResult = blConnect.LoadPJMPerformance(dtPerfChanges);

						if (lResult)
						{
							lResult = blConnect.LoadPJMEvents(dsEvents);
						}
					}
				}		
			}
			catch (Exception e)
			{
				stringException = e.ToString();
				lResult = false;
				// throw(e);
			}
			
			if (lResult)
			{
				return "Units successfully marked as Accepted";
			}
			else
			{
				return "ERROR -- Units NOT successfully marked as Accepted";
			}
		}

#endregion

#region  WriteRevPJMFile 
        public static string WriteRevPJMFile(string strFileName, int intYear, int intPeriod, IEnumerable aUnits, string connect, GADSNGBusinessLayer blConnect)
		{
			DataTable dtPerfChanges;			
			int i;

			System.Collections.IEnumerator myEnumerator = aUnits.GetEnumerator();
			
			string stringFolder;
			string stringFileName;
			string stringPerf95File;
			string stringPerf99File;
			string stringEvent97File;
			string stringException;
			bool lError;
			lError = false;

			bool lInternalOnly;

			stringFileName = "Invalid file name";

			stringPerf95File = strFileName;
			stringPerf99File = strFileName;
			stringEvent97File = strFileName;

			i = strFileName.LastIndexOf(@"\");

			string strTestFile;
			strTestFile = "Test.txt";

			if (i > 0)
			{
				// strFileName has at least one \ -- minimum should be x:\xxxevtnn.dbf

				// typically stringFolder will be x:\yyyy\zzzzz\
				stringFolder  = strFileName.Substring(0, i);

				if (Directory.Exists(stringFolder) == false)
				{
					return "ERROR -- " + stringFolder + " is not a valid directory.";
				}

				if (stringFolder.EndsWith(@"\") == false)
				{
					stringFolder += @"\";
				}
				
				strTestFile = stringFolder + "Test.Txt";
			}
			else
			{
				i = strFileName.LastIndexOf(@":");

				if (i > 0)
				{
					stringFolder  = strFileName.Substring(0, i) + @":\";

					if (Directory.Exists(stringFolder) == false)
					{
						return "ERROR -- " + stringFolder + " is not a valid directory.";
					}
				
					strTestFile = stringFolder + "Test.Txt";
				}
			}

			// This creates a "temporary" TestFileWriter so that the Finally use compiles
			StreamWriter TestFileWriter = new StreamWriter(strTestFile);
			TestFileWriter.WriteLine("Testing on " + System.DateTime.Now.ToLongDateString());
			TestFileWriter.Close();

			try
			{	
				// Test to see if the entered strFileName is a valid file name
				TestFileWriter = new StreamWriter(strFileName,false);
				TestFileWriter.WriteLine("Testing on " + System.DateTime.Now.ToLongDateString());			
			}
			catch (ArgumentException e)
			{
				lError = true;
				stringException = e.ToString();
				stringFileName  = "Missing File Name";
			}
			catch (DirectoryNotFoundException e)
			{
				lError = true;
				stringException = e.ToString();
				stringFileName  = "Directory not found";
			}
			catch (IOException e)
			{
				lError = true;
				stringException = e.ToString();
				stringFileName  = "Invalid Syntax";
			}
			catch (Exception e)
			{
				lError = true;
				stringException = e.ToString();
				stringFileName = "General Exception or Invalid file name";
			}
			finally
			{
				TestFileWriter.Close();
			}
			
			if (lError == true)
			{
				return "ERROR -- " + stringFileName;
			}
		
			// NERC prefers a performance data file and a separate event data file

			i = strFileName.LastIndexOf(@"\");

			if (i > 0)
			{
				// strFileName has at least one \ -- minimum should be x:\xxxevtnn.dbf

				// stringFileName will be *.*
				stringFileName = strFileName.Substring(i + 1);

				// typically stringFolder will be x:\yyyy\zzzzz\
				stringFolder  = strFileName.Substring(0, i);

				if (Directory.Exists(stringFolder) == false)
				{
					return "ERROR -- " + stringFolder + " is not a valid directory.";
				}

				if (stringFolder.EndsWith(@"\") == false)
				{
					stringFolder += @"\";
				}
				stringPerf95File = stringFolder + "Card95_Rev_" + stringFileName;
				stringPerf99File = stringFolder + "Card99_Rev_" + stringFileName;
				stringEvent97File = stringFolder + "Card97_Rev_" + stringFileName;
			}
			else if(strFileName.LastIndexOf(@":") > 0)
			{
				i = strFileName.LastIndexOf(@":");

				stringFileName = strFileName.Substring(i + 1);

				stringFolder  = strFileName.Substring(0, i) + @":\";

				if (Directory.Exists(stringFolder) == false)
				{
					return "ERROR -- " + stringFolder + " is not a valid directory.";
				}
			
				stringPerf95File = stringFolder + "Card95_Rev_" + stringFileName;
				stringPerf99File = stringFolder + "Card99_Rev_" + stringFileName;
				stringEvent97File = stringFolder + "Card97_Rev_" + stringFileName;
			}
			else
			{
				stringFileName = strFileName;
				stringFolder = "";
				stringPerf95File = "Card95_Rev_" + strFileName;
				stringPerf99File = "Card99_Rev_" + strFileName;
				stringEvent97File = "Card97_Rev_" + strFileName;
			}

			if (stringPerf95File.ToUpper().EndsWith(".TXT"))
			{
				stringPerf95File = stringPerf95File.Replace(".TXT",".CSV");
				stringPerf95File = stringPerf95File.Replace(".txt",".csv");
			}

			if (stringEvent97File.ToUpper().EndsWith(".TXT"))
			{
				stringEvent97File = stringEvent97File.Replace(".TXT",".CSV");
				stringEvent97File = stringEvent97File.Replace(".txt",".csv");
			}

			if (stringPerf99File.ToUpper().EndsWith(".TXT"))
			{
				stringPerf99File = stringPerf99File.Replace(".TXT",".CSV");
				stringPerf99File = stringPerf99File.Replace(".txt",".csv");
			}
			
						
			StreamWriter Event97FileWriter;
			Event97FileWriter = new StreamWriter(stringEvent97File,false);
			Event97FileWriter.AutoFlush = true;
		
			StreamWriter Perf95FileWriter;
			Perf95FileWriter = new StreamWriter(stringPerf95File,false);
			Perf95FileWriter.AutoFlush = true;

			StreamWriter Perf99FileWriter;
			Perf99FileWriter = new StreamWriter(stringPerf99File,false);
			Perf99FileWriter.AutoFlush = true;

			StringBuilder Perf95StringBuilder;
			StringBuilder Perf99StringBuilder;
			StringBuilder Event97StringBuilder;

			Perf95StringBuilder = new StringBuilder(0, 1024);
			Perf99StringBuilder = new StringBuilder(0, 1024);
			Event97StringBuilder = new StringBuilder(0, 1024);

			Perf95StringBuilder.Length = 0;
			string Perf95Header = "95,UtilityCode,UnitCode,Year,Month,ReportRevCode," +
				"NMC,NDC,NAG,LoadChar,AttStarts,ActStarts,LoadDesc," + 
				"SH,RSH,PmpHrs,SynCondHrs";
			Perf95StringBuilder.Insert(0,Perf95Header);
			Perf95FileWriter.WriteLine(Perf95StringBuilder.ToString());

			Perf99StringBuilder.Length = 0;
			string Perf99Header = "99,UtilityCode,UnitCode,Year,Month,RevCode," +
				"PriFuelCode,PriQtyBurned,PriAvgHeatContent,PriPercentAsh,PriPercentMoisture,PriPercentSulfur," +
				"SecFuelCode,SecQtyBurned,SecAvgHeatContent,SecPercentAsh,SecPercentMoisture,SecPercentSulfur," + 
				"TerFuelCode,TerQtyBurned,TerAvgHeatContent,TerPercentAsh,TerPercentMoisture,TerPercentSulfur," + 
				"QuaFuelCode,QuaQtyBurned,QuaAvgHeatContent,QuaPercentAsh,QuaPercentMoisture,QuaPercentSulfur";
			Perf99StringBuilder.Insert(0,Perf99Header);
			Perf99FileWriter.WriteLine(Perf99StringBuilder.ToString());

			Event97StringBuilder.Length = 0;
			string Event97Header = "97,UtilityCode,UnitCode,Year,EventNumber,EventIndex,RevCode," +
				"EventType,IOCode,StartOfEvent,EndOfEvent,AvailCapacity,CauseCode," + 
				"ContribCode,WorkStarted,WorkCompleted,MHWorked,VerbalDesc";
			Event97StringBuilder.Insert(0,Event97Header);
			Event97FileWriter.WriteLine(Event97StringBuilder.ToString());

			bool lResult;
			string strUnit;
			string strTemp;

			Regex l_regex = new Regex("[0-9]{2}");
			Regex dt_regex = new Regex("[0-9]{8}");

			//			string connect = "DataEntry";
            // GADSNGBusinessLayer blConnect = new GADSNGBusinessLayer(connect);	
			
			lResult = true;

			Performance.PerformanceDataDataTable dtPerformance;
	
			AllEventData dsEvents = new AllEventData();

			try
			{
				// Process this unit's data
				
				while (myEnumerator.MoveNext())
				{
                    //Application.DoEvents();	

					strUnit = myEnumerator.Current.ToString();

					// get the performance and event datatables

					dtPerformance = blConnect.GetPerformanceData(strUnit, intYear);			
					dsEvents = blConnect.GetBothEventSets(strUnit, intYear);
											
					/*
					* ORIGINAL -
					* 
					* To delete a single card (other than 01 card), repeat columns 1-12 (80-column format)
					* as previously reported, enter an "X" in column 13 and enter the card number of the
					* record to be deleted in columns 79 and 80.
					* 
					* To delete an entire report, repeat columns 1-12 (80-column format) as previously reported
					* and enter an "X" in column 13.  Leave columns 79 and 80 blank.  ALL records (cards) for
					* that report period will be deleted.
					* 
					* October 2002 -
					* 
					* To delete data from one or more data fields, GADS recommends that you resubmit the entire
					* data set -- year-to-date -- for that unit (or all units you report) to GADS.  This 
					* procedure will insure that both you and the GADS database have the same records on file.
					* You have the option to find the record that has the highest revision code and then
					* increase this number by 1 or set all revision codes back to zero.
					* 
					*/
					
#region Write 95 and 99 Card Data
					try
					{
						foreach (Performance.PerformanceDataRow rowPerf in dtPerformance.Rows)
						{
                            //Application.DoEvents();

							if (Convert.ToInt32(rowPerf.Period) == intPeriod)
							{
								if (rowPerf.TimeStamp > rowPerf.PJMStatusDate)
								{
									if ( rowPerf.PJMLoadStatus == "A" && (rowPerf.RevMonthCard1 > rowPerf.PJMStatusDate || rowPerf.RevMonthCard2 > rowPerf.PJMStatusDate))
									{
										/*
											* ----------------------------------
											*  Card 95 data
											* ----------------------------------
										*/
			
										Perf95StringBuilder.Length = 0;
										Perf95StringBuilder.Insert(0,95);
										// -----------------------------
										Perf95StringBuilder.Append(",");
										// -----------------------------

										Perf95StringBuilder.Append(rowPerf.UtilityUnitCode.PadLeft(6,'0').Substring(0,3));
										// -----------------------------
										Perf95StringBuilder.Append(",");
										// -----------------------------

										Perf95StringBuilder.Append(rowPerf.UtilityUnitCode.PadLeft(6,'0').Substring(3,3));
										// -----------------------------
										Perf95StringBuilder.Append(",");
										// -----------------------------

										Perf95StringBuilder.Append(rowPerf.Year.ToString());
										// -----------------------------
										Perf95StringBuilder.Append(",");
										// -----------------------------
							
										Perf95StringBuilder.Append(rowPerf.Period);
										// -----------------------------
										Perf95StringBuilder.Append(",");
										// -----------------------------
							
										/*
										 * Enter 0 or blank if this is the original report
										 * 
										 * Enter R to replace a previously submitted event.  To change any single entry or series of entries it is necessary to resubmit
										 * all data previously reported.  The R stands for REPLACE (not REVISE).  The system will take the new record and completely replace the 
										 * old one.  Columns 1-14 will be used to locate the previously submitted record.
										 * 
										 * Enter D to DELETE an ENTIRE record and repeat columns1-14 as previously reported.
										 * 
										 */

							
										Perf95StringBuilder.Append("R");
										// -----------------------------
										Perf95StringBuilder.Append(",");
										// -----------------------------
								
										if (rowPerf.IsNetMaxCapNull() == false)
										{
											if (rowPerf.NetMaxCap > 0)
												Perf95StringBuilder.Append(System.Math.Round(rowPerf.NetMaxCap,2).ToString("####.00"));
											else
												Perf95StringBuilder.Append(" ");
										}
										else
										{
											Perf95StringBuilder.Append(" ");
										}

										// -----------------------------
										Perf95StringBuilder.Append(",");
										// -----------------------------

										if (rowPerf.IsNetDepCapNull() == false)
										{
											if (rowPerf.NetDepCap > 0)
												Perf95StringBuilder.Append(System.Math.Round(rowPerf.NetDepCap,2).ToString("####.00"));
											else
												Perf95StringBuilder.Append(" ");
										}
										else
										{
											Perf95StringBuilder.Append(" ");
										}

										// -----------------------------
										Perf95StringBuilder.Append(",");
										// -----------------------------

										if (rowPerf.IsNetGenNull() == false)
										{
											if (rowPerf.NetMaxCap == 0 & rowPerf.NetDepCap == 0 & rowPerf.NetGen == 0)
												Perf95StringBuilder.Append(" ");
											else
												Perf95StringBuilder.Append(System.Math.Round(rowPerf.NetGen,2).ToString("#######.00"));
										}
										else
										{
											if (rowPerf.NetMaxCap == 0 & rowPerf.NetDepCap == 0)
												Perf95StringBuilder.Append(" ");
											else
												Perf95StringBuilder.Append("0");
										}
	
										// -----------------------------
										Perf95StringBuilder.Append(",");
										// -----------------------------

										if (rowPerf.IsTypUnitLoadingNull() == false)
											Perf95StringBuilder.Append(rowPerf.TypUnitLoading.ToString());
										else
											Perf95StringBuilder.Append(" ");
								
										// -----------------------------
										Perf95StringBuilder.Append(",");
										// -----------------------------

										//								Perf95StringBuilder.Append(String.Empty.PadLeft(1,' '));

										if (rowPerf.IsAttemptedStartsNull() == false)
											Perf95StringBuilder.Append(rowPerf.AttemptedStarts.ToString().PadLeft(3,' '));
										else
											Perf95StringBuilder.Append(" ");
								
										// -----------------------------
										Perf95StringBuilder.Append(",");
										// -----------------------------

										if (rowPerf.IsActualStartsNull() == false)
											Perf95StringBuilder.Append(rowPerf.ActualStarts.ToString().PadLeft(3,' '));
										else
											Perf95StringBuilder.Append(" ");
								
										// -----------------------------
										Perf95StringBuilder.Append(",");
										// -----------------------------
							
										//								Perf95StringBuilder.Append(String.Empty.PadLeft(3,' '));

										if (rowPerf.IsVerbalDescNull() == false)
										{
											strTemp = rowPerf.VerbalDesc.ToUpper().Replace(","," ").PadRight(19,' ').Substring(0,19);

											if (strTemp.Trim() == string.Empty)
												Perf95StringBuilder.Append(" ");
											else
												Perf95StringBuilder.Append(strTemp.Trim());
										}
										else
											Perf95StringBuilder.Append(" ");
								
										// -----------------------------
										Perf95StringBuilder.Append(",");
										// -----------------------------
					
										// PJM requires 2 decimal places for the hours, but NERC/NYISO only want whole hours

										Perf95StringBuilder.Append(Decimal.Round(rowPerf.ServiceHours,   2).ToString("###.00"));
										// -----------------------------
										Perf95StringBuilder.Append(",");
										// -----------------------------

										Perf95StringBuilder.Append(Decimal.Round(rowPerf.RSHours,        2).ToString("###.00"));
										// -----------------------------
										Perf95StringBuilder.Append(",");
										// -----------------------------

										Perf95StringBuilder.Append(Decimal.Round(rowPerf.PumpingHours,   2).ToString("###.00"));
										// -----------------------------
										Perf95StringBuilder.Append(",");
										// -----------------------------

										Perf95StringBuilder.Append(Decimal.Round(rowPerf.SynchCondHours, 2).ToString("###.00"));
		
										Perf95FileWriter.WriteLine(Perf95StringBuilder.ToString());
										rowPerf.PJMLoadStatus = "S";
										rowPerf.PJMStatusDate = System.DateTime.UtcNow;

									}

									if ( rowPerf.PJMLoadStatus == "A" && (rowPerf.RevMonthCard3 > rowPerf.PJMStatusDate || rowPerf.RevMonthCard4 > rowPerf.PJMStatusDate))
									{
										/*
											* ----------------------------------
											*  Performance 03/04 records
											* ----------------------------------
										*/
								
										Perf99StringBuilder.Length = 0;
										Perf99StringBuilder.Insert(0,99);
										// -----------------------------
										Perf99StringBuilder.Append(",");
										// -----------------------------

										Perf99StringBuilder.Append(rowPerf.UtilityUnitCode.PadLeft(6,'0').Substring(0,3));
										// -----------------------------
										Perf99StringBuilder.Append(",");
										// -----------------------------

										Perf99StringBuilder.Append(rowPerf.UtilityUnitCode.PadLeft(6,'0').Substring(3,3));
										// -----------------------------
										Perf99StringBuilder.Append(",");
										// -----------------------------

										Perf99StringBuilder.Append(rowPerf.Year.ToString());
										// -----------------------------
										Perf99StringBuilder.Append(",");
										// -----------------------------
							
										Perf99StringBuilder.Append(rowPerf.Period);
										// -----------------------------
										Perf99StringBuilder.Append(",");
										// -----------------------------

										/*
										 * Enter 0 or blank if this is the original report
										 * 
										 * Enter R to replace a previously submitted event.  To change any single entry or series of entries it is necessary to resubmit
										 * all data previously reported.  The R stands for REPLACE (not REVISE).  The system will take the new record and completely replace the 
										 * old one.  Columns 1-14 will be used to locate the previously submitted record.
										 * 
										 * Enter D to DELETE an ENTIRE record and repeat columns1-14 as previously reported.
										 * 
										 */

										Perf99StringBuilder.Append("R");
										// -----------------------------
										Perf99StringBuilder.Append(",");
										// -----------------------------

										// Primary Fuel Data

										Perf99StringBuilder.Append(rowPerf.PriFuelCode);
										// -----------------------------
										Perf99StringBuilder.Append(",");
										// -----------------------------

										if (rowPerf.PriFuelCode.ToUpper() == "NU")
										{
											// Leave columns 18-24 blank when reporting data for Nuclear units
											Perf99StringBuilder.Append(" ");
										}
										else
										{
											if (rowPerf.IsPriQtyBurnedNull() == false)
												Perf99StringBuilder.Append(Decimal.Round(rowPerf.PriQtyBurned,2).ToString("#####.00"));
											else
												Perf99StringBuilder.Append(" ");
										}

										// -----------------------------
										Perf99StringBuilder.Append(",");
										// -----------------------------


										if (rowPerf.IsPriAvgHeatContentNull() == false)
											Perf99StringBuilder.Append(rowPerf.PriAvgHeatContent.ToString());
										else
											Perf99StringBuilder.Append(" ");

										// -----------------------------
										Perf99StringBuilder.Append(",");
										// -----------------------------
							
										if (rowPerf.IsPriPercentAshNull() == false && rowPerf.PriPercentAsh > 0)
											Perf99StringBuilder.Append(Decimal.Round(rowPerf.PriPercentAsh,1).ToString("##.0"));
										else
											Perf99StringBuilder.Append(" ");

										// -----------------------------
										Perf99StringBuilder.Append(",");
										// -----------------------------

										if (rowPerf.IsPriPercentMoistureNull() == false && rowPerf.PriPercentMoisture > 0)
											Perf99StringBuilder.Append(Decimal.Round(rowPerf.PriPercentMoisture,1).ToString("##.0"));
										else
											Perf99StringBuilder.Append(" ");
								
										// -----------------------------
										Perf99StringBuilder.Append(",");
										// -----------------------------

										if (rowPerf.IsPriPercentSulfurNull() == false && rowPerf.PriPercentSulfur > 0)
											Perf99StringBuilder.Append(Decimal.Round(rowPerf.PriPercentSulfur,1).ToString("0.0"));
										else
											Perf99StringBuilder.Append(" ");
								
										// -----------------------------
										Perf99StringBuilder.Append(",");
										// -----------------------------
							

										// Secondary Fuel Data

										if (rowPerf.IsSecFuelCodeNull() == false)
										{
											if (rowPerf.SecFuelCode.Trim() != String.Empty)
											{
									
												Perf99StringBuilder.Append(rowPerf.SecFuelCode);
												// -----------------------------
												Perf99StringBuilder.Append(",");
												// -----------------------------
											
												if (rowPerf.IsSecQtyBurnedNull() == false)
													Perf99StringBuilder.Append(Decimal.Round(rowPerf.SecQtyBurned,2).ToString("####.00"));
												else
													Perf99StringBuilder.Append(" ");
										
												// -----------------------------
												Perf99StringBuilder.Append(",");
												// -----------------------------

												if (rowPerf.IsSecAvgHeatContentNull() == false)
													Perf99StringBuilder.Append(rowPerf.SecAvgHeatContent.ToString());
												else
													Perf99StringBuilder.Append(" ");
										
												// -----------------------------
												Perf99StringBuilder.Append(",");
												// -----------------------------

												if (rowPerf.IsSecPercentAshNull() == false && rowPerf.SecPercentAsh > 0)
													Perf99StringBuilder.Append(Decimal.Round(rowPerf.SecPercentAsh,1).ToString("##.0"));
												else
													Perf99StringBuilder.Append(" ");
										
												// -----------------------------
												Perf99StringBuilder.Append(",");
												// -----------------------------

												if (rowPerf.IsSecPercentMoistureNull() == false && rowPerf.SecPercentMoisture > 0)
													Perf99StringBuilder.Append(Decimal.Round(rowPerf.SecPercentMoisture,1).ToString("##.0"));
												else
													Perf99StringBuilder.Append(" ");
										
												// -----------------------------
												Perf99StringBuilder.Append(",");
												// -----------------------------

												if (rowPerf.IsSecPercentSulfurNull() == false && rowPerf.SecPercentSulfur > 0)
													Perf99StringBuilder.Append(Decimal.Round(rowPerf.SecPercentSulfur,1).ToString("0.0"));
												else
													Perf99StringBuilder.Append(" ");
							 
												// -----------------------------
												Perf99StringBuilder.Append(",");
												// -----------------------------	
											}
											else
											{
												Perf99StringBuilder.Append(",,,,,,");
											}
										}
										else
										{
											Perf99StringBuilder.Append(",,,,,,");
										}

																	
										/*
										* ----------------------------------
										*  Performance 04 record
										* ----------------------------------
										*/
										if (rowPerf.RevisionCard4.ToUpper() != "X")
										{
											if (rowPerf.IsTerFuelCodeNull() == false)
											{
												if (rowPerf.TerFuelCode.Trim() != String.Empty)
												{
											
													// Tertiary Fuel Data

													Perf99StringBuilder.Append(rowPerf.TerFuelCode);
													// -----------------------------
													Perf99StringBuilder.Append(",");
													// -----------------------------	

													if (rowPerf.IsTerQtyBurnedNull() == false)
														Perf99StringBuilder.Append(Decimal.Round(rowPerf.TerQtyBurned,2).ToString("####.00"));
													else
														Perf99StringBuilder.Append(" ");
											
													// -----------------------------
													Perf99StringBuilder.Append(",");
													// -----------------------------	

													if (rowPerf.IsTerAvgHeatContentNull() == false)
														Perf99StringBuilder.Append(rowPerf.TerAvgHeatContent.ToString());
													else
														Perf99StringBuilder.Append(" ");

													// -----------------------------
													Perf99StringBuilder.Append(",");
													// -----------------------------	
								
													if (rowPerf.IsTerPercentAshNull() == false && rowPerf.TerPercentAsh > 0)
														Perf99StringBuilder.Append(Decimal.Round(rowPerf.TerPercentAsh,1).ToString("##.0"));
													else
														Perf99StringBuilder.Append(" ");

													// -----------------------------
													Perf99StringBuilder.Append(",");
													// -----------------------------

													if (rowPerf.IsTerPercentMoistureNull() == false && rowPerf.TerPercentMoisture > 0)
														Perf99StringBuilder.Append(Decimal.Round(rowPerf.TerPercentMoisture,1).ToString("##.0"));
													else
														Perf99StringBuilder.Append(" ");

													// -----------------------------
													Perf99StringBuilder.Append(",");
													// -----------------------------

													if (rowPerf.IsTerPercentSulfurNull() == false && rowPerf.TerPercentSulfur > 0)
														Perf99StringBuilder.Append(Decimal.Round(rowPerf.TerPercentSulfur,1).ToString("0.0"));
													else
														Perf99StringBuilder.Append(" ");

													// -----------------------------
													Perf99StringBuilder.Append(",");
													// -----------------------------

													// Quaternary Fuel Data

													if (rowPerf.IsQuaFuelCodeNull() == false)
													{
														if (rowPerf.QuaFuelCode.Trim() != String.Empty)
														{	
															if (rowPerf.IsQuaFuelCodeNull() == false)
																Perf99StringBuilder.Append(rowPerf.QuaFuelCode);
															else
																Perf99StringBuilder.Append(" ");

															// -----------------------------
															Perf99StringBuilder.Append(",");
															// -----------------------------

															if (rowPerf.IsQuaQtyBurnedNull() == false)
																Perf99StringBuilder.Append(Decimal.Round(rowPerf.QuaQtyBurned,2).ToString("####.00"));
															else
																Perf99StringBuilder.Append(" ");

															// -----------------------------
															Perf99StringBuilder.Append(",");
															// -----------------------------

															if (rowPerf.IsQuaAvgHeatContentNull() == false)
																Perf99StringBuilder.Append(rowPerf.QuaAvgHeatContent.ToString());
															else
																Perf99StringBuilder.Append(" ");
													
															// -----------------------------
															Perf99StringBuilder.Append(",");
															// -----------------------------

															if (rowPerf.IsQuaPercentAshNull() == false && rowPerf.QuaPercentAsh > 0)
																Perf99StringBuilder.Append(Decimal.Round(rowPerf.QuaPercentAsh,1).ToString("##.0"));
															else
																Perf99StringBuilder.Append(" ");

															// -----------------------------
															Perf99StringBuilder.Append(",");
															// -----------------------------

															if (rowPerf.IsQuaPercentMoistureNull() == false && rowPerf.QuaPercentMoisture > 0)
																Perf99StringBuilder.Append(Decimal.Round(rowPerf.QuaPercentMoisture,1).ToString("##.0"));
															else
																Perf99StringBuilder.Append(" ");
													
															// -----------------------------
															Perf99StringBuilder.Append(",");
															// -----------------------------

															if (rowPerf.IsQuaPercentSulfurNull() == false && rowPerf.QuaPercentSulfur > 0)
																Perf99StringBuilder.Append(Decimal.Round(rowPerf.QuaPercentSulfur,1).ToString("0.0"));
															else
																Perf99StringBuilder.Append("");

															// -----------------------------
															Perf99StringBuilder.Append(",");
															// -----------------------------

														}
														else
														{
															Perf99StringBuilder.Append(",,,,,,");
														}
													}
													else
													{
														Perf99StringBuilder.Append(",,,,,,");
													}											
												}
											}
											else
											{
												Perf99StringBuilder.Append(",,,,,,");
												Perf99StringBuilder.Append(",,,,,,");
											}
										}
										else
										{
											// rowPerf.RevisionCard4.ToUpper() == "X"

											Perf99StringBuilder.Append(",,,,,,");
											Perf99StringBuilder.Append(",,,,,,");
										}

										Perf99FileWriter.WriteLine(Perf99StringBuilder.ToString());	
										rowPerf.PJMLoadStatus = "S";
										rowPerf.PJMStatusDate = System.DateTime.UtcNow;

									}
								}	
							}
						}					
					}
					catch (System.Data.StrongTypingException e)
					{
						stringException = e.ToString();
						lResult = false;
						// MessageBox.Show(e.ToString());
					}

#endregion

#region Write 97 Card Data
					try
					{

						foreach (AllEventData.EventData01Row EventDriver in dsEvents.Tables["EventData01"].Rows)
						{
                            //Application.DoEvents();	
                            //Application.DoEvents();

							if (EventDriver.EventType == "NC" || 
								EventDriver.EventType == "PU" || 
								EventDriver.EventType == "CO" || 
								EventDriver.CauseCode == 7777 || 
								(EventDriver.CauseCode >= 9180 && EventDriver.CauseCode <= 9199) ||
								(EventDriver.StartDateTime.Month != intPeriod && EventDriver.IsEndDateTimeNull()) || 
								EventDriver.RevisionCard01 == "X")
							{
								// Non-curtailing Events are not going to be included in the output file
								// the other half of the if() is to say if it starts in some other period and is still open-ended then skip
								// do not include deleted event records in transmittal file
						
								lInternalOnly = true;
							}
							else
							{
								lInternalOnly = false;
							}

							if (lInternalOnly == true)
							{
								// Non-curtailing Events are not going to be included in the output file
								// the other half of the if() is to say if it starts in some other period and is still open-ended then skip
							}
							else
							{
								if (EventDriver.StartDateTime.Month == intPeriod || (EventDriver.StartDateTime.Month < intPeriod && !EventDriver.IsEndDateTimeNull()))
								{
									if (EventDriver.StartDateTime.Month == intPeriod || (EventDriver.StartDateTime.Month < intPeriod && EventDriver.EndDateTime.Month == intPeriod))
									{
										// The Event either starts in this month or starts in a prior month and is still open

										// Monthly - intPeriod = specific month

										// process the 01 and 02/03 cards 

										Event97StringBuilder.Length = 0;
										Event97StringBuilder.Insert(0,97);
										Event97StringBuilder.Append(",");
										// -----------------------------

										Event97StringBuilder.Append(EventDriver.UtilityUnitCode.PadLeft(6,'0').Substring(0,3));
										Event97StringBuilder.Append(",");
										// -----------------------------

										Event97StringBuilder.Append(EventDriver.UtilityUnitCode.PadLeft(6,'0').Substring(3,3));
										Event97StringBuilder.Append(",");
										// -----------------------------

										Event97StringBuilder.Append(EventDriver.Year.ToString());
										Event97StringBuilder.Append(",");
										// -----------------------------

										Event97StringBuilder.Append(EventDriver.EventNumber.ToString().PadLeft(4,'0'));
										Event97StringBuilder.Append(",");
										// -----------------------------

										// Event Index
										Event97StringBuilder.Append("01");
										Event97StringBuilder.Append(",");
										// -----------------------------

										/*
											* Enter 0 or blank if this is the original report
											* 
											* Enter R to replace a previously submitted event.  To change any single entry or series of entries it is necessary to resubmit
											* all data previously reported.  The R stands for REPLACE (not REVISE).  The system will take the new record and completely replace the 
											* old one.  Columns 1-14 will be used to locate the previously submitted record.
											* 
											* Enter D to DELETE an ENTIRE record and repeat columns1-14 as previously reported.
											* 
										*/
										Event97StringBuilder.Append("R");	
										Event97StringBuilder.Append(",");
										// -----------------------------


										// If index number is 2-48, leave event type blank
										Event97StringBuilder.Append(EventDriver.EventType);
										Event97StringBuilder.Append(",");
										// -----------------------------

										// IO (Interconnection Office) Code
										// If index number is 2-48, leave IO Code blank
										// The only valid IO Code allowed by the new PJM system is 0 or 9
										//    0 - means no IO Code is being submitted
										//    9 - means an outage or derating of a periodic routine nature (e.g., condenser cleaning, deslagging, etc) which started and ended
										//        during a single off peak period (2200-0800) (MO, SE, U1, U2, U3, D4, DE, D1, D2, D3)
										if (EventDriver.IsPJMIOCodeNull())
										{
											Event97StringBuilder.Append(" 0");
										}
										else
										{
											Event97StringBuilder.Append(" 0");
										}
										Event97StringBuilder.Append(",");
										// -----------------------------
							
										Event97StringBuilder.Append(EventDriver.StartDateTime.ToString("MM/dd/yyyy HH:mm"));
										Event97StringBuilder.Append(",");
										// -----------------------------
						
										if (EventDriver.IsEndDateTimeNull() == true)
										{
											// The End of Event date/time is blank (<null>)
											Event97StringBuilder.Append(" ");
										}
										else
										{
											// End of Event date/time is NOT empty
											if (EventDriver.EndDateTime.Month > intPeriod)
												Event97StringBuilder.Append(" ");
											else
												Event97StringBuilder.Append(EventDriver.EndDateTime.ToString("MM/dd/yyyy HH:mm"));
										}	
										Event97StringBuilder.Append(",");
										// -----------------------------

										if (EventDriver.IsNetAvailCapacityNull() == false && EventDriver.NetAvailCapacity > 0)
											Event97StringBuilder.Append(Math.Round(EventDriver.NetAvailCapacity,2).ToString("####.00"));
										else
											Event97StringBuilder.Append("0");
										
										Event97StringBuilder.Append(",");
										// -----------------------------
							
										Event97StringBuilder.Append(EventDriver.CauseCode.ToString().PadLeft(4, '0'));
										Event97StringBuilder.Append(",");
										// -----------------------------

										Event97StringBuilder.Append(EventDriver.ContribCode.ToString().PadLeft(1,' '));
										Event97StringBuilder.Append(",");
										// -----------------------------

										if (EventDriver.IsWorkStartedNull() == true || EventDriver.EventType == "RS")
										{
											// The Time Work Started date/time is blank (<null>)
											Event97StringBuilder.Append(" ");
										}
										else
										{
											if (EventDriver.WorkStarted > EventDriver.StartDateTime)
											{
												if (EventDriver.IsEndDateTimeNull())
												{
													// Time Work Started date/time is NOT empty
													Event97StringBuilder.Append(EventDriver.WorkStarted.ToString("MM/dd/yyyy HH:mm"));
												}
												else
												{
													if (EventDriver.WorkStarted < EventDriver.EndDateTime)
													{
														// Time Work Started date/time is NOT empty
														Event97StringBuilder.Append(EventDriver.WorkStarted.ToString("MM/dd/yyyy HH:mm"));
													}
													else
													{
														Event97StringBuilder.Append(" ");
													}
												}
											}
											else
											{
												Event97StringBuilder.Append(" ");
											}							
										}

										Event97StringBuilder.Append(",");
										// -----------------------------
									
										if (EventDriver.IsWorkEndedNull() == true || EventDriver.EventType == "RS")
										{
											// The Time Work Ended date/time is blank (<null>)
											Event97StringBuilder.Append(" ");
										}
										else
										{
											if (EventDriver.WorkEnded > EventDriver.StartDateTime)
											{
												if (EventDriver.IsEndDateTimeNull())
												{
													// Fill in the Time Work Ended date/time	
													Event97StringBuilder.Append(EventDriver.WorkEnded.ToString("MM/dd/yyyy HH:mm"));
												}
												else
												{
													if (EventDriver.WorkEnded < EventDriver.EndDateTime)
													{
														// Fill in the Time Work Ended date/time	
														Event97StringBuilder.Append(EventDriver.WorkEnded.ToString("MM/dd/yyyy HH:mm"));
													}
													else
													{
														Event97StringBuilder.Append(" ");
													}
												}
											}
											else
											{
												Event97StringBuilder.Append(" ");
											}
										}

										Event97StringBuilder.Append(",");
										// -----------------------------

										if (EventDriver.EventType == "RS")
											Event97StringBuilder.Append(" ");
										else
										{
											if (EventDriver.IsManhoursWorkedNull() == false && EventDriver.ManhoursWorked > 0)
												Event97StringBuilder.Append(EventDriver.ManhoursWorked.ToString().PadLeft(4,' '));
											else
												Event97StringBuilder.Append(" ");
										}

										Event97StringBuilder.Append(",");
										// -----------------------------

										Event97StringBuilder.Append(EventDriver.VerbalDesc86.ToUpper().Replace(","," "));

										// this became a requirement in March 2010 - RMF
										Event97StringBuilder.Append(",");

										if (EventDriver.IsFailureMechCodeNull() == false)
											Event97StringBuilder.Append(EventDriver.FailureMechCode);
										else
											Event97StringBuilder.Append(" ");

										Event97StringBuilder.Append(",");

										if (EventDriver.IsCauseCodeExtNull() == false)
											Event97StringBuilder.Append(EventDriver.CauseCodeExt);
										else
											Event97StringBuilder.Append(" ");

										if (EventDriver.TimeStamp > EventDriver.PJMStatusDate && EventDriver.PJMLoadStatus == "A")
										{
											Event97FileWriter.WriteLine(Event97StringBuilder.ToString());
											EventDriver.PJMLoadStatus = "S";
											EventDriver.PJMStatusDate = System.DateTime.UtcNow;
										}


										if (!(EventDriver.StartDateTime.Month < intPeriod && EventDriver.EndDateTime.Month == intPeriod))
										{

											// The only thing you can revised on an open-ended event is the End of Event Date/time -- the rest you cannot change
											
											if (EventDriver.EventType == "D1" || EventDriver.EventType == "D2" || EventDriver.EventType == "D3" ||
												EventDriver.EventType == "D4" || EventDriver.EventType == "PD" || EventDriver.EventType == "DE" ||
												EventDriver.EventType == "RS" || EventDriver.EventType == "DP" || EventDriver.EventType == "DM" ||
												(EventDriver.StartDateTime.Month < intPeriod && EventDriver.EndDateTime.Month == intPeriod))
											{
												// deratings may only have index number 1 for each event number
												// RS is not included in the list to have additional cards 02-48
												// Level 1 Error Message 110
												// The only thing you can change on a prior open event is the end of event date time
											}
											else
											{
												// Additional cards 02-48 may be included to indicate other components worked on during a full outage
												// (PO, MO, U1, U2, U3, SF, SE) event
												// process the 04/05 - 98/99 cards

												foreach (AllEventData.EventData02Row EventDetail in EventDriver.GetChildRows("AdditionalWork"))
												{
													Event97StringBuilder.Length = 0;
													Event97StringBuilder.Insert(0,97);
													Event97StringBuilder.Append(",");
													// -----------------------------

													Event97StringBuilder.Append(EventDetail.UtilityUnitCode.PadLeft(6,'0').Substring(0,3));
													Event97StringBuilder.Append(",");
													// -----------------------------

													Event97StringBuilder.Append(EventDetail.UtilityUnitCode.PadLeft(6,'0').Substring(3,3));
													Event97StringBuilder.Append(",");
													// -----------------------------

													Event97StringBuilder.Append(EventDetail.Year.ToString());
													Event97StringBuilder.Append(",");
													// -----------------------------

													Event97StringBuilder.Append(EventDetail.EventNumber.ToString().PadLeft(4,'0'));
													Event97StringBuilder.Append(",");
													// -----------------------------

													// Event Index
													Event97StringBuilder.Append((EventDetail.EvenCardNumber/2).ToString().PadLeft(2, '0'));
													Event97StringBuilder.Append(",");
													// -----------------------------

													/*
														* Enter 0 or blank if this is the original report
														* 
														* Enter R to replace a previously submitted event.  To change any single entry or series of entries it is necessary to resubmit
														* all data previously reported.  The R stands for REPLACE (not REVISE).  The system will take the new record and completely replace the 
														* old one.  Columns 1-14 will be used to locate the previously submitted record.
														* 
														* Enter D to DELETE an ENTIRE record and repeat columns1-14 as previously reported.
														* 
														*/
													Event97StringBuilder.Append("R");	
													Event97StringBuilder.Append(",");
													// -----------------------------

													// If index number is 2-48, leave event type blank
													Event97StringBuilder.Append(" ");
													Event97StringBuilder.Append(",");
													// -----------------------------

													// IO (Interconnection Office) Code
													// If index number is 2-48, leave IO Code blank
													Event97StringBuilder.Append(" ");
													Event97StringBuilder.Append(",");
													// -----------------------------

													// Start Date and Time -- for Index Number 2-48 leave blank
													Event97StringBuilder.Append(" ");
													Event97StringBuilder.Append(",");
													// -----------------------------
									
													// End Date and Time -- for Index Number 2-48 leave blank
													Event97StringBuilder.Append(" ");
													Event97StringBuilder.Append(",");
													// -----------------------------

													// Net Available Capacity -- for Index Number 2-48 leave blank
													Event97StringBuilder.Append(" ");
													Event97StringBuilder.Append(",");
													// -----------------------------

													Event97StringBuilder.Append(EventDetail.CauseCode.ToString().PadLeft(4, '0'));
													Event97StringBuilder.Append(",");
													// -----------------------------

													Event97StringBuilder.Append(EventDetail.ContribCode.ToString().PadLeft(1,' '));
													Event97StringBuilder.Append(",");
													// -----------------------------

													if (EventDetail.IsWorkStartedNull() == true)
													{
														// The Time Work Started date/time is blank (<null>)
														Event97StringBuilder.Append(String.Empty.PadLeft(12,' '));
													}
													else
													{
														if (EventDetail.WorkStarted > EventDriver.StartDateTime)
														{
															if (EventDriver.IsEndDateTimeNull())
															{
																// Time Work Started date/time is NOT empty
																// Fill in the Time Work Started date/time	
																Event97StringBuilder.Append(EventDetail.WorkStarted.ToString("MM/dd/yyyy HH:mm"));
															}
															else
															{
																if (EventDetail.WorkStarted < EventDriver.EndDateTime)
																{
																	// Time Work Started date/time is NOT empty
																	// Fill in the Time Work Started date/time	
																	Event97StringBuilder.Append(EventDetail.WorkStarted.ToString("MM/dd/yyyy HH:mm"));
																}
																else
																{
																	Event97StringBuilder.Append(" ");
																}
															}
														}
														else
														{
															// According to Gary Schuck the PJM edit checks require that the Work Started be greater than the Start of Event
															// and cannot be equal to or less than the event start date/time
															Event97StringBuilder.Append(" ");
														}
														
													}
													Event97StringBuilder.Append(",");
													// -----------------------------
										
													if (EventDetail.IsWorkEndedNull() == true)
													{
														// The Time Work Ended date/time is blank (<null>)
														Event97StringBuilder.Append(" ");
													}
													else
													{
														if (EventDetail.WorkEnded > EventDriver.StartDateTime)
														{
															if (EventDriver.IsEndDateTimeNull())
															{
																// Time Work Ended date/time is NOT empty
																// Fill in the Time Work Ended date/time
																Event97StringBuilder.Append(EventDetail.WorkEnded.ToString("MM/dd/yyyy HH:mm"));		
															}
															else
															{
																if (EventDetail.WorkEnded < EventDriver.EndDateTime)
																{
																	// Time Work Ended date/time is NOT empty
																	// Fill in the Time Work Ended date/time
																	Event97StringBuilder.Append(EventDetail.WorkEnded.ToString("MM/dd/yyyy HH:mm"));		
																}
																else
																{
																	Event97StringBuilder.Append(" ");
																}
															}
														}
														else
														{
															Event97StringBuilder.Append(" ");
														}
													}

													Event97StringBuilder.Append(",");
													// -----------------------------

													if (EventDetail.IsManhoursWorkedNull() == false && EventDetail.ManhoursWorked > 0)
													{
														Event97StringBuilder.Append(EventDetail.ManhoursWorked.ToString());
													}
													else
													{
														Event97StringBuilder.Append(" ");
													}
													Event97StringBuilder.Append(",");
													// -----------------------------
									
													Event97StringBuilder.Append(EventDetail.VerbalDesc86.ToUpper().Replace(","," "));

													// this became a requirement in March 2010 - RMF
													Event97StringBuilder.Append(",");

													if (EventDetail.IsFailureMechCodeNull() == false)
														Event97StringBuilder.Append(EventDetail.FailureMechCode);
													else
														Event97StringBuilder.Append(" ");

													Event97StringBuilder.Append(",");

													if (EventDetail.IsCauseCodeExtNull() == false)
														Event97StringBuilder.Append(EventDetail.CauseCodeExt);
													else
														Event97StringBuilder.Append(" ");

													if (EventDetail.TimeStamp > EventDetail.PJMStatusDate && EventDetail.PJMLoadStatus == "A")
													{
														Event97FileWriter.WriteLine(Event97StringBuilder.ToString());
														EventDetail.PJMLoadStatus = "S";
														EventDetail.PJMStatusDate = System.DateTime.UtcNow;
													}
							
													// ========================================================================================
							
												}
											}
										}
									}
								}
							}	
						}
					}
					
					catch (System.Data.StrongTypingException e)
					{
						stringException = e.ToString();
						lResult = false;
						// MessageBox.Show(e.ToString());
					}

					#endregion

					if (lResult)
					{
						//TODO Insert the updates to Performance and Event databases for the change in PJMLoadStatus

						dtPerfChanges = dtPerformance.GetChanges();
						lResult = blConnect.LoadPJMPerformance(dtPerfChanges);

						if (lResult)
						{
							lResult = blConnect.LoadPJMEvents(dsEvents);
						}
					}
				}		
			}
			catch (Exception e)
			{
				stringException = e.ToString();
				lResult = false;
				// throw(e);
			}
			finally
			{
				Perf95FileWriter.Close();
				Perf99FileWriter.Close();
				Event97FileWriter.Close();
				blConnect.WriteStatusUTC("PJM Rev", stringEvent97File, stringPerf95File, stringPerf99File);
			}
			if (lResult)
			{
				return "OK -- " + strFileName;
			}
			else
			{
				return "ERROR -- " + strFileName;
			}
		}

#endregion

	}
}
