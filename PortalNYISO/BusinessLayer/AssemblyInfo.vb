Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("GADS OS Data Entry Business Layer")> 
<Assembly: AssemblyDescription("GADS OS Data Entry Business Layer")> 
<Assembly: AssemblyCompany("GADS Open Source Project")> 
<Assembly: AssemblyProduct("GADS OS")> 
<Assembly: AssemblyCopyright("Copyright � 2011 by The Outercurve Foundation, All Rights Reserved.")> 
<Assembly: AssemblyTrademark("GADS OS is a service mark of GADS Open Source Project")>
<Assembly: CLSCompliant(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("93D88A0C-B2DF-4C7C-8D61-48C1E5FC5EC3")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:

<Assembly: AssemblyVersion("12.0.0.0")> 



<Assembly: AssemblyFileVersionAttribute("16.1.8.0")> 
<Assembly: ComVisibleAttribute(False)> 