﻿Imports System.Security.Principal
Imports PortalNYISO.WebUtilityModule
Imports BusinessLayer
Imports BusinessLayer.BusinessLayer
Imports PortalNYISO.Process
Imports System.Collections
'Imports Oracle.DataAccess.Client
Imports System.IO
Imports System.Windows

Public Class MainDE
    Inherits System.Web.UI.Page

    Public AppName As String = "GADSNG"
    Public connect As String = ""
    Public blConnect As GADSNGBusinessLayer
    Public dtSetup As ARWindowsUI.SetupDE.SetupDataTable
    Public drSetup As ARWindowsUI.SetupDE.SetupRow

    Private StartPeriodsArray As New ArrayList
    Private EndPeriodsArray As New ArrayList
    Private strDTFormat As String = "MMMM yyyy"
    Private UserName As String
    Private sScript As String = String.Empty
    Public strCulture As String = "en-US"
    Public userLang() As String
    Private lValidUser As Boolean

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            lblName.Text = "Hello " + Context.User.Identity.Name & "."
            UserName = Context.User.Identity.Name
        Catch ex As Exception
            lblName.Text = "User failed to ID."
            UserName = ""
        End Try

        Dim myWorkManager As New Process.WorkManager(Application("AppPath").ToString)

        If myWorkManager IsNot Nothing Then
            Try
                myWorkManager.LogUser(UserName, Now(), Application("IPString").ToString)
            Finally
                If myWorkManager IsNot Nothing Then
                    myWorkManager = Nothing
                End If
            End Try
        End If

        lblAuthType.Text = "Version " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString()

        Dim connect As String = Application("AppPath").ToString
        Dim blConnect As ARWindowsUI.ARWindowsUI.Calcs = Nothing

        Try

            blConnect = New ARWindowsUI.ARWindowsUI.Calcs(connect)

        Catch

            'Dim sScript As String = String.Empty
            sScript = "<script language='javascript'>"
            sScript += "alert('Could not connect (65)');"
            sScript += "</script>"

        End Try

        If Not IsPostBack Then
            Session.Add("strDateRange", String.Empty)
            Session.Add("ErrorMessage", String.Empty)
        End If

        If UserName.Trim <> String.Empty Then

            If blConnect.CountUsersDefined() > 0 Then
                ' There are users defined in the NGUsers table
                If blConnect.IsUserValid(UserName) = 0 Then
                    ' The user ID Tiger\Ron is not a valid user
                    'if myuser.LastIndexOf("\") 
                    UserName = UserName.Substring(UserName.LastIndexOf("\") + 1)
                    ' Try just the part to the right of the "\"
                    If blConnect.IsUserValid(UserName) = 0 Then
                        Session("ErrorMessage") = "Contact the ISO for assistance (001)"
                        HaveErrors("Contact the ISO for assistance (001)")
                        'Response.Redirect("NotAuthorized.aspx", True)
                        'Exit Sub
                    End If
                End If

                ' see if there are any units assigned to this person

                Dim intGroups As Integer = 0
                intGroups = blConnect.IsUserAssignedToDEGroup(UserName.Trim)
                If intGroups > 0 Then
                    ' assigned to one or more Groups
                    Dim GroupIDReader As IDataReader
                    Dim UnitsReader As IDataReader
                    Dim GroupArray As New ArrayList
                    Dim UnitArray As New ArrayList
                    'Dim myTest As Integer
                    Dim intValue As Integer
                    Dim strValue As String

                    GroupIDReader = blConnect.GetUserDEGroupsReader(UserName.Trim)

                    While GroupIDReader.Read()
                        GroupArray.Add(CInt(GroupIDReader.GetValue(0)))
                    End While

                    ' always call Close when done reading
                    GroupIDReader.Close()

                    dtSetup = blConnect.GetUtilityUnitData()

                    For Each drSetup As ARWindowsUI.SetupDE.SetupRow In dtSetup
                        drSetup.UnitSelected = False
                    Next

                    dtSetup.AcceptChanges()

                    'Dim PermsReader As IDataReader
                    'Dim intGroupID As Integer

                    For intValue = 0 To (GroupArray.Count - 1)

                        UnitsReader = blConnect.GetUnitsInDEGroup(CInt(GroupArray.Item(intValue)))

                        While UnitsReader.Read()
                            strValue = UnitsReader.GetString(0)
                            drSetup = dtSetup.FindByUtilityUnitCode(UnitsReader.GetString(0))
                            If drSetup Is Nothing Then
                                Session("ErrorMessage") = "Invalid or Missing Unit Code " & strValue
                            Else
                                drSetup.UnitSelected = True
                            End If
                        End While

                        UnitsReader.Close()

                    Next

                    For Each drSetup As ARWindowsUI.SetupDE.SetupRow In dtSetup
                        If drSetup.UnitSelected = False Then
                            drSetup.Delete()
                        End If
                    Next

                    dtSetup.AcceptChanges()

                    If dtSetup.Rows.Count <= 0 Then
                        Session("ErrorMessage") = "Contact the ISO for assistance (002)"
                        HaveErrors("Contact the ISO for assistance (002)")
                        'Response.Redirect("NotAuthorized.aspx", True)
                        'Exit Sub
                    End If
                Else
                    Session("ErrorMessage") = "Contact the ISO for assistance (003)"
                    HaveErrors("Contact the ISO for assistance (003)")
                    'Response.Redirect("NotAuthorized.aspx", True)
                    'Exit Sub
                End If
            Else
                ' No users are defined in table -- probably first time or standalone
                Response.Redirect("NotAuthorized.aspx", True)
                Exit Sub
            End If

        Else
            Response.Redirect("NotAuthorized.aspx", True)
            Exit Sub
        End If

        If Not IsPostBack Then

            'Me.comboEndPeriod.Items.Clear()
            'Me.comboStartPeriod.Items.Clear()

            StartPeriodsArray.Clear()
            EndPeriodsArray.Clear()

            Dim rdrGetPeriods As IDataReader
            rdrGetPeriods = blConnect.GetMyPeriodsList("Monthly")

            While rdrGetPeriods.Read

                If Not rdrGetPeriods.IsDBNull(0) Then
                    StartPeriodsArray.Add(New StartList(rdrGetPeriods.GetDateTime(0).ToString(strDTFormat), rdrGetPeriods.GetDateTime(0)))
                    EndPeriodsArray.Add(New EndList(rdrGetPeriods.GetDateTime(0).ToString(strDTFormat), rdrGetPeriods.GetDateTime(0)))
                End If

            End While

            rdrGetPeriods.Close()
            rdrGetPeriods.Dispose()

            'Me.comboStartPeriod.DataSource = StartPeriodsArray
            'Me.comboStartPeriod.DataTextField = "PeriodDesc"
            'Me.comboStartPeriod.DataValueField = "PeriodValue"
            'Me.comboStartPeriod.DataBind()

            'Try
            '    If Me.comboStartPeriod.Items.Count > 0 Then
            '        If Me.comboStartPeriod.Items.Count > 12 Then
            '            Me.comboStartPeriod.SelectedIndex = Me.comboStartPeriod.Items.Count - 12
            '        Else
            '            Me.comboStartPeriod.SelectedIndex = 0
            '        End If
            '    End If
            'Catch ex As System.ArgumentOutOfRangeException
            '    'strError = ex.ToString
            'End Try

            'Me.comboEndPeriod.DataSource = EndPeriodsArray
            'Me.comboEndPeriod.DataTextField = "PeriodDesc"
            'Me.comboEndPeriod.DataValueField = "PeriodValue"
            'Me.comboEndPeriod.DataBind()

            'Try
            '    If Me.comboEndPeriod.Items.Count > 0 Then
            '        Me.comboEndPeriod.SelectedIndex = Me.comboEndPeriod.Items.Count - 1
            '    End If
            'Catch ex As System.ArgumentOutOfRangeException
            '    'strError = ex.ToString
            'End Try

        End If

    End Sub

    Public Sub HaveErrors(ByVal strErrors As String)

        Dim strErrorPage As String

        strErrorPage = "NotAuthorized.aspx"

        Try
            ' This always fails on a thread exception, but gives the correct display anyway.
            Response.Redirect(strErrorPage, True)

        Catch ex As System.Threading.ThreadAbortException
            strErrorPage = ex.ToString
        Catch ex As System.Exception
            strErrorPage = ex.ToString
        Finally
            strErrorPage = ""
        End Try

    End Sub

    Protected Sub UploadButton_Click(sender As Object, e As EventArgs) Handles UploadButton.Click
        'Try

        'Catch ex As Exception

        'End Try
        Dim SQL As String = "UPDATE WebProcess SET STATUS = 5, REASONFORFAILURE = 'Browser closed during processing'  WHERE STATUS IN (0,1) AND USERNAME = '" & UserName & "'"
        Dim connect As String

        connect = Application("AppPath").ToString
        Dim blConnect As New ARWindowsUI.ARWindowsUI.Calcs(Application("AppPath").ToString)

        blConnect.intExecuteNonQuery(SQL, Nothing)

        Response.Redirect("~/secure/DataUpload.aspx")
    End Sub

    Protected Sub EditButton_Click(sender As Object, e As EventArgs) Handles EditButton.Click
        Response.Redirect("~/secure/DEApp.aspx")
    End Sub

    Protected Sub ReportButton_Click(sender As Object, e As EventArgs) Handles ReportButton.Click
        Response.Redirect("~/secure/ReportsPage.aspx")
    End Sub

End Class