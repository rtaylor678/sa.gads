﻿Public Class GenericError
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ErrorTextLabel.Text = Request.QueryString("error")
        Catch ex As Exception
            ErrorTextLabel.Text = "No error reported."
        End Try
    End Sub

    Protected Sub HomeButton_Click(sender As Object, e As EventArgs) Handles HomeButton.Click
        Response.Redirect("~/mainde.aspx")
    End Sub
End Class