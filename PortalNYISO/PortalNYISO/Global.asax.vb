﻿Imports System.Web
Imports System.Web.SessionState
Imports System.Uri
Imports System.Web.HttpRequest
Imports System.Xml
'Imports System.Web.Optimization
Imports System.Web.Security
Imports System.Security.Principal

Public Class Global_asax
    Inherits System.Web.HttpApplication

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the application is started
        Application("Worker") = New Process.Worker(Me)
    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the session is started
        If Application("Worker") Is Nothing Then 'in case an error terminated worker
            Application.Lock()
            Application("Worker") = New Process.Worker(Me)
            Application.UnLock()
        End If

    End Sub

    Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires at the beginning of each request
        ' Build the Application Path

        'If Application("AppPath") Is Nothing Then
        '    Dim sAbsUri As String = Request.Url.AbsoluteUri
        '    Dim sRawUrl As String = Request.RawUrl
        '    If Request.ApplicationPath = "/" Then
        '        Application("AppPath") = Left(sAbsUri, Len(sAbsUri) - Len(sRawUrl))
        '    Else
        '        Application("AppPath") = Left(sAbsUri, Len(sAbsUri) - Len(sRawUrl)) & Request.ApplicationPath
        '    End If
        'End If

        Application("PhysicalAppPath") = Request.PhysicalApplicationPath
        Application("AppPath") = Request.ServerVariables("APPL_PHYSICAL_PATH")
        Application("ServerMapPath") = Server.MapPath(".")
        Application("BatchErrors") = Server.MapPath("BatchErrors.rdlc")
        Application("webconfig") = Server.MapPath("~/web.config")

        'PhysicalAppPath - D:\PortalNYISO\PortalNYISO\ 
        'AppPath - D:\PortalNYISO\PortalNYISO\ 
        'ServerMapPath - D:\PortalNYISO\PortalNYISO\Secure 

        Dim context As HttpApplication = DirectCast(sender, HttpApplication)
        Dim ipAddress As String = context.Request.UserHostAddress
        Application("IPString") = ipAddress

    End Sub

    Sub Application_AuthenticateRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires upon attempting to authenticate the user
        Dim App As HttpApplication
        Dim strError As String = String.Empty

        App = CType(sender, HttpApplication)

        Try

            ' $$$$$ --LDAP Authentication-----------------------------------------
            ' This event handler retrieves the authentication cookie from the Context.Request.Cookies collection, 
            ' decrypts the cookie, and retrieves the list of groups that will be stored in the
            ' FormsAuthenticationTicket.UserData property. The groups appear in a pipe-separated list that is
            ' created in the Logon.aspx page.

            ' The code parses the string in a string array to create a GenericPrincipal object.
            ' After the GenericPrincipal object is created, this object is placed in the HttpContext.User property. 

            Dim cookieName As String = FormsAuthentication.FormsCookieName
            Dim authCookie As HttpCookie = Context.Request.Cookies(cookieName)

            If (authCookie Is Nothing) Then
                'There is no authentication cookie.
                Return
            End If

            Dim authTicket As FormsAuthenticationTicket = Nothing

            Try
                authTicket = FormsAuthentication.Decrypt(authCookie.Value)
            Catch ex As Exception
                strError = ex.ToString & vbCrLf & ex.Message
                Dim WorkManager2 As New Process.WorkManager(Request.ServerVariables("APPL_PHYSICAL_PATH"))            
                WorkManager2.LogMessageToDB(LogTypes.WebError, "Application_AuthenticateRequest", ex.Message, ex.ToString)
                Return
            End Try

            If (authTicket Is Nothing) Then
                'Cookie failed to decrypt.
                Return
            End If

            'When the ticket was created, the UserData property was assigned a
            'pipe-delimited string of group names.
            Dim groups As String() = authTicket.UserData.Split(New Char() {"|"})

            'Create an Identity.
            Dim id As GenericIdentity = New GenericIdentity(authTicket.Name, "LdapAuthentication")

            'This principal flows throughout the request.
            Dim principal As GenericPrincipal = New GenericPrincipal(id, groups)

            ' $$$$$ --LDAP Authentication-----------------------------------------

            'Attach the new principal object to the current HttpContext object
            Context.User = principal
            Application("IdentityName") = Context.User.Identity.Name

        Catch ex As Exception
            strError = ex.ToString & vbCrLf & ex.Message
            Dim WorkManager As New Process.WorkManager(Request.ServerVariables("APPL_PHYSICAL_PATH"))          
            WorkManager.LogMessageToDB(LogTypes.WebError, "Application_AuthenticateRequest", ex.Message, ex.ToString)
        End Try

    End Sub

    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when an error occurs
        Dim LastError As Exception = Server.GetLastError
        Dim ErrorMessage As String = String.Empty
        Dim Value As Integer

        If Not IsNothing(LastError.InnerException) Then

            If LastError.GetType Is GetType(HttpUnhandledException) And LastError.InnerException.Message = "Maximum request length exceeded." Then 'do not like using text check, but there isnt a specific exception type for this
                Dim XMLReader As New XmlTextReader(Server.MapPath("~/web.config"))
                Try
                    While XMLReader.Read
                        If XMLReader.Name = "httpRuntime" Then
                            XMLReader.MoveToAttribute("maxRequestLength")
                            XMLReader.ReadAttributeValue()
                            Value = CInt(XMLReader.Value)
                            Exit While
                        End If
                    End While
                    ErrorMessage = "Uploaded file sizes have exceeded maximum. The maximum amount of bytes that can be uploaded in a single request is " & Format(Value, "d") & " KB."
                Catch ex As Exception
                    Dim WorkManager As New Process.WorkManager(Server.MapPath("~"))
                    WorkManager.LogMessageToDB(LogTypes.WebError, "Error receiving web request. (" & ex.Message & ")")
                Finally
                    XMLReader.Close()
                End Try
            Else
                ErrorMessage = LastError.Message
            End If
            Application("LastError") = ErrorMessage

        End If

        Server.ClearError()

    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the session ends
        GC.Collect()
    End Sub

    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the application ends
        Application.Lock()
        Application("Worker") = Nothing
        Application.UnLock()
        GC.Collect()
    End Sub

    Private Sub Global_BeginRequest(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.BeginRequest

    End Sub

End Class