﻿Public Class GeneralError
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ErrorMessage As String = ""

        Try
            ErrorMessage = Session("LastError").ToString
        Catch ex As Exception
            ErrorMessage = Nothing
        End Try

        If ErrorMessage Is Nothing Then
            Try
                ErrorMessage = Application("LastError").ToString
            Catch ex As Exception
                ErrorMessage = Nothing
            End Try
            Application("LastError") = Nothing 'clear error after getting
        End If

        If ErrorMessage Is Nothing Then
            Try
                ErrorMessage = Application("ThreadError").ToString
            Catch ex As Exception
                ErrorMessage = Nothing
            End Try
            Application("ThreadError") = Nothing
        End If

        If ErrorMessage Is Nothing OrElse ErrorMessage = "" Then

            If Not Server.GetLastError Is Nothing Then
                ErrorMessage = Server.GetLastError.Message & vbCr & Server.GetLastError.InnerException.Message
                Server.ClearError()
                Dim WorkManager As New Process.WorkManager(Application("AppPath").ToString)
                WorkManager.LogMessageToDB(LogTypes.GeneralError, ErrorMessage)
            Else
                ErrorMessage = "No error reported."
            End If

        End If

        ErrorTextLabel.Text = ErrorMessage

    End Sub

    Protected Sub HomeButton_Click(sender As Object, e As EventArgs) Handles HomeButton.Click
        Response.Redirect("~/mainde.aspx")
    End Sub

End Class