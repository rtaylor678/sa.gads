﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CauseCodeTreeView.aspx.vb" Inherits="PortalNYISO.CauseCodeTreeView" EnableSessionState="True" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Cause Codes</title>
    <link href="../NewStyles.css" type="text/css" rel="stylesheet" />
    <script lang="jscript">        function CloseMe() { window.opener.__doPostBack('',''); close(); }</script>
    <script lang="jscript">
        var pWin
        function setParent() {
            pWin = top.window.opener
        }
        function reloadParent() {
            pWin.location.reload(true)
        }
        function tickleParent() {
            // pWin.timeDiv.innerText="Oh!..."
            // onload="self.focus();setParent();"
        }
    </script>
    <link href="../StylesMainDE.css" type="text/css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server" style="padding-left:5px;">
        <div>

            <a href="../MainDE.aspx">
                <asp:Image ID="Image1" ImageUrl="../images/logo.gif" runat="server" AlternateText="NYISO: New York Independent System Operator" /></a>
            <p>
                <asp:Label ID="myTitle" runat="server" Font-Bold="True"></asp:Label>
            </p>
            <p>
                <input onclick="CloseMe();" type="button" value="Close" />
            </p>
            <p>
            </p>
            <asp:TreeView ID="TreeView1" runat="server" OnSelectedNodeChanged="TreeView1_SelectedNodeChanged">
            </asp:TreeView>
            
            <p>
                <u><b>Instructions</b></u>
            </p>
            <p>
                (1) Expand the Cause Code tree to locate the desired cause code.<br />
                (2) Select the desired cause code by clicking on it - this causes it to be highlighted<br />
                (3) Press the <b>Close</b> button to close this window and return to the Data Entry
        form.<br />
            </p>
        </div>
    </form>
</body>
</html>
