﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="WebReportViewer.aspx.vb" Inherits="PortalNYISO.WebReportViewer" EnableViewState="true" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Standard EFORd Demand Statistics Export to Excel</title>
</head>
<body>
    <form id="form1" runat="server">
        <table style="width:100%">
            <tr style="width:100%">
                <td >
                    <asp:Button ID="btnExcel" runat="server" Text="Export to Excel" OnClick="btnExcel_Click" /></td>
            </tr>
                        <tr style="width:100%">
                <td style="width:100%">
                    <asp:Label ID="lblConfirm" Text="" runat="server"></asp:Label>
                </td>
            </tr>
            <tr style="width:100%">
                <td style="width:100%">
                    <hr />
                </td>
               
            </tr>
            <tr style="width:100%">
                <td style="width:100%">
                    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="Small" Height="600px" Width="100%" />
                   </td>
            </tr>
        </table>
        <asp:ScriptManager ID="Scriptmanager1" runat="server"></asp:ScriptManager>
    </form>
</body>
</html>
