﻿Imports BusinessLayer.BusinessLayer
Imports Microsoft.Reporting.WebForms
Imports System.Data.Common
Imports System.Data
Imports System.IO
Imports System.Web
Imports System.Collections

Public Class ListEventData
    Inherits System.Web.UI.Page

    Public AppName As String = "GADSNG"
    Public connect As String = ""
    'Public blConnect As GADSNGBusinessLayer
    Public dtSetup As BusinessLayer.Setup.SetupDataTable
    Public drSetup As BusinessLayer.Setup.SetupRow
    Public dsCauseCodes As DataSet
    Public dtPerformance As BusinessLayer.Performance.PerformanceDataDataTable
    Public drPerformance As BusinessLayer.Performance.PerformanceDataRow
    Public dsAllEvents As AllEventDataISO
    Public dsGridEvents As DataSet
    Public dsFailMech As DataSet
    Public dsCauseCodeExt As DataSet
    Public dsSavedVerbDesc As DataSet
    Public dsRS As BusinessLayer.dsBulkRS
    Public intCurrentYear As Integer
    Public strCurrentUnit As String = ""
    Public strCurrentPeriod As String = ""
    Public strCurrentMonthNo As String = ""
    'Public intSECauseCode As Integer
    'Public dtSEBeginning As DateTime
    Public myUser As String = ""
    Public alPeriods As New ArrayList
    Public lbUnitsText As String = ""
    Public mydtEvents As New DataTable
    Public sScript As String
    Public strUnitName As String
    Public dsAllEventsCopy As DataSet = New DataSet

    Private Sub ListEventData_Load(sender As Object, e As EventArgs) Handles Me.Load

        If Not IsPostBack Then

            dsAllEventsCopy.EnforceConstraints = False

            RestoreFromSession()

            strUnitName = Me.lbUnitsText

            If Not IsPostBack Then

                If Me.dsGridEvents.Tables("EventData01").Rows.Count = 0 Then

                    sScript = "<script language='javascript'>"
                    sScript += "alert('There are no event records to list');"
                    sScript += "</script>"
                    Response.Write(sScript)

                    Me.ReportViewer1.Visible = False

                    Exit Sub

                End If

                dsAllEventsCopy = Me.dsGridEvents.Copy()
                dsAllEventsCopy.Merge(dsCauseCodes)
                Session.Add("dsAllEventsCopy", dsAllEventsCopy)

            Else
                dsAllEventsCopy = CType(Session("dsAllEventsCopy"), DataSet)
            End If

            mydtEvents.Merge(dsGridEvents.Tables("EventData01"))
            mydtEvents.TableName = "EventData01"

            'ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/secure") & "\BatchErrors.rdlc"
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("crListEvents.rdlc")
            Dim ReportParams(0) As ReportParameter
            ReportParams(0) = New ReportParameter("UnitName", lbUnitsText)
            ReportViewer1.LocalReport.SetParameters(ReportParams)
            ReportViewer1.LocalReport.DisplayName = "GADS Event Listing Report"
            ReportViewer1.LocalReport.DataSources.Clear()
            ReportViewer1.LocalReport.EnableExternalImages = True
            ReportViewer1.ProcessingMode = ProcessingMode.Local
            ReportViewer1.AsyncRendering = False
            ReportViewer1.SizeToReportContent = True
            ReportViewer1.LocalReport.DataSources.Add(New ReportDataSource("EventData", mydtEvents))
            ReportViewer1.LocalReport.Refresh()

        End If

    End Sub

#Region " RestoreFromSession() "

    Public Sub RestoreFromSession()

        AppName = Session("AppName").ToString
        connect = Session("connect").ToString
        'blConnect = CType(Session("blConnect"), BusinessLayer.BusinessLayer.GADSNGBusinessLayer)
        dtSetup = CType(Session("dtSetup"), BusinessLayer.Setup.SetupDataTable)
        drSetup = CType(Session("drSetup"), BusinessLayer.Setup.SetupRow)
        dsCauseCodes = CType(Session("dsCauseCodes"), DataSet)
        dtPerformance = CType(Session("dtPerformance"), BusinessLayer.Performance.PerformanceDataDataTable)
        drPerformance = CType(Session("drPerformance"), BusinessLayer.Performance.PerformanceDataRow)
        dsAllEvents = CType(Session("dsAllEvents"), AllEventDataISO)
        dsGridEvents = CType(Session("dsGridEvents"), DataSet)
        dsFailMech = CType(Session("dsFailMech"), DataSet)
        dsCauseCodeExt = CType(Session("dsCauseCodeExt"), DataSet)
        dsSavedVerbDesc = CType(Session("dsSavedVerbDesc"), DataSet)
        dsRS = CType(Session("dsRS"), BusinessLayer.dsBulkRS)
        intCurrentYear = CType(Session("intCurrentYear"), Integer)
        strCurrentUnit = Session("strCurrentUnit").ToString
        strCurrentPeriod = Session("strCurrentPeriod").ToString
        strCurrentMonthNo = Session("strCurrentMonthNo").ToString
        'intSECauseCode = CType(Session("intSECauseCode"), Integer)
        'dtSEBeginning = CType(Session("dtSEBeginning"), DateTime)
        'myUser = Session("myUser").ToString
        myUser = Context.User.Identity.Name
        alPeriods = CType(Session("alPeriods"), ArrayList)
        lbUnitsText = Session("lbUnitsText").ToString

    End Sub

#End Region

    Protected Sub btnPDF_Click(sender As Object, e As EventArgs) Handles btnPDF.Click

        Dim warnings As Warning() = Nothing
        Dim streamids As String() = Nothing
        Dim mimeType As String = Nothing
        Dim encoding As String = Nothing
        Dim extension As String = Nothing
        Dim bytes As Byte()

        RestoreFromSession()
        mydtEvents.Clear()
        mydtEvents.Merge(dsGridEvents.Tables("EventData01"))
        mydtEvents.TableName = "EventData01"

        ReportViewer1.LocalReport.ReportPath = Server.MapPath("crListEvents.rdlc")
        Dim ReportParams(0) As ReportParameter
        ReportParams(0) = New ReportParameter("UnitName", lbUnitsText)
        ReportViewer1.LocalReport.SetParameters(ReportParams)
        ReportViewer1.LocalReport.DisplayName = "GADS Event Listing Report"
        ReportViewer1.LocalReport.DataSources.Clear()
        ReportViewer1.LocalReport.EnableExternalImages = True
        ReportViewer1.ProcessingMode = ProcessingMode.Local
        ReportViewer1.AsyncRendering = False
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.LocalReport.DataSources.Add(New ReportDataSource("EventData", mydtEvents))

        bytes = ReportViewer1.LocalReport.Render("PDF", Nothing, mimeType, encoding, extension, streamids, warnings)

        ' Now that you have all the bytes representing the Excel report, buffer it and send it to the client.
        Response.Buffer = True
        Response.Clear()
        Response.ContentType = mimeType
        Response.AddHeader("content-disposition", "attachment; filename=GADS_Event_Listing_Report." + extension)
        Response.BinaryWrite(bytes) ' create the file
        Response.Flush() ' send it to the client to download
    End Sub

   
End Class