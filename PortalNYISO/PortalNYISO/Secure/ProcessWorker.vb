Imports System.Threading
Imports System.Web
Imports System.Collections

Imports System.Collections.Specialized
Imports GADSDataInterfaces.ISOLoadASCIIData
Imports GADSDataInterfaces.LoadASCIIData
Imports GADSDataInterfaces

Namespace Process

    Public Class Worker

        Private _ThreadTimer As Threading.Timer
        Private _ServerPath As String
        Private _HttpServer As HttpApplication
        Private _WorkManager As WorkManager
        Private _Available As Boolean = True

        Public Enum ProcessStatus 'must be updated in WebProcessEnum table
            Queued = 0        'not yet processed
            Working           'processing
            Completed         'finished processing
            Rejected          'error found
            None              'when a user has never started a process or no process queued
            Aborted           'user cancelled queued item
            Invalid           'returned when requesting info on an invalid workid
            InvalidFileFormat 'for FileCheck type
        End Enum

        Public Enum ProcessType 'must be updated in WebProcessEnum table
            FileCheck = 0  'ASCII test routines
            DataCheck      'ISO routines
            Analysis       'Data analysis routines
            None           'for invalid processes, requests, etc.
        End Enum

        Public Enum LogTypes 'must be updated in SQL table
            Information = 0  'misc info
            Alert            'possible failure or warning
            GeneralError     'error source unknown
            DBError          'error on database operation
            FileError        'error on file operation
            CodeError        'error in source code
            ThreadError      'error in worker thread
            WebError         'IIS or other related web errors
        End Enum

        Public Sub ThreadStart(ByVal State As Object)
            Dim WorkManager As WorkManager = New WorkManager(_ServerPath)
            Dim thisUser As String = ""
            Try
                thisUser = _HttpServer.Application("IdentityName").ToString
            Catch ex As Exception
                thisUser = ""
            End Try
            'check for queued items
            If WorkManager.GetQueuedItemCount(thisUser) > 0 AndAlso _Available Then
                SyncLock GetType(Process.Worker)
                    _Available = False
                End SyncLock
                ProcessItems()
                _Available = True
            End If

        End Sub

        Private Sub ProcessItems()

            Dim SetupTable As BusinessLayer.Setup.SetupDataTable

            Dim BusinessLayer As BusinessLayer.BusinessLayer.GADSNGBusinessLayer

            BusinessLayer = New BusinessLayer.BusinessLayer.GADSNGBusinessLayer(_ServerPath)

            Dim WorkingInfo As ProcessInfo
            Dim thisUser As String = _HttpServer.Application("IdentityName").ToString
            Do While _WorkManager.GetQueuedItemCount(thisUser) > 0

                WorkingInfo = _WorkManager.GetProcessInfo(_WorkManager.GetNextQueuedItem(thisUser))

                Select Case WorkingInfo.ProcessType
                    Case ProcessType.FileCheck

                        Dim GroupIDReader As IDataReader
                        Dim GroupArray As New ArrayList

                        GroupIDReader = BusinessLayer.GetUserGroupsReader(WorkingInfo.User)

                        While GroupIDReader.Read()
                            GroupArray.Add(CInt(GroupIDReader.GetValue(0)))
                        End While

                        ' always call Close when done reading
                        GroupIDReader.Close()

                        Dim PermsReader As IDataReader
                        Dim intYearLimit As Integer = 1980

                        For intValue = 0 To (GroupArray.Count - 1)

                            PermsReader = BusinessLayer.GetPermissions(CInt(GroupArray.Item(intValue)))

                            While PermsReader.Read
                                intYearLimit = Math.Max(intYearLimit, CInt(PermsReader.GetValue(1)))
                            End While

                            PermsReader.Close()
                        Next

                        Dim ReturnValue As String = String.Empty

                        Try
                            _WorkManager.UpdateStatus(WorkingInfo.WorkID, ProcessStatus.Working)
                            ReturnValue = ISOLoadASCIIFile(WorkingInfo.UploadFile, _ServerPath, WorkingInfo.User, BusinessLayer, intYearLimit)

                            If ReturnValue.EndsWith("OK") Then
                                _WorkManager.UpdateStatus(WorkingInfo.WorkID, ProcessStatus.Completed)
                            ElseIf ReturnValue.StartsWith("EXCEPTION:") Then
                                _WorkManager.UpdateStatus(WorkingInfo.WorkID, ProcessStatus.Aborted, ReturnValue.Substring(10).Trim())
                            Else
                                _WorkManager.UpdateStatus(WorkingInfo.WorkID, ProcessStatus.Rejected, ReturnValue)
                            End If

                        Catch ex As Exception

                            If Not IsNothing(ex.InnerException) Then
                                _WorkManager.LogMessageToDB(LogTypes.ThreadError, "Error in file processing.", ex.Message, ex.InnerException.Message)
                            Else
                                _WorkManager.LogMessageToDB(LogTypes.ThreadError, "Error in file processing.", ex.Message)
                            End If

                            _WorkManager.UpdateStatus(WorkingInfo.WorkID, ProcessStatus.Aborted, ex.Message)

                        End Try

                    Case ProcessType.DataCheck

                        Try
                            _WorkManager.UpdateStatus(WorkingInfo.WorkID, ProcessStatus.Working)
                            SetupTable = BusinessLayer.GetUtilityUnitData()

                            Dim ReturnValue As String = String.Empty

                            Dim GetFromDictionary As HybridDictionary
                            GetFromDictionary = LanguageBuilder.LanguageBuilder.BuildDictionary("False", _ServerPath)

                            ReturnValue = ISOLoadCheck(WorkingInfo.User, BusinessLayer, SetupTable, GetFromDictionary)

                            If ReturnValue = "" Then
                                _WorkManager.UpdateStatus(WorkingInfo.WorkID, ProcessStatus.Completed)
                                _WorkManager.AddProcess(WorkingInfo.User, ProcessType.Analysis)     'queue up analysis processing
                            Else
                                _WorkManager.UpdateStatus(WorkingInfo.WorkID, ProcessStatus.Rejected, ReturnValue)
                            End If

                        Catch ex As Exception

                            If Not IsNothing(ex.InnerException) Then
                                _WorkManager.LogMessageToDB(LogTypes.ThreadError, "Error in data check processing.", ex.Message, ex.InnerException.Message)
                            Else
                                _WorkManager.LogMessageToDB(LogTypes.ThreadError, "Error in data check processing.", ex.Message)
                            End If

                            _WorkManager.UpdateStatus(WorkingInfo.WorkID, ProcessStatus.Aborted, ex.Message)

                        End Try

                    Case ProcessType.Analysis

                        Try
                            _WorkManager.UpdateStatus(WorkingInfo.WorkID, ProcessStatus.Working)

                            Dim ISOReturn As String = ""

                            BusinessLayer = New BusinessLayer.BusinessLayer.GADSNGBusinessLayer(_ServerPath)
                            SetupTable = BusinessLayer.GetUtilityUnitData()

                            ' this is where the call to AR goes - mimic above
                            ISOReturn = ISOSnapShot(WorkingInfo.User, BusinessLayer, _ServerPath)

                            If ISOReturn = "" Then
                                _WorkManager.UpdateStatus(WorkingInfo.WorkID, ProcessStatus.Completed)
                                ' Add report generation here
                                '_WorkManager.AddProcess(WorkingInfo.User, ProcessType.Analysis)								  'queue up analysis processing
                            Else
                                _WorkManager.UpdateStatus(WorkingInfo.WorkID, ProcessStatus.Rejected, ISOReturn)
                            End If

                            BusinessLayer = Nothing

                        Catch ex As Exception

                            If Not IsNothing(ex.InnerException) Then
                                _WorkManager.LogMessageToDB(LogTypes.ThreadError, WorkingInfo.WorkID.ToString("00000000") & " Error in analysis processing.", ex.Message, ex.InnerException.Message)
                            Else
                                _WorkManager.LogMessageToDB(LogTypes.ThreadError, WorkingInfo.WorkID.ToString("00000000") & " Error in analysis processing.", ex.Message)
                            End If

                            _WorkManager.UpdateStatus(WorkingInfo.WorkID, ProcessStatus.Aborted, ex.Message)

                        End Try

                End Select

            Loop

        End Sub

        Protected Overrides Sub Finalize()
            MyBase.Finalize()
            Try
                If Not IsNothing(_ThreadTimer) Then
                    _ThreadTimer.Dispose()
                End If
            Catch ex As Exception
                Dim strTemp As String
                strTemp = ex.ToString
            Finally
                _ThreadTimer = Nothing
            End Try
            _WorkManager = Nothing
        End Sub

        Public Sub New(ByVal App As HttpApplication)
            _HttpServer = App
            _ServerPath = _HttpServer.Server.MapPath("~")
            _WorkManager = New WorkManager(_ServerPath)
            '_WorkManager = New WorkManager("NYISOPortal")

            ' Timer Constructor (TimerCallback, Object, Int32, Int32)

            ' Public Sub New( _
            '        ByVal callback As TimerCallback, _
            '        ByVal state As Object, _
            '        ByVal dueTime As Integer, _
            '        ByVal period As Integer )

            'Parameters
            '------------
            '  callback
            '    Type: System.Threading.TimerCallback
            '    A TimerCallback delegate representing a method to be executed. 
            '
            '    Visual Basic users can omit the TimerCallback constructor, and 
            '       simply use the AddressOf operator when specifying the callback method. 
            '       Visual Basic automatically calls the correct delegate constructor. 
            ' -----------
            '  state
            '    Type: System.Object
            '    An object containing information to be used by the callback method, or 
            '       null reference (Nothing in Visual Basic).
            ' -----------
            '  dueTime
            '    Type: System.Int32
            '    The amount of time to delay before callback is invoked, in milliseconds. 
            '        Specify Timeout.Infinite to prevent the timer from starting. 
            '        Specify zero (0) to start the timer immediately.
            '
            '    If dueTime is zero (0), callback is invoked immediately. 
            '    If dueTime is Timeout.Infinite, callback is not invoked; the timer is disabled, 
            '        but can be re-enabled by calling the Change method. 
            ' -----------
            '  period
            '    Type: System.Int32
            '    The time interval between invocations of callback, in milliseconds. 
            '        Specify Timeout.Infinite to disable periodic signaling. 
            '
            '   If period is zero (0) or Infinite and dueTime is not Infinite, 
            '      callback is invoked once; the periodic behavior of the timer is disabled, 
            '      but can be re-enabled using the Change method. 
            ' -----------
            ' The delegate specified by the callback parameter is invoked once after dueTime elapses, and 
            ' thereafter each time the period time interval elapses.
            '
            ' The method specified for callback should be reentrant, because it is called on ThreadPool 
            ' threads. The method can be executed simultaneously on two thread pool threads 
            ' if the timer interval is less than the time required to execute the method, or 
            ' if all thread pool threads are in use and the method is queued multiple times. 
            ' -----------
            ' Initially, this line was:
            ' _ThreadTimer = New Threading.Timer(AddressOf ThreadStart, Nothing, 0, 10000)
            ' but it would not work at EUCG and resulted in object null reference exceptions
            ' because of the application pool not being responsive enough so I changed it as shown 
            ' below:

            _ThreadTimer = New Threading.Timer(AddressOf ThreadStart, Nothing, 0, 10000)
        End Sub

    End Class

End Namespace
