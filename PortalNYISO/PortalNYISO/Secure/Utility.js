	//gets any element on the page given its ID
	function GetElement(ElementID)
	{
		if(document.all) return document.getElementById(ElementID);
		else return document.all.item(ElementID);
	}
	
	//resizes an element
	function ResizeElement(ElementID, Width, Height, AddWidth, AddHeight)
	{
		var Item = GetElement(ElementID);
		if(AddWidth > 0 && Item !== undefined) Item.style.width = parseInt(Item.style.width,10) + AddWidth + "px";
		else if(Width > 0) Item.style.width = Width + "px";
		if(AddHeight > 0 && Item !== undefined) Item.style.height = parseInt(Item.style.height,10) + AddHeight + "px";
		else if(Height > 0) Item.style.height = Height + "px";
	}
	
	//increments a value of an element
	function IncrementElement(ElementID)
	{
		var Item = GetElement(ElementID);
		Item.value = parseInt(Item.value,10) + 1;
	}

	//gets any element from a document (for a different window)
	function GetElementFromDoc(Document, ElementID)
	{
		if(Document.all)
		{
			return Document.getElementById(ElementID);
		} else {
			return Document.all.item(ElementID);
		}
	}

	//this forces the element to anchor to the right and/or bottom edges of the browser
	function AnchorElement(ElementID, Padding, Right, Bottom)
	{
		var Item = GetElement(ElementID);
		if(Padding > 0 && Item !== undefined)
		{
			if(Right) Item.style.width = document.body.clientWidth - Padding;
			if(Bottom && document.body.clientHeight - Item.style.posTop > 0) Item.style.height = document.body.clientHeight - Item.style.posTop - Padding;
		} else {
			if(Right) Item.style.width = document.body.clientWidth;
			if(Bottom && (document.body.clientHeight - Item.style.posTop) > 0) Item.style.height = document.body.clientHeight - Item.style.posTop;
		}
	}
		
	//scrolls a scrollable element to a given position
	function ScrollTo(ElementID, YPos)
	{
		var Item = GetElement(ElementID);
		Item.scrollTop = YPos;
	}
	
	//set the value of the current scroll position of an element into a hidden field
	function SetScrollPos(ScrollElement, HiddenField)
	{
		var ScrollItem = GetElement(ScrollElement);
		var ScrollVal = GetElement(HiddenField);
		ScrollVal.value = ScrollItem.scrollTop;
	}
	
	
	//hides an element on the page
	function ShowElement(ElementID, Visible) //Visible should be true or false
	{
		var Item = GetElement(ElementID);
		if(document.all)//IE
		{
			if(Visible === true) Item.style.visibility = 'visible'; 
			else Item.style.visibility = 'hidden';
		} else { //other
			if(Visible === true) Item.visibility = 'show';
			else Item.visibility = 'hide';
		}
	}
	
	//enables or disables an element
	function EnableElement(ElementID, Enabled) //Enabled should be true or false
	{
		var Item = GetElement(ElementID);
		Item.disabled = (Enabled === true ? false : true);
	}
	
	//removes html from page
	function CollapseElement(ElementID)
	{
		var Item = GetElement(ElementID);
		Item.outerHTML = '';	
	}
	
	function RefreshImage(ElementID)

	{ // Only will work on IE4+
		if (document.all && ElementID !== "") {document.images[ElementID].src = document.images[ElementID].src;}
	}
	
