﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ErrorMessage.aspx.vb" Inherits="PortalNYISO.ErrorMessage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Error Message</title>
    <link href="Styles.css" type="text/css" rel="stylesheet" />
    <link href="../Styles.css" type="text/css" rel="stylesheet" />
</head>
<body>
    <h1 style="text-align: center">NYISO GADS Portal</h1>
    <h2 style="text-align: center">ERROR MESSAGE</h2>
    <form id="Form1" method="post" runat="server">
        <table style="text-align: center">
            <tr>
                <td>
                    <asp:Label ID="lblErrorMessage" runat="server">No Errors</asp:Label>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
