﻿Imports System.Collections
Imports ARWindowsUI
Imports Microsoft.Reporting.WebForms

Public Class WebReportViewer
    Inherits System.Web.UI.Page

    Public AppName As String = "GADSNG"
    Public connect As String = "Analysis"
    Public blConnect As ARWindowsUI.ARWindowsUI.Calcs

    Public strCauseCodes As String
    Public strReportLine1 As String
    Public strReportLine2 As String
    Public strReportLine3 As String
    Public strDateSelectionRange As String
    Public lFooter As Boolean = True
    Public strFooter As String = "Confidential"
    Public lJOReport As Boolean = False  ' Use Ownership Share generation and fuel data in reports -- get from reports dialog
    Private lIncludeFooter As Boolean = False
    Private lIncludeAssumptionsSummary As Boolean = False
    Private strCustomLine3 As String
    Public strReportFooter As String
  
    Private alFOSofE As ArrayList
    Private alFOEofE As ArrayList

    Private strPMCFilter As String = " (PlantMgtControl = 1) AND "
    'If Me.cbPlantMgtControl.Checked = True Then 
    '   frmCalcs.PMCFilter = " (PlantMgtControl = 1) AND "
    'End If
    Dim strGranularity As String = "PMCMonthly"

    Private cEFORd As String = "PJM"
    Private cEFORdFleet As String = "NERC2"
    Private der_nac_pref As Boolean = True
    'Private der_nac As Boolean = True
    Private intSHPumpSynCo As Integer = 0
    Private lDo3D As Boolean = False
    Private DSTotalArray As New HybridDictionary
    Private sortType As Integer = 2
    Private intW_Factor As Integer = 1
    Public mydtExcel As New DataTable
    Private strDateRange As String
    'Private reader As StreamReader
    Private strMode As String
    Public myUser As String = ""
    Private OrderByString As String = "ORDER BY UnitShortName, TL_DateTime"
    Private sScript As String = String.Empty

    Dim strSelect As String

    'Dim dtEFORdTotal As EFORd.EFORdDataTable
    Dim str1BOM As String
    Dim dtstr1BOM As System.DateTime
    Dim dtstr2 As System.DateTime

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then

            strDateRange = Session("strDateRange").ToString

            Me.DateSelectionRange = strDateRange

            Dim sScript As String
            Dim dtSetup As SetupDE.SetupDataTable
            Dim drSetup As SetupDE.SetupRow
            Dim intYearLimit As Integer = 1980
            Dim StringToPrint As String = String.Empty
            Dim AppName As String = "GADSNG"
            connect = Application("AppPath").ToString
            ' connect = Application("AppPath").ToString()

            Dim strTemp As String = String.Empty
            'reader = New IO.StreamReader(HttpContext.Current.Server.MapPath("~/Secure/GADSNG.xml"))

            Dim ALConnect As New ARWindowsUI.ARWindowsUI.Calcs(Application("AppPath").ToString)

            blConnect = New ARWindowsUI.ARWindowsUI.Calcs(Application("AppPath").ToString)

            dtSetup = GetUtilityUnitData()

            If dtSetup.Rows.Count > 0 Then
                ' find out what group(s) this person is assigned to -- could be more than one group

                myUser = Context.User.Identity.Name

                Dim intGroups As Integer = blConnect.IsUserAssignedToDEGroup(myUser)

                If intGroups > 0 Then
                    ' assigned to one or more DE Groups

                    Dim GroupIDReader As IDataReader
                    Dim UnitsReader As IDataReader
                    Dim GroupArray As New ArrayList
                    Dim UnitArray As New ArrayList

                    Dim intValue As Integer
                    Dim strValue As String

                    GroupIDReader = blConnect.GetUserDEGroupsReader(myUser)
                    While GroupIDReader.Read()
                        GroupArray.Add(CInt(GroupIDReader.GetValue(0)))
                    End While
                    ' always call Close when done reading
                    GroupIDReader.Close()

                    For Each drSetup In dtSetup
                        drSetup.UnitSelected = False
                    Next

                    dtSetup.AcceptChanges()

                    Dim intGroupID As Integer

                    For intValue = 0 To (GroupArray.Count - 1)

                        UnitsReader = blConnect.GetUnitsInDEGroup(CInt(GroupArray.Item(intValue)))

                        While UnitsReader.Read()

                            strValue = UnitsReader.GetString(0)
                            drSetup = dtSetup.FindByUtilityUnitCode(UnitsReader.GetString(0))

                            If drSetup Is Nothing Then
                                StringToPrint += "Invalid or Missing Unit Code " & strValue & "<br><br>"
                            Else
                                drSetup.UnitSelected = True
                            End If

                        End While

                        UnitsReader.Close()

                    Next

                    For Each drSetup In dtSetup
                        If drSetup.UnitSelected = False Then
                            drSetup.Delete()
                        End If
                    Next

                    dtSetup.AcceptChanges()

                    If dtSetup.Rows.Count = 0 Then

                        StringToPrint += "No Units Assigned to " & myUser & "<br><br>"

                        sScript = "<script language='javascript'>"
                        sScript += "alert('Could not generate report - No Units Assigned to " & myUser & "');"
                        sScript += "window.close();"
                        sScript += "</script>"
                        Response.Write(sScript)

                        Me.Visible = False

                        sScript = "<script language='javascript'>"
                        sScript += "window.close();"
                        sScript += "</script>"
                        Response.Write(sScript)

                        Exit Sub

                    End If

                    For Each drSetup In dtSetup
                        If drSetup.CheckStatus.Trim.ToUpper <> "O" Then
                            drSetup.Delete()
                        End If
                    Next

                    dtSetup.AcceptChanges()

                    If dtSetup.Rows.Count = 0 Then

                        StringToPrint += "No Units Assigned to " & myUser & " that have OK status<br><br>"

                        sScript = "<script language='javascript'>"
                        sScript += "alert('Could not generate report - no units have OK status');"
                        sScript += "window.close();"
                        sScript += "</script>"
                        Response.Write(sScript)

                        Me.Visible = False

                        sScript = "<script language='javascript'>"
                        sScript += "window.close();"
                        sScript += "</script>"
                        Response.Write(sScript)

                        Exit Sub

                    Else
                        'Dim reader As New IO.StreamReader(HttpContext.Current.Server.MapPath("~/Secure/GADSNG.xml"))
                        'Dim moXML As SetupSettings.Settings
                        'Dim strMyKey As String
                        'strMyKey = Server.MapPath("..") & "/secure/GADSNG.xml"
                        'moXML = New SetupSettings.Settings(AppName, strMyKey, reader)

                        ' Calculational settings from Admin Console

                        ' ========================================================================================
                        ' Deratings Calculations

                        '' der_nac and der_nac_pref can be either true or false
                        'Try
                        '    'der_nac_pref = Convert.ToBoolean(moXML.GetSettingStr("DERNAC", "Der_Nac", "True"))    ' See Calcs for translation to der_nac
                        '    der_nac_pref = Convert.ToBoolean(blConnect.GetAnalysisSettingStr("DERNAC", "Der_Nac", "True"))    ' See Calcs for translation to der_nac
                        'Catch ex As Exception
                        '    der_nac_pref = True      ' See Calcs for translation to der_nac
                        'End Try

                        blConnect.Der_NAC_pref = der_nac_pref

                        Try
                            ' Check to see how lDo3D interacts with lExample3D
                            'lDo3D = Convert.ToBoolean(moXML.GetSettingStr("EXAMPLE3D", "lExample3D", "False"))
                            lDo3D = Convert.ToBoolean(blConnect.GetAnalysisSettingStr("EXAMPLE3D", "lExample3D", "False"))
                        Catch ex As Exception
                            lDo3D = False
                        End Try

                        blConnect.lDo3D = lDo3D

                        ' ========================================================================================
                        ' EFORd Equation / EFORd Fleet Calculations

                        ' cEFORd can be either PJM or MARKOV -- all UPPERCASE
                        'cEFORd = moXML.GetSettingStr("EFORd", "cEFORd", "PJM").ToUpper.Trim
                        cEFORd = blConnect.GetAnalysisSettingStr("EFORd", "cEFORd", "PJM").ToUpper.Trim

                        If cEFORd <> "PJM" And cEFORd <> "MARKOV" Then
                            cEFORd = "PJM"
                        End If

                        blConnect.cEFORd = cEFORd

                        'cEFORdFleet = moXML.GetSettingStr("EFORd", "Fleet", "NERC2").ToUpper.Trim
                        cEFORdFleet = blConnect.GetAnalysisSettingStr("EFORd", "Fleet", "NERC2").ToUpper.Trim

                        If cEFORdFleet <> "PJM" And cEFORdFleet <> "NERC" And cEFORdFleet <> "NERC2" Then
                            cEFORdFleet = "NERC2"
                        End If

                        blConnect.cEFORdFleet = cEFORdFleet

                        ' ========================================================================================
                        ' Calculations that use Service Hours

                        '<Section Name="SHINCLUDES">
                        '  <Key Name="SHPumpSynCo" Value="0" />
                        ' SHPumpSynCo values are:
                        '    0 - Do not include in SH
                        '    1 - Include Pumping Hours only with SH
                        '    2 - Include Synchronous Condensing Hours only with SH
                        '    3 - Include BOTH Pumping and Synchronous Condensing Hours with SH
                        '</Section>

                        Try
                            'intSHPumpSynCo = moXML.GetSettingInt("SHINCLUDES", "SHPumpSynCo", 0)
                            intSHPumpSynCo = blConnect.GetAnalysisSettingInt("SHINCLUDES", "SHPumpSynCo", 0)
                        Catch ex As Exception
                            intSHPumpSynCo = 0
                        End Try


                        Try
                            'sortType = moXML.GetSettingInt("TSORT", "nSort", 2)
                            sortType = blConnect.GetAnalysisSettingInt("TSORT", "nSort", 2)
                        Catch ex As Exception
                            sortType = 2
                        End Try

                        blConnect.sortType = sortType

                        ' ========================================================================================
                        ' Rpt Settings

                        'strReportLine1 = moXML.GetSettingStr("TITLE", "Line1", "GADS NxL")
                        'strReportLine2 = moXML.GetSettingStr("TITLE", "Line2", "Analysis & Reporting")
                        'strReportLine3 = moXML.GetSettingStr("TITLE", "Line3", "Standard Reports")
                        strReportLine1 = blConnect.GetAnalysisSettingStr("TITLE", "Line1", "NYISO GADS")
                        strReportLine2 = blConnect.GetAnalysisSettingStr("TITLE", "Line2", "Analysis & Reporting")
                        strReportLine3 = blConnect.GetAnalysisSettingStr("TITLE", "Line3", "Standard Reports")

                        ' lFooter is either true or false
                        Try
                            'lFooter = Convert.ToBoolean(moXML.GetSettingStr("FOOTER", "lFooter", "True"))
                            lFooter = Convert.ToBoolean(blConnect.GetAnalysisSettingStr("FOOTER", "lFooter", "True"))
                        Catch ex As Exception
                            lFooter = True
                        End Try

                        ' strFooter is a string -- MAX 600 characters
                        'strFooter = moXML.GetSettingStr("FOOTER", "cFooter", "Confidential")
                        strFooter = blConnect.GetAnalysisSettingStr("FOOTER", "cFooter", "Confidential")

                        If strFooter.Length > 600 Then
                            strFooter = strFooter.Substring(0, 600)
                        End If

                        ' ========================================================================================
                        ' Fleet Calculations

                        Try
                            'intW_Factor = moXML.GetSettingInt("FLEET", "w_factor", 1)
                            intW_Factor = blConnect.GetAnalysisSettingInt("FLEET", "w_factor", 2)

                        Catch ex As Exception
                            intW_Factor = 2
                        End Try

                        blConnect.intW_Factor = intW_Factor

                        If intW_Factor = 1 Or intW_Factor = 3 Then
                            ' ==========================
                            ' intW_Factor = 1 Or intW_Factor = 3 => weighted
                            ' ==========================
                            blConnect.boolWeighted = True
                        Else
                            blConnect.boolWeighted = False
                        End If

                        ' have a valid user with units assigned

                        Dim strUnitOrGroupSelected As String = String.Empty
                        'Dim i As Integer

                        Dim strUnitGroupString As String = String.Empty

                        intGroupID = Convert.ToInt32(Now.ToString("HHmmss"))

                        blConnect.DeleteMyGroups(intGroupID)

                        blConnect.CreateMyGroups(intGroupID)

                        For Each drSetup In dtSetup.Rows
                            blConnect.CreateMyGroupsOfUnits(drSetup.UnitShortName, intGroupID)
                        Next

                        strUnitOrGroupSelected = intGroupID.ToString.Trim

                        Dim strGroupString As String = String.Empty
                        strUnitGroupString = String.Empty

                        DSTotalArray.Clear()

                        Dim dtMaxEFORd As DateTime = blConnect.BOM(Now.AddYears(99))

                        ' strDateRange = "(TL_DateTime  BETWEEN '1/31/2002 11:59:59 PM' AND '12/31/2002 11:59:59 PM')"
                        Dim intTemp1 As Integer
                        Dim intTemp2 As Integer

                        Dim strPeriodStart As String
                        Dim strPeriodEnd As String
                        Dim dtPeriodStart As DateTime
                        Dim dtPeriodEnd As DateTime

                        intTemp1 = strDateRange.IndexOf("BETWEEN")


                            intTemp1 = strDateRange.IndexOf("'", intTemp1) + 1
                            intTemp2 = strDateRange.IndexOf("'", intTemp1 + 1)

                        strPeriodStart = strDateRange.Substring(intTemp1, (intTemp2 - intTemp1))

                        Try
                            dtPeriodStart = System.DateTime.Parse(strPeriodStart)
                        Catch ex As Exception
                            dtPeriodStart = System.DateTime.Now

                        End Try

                        intTemp1 = strDateRange.IndexOf("AND")

                            intTemp1 = strDateRange.IndexOf("'", intTemp1) + 1
                            intTemp2 = strDateRange.IndexOf("'", intTemp1 + 1)


                        strPeriodEnd = strDateRange.Substring(intTemp1, (intTemp2 - intTemp1))

                        Try
                            dtPeriodEnd = System.DateTime.Parse(strPeriodEnd)
                        Catch ex As Exception
                            dtPeriodEnd = System.DateTime.Now
                        End Try

                        Dim strPerfRecordsSelect As String
                        strPerfRecordsSelect = "SELECT UnitShortName, UtilityUnitCode, Year, Period, NetMaxCap, NetGen, " & _
                                               "PeriodHours, NCF, AttemptedStarts, ActualStarts, StartingReliability, " & _
                                               "PerfCalcDate AS TL_DateTime FROM PerformanceRecords WHERE (Period BETWEEN '01' AND '12') AND "

                        Dim strDateRangePerfRecords As String
                        Dim str1 As String = strPeriodStart
                        Dim str2 As String = strPeriodEnd

                        If strPeriodStart.Contains("'") Then
                            Dim strPeriodStartTemp As String = strPeriodStart.Replace("'", "")
                            strPeriodStart = strPeriodStartTemp
                        End If

                        If strPeriodEnd.Contains("'") Then
                            Dim strPeriodEndTemp As String = strPeriodEnd.Replace("'", "")
                            strPeriodEnd = strPeriodEndTemp
                        End If

                        dtstr1BOM = blConnect.BOM(Convert.ToDateTime(strPeriodStart))
                        dtstr2 = Convert.ToDateTime(strPeriodEnd)

                        If blConnect.ThisProvider = "Oracle" Then

                            str1 = "to_date ('" & Convert.ToDateTime(strPeriodStart).ToString("MM/dd/yyyy HH:mm:ss") & "', 'mm/dd/yyyy HH24:MI:SS')"
                            str1BOM = "to_date ('" & blConnect.BOM(Convert.ToDateTime(strPeriodStart)).ToString("MM/dd/yyyy HH:mm:ss") & "', 'mm/dd/yyyy HH24:MI:SS')"
                            str2 = "to_date ('" & Convert.ToDateTime(strPeriodEnd).ToString("MM/dd/yyyy HH:mm:ss") & "', 'mm/dd/yyyy HH24:MI:SS')"

                        Else

                            str1 = "'" & DateTime.Parse(strPeriodStart).ToString("yyyy-MM-dd HH:mm:ss") & "'"
                            str1BOM = "'" & blConnect.BOM(DateTime.Parse(strPeriodStart)).ToString("yyyy-MM-dd HH:mm:ss") & "'"
                            str2 = "'" & DateTime.Parse(strPeriodEnd).ToString("yyyy-MM-dd HH:mm:ss") & "'"

                        End If

                        strDateRangePerfRecords = "(PerfCalcDate BETWEEN " & str1 & " AND " & str2 & ") AND "
                        strGroupString = String.Empty
                        strUnitGroupString = "(UnitShortName IN (SELECT UnitShortName FROM Setup WHERE UnitShortName IN (SELECT UnitShortName FROM ARUnitPerm WHERE GroupID = " & strUnitOrGroupSelected & ")))"

                        If strUnitGroupString.Trim <> String.Empty Then
                            strDateRangePerfRecords += strUnitGroupString + " AND "
                        End If

                        If strDateRangePerfRecords.Trim.EndsWith("AND") Then
                            strDateRangePerfRecords = strDateRangePerfRecords.Substring(0, (strDateRangePerfRecords.Length - 5))
                        End If

                        Dim dtPR As New DataTable With {
                            .TableName = "PerformanceRecords"
                        }

                        ' NYISO IIFO ---------------------------------------------------

                        ' SELECT UnitShortName, UtilityUnitCode, Year, Period, NetMaxCap, NetGen, PeriodHours, NCF, AttemptedStarts, ActualStarts, StartingReliability, PerfCalcDate AS TL_DateTime 
                        ' FROM PerformanceRecords 
                        ' WHERE (Period BETWEEN '01' AND '12') AND
                        ' (PerfCalcDate BETWEEN to_date ('01/31/2016 23:59:59', 'mm/dd/yyyy HH24:MI:SS') AND to_date ('12/31/2016 23:59:59', 'mm/dd/yyyy HH24:MI:SS')) AND
                        ' (UnitShortName IN (SELECT UnitShortName FROM Setup WHERE UnitShortName IN (SELECT UnitShortName FROM ARUnitPerm WHERE GroupID = 172731)))
                        Dim strMin As String = ""
                        Dim intNoOfMos As Integer = DateDiff(DateInterval.Month, dtPeriodStart, dtPeriodEnd) + 1
                        Dim myObj As Object
                        Dim dtMin As DateTime = DateTime.Now
                        Dim myD As HybridDictionary = New HybridDictionary

                        For Each drSetup In dtSetup.Rows

                            If blConnect.ThisProvider = "Oracle" Then
                                strMin = "SELECT MIN(TL_DateTime) " &
                                           "FROM (SELECT * FROM EventHours WHERE (IR + MB + RU) = 0.0 And UnitShortName = '" & drSetup.UnitShortName &
                                           "' AND Granularity = '" + strGranularity.Trim + "' AND TL_DateTime <= " & str2 &
                                           " ORDER BY TL_DateTime DESC) NOIIFO " &
                                           "WHERE rownum <= " & intNoOfMos.ToString & " ORDER BY rownum"
                            Else
                                strMin = "SELECT MIN(TL_DateTime) " &
                                           "FROM (SELECT TOP " & intNoOfMos.ToString & " * FROM EventHours WHERE (IR + MB + RU) = 0.0 And UnitShortName = '" & drSetup.UnitShortName &
                                           "' AND Granularity = '" + strGranularity.Trim + "' AND TL_DateTime <= " & str2 &
                                           " ORDER BY TL_DateTime DESC) NOIIFO"
                            End If

                            Try
                                myObj = blConnect.ExecuteScalar(strMin)
                                dtMin = Convert.ToDateTime(myObj)
                                myD.Add(drSetup.UnitShortName, dtMin)
                            Catch ex As Exception

                            End Try
                        Next

                        Dim strD As String = strPeriodStart

                        For Each demyD As DictionaryEntry In myD

                            If blConnect.ThisProvider = "Oracle" Then
                                strD = "to_date ('" & Convert.ToDateTime(demyD.Value.ToString).ToString("MM/dd/yyyy HH:mm:ss") & "', 'mm/dd/yyyy HH24:MI:SS')"
                            Else
                                strD = "'" & DateTime.Parse(demyD.Value.ToString).ToString("yyyy-MM-dd HH:mm:ss") & "'"
                            End If

                            strDateRangePerfRecords = "(PerfCalcDate BETWEEN " & strD & " AND " & str2 & ") AND UnitShortName = '" & demyD.Key.ToString & "'"

                            dtPR.Merge(blConnect.GetDataTable(strPerfRecordsSelect & strDateRangePerfRecords), False, MissingSchemaAction.Add)

                        Next

                        ' NYISO IIFO ---------------------------------------------------

                        'dtPR = blConnect.GetDataTable(strPerfRecordsSelect & strDateRangePerfRecords)
                        dtPR.TableName = "PerformanceRecords"

                        Dim dsDSPerfRecords As New PerfRecordsDemandStats
                        Dim dtDSPerfRecords As PerfRecordsDemandStats.PerformanceRecordsDataTable
                        dtDSPerfRecords = blConnect.GetPerfRecordsDemandStatsEmpty
                        dtDSPerfRecords.Rows.Clear()

                        Dim drPR1 As DataRow
                        Dim drDSPerfRecords As PerfRecordsDemandStats.PerformanceRecordsRow

                        For Each drPR1 In dtPR.Rows
                            drDSPerfRecords = dtDSPerfRecords.NewPerformanceRecordsRow
                            drDSPerfRecords.UnitShortName = drPR1.Item("UnitShortName").ToString
                            drDSPerfRecords.UtilityUnitCode = drPR1.Item("UtilityUnitCode").ToString
                            drDSPerfRecords.Year = Convert.ToInt16(drPR1.Item("Year"))
                            drDSPerfRecords.Period = drPR1.Item("Period").ToString
                            If Not IsDBNull(drPR1.Item("NetMaxCap")) Then
                                drDSPerfRecords.NetMaxCap = Convert.ToDecimal(drPR1.Item("NetMaxCap"))
                            End If
                            If Not IsDBNull(drPR1.Item("NetGen")) Then
                                drDSPerfRecords.NetGen = Convert.ToDecimal(drPR1.Item("NetGen"))
                            End If
                            drDSPerfRecords.PeriodHours = Convert.ToDecimal(drPR1.Item("PeriodHours"))
                            If Not IsDBNull(drPR1.Item("NCF")) Then
                                drDSPerfRecords.NCF = Convert.ToDouble(drPR1.Item("NCF"))
                            End If
                            If Not IsDBNull(drPR1.Item("AttemptedStarts")) Then
                                drDSPerfRecords.AttemptedStarts = Convert.ToInt32(drPR1.Item("AttemptedStarts"))
                            End If
                            If Not IsDBNull(drPR1.Item("ActualStarts")) Then
                                drDSPerfRecords.ActualStarts = Convert.ToInt32(drPR1.Item("ActualStarts"))
                            End If
                            If Not IsDBNull(drPR1.Item("StartingReliability")) Then
                                drDSPerfRecords.StartingReliability = Convert.ToDecimal(drPR1.Item("StartingReliability"))
                            End If
                            drDSPerfRecords.TL_DateTime = Convert.ToDateTime(drPR1.Item("TL_DateTime"))
                            dtDSPerfRecords.AddPerformanceRecordsRow(drDSPerfRecords)
                        Next

                        dtDSPerfRecords.TableName = "PerformanceRecords"

                        Dim hdTemp As DemandStatsTotal
                        Dim de As DictionaryEntry
                        Try

                            For Each drDSPerfRecords In dtDSPerfRecords.Rows

                                If DSTotalArray.Contains(drDSPerfRecords.UnitShortName) Then

                                    hdTemp = DirectCast(DSTotalArray.Item(drDSPerfRecords.UnitShortName), DemandStatsTotal)

                                    If drDSPerfRecords.Year > hdTemp.Year Then
                                        hdTemp.Year = drDSPerfRecords.Year
                                    End If

                                    If drDSPerfRecords.Period > hdTemp.Period Then
                                        hdTemp.Period = drDSPerfRecords.Period
                                    End If

                                    If Not drDSPerfRecords.IsNetMaxCapNull Then

                                        If IsDBNull(hdTemp.NetMaxCap) Then
                                            hdTemp.NetMaxCap = drDSPerfRecords.NetMaxCap
                                        Else
                                            hdTemp.NetMaxCap = hdTemp.NetMaxCap + drDSPerfRecords.NetMaxCap
                                        End If

                                    End If

                                    If Not drDSPerfRecords.IsNetGenNull Then

                                        If IsDBNull(hdTemp.NetGen) Then
                                            hdTemp.NetGen = drDSPerfRecords.NetGen
                                        Else
                                            hdTemp.NetGen = hdTemp.NetGen + drDSPerfRecords.NetGen
                                        End If

                                    End If

                                    If Not drDSPerfRecords.IsPeriodHoursNull Then

                                        If IsDBNull(hdTemp.PeriodHours) Then
                                            hdTemp.PeriodHours = (drDSPerfRecords.NetMaxCap * drDSPerfRecords.PeriodHours)
                                        Else
                                            hdTemp.PeriodHours = hdTemp.PeriodHours + (drDSPerfRecords.NetMaxCap * drDSPerfRecords.PeriodHours)
                                        End If

                                    End If

                                    If Not drDSPerfRecords.IsAttemptedStartsNull Then

                                        If IsDBNull(hdTemp.AttemptedStarts) Then
                                            hdTemp.AttemptedStarts = drDSPerfRecords.AttemptedStarts
                                        Else
                                            hdTemp.AttemptedStarts = hdTemp.AttemptedStarts + drDSPerfRecords.AttemptedStarts
                                        End If

                                    End If

                                    If Not drDSPerfRecords.IsActualStartsNull Then
                                        If IsDBNull(hdTemp.ActualStarts) Then
                                            hdTemp.ActualStarts = drDSPerfRecords.ActualStarts
                                        Else
                                            hdTemp.ActualStarts = hdTemp.ActualStarts + drDSPerfRecords.ActualStarts
                                        End If
                                    End If

                                    hdTemp.NumberOfRecords = hdTemp.NumberOfRecords + 1

                                    DSTotalArray.Item(drDSPerfRecords.UnitShortName) = hdTemp

                                Else

                                    If drDSPerfRecords.IsNetMaxCapNull Then
                                        drDSPerfRecords.NetMaxCap = 0
                                    End If

                                    If drDSPerfRecords.IsNetGenNull Then
                                        drDSPerfRecords.NetGen = 0
                                    End If

                                    If drDSPerfRecords.IsPeriodHoursNull Then
                                        drDSPerfRecords.PeriodHours = 0
                                    End If

                                    If drDSPerfRecords.IsAttemptedStartsNull Then
                                        drDSPerfRecords.AttemptedStarts = 0
                                    End If

                                    If drDSPerfRecords.IsActualStartsNull Then
                                        drDSPerfRecords.ActualStarts = 0
                                    End If

                                    DSTotalArray.Add(drDSPerfRecords.UnitShortName, New DemandStatsTotal(drDSPerfRecords.UnitShortName, _
                                        drDSPerfRecords.UtilityUnitCode, drDSPerfRecords.Year, drDSPerfRecords.Period, drDSPerfRecords.NetMaxCap, _
                                        drDSPerfRecords.NetGen, (drDSPerfRecords.NetMaxCap * drDSPerfRecords.PeriodHours), 0, drDSPerfRecords.AttemptedStarts, _
                                        drDSPerfRecords.ActualStarts, 0, dtMaxEFORd, 1))
                                End If

                            Next

                        Catch ex As Exception
                            Dim strWhatIsThis As String = ex.ToString
                        End Try

                        For Each de In DSTotalArray

                            hdTemp = DirectCast(de.Value, DemandStatsTotal)

                            drDSPerfRecords = dtDSPerfRecords.NewPerformanceRecordsRow
                            drDSPerfRecords.UnitShortName = hdTemp.UnitShortName
                            drDSPerfRecords.UtilityUnitCode = hdTemp.UtilityUnitCode
                            drDSPerfRecords.Year = hdTemp.Year
                            drDSPerfRecords.Period = hdTemp.Period
                            If Not IsDBNull(hdTemp.NetMaxCap) And hdTemp.NumberOfRecords > 0 Then
                                hdTemp.NetMaxCap = hdTemp.NetMaxCap / hdTemp.NumberOfRecords
                            End If
                            drDSPerfRecords.NetMaxCap = hdTemp.NetMaxCap
                            drDSPerfRecords.NetGen = hdTemp.NetGen
                            drDSPerfRecords.PeriodHours = hdTemp.PeriodHours

                            If drDSPerfRecords.PeriodHours > 0 Then
                                drDSPerfRecords.NCF = (100.0 * drDSPerfRecords.NetGen) / drDSPerfRecords.PeriodHours
                            End If

                            If IsDBNull(hdTemp.AttemptedStarts) Then
                                drDSPerfRecords.AttemptedStarts = 0
                            Else
                                drDSPerfRecords.AttemptedStarts = hdTemp.AttemptedStarts
                            End If

                            If IsDBNull(hdTemp.ActualStarts) Then
                                drDSPerfRecords.ActualStarts = 0
                            Else
                                drDSPerfRecords.ActualStarts = hdTemp.ActualStarts
                            End If

                            If drDSPerfRecords.AttemptedStarts > 0 Then
                                drDSPerfRecords.StartingReliability = Convert.ToDecimal(100.0 * drDSPerfRecords.ActualStarts) / drDSPerfRecords.AttemptedStarts
                            End If

                            drDSPerfRecords.TL_DateTime = dtMaxEFORd

                            dtDSPerfRecords.AddPerformanceRecordsRow(drDSPerfRecords)

                        Next

                        Dim myColumn As DataColumn

                        dsDSPerfRecords.Merge(dtDSPerfRecords)

                        Dim dtEFORd As ARWindowsUI.EFORd.EFORdDataTable
                        'Dim drEFORd As EFORd.EFORdRow

                        Dim dtEFORdTotal As ARWindowsUI.EFORd.EFORdDataTable
                        Dim drEFORdTotal As ARWindowsUI.EFORd.EFORdRow

                        Dim drEFORdGroupSum As ARWindowsUI.EFORd.EFORdRow

                        Dim dtEFORdTotalTable As EFORdTotal.EFORdTotalDataTable

                        dtEFORdTotalTable = blConnect.GetEFORdTotalTable()
                        dtEFORdTotalTable.Rows.Clear()

                        Dim drEFORdTotalTable As EFORdTotal.EFORdTotalRow
                        drEFORdTotalTable = dtEFORdTotalTable.NewEFORdTotalRow

                        ' Create SELECT statement

                        'strSelect = "SELECT * FROM EFORd WHERE " + strDateRange + " AND (Granularity = '" & strGranularity.Trim & "') AND "

                        'strTemp = String.Empty

                        'If strUnitGroupString.Trim <> String.Empty Then
                        '    strSelect += strUnitGroupString + " AND "
                        'End If

                        'If strSelect.Trim.EndsWith("AND") Then
                        '    strSelect = strSelect.Substring(0, (strSelect.Length - 5))
                        'End If

                        'strSelect += OrderByString

                        'dtEFORd = blConnect.GetEFORdData(strSelect)

                        ' NYISO IIFO ---------------------------------------------------

                        dtEFORd = New ARWindowsUI.EFORd.EFORdDataTable

                        For Each demyD As DictionaryEntry In myD

                            If blConnect.ThisProvider = "Oracle" Then
                                strD = "to_date ('" & Convert.ToDateTime(demyD.Value.ToString).ToString("MM/dd/yyyy HH:mm:ss") & "', 'mm/dd/yyyy HH24:MI:SS')"
                            Else
                                strD = "'" & DateTime.Parse(demyD.Value.ToString).ToString("yyyy-MM-dd HH:mm:ss") & "'"
                            End If

                            strSelect = "SELECT * FROM EFORd WHERE (TL_DateTime BETWEEN " & strD & " AND " & str2 & ") AND UnitShortName = '" & demyD.Key.ToString & "' AND (Granularity = '" & strGranularity.Trim & "') " & OrderByString

                            dtEFORd.Merge(blConnect.GetEFORdData(strSelect), False, MissingSchemaAction.Add)

                        Next

                        ' NYISO IIFO ---------------------------------------------------

                        If dtEFORd.Rows.Count = 0 Then
                            sScript = "<script language='javascript'>"
                            sScript += "alert('EFORd Table Data - No records met the selection criteria');"
                            sScript += "</script>"
                            Response.Write(sScript)
                            Exit Sub
                        End If

                        Dim dsEFORd As New DataSet

                        dsEFORd.Merge(dtEFORd)

                        ' NYISO IIFO ---------------------------------------------------

                        Dim myDMonths As HybridDictionary = New HybridDictionary
                        Dim MonthsReader As IDataReader
                        Dim listDMonths As New List(Of ActiveMonths)()

                        For Each drSetup In dtSetup.Rows

                            strD = ""

                            If blConnect.ThisProvider = "Oracle" Then
                                strMin = "SELECT TL_DateTime " &
                                       "FROM (SELECT * FROM EventHours WHERE (IR + MB + RU) = 0.0 And UnitShortName = '" & drSetup.UnitShortName &
                                       "' AND Granularity = '" + strGranularity.Trim + "' AND TL_DateTime <= " & str2 &
                                       " ORDER BY TL_DateTime DESC ) NOIIFO " &
                                       "WHERE rownum <= " & intNoOfMos.ToString & " ORDER BY rownum"
                            Else
                                strMin = "SELECT TOP " & intNoOfMos.ToString & " TL_DateTime " &
                                           "FROM EventHours WHERE (IR + MB + RU) = 0.0 And UnitShortName = '" & drSetup.UnitShortName &
                                           "' AND Granularity = '" + strGranularity.Trim + "' AND TL_DateTime <= " & str2 &
                                           " ORDER BY TL_DateTime DESC"
                            End If

                            MonthsReader = blConnect.ExecuteDataReader(strMin)

                            While MonthsReader.Read()
                                Try
                                    If blConnect.ThisProvider = "Oracle" Then
                                        strD += "to_date ('" & Convert.ToDateTime(MonthsReader.GetValue(0).ToString).ToString("MM/dd/yyyy HH:mm:ss") & "', 'mm/dd/yyyy HH24:MI:SS'), "
                                    Else
                                        strD += "'" & DateTime.Parse(MonthsReader.GetValue(0).ToString).ToString("yyyy-MM-dd HH:mm:ss") & "', "
                                    End If

                                    listDMonths.Add(New ActiveMonths() With {
                                         .ShortName = drSetup.UnitShortName,
                                         .T_DateTime = Convert.ToDateTime(MonthsReader.GetValue(0))
                                    })

                                Catch ex As Exception

                                End Try
                            End While

                            ' always call Close when done reading
                            MonthsReader.Close()

                            If strD.Trim.EndsWith(",") Then
                                strD = strD.Substring(0, (strD.Length - 2))
                            End If

                            myDMonths.Add(drSetup.UnitShortName, strD)

                        Next

                        dtEFORdTotal = New ARWindowsUI.EFORd.EFORdDataTable

                        For Each demyD As DictionaryEntry In myDMonths

                            strSelect = "SELECT UnitShortName, MAX(Granularity) AS Granularity, SUM(PO) AS PO, SUM(PO_SE) AS PO_SE, SUM(MO) AS MO, SUM(MO_SE) AS MO_SE, " &
                           "SUM(SF) AS SF,  SUM(U1) AS U1, SUM(U2) AS U2, SUM(U3) AS U3, SUM(D1) AS D1, SUM(D2) AS D2, SUM(D3) AS D3, SUM(D4) AS D4, SUM(D4_DE) AS D4_DE, SUM(PD) AS PD, SUM(PD_DE) AS PD_DE, SUM(RS) AS RS, " &
                           "SUM(EUFDH_RS) AS EUFDH_RS, SUM(SH) AS SH, SUM(PH) AS PH, SUM(ESEDH) AS ESEDH, SUM(AH) AS AH, SUM(E_PO) AS E_PO, SUM(E_PO_SE) AS E_PO_SE, SUM(E_MO) AS E_MO, SUM(E_MO_SE) AS E_MO_SE, " &
                           "SUM(E_SF) AS E_SF, SUM(E_U1) AS E_U1, SUM(E_U2) AS E_U2, SUM(E_U3) AS E_U3, SUM(E_D1) AS E_D1, SUM(E_D2) AS E_D2, SUM(E_D3) AS E_D3, SUM(E_D4) AS E_D4, SUM(E_D4_DE) AS E_D4_DE, SUM(E_PD) AS E_PD, " &
                           "SUM(E_PD_DE) AS E_PD_DE, SUM(E_RS) AS E_RS, SUM(E_EUFDH_RS) AS E_EUFDH_RS, SUM(E_SH) AS E_SH, SUM(E_PH) AS E_PH, SUM(E_ESEDH) AS E_ESEDH, SUM(E_AH) AS E_AH,  " &
                           "SUM(ActualStartsCount) AS ActualStartsCount, SUM(AttemptedStartsCount) AS AttemptedStartsCount, SUM(AttemptedStarts) AS AttemptedStarts, SUM(ActualStarts) AS ActualStarts, SUM(GrossMaxCap) AS GrossMaxCap, " &
                           "SUM(NetMaxCap) AS NetMaxCap, SUM(PumpingHours) AS PumpingHours, SUM(SynchCondHours) AS SynchCondHours, SUM(E_PumpingHours) AS E_PumpingHours, SUM(E_SynchCondHours) AS E_SynchCondHours, SUM(GMC_Weight) AS GMC_Weight, SUM(NMC_Weight) AS NMC_Weight, " &
                           "MIN(ServiceHourMethod) AS ServiceHourMethod FROM EFORd WHERE TL_DateTime IN (" + demyD.Value.ToString + ") AND (Granularity = '" + strGranularity.Trim + "') AND UnitShortName = '" + demyD.Key.ToString + "' GROUP BY UnitShortName"

                            If blConnect.ThisProvider = "Oracle" Then
                                strSelect = strSelect.Replace("SUM(SH)", "TRUNC(SUM(SH), 7)")
                                strSelect = strSelect.Replace("SUM(FL_Numerator)", "TRUNC(SUM(FL_Numerator), 7)")
                                strSelect = strSelect.Replace("SUM(FL_Denominator)", "TRUNC(SUM(FL_Denominator), 7)")
                                strSelect = strSelect.Replace("SUM(FL_FORdNumerator)", "TRUNC(SUM(FL_FORdNumerator), 7)")
                            End If

                            Try
                                dtEFORdTotal.Merge(blConnect.GetEFORdData(strSelect), False, MissingSchemaAction.Add)
                            Catch ex As Exception
                                Dim strERR As String = ex.Message
                            End Try

                        Next


                        ' NYISO IIFO ---------------------------------------------------

                        'strSelect = "SELECT UnitShortName, MAX(Granularity) AS Granularity, SUM(PO) AS PO, SUM(PO_SE) AS PO_SE, SUM(MO) AS MO, SUM(MO_SE) AS MO_SE, " & _
                        '   "SUM(SF) AS SF,  SUM(U1) AS U1, SUM(U2) AS U2, SUM(U3) AS U3, SUM(D1) AS D1, SUM(D2) AS D2, SUM(D3) AS D3, SUM(D4) AS D4, SUM(D4_DE) AS D4_DE, SUM(PD) AS PD, SUM(PD_DE) AS PD_DE, SUM(RS) AS RS, " & _
                        '   "SUM(EUFDH_RS) AS EUFDH_RS, SUM(SH) AS SH, SUM(PH) AS PH, SUM(ESEDH) AS ESEDH, SUM(AH) AS AH, SUM(E_PO) AS E_PO, SUM(E_PO_SE) AS E_PO_SE, SUM(E_MO) AS E_MO, SUM(E_MO_SE) AS E_MO_SE, " & _
                        '   "SUM(E_SF) AS E_SF, SUM(E_U1) AS E_U1, SUM(E_U2) AS E_U2, SUM(E_U3) AS E_U3, SUM(E_D1) AS E_D1, SUM(E_D2) AS E_D2, SUM(E_D3) AS E_D3, SUM(E_D4) AS E_D4, SUM(E_D4_DE) AS E_D4_DE, SUM(E_PD) AS E_PD, " & _
                        '   "SUM(E_PD_DE) AS E_PD_DE, SUM(E_RS) AS E_RS, SUM(E_EUFDH_RS) AS E_EUFDH_RS, SUM(E_SH) AS E_SH, SUM(E_PH) AS E_PH, SUM(E_ESEDH) AS E_ESEDH, SUM(E_AH) AS E_AH,  " & _
                        '   "SUM(ActualStartsCount) AS ActualStartsCount, SUM(AttemptedStartsCount) AS AttemptedStartsCount, SUM(AttemptedStarts) AS AttemptedStarts, SUM(ActualStarts) AS ActualStarts, SUM(GrossMaxCap) AS GrossMaxCap, " & _
                        '   "SUM(NetMaxCap) AS NetMaxCap, SUM(PumpingHours) AS PumpingHours, SUM(SynchCondHours) AS SynchCondHours, SUM(E_PumpingHours) AS E_PumpingHours, SUM(E_SynchCondHours) AS E_SynchCondHours, SUM(GMC_Weight) AS GMC_Weight, SUM(NMC_Weight) AS NMC_Weight, " & _
                        '   "MIN(ServiceHourMethod) AS ServiceHourMethod FROM EFORd WHERE " + strDateRange + " AND (Granularity = '" + strGranularity.Trim + "') AND "

                        'If strUnitGroupString.Trim <> String.Empty Then
                        '    strSelect += strUnitGroupString + " AND "
                        'End If

                        'If strSelect.Trim.EndsWith("AND") Then
                        '    strSelect = strSelect.Substring(0, (strSelect.Length - 5))
                        'End If

                        'strSelect += " GROUP BY UnitShortName"

                        'If blConnect.ThisProvider = "Oracle" Then
                        '    strSelect = strSelect.Replace("SUM(SH)", "TRUNC(SUM(SH), 7)")
                        '    strSelect = strSelect.Replace("SUM(FL_Numerator)", "TRUNC(SUM(FL_Numerator), 7)")
                        '    strSelect = strSelect.Replace("SUM(FL_Denominator)", "TRUNC(SUM(FL_Denominator), 7)")
                        '    strSelect = strSelect.Replace("SUM(FL_FORdNumerator)", "TRUNC(SUM(FL_FORdNumerator), 7)")
                        'End If

                        'dtEFORdTotal = blConnect.GetEFORdData(strSelect)
                        dtEFORdTotal.TableName = "EFORd"

                        ' NYISO IIFO ---------------------------------------------------

                        Dim dtEventDetails As EventDetails.EventDetailsDataTable = New EventDetails.EventDetailsDataTable
                        Dim drEventDetails As EventDetails.EventDetailsRow

                        For Each demyD As DictionaryEntry In myD

                            If blConnect.ThisProvider = "Oracle" Then
                                strD = "to_date ('" & Convert.ToDateTime(demyD.Value.ToString).ToString("MM/dd/yyyy HH:mm:ss") & "', 'mm/dd/yyyy HH24:MI:SS')"
                            Else
                                strD = "'" & DateTime.Parse(demyD.Value.ToString).ToString("yyyy-MM-dd HH:mm:ss") & "'"
                            End If

                            strDateRangePerfRecords = "(TL_DateTime BETWEEN " & strD & " AND " & str2 & ") AND UnitShortName = '" & demyD.Key.ToString & "' AND TL_DateTime IN (" + myDMonths.Item(demyD.Key).ToString() + ")"

                            strSelect = "SELECT UnitShortName, MAX(UtilityUnitCode) AS UtilityUnitcode, EventNumber, EventType, ContribCode, MAX(EventContribCode) AS EventContribCode,  " &
                                        "SUM(CalcHours) AS CalcHours, AVG(CAST(PlantMgtControl AS int)) AS PlantMgtControl, MAX(PJMIOCode) AS PJMIOCode, AVG(CAST(lFilter AS int)) AS lFilter, Granularity, EV_DateTime " &
                                        "FROM EventDetails WHERE " + strDateRangePerfRecords + " AND Granularity = '" + strGranularity.Trim + "' AND EventType IN ('U1','U2','U3','SF','RS') AND EventContribCode IN (10,60,70) " &
                                        "GROUP BY UnitShortName, EventNumber, EventType, ContribCode,  CauseCode, EV_DateTime, Granularity "

                            Try
                                dtEventDetails.Merge(blConnect.GetEventDetails(strSelect), False, MissingSchemaAction.Add)
                            Catch ex As Exception
                                Dim strERR As String = ex.Message
                            End Try

                        Next

                        ' NYISO IIFO ---------------------------------------------------


                        'strSelect = "SELECT UnitShortName, MAX(UtilityUnitCode) AS UtilityUnitcode, EventNumber, EventType, ContribCode, MAX(EventContribCode) AS EventContribCode,  " & _
                        '        "SUM(CalcHours) AS CalcHours, AVG(CAST(PlantMgtControl AS int)) AS PlantMgtControl, MAX(PJMIOCode) AS PJMIOCode, AVG(CAST(lFilter AS int)) AS lFilter, Granularity, EV_DateTime " & _
                        '        "FROM EventDetails WHERE " + strDateRange + " AND Granularity = '" + strGranularity.Trim + "' AND EventType IN ('U1','U2','U3','SF','RS') AND EventContribCode IN (10,60,70) AND "

                        'If strUnitGroupString.Trim <> String.Empty Then
                        '    strSelect += strUnitGroupString + " AND "
                        'End If

                        'If strSelect.Trim.EndsWith("AND") Then
                        '    strSelect = strSelect.Substring(0, (strSelect.Length - 5))
                        'End If

                        'strSelect += " GROUP BY UnitShortName, EventNumber, EventType, ContribCode,  CauseCode, EV_DateTime, Granularity "

                        'Dim dtEventDetails As EventDetails.EventDetailsDataTable
                        'Dim drEventDetails As EventDetails.EventDetailsRow

                        'dtEventDetails = blConnect.GetEventDetails(strSelect)

                        For Each myColumn In dtEventDetails.Columns
                            myColumn.ReadOnly = False
                        Next

                        Dim hdictFOCount As New HybridDictionary
                        Dim hdictRSCount As New HybridDictionary
                        Dim intFOCount As Integer
                        Dim intRSCount As Integer

                        ' NYISO IIFO ---------------------------------------------------

                        alFOSofE = New ArrayList
                        alFOEofE = New ArrayList

                        Dim alFOSofEtemp As ArrayList
                        Dim alFOEofEtemp As ArrayList

                        For Each demyD As DictionaryEntry In myD

                            Try
                                alFOSofEtemp = blConnect.FOEventStarts(DateTime.Parse(demyD.Value.ToString).ToString("yyyy-MM-dd HH:mm:ss"), strPeriodEnd, strPMCFilter, "UnitShortName = '" & demyD.Key.ToString & "'")
                                alFOEofEtemp = blConnect.FOEventEnds(DateTime.Parse(demyD.Value.ToString).ToString("yyyy-MM-dd HH:mm:ss"), strPeriodEnd, strPMCFilter, "UnitShortName = '" & demyD.Key.ToString & "'")

                                alFOSofE.AddRange(alFOSofEtemp)
                                alFOEofE.AddRange(alFOEofEtemp)

                            Catch ex As Exception
                                Dim strERR As String = ex.Message
                            End Try

                        Next

                        ' NYISO IIFO ---------------------------------------------------

                        'alFOSofE = blConnect.FOEventStarts(strPeriodStart, strPeriodEnd, strPMCFilter, strUnitGroupString)
                        'alFOEofE = blConnect.FOEventEnds(strPeriodStart, strPeriodEnd, strPMCFilter, strUnitGroupString)

                        For Each drEventDetails In dtEventDetails.Rows

                            If blConnect.CountThisFO(drEventDetails, alFOSofE, alFOEofE) And (drEventDetails.EventContribCode = 10 Or drEventDetails.EventContribCode = 60 Or drEventDetails.EventContribCode = 70) Then

                                Select Case drEventDetails.EventType
                                    Case "U1", "U2", "U3", "SF"

                                        If drEventDetails.CalcHours > 0.0005 Then

                                            ' if, for example,  May events end on June 1 at 00:00 hours -- this makes sure it is not counted in June

                                            If hdictFOCount.Contains(drEventDetails.UnitShortName) = True Then
                                                intFOCount = Convert.ToInt32(hdictFOCount.Item(drEventDetails.UnitShortName))
                                                intFOCount += 1
                                                hdictFOCount.Item(drEventDetails.UnitShortName) = intFOCount
                                            Else
                                                hdictFOCount.Add(drEventDetails.UnitShortName, 1)
                                            End If

                                        End If

                                    Case "RS"

                                        If drEventDetails.CalcHours > 0.0005 Then
                                            ' if, for example,  May events end on June 1 at 00:00 hours -- this makes sure it is not counted in June

                                            If hdictRSCount.Contains(drEventDetails.UnitShortName) = True Then
                                                intRSCount = Convert.ToInt32(hdictRSCount.Item(drEventDetails.UnitShortName))
                                                intRSCount += 1
                                                hdictRSCount.Item(drEventDetails.UnitShortName) = intRSCount
                                            Else
                                                hdictRSCount.Add(drEventDetails.UnitShortName, 1)
                                            End If

                                        End If

                                End Select

                            End If

                        Next

                        For Each drEFORdTotal In dtEFORdTotal.Rows

                            If hdictFOCount.Contains(drEFORdTotal.UnitShortName) = True Then
                                If (drEFORdTotal.U1 + drEFORdTotal.U2 + drEFORdTotal.U3 + drEFORdTotal.SF) > 0.0005 And Convert.ToInt32(hdictFOCount.Item(drEFORdTotal.UnitShortName)) = 0 Then
                                    drEFORdTotal.FOCount = 1
                                Else
                                    drEFORdTotal.FOCount = Convert.ToInt32(hdictFOCount.Item(drEFORdTotal.UnitShortName))
                                End If
                            End If

                            If hdictRSCount.Contains(drEFORdTotal.UnitShortName) = True Then
                                If drEFORdTotal.RS > 0.0005 And Convert.ToInt32(hdictRSCount.Item(drEFORdTotal.UnitShortName)) = 0 Then
                                    drEFORdTotal.RSCount = 1
                                Else
                                    drEFORdTotal.RSCount = Convert.ToInt32(hdictRSCount.Item(drEFORdTotal.UnitShortName))
                                End If
                            End If

                        Next

                        drEFORdGroupSum = dtEFORdTotal.NewEFORdRow

                        ' initialize values for loop += summing
                        drEFORdGroupSum.TL_DateTime = dtMaxEFORd
                        drEFORdGroupSum.PO = 0.0
                        drEFORdGroupSum.PO_SE = 0.0
                        drEFORdGroupSum.MO = 0.0
                        drEFORdGroupSum.MO_SE = 0.0
                        drEFORdGroupSum.SF = 0.0
                        drEFORdGroupSum.U1 = 0.0
                        drEFORdGroupSum.U2 = 0.0
                        drEFORdGroupSum.U3 = 0.0
                        drEFORdGroupSum.D1 = 0.0
                        drEFORdGroupSum.D2 = 0.0
                        drEFORdGroupSum.D3 = 0.0
                        drEFORdGroupSum.D4 = 0.0
                        drEFORdGroupSum.D4_DE = 0.0
                        drEFORdGroupSum.PD = 0.0
                        drEFORdGroupSum.PD_DE = 0.0
                        drEFORdGroupSum.RS = 0.0
                        drEFORdGroupSum.EUFDH_RS = 0.0
                        drEFORdGroupSum.SH = 0.0
                        drEFORdGroupSum.PH = 0.0
                        drEFORdGroupSum.ESEDH = 0.0
                        drEFORdGroupSum.AH = 0.0
                        drEFORdGroupSum.E_PO = 0.0
                        drEFORdGroupSum.E_PO_SE = 0.0
                        drEFORdGroupSum.E_MO = 0.0
                        drEFORdGroupSum.E_MO_SE = 0.0
                        drEFORdGroupSum.E_SF = 0.0
                        drEFORdGroupSum.E_U1 = 0.0
                        drEFORdGroupSum.E_U2 = 0.0
                        drEFORdGroupSum.E_U3 = 0.0
                        drEFORdGroupSum.E_D1 = 0.0
                        drEFORdGroupSum.E_D2 = 0.0
                        drEFORdGroupSum.E_D3 = 0.0
                        drEFORdGroupSum.E_D4 = 0.0
                        drEFORdGroupSum.E_D4_DE = 0.0
                        drEFORdGroupSum.E_PD = 0.0
                        drEFORdGroupSum.E_PD_DE = 0.0
                        drEFORdGroupSum.E_RS = 0.0
                        drEFORdGroupSum.E_EUFDH_RS = 0.0
                        drEFORdGroupSum.E_SH = 0.0
                        drEFORdGroupSum.E_PH = 0.0
                        drEFORdGroupSum.E_ESEDH = 0.0
                        drEFORdGroupSum.E_AH = 0.0
                        drEFORdGroupSum.FOCount = 0
                        drEFORdGroupSum.RSCount = 0
                        drEFORdGroupSum.ActualStartsCount = 0
                        drEFORdGroupSum.AttemptedStartsCount = 0
                        drEFORdGroupSum.AttemptedStarts = 0
                        drEFORdGroupSum.ActualStarts = 0
                        drEFORdGroupSum.StartingReliability = 0.0
                        drEFORdGroupSum.GrossMaxCap = 0
                        drEFORdGroupSum.NetMaxCap = 0
                        drEFORdGroupSum.PumpingHours = 0.0
                        drEFORdGroupSum.SynchCondHours = 0.0
                        drEFORdGroupSum.E_PumpingHours = 0.0
                        drEFORdGroupSum.E_SynchCondHours = 0.0
                        drEFORdGroupSum.GMC_Weight = 0
                        drEFORdGroupSum.NMC_Weight = 0
                        drEFORdGroupSum.FL_Numerator = 0.0
                        drEFORdGroupSum.FL_Denominator = 0.0
                        drEFORdGroupSum.FL_FORdNumerator = 0.0

                        Dim dtSetupDE As ARWindowsUI.SetupDE.SetupDataTable
                        dtSetupDE = blConnect.GetSetupDEData
                        Dim drSetupDE As ARWindowsUI.SetupDE.SetupRow

                        Dim hd As SortedList = New SortedList

                        For Each drEFORdTotal In dtEFORdTotal.Rows

                            blConnect.EFORdCalc(drEFORdTotal)

                            drEFORdTotal.TL_DateTime = dtMaxEFORd

                            If Not drEFORdTotal.IsPONull Then
                                drEFORdGroupSum.PO += drEFORdTotal.PO
                            Else
                                drEFORdTotal.PO = 0.0
                            End If

                            If Not drEFORdTotal.IsPO_SENull Then
                                drEFORdGroupSum.PO_SE += drEFORdTotal.PO_SE
                            Else
                                drEFORdTotal.PO_SE = 0.0
                            End If

                            If Not drEFORdTotal.IsMONull Then
                                drEFORdGroupSum.MO += drEFORdTotal.MO
                            Else
                                drEFORdTotal.MO = 0.0
                            End If

                            If Not drEFORdTotal.IsMO_SENull Then
                                drEFORdGroupSum.MO_SE += drEFORdTotal.MO_SE
                            Else
                                drEFORdTotal.MO_SE = 0.0
                            End If

                            If Not drEFORdTotal.IsSFNull Then
                                drEFORdGroupSum.SF += drEFORdTotal.SF
                            Else
                                drEFORdTotal.SF = 0.0
                            End If

                            If Not drEFORdTotal.IsU1Null Then
                                drEFORdGroupSum.U1 += drEFORdTotal.U1
                            Else
                                drEFORdTotal.U1 = 0.0
                            End If

                            If Not drEFORdTotal.IsU2Null Then
                                drEFORdGroupSum.U2 += drEFORdTotal.U2
                            Else
                                drEFORdTotal.U2 = 0.0
                            End If

                            If Not drEFORdTotal.IsU3Null Then
                                drEFORdGroupSum.U3 += drEFORdTotal.U3
                            Else
                                drEFORdTotal.U3 = 0.0
                            End If

                            If Not drEFORdTotal.IsD1Null Then
                                drEFORdGroupSum.D1 += drEFORdTotal.D1
                            Else
                                drEFORdTotal.D1 = 0.0
                            End If

                            If Not drEFORdTotal.IsD2Null Then
                                drEFORdGroupSum.D2 += drEFORdTotal.D2
                            Else
                                drEFORdTotal.D2 = 0.0
                            End If

                            If Not drEFORdTotal.IsD3Null Then
                                drEFORdGroupSum.D3 += drEFORdTotal.D3
                            Else
                                drEFORdTotal.D3 = 0.0
                            End If

                            If Not drEFORdTotal.IsD4Null Then
                                drEFORdGroupSum.D4 += drEFORdTotal.D4
                            Else
                                drEFORdTotal.D4 = 0.0
                            End If

                            If Not drEFORdTotal.IsD4_DENull Then
                                drEFORdGroupSum.D4_DE += drEFORdTotal.D4_DE
                            Else
                                drEFORdTotal.D4_DE = 0.0
                            End If

                            If Not drEFORdTotal.IsPDNull Then
                                drEFORdGroupSum.PD += drEFORdTotal.PD
                            Else
                                drEFORdTotal.PD = 0.0
                            End If

                            If Not drEFORdTotal.IsPD_DENull Then
                                drEFORdGroupSum.PD_DE += drEFORdTotal.PD_DE
                            Else
                                drEFORdTotal.PD_DE = 0.0
                            End If

                            If Not drEFORdTotal.IsRSNull Then
                                drEFORdGroupSum.RS += drEFORdTotal.RS
                            Else
                                drEFORdTotal.RS = 0.0
                            End If

                            If Not drEFORdTotal.IsEUFDH_RSNull Then
                                drEFORdGroupSum.EUFDH_RS += drEFORdTotal.EUFDH_RS
                            Else
                                drEFORdTotal.EUFDH_RS = 0.0
                            End If

                            If Not drEFORdTotal.IsSHNull Then
                                drEFORdGroupSum.SH += drEFORdTotal.SH
                            Else
                                drEFORdTotal.SH = 0.0
                            End If

                            If Not drEFORdTotal.IsPHNull Then
                                drEFORdGroupSum.PH += drEFORdTotal.PH
                            Else
                                drEFORdTotal.PH = 0.0
                            End If

                            If Not drEFORdTotal.IsESEDHNull Then
                                drEFORdGroupSum.ESEDH += drEFORdTotal.ESEDH
                            Else
                                drEFORdTotal.ESEDH = 0.0
                            End If

                            If Not drEFORdTotal.IsAHNull Then
                                drEFORdGroupSum.AH += drEFORdTotal.AH
                            Else
                                drEFORdTotal.AH = 0.0
                            End If

                            If Not drEFORdTotal.IsE_PONull Then
                                drEFORdGroupSum.E_PO += drEFORdTotal.E_PO
                            Else
                                drEFORdTotal.E_PO = 0.0
                            End If

                            If Not drEFORdTotal.IsE_PO_SENull Then
                                drEFORdGroupSum.E_PO_SE += drEFORdTotal.E_PO_SE
                            Else
                                drEFORdTotal.E_PO_SE = 0.0
                            End If

                            If Not drEFORdTotal.IsE_MONull Then
                                drEFORdGroupSum.E_MO += drEFORdTotal.E_MO
                            Else
                                drEFORdTotal.E_MO = 0.0
                            End If

                            If Not drEFORdTotal.IsE_MO_SENull Then
                                drEFORdGroupSum.E_MO_SE += drEFORdTotal.E_MO_SE
                            Else
                                drEFORdTotal.E_MO_SE = 0.0
                            End If

                            If Not drEFORdTotal.IsE_SFNull Then
                                drEFORdGroupSum.E_SF += drEFORdTotal.E_SF
                            Else
                                drEFORdTotal.E_SF = 0.0
                            End If

                            If Not drEFORdTotal.IsE_U1Null Then
                                drEFORdGroupSum.E_U1 += drEFORdTotal.E_U1
                            Else
                                drEFORdTotal.E_U1 = 0.0
                            End If

                            If Not drEFORdTotal.IsE_U2Null Then
                                drEFORdGroupSum.E_U2 += drEFORdTotal.E_U2
                            Else
                                drEFORdTotal.E_U2 = 0.0
                            End If

                            If Not drEFORdTotal.IsE_U3Null Then
                                drEFORdGroupSum.E_U3 += drEFORdTotal.E_U3
                            Else
                                drEFORdTotal.E_U3 = 0.0
                            End If

                            If Not drEFORdTotal.IsE_D1Null Then
                                drEFORdGroupSum.E_D1 += drEFORdTotal.E_D1
                            Else
                                drEFORdTotal.E_D1 = 0.0
                            End If

                            If Not drEFORdTotal.IsE_D2Null Then
                                drEFORdGroupSum.E_D2 += drEFORdTotal.E_D2
                            Else
                                drEFORdTotal.E_D2 = 0.0
                            End If

                            If Not drEFORdTotal.IsE_D3Null Then
                                drEFORdGroupSum.E_D3 += drEFORdTotal.E_D3
                            Else
                                drEFORdTotal.E_D3 = 0.0
                            End If

                            If Not drEFORdTotal.IsE_D4Null Then
                                drEFORdGroupSum.E_D4 += drEFORdTotal.E_D4
                            Else
                                drEFORdTotal.E_D4 = 0.0
                            End If

                            If Not drEFORdTotal.IsE_D4_DENull Then
                                drEFORdGroupSum.E_D4_DE += drEFORdTotal.E_D4_DE
                            Else
                                drEFORdTotal.E_D4_DE = 0.0
                            End If

                            If Not drEFORdTotal.IsE_PDNull Then
                                drEFORdGroupSum.E_PD += drEFORdTotal.E_PD
                            Else
                                drEFORdTotal.E_PD = 0.0
                            End If

                            If Not drEFORdTotal.IsE_PD_DENull Then
                                drEFORdGroupSum.E_PD_DE += drEFORdTotal.E_PD_DE
                            Else
                                drEFORdTotal.E_PD_DE = 0.0
                            End If

                            If Not drEFORdTotal.IsE_RSNull Then
                                drEFORdGroupSum.E_RS += drEFORdTotal.E_RS
                            Else
                                drEFORdTotal.E_RS = 0.0
                            End If

                            If Not drEFORdTotal.IsE_EUFDH_RSNull Then
                                drEFORdGroupSum.E_EUFDH_RS += drEFORdTotal.E_EUFDH_RS
                            Else
                                drEFORdTotal.E_EUFDH_RS = 0.0
                            End If

                            If Not drEFORdTotal.IsE_SHNull Then
                                drEFORdGroupSum.E_SH += drEFORdTotal.E_SH
                            Else
                                drEFORdTotal.E_SH = 0.0
                            End If

                            If Not drEFORdTotal.IsE_PHNull Then
                                drEFORdGroupSum.E_PH += drEFORdTotal.E_PH
                            Else
                                drEFORdTotal.E_PH = 0.0
                            End If

                            If Not drEFORdTotal.IsE_ESEDHNull Then
                                drEFORdGroupSum.E_ESEDH += drEFORdTotal.E_ESEDH
                            Else
                                drEFORdTotal.E_ESEDH = 0.0
                            End If

                            If Not drEFORdTotal.IsE_AHNull Then
                                drEFORdGroupSum.E_AH += drEFORdTotal.E_AH
                            Else
                                drEFORdTotal.E_AH = 0.0
                            End If

                            If Not drEFORdTotal.IsFOCountNull Then
                                drEFORdGroupSum.FOCount += drEFORdTotal.FOCount
                            Else
                                drEFORdTotal.FOCount = 0
                            End If

                            If Not drEFORdTotal.IsRSCountNull Then
                                drEFORdGroupSum.RSCount += drEFORdTotal.RSCount
                            Else
                                drEFORdTotal.RSCount = 0
                            End If

                            If Not drEFORdTotal.IsActualStartsCountNull Then
                                drEFORdGroupSum.ActualStartsCount += drEFORdTotal.ActualStartsCount
                            Else
                                drEFORdTotal.ActualStartsCount = 0
                            End If

                            If Not drEFORdTotal.IsAttemptedStartsCountNull Then
                                drEFORdGroupSum.AttemptedStartsCount += drEFORdTotal.AttemptedStartsCount
                            Else
                                drEFORdTotal.AttemptedStartsCount = 0
                            End If

                            If Not drEFORdTotal.IsAttemptedStartsNull Then
                                drEFORdGroupSum.AttemptedStarts += drEFORdTotal.AttemptedStarts
                            Else
                                drEFORdTotal.AttemptedStarts = 0
                            End If

                            If Not drEFORdTotal.IsActualStartsNull Then
                                drEFORdGroupSum.ActualStarts += drEFORdTotal.ActualStarts
                            Else
                                drEFORdTotal.ActualStarts = 0
                            End If

                            If Not drEFORdTotal.IsStartingReliabilityNull Then
                                drEFORdGroupSum.StartingReliability += drEFORdTotal.StartingReliability
                            Else
                                drEFORdTotal.StartingReliability = 0.0
                            End If

                            If Not drEFORdTotal.IsGrossMaxCapNull Then
                                drEFORdGroupSum.GrossMaxCap += drEFORdTotal.GrossMaxCap
                            Else
                                drEFORdTotal.GrossMaxCap = 0
                            End If

                            If Not drEFORdTotal.IsNetMaxCapNull Then
                                drEFORdGroupSum.NetMaxCap += drEFORdTotal.NetMaxCap
                            Else
                                drEFORdTotal.NetMaxCap = 0
                            End If

                            If Not drEFORdTotal.IsPumpingHoursNull Then
                                drEFORdGroupSum.PumpingHours += drEFORdTotal.PumpingHours
                            Else
                                drEFORdTotal.PumpingHours = 0.0
                            End If

                            If Not drEFORdTotal.IsSynchCondHoursNull Then
                                drEFORdGroupSum.SynchCondHours += drEFORdTotal.SynchCondHours
                            Else
                                drEFORdTotal.SynchCondHours = 0
                            End If

                            If Not drEFORdTotal.IsE_PumpingHoursNull Then
                                drEFORdGroupSum.E_PumpingHours += drEFORdTotal.E_PumpingHours
                            Else
                                drEFORdTotal.E_PumpingHours = 0.0
                            End If

                            If Not drEFORdTotal.IsE_SynchCondHoursNull Then
                                drEFORdGroupSum.E_SynchCondHours += drEFORdTotal.E_SynchCondHours
                            Else
                                drEFORdTotal.E_SynchCondHours = 0.0
                            End If

                            If Not drEFORdTotal.IsGMC_WeightNull Then
                                drEFORdGroupSum.GMC_Weight += drEFORdTotal.GMC_Weight
                            Else
                                drEFORdTotal.GMC_Weight = 0
                            End If

                            If Not drEFORdTotal.IsNMC_WeightNull Then
                                drEFORdGroupSum.NMC_Weight += drEFORdTotal.NMC_Weight
                            Else
                                drEFORdTotal.NMC_Weight = 0
                            End If

                            If Not drEFORdTotal.IsFL_NumeratorNull Then
                                drEFORdGroupSum.FL_Numerator += drEFORdTotal.FL_Numerator
                            Else
                                drEFORdTotal.FL_Numerator = 0.0
                            End If

                            If Not drEFORdTotal.IsFL_DenominatorNull Then
                                drEFORdGroupSum.FL_Denominator += drEFORdTotal.FL_Denominator
                            Else
                                drEFORdTotal.FL_Denominator = 0.0
                            End If

                            If Not drEFORdTotal.IsFL_FORdNumeratorNull Then
                                drEFORdGroupSum.FL_FORdNumerator += drEFORdTotal.FL_FORdNumerator
                            Else
                                drEFORdTotal.FL_FORdNumerator = 0.0
                            End If

                            drEFORdTotalTable.UnitShortName = drEFORdTotal.UnitShortName.Trim

                            For Each drSetupDE In dtSetupDE.Rows
                                If drSetupDE.UnitShortName.Trim = drEFORdTotal.UnitShortName.Trim Then
                                    drEFORdTotalTable.UnitName = drSetupDE.UnitName.Trim
                                    drEFORdTotalTable.UtilityUnitCode = drSetupDE.UtilityUnitCode.Trim
                                    Exit For
                                End If
                            Next

                            drEFORdTotalTable.PeriodStart = DateTime.Parse(strPeriodStart)
                            drEFORdTotalTable.PeriodEnd = DateTime.Parse(strPeriodEnd)
                            drEFORdTotalTable.SF = drEFORdTotal.SF
                            drEFORdTotalTable.U1 = drEFORdTotal.U1
                            drEFORdTotalTable.U2 = drEFORdTotal.U2
                            drEFORdTotalTable.U3 = drEFORdTotal.U3
                            drEFORdTotalTable.D1 = drEFORdTotal.D1
                            drEFORdTotalTable.D2 = drEFORdTotal.D2
                            drEFORdTotalTable.D3 = drEFORdTotal.D3
                            drEFORdTotalTable.RS = drEFORdTotal.RS
                            drEFORdTotalTable.EUFDH_RS = drEFORdTotal.EUFDH_RS
                            drEFORdTotalTable.SH = drEFORdTotal.SH
                            drEFORdTotalTable.AH = drEFORdTotal.AH
                            drEFORdTotalTable.FOCount = drEFORdTotal.FOCount
                            drEFORdTotalTable.RSCount = drEFORdTotal.RSCount
                            drEFORdTotalTable.ActualStartsCount = drEFORdTotal.ActualStartsCount
                            drEFORdTotalTable.AttemptedStartsCount = drEFORdTotal.AttemptedStartsCount
                            drEFORdTotalTable.AttemptedStarts = drEFORdTotal.AttemptedStarts
                            drEFORdTotalTable.ActualStarts = drEFORdTotal.ActualStarts
                            If Not drEFORdTotal.IsServiceHourMethodNull Then
                                drEFORdTotalTable.ServiceHourMethod = drEFORdTotal.ServiceHourMethod
                            End If
                            drEFORdTotalTable.DEFOR = drEFORdTotal.DEFOR
                            drEFORdTotalTable.DFOR = drEFORdTotal.DFOR
                            drEFORdTotalTable.FL_Numerator = drEFORdTotal.FL_Numerator
                            drEFORdTotalTable.FL_Denominator = drEFORdTotal.FL_Denominator
                            drEFORdTotalTable.FL_FORdNumerator = drEFORdTotal.FL_FORdNumerator

                            blConnect.LoadEFORdTotal(strPeriodStart, strPeriodEnd, drEFORdTotalTable)

                        Next

                        If Not drEFORdGroupSum.IsFL_DenominatorNull Then

                            If drEFORdGroupSum.FL_Denominator > 0 Then

                                If Not drEFORdGroupSum.IsFL_NumeratorNull Then
                                    drEFORdGroupSum.DEFOR = drEFORdGroupSum.FL_Numerator / drEFORdGroupSum.FL_Denominator
                                End If

                                If Not drEFORdGroupSum.IsFL_FORdNumeratorNull Then
                                    drEFORdGroupSum.DFOR = drEFORdGroupSum.FL_FORdNumerator / drEFORdGroupSum.FL_Denominator
                                End If

                            End If

                        End If

                        If strGroupString.Trim <> String.Empty Then

                            If strGroupString.ToUpper.IndexOf("ARGROUPS") > 0 Then

                                strSelect = "SELECT * FROM EFORd WHERE " + strDateRange + " AND (Granularity = '" + strGranularity.Trim + "') AND " + strGroupString + " " + OrderByString

                                dtEFORd = blConnect.GetEFORdData(strSelect)

                                dsEFORd.Merge(dtEFORd)

                            End If

                        End If

                        Dim strTempGroupName As String = "ALL UNITS"

                        If strUnitGroupString.ToUpper.IndexOf("ARGROUPS") > 0 Or _
                           strGroupString.ToUpper.IndexOf("ARGROUPS") > 0 Then

                            If strGroupString.Trim <> String.Empty Then

                                strPerfRecordsSelect = "SELECT UnitShortName, UtilityUnitCode, Year, Period, NetMaxCap, NetGen, " & _
                                              "N_PeriodHours AS PeriodHours, NCF, AttemptedStarts, ActualStarts, StartingReliability, " & _
                                              "PerfCalcDate AS TL_DateTime FROM PerformanceRecords WHERE (Period BETWEEN '01' AND '12') AND "

                                str1 = strPeriodStart
                                str2 = strPeriodEnd

                                If blConnect.ThisProvider = "Oracle" Then

                                    str1 = "to_date ('" & Convert.ToDateTime(strPeriodStart).ToString("MM/dd/yyyy HH:mm:ss") & "', 'mm/dd/yyyy HH24:MI:SS')"
                                    str2 = "to_date ('" & Convert.ToDateTime(strPeriodEnd).ToString("MM/dd/yyyy HH:mm:ss") & "', 'mm/dd/yyyy HH24:MI:SS')"

                                Else

                                    str1 = "'" & DateTime.Parse(strPeriodStart).ToString("yyyy-MM-dd HH:mm:ss") & "'"
                                    str2 = "'" & DateTime.Parse(strPeriodEnd).ToString("yyyy-MM-dd HH:mm:ss") & "'"

                                End If

                                strDateRangePerfRecords = "(PerfCalcDate BETWEEN " & str1 & " AND " & str2 & ") AND " + strGroupString
                                dtPR = blConnect.GetDataTable(strPerfRecordsSelect & strDateRangePerfRecords)
                                dtPR.TableName = "PerformanceRecords"

                                dtDSPerfRecords.Rows.Clear()

                                For Each drPR1 In dtPR.Rows
                                    drDSPerfRecords = dtDSPerfRecords.NewPerformanceRecordsRow
                                    drDSPerfRecords.UnitShortName = drPR1.Item("UnitShortName").ToString
                                    drDSPerfRecords.UtilityUnitCode = drPR1.Item("UtilityUnitCode").ToString
                                    drDSPerfRecords.Year = Convert.ToInt16(drPR1.Item("Year"))
                                    drDSPerfRecords.Period = drPR1.Item("Period").ToString
                                    If Not IsDBNull(drPR1.Item("NetMaxCap")) Then
                                        drDSPerfRecords.NetMaxCap = Convert.ToDecimal(drPR1.Item("NetMaxCap"))
                                    End If
                                    If Not IsDBNull(drPR1.Item("NetGen")) Then
                                        drDSPerfRecords.NetGen = Convert.ToDecimal(drPR1.Item("NetGen"))
                                    End If
                                    drDSPerfRecords.PeriodHours = Convert.ToDecimal(drPR1.Item("PeriodHours"))
                                    If Not IsDBNull(drPR1.Item("NCF")) Then
                                        drDSPerfRecords.NCF = Convert.ToDouble(drPR1.Item("NCF"))
                                    End If
                                    drDSPerfRecords.AttemptedStarts = Convert.ToInt32(drPR1.Item("AttemptedStarts"))
                                    drDSPerfRecords.ActualStarts = Convert.ToInt32(drPR1.Item("ActualStarts"))
                                    If Not IsDBNull(drPR1.Item("StartingReliability")) Then
                                        drDSPerfRecords.StartingReliability = Convert.ToDecimal(drPR1.Item("StartingReliability"))
                                    End If
                                    drDSPerfRecords.TL_DateTime = Convert.ToDateTime(drPR1.Item("TL_DateTime"))
                                    dtDSPerfRecords.AddPerformanceRecordsRow(drDSPerfRecords)
                                Next

                                dtDSPerfRecords.TableName = "PerformanceRecords"

                                DSTotalArray.Clear()

                                For Each drDSPerfRecords In dtDSPerfRecords.Rows

                                    If DSTotalArray.Contains(drDSPerfRecords.UnitShortName) Then

                                        hdTemp = DirectCast(DSTotalArray.Item(drDSPerfRecords.UnitShortName), DemandStatsTotal)

                                        If drDSPerfRecords.Year > hdTemp.Year Then
                                            hdTemp.Year = drDSPerfRecords.Year
                                        End If

                                        If drDSPerfRecords.Period > hdTemp.Period Then
                                            hdTemp.Period = drDSPerfRecords.Period
                                        End If

                                        If Not drDSPerfRecords.IsNetMaxCapNull Then

                                            If IsDBNull(hdTemp.NetMaxCap) Then
                                                hdTemp.NetMaxCap = drDSPerfRecords.NetMaxCap
                                            Else
                                                hdTemp.NetMaxCap = hdTemp.NetMaxCap + drDSPerfRecords.NetMaxCap
                                            End If

                                        End If

                                        If Not drDSPerfRecords.IsNetGenNull Then

                                            If IsDBNull(hdTemp.NetGen) Then
                                                hdTemp.NetGen = drDSPerfRecords.NetGen
                                            Else
                                                hdTemp.NetGen = hdTemp.NetGen + drDSPerfRecords.NetGen
                                            End If

                                        End If

                                        If Not drDSPerfRecords.IsPeriodHoursNull Then

                                            If IsDBNull(hdTemp.PeriodHours) Then
                                                hdTemp.PeriodHours = drDSPerfRecords.PeriodHours
                                            Else
                                                hdTemp.PeriodHours = hdTemp.PeriodHours + drDSPerfRecords.PeriodHours
                                            End If

                                        End If

                                        If Not drDSPerfRecords.IsAttemptedStartsNull Then

                                            If IsDBNull(hdTemp.AttemptedStarts) Then
                                                hdTemp.AttemptedStarts = drDSPerfRecords.AttemptedStarts
                                            Else
                                                hdTemp.AttemptedStarts = hdTemp.AttemptedStarts + drDSPerfRecords.AttemptedStarts
                                            End If

                                        End If

                                        If Not drDSPerfRecords.IsActualStartsNull Then
                                            If IsDBNull(hdTemp.ActualStarts) Then
                                                hdTemp.ActualStarts = drDSPerfRecords.ActualStarts
                                            Else
                                                hdTemp.ActualStarts = hdTemp.ActualStarts + drDSPerfRecords.ActualStarts
                                            End If
                                        End If

                                        hdTemp.NumberOfRecords = hdTemp.NumberOfRecords + 1

                                        DSTotalArray.Item(drDSPerfRecords.UnitShortName) = hdTemp

                                    Else
                                        DSTotalArray.Add(drDSPerfRecords.UnitShortName, New DemandStatsTotal(drDSPerfRecords.UnitShortName, _
                                            drDSPerfRecords.UtilityUnitCode, drDSPerfRecords.Year, drDSPerfRecords.Period, drDSPerfRecords.NetMaxCap, _
                                            drDSPerfRecords.NetGen, drDSPerfRecords.PeriodHours, 0, drDSPerfRecords.AttemptedStarts, _
                                            drDSPerfRecords.ActualStarts, 0, dtMaxEFORd, 1))

                                    End If

                                Next

                                For Each de In DSTotalArray

                                    hdTemp = DirectCast(de.Value, DemandStatsTotal)

                                    drDSPerfRecords = dtDSPerfRecords.NewPerformanceRecordsRow
                                    drDSPerfRecords.UnitShortName = hdTemp.UnitShortName
                                    drDSPerfRecords.UtilityUnitCode = hdTemp.UtilityUnitCode
                                    drDSPerfRecords.Year = hdTemp.Year
                                    drDSPerfRecords.Period = hdTemp.Period
                                    If Not IsDBNull(hdTemp.NetMaxCap) And hdTemp.NumberOfRecords > 0 Then
                                        hdTemp.NetMaxCap = hdTemp.NetMaxCap / hdTemp.NumberOfRecords
                                    End If
                                    drDSPerfRecords.NetMaxCap = hdTemp.NetMaxCap
                                    drDSPerfRecords.NetGen = hdTemp.NetGen
                                    drDSPerfRecords.PeriodHours = hdTemp.PeriodHours

                                    If hdTemp.PeriodHours > 0 Then
                                        drDSPerfRecords.NCF = (100.0 * hdTemp.NetGen) / hdTemp.PeriodHours
                                    End If

                                    If IsDBNull(hdTemp.AttemptedStarts) Then
                                        drDSPerfRecords.AttemptedStarts = 0
                                    Else
                                        drDSPerfRecords.AttemptedStarts = hdTemp.AttemptedStarts
                                    End If

                                    If IsDBNull(hdTemp.ActualStarts) Then
                                        drDSPerfRecords.ActualStarts = 0
                                    Else
                                        drDSPerfRecords.ActualStarts = hdTemp.ActualStarts
                                    End If

                                    If drDSPerfRecords.AttemptedStarts > 0 Then
                                        drDSPerfRecords.StartingReliability = Convert.ToDecimal(100.0 * drDSPerfRecords.ActualStarts) / drDSPerfRecords.AttemptedStarts
                                    End If

                                    drDSPerfRecords.TL_DateTime = dtMaxEFORd

                                    dtDSPerfRecords.AddPerformanceRecordsRow(drDSPerfRecords)

                                Next

                                dsDSPerfRecords.Merge(dtDSPerfRecords)

                            End If

                            Dim myArray As New ArrayList

                            myArray = blConnect.getARGroupRecord(strUnitOrGroupSelected)

                            strTempGroupName = myArray(1).ToString.Trim

                            drEFORdGroupSum.UnitShortName = myArray(0).ToString.Trim
                            drEFORdGroupSum.Granularity = "Monthly"

                            drEFORdTotalTable.UnitShortName = myArray(0).ToString.Trim
                            drEFORdTotalTable.UnitName = myArray(1).ToString.Trim
                            drEFORdTotalTable.UtilityUnitCode = "Group"

                            drEFORdTotalTable.PeriodStart = DateTime.Parse(strPeriodStart)
                            drEFORdTotalTable.PeriodEnd = DateTime.Parse(strPeriodEnd)
                            drEFORdTotalTable.SF = drEFORdGroupSum.SF
                            drEFORdTotalTable.U1 = drEFORdGroupSum.U1
                            drEFORdTotalTable.U2 = drEFORdGroupSum.U2
                            drEFORdTotalTable.U3 = drEFORdGroupSum.U3
                            drEFORdTotalTable.D1 = drEFORdGroupSum.D1
                            drEFORdTotalTable.D2 = drEFORdGroupSum.D2
                            drEFORdTotalTable.D3 = drEFORdGroupSum.D3
                            drEFORdTotalTable.RS = drEFORdGroupSum.RS
                            drEFORdTotalTable.EUFDH_RS = drEFORdGroupSum.EUFDH_RS
                            drEFORdTotalTable.SH = drEFORdGroupSum.SH
                            drEFORdTotalTable.AH = drEFORdGroupSum.AH
                            drEFORdTotalTable.FOCount = drEFORdGroupSum.FOCount
                            drEFORdTotalTable.RSCount = drEFORdGroupSum.RSCount
                            drEFORdTotalTable.ActualStartsCount = drEFORdGroupSum.ActualStartsCount
                            drEFORdTotalTable.AttemptedStartsCount = drEFORdGroupSum.AttemptedStartsCount
                            drEFORdTotalTable.AttemptedStarts = drEFORdGroupSum.AttemptedStarts
                            drEFORdTotalTable.ActualStarts = drEFORdGroupSum.ActualStarts
                            If Not drEFORdGroupSum.IsServiceHourMethodNull Then
                                drEFORdTotalTable.ServiceHourMethod = drEFORdGroupSum.ServiceHourMethod
                            End If

                            strTemp = blConnect.GetAnalysisSettingStr("EFORd", "Fleet", "NERC").Trim.ToUpper

                            If strTemp = "NERC" Then

                                If drEFORdGroupSum.IsDEFORNull Then
                                    drEFORdGroupSum.DEFOR = 0
                                End If

                                drEFORdTotalTable.DEFOR = drEFORdGroupSum.DEFOR

                                If drEFORdGroupSum.IsDFORNull Then
                                    drEFORdGroupSum.DFOR = 0
                                End If

                                drEFORdTotalTable.DFOR = drEFORdGroupSum.DFOR

                                drEFORdTotalTable.FL_Numerator = drEFORdGroupSum.FL_Numerator
                                drEFORdTotalTable.FL_Denominator = drEFORdGroupSum.FL_Denominator
                                drEFORdTotalTable.FL_FORdNumerator = drEFORdGroupSum.FL_FORdNumerator

                                ' Method (I): Pooled individual Unit Demand Studies

                                ' This method can give more weight to individual units with extreme 
                                ' EFORd that have very few service hours, but with longer study time
                                ' periods the difference between the results of Methods I and II should be less.

                                If Not drEFORdTotalTable.IsFL_DenominatorNull Then

                                    If drEFORdTotalTable.FL_Denominator > 0 Then

                                        If Not drEFORdTotalTable.IsFL_NumeratorNull Then
                                            drEFORdTotalTable.DEFOR = drEFORdTotalTable.FL_Numerator / drEFORdTotalTable.FL_Denominator
                                        End If

                                        If Not drEFORdTotalTable.IsFL_FORdNumeratorNull Then
                                            drEFORdTotalTable.DFOR = drEFORdTotalTable.FL_FORdNumerator / drEFORdTotalTable.FL_Denominator
                                        End If

                                    End If

                                End If

                            ElseIf strTemp = "NERC2" Then

                                ' Method (II):  Group Demand Studies

                                ' This method may be more applicable in studying group statistics on units
                                ' with known similar demand patterns, especially for forecasting and modeling. 
                                ' By calculating the f-factors over the group’s total FOH, SH, RSH, and starts,
                                ' the f-factor is "smoothed" and not subject to be unduly influenced by an one
                                ' or more single units statistics that may have very high or very low hours or starts.

                                blConnect.ISONEEFORdCalc(drEFORdGroupSum)

                                If drEFORdGroupSum.IsDEFORNull Then
                                    drEFORdGroupSum.DEFOR = 0
                                End If
                                drEFORdTotalTable.DEFOR = drEFORdGroupSum.DEFOR

                                If drEFORdGroupSum.IsDFORNull Then
                                    drEFORdGroupSum.DFOR = 0
                                End If

                                drEFORdTotalTable.DFOR = drEFORdGroupSum.DFOR

                                drEFORdTotalTable.FL_Numerator = drEFORdGroupSum.FL_Numerator
                                drEFORdTotalTable.FL_Denominator = drEFORdGroupSum.FL_Denominator
                                drEFORdTotalTable.FL_FORdNumerator = drEFORdGroupSum.FL_FORdNumerator

                            End If

                            blConnect.LoadEFORdTotal(strPeriodStart, strPeriodEnd, drEFORdTotalTable)

                            If strUnitGroupString.ToUpper.IndexOf("ARGROUPS") <= 0 Then
                                ' testing only
                                dtEFORdTotal.AddEFORdRow(drEFORdGroupSum)
                            End If

                            dsEFORd.Merge(dtEFORdTotal)

                            drSetupDE = dtSetupDE.NewSetupRow
                            drSetupDE.Item("UnitShortName") = myArray(0)
                            drSetupDE.Item("UnitName") = myArray(1)
                            drSetupDE.Item("UtilityUnitCode") = "000000"
                            drSetupDE.Item("PriFuelCode") = "XX"
                            drSetupDE.Item("UnitType") = "TempGroup"
                            drSetupDE.Item("DaylightSavingTime") = 0
                            drSetupDE.Item("JointOwnership") = 0
                            drSetupDE.Item("ExpandedReporting") = 0
                            drSetupDE.Item("InputDataRequirements") = 0
                            drSetupDE.Item("BtuOrHeatContent") = 0
                            drSetupDE.Item("FuelQualityShow") = 0
                            drSetupDE.Item("Perf02DecimalPlaces") = 1
                            drSetupDE.Item("BulkRS") = 0
                            drSetupDE.Item("EnglishOrMetric") = 0
                            drSetupDE.Item("ServiceHourMethod") = 0
                            drSetupDE.Item("Example3D") = 0
                            dtSetupDE.AddSetupRow(drSetupDE)
                        Else

                            dsEFORd.Merge(dtEFORdTotal)

                        End If

                        dsEFORd.Merge(dtSetupDE)
                        dsEFORd.Merge(dsDSPerfRecords.Tables("PerformanceRecords"))
                        dsDSPerfRecords.Tables("PerformanceRecords").Rows.Clear()

                        dsEFORd.DataSetName = "EFORd"

                        Dim dsNYISOExport As New NYISOExportToExcel
                        Dim dtNYISOExport As New NYISOExportToExcel.DemandStatsDataTable
                        Dim dtNYISOExportTOTAL As New NYISOExportToExcel.DemandStatsDataTable
                        Dim drNYISOExport As NYISOExportToExcel.DemandStatsRow

                        Dim ExportTableRel As DataRelation

                        Try

                            Dim parentCols(1) As DataColumn
                            parentCols(0) = dsEFORd.Tables("EFORd").Columns("UnitShortName")
                            parentCols(1) = dsEFORd.Tables("EFORd").Columns("TL_DateTime")

                            Dim childCols(1) As DataColumn
                            childCols(0) = dsEFORd.Tables("PerformanceRecords").Columns("UnitShortName")
                            childCols(1) = dsEFORd.Tables("PerformanceRecords").Columns("TL_DateTime")

                            Dim keys1(1) As DataColumn
                            keys1(0) = dsEFORd.Tables("EFORd").Columns("UnitShortName")
                            keys1(1) = dsEFORd.Tables("EFORd").Columns("TL_DateTime")

                            dsEFORd.Tables("EFORd").PrimaryKey = keys1

                            Dim drRow As DataRow
                            Dim findTheseVals(1) As Object
                            Dim foundRow As DataRow
                            dtDSPerfRecords.Rows.Clear()

                            For Each drRow In dsEFORd.Tables("PerformanceRecords").Rows
                                ' Set the values of the keys to find.
                                findTheseVals(0) = drRow("UnitShortName")
                                findTheseVals(1) = drRow("TL_DateTime")

                                foundRow = dsEFORd.Tables("EFORd").Rows.Find(findTheseVals)

                                If Not (foundRow Is Nothing) Then
                                    drDSPerfRecords = dtDSPerfRecords.NewPerformanceRecordsRow
                                    drDSPerfRecords.UnitShortName = drRow.Item("UnitShortName").ToString
                                    drDSPerfRecords.UtilityUnitCode = drRow.Item("UtilityUnitCode").ToString
                                    drDSPerfRecords.Year = Convert.ToInt16(drRow.Item("Year"))
                                    drDSPerfRecords.Period = drRow.Item("Period").ToString
                                    If Not IsDBNull(drRow.Item("NetMaxCap")) Then
                                        drDSPerfRecords.NetMaxCap = Convert.ToDecimal(drRow.Item("NetMaxCap"))
                                    End If
                                    If Not IsDBNull(drRow.Item("NetGen")) Then
                                        drDSPerfRecords.NetGen = Convert.ToDecimal(drRow.Item("NetGen"))
                                    End If

                                    drDSPerfRecords.PeriodHours = Convert.ToDecimal(drRow.Item("PeriodHours"))

                                    If Not IsDBNull(drRow.Item("NCF")) Then
                                        drDSPerfRecords.NCF = Convert.ToDouble(drRow.Item("NCF"))
                                    End If
                                    If Not IsDBNull(drRow.Item("AttemptedStarts")) Then
                                        drDSPerfRecords.AttemptedStarts = Convert.ToInt32(drRow.Item("AttemptedStarts"))
                                    End If
                                    If Not IsDBNull(drRow.Item("ActualStarts")) Then
                                        drDSPerfRecords.ActualStarts = Convert.ToInt32(drRow.Item("ActualStarts"))
                                    End If
                                    If Not IsDBNull(drRow.Item("StartingReliability")) Then
                                        drDSPerfRecords.StartingReliability = Convert.ToDecimal(drRow.Item("StartingReliability"))
                                    End If

                                    drDSPerfRecords.TL_DateTime = Convert.ToDateTime(drRow.Item("TL_DateTime"))
                                    dtDSPerfRecords.AddPerformanceRecordsRow(drDSPerfRecords)
                                End If

                            Next

                            dsEFORd.Tables("PerformanceRecords").Rows.Clear()
                            dtDSPerfRecords.TableName = "PerformanceRecords"

                            dsDSPerfRecords.Merge(dtDSPerfRecords)

                            dsEFORd.Merge(dsDSPerfRecords.Tables("PerformanceRecords"))

                            ExportTableRel = New DataRelation("NYISOWork", parentCols, childCols)

                            dsEFORd.Relations.Add(ExportTableRel)

                            Dim drEFORdTemp As DataRow
                            Dim drPerfData() As DataRow

                            dsEFORd.Tables("EFORd").DefaultView.Sort = "UnitShortName, TL_DateTime"
                            Dim boolInActive As Boolean = False

                            For Each drEFORdTemp In dsEFORd.Tables("EFORd").Rows

                                drPerfData = drEFORdTemp.GetChildRows("NYISOWork", DataRowVersion.Current)

                                If drPerfData.Length <> 0 Then

                                    ' this is needed for Group or Summary ONLY so that the total records do not get added for the units

                                    If (Convert.ToDateTime(drEFORdTemp("TL_DateTime")).Year - Now.Year) > 75 Then
                                        drNYISOExport = dtNYISOExportTOTAL.NewDemandStatsRow
                                    Else
                                        drNYISOExport = dtNYISOExport.NewDemandStatsRow
                                    End If

                                    For Each drSetupDE In dtSetupDE.Rows
                                        If drSetupDE.UnitShortName.Trim = drEFORdTemp("UnitShortName").ToString.Trim Then
                                            drNYISOExport.UnitName = drSetupDE.UnitName
                                            Exit For
                                        End If
                                    Next

                                    If (Convert.ToDateTime(drEFORdTemp("TL_DateTime")).Year - Now.Year) > 75 Then
                                        drNYISOExport.MonthOrTotal = "TOTAL"
                                    Else

                                        boolInActive = True

                                        drNYISOExport.MonthOrTotal = Convert.ToDateTime(drEFORdTemp("TL_DateTime")).ToString("MMM yyyy")

                                        For Each aMonth As ActiveMonths In listDMonths
                                            If drEFORdTemp("UnitShortName").ToString.Trim = aMonth.ShortName.Trim And Convert.ToDateTime(drEFORdTemp("TL_DateTime")) = aMonth.T_DateTime Then
                                                boolInActive = False
                                                Exit For
                                            End If
                                        Next

                                        If boolInActive Then
                                            drNYISOExport.MonthOrTotal = Convert.ToDateTime(drEFORdTemp("TL_DateTime")).ToString("MMM yyyy") & " (IA)"
                                        End If


                                    End If

                                    Try
                                        If Not IsDBNull(drPerfData(0).Item("NetGen")) Then
                                            drNYISOExport.NetActualGeneration = Convert.ToDouble(drPerfData(0).Item("NetGen"))
                                        End If
                                    Catch ex As Exception
                                        drNYISOExport.NetActualGeneration = 0
                                    End Try

                                    drNYISOExport.RSH = Math.Round(Convert.ToDouble(drEFORdTemp("RS")), 2)
                                    drNYISOExport.EFDH = Math.Round(Convert.ToDouble(drEFORdTemp("D1")) + Convert.ToDouble(drEFORdTemp("D2")) + Convert.ToDouble(drEFORdTemp("D3")), 2)

                                    drNYISOExport.SH = Math.Round(Convert.ToDouble(drEFORdTemp("SH")), 2)
                                    drNYISOExport.OH = Math.Round(Convert.ToDouble(drEFORdTemp("U1")) + Convert.ToDouble(drEFORdTemp("U2")) + Convert.ToDouble(drEFORdTemp("U3")) + Convert.ToDouble(drEFORdTemp("SF")), 2)
                                    drNYISOExport.FOCount = Convert.ToInt32(drEFORdTemp("FOCount"))

                                    drNYISOExport.AttemptedStarts = Convert.ToInt32(drEFORdTemp("AttemptedStarts"))
                                    drNYISOExport.ActualStarts = Convert.ToInt32(drEFORdTemp("ActualStarts"))

                                    Try
                                        If Not IsDBNull(drPerfData(0).Item("StartingReliability")) Then
                                            drNYISOExport.StartingReliability = Math.Round(Convert.ToDouble(drPerfData(0).Item("StartingReliability")), 2)
                                        End If
                                    Catch ex As Exception

                                    End Try

                                    If (Convert.ToDouble(drEFORdTemp("E_U1")) + Convert.ToDouble(drEFORdTemp("E_U2")) + _
                                        Convert.ToDouble(drEFORdTemp("E_U3")) + Convert.ToDouble(drEFORdTemp("E_SF")) + _
                                        Convert.ToDouble(drEFORdTemp("E_SH"))) > 0 Then

                                        drNYISOExport.ForcedOutageRate = Math.Round((100.0 * (Convert.ToDouble(drEFORdTemp("E_U1")) + Convert.ToDouble(drEFORdTemp("E_U2")) + _
                                            Convert.ToDouble(drEFORdTemp("E_U3")) + Convert.ToDouble(drEFORdTemp("E_SF")))) / _
                                            (Convert.ToDouble(drEFORdTemp("E_U1")) + Convert.ToDouble(drEFORdTemp("E_U2")) + _
                                            Convert.ToDouble(drEFORdTemp("E_U3")) + Convert.ToDouble(drEFORdTemp("E_SF")) + _
                                            Convert.ToDouble(drEFORdTemp("E_SH"))), 2)

                                    End If

                                    drNYISOExport.EFORd = Math.Round(Convert.ToDouble(drEFORdTemp("DEFOR")), 2)

                                    If Convert.ToDouble(drEFORdTemp("E_PH")) > 0 Then

                                        drNYISOExport.EAF = Math.Round(Convert.ToDouble((100 * (Convert.ToDouble(drEFORdTemp("E_AH")) - Convert.ToDouble(drEFORdTemp("E_D1")) - Convert.ToDouble(drEFORdTemp("E_D2")) - _
                                          Convert.ToDouble(drEFORdTemp("E_D3")) - Convert.ToDouble(drEFORdTemp("E_D4")) - Convert.ToDouble(drEFORdTemp("E_D4_DE")) - _
                                          Convert.ToDouble(drEFORdTemp("E_PD")) - Convert.ToDouble(drEFORdTemp("E_PD_DE")) - Convert.ToDouble(drEFORdTemp("E_ESEDH")))) / Convert.ToDouble(drEFORdTemp("E_PH"))), 2)

                                    End If

                                    Try
                                        If Not IsDBNull(drPerfData(0).Item("NCF")) Then
                                            drNYISOExport.NetCapacityFactor = Math.Round(Convert.ToDouble(drPerfData(0).Item("NCF")), 2)
                                        End If
                                    Catch ex As Exception

                                    End Try

                                    drNYISOExport.Period = System.DateTime.Parse(strPeriodStart).ToString("MMM yyyy") & " - " & System.DateTime.Parse(strPeriodEnd).ToString("MMM yyyy")

                                    If drNYISOExport.MonthOrTotal = "TOTAL" Then
                                        dtNYISOExportTOTAL.AddDemandStatsRow(drNYISOExport)
                                    Else
                                        dtNYISOExport.AddDemandStatsRow(drNYISOExport)
                                    End If

                                End If

                            Next

                            dtNYISOExportTOTAL.DefaultView.Sort = "UnitName"

                            Dim expression As String = ""

                            If IsNothing(strTempGroupName) Then
                                expression = String.Empty
                            Else
                                If strTempGroupName.Trim = String.Empty Then
                                    expression = String.Empty
                                Else
                                    expression = "UnitName <> '" & strTempGroupName & "'"
                                End If
                            End If

                            Dim arrSortedDataRows As DataRow() = dtNYISOExportTOTAL.Select(expression)

                            For Each dr As DataRow In arrSortedDataRows
                                dtNYISOExport.ImportRow(dr)
                            Next

                            If IsNothing(strTempGroupName) Then
                                expression = String.Empty
                            Else
                                If strTempGroupName.Trim = String.Empty Then
                                    expression = String.Empty
                                Else
                                    expression = "UnitName = '" & strTempGroupName & "'"
                                End If
                            End If

                            arrSortedDataRows = dtNYISOExportTOTAL.Select(expression)

                            For Each dr As DataRow In arrSortedDataRows
                                dtNYISOExport.ImportRow(dr)
                            Next

                            dtNYISOExport.TableName = "dsNYISOExportToExcel"
                            dsNYISOExport.Merge(dtNYISOExport)
                            dsNYISOExport.DataSetName = "NYISOExportToExcel"

                            Try

                                mydtExcel.Merge(dtNYISOExport)
                                Session("dtExcel") = mydtExcel

                                'ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/secure") & "\BatchErrors.rdlc"
                                ReportViewer1.LocalReport.ReportPath = Server.MapPath("NYISOExportToExcel.rdlc")
                                ReportViewer1.LocalReport.DisplayName = "Demand-related Statistics (NYISO Portal)"
                                ReportViewer1.LocalReport.DataSources.Clear()
                                ReportViewer1.LocalReport.EnableExternalImages = True
                                ReportViewer1.ProcessingMode = ProcessingMode.Local
                                ReportViewer1.AsyncRendering = False
                                ReportViewer1.SizeToReportContent = True
                                ReportViewer1.LocalReport.DataSources.Add(New ReportDataSource("dsNYISOExportToExcel", mydtExcel))
                                ReportViewer1.LocalReport.Refresh()

                                lblConfirm.Text = "Data Exported."
                                lblConfirm.Attributes.Add("style", "color:green; font: bold 14px/16px Sans-Serif,Arial")

                            Catch ex As Exception
                                lblConfirm.Text = "There was an error."
                                lblConfirm.Attributes.Add("style", "color:red; font: bold 14px/16px Sans-Serif,Arial")

                            End Try

                        Catch ex As System.ArgumentException
                            sScript = "<script language='javascript'>"
                            sScript += "alert('Error in Creating Relationship - " & ex.Message & " - Argument Error in Export EFORd tables to Excel (NYISO)');"
                            sScript += "</script>"
                            Response.Write(sScript)

                        Catch ex As Exception
                            sScript = "<script language='javascript'>"
                            sScript += "alert('Error in Creating Relationship - " & ex.Message & " - ERROR in Export EFORd tables to Excel (NYISO)');"
                            sScript += "</script>"
                            Response.Write(sScript)
                        End Try

                        blConnect.DeleteMyGroups(intGroupID)

                    End If

                End If

            End If

        End If

    End Sub

    Protected Sub btnExcel_Click(sender As Object, e As EventArgs)

        mydtExcel = CType(Session("dtExcel"), DataTable)

        Dim warnings As Warning() = Nothing
        Dim streamids As String() = Nothing
        Dim mimeType As String = Nothing
        Dim encoding As String = Nothing
        Dim extension As String = Nothing
        Dim bytes As Byte()

        ReportViewer1.LocalReport.ReportPath = Server.MapPath("NYISOExportToExcel.rdlc")
        ReportViewer1.LocalReport.DisplayName = "NYISO Export To Excel"
        ReportViewer1.LocalReport.DataSources.Clear()
        ReportViewer1.LocalReport.EnableExternalImages = True
        ReportViewer1.ProcessingMode = ProcessingMode.Local
        ReportViewer1.AsyncRendering = False
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.LocalReport.DataSources.Add(New ReportDataSource("dsNYISOExportToExcel", mydtExcel))

        bytes = ReportViewer1.LocalReport.Render("Excel", Nothing, mimeType, encoding, extension, streamids, warnings)

        ' Now that you have all the bytes representing the Excel report, buffer it and send it to the client.
        Response.Buffer = True
        Response.Clear()
        Response.ContentType = mimeType
        Response.AddHeader("content-disposition", "attachment; filename=NYISOExportToExcel." + extension)
        Response.BinaryWrite(bytes) ' create the file
        Response.Flush() ' send it to the client to download
    End Sub

    Public Property DateSelectionRange() As String
        Get
            Return strDateSelectionRange
        End Get
        Set(ByVal Value As String)
            strDateSelectionRange = Value.Trim
        End Set
    End Property

    Private Function GetUtilityUnitData() As SetupDE.SetupDataTable

        Dim stringSelect As String = "SELECT * FROM Setup ORDER BY UnitName, UtilityUnitCode"

        Dim tblSetup As SetupDE.SetupDataTable = New SetupDE.SetupDataTable

        Try
            tblSetup.Merge(blConnect.GetDataTable(stringSelect), True, MissingSchemaAction.Ignore)

        Catch ex As System.Exception

            'Dim reader As New IO.StreamReader(HttpContext.Current.Server.MapPath("~/Secure/GADSNG.xml"))
            'Dim moXML As SetupSettings.Settings
            'Dim strMyKey As String
            'strMyKey = Server.MapPath("..") & "/secure/GADSNG.xml"
            'moXML = New SetupSettings.Settings(AppName, strMyKey, reader)

            If blConnect.ThisProvider = "SqlClient" Then

                Dim tmpStatement As String = stringSelect.Replace("GADSNG.", "dbo.")
                Try
                    tblSetup.Merge(blConnect.GetDataTable(tmpStatement))
                Catch e As Exception
                    Dim sScript As String = String.Empty
                    sScript = "<script language='javascript'>"
                    sScript += "alert('Error in GetUtilityUnitData:" & e.ToString & "');"
                    sScript += "</script>"
                End Try
            Else
                Dim sScript As String = String.Empty
                sScript = "<script language='javascript'>"
                sScript += "alert('Error in GetUtilityUnitData:" & ex.ToString & "');"
                sScript += "</script>"
            End If

        End Try

        Return tblSetup

    End Function

End Class


Public Class ActiveMonths

    Public Property ShortName() As String
        Get
            Return m_ShortName
        End Get
        Set(value As String)
            m_ShortName = value
        End Set
    End Property
    Private m_ShortName As String

    Public Property T_DateTime() As DateTime
        Get
            Return mT_DateTime
        End Get
        Set(value As DateTime)
            mT_DateTime = value
        End Set
    End Property
    Private mT_DateTime As DateTime

End Class

Public Class DemandStatsTotal

    'strPerfRecordsSelect = "SELECT UnitShortName, UtilityUnitCode, MAX(Year) AS Year, MAX(Period) AS Period, AVG(NetMaxCap) AS NetMaxCap, SUM(NetGen) AS NetGen, " & _
    '                       "SUM(N_PeriodHours) AS PeriodHours, 0.00 AS NCF, SUM(AttemptedStarts) AS AttemptedStarts, SUM(ActualStarts) AS ActualStarts, 0.00 AS StartingReliability, " & _
    '                       "MAX(PerfCalcDate) AS TL_DateTime FROM PerformanceRecords WHERE (Period BETWEEN '01' AND '12') AND "

    Private columnUnitShortName As String
    Private columnUtilityUnitCode As String
    Private columnYear As Short
    Private columnPeriod As String
    Private columnNetMaxCap As Decimal
    Private columnNetGen As Decimal
    Private columnPeriodHours As Decimal
    Private columnNCF As Double
    Private columnAttemptedStarts As Integer
    Private columnActualStarts As Integer
    Private columnStartingReliability As Decimal
    Private columnTL_DateTime As DateTime
    Private fieldNumberOfRecords As Integer

    Public Sub New(ByVal UnitShortName As String, ByVal UtilityUnitCode As String, ByVal Year As Short, ByVal Period As String, _
        ByVal NetMaxCap As Decimal, ByVal NetGen As Decimal, ByVal PeriodHours As Decimal, ByVal NCF As Double, _
        ByVal AttemptedStarts As Integer, ByVal ActualStarts As Integer, ByVal StartingReliability As Decimal, _
        ByVal TL_DateTime As DateTime, ByVal NumberOfRecords As Integer)

        columnUnitShortName = UnitShortName
        columnUtilityUnitCode = UtilityUnitCode
        columnYear = Year
        columnPeriod = Period
        columnNetMaxCap = NetMaxCap
        columnNetGen = NetGen
        columnPeriodHours = PeriodHours
        columnNCF = NCF
        columnAttemptedStarts = AttemptedStarts
        columnActualStarts = ActualStarts
        columnStartingReliability = StartingReliability
        columnTL_DateTime = TL_DateTime
        fieldNumberOfRecords = NumberOfRecords

    End Sub

    Public Property UnitShortName() As String
        Get
            Return columnUnitShortName
        End Get
        Set(ByVal Value As String)
            columnUnitShortName = Value
        End Set
    End Property

    Public Property UtilityUnitCode() As String
        Get
            Return columnUtilityUnitCode
        End Get
        Set(ByVal Value As String)
            columnUtilityUnitCode = Value
        End Set
    End Property

    Public Property Year() As Short
        Get
            Return columnYear
        End Get
        Set(ByVal Value As Short)
            columnYear = Value
        End Set
    End Property

    Public Property Period() As String
        Get
            Return columnPeriod
        End Get
        Set(ByVal Value As String)
            columnPeriod = Value
        End Set
    End Property

    Public Property NetMaxCap() As Decimal
        Get
            Return columnNetMaxCap
        End Get
        Set(ByVal Value As Decimal)
            columnNetMaxCap = Value
        End Set
    End Property

    Public Property NetGen() As Decimal
        Get
            Return columnNetGen
        End Get
        Set(ByVal Value As Decimal)
            columnNetGen = Value
        End Set
    End Property

    Public Property PeriodHours() As Decimal
        Get

            Return columnPeriodHours
        End Get
        Set(ByVal Value As Decimal)
            columnPeriodHours = Value
        End Set
    End Property

    Public Property NCF() As Double
        Get
            Return columnNCF
        End Get
        Set(ByVal Value As Double)
            columnNCF = Value
        End Set
    End Property

    Public Property AttemptedStarts() As Integer
        Get
            Return columnAttemptedStarts
        End Get
        Set(ByVal Value As Integer)
            columnAttemptedStarts = Value
        End Set
    End Property

    Public Property ActualStarts() As Integer
        Get
            Return columnActualStarts
        End Get
        Set(ByVal Value As Integer)
            columnActualStarts = Value
        End Set
    End Property

    Public Property StartingReliability() As Decimal
        Get
            Return columnStartingReliability
        End Get
        Set(ByVal Value As Decimal)
            columnStartingReliability = Value
        End Set
    End Property

    Public Property TL_DateTime() As Date
        Get
            Return columnTL_DateTime
        End Get
        Set(ByVal Value As Date)
            columnTL_DateTime = Value
        End Set
    End Property

    Public Property NumberOfRecords() As Integer
        Get
            Return fieldNumberOfRecords
        End Get
        Set(ByVal Value As Integer)
            fieldNumberOfRecords = Value
        End Set
    End Property

End Class

