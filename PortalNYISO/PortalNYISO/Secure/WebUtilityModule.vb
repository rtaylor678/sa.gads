Imports Microsoft.DSG.Security.CryptoServices
Imports BusinessLayer
Imports ARWindowsUI.ARWindowsUI
Imports SetupSettings.Settings
Imports Microsoft.ApplicationBlocks.ExceptionManagement
Imports System
Imports System.Globalization
Imports System.IO
Imports System.Web.HttpRequest
Imports System.Security.Principal
Imports Microsoft.VisualBasic
Imports System.Threading
Imports System.Web.UI.WebControls
Imports System.Web.SessionState.HttpSessionState
Imports System.Collections.Specialized
Imports System.Collections

Module WebUtilityModule

    'Const COPUCUTOFFDATE As String = "10/01/2006"

#Region " TimeBetween(ByVal BeginDateTime As DateTime, ByVal EndingDateTime As DateTime, ByVal DaylightSavingTime As Integer) As Double "

    Public Function TimeBetween(ByVal BeginDateTime As DateTime, ByVal EndingDateTime As DateTime, ByVal DaylightSavingTime As Integer) As Double

        Dim longInterval As Long
        Dim douInterval As Double

        ' Calculate the "unadjusted" time difference between the two date/times in fractions of an hour

        If IsNothing(BeginDateTime) Or IsNothing(EndingDateTime) Then
            Return 0
        End If

        If BeginDateTime.Hour = 23 And BeginDateTime.Minute = 59 And BeginDateTime.Second = 59 Then
            BeginDateTime = BeginDateTime.AddSeconds(1)
        End If

        If EndingDateTime.Hour = 23 And EndingDateTime.Minute = 59 And EndingDateTime.Second = 59 Then
            EndingDateTime = EndingDateTime.AddSeconds(1)
        End If

        ' Calculate the "unadjusted" time difference between the two date/times in fractions of an hour

        Try

            longInterval = DateDiff(DateInterval.Second, BeginDateTime, EndingDateTime)
            'douInterval = Math.Round((longInterval / 3600.0), 6)
            douInterval = Convert.ToDouble((longInterval / 3600.0))

        Catch ex As Exception

        End Try

        Dim dtBegin1 As DateTime
        Dim dtEnd1 As DateTime
        Dim dtBegin2 As DateTime
        Dim dtEnd2 As DateTime

        'Dim Fastest As Integer
        Dim dtDate As DateTime
        Dim dtStart As DateTime
        Dim dtStop As DateTime

        ' DaylightSavingTime = 1

        ' In 1987 daylight saving time was moved to the first Sunday in April; it used to start the LAST Sunday

        ' Daylight Saving Time 1987 - 2006
        ' North America (Canada, US and Mexico) First Sunday in April through last Sunday in October at 2:00 AM local time
        ' In April 2:00 AM -> 3:00 AM
        ' In October 2:00 AM -> 1:00 AM

        'Daylight Saving Time Schedule USA and Canada starting in 2007
        'As prescribed: The Energy Policy Act of 2005 

        'Start: Second Sunday in March 
        'End: First Sunday in November 
        'Time: 2 am local time 

        'Beginning in 2007, Daylight Saving Time is extended one month and the schedule for Canada and
        '   the states of the United States that adopt daylight saving time will be:

        '2 a.m. on the Second Sunday in March
        'to
        '2 a.m. on the First Sunday of November

        If BeginDateTime.Year >= 1987 And BeginDateTime.Year < 2007 Then

            ' First Sunday in April
            dtStart = System.DateTime.Parse("04/01/" & BeginDateTime.Year.ToString)
            dtStop = System.DateTime.Parse("04/07/" & BeginDateTime.Year.ToString)

        ElseIf BeginDateTime.Year >= 2007 Then

            ' Second Sunday in March
            dtStart = System.DateTime.Parse("03/08/" & BeginDateTime.Year.ToString)
            dtStop = System.DateTime.Parse("03/14/" & BeginDateTime.Year.ToString)

        ElseIf BeginDateTime.Year < 1987 Then

            ' Last Sunday in April
            dtStart = System.DateTime.Parse("04/24/" & BeginDateTime.Year.ToString)
            dtStop = System.DateTime.Parse("04/30/" & BeginDateTime.Year.ToString)

        End If

        dtDate = ReturnNextSunday(dtStart)
        dtBegin1 = System.DateTime.Parse(dtDate.ToShortDateString & " 02:00 AM")
        dtEnd1 = System.DateTime.Parse(dtDate.ToShortDateString & " 03:00 AM")

        'dtDate = dtStart

        'Do While dtDate <= dtStop

        '    If dtDate.DayOfWeek = DayOfWeek.Sunday Then

        '        dtBegin1 = System.DateTime.Parse(dtDate.ToShortDateString & " 02:00 AM")
        '        dtEnd1 = System.DateTime.Parse(dtDate.ToShortDateString & " 03:00 AM")

        '        Exit Do

        '    End If

        '    dtDate = DateAdd(DateInterval.Day, 1, dtDate)

        'Loop

        If BeginDateTime.Year < 2007 Then

            ' Last Sunday in October for Mexico and USA/Canada before 2007
            dtStart = System.DateTime.Parse("10/25/" & BeginDateTime.Year.ToString)
            dtStop = System.DateTime.Parse("10/31/" & BeginDateTime.Year.ToString)

        Else

            ' First Sunday in November for USA and Canada starting in 2007
            dtStart = System.DateTime.Parse("11/01/" & BeginDateTime.Year.ToString)
            dtStop = System.DateTime.Parse("11/07/" & BeginDateTime.Year.ToString)

        End If

        dtDate = ReturnNextSunday(dtStart)
        dtBegin2 = System.DateTime.Parse(dtDate.ToShortDateString & " 01:00 AM")
        dtEnd2 = System.DateTime.Parse(dtDate.ToShortDateString & " 02:00 AM")

        'dtDate = dtStart

        'Do While dtDate <= dtStop

        '    If dtDate.DayOfWeek = DayOfWeek.Sunday Then

        '        dtBegin2 = System.DateTime.Parse(dtDate.ToShortDateString & " 01:00 AM")
        '        dtEnd2 = System.DateTime.Parse(dtDate.ToShortDateString & " 02:00 AM")

        '        Exit Do

        '    End If

        '    dtDate = DateAdd(DateInterval.Day, 1, dtDate)

        'Loop

        ' BeginDateTime, EndingDateTime

        If DateTime.Compare(BeginDateTime, dtBegin1) <= 0 And DateTime.Compare(EndingDateTime, dtEnd1) >= 0 And _
            DateTime.Compare(EndingDateTime, dtBegin2) <= 0 Then

            ' The event straddles the change to DST time from Standard time
            Return douInterval - 1

        ElseIf DateTime.Compare(BeginDateTime, dtBegin2) <= 0 And DateTime.Compare(EndingDateTime, dtEnd2) >= 0 And _
            DateTime.Compare(BeginDateTime, dtEnd1) >= 0 Then

            ' The event straddles the change from DST time to Standard time
            Return douInterval + 1

        Else

            Return douInterval

        End If

        Return douInterval

    End Function

    Private Function ReturnNextSunday(ByVal dtTemp As DateTime) As DateTime

        Select Case dtTemp.DayOfWeek
            Case DayOfWeek.Sunday
                ' Trying to find Sunday
                Return dtTemp
            Case DayOfWeek.Monday
                Return dtTemp.AddDays(6)
            Case DayOfWeek.Tuesday
                Return dtTemp.AddDays(5)
            Case DayOfWeek.Wednesday
                Return dtTemp.AddDays(4)
            Case DayOfWeek.Thursday
                Return dtTemp.AddDays(3)
            Case DayOfWeek.Friday
                Return dtTemp.AddDays(2)
            Case DayOfWeek.Saturday
                Return dtTemp.AddDays(1)
            Case Else
                Return dtTemp
        End Select

    End Function

#End Region

#Region " ValidDSTStart(ByVal CheckDateTime As DateTime, ByVal DaylightSavingTime As Integer) As String "

    Public Function ValidDSTStart(ByVal CheckDateTime As DateTime, ByVal DaylightSavingTime As Integer) As String

        Dim dtBegin As DateTime
        Dim dtEnd As DateTime
        'Dim Fastest As Integer
        Dim dtDate As DateTime
        Dim dtStart As DateTime
        Dim dtStop As DateTime


        ' Daylight Saving Time 1987 - 2006
        ' North America (Canada, US and Mexico) First Sunday in April through last Sunday in October at 2:00 AM local time
        ' In April 2:00 AM -> 3:00 AM
        ' In October 2:00 AM -> 1:00 AM

        'Daylight Saving Time Schedule USA and Canada starting in 2007
        'As prescribed: The Energy Policy Act of 2005 

        'Rule

        'Start: Second Sunday in March 
        'End: First Sunday in November 
        'Time: 2 am local time 

        'Beginning in 2007, Daylight Saving Time is extended one month and the schedule for Canada and
        '   the states of the United States that adopt daylight saving time will be:

        '2 a.m. on the Second Sunday in March
        'to
        '2 a.m. on the First Sunday of November

        If CheckDateTime.Month = 4 Or CheckDateTime.Month = 3 Then

            If CheckDateTime.Year >= 1987 And CheckDateTime.Year < 2007 And CheckDateTime.Month = 4 Then

                ' First Sunday in April - only applies to USA and Canada

                dtStart = System.DateTime.Parse("04/01/" & CheckDateTime.Year.ToString)
                dtStop = System.DateTime.Parse("04/07/" & CheckDateTime.Year.ToString)

            ElseIf CheckDateTime.Year >= 2007 And CheckDateTime.Month = 3 Then

                ' Second Sunday in March - only applies to USA and Canada

                dtStart = System.DateTime.Parse("03/08/" & CheckDateTime.Year.ToString)
                dtStop = System.DateTime.Parse("03/14/" & CheckDateTime.Year.ToString)

            ElseIf CheckDateTime.Year < 1987 And CheckDateTime.Month = 4 Then

                ' Last Sunday in April
                dtStart = System.DateTime.Parse("04/24/" & CheckDateTime.Year.ToString)
                dtStop = System.DateTime.Parse("04/30/" & CheckDateTime.Year.ToString)

            Else

                ' Something else
                Return String.Empty

            End If

            dtDate = dtStart

            Do While dtDate <= dtStop

                If dtDate.DayOfWeek = DayOfWeek.Sunday Then

                    dtBegin = System.DateTime.Parse(dtDate.ToShortDateString & " 02:00 AM")
                    dtEnd = System.DateTime.Parse(dtDate.ToShortDateString & " 03:00 AM")

                    If DateTime.Compare(CheckDateTime, dtBegin) > 0 And DateTime.Compare(CheckDateTime, dtEnd) < 0 Then
                        'MessageBox.Show(CheckDateTime.ToString, "Invalid date")
                        Return "You have entered an invalid time between" & vbCrLf & "2:00 AM and 3:00 AM during change in DST"
                    Else
                        'MessageBox.Show(CheckDateTime.ToString, "Valid date")
                        Return String.Empty
                    End If

                    Exit Do

                End If

                dtDate = DateAdd(DateInterval.Day, 1, dtDate)

            Loop

        Else

            Return String.Empty

        End If

        Return String.Empty

    End Function

#End Region

#Region " LoadEventTypeLB(ByRef lbEventType As ListBox) "

    Public Sub LoadEventTypeLB(ByRef lbEventType As ListBox)

        Dim ds As New DataSet

        With ds.Tables.Add("EventTypes")
            .Columns.Add("EventType", GetType(String))
            .Columns.Add("Description", GetType(String))
        End With

        Dim row As DataRow = ds.Tables("EventTypes").NewRow
        ' U1-Immediate
        row("EventType") = "U1"
        row("Description") = "U1-Immediate"
        ds.Tables("EventTypes").Rows.Add(row)

        row = ds.Tables("EventTypes").NewRow
        ' U2-Delayed
        row("EventType") = "U2"
        row("Description") = "U2-Delayed"
        ds.Tables("EventTypes").Rows.Add(row)

        row = ds.Tables("EventTypes").NewRow
        ' U3-Postponed
        row("EventType") = "U3"
        row("Description") = "U3-Postponed"
        ds.Tables("EventTypes").Rows.Add(row)

        row = ds.Tables("EventTypes").NewRow
        ' SF-Startup Failure
        row("EventType") = "SF"
        row("Description") = "SF-Startup Failure"
        ds.Tables("EventTypes").Rows.Add(row)

        row = ds.Tables("EventTypes").NewRow
        ' MO-Maintenance
        row("EventType") = "MO"
        row("Description") = "MO-Maintenance"
        ds.Tables("EventTypes").Rows.Add(row)

        row = ds.Tables("EventTypes").NewRow
        ' PO-Planned
        row("EventType") = "PO"
        row("Description") = "PO-Planned"
        ds.Tables("EventTypes").Rows.Add(row)

        row = ds.Tables("EventTypes").NewRow
        ' SE-Extension
        row("EventType") = "SE"
        row("Description") = "SE-MO/PO Extension"
        ds.Tables("EventTypes").Rows.Add(row)

        row = ds.Tables("EventTypes").NewRow
        ' Maintenance Outage Extension
        row("EventType") = "ME"
        row("Description") = "ME-MO Extension"
        ds.Tables("EventTypes").Rows.Add(row)

        row = ds.Tables("EventTypes").NewRow
        ' Planned Outage Extension
        row("EventType") = "PE"
        row("Description") = "PE-PO Extension"
        ds.Tables("EventTypes").Rows.Add(row)


        row = ds.Tables("EventTypes").NewRow
        ' RS-Reserve Shutdown
        row("EventType") = "RS"
        row("Description") = "RS-Reserve Shutdown"
        ds.Tables("EventTypes").Rows.Add(row)

        row = ds.Tables("EventTypes").NewRow
        ' D1-Immediate
        row("EventType") = "D1"
        row("Description") = "D1-Immediate"
        ds.Tables("EventTypes").Rows.Add(row)

        row = ds.Tables("EventTypes").NewRow
        ' D2-Delayed
        row("EventType") = "D2"
        row("Description") = "D2-Delayed"
        ds.Tables("EventTypes").Rows.Add(row)

        row = ds.Tables("EventTypes").NewRow
        ' D3-Postponed
        row("EventType") = "D3"
        row("Description") = "D3-Postponed"
        ds.Tables("EventTypes").Rows.Add(row)

        row = ds.Tables("EventTypes").NewRow
        ' D4-Maintenance
        row("EventType") = "D4"
        row("Description") = "D4-Maintenance"
        ds.Tables("EventTypes").Rows.Add(row)

        row = ds.Tables("EventTypes").NewRow
        ' PD-Planned
        row("EventType") = "PD"
        row("Description") = "PD-Planned"
        ds.Tables("EventTypes").Rows.Add(row)

        row = ds.Tables("EventTypes").NewRow
        ' DE-Extension
        row("EventType") = "DE"
        row("Description") = "DE-D4/PD Extension"
        ds.Tables("EventTypes").Rows.Add(row)

        row = ds.Tables("EventTypes").NewRow
        ' Maintenance Derating Extension
        row("EventType") = "DM"
        row("Description") = "DM-D4 Extension"
        ds.Tables("EventTypes").Rows.Add(row)

        row = ds.Tables("EventTypes").NewRow
        ' Planned Derating Extension
        row("EventType") = "DP"
        row("Description") = "DP-PD Extension"
        ds.Tables("EventTypes").Rows.Add(row)

        row = ds.Tables("EventTypes").NewRow
        ' NC-Noncurtailing
        row("EventType") = "NC"
        row("Description") = "NC-Noncurtailing"
        ds.Tables("EventTypes").Rows.Add(row)

        row = ds.Tables("EventTypes").NewRow
        ' PU-Pumping
        row("EventType") = "PU"
        row("Description") = "PU-Pumping"
        ds.Tables("EventTypes").Rows.Add(row)

        row = ds.Tables("EventTypes").NewRow
        ' CO-Synchronous Condensing
        row("EventType") = "CO"
        row("Description") = "CO-Synch Condensing"
        ds.Tables("EventTypes").Rows.Add(row)

        lbEventType.Items.Clear()
        lbEventType.DataSource = ds.Tables("EventTypes")
        lbEventType.DataMember = "EventTypes"
        lbEventType.DataValueField = "EventType"
        lbEventType.DataTextField = "Description"
        lbEventType.DataBind()

        lbEventType.SelectedIndex = Nothing

    End Sub

#End Region

#Region " LoadEventTypeLB1996(ByRef lbEventType As ListBox) "

    Public Sub LoadEventTypeLB1996(ByRef lbEventType As ListBox)

        Dim ds As New DataSet

        With ds.Tables.Add("EventTypes")
            .Columns.Add("EventType", GetType(String))
            .Columns.Add("Description", GetType(String))
        End With

        Dim row As DataRow = ds.Tables("EventTypes").NewRow
        ' U1-Immediate
        row("EventType") = "U1"
        row("Description") = "U1-Immediate"
        ds.Tables("EventTypes").Rows.Add(row)

        row = ds.Tables("EventTypes").NewRow
        ' U2-Delayed
        row("EventType") = "U2"
        row("Description") = "U2-Delayed"
        ds.Tables("EventTypes").Rows.Add(row)

        row = ds.Tables("EventTypes").NewRow
        ' U3-Postponed
        row("EventType") = "U3"
        row("Description") = "U3-Postponed"
        ds.Tables("EventTypes").Rows.Add(row)

        row = ds.Tables("EventTypes").NewRow
        ' SF-Startup Failure
        row("EventType") = "SF"
        row("Description") = "SF-Startup Failure"
        ds.Tables("EventTypes").Rows.Add(row)

        row = ds.Tables("EventTypes").NewRow
        ' MO-Maintenance
        row("EventType") = "MO"
        row("Description") = "MO-Maintenance"
        ds.Tables("EventTypes").Rows.Add(row)

        row = ds.Tables("EventTypes").NewRow
        ' PO-Planned
        row("EventType") = "PO"
        row("Description") = "PO-Planned"
        ds.Tables("EventTypes").Rows.Add(row)

        row = ds.Tables("EventTypes").NewRow
        ' SE-Extension
        row("EventType") = "SE"
        row("Description") = "SE-Extension"
        ds.Tables("EventTypes").Rows.Add(row)

        row = ds.Tables("EventTypes").NewRow
        ' RS-Reserve Shutdown
        row("EventType") = "RS"
        row("Description") = "RS-Reserve Shutdown"
        ds.Tables("EventTypes").Rows.Add(row)

        row = ds.Tables("EventTypes").NewRow
        ' D4-Maintenance
        row("EventType") = "D4"
        row("Description") = "D4-Maintenance"
        ds.Tables("EventTypes").Rows.Add(row)

        row = ds.Tables("EventTypes").NewRow
        ' PD-Planned
        row("EventType") = "PD"
        row("Description") = "PD-Planned"
        ds.Tables("EventTypes").Rows.Add(row)

        row = ds.Tables("EventTypes").NewRow
        ' DE-Extension
        row("EventType") = "DE"
        row("Description") = "DE-Extension"
        ds.Tables("EventTypes").Rows.Add(row)

        lbEventType.Items.Clear()
        lbEventType.DataSource = ds.Tables("EventTypes")
        lbEventType.DataMember = "EventTypes"
        lbEventType.DataValueField = "EventType"
        lbEventType.DataTextField = "Description"
        lbEventType.DataBind()

        lbEventType.SelectedIndex = Nothing

    End Sub

#End Region

#Region " LoadFailMechCB(ByRef dsFailMech As DataSet, ByRef cbFailureMech As ListBox) "

    Public Sub LoadFailMechCB(ByRef dsFailMech As DataSet, ByRef cbFailureMech As ListBox)

        Dim newRow As DataRow
        newRow = dsFailMech.Tables(0).NewRow
        newRow("FULLDESC") = " "
        newRow("FAILUREMECHCODE") = " "
        dsFailMech.Tables(0).Rows.Add(newRow)
        cbFailureMech.DataSource = dsFailMech.Tables(0)
        ' these must be upper case since Oracle returns an "AS" in upper case regardless
        cbFailureMech.DataTextField = "FULLDESC"
        cbFailureMech.DataValueField = "FAILUREMECHCODE"
        cbFailureMech.DataBind()
        cbFailureMech.SelectedIndex = cbFailureMech.Items.Count - 1

    End Sub

#End Region

#Region " LoadCauseCodeExtCB(ByRef dsCauseCodeExt As DataSet, ByRef cbCauseCodeExt As DropDownList) "

    Public Sub LoadCauseCodeExtCB(ByRef dsCauseCodeExt As DataSet, ByRef cbCauseCodeExt As DropDownList)

        Dim newRow As DataRow
        newRow = dsCauseCodeExt.Tables(0).NewRow()
        newRow("FULLDESC") = "  "
        newRow("CODE") = "  "
        dsCauseCodeExt.Tables(0).Rows.Add(newRow)
        cbCauseCodeExt.DataSource = dsCauseCodeExt.Tables(0)
        ' this must be upper case since Oracle returns an "AS" in upper case regardless
        cbCauseCodeExt.DataTextField = "FULLDESC"
        cbCauseCodeExt.DataValueField = "CODE"
        cbCauseCodeExt.DataBind()
        cbCauseCodeExt.SelectedIndex = cbCauseCodeExt.Items.Count - 1

    End Sub

#End Region

#Region " LoadTripMechLB(ByRef lbTripMech As ListBox) "

    Public Sub LoadTripMechLB(ByRef lbTripMech As ListBox)

        Dim ds As New DataSet

        With ds.Tables.Add("TripMechCodes")
            .Columns.Add("Code", GetType(String))
            .Columns.Add("Description", GetType(String))
        End With

        Dim row As DataRow = ds.Tables("TripMechCodes").NewRow
        row("Code") = "A"
        row("Description") = "Automatic"
        ds.Tables("TripMechCodes").Rows.Add(row)

        row = ds.Tables("TripMechCodes").NewRow
        row("Code") = "M"
        row("Description") = "Manual"
        ds.Tables("TripMechCodes").Rows.Add(row)

        lbTripMech.Items.Clear()
        lbTripMech.DataSource = ds.Tables("TripMechCodes")
        lbTripMech.DataTextField = "Description"
        lbTripMech.DataValueField = "Code"
        lbTripMech.DataMember = "TripMechCodes"
        lbTripMech.DataBind()
        lbTripMech.SelectedIndex = 0

    End Sub

#End Region

#Region " LoadContCodeLB(ByRef lbContCode As ListBox, ByRef intCurrentYear As Integer, ByVal strEventType As String) "

    Public Sub LoadContCodeLB(ByRef lbContCode As ListBox, ByRef intCurrentYear As Integer, ByVal strEventType As String)

        Dim ds As New DataSet

        With ds.Tables.Add("ContCodes")
            .Columns.Add("Code", GetType(Int16))
            .Columns.Add("Description", GetType(String))
        End With

        Dim row As DataRow = ds.Tables("ContCodes").NewRow

        If strEventType.ToUpper = "PO" Or strEventType.ToUpper = "MO" Then
            '1 - Primary cause of event
            row("Code") = 1
            row("Description") = "1 - Primary cause of event"
            ds.Tables("ContCodes").Rows.Add(row)
        Else
            ' filler
            row("Code") = 0
            row("Description") = "  "
            ds.Tables("ContCodes").Rows.Add(row)
        End If

        row = ds.Tables("ContCodes").NewRow
        '2 - Contributed to primary cause of event
        row("Code") = 2
        row("Description") = "2 - Contributed to primary cause of event"
        ds.Tables("ContCodes").Rows.Add(row)

        row = ds.Tables("ContCodes").NewRow
        '3 - Work done during the event
        row("Code") = 3
        row("Description") = "3 - Work done during the event"
        ds.Tables("ContCodes").Rows.Add(row)

        If intCurrentYear < 1985 Then
            ' was deleted in 1985
            row = ds.Tables("ContCodes").NewRow
            '4 - Caused a startup failure
            row("Code") = 4
            row("Description") = "4 - Caused a startup failure"
            ds.Tables("ContCodes").Rows.Add(row)
        End If

        row = ds.Tables("ContCodes").NewRow
        '5 - After startup, delayed unit from reaching load point
        row("Code") = 5
        row("Description") = "5 - After startup, delayed unit from reaching load point"
        ds.Tables("ContCodes").Rows.Add(row)

        If intCurrentYear < 1996 Then

            ' deleted in 1996

            row = ds.Tables("ContCodes").NewRow
            '6 - First Change in Event type
            row("Code") = 6
            row("Description") = "6 - First Change in Event type"
            ds.Tables("ContCodes").Rows.Add(row)

            row = ds.Tables("ContCodes").NewRow
            '7 - Second Change in Event Type
            row("Code") = 7
            row("Description") = "7 - Second Change in Event type"
            ds.Tables("ContCodes").Rows.Add(row)

        End If

        lbContCode.Items.Clear()
        lbContCode.DataSource = ds.Tables("ContCodes")
        lbContCode.DataTextField = "Description"
        lbContCode.DataValueField = "Code"
        lbContCode.DataBind()
        lbContCode.SelectedIndex = 2

    End Sub

#End Region

#Region " ETChange(ByVal orig As String, ByVal dest As String) As Boolean "

    Public Function ETChange(ByVal orig As String, ByVal dest As String) As Boolean

        Dim lAllowed As Boolean

        lAllowed = True

        '   Event Transitions* 

        'Sometimes events occur in succession with no intervening unit synchronization. 

        'These events are considered related, even though they must be reported separately.

        'The matrix below describes the relationships between events and details permissible event type changes, 
        'see Example 9 in Appendix G.   

        '               Figure III-6 -- Allowable Event Type Changes (Page III-15, 10/02)

        '                       TO 
        '       FROM	            U1      U2	    U3	    SF	    MO	    PO	    SE	    RS      DE

        ' U1  Immediate	        Yes	    No	    No	    Yes	    Yes	    Yes	    No	    Yes	
        ' U2  Delayed	            Yes	    No	    No	    Yes	    Yes	    Yes	    No	    Yes	
        ' U3  Postponed	        Yes	    No	    No	    Yes	    Yes	    Yes	    No	    Yes	
        ' SF - Startup Failure	    Yes	    No	    No	    Yes	    Yes	    Yes	    No	    Yes	
        ' MO  Maintenance	        Yes	    No	    No	    Yes	    Yes	    Yes	    Yes	    Yes	
        ' PO  Planned	            Yes	    No	    No	    Yes	    No	    Yes	    Yes	    Yes	
        ' SE  Extension	        Yes	    No	    No	    Yes	    No	    No	    Yes	    Yes	
        ' RS - Reserve Shutdown	    Yes	    No	    No      Yes	    Yes	    Yes	    No	    Yes	

        ' D1  Immediate	            IEEE Standard 762 does not recognize                        No
        ' D2  Delayed		            transition to/of deratings from/to                          No
        ' D3  Postponed		        other event types except as shown.                          No
        ' D4  Maintenance		                                                                    Yes
        ' PD  Planned		                                                                        Yes
        ' DE  Extension		                                                                    Yes

        'YES denotes that a change from one event type to another without intervening synchronization is permissible 
        'and the end date of the first event can be the same as the start date of the successive event.

        'NO indicates that there is no relationship between the event types and individual events separated by some 
        'period of time must be reported.

        'When there is no intervening synchronization between events, the start time of one event is the same as the 
        'end time of the immediately preceding event. The change in event typically does not occur until the full term 
        'of the preceding event is complete. This means that the time it normally takes the unit to reach the 
        'synchronization point must be included before the change in event occurs. 
        'Use the following matrix to determine whether startup time should be included before the event change occurs:

        '* Effective January 1, 1996 the data fields in Event Report (97) Section BEvent Magnitude relating to change 
        'in event type may no longer be used to report event transitions. Each event must be reported using a unique 
        'event number.

        ' *** On-line testing (synchronized) ***
        'If the unit must be on line and in service at some reduced load to perform testing following a Planned Outage (PO), 
        'Maintenance Outage (MO), or Unplanned (Forced) Outage (U1, U2, U3, SF), report the testing as a  
        'Planned Derating (PD), Maintenance Derating (D4), or Unplanned (Forced) Derating (D1), respectively (see Page III-8).

        'The PD, D4, or D1 starts when the testing begins, and ends when testing is completed.

        ' Therefore, the following case statement was added below:

        '   Case "PD", "D4", "D1"
        '      lAllowed = True

        'Report any generation produced while the unit was on line during the testing period on the Performance Report (95) 
        '(see Page IV-5).


        Select Case orig.ToUpper

            Case "U1", "U2", "U3", "SF"

                Select Case dest.ToUpper
                    Case "U1", "SF", "MO", "PO", "RS"
                        lAllowed = True
                    Case "PD", "D4", "D1"
                        lAllowed = True
                    Case "U2", "U3", "SE", "ME", "PE"
                        lAllowed = False
                    Case Else
                        lAllowed = True
                End Select

            Case "MO"

                Select Case dest.ToUpper
                    Case "U1", "SF", "MO", "PO", "SE", "RS", "ME"
                        lAllowed = True
                    Case "PD", "D4", "D1"
                        lAllowed = True
                    Case "U2", "U3", "PE"
                        lAllowed = False
                    Case Else
                        lAllowed = True
                End Select

            Case "PO"

                Select Case dest.ToUpper
                    Case "U1", "SF", "PO", "SE", "RS", "PE"
                        lAllowed = True
                    Case "PD", "D4", "D1"
                        lAllowed = True
                    Case "U2", "U3", "MO", "ME"
                        lAllowed = False
                    Case Else
                        lAllowed = True
                End Select

            Case "SE", "ME", "PE"

                Select Case dest.ToUpper
                    Case "U1", "SF", "SE", "RS", "ME", "PE"
                        lAllowed = True
                    Case "PD", "D4", "D1"
                        lAllowed = True
                    Case "U2", "U3", "MO", "PO"
                        lAllowed = False
                    Case Else
                        lAllowed = True
                End Select

            Case "RS"

                Select Case dest.ToUpper
                    Case "U1", "SF", "MO", "PO", "RS"
                        lAllowed = True
                    Case "U2", "U3", "SE", "ME", "PE"
                        lAllowed = False
                    Case Else
                        lAllowed = True
                End Select

            Case "D1", "D2", "D3"

                Select Case dest.ToUpper
                    Case "DE", "DM", "DP"
                        lAllowed = False
                    Case Else
                        lAllowed = True
                End Select

            Case "D4", "DM"

                Select Case dest.ToUpper
                    Case "DP"
                        lAllowed = False
                    Case Else
                        lAllowed = True
                End Select

            Case "PD", "DP"

                Select Case dest.ToUpper
                    Case "DM"
                        lAllowed = False
                    Case Else
                        lAllowed = True
                End Select

            Case Else

                lAllowed = True

        End Select

        Return lAllowed

    End Function

#End Region

#Region " ISOSnapShot(myUser, blConnect, connect) As String "

    Public Function ISOSnapShot(ByVal myUser As String, ByVal blConnect As BusinessLayer.BusinessLayer.GADSNGBusinessLayer, ByVal connect As String) As String

        Dim dtSetup As ARWindowsUI.SetupDE.SetupDataTable
        Dim drSetup As ARWindowsUI.SetupDE.SetupRow
        Dim intYearLimit As Integer = 1980
        Dim StringToPrint As String = String.Empty
        Dim AppName As String = "GADSNG"
        'Dim reader As New IO.StreamReader(HttpContext.Current.Server.MapPath("~/Secure/GADSNG.xml"))
        Dim ALConnect As New Calcs(connect)

        dtSetup = ALConnect.GetUtilityUnitData()

        Dim cEFORdFleet As String

        'cEFORdFleet = moXML.GetSettingStr("EFORd", "Fleet", "NERC2").ToUpper.Trim
        cEFORdFleet = ALConnect.GetAnalysisSettingStr("EFORd", "Fleet", "NERC2").ToUpper.Trim

        If cEFORdFleet <> "PJM" And cEFORdFleet <> "NERC" And cEFORdFleet <> "NERC2" Then
            cEFORdFleet = "NERC2"
        End If

        ALConnect.cEFORdFleet = cEFORdFleet

        If dtSetup.Rows.Count > 0 Then
            ' find out what group(s) this person is assigned to -- could be more than one group

            Dim intGroups As Integer

            intGroups = blConnect.IsUserAssignedToGroup(myUser)

            If intGroups > 0 Then
                ' assigned to one or more Groups

                Dim GroupIDReader As IDataReader
                Dim UnitsReader As IDataReader
                Dim GroupArray As New ArrayList
                Dim UnitArray As New ArrayList

                'Dim myTest As Integer
                Dim intValue As Integer
                Dim strValue As String

                GroupIDReader = blConnect.GetUserGroupsReader(myUser)
                While GroupIDReader.Read()
                    GroupArray.Add(CInt(GroupIDReader.GetValue(0)))
                End While
                ' always call Close when done reading
                GroupIDReader.Close()

                'dtSetup = blConnect.GetUtilityUnitData()

                For Each drSetup In dtSetup
                    drSetup.UnitSelected = False
                Next

                dtSetup.AcceptChanges()

                Dim PermsReader As IDataReader
                Dim intGroupID As Integer

                For intValue = 0 To (GroupArray.Count - 1)

                    UnitsReader = blConnect.GetUnitsInGroup(CInt(GroupArray.Item(intValue)))

                    While UnitsReader.Read()

                        strValue = UnitsReader.GetString(0)
                        drSetup = dtSetup.FindByUtilityUnitCode(UnitsReader.GetString(0))

                        If drSetup Is Nothing Then
                            StringToPrint += "Invalid or Missing Unit Code " & strValue & "<br><br>"
                        Else
                            drSetup.UnitSelected = True
                        End If

                    End While

                    UnitsReader.Close()

                    PermsReader = blConnect.GetPermissions(CInt(GroupArray.Item(intValue)))

                    While PermsReader.Read

                        intGroupID = CInt(PermsReader.GetValue(0))
                        intYearLimit = CInt(PermsReader.GetValue(1))

                    End While

                    PermsReader.Close()

                Next

                For Each drSetup In dtSetup
                    If drSetup.UnitSelected = False Then
                        drSetup.Delete()
                    End If
                Next

                dtSetup.AcceptChanges()

                If dtSetup.Rows.Count <= 0 Then
                    StringToPrint += "No Units Assigned to " & myUser & "<br><br>"
                Else

                    For Each drSetup In dtSetup
                        If drSetup.CheckStatus.Trim.ToUpper <> "O" Then
                            drSetup.Delete()
                        End If
                    Next

                    dtSetup.AcceptChanges()

                    If dtSetup.Rows.Count = 0 Then
                        StringToPrint += "No Units Assigned to " & myUser & " that have OK status<br><br>"
                    Else

                        ALConnect.Snapshot(dtSetup, ALConnect.EOM(System.DateTime.Now.AddMonths(-1)).ToString)

                        ALConnect.DoWebCalcs(ALConnect, dtSetup, myUser)

                    End If

                End If

            End If

        End If

        'reader.Close()

        Return StringToPrint

    End Function

#End Region

#Region " ISOLoadCheck "

    Public Function ISOLoadCheck(ByVal myUser As String, ByVal blConnect As BusinessLayer.BusinessLayer.GADSNGBusinessLayer, ByRef dtSetup As Setup.SetupDataTable, ByVal GetFromDictionary As HybridDictionary) As String

        ' dtSetup is ByRef so that I can change it and return it to calling function with CheckStatus set correctly for any Errors

        Dim AppName As String = "GADSNG"

        Dim drSetup As Setup.SetupRow
        Dim dsCauseCodes As DataSet
        Dim dtPerformance As Performance.PerformanceDataDataTable = Nothing
        Dim drPerformance As Performance.PerformanceDataRow
        Dim dtEvents01 As Event01.EventData01DataTable = Nothing
        Dim drEvents01 As Event01.EventData01Row
        'Dim dsGridEvents As DataSet
        'Dim dsErrors As DataSet
        'Dim dsFailMech As DataSet
        'Dim dsCauseCodeExt As DataSet
        ' Dim dsSavedVerbDesc As DataSet
        ' Dim dsRS As dsBulkRS
        'Dim intCurrentYear As Integer
        Dim strCurrentUnit As String = ""
        Dim strCurrentPeriod As String = ""
        Dim strCurrentMonthNo As String = ""
        'Dim intSECauseCode As Integer
        'Dim dtSEBeginning As DateTime
        Dim alPeriods As New ArrayList
        Dim lbUnitsText As String = ""
        'Dim l_regex As System.Text.RegularExpressions.Regex
        Dim lForceRefresh As Boolean
        Dim PrintFont As New System.Drawing.Font("Arial", 10)

        Dim intYearLimit As Integer = 1980
        Dim StringToPrint As String = String.Empty
        Dim lMissingUnits As Boolean = False
        Dim lSimpleSetup As Boolean = False

        Dim strMyTemp As String = String.Empty

        lForceRefresh = False

        ' ---------------------------------------
        dtSetup = blConnect.GetUtilityUnitData()

        If dtSetup.Rows.Count > 0 Then

            ' dsGridEvents contains all of the event data for the specific unit and year

            ' dsGridEvents = New DataSet

            ' dsErrors contains all of the Errors data for the specific units this user is allowed to access

            ' dsErrors = New DataSet

            ' find out what group(s) this person is assigned to -- could be more than one group

            Dim intGroups As Integer

            intGroups = blConnect.IsUserAssignedToGroup(myUser)

            If intGroups > 0 Then

                ' assigned to one or more Groups

                Dim GroupIDReader As IDataReader
                Dim UnitsReader As IDataReader
                Dim GroupArray As New ArrayList
                Dim UnitArray As New ArrayList

                'Dim myTest As Integer
                Dim intValue As Integer
                Dim strValue As String

                GroupIDReader = blConnect.GetUserGroupsReader(myUser)
                While GroupIDReader.Read()
                    GroupArray.Add(CInt(GroupIDReader.GetValue(0)))
                End While
                ' always call Close when done reading
                GroupIDReader.Close()

                'dtSetup = blConnect.GetUtilityUnitData()

                For Each drSetup In dtSetup
                    drSetup.UnitSelected = False
                Next

                dtSetup.AcceptChanges()

                Dim PermsReader As IDataReader
                Dim intGroupID As Integer

                For intValue = 0 To (GroupArray.Count - 1)

                    UnitsReader = blConnect.GetUnitsInGroup(CInt(GroupArray.Item(intValue)))

                    While UnitsReader.Read()

                        strValue = UnitsReader.GetString(0)
                        drSetup = dtSetup.FindByUtilityUnitCode(UnitsReader.GetString(0))

                        If drSetup Is Nothing Then
                            StringToPrint += "Invalid or Missing Unit Code " & strValue & "<br><br>"
                        Else
                            drSetup.UnitSelected = True
                        End If

                    End While

                    UnitsReader.Close()

                    PermsReader = blConnect.GetPermissions(CInt(GroupArray.Item(intValue)))

                    While PermsReader.Read

                        intGroupID = CInt(PermsReader.GetValue(0))
                        intYearLimit = CInt(PermsReader.GetValue(1))

                    End While

                    PermsReader.Close()

                Next

                For Each drSetup In dtSetup
                    If drSetup.UnitSelected = False Then
                        drSetup.Delete()
                    End If
                Next

                dtSetup.AcceptChanges()

                If dtSetup.Rows.Count = 0 Then
                    StringToPrint += "No Units Assigned to " & myUser & "<br><br>"
                Else

                    dsCauseCodes = blConnect.GetCauseCodes
                    intValue = dsCauseCodes.Tables(0).Rows.Count
                    dsCauseCodes.Tables(0).DefaultView.Sort = "CauseCode"

                    Dim lHasErrors As Boolean
                    lHasErrors = False
                    'Dim decValue As Decimal
                    'Dim intDecPlaces As Int16
                    'Dim decNAG As Decimal
                    'Dim decGAG As Decimal
                    'Dim decNDC As Decimal
                    'Dim decGDC As Decimal
                    'Dim decGMC As Decimal
                    'Dim decNMC As Decimal
                    'Dim intActStarts As Integer
                    'Dim intAttStarts As Integer

                    Dim intYear As Short = Convert.ToInt16(Now.Year)
                    Dim intLoc As Integer = 0
                    Dim strMonth As String = String.Empty
                    Dim strMonthNo As String = "0"
                    Dim intMonthNo As Short = 0
                    Dim myObj As Object
                    Dim boolHasData As Boolean = True

                    For Each drSetup In dtSetup

                        boolHasData = True

                        StringToPrint = String.Empty

                        blConnect.ClearMyErrors(drSetup.UtilityUnitCode)
                        blConnect.UpdateErrorCheckStatus(drSetup.UtilityUnitCode, "O")
                        drSetup.CheckStatus = "O"

                        myObj = blConnect.GetCurrentMonth(drSetup.UtilityUnitCode, GetFromDictionary)

                        If IsNothing(myObj) Then
                            strMyTemp = "Error in GetCurrentMonth processing " & drSetup.UnitName & "(" & drSetup.UtilityUnitCode & ")" & vbCrLf
                            boolHasData = False
                            myObj = 0
                        End If

                        If IsDBNull(myObj) Then
                            strMyTemp = "Error in GetCurrentMonth processing " & drSetup.UnitName & "(" & drSetup.UtilityUnitCode & ")" & vbCrLf
                            boolHasData = False
                            myObj = 0
                        End If

                        strCurrentPeriod = Convert.ToString(myObj)

                        If strCurrentPeriod = "0" Then
                            strMyTemp = "Error in GetCurrentMonth processing " & drSetup.UnitName & "(" & drSetup.UtilityUnitCode & ")" & vbCrLf
                            boolHasData = False
                        End If

                        If boolHasData = True Then

                            intYear = CShort(strCurrentPeriod.Substring((strCurrentPeriod.Length - 4), 4))
                            intLoc = strCurrentPeriod.IndexOf(",")

                            If intLoc > 0 Then
                                strMonth = strCurrentPeriod.Substring(0, intLoc).ToUpper()
                            Else
                                intLoc = strCurrentPeriod.IndexOf(" ")
                                If intLoc > 0 Then
                                    strMonth = strCurrentPeriod.Substring(0, intLoc).ToUpper()
                                End If
                            End If

                            Select Case strMonth
                                Case "JANUARY"
                                    strMonthNo = "01"
                                Case "FEBRUARY"
                                    strMonthNo = "02"
                                Case "MARCH"
                                    strMonthNo = "03"
                                Case "APRIL"
                                    strMonthNo = "04"
                                Case "MAY"
                                    strMonthNo = "05"
                                Case "JUNE"
                                    strMonthNo = "06"
                                Case "JULY"
                                    strMonthNo = "07"
                                Case "AUGUST"
                                    strMonthNo = "08"
                                Case "SEPTEMBER"
                                    strMonthNo = "09"
                                Case "OCTOBER"
                                    strMonthNo = "10"
                                Case "NOVEMBER"
                                    strMonthNo = "11"
                                Case "DECEMBER"
                                    strMonthNo = "12"
                                Case Else
                                    strMonthNo = "0"
                            End Select

                            intMonthNo = Convert.ToInt16(strMonthNo)

                            If IsNumeric(strCurrentPeriod.Substring(strCurrentPeriod.Trim.Length - 4)) Then
                                intYear = Convert.ToInt16(strCurrentPeriod.Substring(strCurrentPeriod.Trim.Length - 4))
                            Else
                                intYear = Convert.ToInt16(Now.Year)
                            End If

                            'Dim iPriorYr As Int16 = intYear - Convert.ToInt16(1)
                            Dim iCurrentYr As Int16 = intYear
                            Dim iPriorYr As Int16 = intYearLimit

                            For intYear = iPriorYr To iCurrentYr

                                If Not IsNothing(dtPerformance) Then
                                    dtPerformance.Clear()
                                End If

                                If Not IsNothing(dtEvents01) Then
                                    dtEvents01.Clear()
                                End If

                                If intYear < iCurrentYr Then
                                    intMonthNo = 12
                                Else
                                    ' Get Current Year
                                    intMonthNo = Convert.ToInt16(strMonthNo)
                                End If

                                dtPerformance = blConnect.GetPerformanceData(drSetup.UtilityUnitCode, intYear)

                                If intYear = iCurrentYr Then

                                    Dim boolMissingMonth As Boolean = False
                                    Dim strThisPeriod As String = String.Empty

                                    If drSetup.CommercialDate.Year = iCurrentYr Then

                                        For i As Integer = drSetup.CommercialDate.Month To intMonthNo

                                            boolMissingMonth = True

                                            For Each drPerformance In dtPerformance
                                                strThisPeriod = drPerformance.Period
                                                If i = Convert.ToInt32(drPerformance.Period) Then
                                                    boolMissingMonth = False
                                                    Exit For
                                                End If
                                            Next

                                            If boolMissingMonth Then
                                                StringToPrint += drSetup.UnitName & " " & iCurrentYr.ToString & " " & i.ToString("00") & " ERROR: missing performance record"
                                                blConnect.LoadErrors(drSetup.UtilityUnitCode, iCurrentYr, i.ToString("00"), 0, 0, "Missing performance record", "E", drSetup.UnitShortName, GetFromDictionary)
                                                drSetup.CheckStatus = "E"
                                            End If

                                        Next

                                    Else
                                        If intMonthNo <> dtPerformance.Rows.Count Then

                                            For i As Int16 = 1 To intMonthNo

                                                boolMissingMonth = True

                                                For Each drPerformance In dtPerformance
                                                    strThisPeriod = drPerformance.Period
                                                    If i = Convert.ToInt16(drPerformance.Period) Then
                                                        boolMissingMonth = False
                                                        Exit For
                                                    End If
                                                Next

                                                If boolMissingMonth Then
                                                    StringToPrint += drSetup.UnitName & " " & iCurrentYr.ToString & " " & i.ToString("00") & " ERROR: missing performance record"
                                                    blConnect.LoadErrors(drSetup.UtilityUnitCode, iCurrentYr, i.ToString("00"), 0, 0, "Missing performance record", "E", drSetup.UnitShortName, GetFromDictionary)
                                                    drSetup.CheckStatus = "E"
                                                End If

                                            Next

                                        End If

                                    End If

                                End If

                                For Each drPerformance In dtPerformance

                                    If drPerformance.IsNetDepCapNull Then
                                        drPerformance.NetDepCap = 0
                                    End If

                                    If drPerformance.IsNetMaxCapNull Then
                                        drPerformance.NetMaxCap = 0
                                    End If

                                    If drPerformance.NetDepCap <= 0 Then
                                        StringToPrint += drSetup.UnitName & " " & drPerformance.Year.ToString & " " & drPerformance.Period & " ERROR: NDC <= 0"
                                        blConnect.LoadErrors(drSetup.UtilityUnitCode, drPerformance.Year, drPerformance.Period, 0, 0, "NDC <= 0", "E", drSetup.UnitShortName, GetFromDictionary)
                                        drSetup.CheckStatus = "E"
                                    End If

                                    If drPerformance.NetMaxCap <= 0 Then
                                        StringToPrint += drSetup.UnitName & " " & drPerformance.Year.ToString & " " & drPerformance.Period & " ERROR: NMC <= 0"
                                        blConnect.LoadErrors(drSetup.UtilityUnitCode, drPerformance.Year, drPerformance.Period, 0, 0, "NMC <= 0", "E", drSetup.UnitShortName, GetFromDictionary)
                                        drSetup.CheckStatus = "E"
                                    End If

                                    If drPerformance.NetDepCap > drPerformance.NetMaxCap Then
                                        StringToPrint += drSetup.UnitName & " " & drPerformance.Year.ToString & " " & drPerformance.Period & " ERROR: NDC > NMC"
                                        blConnect.LoadErrors(drSetup.UtilityUnitCode, drPerformance.Year, drPerformance.Period, 0, 0, "NDC > NMC", "E", drSetup.UnitShortName, GetFromDictionary)
                                        drSetup.CheckStatus = "E"
                                    End If

                                    If drPerformance.IsInactiveHoursNull Then
                                        drPerformance.InactiveHours = 0
                                    End If

                                    If (drPerformance.PeriodHours = 0 And drPerformance.InactiveHours = 0) Or (drPerformance.PeriodHours <> ((drPerformance.ServiceHours + drPerformance.RSHours + drPerformance.SynchCondHours + drPerformance.PumpingHours) + _
                                        (drPerformance.ForcedOutageHours + drPerformance.PlannedOutageHours + drPerformance.MaintOutageHours + drPerformance.ExtofSchedOutages) + _
                                        drPerformance.InactiveHours)) Then

                                        StringToPrint += drSetup.UnitName & " " & drPerformance.Year.ToString & " " & drPerformance.Period & " ERROR: Hours do not add to Period Hours"
                                        blConnect.LoadErrors(drSetup.UtilityUnitCode, drPerformance.Year, drPerformance.Period, 0, 0, "Hours do not add to Period Hours", "E", drSetup.UnitShortName, GetFromDictionary)
                                        drSetup.CheckStatus = "E"

                                    End If

                                    If drPerformance.IsActualStartsNull Then
                                        drPerformance.ActualStarts = 0
                                    End If

                                    If drPerformance.IsAttemptedStartsNull Then
                                        drPerformance.AttemptedStarts = 0
                                    End If

                                    If drPerformance.ActualStarts > drPerformance.AttemptedStarts Then
                                        StringToPrint += drSetup.UnitName & " " & drPerformance.Year.ToString & " " & drPerformance.Period & " ERROR: Actual Starts > Attempted Starts"
                                        blConnect.LoadErrors(drSetup.UtilityUnitCode, drPerformance.Year, drPerformance.Period, 0, 0, "Actual Starts > Attempted Starts", "E", drSetup.UnitShortName, GetFromDictionary)
                                        drSetup.CheckStatus = "E"
                                    End If

                                    If (drPerformance.ServiceHours + drPerformance.RSHours + drPerformance.PumpingHours + drPerformance.SynchCondHours + _
                                    drPerformance.PlannedOutageHours + drPerformance.ForcedOutageHours + drPerformance.MaintOutageHours + _
                                    drPerformance.ExtofSchedOutages + drPerformance.InactiveHours) <> drPerformance.PeriodHours Then
                                        StringToPrint += drSetup.UnitName & " " & drPerformance.Year.ToString & " " & drPerformance.Period & " ERROR: Performance 02 Hours do not add to Period Hours"
                                        blConnect.LoadErrors(drSetup.UtilityUnitCode, drPerformance.Year, drPerformance.Period, 0, 0, "Performance 02 Hours do not add to Period Hours", "E", drSetup.UnitShortName, GetFromDictionary)
                                        drSetup.CheckStatus = "E"
                                    End If

                                    If Not drPerformance.IsNetMaxCapNull And Not drPerformance.IsNetGenNull And drPerformance.ServiceHours > 0 Then
                                        If drPerformance.NetMaxCap > 0 Then
                                            Dim nof As Decimal = Math.Round(((drPerformance.NetGen * 100) / (drPerformance.NetMaxCap * drPerformance.ServiceHours)), 0)
                                            If nof > 100.0 Then
                                                'StringToPrint += drSetup.UnitName & " " & drPerformance.Year.ToString & " " & drPerformance.Period & " WARNING: NOF is calculated as " & nof.ToString() & "% - should be <= 100%"
                                                blConnect.LoadErrors(drSetup.UtilityUnitCode, drPerformance.Year, drPerformance.Period, 0, 0, "NOF (" & nof.ToString & "%) should be <= 100%", "W", drSetup.UnitShortName, GetFromDictionary)
                                                'drSetup.CheckStatus = "E"
                                            End If
                                        End If
                                    End If

                                Next

                                dtEvents01 = blConnect.GetEvent01Data(drSetup.UtilityUnitCode, intYear)

                                For Each drEvents01 In dtEvents01

                                    intValue = dsCauseCodes.Tables(0).DefaultView.Find(drEvents01.CauseCode)

                                    If intValue < 1 Then
                                        StringToPrint += drSetup.UnitName & " " & drEvents01.Year.ToString & " " & drEvents01.EventNumber.ToString & " WARNING: Invalid Cause Code on 02 Event Record"
                                        blConnect.LoadErrors(drSetup.UtilityUnitCode, drEvents01.Year, String.Empty, drEvents01.EventNumber, 2, "Invalid Cause Code on 02 Event Record", "W", drSetup.UnitShortName, GetFromDictionary)
                                        drSetup.CheckStatus = "W"
                                    End If

                                    If drEvents01.IsEndDateTimeNull = False Then
                                        If drEvents01.EndDateTime <= drEvents01.StartDateTime Then
                                            StringToPrint += drSetup.UnitName & " " & drEvents01.Year.ToString & " " & drEvents01.EventNumber.ToString & " ERROR: End of Event Date/time and Start of Event Date/time are reversed"
                                            blConnect.LoadErrors(drSetup.UtilityUnitCode, drEvents01.Year, String.Empty, drEvents01.EventNumber, 1, "End of Event Date/time and Start of Event Date/time are reversed", "E", drSetup.UnitShortName, GetFromDictionary)
                                            drSetup.CheckStatus = "E"
                                        End If
                                    End If

                                    Select Case drEvents01.EventType
                                        Case "U1", "U2", "U3", "SF", "PO", "MO", "SE", "NC", "RS", "D1", "D2", "D3", "D4", "PD", "DE", "CO", "PU", "ME", "PE", "DM", "DP"
                                        Case "RU", "MB", "IR"
                                            'StringToPrint += drSetup.UnitName & " " & drEvents01.Year.ToString & " " & drEvents01.EventNumber.ToString & " ERROR: Invalid Event Type"
                                            'blConnect.LoadErrors(drSetup.UtilityUnitCode, drEvents01.Year, String.Empty, drEvents01.EventNumber, 1, "Contact ISO for this event type", "E", drSetup.UnitShortName, GetFromDictionary)
                                            'drSetup.CheckStatus = "E"
                                        Case Else
                                            StringToPrint += drSetup.UnitName & " " & drEvents01.Year.ToString & " " & drEvents01.EventNumber.ToString & " ERROR: Invalid Event Type"
                                            blConnect.LoadErrors(drSetup.UtilityUnitCode, drEvents01.Year, String.Empty, drEvents01.EventNumber, 1, "Invalid Event Type", "E", drSetup.UnitShortName, GetFromDictionary)
                                            drSetup.CheckStatus = "E"
                                    End Select

                                Next

                                StringToPrint += ISOEventUpdate(dtEvents01, intMonthNo, intYear, drSetup, blConnect, GetFromDictionary)
                                StringToPrint += ISOErrorCheck(blConnect, drSetup, intYear, dtEvents01, dtPerformance, GetFromDictionary)

                                If StringToPrint.Trim <> String.Empty Then

                                    blConnect.UpdateErrorCheckStatus(drSetup.UtilityUnitCode, "E")

                                Else

                                    If drSetup.CheckStatus.ToUpper = "E" Then
                                        blConnect.UpdateErrorCheckStatus(drSetup.UtilityUnitCode, "E")
                                    Else
                                        blConnect.UpdateErrorCheckStatus(drSetup.UtilityUnitCode, "O")
                                    End If

                                End If

                            Next

                            ' Get Current Year

                            'dtPerformance = blConnect.GetPerformanceData(drSetup.UtilityUnitCode, intYear)

                            'For Each drPerformance In dtPerformance

                            '    If drPerformance.IsNetDepCapNull Then
                            '        drPerformance.NetDepCap = 0
                            '    End If

                            '    If drPerformance.IsNetMaxCapNull Then
                            '        drPerformance.NetMaxCap = 0
                            '    End If

                            '    If drPerformance.NetDepCap <= 0 Then
                            '        StringToPrint += drSetup.UnitName & " " & drPerformance.Year.ToString & " " & drPerformance.Period & " ERROR: NDC <= 0"
                            '        blConnect.LoadErrors(drSetup.UtilityUnitCode, drPerformance.Year, drPerformance.Period, 0, 0, "NDC <= 0", "E", drSetup.UnitShortName, GetFromDictionary)
                            '        drSetup.CheckStatus = "E"
                            '    End If

                            '    If drPerformance.NetMaxCap <= 0 Then
                            '        StringToPrint += drSetup.UnitName & " " & drPerformance.Year.ToString & " " & drPerformance.Period & " ERROR: NMC <= 0"
                            '        blConnect.LoadErrors(drSetup.UtilityUnitCode, drPerformance.Year, drPerformance.Period, 0, 0, "NMC <= 0", "E", drSetup.UnitShortName, GetFromDictionary)
                            '        drSetup.CheckStatus = "E"
                            '    End If

                            '    If drPerformance.NetDepCap > drPerformance.NetMaxCap Then
                            '        StringToPrint += drSetup.UnitName & " " & drPerformance.Year.ToString & " " & drPerformance.Period & " ERROR: NDC > NMC"
                            '        blConnect.LoadErrors(drSetup.UtilityUnitCode, drPerformance.Year, drPerformance.Period, 0, 0, "NDC > NMC", "E", drSetup.UnitShortName, GetFromDictionary)
                            '        drSetup.CheckStatus = "E"
                            '    End If

                            '    If drPerformance.IsInactiveHoursNull Then
                            '        drPerformance.InactiveHours = 0
                            '    End If

                            '    If (drPerformance.PeriodHours = 0 And drPerformance.InactiveHours = 0) Or (drPerformance.PeriodHours <> ((drPerformance.ServiceHours + drPerformance.RSHours + drPerformance.SynchCondHours + drPerformance.PumpingHours) + _
                            '        (drPerformance.ForcedOutageHours + drPerformance.PlannedOutageHours + drPerformance.MaintOutageHours + drPerformance.ExtofSchedOutages) + _
                            '        drPerformance.InactiveHours)) Then

                            '        StringToPrint += drSetup.UnitName & " " & drPerformance.Year.ToString & " " & drPerformance.Period & " ERROR: Hours do not add to Period Hours"
                            '        blConnect.LoadErrors(drSetup.UtilityUnitCode, drPerformance.Year, drPerformance.Period, 0, 0, "Hours do not add to Period Hours", "E", drSetup.UnitShortName, GetFromDictionary)
                            '        drSetup.CheckStatus = "E"

                            '    End If

                            '    If drPerformance.IsActualStartsNull Then
                            '        drPerformance.ActualStarts = 0
                            '    End If

                            '    If drPerformance.IsAttemptedStartsNull Then
                            '        drPerformance.AttemptedStarts = 0
                            '    End If

                            '    If drPerformance.ActualStarts > drPerformance.AttemptedStarts Then
                            '        StringToPrint += drSetup.UnitName & " " & drPerformance.Year.ToString & " " & drPerformance.Period & " ERROR: Actual Starts > Attempted Starts"
                            '        blConnect.LoadErrors(drSetup.UtilityUnitCode, drPerformance.Year, drPerformance.Period, 0, 0, "Actual Starts > Attempted Starts", "E", drSetup.UnitShortName, GetFromDictionary)
                            '        drSetup.CheckStatus = "E"
                            '    End If

                            '    If (drPerformance.ServiceHours + drPerformance.RSHours + drPerformance.PumpingHours + drPerformance.SynchCondHours + _
                            '    drPerformance.PlannedOutageHours + drPerformance.ForcedOutageHours + _
                            '    drPerformance.MaintOutageHours + drPerformance.ExtofSchedOutages) <> drPerformance.PeriodHours Then
                            '        StringToPrint += drSetup.UnitName & " " & drPerformance.Year.ToString & " " & drPerformance.Period & " ERROR: Performance 02 Hours do not add to Period Hours"
                            '        blConnect.LoadErrors(drSetup.UtilityUnitCode, drPerformance.Year, drPerformance.Period, 0, 0, "Performance 02 Hours do not add to Period Hours", "E", drSetup.UnitShortName, GetFromDictionary)
                            '        drSetup.CheckStatus = "E"
                            '    End If

                            'Next

                            'dtEvents01 = blConnect.GetEvent01Data(drSetup.UtilityUnitCode, intYear)

                            'For Each drEvents01 In dtEvents01

                            '    'intValue = dsCauseCodes.Tables(0).DefaultView.Find(drEvents01.CauseCode)

                            '    'If intValue < 1 Then
                            '    '    StringToPrint += drSetup.UnitName & " " & drEvents01.Year.ToString & " " & drEvents01.EventNumber.ToString & " ERROR: Invalid Cause Code on 02 Event Record"
                            '    '    blConnect.LoadErrors(drSetup.UtilityUnitCode, drEvents01.Year, String.Empty, drEvents01.EventNumber, 2, "Invalid Cause Code on 02 Event Record", "E", drSetup.UnitShortName)
                            '    '    drSetup.CheckStatus = "E"
                            '    'End If

                            '    If drEvents01.IsEndDateTimeNull = False Then
                            '        If drEvents01.EndDateTime <= drEvents01.StartDateTime Then
                            '            StringToPrint += drSetup.UnitName & " " & drEvents01.Year.ToString & " " & drEvents01.EventNumber.ToString & " ERROR: End of Event Date/time and Start of Event Date/time are reversed"
                            '            blConnect.LoadErrors(drSetup.UtilityUnitCode, drEvents01.Year, String.Empty, drEvents01.EventNumber, 1, "End of Event Date/time and Start of Event Date/time are reversed", "E", drSetup.UnitShortName, GetFromDictionary)
                            '            drSetup.CheckStatus = "E"
                            '        End If
                            '    End If

                            '    'Select Case drEvents01.EventType
                            '    '    Case "U1", "U2", "U3", "SF", "PO", "MO", "SE", "NC", "RS", "D1", "D2", "D3", "D4", "PD", "DE", "CO", "PU", "ME", "PE", "DM", "DP"
                            '    '    Case "RU", "MB", "IR"
                            '    '        StringToPrint += drSetup.UnitName & " " & drEvents01.Year.ToString & " " & drEvents01.EventNumber.ToString & " ERROR: Invalid Event Type"
                            '    '        blConnect.LoadErrors(drSetup.UtilityUnitCode, drEvents01.Year, String.Empty, drEvents01.EventNumber, 1, "Contact ISO for this event type", "E", drSetup.UnitShortName, GetFromDictionary)
                            '    '        drSetup.CheckStatus = "E"
                            '    '    Case Else
                            '    '        StringToPrint += drSetup.UnitName & " " & drEvents01.Year.ToString & " " & drEvents01.EventNumber.ToString & " ERROR: Invalid Event Type"
                            '    '        blConnect.LoadErrors(drSetup.UtilityUnitCode, drEvents01.Year, String.Empty, drEvents01.EventNumber, 1, "Invalid Event Type", "E", drSetup.UnitShortName, GetFromDictionary)
                            '    '        drSetup.CheckStatus = "E"
                            '    'End Select

                            'Next

                            'StringToPrint += ISOEventUpdate(dtEvents01, intMonthNo, intYear, drSetup, blConnect, GetFromDictionary)
                            'StringToPrint += ISOErrorCheck(blConnect, drSetup, intYear, dtEvents01, dtPerformance, GetFromDictionary)

                            'If StringToPrint.Trim <> String.Empty Then

                            '    blConnect.UpdateErrorCheckStatus(drSetup.UtilityUnitCode, "E")

                            'Else

                            '    If drSetup.CheckStatus.ToUpper = "E" Then
                            '        blConnect.UpdateErrorCheckStatus(drSetup.UtilityUnitCode, "E")
                            '    Else
                            '        blConnect.UpdateErrorCheckStatus(drSetup.UtilityUnitCode, "O")
                            '    End If

                            'End If


                            ' Get one year back based on request from Ron C.

                            'intYear = intYear - Convert.ToInt16(1)
                            'intMonthNo = 12

                            'dtPerformance.Clear()
                            'dtPerformance = blConnect.GetPerformanceData(drSetup.UtilityUnitCode, intYear)

                            'For Each drPerformance In dtPerformance

                            '    If drPerformance.IsNetDepCapNull Then
                            '        drPerformance.NetDepCap = 0
                            '    End If

                            '    If drPerformance.IsNetMaxCapNull Then
                            '        drPerformance.NetMaxCap = 0
                            '    End If

                            '    If drPerformance.NetDepCap <= 0 Then
                            '        StringToPrint += drSetup.UnitName & " " & drPerformance.Year.ToString & " " & drPerformance.Period & " ERROR: NDC <= 0"
                            '        blConnect.LoadErrors(drSetup.UtilityUnitCode, drPerformance.Year, drPerformance.Period, 0, 0, "NDC <= 0", "E", drSetup.UnitShortName, GetFromDictionary)
                            '        drSetup.CheckStatus = "E"
                            '    End If

                            '    If drPerformance.NetMaxCap <= 0 Then
                            '        StringToPrint += drSetup.UnitName & " " & drPerformance.Year.ToString & " " & drPerformance.Period & " ERROR: NMC <= 0"
                            '        blConnect.LoadErrors(drSetup.UtilityUnitCode, drPerformance.Year, drPerformance.Period, 0, 0, "NMC <= 0", "E", drSetup.UnitShortName, GetFromDictionary)
                            '        drSetup.CheckStatus = "E"
                            '    End If

                            '    If drPerformance.NetDepCap > drPerformance.NetMaxCap Then
                            '        StringToPrint += drSetup.UnitName & " " & drPerformance.Year.ToString & " " & drPerformance.Period & " ERROR: NDC > NMC"
                            '        blConnect.LoadErrors(drSetup.UtilityUnitCode, drPerformance.Year, drPerformance.Period, 0, 0, "NDC > NMC", "E", drSetup.UnitShortName, GetFromDictionary)
                            '        drSetup.CheckStatus = "E"
                            '    End If

                            '    If drPerformance.IsInactiveHoursNull Then
                            '        drPerformance.InactiveHours = 0
                            '    End If

                            '    If (drPerformance.PeriodHours = 0 And drPerformance.InactiveHours = 0) Or (drPerformance.PeriodHours <> ((drPerformance.ServiceHours + drPerformance.RSHours + drPerformance.SynchCondHours + drPerformance.PumpingHours) + _
                            '        (drPerformance.ForcedOutageHours + drPerformance.PlannedOutageHours + drPerformance.MaintOutageHours + drPerformance.ExtofSchedOutages) + _
                            '        drPerformance.InactiveHours)) Then

                            '        StringToPrint += drSetup.UnitName & " " & drPerformance.Year.ToString & " " & drPerformance.Period & " ERROR: Hours do not add to Period Hours"
                            '        blConnect.LoadErrors(drSetup.UtilityUnitCode, drPerformance.Year, drPerformance.Period, 0, 0, "Hours do not add to Period Hours", "E", drSetup.UnitShortName, GetFromDictionary)
                            '        drSetup.CheckStatus = "E"

                            '    End If

                            '    If drPerformance.IsActualStartsNull Then
                            '        drPerformance.ActualStarts = 0
                            '    End If

                            '    If drPerformance.IsAttemptedStartsNull Then
                            '        drPerformance.AttemptedStarts = 0
                            '    End If

                            '    If drPerformance.ActualStarts > drPerformance.AttemptedStarts Then
                            '        StringToPrint += drSetup.UnitName & " " & drPerformance.Year.ToString & " " & drPerformance.Period & " ERROR: Actual Starts > Attempted Starts"
                            '        blConnect.LoadErrors(drSetup.UtilityUnitCode, drPerformance.Year, drPerformance.Period, 0, 0, "Actual Starts > Attempted Starts", "E", drSetup.UnitShortName, GetFromDictionary)
                            '        drSetup.CheckStatus = "E"
                            '    End If

                            '    If (drPerformance.ServiceHours + drPerformance.RSHours + drPerformance.PumpingHours + drPerformance.SynchCondHours + _
                            '    drPerformance.PlannedOutageHours + drPerformance.ForcedOutageHours + _
                            '    drPerformance.MaintOutageHours + drPerformance.ExtofSchedOutages) <> drPerformance.PeriodHours Then
                            '        StringToPrint += drSetup.UnitName & " " & drPerformance.Year.ToString & " " & drPerformance.Period & " ERROR: Performance 02 Hours do not add to Period Hours"
                            '        blConnect.LoadErrors(drSetup.UtilityUnitCode, drPerformance.Year, drPerformance.Period, 0, 0, "Performance 02 Hours do not add to Period Hours", "E", drSetup.UnitShortName, GetFromDictionary)
                            '        drSetup.CheckStatus = "E"
                            '    End If

                            'Next

                            'dtEvents01.Clear()

                            'dtEvents01 = blConnect.GetEvent01Data(drSetup.UtilityUnitCode, intYear)

                            'For Each drEvents01 In dtEvents01

                            '    'intValue = dsCauseCodes.Tables(0).DefaultView.Find(drEvents01.CauseCode)

                            '    'If intValue < 1 Then
                            '    '    StringToPrint += drSetup.UnitName & " " & drEvents01.Year.ToString & " " & drEvents01.EventNumber.ToString & " ERROR: Invalid Cause Code on 02 Event Record"
                            '    '    blConnect.LoadErrors(drSetup.UtilityUnitCode, drEvents01.Year, String.Empty, drEvents01.EventNumber, 2, "Invalid Cause Code on 02 Event Record", "E", drSetup.UnitShortName)
                            '    '    drSetup.CheckStatus = "E"
                            '    'End If

                            '    If drEvents01.IsEndDateTimeNull = False Then
                            '        If drEvents01.EndDateTime <= drEvents01.StartDateTime Then
                            '            StringToPrint += drSetup.UnitName & " " & drEvents01.Year.ToString & " " & drEvents01.EventNumber.ToString & " ERROR: End of Event Date/time and Start of Event Date/time are reversed"
                            '            blConnect.LoadErrors(drSetup.UtilityUnitCode, drEvents01.Year, String.Empty, drEvents01.EventNumber, 1, "End of Event Date/time and Start of Event Date/time are reversed", "E", drSetup.UnitShortName, GetFromDictionary)
                            '            drSetup.CheckStatus = "E"
                            '        End If
                            '    End If

                            '    'Select Case drEvents01.EventType
                            '    '    Case "U1", "U2", "U3", "SF", "PO", "MO", "SE", "NC", "RS", "D1", "D2", "D3", "D4", "PD", "DE", "CO", "PU", "ME", "PE", "DM", "DP"
                            '    '    Case "RU", "MB", "IR"
                            '    '        StringToPrint += drSetup.UnitName & " " & drEvents01.Year.ToString & " " & drEvents01.EventNumber.ToString & " ERROR: Invalid Event Type"
                            '    '        blConnect.LoadErrors(drSetup.UtilityUnitCode, drEvents01.Year, String.Empty, drEvents01.EventNumber, 1, "Contact ISO for this event type", "E", drSetup.UnitShortName, GetFromDictionary)
                            '    '        drSetup.CheckStatus = "E"
                            '    '    Case Else
                            '    '        StringToPrint += drSetup.UnitName & " " & drEvents01.Year.ToString & " " & drEvents01.EventNumber.ToString & " ERROR: Invalid Event Type"
                            '    '        blConnect.LoadErrors(drSetup.UtilityUnitCode, drEvents01.Year, String.Empty, drEvents01.EventNumber, 1, "Invalid Event Type", "E", drSetup.UnitShortName, GetFromDictionary)
                            '    '        drSetup.CheckStatus = "E"
                            '    'End Select

                            'Next

                            'StringToPrint += ISOEventUpdate(dtEvents01, intMonthNo, intYear, drSetup, blConnect, GetFromDictionary)
                            'StringToPrint += ISOErrorCheck(blConnect, drSetup, intYear, dtEvents01, dtPerformance, GetFromDictionary)

                            'If StringToPrint.Trim <> String.Empty Then

                            '    blConnect.UpdateErrorCheckStatus(drSetup.UtilityUnitCode, "E")

                            'Else

                            '    If drSetup.CheckStatus.ToUpper = "E" Then
                            '        blConnect.UpdateErrorCheckStatus(drSetup.UtilityUnitCode, "E")
                            '    Else
                            '        blConnect.UpdateErrorCheckStatus(drSetup.UtilityUnitCode, "O")
                            '    End If

                            'End If

                        End If

                    Next

                End If

            Else
                StringToPrint += "No Groups/Units Assigned to " & myUser & "<br><br>"
            End If

        Else

            If dtSetup.Rows.Count = 0 Then

                lMissingUnits = True

                StringToPrint += "No generating units defined" & "<br>" & "<br>" & _
                  "Contact the Administrator and ask the Administrator to run the Windows application <b>Unit Setup Console</b><br><br>"

            Else
                ' Not valid user, invalid keys.xml settings, etc
                ' Stop user from doing anything else

                StringToPrint += "Login problems for User: " & myUser & "<br><br>"
            End If

        End If


        For Each drSetup In dtSetup

            If blConnect.GetMyCheckStatus(drSetup.UtilityUnitCode) = "E" Or drSetup.CheckStatus = "E" Then

                strMyTemp = "Error in Data Loaded"
                Exit For

            End If

        Next

        StringToPrint += strMyTemp

        Return StringToPrint.Trim

    End Function

#End Region

#Region " ISOEventUpdate "

    Public Function ISOEventUpdate(ByVal dtEvents01 As Event01.EventData01DataTable, ByVal intMonthNo As Short, ByVal intYear As Short, ByVal drsetup As Setup.SetupRow, ByVal blConnect As BusinessLayer.BusinessLayer.GADSNGBusinessLayer, ByRef GetFromDictionary As HybridDictionary) As String

        Dim strCulture As String = "en-US"
        Dim dtNow As DateTime = System.DateTime.Now

        If dtNow < Convert.ToDateTime("01/01/1980") Then
            dtNow = System.DateTime.Now
        End If

        'Dim strCurrentUnit As String = drsetup.UtilityUnitCode
        'blConnect.ClearMyErrors(strCurrentUnit)

        'Dim l_regex As System.Text.RegularExpressions.Regex
        Dim dsCauseCodes As DataSet = blConnect.GetCauseCodes
        Dim lHasErrors As Boolean
        Dim lNewRecord As Boolean

        lNewRecord = False
        lHasErrors = False

        'Dim decValue As Decimal
        'Dim intValue As Integer
        Dim intCounter As Integer

        'Dim decGAC As Decimal
        Dim decNAC As Decimal

        Dim longDiff As Long

        'Dim drEvent1 As DataRow
        'Dim drEvent2 As DataRow

        Dim drvEvent1 As DataRowView

        Dim dtEndOfPrior As DateTime
        Dim dtStartOfNext As DateTime
        Dim intPriorNumber As Integer
        'Dim intPriorCauseCode As Integer
        Dim strPriorEventType As String

        Dim dtEndOfPriorPD As DateTime
        Dim dtStartOfNextPD As DateTime
        Dim intPriorNumberPD As Integer
        'Dim intPriorCauseCodePD As Integer
        Dim strPriorEventTypePD As String

        Dim dtEndOfPriorD4 As DateTime
        Dim dtStartOfNextD4 As DateTime
        Dim intPriorNumberD4 As Integer
        'Dim intPriorCauseCodeD4 As Integer
        Dim strPriorEventTypeD4 As String

        dtEndOfPrior = Nothing
        dtStartOfNext = Nothing
        intPriorNumber = -1
        'intPriorCauseCode = -1
        strPriorEventType = Nothing

        dtEndOfPriorPD = Nothing
        dtStartOfNextPD = Nothing
        intPriorNumberPD = -1
        'intPriorCauseCodePD = -1
        strPriorEventTypePD = Nothing

        dtEndOfPriorD4 = Nothing
        dtStartOfNextD4 = Nothing
        intPriorNumberD4 = -1
        'intPriorCauseCodeD4 = -1
        strPriorEventTypeD4 = Nothing

        Dim dtEndOfPriorMOPO As DateTime
        Dim dtStartOfNextSE As DateTime
        Dim intPriorMOPONumber As Integer
        'Dim intPriorMOPOCauseCode As Integer
        Dim strPriorMOPOEventType As String

        dtEndOfPriorMOPO = Nothing
        dtStartOfNextSE = Nothing
        intPriorMOPONumber = -1
        'intPriorMOPOCauseCode = -1
        strPriorMOPOEventType = Nothing

        Dim strRowErrorMsg As String = ""
        Dim strErrors As String = ""

        strErrors = String.Empty

        Dim vue As DataView
        Dim dtTemp As DateTime
        vue = dtEvents01.DefaultView

        vue.Sort = "StartDateTime, EndDateTime"

        Dim PDEndDT As New SortedList(Of DateTime, Integer)
        Dim D4EndDT As New SortedList(Of DateTime, Integer)
        Dim dtJunk As DateTime = System.DateTime.Now
        Dim intJunk As Integer = 0

        vue.RowFilter = "EventType IN ('D4','PD') AND RevisionCard01 <> 'X'"

        For intCounter = 0 To vue.Count - 1
            drvEvent1 = vue(intCounter)
            If Convert.IsDBNull(drvEvent1.Item("EndDateTime")) = False Then
                dtJunk = Convert.ToDateTime(drvEvent1.Item("EndDateTime"))
                intJunk = Convert.ToInt32(drvEvent1.Item("EventNumber"))
                Select Case drvEvent1.Item("EventType").ToString
                    Case "D4"
                        If Not D4EndDT.ContainsKey(dtJunk) Then
                            D4EndDT.Add(dtJunk, intJunk)
                        End If
                    Case "PD"
                        If Not PDEndDT.ContainsKey(dtJunk) Then
                            PDEndDT.Add(dtJunk, intJunk)
                        End If
                End Select
            End If
        Next

        '=============================================================================================================================================================
        ' Validate the Deratings

        Dim boolFoundMatch As Boolean = False

        vue.RowFilter = "EventType IN ('D1','D2','D3','D4','PD','DE','DM','DP') AND RevisionCard01 <> 'X'"

        For intCounter = 0 To vue.Count - 1

            drvEvent1 = vue(intCounter)

            boolFoundMatch = False

            If (drvEvent1.Item("EventType").ToString = "DE" Or drvEvent1.Item("EventType").ToString = "DM") And _
                   Convert.IsDBNull(drvEvent1.Item("StartDateTime")) = False Then

                If D4EndDT.ContainsKey(Convert.ToDateTime(drvEvent1.Item("StartDateTime"))) Then
                    boolFoundMatch = True
                Else
                    For Each de As KeyValuePair(Of DateTime, Integer) In D4EndDT
                        longDiff = DateDiff(DateInterval.Minute, de.Key, Convert.ToDateTime(drvEvent1.Item("StartDateTime")))
                        If de.Key <= Convert.ToDateTime(drvEvent1.Item("StartDateTime")) Then
                            dtEndOfPriorD4 = de.Key
                            intPriorNumberD4 = de.Value
                            strPriorEventTypeD4 = "D4"
                        Else
                            Exit For
                        End If
                        If longDiff = 0 Then
                            boolFoundMatch = True
                            Exit For
                        End If
                    Next
                End If
            End If

            If (drvEvent1.Item("EventType").ToString = "DE" Or drvEvent1.Item("EventType").ToString = "DP") And _
                   Convert.IsDBNull(drvEvent1.Item("StartDateTime")) = False And boolFoundMatch = False Then
                If PDEndDT.ContainsKey(Convert.ToDateTime(drvEvent1.Item("StartDateTime"))) Then
                    boolFoundMatch = True
                Else
                    For Each de As KeyValuePair(Of DateTime, Integer) In PDEndDT
                        longDiff = DateDiff(DateInterval.Minute, de.Key, Convert.ToDateTime(drvEvent1.Item("StartDateTime")))
                        If de.Key <= Convert.ToDateTime(drvEvent1.Item("StartDateTime")) Then
                            dtEndOfPriorPD = de.Key
                            intPriorNumberPD = de.Value
                            strPriorEventTypePD = "PD"
                        Else
                            Exit For
                        End If
                        If longDiff = 0 Then
                            boolFoundMatch = True
                            Exit For
                        End If
                    Next
                End If
            End If

            If boolFoundMatch = False Then

                ' D4 ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

                If drvEvent1.Item("EventType").ToString = "D4" And Convert.IsDBNull(drvEvent1.Item("EndDateTime")) = False Then

                    dtEndOfPriorD4 = Convert.ToDateTime(drvEvent1.Item("EndDateTime"))
                    intPriorNumberD4 = Convert.ToInt32(drvEvent1.Item("EventNumber"))
                    strPriorEventTypeD4 = drvEvent1.Item("EventType").ToString
                    'intPriorCauseCodeD4 = Convert.ToInt32(drvEvent1.Item("CauseCode"))

                End If

                If (drvEvent1.Item("EventType").ToString = "DE" Or drvEvent1.Item("EventType").ToString = "DM") And _
                       Convert.IsDBNull(drvEvent1.Item("StartDateTime")) = False Then

                    dtStartOfNextD4 = Convert.ToDateTime(drvEvent1.Item("StartDateTime"))

                    dtTemp = Convert.ToDateTime("01/01/" & drvEvent1.Item("Year").ToString & " 00:00")

                    If dtStartOfNextD4 > dtTemp Then

                        ' This deals with the possibility that the DE might be the first reported event for the year or 
                        ' carries over from last year

                        If DateTime.Compare(dtEndOfPriorD4, dtStartOfNextD4) <> 0 Then

                            longDiff = DateDiff(DateInterval.Minute, dtEndOfPriorD4, dtStartOfNextD4)

                            If longDiff = 1 Then

                                strRowErrorMsg += "- 1 minute gap between this event and end of " & strPriorEventTypeD4 & " (" & intPriorNumberD4.ToString & ")" & vbCrLf
                            Else

                                ' these must be the same D4 -> DE or PD -> DE or DE -> DE
                                lHasErrors = True
                                If strPriorEventTypeD4 = String.Empty Or intPriorNumberD4 = -1 Then
                                    strRowErrorMsg = "Start of derating extension must exactly match end of missing D4 or DE/DM" & vbCrLf
                                Else
                                    strRowErrorMsg = "Start of derating extension must exactly match end of " & strPriorEventTypeD4 & " (" & intPriorNumberD4.ToString & ")" & vbCrLf
                                End If
                                blConnect.LoadErrors(drsetup.UtilityUnitCode, Convert.ToInt16(drvEvent1.Item("Year")), Nothing, Convert.ToInt32(drvEvent1.Item("EventNumber")), 1, strRowErrorMsg, "E", drsetup.UnitShortName, GetFromDictionary)
                            End If

                        End If

                    End If

                    If Convert.IsDBNull(drvEvent1.Item("EndDateTime")) = False Then

                        ' Allow a DE to DE transition

                        dtEndOfPriorD4 = Convert.ToDateTime(drvEvent1.Item("EndDateTime"))
                        intPriorNumberD4 = Convert.ToInt32(drvEvent1.Item("EventNumber"))
                        strPriorEventTypeD4 = drvEvent1.Item("EventType").ToString
                        'intPriorCauseCodeD4 = Convert.ToInt32(drvEvent1.Item("CauseCode"))

                    End If

                Else
                    dtStartOfNextD4 = Nothing
                End If

                ' PD ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

                If drvEvent1.Item("EventType").ToString = "PD" And Convert.IsDBNull(drvEvent1.Item("EndDateTime")) = False Then

                    dtEndOfPriorPD = Convert.ToDateTime(drvEvent1.Item("EndDateTime"))
                    intPriorNumberPD = Convert.ToInt32(drvEvent1.Item("EventNumber"))
                    strPriorEventTypePD = drvEvent1.Item("EventType").ToString

                End If

                If (drvEvent1.Item("EventType").ToString = "DE" Or drvEvent1.Item("EventType").ToString = "DP") And _
                    Convert.IsDBNull(drvEvent1.Item("StartDateTime")) = False Then

                    dtStartOfNextPD = Convert.ToDateTime(drvEvent1.Item("StartDateTime"))

                    dtTemp = Convert.ToDateTime("01/01/" & drvEvent1.Item("Year").ToString & " 00:00")

                    If dtStartOfNextPD > dtTemp Then

                        ' This deals with the possibility that the DE/DM/DP might be the first reported event for the year or 
                        ' carries over from last year

                        If DateTime.Compare(dtEndOfPriorPD, dtStartOfNextPD) <> 0 Then

                            longDiff = DateDiff(DateInterval.Minute, dtEndOfPriorPD, dtStartOfNextPD)

                            If longDiff = 1 Then

                                strRowErrorMsg = "- 1 minute gap between this event and end of " & strPriorEventTypePD & " (" & intPriorNumberPD.ToString & ")" & vbCrLf
                            Else

                                ' these must be the same D4 -> DE or PD -> DE or DE -> DE
                                lHasErrors = True
                                If strPriorEventTypePD = String.Empty Or intPriorNumberPD = -1 Then
                                    strRowErrorMsg = "Start of derating extension must exactly match end of missing PD or DE/DP" & vbCrLf
                                Else
                                    strRowErrorMsg = "Start of derating extension must exactly match end of " & strPriorEventTypePD & " (" & intPriorNumberPD.ToString & ")" & vbCrLf
                                End If
                                blConnect.LoadErrors(drsetup.UtilityUnitCode, Convert.ToInt16(drvEvent1.Item("Year")), Nothing, Convert.ToInt32(drvEvent1.Item("EventNumber")), 1, strRowErrorMsg, "E", drsetup.UnitShortName, GetFromDictionary)
                            End If

                        End If

                    End If

                    If Convert.IsDBNull(drvEvent1.Item("EndDateTime")) = False Then

                        ' Allow a DE to DE transition

                        dtEndOfPriorPD = Convert.ToDateTime(drvEvent1.Item("EndDateTime"))
                        intPriorNumberPD = Convert.ToInt32(drvEvent1.Item("EventNumber"))
                        strPriorEventTypePD = drvEvent1.Item("EventType").ToString

                    End If

                Else
                    dtStartOfNextPD = Nothing
                End If

            End If

            ' ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

            If Convert.IsDBNull(drvEvent1.Item("NetAvailCapacity")) = False Then

                ' ======================================
                ' NAC cannot be negative

                Try

                    If Text.RegularExpressions.Regex.IsMatch(drvEvent1.Item("NetAvailCapacity").ToString, "^\d+(\.\d+)?$") Then

                        If IsNumeric(drvEvent1.Item("NetAvailCapacity").ToString) Then

                            decNAC = Convert.ToDecimal(drvEvent1.Item("NetAvailCapacity").ToString)

                            If decNAC < 0 Then
                                lHasErrors = True
                                strRowErrorMsg = "NAC cannot be negative"
                                blConnect.LoadErrors(drsetup.UtilityUnitCode, Convert.ToInt16(drvEvent1.Item("Year")), Nothing, Convert.ToInt32(drvEvent1.Item("EventNumber")), 1, strRowErrorMsg, "E", drsetup.UnitShortName, GetFromDictionary)
                            End If

                        End If

                    End If

                Catch
                    lHasErrors = True
                    strRowErrorMsg = "NAC is not an valid value"
                    blConnect.LoadErrors(drsetup.UtilityUnitCode, Convert.ToInt16(drvEvent1.Item("Year")), Nothing, Convert.ToInt32(drvEvent1.Item("EventNumber")), 1, strRowErrorMsg, "E", drsetup.UnitShortName, GetFromDictionary)
                End Try

            End If

            ' =================================================
            ' NAC must be greater than 0

            ' Accepts an unsigned integer number - also matches empty strings
            '   ^\d*$
            Try

                If Convert.IsDBNull(drvEvent1.Item("NetAvailCapacity")) = False Then

                    If Text.RegularExpressions.Regex.IsMatch(drvEvent1.Item("NetAvailCapacity").ToString, "^\d+(\.\d+)?$") Then

                        If IsNumeric(drvEvent1.Item("NetAvailCapacity").ToString) Then
                            decNAC = Convert.ToDecimal(drvEvent1.Item("NetAvailCapacity").ToString)
                        Else
                            decNAC = 0
                        End If
                    Else
                        decNAC = 0
                    End If
                Else
                    decNAC = 0
                End If

                If decNAC = 0 Then
                    lHasErrors = True
                    strRowErrorMsg = "NAC must be greater than 0"
                    blConnect.LoadErrors(drsetup.UtilityUnitCode, Convert.ToInt16(drvEvent1.Item("Year")), Nothing, Convert.ToInt32(drvEvent1.Item("EventNumber")), 1, strRowErrorMsg, "E", drsetup.UnitShortName, GetFromDictionary)
                End If

            Catch
                lHasErrors = True
                strRowErrorMsg = "NAC not an valid value"
                blConnect.LoadErrors(drsetup.UtilityUnitCode, Convert.ToInt16(drvEvent1.Item("Year")), Nothing, Convert.ToInt32(drvEvent1.Item("EventNumber")), 1, strRowErrorMsg, "E", drsetup.UnitShortName, GetFromDictionary)
            End Try

            ' ======================================
            ' Checking the Event Date/time values

            If Convert.IsDBNull(drvEvent1.Item("StartDateTime")) = False And Convert.IsDBNull(drvEvent1.Item("EndDateTime")) = False Then

                If DateTime.Compare(Convert.ToDateTime(drvEvent1.Item("EndDateTime")), Convert.ToDateTime(drvEvent1.Item("StartDateTime"))) <= 0 Then
                    lHasErrors = True
                    strRowErrorMsg = "Event End date/time must be after Start date/time"
                    blConnect.LoadErrors(drsetup.UtilityUnitCode, Convert.ToInt16(drvEvent1.Item("Year")), Nothing, Convert.ToInt32(drvEvent1.Item("EventNumber")), 1, strRowErrorMsg, "E", drsetup.UnitShortName, GetFromDictionary)
                End If

                ' do not convert to UtcNow
                If DateTime.Compare(Convert.ToDateTime(dtNow), Convert.ToDateTime(drvEvent1.Item("EndDateTime"))) < 0 Then
                    lHasErrors = True
                    strRowErrorMsg = "End of Event date/time cannot be a future date/time"
                    blConnect.LoadErrors(drsetup.UtilityUnitCode, Convert.ToInt16(drvEvent1.Item("Year")), Nothing, Convert.ToInt32(drvEvent1.Item("EventNumber")), 1, strRowErrorMsg, "E", drsetup.UnitShortName, GetFromDictionary)
                End If

            ElseIf Convert.IsDBNull(drvEvent1.Item("StartDateTime")) = False And Convert.IsDBNull(drvEvent1.Item("EndDateTime")) = True Then

                ' do not convert to UtcNow
                If Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Year < dtNow.Year Then
                    lHasErrors = True
                    strRowErrorMsg = "End of Event must be filled in"
                    blConnect.LoadErrors(drsetup.UtilityUnitCode, Convert.ToInt16(drvEvent1.Item("Year")), Nothing, Convert.ToInt32(drvEvent1.Item("EventNumber")), 1, strRowErrorMsg, "E", drsetup.UnitShortName, GetFromDictionary)
                Else
                    ' Open-ended derating
                    If intMonthNo = 12 Then
                        lHasErrors = True
                        strRowErrorMsg = "End of Event must be filled in at end of year"
                        blConnect.LoadErrors(drsetup.UtilityUnitCode, Convert.ToInt16(drvEvent1.Item("Year")), Nothing, Convert.ToInt32(drvEvent1.Item("EventNumber")), 1, strRowErrorMsg, "E", drsetup.UnitShortName, GetFromDictionary)
                    Else
                        ' Open-ended derating
                        strRowErrorMsg = "Warning:  End of Event is still open"
                        blConnect.LoadErrors(drsetup.UtilityUnitCode, Convert.ToInt16(drvEvent1.Item("Year")), Nothing, Convert.ToInt32(drvEvent1.Item("EventNumber")), 1, strRowErrorMsg, "W", drsetup.UnitShortName, GetFromDictionary)
                    End If
                End If

            End If

        Next


        '=============================================================================================================================================================
        ' Validate the Full Outage and RS events

        vue.RowFilter = "EventType IN ('U1','U2','U3','MO','PO','SE','SF','RS','ME','PE','CO','PU','MB','IR','RU') AND RevisionCard01 <> 'X'"

        If vue.Count > 0 Then

            drvEvent1 = vue(0)

            ' This handles all outage types; not MO, PO and SE only

            If Convert.IsDBNull(drvEvent1.Item("EndDateTime")) Then
                ' The first full outage has an open ended End date/time
                dtEndOfPrior = dtNow      ' do not convert to UtcNow
            Else
                dtEndOfPrior = Convert.ToDateTime("01/01/1982")
            End If

            intPriorNumber = Convert.ToInt32(drvEvent1.Item("EventNumber"))
            strPriorEventType = drvEvent1.Item("EventType").ToString
            'intPriorCauseCode = Convert.ToInt32(drvEvent1.Item("CauseCode"))

            For intCounter = 0 To vue.Count - 1

                drvEvent1 = vue(intCounter)

                Try

                    If Convert.IsDBNull(drvEvent1.Item("NetAvailCapacity")) = False Then

                        If Text.RegularExpressions.Regex.IsMatch(drvEvent1.Item("NetAvailCapacity").ToString, "^\d+(\.\d+)?$") Then

                            If IsNumeric(drvEvent1.Item("NetAvailCapacity").ToString) Then
                                decNAC = Convert.ToDecimal(drvEvent1.Item("NetAvailCapacity").ToString)
                                If decNAC <> 0 Then
                                    lHasErrors = True
                                    strRowErrorMsg = "Net Available Capacity must be 0"
                                    blConnect.LoadErrors(drsetup.UtilityUnitCode, Convert.ToInt16(drvEvent1.Item("Year")), Nothing, Convert.ToInt32(drvEvent1.Item("EventNumber")), 1, strRowErrorMsg, "E", drsetup.UnitShortName, GetFromDictionary)
                                End If
                            End If
                        Else
                            lHasErrors = True
                            strRowErrorMsg = "Invalid Net Available Capacity"
                            blConnect.LoadErrors(drsetup.UtilityUnitCode, Convert.ToInt16(drvEvent1.Item("Year")), Nothing, Convert.ToInt32(drvEvent1.Item("EventNumber")), 1, strRowErrorMsg, "E", drsetup.UnitShortName, GetFromDictionary)
                        End If

                    End If

                Catch
                    lHasErrors = True
                    strRowErrorMsg = "NAC not an valid value"
                    blConnect.LoadErrors(drsetup.UtilityUnitCode, Convert.ToInt16(drvEvent1.Item("Year")), Nothing, Convert.ToInt32(drvEvent1.Item("EventNumber")), 1, strRowErrorMsg, "E", drsetup.UnitShortName, GetFromDictionary)
                End Try

                ' ======================================
                ' Checking the Event Date/time values on a single Event 01 record


                If Convert.IsDBNull(drvEvent1.Item("StartDateTime")) = False And Convert.IsDBNull(drvEvent1.Item("EndDateTime")) = False Then

                    If DateTime.Compare(Convert.ToDateTime(drvEvent1.Item("EndDateTime")), Convert.ToDateTime(drvEvent1.Item("StartDateTime"))) <= 0 Then
                        lHasErrors = True
                        strRowErrorMsg = "Event End date/time must be after Start date/time"
                        blConnect.LoadErrors(drsetup.UtilityUnitCode, Convert.ToInt16(drvEvent1.Item("Year")), Nothing, Convert.ToInt32(drvEvent1.Item("EventNumber")), 1, strRowErrorMsg, "E", drsetup.UnitShortName, GetFromDictionary)
                    End If

                    ' do not convert to UtcNow
                    If DateTime.Compare(Convert.ToDateTime(dtNow), Convert.ToDateTime(drvEvent1.Item("EndDateTime"))) < 0 Then
                        lHasErrors = True
                        strRowErrorMsg = "End of Event date/time cannot be a future date/time"
                        blConnect.LoadErrors(drsetup.UtilityUnitCode, Convert.ToInt16(drvEvent1.Item("Year")), Nothing, Convert.ToInt32(drvEvent1.Item("EventNumber")), 1, strRowErrorMsg, "E", drsetup.UnitShortName, GetFromDictionary)
                    End If

                ElseIf Convert.IsDBNull(drvEvent1.Item("StartDateTime")) = False And Convert.IsDBNull(drvEvent1.Item("EndDateTime")) = True Then

                    ' do not convert to UtcNow
                    If Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Year < dtNow.Year Then
                        lHasErrors = True
                        strRowErrorMsg = "End of Event must be filled in"
                        blConnect.LoadErrors(drsetup.UtilityUnitCode, Convert.ToInt16(drvEvent1.Item("Year")), Nothing, Convert.ToInt32(drvEvent1.Item("EventNumber")), 1, strRowErrorMsg, "E", drsetup.UnitShortName, GetFromDictionary)
                    Else
                        If intMonthNo = 12 Then
                            lHasErrors = True
                            strRowErrorMsg = "End of Event must be filled in at end of year"
                            blConnect.LoadErrors(drsetup.UtilityUnitCode, Convert.ToInt16(drvEvent1.Item("Year")), Nothing, Convert.ToInt32(drvEvent1.Item("EventNumber")), 1, strRowErrorMsg, "E", drsetup.UnitShortName, GetFromDictionary)
                        Else
                            ' Open-ended full outage
                            strRowErrorMsg = "Warning:  End of Event is still open"
                            blConnect.LoadErrors(drsetup.UtilityUnitCode, Convert.ToInt16(drvEvent1.Item("Year")), Nothing, Convert.ToInt32(drvEvent1.Item("EventNumber")), 1, strRowErrorMsg, "W", drsetup.UnitShortName, GetFromDictionary)
                        End If
                    End If

                End If


                ' ======================================
                ' Checking for valid Cause Code

                Dim foundRows As DataRow()
                'Dim r As DataRow

                Try
                    If Convert.IsDBNull(drvEvent1.Item("CauseCode")) = False Then

                        If IsNumeric(drvEvent1.Item("CauseCode")) Then

                            If Convert.ToInt16(drvEvent1.Item("CauseCode")) >= 0 Then

                                Select Case drvEvent1.Item("EventType").ToString
                                    Case "IR"
                                        If Convert.ToInt16(drvEvent1.Item("CauseCode")) <> 2 Then
                                            lHasErrors = True
                                            strRowErrorMsg = "Invalid Cause Code" & vbCrLf
                                            blConnect.LoadErrors(drsetup.UtilityUnitCode, Convert.ToInt16(drvEvent1.Item("Year")), Nothing, Convert.ToInt32(drvEvent1.Item("EventNumber")), 1, strRowErrorMsg, "E", drsetup.UnitShortName, GetFromDictionary)
                                        End If
                                    Case "MB"
                                        If Convert.ToInt16(drvEvent1.Item("CauseCode")) <> 9991 Then
                                            lHasErrors = True
                                            strRowErrorMsg = "Invalid Cause Code" & vbCrLf
                                            blConnect.LoadErrors(drsetup.UtilityUnitCode, Convert.ToInt16(drvEvent1.Item("Year")), Nothing, Convert.ToInt32(drvEvent1.Item("EventNumber")), 1, strRowErrorMsg, "E", drsetup.UnitShortName, GetFromDictionary)
                                        End If
                                    Case "RU"
                                        If Convert.ToInt16(drvEvent1.Item("CauseCode")) <> 9990 Then
                                            lHasErrors = True
                                            strRowErrorMsg = "Invalid Cause Code" & vbCrLf
                                            blConnect.LoadErrors(drsetup.UtilityUnitCode, Convert.ToInt16(drvEvent1.Item("Year")), Nothing, Convert.ToInt32(drvEvent1.Item("EventNumber")), 1, strRowErrorMsg, "E", drsetup.UnitShortName, GetFromDictionary)
                                        End If
                                    Case Else
                                        foundRows = dsCauseCodes.Tables(0).Select("CauseCode = " & drvEvent1.Item("CauseCode").ToString)

                                        If foundRows.Length = 0 Then
                                            lHasErrors = True
                                            strRowErrorMsg = "Invalid Cause Code" & vbCrLf
                                            blConnect.LoadErrors(drsetup.UtilityUnitCode, Convert.ToInt16(drvEvent1.Item("Year")), Nothing, Convert.ToInt32(drvEvent1.Item("EventNumber")), 1, strRowErrorMsg, "E", drsetup.UnitShortName, GetFromDictionary)
                                        Else
                                            foundRows = dsCauseCodes.Tables(0).Select("CauseCode = " & drvEvent1.Item("CauseCode").ToString & " AND " & drsetup.UnitType & " = True")

                                            If foundRows.Length = 0 Then
                                                strRowErrorMsg = "WARNING:  Invalid Cause Code for this Unit Type" & vbCrLf
                                                blConnect.LoadErrors(drsetup.UtilityUnitCode, Convert.ToInt16(drvEvent1.Item("Year")), Nothing, Convert.ToInt32(drvEvent1.Item("EventNumber")), 1, strRowErrorMsg, "W", drsetup.UnitShortName, GetFromDictionary)
                                            End If
                                        End If
                                End Select

                            Else
                                lHasErrors = True
                                strRowErrorMsg += "- Invalid Cause Code" & vbCrLf
                            End If

                        End If

                    End If

                Catch
                    lHasErrors = True
                    strRowErrorMsg += "- Invalid Cause Code" & vbCrLf
                End Try


                longDiff = DateDiff(DateInterval.Second, dtEndOfPrior, Convert.ToDateTime(drvEvent1.Item("StartDateTime")))

                If vue.Count > 1 And longDiff <= 61 And (drvEvent1.Item("EventType").ToString <> "SE" And drvEvent1.Item("EventType").ToString <> "ME" And drvEvent1.Item("EventType").ToString <> "PE") Then

                    If longDiff = 0 Then

                        If Convert.IsDBNull(drvEvent1.Item("EventType")) = False Then
                            If Not ETChange(strPriorEventType, drvEvent1.Item("EventType").ToString) Then
                                lHasErrors = True
                                strRowErrorMsg = "Transition from " & strPriorEventType & " (" & intPriorNumber.ToString & ") to " & _
                                       drvEvent1.Item("EventType").ToString & " (" & drvEvent1.Item("EventNumber").ToString & ") is not allowed" & vbCrLf
                                blConnect.LoadErrors(drsetup.UtilityUnitCode, Convert.ToInt16(drvEvent1.Item("Year")), Nothing, Convert.ToInt32(drvEvent1.Item("EventNumber")), 1, strRowErrorMsg, "E", drsetup.UnitShortName, GetFromDictionary)
                            End If
                        End If

                    ElseIf longDiff = 60 Or longDiff = 61 Then

                        If Convert.IsDBNull(drvEvent1.Item("EventType")) = False Then
                            If Not ETChange(strPriorEventType, drvEvent1.Item("EventType").ToString) Then
                                lHasErrors = True
                                strRowErrorMsg = "Transition from " & strPriorEventType & " (" & intPriorNumber.ToString & ") to " & _
                                    drvEvent1.Item("EventType").ToString & " (" & drvEvent1.Item("EventNumber").ToString & ") is not allowed" & vbCrLf
                                blConnect.LoadErrors(drsetup.UtilityUnitCode, Convert.ToInt16(drvEvent1.Item("Year")), Nothing, Convert.ToInt32(drvEvent1.Item("EventNumber")), 1, strRowErrorMsg, "E", drsetup.UnitShortName, GetFromDictionary)
                            Else
                                strRowErrorMsg += "- 1 minute gap between this event and event # " & intPriorNumber.ToString & vbCrLf
                            End If
                        End If

                    ElseIf longDiff = 1 Then

                        If dtEndOfPrior.Hour = 23 And dtEndOfPrior.Minute = 59 And dtEndOfPrior.Second = 59 And _
                            Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Hour = 0 And _
                            Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Minute = 0 And _
                            Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Second = 0 And _
                            ETChange(strPriorEventType, drvEvent1.Item("EventType").ToString) Then

                            'lHasErrors = True
                            ''strRowErrorMsg += "- End date/time and Start datetime are not exactly the same: " & strPriorEventType & " (" & intPriorNumber.ToString & ") to " & _
                            ''    drvEvent1.Item("EventType").ToString & " (" & drvEvent1.Item("EventNumber").ToString & ")" & vbCrLf
                            'strRowErrorMsg += "- End date/time and Start date/time are not exactly the same: " & strPriorEventType & " (" & intPriorNumber.ToString & ") " & MF.GetFromDictionary(791).ToString & " " & _
                            '    drvEvent1.Item("EventType").ToString & " (" & drvEvent1.Item("EventNumber").ToString & ")" & vbCrLf

                        ElseIf Not ETChange(strPriorEventType, drvEvent1.Item("EventType").ToString) Then

                            lHasErrors = True
                            strRowErrorMsg = "Transition from " & strPriorEventType & " (" & intPriorNumber.ToString & ") to " & _
                                drvEvent1.Item("EventType").ToString & " (" & drvEvent1.Item("EventNumber").ToString & ") is not allowed" & vbCrLf
                            blConnect.LoadErrors(drsetup.UtilityUnitCode, Convert.ToInt16(drvEvent1.Item("Year")), Nothing, Convert.ToInt32(drvEvent1.Item("EventNumber")), 1, strRowErrorMsg, "E", drsetup.UnitShortName, GetFromDictionary)

                        End If

                    ElseIf longDiff > 1 Then

                        lHasErrors = True
                        strRowErrorMsg = "Unexpected event transition error: " & strPriorEventType & " (" & intPriorNumber.ToString & ") to " & _
                            drvEvent1.Item("EventType").ToString & " (" & drvEvent1.Item("EventNumber").ToString & ") is not allowed" & vbCrLf
                        blConnect.LoadErrors(drsetup.UtilityUnitCode, Convert.ToInt16(drvEvent1.Item("Year")), Nothing, Convert.ToInt32(drvEvent1.Item("EventNumber")), 1, strRowErrorMsg, "E", drsetup.UnitShortName, GetFromDictionary)

                    ElseIf longDiff < 0 Then

                        lHasErrors = True
                        If intPriorNumber.ToString.Trim = drvEvent1.Item("EventNumber").ToString.Trim Then
                            strRowErrorMsg = "Overlapping Events: " & _
                            drvEvent1.Item("EventType").ToString & " (" & drvEvent1.Item("EventNumber").ToString & ") is overlapped by another event and this is not allowed" & vbCrLf
                            blConnect.LoadErrors(drsetup.UtilityUnitCode, Convert.ToInt16(drvEvent1.Item("Year")), Nothing, Convert.ToInt32(drvEvent1.Item("EventNumber")), 1, strRowErrorMsg, "E", drsetup.UnitShortName, GetFromDictionary)
                        Else
                            strRowErrorMsg = "Overlapping Events: " & strPriorEventType & " (" & intPriorNumber.ToString & ") and " & _
                                   drvEvent1.Item("EventType").ToString & " (" & drvEvent1.Item("EventNumber").ToString & ") is not allowed" & vbCrLf
                            blConnect.LoadErrors(drsetup.UtilityUnitCode, Convert.ToInt16(drvEvent1.Item("Year")), Nothing, Convert.ToInt32(drvEvent1.Item("EventNumber")), 1, strRowErrorMsg, "E", drsetup.UnitShortName, GetFromDictionary)
                        End If

                    End If

                End If

                If (drvEvent1.Item("EventType").ToString = "SE" Or drvEvent1.Item("EventType").ToString = "ME" Or drvEvent1.Item("EventType").ToString = "PE") And Convert.IsDBNull(drvEvent1.Item("StartDateTime")) = False Then

                    dtStartOfNextSE = Convert.ToDateTime(drvEvent1.Item("StartDateTime"))

                    If dtStartOfNextSE > Convert.ToDateTime("01/01/" & drvEvent1.Item("Year").ToString & " 00:00") Then

                        ' This deals with the possibility that the SE might be the first reported event for the year or 
                        ' carries over from last year


                        longDiff = DateDiff(DateInterval.Minute, dtEndOfPriorMOPO, dtStartOfNextSE)
                        ' The use of longDiff allows for 1/31/2003 24:00 to be the same as 2/1/2003 00:00

                        If longDiff = 1 Then

                            lHasErrors = True
                            strRowErrorMsg += "- 1 minute gap between this event and end of " & strPriorMOPOEventType & " (" & intPriorMOPONumber.ToString & ")" & vbCrLf

                        ElseIf longDiff > 1 Then

                            lHasErrors = True

                            If intPriorMOPONumber > 0 Then

                                ' these must be the same MO -> SE or PO -> SE or SE -> SE
                                strRowErrorMsg = "Start of SE must exactly match end of " & strPriorMOPOEventType & " (" & intPriorMOPONumber.ToString & ")" & vbCrLf
                                blConnect.LoadErrors(drsetup.UtilityUnitCode, Convert.ToInt16(drvEvent1.Item("Year")), Nothing, Convert.ToInt32(drvEvent1.Item("EventNumber")), 1, strRowErrorMsg, "E", drsetup.UnitShortName, GetFromDictionary)
                            Else
                                strRowErrorMsg = "Start of SE must exactly match end of " & strPriorMOPOEventType & " (" & intPriorMOPONumber.ToString & ")" & vbCrLf
                                blConnect.LoadErrors(drsetup.UtilityUnitCode, Convert.ToInt16(drvEvent1.Item("Year")), Nothing, Convert.ToInt32(drvEvent1.Item("EventNumber")), 1, strRowErrorMsg, "E", drsetup.UnitShortName, GetFromDictionary)
                            End If

                        ElseIf longDiff < 0 Then

                            lHasErrors = True

                            If intPriorNumber.ToString.Trim = drvEvent1.Item("EventNumber").ToString.Trim Then
                                strRowErrorMsg = "Overlapping Events: " & _
                                drvEvent1.Item("EventType").ToString & " (" & drvEvent1.Item("EventNumber").ToString & ") is overlapped by another event and this is not allowed" & vbCrLf
                                blConnect.LoadErrors(drsetup.UtilityUnitCode, Convert.ToInt16(drvEvent1.Item("Year")), Nothing, Convert.ToInt32(drvEvent1.Item("EventNumber")), 1, strRowErrorMsg, "E", drsetup.UnitShortName, GetFromDictionary)
                            Else
                                strRowErrorMsg = "Overlapping Events: " & strPriorEventType & " (" & intPriorNumber.ToString & ") and " & _
                                       drvEvent1.Item("EventType").ToString & " (" & drvEvent1.Item("EventNumber").ToString & ") is not allowed" & vbCrLf
                                blConnect.LoadErrors(drsetup.UtilityUnitCode, Convert.ToInt16(drvEvent1.Item("Year")), Nothing, Convert.ToInt32(drvEvent1.Item("EventNumber")), 1, strRowErrorMsg, "E", drsetup.UnitShortName, GetFromDictionary)
                            End If

                        Else

                            If Not ETChange(strPriorEventType, drvEvent1.Item("EventType").ToString) Then
                                lHasErrors = True
                                strRowErrorMsg = "Transition from " & strPriorEventType & " (" & intPriorNumber.ToString & ") to " & _
                                    drvEvent1.Item("EventType").ToString & " (" & drvEvent1.Item("EventNumber").ToString & ") is not allowed" & vbCrLf
                                blConnect.LoadErrors(drsetup.UtilityUnitCode, Convert.ToInt16(drvEvent1.Item("Year")), Nothing, Convert.ToInt32(drvEvent1.Item("EventNumber")), 1, strRowErrorMsg, "E", drsetup.UnitShortName, GetFromDictionary)
                            End If

                        End If

                    End If

                    If Convert.IsDBNull(drvEvent1.Item("EndDateTime")) = False Then

                        dtEndOfPriorMOPO = Convert.ToDateTime(drvEvent1.Item("EndDateTime"))
                        intPriorMOPONumber = Convert.ToInt32(drvEvent1.Item("EventNumber"))
                        strPriorMOPOEventType = drvEvent1.Item("EventType").ToString


                    Else

                        dtEndOfPriorMOPO = Nothing
                        dtStartOfNextSE = Nothing
                        intPriorMOPONumber = -1

                        strPriorMOPOEventType = Nothing

                    End If

                Else
                    dtStartOfNextSE = Nothing
                End If

                intPriorNumber = Convert.ToInt32(drvEvent1.Item("EventNumber"))
                strPriorEventType = drvEvent1.Item("EventType").ToString

                If Convert.IsDBNull(drvEvent1.Item("EndDateTime")) Then
                    ' The first full outage has an open ended End date/time
                    dtEndOfPrior = blConnect.EOM(dtNow)        ' do not convert to UtcNow
                Else
                    dtEndOfPrior = Convert.ToDateTime(drvEvent1.Item("EndDateTime"))
                End If

                If (drvEvent1.Item("EventType").ToString = "MO" Or drvEvent1.Item("EventType").ToString = "PO") And _
                       Convert.IsDBNull(drvEvent1.Item("EndDateTime")) = False Then

                    dtEndOfPriorMOPO = Convert.ToDateTime(drvEvent1.Item("EndDateTime"))
                    intPriorMOPONumber = Convert.ToInt32(drvEvent1.Item("EventNumber"))
                    strPriorMOPOEventType = drvEvent1.Item("EventType").ToString

                End If

                'Select Case drvEvent1.Item("EventType").ToString.ToUpper
                '    Case "U1", "U2", "U3", "SF", "PO", "MO", "SE", "NC", "RS", "D1", "D2", "D3", "D4", "PD", "DE", "CO", "PU", "ME", "PE", "DM", "DP"
                '    Case "RU", "MB", "IR"
                '        'lHasErrors = True
                '        'strRowErrorMsg = drsetup.UnitName & " " & drvEvent1.Item("Year").ToString & " " & drvEvent1.Item("EventNumber").ToString & " ERROR: Invalid Event Type"
                '        'blConnect.LoadErrors(drsetup.UtilityUnitCode, CInt(drvEvent1.Item("Year")), String.Empty, CInt(drvEvent1.Item("EventNumber")), 1, "Contact NYISO for this event type", "E", drsetup.UnitShortName, GetFromDictionary)
                '        'drsetup.CheckStatus = "E"
                '    Case Else
                '        lHasErrors = True
                '        strRowErrorMsg = drsetup.UnitName & " " & drvEvent1.Item("Year").ToString & " " & drvEvent1.Item("EventNumber").ToString & " ERROR: Invalid Event Type"
                '        blConnect.LoadErrors(drsetup.UtilityUnitCode, CInt(drvEvent1.Item("Year")), String.Empty, CInt(drvEvent1.Item("EventNumber")), 1, "Invalid Event Type", "E", drsetup.UnitShortName, GetFromDictionary)
                'End Select


            Next

        End If

        '=============================================================================================================================================================
        ' Validate the NC events

        vue.RowFilter = "EventType = 'NC'"

        For intCounter = 0 To vue.Count - 1

            drvEvent1 = vue(intCounter)

            Try

                If Convert.IsDBNull(drvEvent1.Item("NetAvailCapacity")) = False Then

                    If Text.RegularExpressions.Regex.IsMatch(drvEvent1.Item("NetAvailCapacity").ToString, "^\d+(\.\d+)?$") Then

                        If IsNumeric(drvEvent1.Item("NetAvailCapacity").ToString) Then
                            decNAC = Convert.ToDecimal(drvEvent1.Item("NetAvailCapacity").ToString)
                            If decNAC <> 0 Then
                                strRowErrorMsg = "WARNING: Net Available Capacity should be blank"
                                blConnect.LoadErrors(drsetup.UtilityUnitCode, Convert.ToInt16(drvEvent1.Item("Year")), Nothing, Convert.ToInt32(drvEvent1.Item("EventNumber")), 1, strRowErrorMsg, "W", drsetup.UnitShortName, GetFromDictionary)
                            End If
                        End If
                    Else
                        lHasErrors = True
                        strRowErrorMsg = "Invalid Net Available Capacity"
                        blConnect.LoadErrors(drsetup.UtilityUnitCode, Convert.ToInt16(drvEvent1.Item("Year")), Nothing, Convert.ToInt32(drvEvent1.Item("EventNumber")), 1, strRowErrorMsg, "E", drsetup.UnitShortName, GetFromDictionary)
                    End If

                End If

            Catch
                lHasErrors = True
                'strRowErrorMsg = "GAC and/or NAC not an integer value"
                strRowErrorMsg = "NAC not an valid value"
                blConnect.LoadErrors(drsetup.UtilityUnitCode, Convert.ToInt16(drvEvent1.Item("Year")), Nothing, Convert.ToInt32(drvEvent1.Item("EventNumber")), 1, strRowErrorMsg, "E", drsetup.UnitShortName, GetFromDictionary)
            End Try

            If Convert.IsDBNull(drvEvent1.Item("StartDateTime")) = False And Convert.IsDBNull(drvEvent1.Item("EndDateTime")) = False Then

                If DateTime.Compare(Convert.ToDateTime(drvEvent1.Item("EndDateTime")), Convert.ToDateTime(drvEvent1.Item("StartDateTime"))) <= 0 Then
                    lHasErrors = True
                    strRowErrorMsg = "Event End date/time must be after Start date/time"
                    blConnect.LoadErrors(drsetup.UtilityUnitCode, Convert.ToInt16(drvEvent1.Item("Year")), Nothing, Convert.ToInt32(drvEvent1.Item("EventNumber")), 1, strRowErrorMsg, "E", drsetup.UnitShortName, GetFromDictionary)
                End If

                ' do not convert to UtcNow
                If DateTime.Compare(Convert.ToDateTime(dtNow), Convert.ToDateTime(drvEvent1.Item("EndDateTime"))) < 0 Then
                    lHasErrors = True
                    strRowErrorMsg = "End of Event date/time cannot be a future date/time"
                    blConnect.LoadErrors(drsetup.UtilityUnitCode, Convert.ToInt16(drvEvent1.Item("Year")), Nothing, Convert.ToInt32(drvEvent1.Item("EventNumber")), 1, strRowErrorMsg, "E", drsetup.UnitShortName, GetFromDictionary)
                End If

            ElseIf Convert.IsDBNull(drvEvent1.Item("StartDateTime")) = False And Convert.IsDBNull(drvEvent1.Item("EndDateTime")) = True Then

                ' do not convert to UtcNow
                If Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Year < dtNow.Year Then
                    lHasErrors = True
                    strRowErrorMsg = "End of Event must be filled in"
                    blConnect.LoadErrors(drsetup.UtilityUnitCode, Convert.ToInt16(drvEvent1.Item("Year")), Nothing, Convert.ToInt32(drvEvent1.Item("EventNumber")), 1, strRowErrorMsg, "E", drsetup.UnitShortName, GetFromDictionary)
                Else
                    If intMonthNo = 12 Then
                        lHasErrors = True
                        strRowErrorMsg = "End of Event must be filled in at end of year"
                        blConnect.LoadErrors(drsetup.UtilityUnitCode, Convert.ToInt16(drvEvent1.Item("Year")), Nothing, Convert.ToInt32(drvEvent1.Item("EventNumber")), 1, strRowErrorMsg, "E", drsetup.UnitShortName, GetFromDictionary)
                    Else
                        ' Open-ended NC
                        strRowErrorMsg = "Warning:  End of Event is still open"
                        blConnect.LoadErrors(drsetup.UtilityUnitCode, Convert.ToInt16(drvEvent1.Item("Year")), Nothing, Convert.ToInt32(drvEvent1.Item("EventNumber")), 1, strRowErrorMsg, "W", drsetup.UnitShortName, GetFromDictionary)
                    End If
                End If

            End If

            'If strRowErrorMsg.Trim <> String.Empty Then
            '	strErrors += "Event No. " & drvEvent1.Item("EventNumber").ToString & " Primary Cause of Event" & _
            '	 vbCrLf & strRowErrorMsg.Replace("-", (vbTab + "-"))
            'End If

        Next

        vue.RowFilter = String.Empty

        If lHasErrors = True Then
            Return "Errors Exist"
        Else
            Return ""
        End If

    End Function

#End Region

#Region " ISOErrorCheck "

    Public Function ISOErrorCheck(ByVal blConnect As BusinessLayer.BusinessLayer.GADSNGBusinessLayer, ByVal drSetup As Setup.SetupRow, _
                                  ByVal intCurrentYear As Integer, ByVal dtEvents01 As Event01.EventData01DataTable, _
                                  ByVal dtPerformance As Performance.PerformanceDataDataTable, ByRef GetFromDictionary As HybridDictionary) As String

        Dim dtNow As DateTime = System.DateTime.Now
        Dim intOpenEvents As Integer = 0
        Dim intMonth As Integer = 0
        Dim strErrors As String = String.Empty
        Dim strWarnings As String = String.Empty
        Dim strCurrentUnit As String = drSetup.UtilityUnitCode
        Dim drPerformance As Performance.PerformanceDataRow = Nothing
        Dim dtRetirementDateTime As DateTime = dtNow

        ' ===================================================================
        Dim lHasErrors As Boolean = False

        If dtNow < Convert.ToDateTime("01/01/1980") Then
            dtNow = System.DateTime.Now
        End If

        ' --------------------------------------------------
        ' Looking for open-ended events in any prior period
        ' --------------------------------------------------

        intOpenEvents = blConnect.CountOpenEvents(drSetup.UtilityUnitCode, intCurrentYear)

        If intOpenEvents > 0 Then

            Dim OpenEventsReader As IDataReader
            Dim intOpenYear As New ArrayList
            Dim intEventNumber As New ArrayList
            Dim strEventType As New ArrayList
            Dim dtimeStartDateTime As New ArrayList

            OpenEventsReader = blConnect.OpenEventYears(drSetup.UtilityUnitCode, intCurrentYear)

            While OpenEventsReader.Read

                ' Year, EventNumber, EventType, StartDateTime
                intOpenYear.Add(CInt(OpenEventsReader.GetValue(0)))
                intEventNumber.Add(CInt(OpenEventsReader.GetValue(1)))
                strEventType.Add(OpenEventsReader.GetString(2))
                dtimeStartDateTime.Add(OpenEventsReader.GetDateTime(3).ToString("MM/dd/yyyy HH:mm"))

            End While

            OpenEventsReader.Close()

            If intOpenYear.Count > 0 Then

                Dim i As Integer

                For i = 0 To (intOpenYear.Count - 1)
                    strErrors = "Open-ended " & strEventType(i).ToString & " starting at " & dtimeStartDateTime(i).ToString & " is still open"
                    blConnect.LoadErrors(drSetup.UtilityUnitCode, Convert.ToInt16(intOpenYear(i)), Nothing, Convert.ToInt32(intEventNumber(i)), 1, strErrors, "E", drSetup.UnitShortName, GetFromDictionary)
                Next

            End If

        End If

        Dim drCurrent As DataRow

        Dim dtDate As DateTime = dtNow
        Dim douTime As Double = 0

        Dim dtStart As DateTime = dtNow
        Dim dtStart1 As DateTime = dtNow
        Dim dtStart2 As DateTime = dtNow

        Dim dtEnd As DateTime = dtNow
        Dim dtEnd1 As DateTime = dtNow
        Dim dtEnd2 As DateTime = dtNow

        Dim dtTemp As DateTime = dtNow
        Dim dtTemp2 As DateTime = dtNow

        Dim alSHMethod As New ArrayList
        alSHMethod = blConnect.BuildALSHMethod()
        Dim dtCheck As System.DateTime = dtNow

        Dim intDecPlaces As Int16 = 2
        Dim strHoursFormat As String = "000.00"

        Dim douAccuracy As Double = 0.15

        If blConnect.IncludedEvents(DirectCast(drSetup, System.Data.DataRow), alSHMethod, dtCheck) = 0 Or _
           blConnect.IncludedEvents(DirectCast(drSetup, System.Data.DataRow), alSHMethod, dtCheck) = 1 Then ' drSetup.ServiceHourMethod = 0 Or drSetup.ServiceHourMethod = 1 Then

            ' ServiceHourMethod = 0 : All Event data are reported
            ' ServiceHourMethod = 1 : All Event data EXCEPT RS Events are reported
            ' ----------------------------------------------------------
            ' Comparing Performance 02 hours with calculated event data

            Dim lFirstChange As Boolean = False
            Dim lSecondChange As Boolean = False

            Dim strTemp As String = String.Empty
            Dim douPOH(12) As Double
            Dim douFOH(12) As Double
            Dim douMOH(12) As Double
            Dim douSEH(12) As Double

            Dim douSH As Double = 0
            Dim douPumpHrs(12) As Double
            Dim douSynHrs(12) As Double
            Dim douRSH(12) As Double
            Dim douPH(12) As Double

            Dim douIH(12) As Double

            For Each drCurrent In dtEvents01.Rows

                lFirstChange = False
                lSecondChange = False
                douTime = 0.0

                If drCurrent("RevisionCard01").ToString <> "X" Then

                    If Not Convert.IsDBNull(drCurrent("StartDateTime")) And Not Convert.IsDBNull(drCurrent("EventType")) Then

                        Try
                            ' the check to accept or reject this event record is based on the start of event datetime
                            dtCheck = System.DateTime.Parse(drCurrent("StartDateTime").ToString)
                        Catch ex As Exception
                            dtCheck = dtNow
                        End Try

                        If blConnect.IncludedEvents(DirectCast(drSetup, System.Data.DataRow), alSHMethod, dtCheck) = 2 Then
                            ' Event Data are NOT reported so do not load
                            strErrors = "Settings show NO events to be reported"
                            blConnect.LoadErrors(drSetup("UtilityUnitCode").ToString, CInt(drCurrent("Year")), Nothing, CInt(drCurrent("EventNumber")), Nothing, strErrors, "E", drSetup("UnitShortName").ToString, GetFromDictionary)
                        ElseIf blConnect.IncludedEvents(DirectCast(drSetup, System.Data.DataRow), alSHMethod, dtCheck) = 1 And drCurrent("EventType").ToString.Trim = "RS" Then
                            ' Event Data are report but RS events are NOT reported so do not load
                            strErrors = "Settings show NO RS events to be reported"
                            blConnect.LoadErrors(drSetup("UtilityUnitCode").ToString, CInt(drCurrent("Year")), Nothing, CInt(drCurrent("EventNumber")), Nothing, strErrors, "E", drSetup("UnitShortName").ToString, GetFromDictionary)
                            'ElseIf (drCurrent("EventType").ToString.Trim = "CO" Or drCurrent("EventType").ToString.Trim = "PU") And dtCheck < System.DateTime.Parse(COPUCUTOFFDATE) Then
                            '    ' Cannot report CO or PU events prior to 10/01/2006
                            '    strErrors = "CO/PU events are not allowed before 10/01/2006"
                            '    blConnect.LoadErrors(drSetup("UtilityUnitCode").ToString, CInt(drCurrent("Year")), Nothing, CInt(drCurrent("EventNumber")), Nothing, strErrors, "E", drSetup("UnitShortName").ToString, GetFromDictionary)
                        End If

                        ' Have a valid Starting Date/time and Event Type

                        dtStart = Convert.ToDateTime(drCurrent("StartDateTime"))
                        strTemp = drCurrent("EventNumber").ToString

                        If Convert.IsDBNull(drCurrent("ChangeDateTime1")) Or Convert.IsDBNull(drCurrent("ChangeInEventType1")) Then

                            ' First Change in Event Date/time is null or the first Change in event type is null 
                            ' use End Date/time

                            If Convert.IsDBNull(drCurrent("EndDateTime")) Then
                                ' This event is open ended use today's date and time
                                ' ... unless it retired before today.

                                If drSetup.RetirementDate < dtNow Then
                                    dtEnd = DateTime.Parse(drSetup.RetirementDate.ToShortDateString & " 23:59:59")
                                Else
                                    'dtEnd = dtNow        ' do not convert to UtcNow
                                    dtEnd = blConnect.EOM(dtNow)
                                End If

                                If dtEnd.Year <> dtStart.Year Then
                                    dtEnd = DateTime.Parse("12/31/" & dtStart.Year.ToString & " 23:59:59")
                                End If

                            Else
                                dtEnd = Convert.ToDateTime(drCurrent("EndDateTime"))
                            End If

                            'If Convert.ToInt16(drCurrent("CauseCode").ToString) = 0 Then
                            '	If drCurrent("EventType").ToString.ToUpper <> "RS" Then
                            '		If drSetup.InputDataRequirements = 0 Then
                            '			' NERC full data set requires that the Cause Code be filled in
                            '           strErrors = "Cause Code of 0000 is only valid for RS"
                            '			blConnect.LoadErrors(strCurrentUnit, Convert.ToInt16(drCurrent("Year")), dtStart.Month.ToString("00"), Convert.ToInt32(drCurrent("EventNumber")), 1, strErrors, "E", drSetup.UnitShortName)
                            '		End If
                            '	End If
                            'End If

                            Select Case drCurrent("EventType").ToString.ToUpper

                                Case "U1", "U2", "U3", "SF", "PO", "MO", "SE", "RS", "PU", "CO", "ME", "PE", "IR", "MB", "RU"

                                    'If drCurrent("EventType").ToString.ToUpper = "RS" Then
                                    '	If Convert.ToInt16(drCurrent("CauseCode").ToString) <> 0 Then
                                    '       strErrors = "RS requires a Cause Code of 0000"
                                    '		blConnect.LoadErrors(strCurrentUnit, Convert.ToInt16(drCurrent("Year")), dtStart.Month.ToString("00"), Convert.ToInt32(drCurrent("EventNumber")), 1, strErrors, "E", drSetup.UnitShortName)
                                    '	End If
                                    'End If

                                    dtDate = dtStart

                                    If dtStart.Month = dtEnd.Month Then
                                        ' the event starts and stops in same month

                                        intMonth = dtStart.Month - 1

                                        douTime = TimeBetween(dtStart, dtEnd, drSetup.DaylightSavingTime)

                                        Select Case drCurrent("EventType").ToString.ToUpper
                                            Case "PO"
                                                douPOH(intMonth) += douTime
                                            Case "U1", "U2", "U3", "SF"
                                                douFOH(intMonth) += douTime
                                            Case "MO"
                                                douMOH(intMonth) += douTime
                                            Case "SE", "PE", "ME"
                                                douSEH(intMonth) += douTime
                                            Case "RS"
                                                douRSH(intMonth) += douTime
                                            Case "PU"
                                                douPumpHrs(intMonth) += douTime
                                            Case "CO"
                                                douSynHrs(intMonth) += douTime
                                            Case "IR", "MB", "RU"
                                                douIH(intMonth) += douTime
                                        End Select

                                    ElseIf dtEnd.Month = dtStart.Month + 1 Then

                                        ' the event starts in month n and ends the next month

                                        intMonth = dtStart.Month - 1
                                        strTemp = dtStart.Month.ToString & "/" & System.DateTime.DaysInMonth(dtStart.Year, dtStart.Month).ToString & "/" & dtStart.Year.ToString & " 23:59:59"
                                        dtTemp = System.DateTime.Parse(strTemp)
                                        douTime = TimeBetween(dtStart, dtTemp, drSetup.DaylightSavingTime)

                                        Select Case drCurrent("EventType").ToString.ToUpper
                                            Case "PO"
                                                douPOH(intMonth) += douTime
                                            Case "U1", "U2", "U3", "SF"
                                                douFOH(intMonth) += douTime
                                            Case "MO"
                                                douMOH(intMonth) += douTime
                                            Case "SE", "PE", "ME"
                                                douSEH(intMonth) += douTime
                                            Case "RS"
                                                douRSH(intMonth) += douTime
                                            Case "PU"
                                                douPumpHrs(intMonth) += douTime
                                            Case "CO"
                                                douSynHrs(intMonth) += douTime
                                            Case "IR", "MB", "RU"
                                                douIH(intMonth) += douTime
                                        End Select

                                        intMonth = dtEnd.Month - 1
                                        strTemp = dtEnd.Month.ToString & "/01/" & dtEnd.Year.ToString & " 0:00:00"
                                        dtTemp = System.DateTime.Parse(strTemp)
                                        douTime = TimeBetween(dtTemp, dtEnd, drSetup.DaylightSavingTime)

                                        Select Case drCurrent("EventType").ToString.ToUpper
                                            Case "PO"
                                                douPOH(intMonth) += douTime
                                            Case "U1", "U2", "U3", "SF"
                                                douFOH(intMonth) += douTime
                                            Case "MO"
                                                douMOH(intMonth) += douTime
                                            Case "SE", "PE", "ME"
                                                douSEH(intMonth) += douTime
                                            Case "RS"
                                                douRSH(intMonth) += douTime
                                            Case "PU"
                                                douPumpHrs(intMonth) += douTime
                                            Case "CO"
                                                douSynHrs(intMonth) += douTime
                                            Case "IR", "MB", "RU"
                                                douIH(intMonth) += douTime
                                        End Select

                                    Else
                                        ' there are one or more months between the start and end of event

                                        intMonth = dtStart.Month - 1
                                        strTemp = dtStart.Month.ToString & "/" & System.DateTime.DaysInMonth(dtStart.Year, dtStart.Month).ToString & "/" & dtStart.Year.ToString & " 23:59:59"
                                        dtTemp = System.DateTime.Parse(strTemp)
                                        douTime = TimeBetween(dtStart, dtTemp, drSetup.DaylightSavingTime)

                                        Select Case drCurrent("EventType").ToString.ToUpper
                                            Case "PO"
                                                douPOH(intMonth) += douTime
                                            Case "U1", "U2", "U3", "SF"
                                                douFOH(intMonth) += douTime
                                            Case "MO"
                                                douMOH(intMonth) += douTime
                                            Case "SE", "PE", "ME"
                                                douSEH(intMonth) += douTime
                                            Case "RS"
                                                douRSH(intMonth) += douTime
                                            Case "PU"
                                                douPumpHrs(intMonth) += douTime
                                            Case "CO"
                                                douSynHrs(intMonth) += douTime
                                            Case "IR", "MB", "RU"
                                                douIH(intMonth) += douTime
                                        End Select


                                        For intMonth = dtStart.Month + 1 To dtEnd.Month - 1

                                            strTemp = intMonth.ToString & "/01/" & dtEnd.Year.ToString & " 0:00:00"
                                            dtTemp = System.DateTime.Parse(strTemp)
                                            strTemp = dtTemp.Month.ToString & "/" & System.DateTime.DaysInMonth(dtTemp.Year, dtTemp.Month).ToString & "/" & dtTemp.Year.ToString & " 23:59:59"
                                            dtTemp2 = System.DateTime.Parse(strTemp)

                                            douTime = TimeBetween(dtTemp, dtTemp2, drSetup.DaylightSavingTime)

                                            Select Case drCurrent("EventType").ToString.ToUpper
                                                Case "PO"
                                                    douPOH(intMonth - 1) += douTime
                                                Case "U1", "U2", "U3", "SF"
                                                    douFOH(intMonth - 1) += douTime
                                                Case "MO"
                                                    douMOH(intMonth - 1) += douTime
                                                Case "SE", "PE", "ME"
                                                    douSEH(intMonth - 1) += douTime
                                                Case "RS"
                                                    douRSH(intMonth - 1) += douTime
                                                Case "PU"
                                                    douPumpHrs(intMonth - 1) += douTime
                                                Case "CO"
                                                    douSynHrs(intMonth - 1) += douTime
                                                Case "IR", "MB", "RU"
                                                    douIH(intMonth - 1) += douTime
                                            End Select
                                        Next

                                        intMonth = dtEnd.Month - 1
                                        strTemp = dtEnd.Month.ToString & "/01/" & dtEnd.Year.ToString & " 0:00:00"
                                        dtTemp = System.DateTime.Parse(strTemp)
                                        douTime = TimeBetween(dtTemp, dtEnd, drSetup.DaylightSavingTime)

                                        Select Case drCurrent("EventType").ToString.ToUpper
                                            Case "PO"
                                                douPOH(intMonth) += douTime
                                            Case "U1", "U2", "U3", "SF"
                                                douFOH(intMonth) += douTime
                                            Case "MO"
                                                douMOH(intMonth) += douTime
                                            Case "SE", "PE", "ME"
                                                douSEH(intMonth) += douTime
                                            Case "RS"
                                                douRSH(intMonth) += douTime
                                            Case "PU"
                                                douPumpHrs(intMonth) += douTime
                                            Case "CO"
                                                douSynHrs(intMonth) += douTime
                                            Case "IR", "MB", "RU"
                                                douIH(intMonth) += douTime
                                        End Select

                                    End If

                                Case Else
                                    douTime = 0.0
                            End Select

                        Else

                            'lFirstChange = True
                            'dtEnd1 = Convert.ToDateTime(drCurrent("ChangeDateTime1"))

                            'Select Case drCurrent("EventType").ToString.ToUpper

                            '    Case "U1", "U2", "U3", "SF", "PO", "MO", "SE", "RS", "PU", "CO", "PE", "ME", "IR", "MB", "RU"

                            '        douTime = TimeBetween(dtStart, dtEnd1, drSetup.DaylightSavingTime)

                            '        Select Case drCurrent("EventType").ToString.ToUpper
                            '            Case "PO"
                            '                douPOH(intMonth) += douTime
                            '            Case "U1", "U2", "U3", "SF"
                            '                douFOH(intMonth) += douTime
                            '            Case "MO"
                            '                douMOH(intMonth) += douTime
                            '            Case "SE", "PE", "ME"
                            '                douSEH(intMonth) += douTime
                            '            Case "RS"
                            '                douRSH(intMonth) += douTime
                            '            Case "PU"
                            '                douPumpHrs(intMonth) += douTime
                            '            Case "CO"
                            '                douSynHrs(intMonth) += douTime
                            '            Case "IR", "MB", "RU"
                            '                douIH(intMonth) += douTime
                            '        End Select

                            '    Case Else
                            '        douTime = 0.0
                            'End Select

                            'dtStart1 = dtEnd1

                            'If Convert.IsDBNull(drCurrent("ChangeDateTime2")) Or Convert.IsDBNull(drCurrent("ChangeInEventType2")) Then
                            '    ' Second change in event date/time is null or the second change in event type is null
                            '    ' use End Date/time

                            '    If Convert.IsDBNull(drCurrent("EndDateTime")) Then
                            '        ' This event is open ended use today's date and time
                            '        dtEnd = dtNow          ' do not convert to UtcNow
                            '        If dtEnd.Year <> dtStart1.Year Then
                            '            dtEnd = DateTime.Parse("12/31/" & dtStart1.Year.ToString & " 23:59:59")
                            '        End If

                            '    Else
                            '        dtEnd = Convert.ToDateTime(drCurrent("EndDateTime"))
                            '    End If

                            '    Select Case drCurrent("ChangeInEventType1").ToString.ToUpper

                            '        Case "U1", "U2", "U3", "SF", "PO", "MO", "SE", "RS", "PU", "CO", "PE", "ME", "IR", "MB", "RU"

                            '            douTime = TimeBetween(dtStart1, dtEnd, drSetup.DaylightSavingTime)

                            '            Select Case drCurrent("ChangeInEventType1").ToString.ToUpper
                            '                Case "PO"
                            '                    douPOH(intMonth) += douTime
                            '                Case "U1", "U2", "U3", "SF"
                            '                    douFOH(intMonth) += douTime
                            '                Case "MO"
                            '                    douMOH(intMonth) += douTime
                            '                Case "SE", "PE", "ME"
                            '                    douSEH(intMonth) += douTime
                            '                Case "RS"
                            '                    douRSH(intMonth) += douTime
                            '                Case "PU"
                            '                    douPumpHrs(intMonth) += douTime
                            '                Case "CO"
                            '                    douSynHrs(intMonth) += douTime
                            '                Case "IR", "MB", "RU"
                            '                    douIH(intMonth) += douTime
                            '            End Select

                            '        Case Else
                            '            douTime = 0.0
                            '    End Select

                            'Else

                            '    lSecondChange = True
                            '    dtEnd2 = Convert.ToDateTime(drCurrent("ChangeDateTime2"))

                            '    Select Case drCurrent("ChangeInEventType1").ToString.ToUpper
                            '        Case "U1", "U2", "U3", "SF", "PO", "MO", "SE", "RS", "PU", "CO", "PE", "ME", "IR", "MB", "RU"

                            '            douTime = TimeBetween(dtStart1, dtEnd2, drSetup.DaylightSavingTime)

                            '            Select Case drCurrent("ChangeInEventType1").ToString.ToUpper
                            '                Case "PO"
                            '                    douPOH(intMonth) += douTime
                            '                Case "U1", "U2", "U3", "SF"
                            '                    douFOH(intMonth) += douTime
                            '                Case "MO"
                            '                    douMOH(intMonth) += douTime
                            '                Case "SE", "PE", "ME"
                            '                    douSEH(intMonth) += douTime
                            '                Case "RS"
                            '                    douRSH(intMonth) += douTime
                            '                Case "PU"
                            '                    douPumpHrs(intMonth) += douTime
                            '                Case "CO"
                            '                    douSynHrs(intMonth) += douTime
                            '                Case "IR", "MB", "RU"
                            '                    douIH(intMonth) += douTime
                            '            End Select

                            '        Case Else
                            '            douTime = 0.0
                            '    End Select

                            '    dtStart2 = dtEnd2

                            '    If Convert.IsDBNull(drCurrent("EndDateTime")) Then
                            '        ' This event is open ended use today's date and time
                            '        dtEnd = dtNow           ' do not convert to UtcNow
                            '        If dtEnd.Year <> dtStart2.Year Then
                            '            dtEnd = DateTime.Parse("12/31/" & dtStart2.Year.ToString & " 23:59:59")
                            '        End If

                            '    Else
                            '        dtEnd = Convert.ToDateTime(drCurrent("EndDateTime"))
                            '    End If

                            '    Select Case drCurrent("ChangeInEventType2").ToString.ToUpper
                            '        Case "U1", "U2", "U3", "SF", "PO", "MO", "SE", "RS", "PU", "CO", "PE", "ME", "IR", "MB", "RU"
                            '            douTime = TimeBetween(dtStart2, dtEnd, drSetup.DaylightSavingTime)

                            '            Select Case drCurrent("ChangeInEventType2").ToString.ToUpper
                            '                Case "PO"
                            '                    douPOH(intMonth) += douTime
                            '                Case "U1", "U2", "U3", "SF"
                            '                    douFOH(intMonth) += douTime
                            '                Case "MO"
                            '                    douMOH(intMonth) += douTime
                            '                Case "SE", "PE", "ME"
                            '                    douSEH(intMonth) += douTime
                            '                Case "RS"
                            '                    douRSH(intMonth) += douTime
                            '                Case "PU"
                            '                    douPumpHrs(intMonth) += douTime
                            '                Case "CO"
                            '                    douSynHrs(intMonth) += douTime
                            '                Case "IR", "MB", "RU"
                            '                    douIH(intMonth) += douTime
                            '            End Select

                            '        Case Else
                            '            douTime = 0.0
                            '    End Select
                            'End If

                        End If

                    End If

                End If

            Next

            Dim intValue As Integer
            Dim intPerfMonth As Int16

            For Each drPerformance In dtPerformance
                ' This loop works for saved performance records including current month since a "Save" was performned when the Batch Check started

                intMonth = Convert.ToInt16(drPerformance.Period) - 1
                dtCheck = System.DateTime.Parse(drPerformance.Period & "/01/" & drPerformance.Year.ToString)

                ' this was required because if the hours were supposed to be xxx.50 and there was the "one second" adjustment, 
                ' it would round down; not the same as xxx.50
                douPOH(intMonth) = System.Math.Round(douPOH(intMonth), 3)
                douMOH(intMonth) = System.Math.Round(douMOH(intMonth), 3)
                douFOH(intMonth) = System.Math.Round(douFOH(intMonth), 3)
                douSEH(intMonth) = System.Math.Round(douSEH(intMonth), 3)
                douRSH(intMonth) = System.Math.Round(douRSH(intMonth), 3)

                'If dtCheck >= System.DateTime.Parse(COPUCUTOFFDATE) Then
                '    ' At ISO-NE these events are only allowed afer 10/01/2006
                '    douPumpHrs(intMonth) = System.Math.Round(douPumpHrs(intMonth), 3)
                '    douSynHrs(intMonth) = System.Math.Round(douSynHrs(intMonth), 3)
                'Else
                douPumpHrs(intMonth) = System.Math.Round(drPerformance.PumpingHours, 3)
                douSynHrs(intMonth) = System.Math.Round(drPerformance.SynchCondHours, 3)
                'End If

                douIH(intMonth) = System.Math.Round(douIH(intMonth), 3)

                Select Case blConnect.IncludedEvents(DirectCast(drSetup, System.Data.DataRow), alSHMethod, dtCheck) ' drSetup.ServiceHourMethod
                    Case 0
                        ' ServiceHourMethod = 0 : All Event data are reported

                        If System.Math.Abs(drPerformance.RSHours - Convert.ToDecimal(System.Math.Round(douRSH(intMonth), intDecPlaces))) > douAccuracy Then

                            ' Issue Error since they are both reported and the error is bigger than allowed
                            strErrors = "Reserve Shutdown hours (" & drPerformance.RSHours.ToString & ") do not match event data hours (" & System.Math.Round(douRSH(intMonth), intDecPlaces).ToString & ")"
                            blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strErrors, "E", drSetup.UnitShortName, GetFromDictionary)

                        End If

                    Case 1
                        ' ServiceHourMethod = 1 : All Event data EXCEPT RS Events are reported

                        If System.Math.Abs(drPerformance.RSHours - Convert.ToDecimal(System.Math.Round(douRSH(intMonth), intDecPlaces))) > douAccuracy Then

                            If drPerformance.RSHours > 0.0 And douRSH(intMonth) > 0.0 Then
                                strWarnings = "Setup indicates that Reserve Shutdowns are NOT Reported, but there are RS Events"
                                blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strWarnings, "W", drSetup.UnitShortName, GetFromDictionary)

                                strWarnings = "Reserve Shutdown hours (" & drPerformance.RSHours.ToString & ") do not match event data hours (" & System.Math.Round(douRSH(intMonth), intDecPlaces).ToString & ")"
                                blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strWarnings, "W", drSetup.UnitShortName, GetFromDictionary)
                            End If

                            douRSH(intMonth) = drPerformance.RSHours

                        End If

                End Select

                ' Planned Outage Hours

                If System.Math.Abs(drPerformance.PlannedOutageHours - Convert.ToDecimal(System.Math.Round(douPOH(intMonth), intDecPlaces))) > douAccuracy Then
                    strErrors = "Planned Outage hours (" & drPerformance.PlannedOutageHours.ToString & ") do not match event data hours (" & System.Math.Round(douPOH(intMonth), intDecPlaces).ToString & ")"
                    blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strErrors, "E", drSetup.UnitShortName, GetFromDictionary)
                End If

                ' Maintenance Outage Hours

                If System.Math.Abs(drPerformance.MaintOutageHours - Convert.ToDecimal(System.Math.Round(douMOH(intMonth), intDecPlaces))) > douAccuracy Then
                    strErrors = "Maintenance Outage hours (" & drPerformance.MaintOutageHours.ToString & ") do not match event data hours (" & System.Math.Round(douMOH(intMonth), intDecPlaces).ToString & ")"
                    blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strErrors, "E", drSetup.UnitShortName, GetFromDictionary)
                End If

                ' Forced Outage Hours

                If System.Math.Abs(drPerformance.ForcedOutageHours - Convert.ToDecimal(System.Math.Round(douFOH(intMonth), intDecPlaces))) > douAccuracy Then
                    strErrors = "Forced Outage hours (" & drPerformance.ForcedOutageHours.ToString & ") do not match event data hours (" & System.Math.Round(douFOH(intMonth), intDecPlaces).ToString & ")"
                    blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strErrors, "E", drSetup.UnitShortName, GetFromDictionary)
                End If

                'If dtCheck >= System.DateTime.Parse(COPUCUTOFFDATE) Then

                '    ' At ISO-NE these events are only allowed afer 10/01/2006

                '    ' Synch Condensing Hours

                '    If System.Math.Abs(drPerformance.SynchCondHours - Convert.ToDecimal(System.Math.Round(douSynHrs(intMonth), intDecPlaces))) > douAccuracy Then
                '        strErrors = "Synch Cond hours (" & drPerformance.SynchCondHours.ToString & ") do not match event data hours (" & System.Math.Round(douSynHrs(intMonth), intDecPlaces).ToString & ")"
                '        blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strErrors, "E", drSetup.UnitShortName, GetFromDictionary)
                '    End If

                '    ' Pumping Hours

                '    If System.Math.Abs(drPerformance.PumpingHours - Convert.ToDecimal(System.Math.Round(douPumpHrs(intMonth), intDecPlaces))) > douAccuracy Then
                '        strErrors = "Pumping hours (" & drPerformance.PumpingHours.ToString & ") do not match event data hours (" & System.Math.Round(douPumpHrs(intMonth), intDecPlaces).ToString & ")"
                '        blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strErrors, "E", drSetup.UnitShortName, GetFromDictionary)
                '    End If

                'End If



                ' =========
                ' SE Hours

                If System.Math.Abs(drPerformance.ExtofSchedOutages - Convert.ToDecimal(System.Math.Round(douSEH(intMonth), intDecPlaces))) > douAccuracy Then
                    strErrors = "Ext of Sch Outage hours (" & drPerformance.ExtofSchedOutages.ToString & ") do not match event data hours (" & System.Math.Round(douSEH(intMonth), intDecPlaces).ToString & ")"
                    blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strErrors, "E", drSetup.UnitShortName, GetFromDictionary)
                End If

                ' =============
                ' Period Hours

                dtRetirementDateTime = DateTime.Parse(drSetup.RetirementDate.ToShortDateString & " 23:59:59")

                If drSetup.CommercialDate >= DateTime.Parse(drPerformance.Period & "/01/" & drPerformance.Year.ToString) Then

                    If drSetup.CommercialDate < DateTime.Parse(drPerformance.Period & "/" & System.DateTime.DaysInMonth(drPerformance.Year, Convert.ToInt16(drPerformance.Period)) & "/" & drPerformance.Year.ToString & " 23:59:59") Then

                        douSH = TimeBetween(drSetup.CommercialDate, DateTime.Parse(drPerformance.Period & "/" & System.DateTime.DaysInMonth(drPerformance.Year, Convert.ToInt16(drPerformance.Period)) & "/" & drPerformance.Year.ToString & " 23:59:59"), drSetup.DaylightSavingTime) _
                         - Convert.ToDouble(System.Math.Round(douRSH(intMonth), intDecPlaces)) _
                         - Convert.ToDouble(System.Math.Round(douPOH(intMonth), intDecPlaces)) _
                         - Convert.ToDouble(System.Math.Round(douMOH(intMonth), intDecPlaces)) _
                         - Convert.ToDouble(System.Math.Round(douFOH(intMonth), intDecPlaces)) _
                         - Convert.ToDouble(System.Math.Round(douSEH(intMonth), intDecPlaces)) _
                         - Convert.ToDouble(System.Math.Round(douPumpHrs(intMonth), intDecPlaces)) _
                         - Convert.ToDouble(System.Math.Round(douSynHrs(intMonth), intDecPlaces)) _
                         - Convert.ToDouble(System.Math.Round(douIH(intMonth), intDecPlaces))
                    Else

                        douSH = 0.0

                    End If

                ElseIf dtRetirementDateTime <= DateTime.Parse(drPerformance.Period & "/" & System.DateTime.DaysInMonth(drPerformance.Year, Convert.ToInt16(drPerformance.Period)) & "/" & drPerformance.Year.ToString & " 23:59:59") Then

                    If dtRetirementDateTime > DateTime.Parse(drPerformance.Period & "/01/" & drPerformance.Year.ToString & " 0:00:00") Then

                        douSH = TimeBetween(DateTime.Parse(drPerformance.Period & "/01/" & drPerformance.Year.ToString & " 0:00:00"), drSetup.RetirementDate, drSetup.DaylightSavingTime) _
                         - Convert.ToDouble(System.Math.Round(douRSH(intMonth), intDecPlaces)) _
                         - Convert.ToDouble(System.Math.Round(douPOH(intMonth), intDecPlaces)) _
                         - Convert.ToDouble(System.Math.Round(douMOH(intMonth), intDecPlaces)) _
                         - Convert.ToDouble(System.Math.Round(douFOH(intMonth), intDecPlaces)) _
                         - Convert.ToDouble(System.Math.Round(douSEH(intMonth), intDecPlaces)) _
                         - Convert.ToDouble(System.Math.Round(douPumpHrs(intMonth), intDecPlaces)) _
                         - Convert.ToDouble(System.Math.Round(douSynHrs(intMonth), intDecPlaces)) _
                         - Convert.ToDouble(System.Math.Round(douIH(intMonth), intDecPlaces))

                    Else

                        douSH = 0.0

                    End If

                Else

                    intValue = System.DateTime.DaysInMonth(drPerformance.Year, Convert.ToInt16(drPerformance.Period)) * 24
                    intPerfMonth = Convert.ToInt16(drPerformance.Period)

                    Select Case drSetup.DaylightSavingTime

                        Case 0

                            ' 0 = No daylight saving time
                            ' plus Japan and China

                        Case 1

                            ' USA and Canada

                            If drPerformance.Year < 2007 Then
                                If intPerfMonth = 4 Then
                                    intValue -= 1
                                ElseIf intPerfMonth = 10 Then
                                    intValue += 1
                                End If
                            Else
                                If intPerfMonth = 3 Then
                                    intValue -= 1
                                ElseIf intPerfMonth = 11 Then
                                    intValue += 1
                                End If
                            End If

                            'Case 2, 3, 4, 5

                            '	' Europe and Russia
                            '	If intPerfMonth = 3 Then
                            '		intValue -= 1
                            '	ElseIf intPerfMonth = 10 Then
                            '		intValue += 1
                            '	End If

                            'Case 6, 7, 8

                            '	' Australia, New Zealand, et al
                            '	If intPerfMonth = 3 Then
                            '		intValue += 1
                            '	ElseIf intPerfMonth = 10 Then
                            '		intValue -= 1
                            '	End If

                            'Case 9

                            '	' Tonga
                            '	If intPerfMonth = 1 Then
                            '		intValue += 1
                            '	ElseIf intPerfMonth = 11 Then
                            '		intValue -= 1
                            '	End If

                    End Select

                    ' ==============================
                    ' Calculate an event-derived SH

                    douSH = Math.Round((Convert.ToDouble(intValue) _
                    - Convert.ToDouble(System.Math.Round(douRSH(intMonth), intDecPlaces)) _
                    - Convert.ToDouble(System.Math.Round(douPOH(intMonth), intDecPlaces)) _
                    - Convert.ToDouble(System.Math.Round(douMOH(intMonth), intDecPlaces)) _
                    - Convert.ToDouble(System.Math.Round(douFOH(intMonth), intDecPlaces)) _
                    - Convert.ToDouble(System.Math.Round(douSEH(intMonth), intDecPlaces)) _
                    - Convert.ToDouble(System.Math.Round(douPumpHrs(intMonth), intDecPlaces)) _
                    - Convert.ToDouble(System.Math.Round(douSynHrs(intMonth), intDecPlaces)) _
                    - Convert.ToDouble(System.Math.Round(douIH(intMonth), intDecPlaces))), intDecPlaces)

                End If

                ' =====================================
                ' Subtract Pumping Hours if applicable

                If drSetup.IsPumpingDataNull Then
                    drSetup.PumpingData = True
                End If

                If drSetup.PumpingData Or douPumpHrs(intMonth) > 0 Then

                    If douPumpHrs(intMonth) > 0 Then
                        drPerformance.PumpingHours = Convert.ToDecimal(douPumpHrs(intMonth))
                    Else
                        douSH -= drPerformance.PumpingHours
                    End If

                End If

                ' ====================================================
                ' Subtract Synchronous Condensing Hours if applicable

                If drSetup.IsSynchCondDataNull Then
                    drSetup.SynchCondData = True
                End If

                If drSetup.SynchCondData Or douSynHrs(intMonth) > 0 Then

                    If douSynHrs(intMonth) > 0 Then
                        drPerformance.SynchCondHours = Convert.ToDecimal(douSynHrs(intMonth))
                    Else
                        douSH -= drPerformance.SynchCondHours
                    End If

                End If

                ' ==============
                ' PJM 025 Check

                'If Not drPerformance.IsActualStartsNull Then

                '    If drSetup.PJM And ((douSH + drPerformance.SynchCondHours) = 0 Or (drPerformance.ServiceHours + drPerformance.SynchCondHours) = 0) And drPerformance.ActualStarts > 0 Then
                '        strErrors = "If Service and Condensing hours are 0, Actual Starts must also be 0 (PJM25)"
                '        blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strErrors, "E", drSetup.UnitShortName, GetFromDictionary)
                '    End If

                'End If

                ' =========================
                ' PJM 027 and PJM 6 Checks

                'If drSetup.PJM And drPerformance.TypUnitLoading = 1 And drPerformance.RSHours > 0 Then
                '    strErrors = "If RS hours are greater than 0, Loading Characteristic cannot be 1 (PJM027/6)"
                '    blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strErrors, "E", drSetup.UnitShortName, GetFromDictionary)
                'End If

                ' =====================================
                ' Generation Checks with Service Hours

                If drSetup.IsGrossNetBothNull Then
                    drSetup.GrossNetBoth = Convert.ToByte(GrossNetOrBoth.Both.ToString)
                End If

                If drSetup.GrossNetBoth = GrossNetOrBoth.Both Then

                    If drPerformance.IsGrossGenNull Then
                        drPerformance.GrossGen = 0
                    End If

                    'If Not drPerformance.IsGrossGenNull Then

                    '	If (drPerformance.GrossGen > 0 And (drPerformance.ServiceHours = 0 Or douSH = 0)) Then
                    '		strWarnings = "If Gross Generation > 0, Service Hours must be > 0"
                    '		blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strWarnings, "W", drSetup.UnitShortName)
                    '	End If

                    '	If (drPerformance.GrossGen = 0 And (drPerformance.ServiceHours > 0 Or douSH > 0)) Then
                    '		strWarnings = "If Service Hours > 0, Gross Generation must be > 0"
                    '		blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strWarnings, "W", drSetup.UnitShortName)
                    '	End If

                    '	If (drPerformance.ServiceHours = drPerformance.PeriodHours Or douSH = Convert.ToDouble(intValue)) And drPerformance.GrossGen = 0 Then
                    '		strWarnings = "If Service Hours = Period Hours, Gross Generation must be > 0"
                    '		blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strWarnings, "W", drSetup.UnitShortName)
                    '	End If

                    'End If

                    If drPerformance.IsNetGenNull Then
                        drPerformance.NetGen = 0
                    End If

                    If Not drPerformance.IsNetGenNull Then

                        If (drPerformance.NetGen > 0 And (drPerformance.ServiceHours = 0 Or douSH = 0)) Then
                            strWarnings = "If Net Generation > 0, Service Hours must be > 0"
                            blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strWarnings, "W", drSetup.UnitShortName, GetFromDictionary)
                        End If

                        If (drPerformance.NetGen = 0 And (drPerformance.ServiceHours > 0 Or douSH > 0)) Then
                            strWarnings = "If Service Hours > 0, Net Generation is probably not 0"
                            blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strWarnings, "W", drSetup.UnitShortName, GetFromDictionary)
                        End If

                        If (drPerformance.ServiceHours = drPerformance.PeriodHours Or douSH = Convert.ToDouble(intValue)) And drPerformance.NetGen = 0 Then
                            strWarnings = "If Service Hours = Period Hours, Net Generation must be > 0"
                            blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strWarnings, "W", drSetup.UnitShortName, GetFromDictionary)
                        End If

                    End If

                ElseIf drSetup.GrossNetBoth = GrossNetOrBoth.GrossOnly Then

                    If drPerformance.IsGrossGenNull Then
                        drPerformance.GrossGen = 0
                    End If

                    'If Not drPerformance.IsGrossGenNull Then

                    '	If (drPerformance.GrossGen > 0 And (drPerformance.ServiceHours = 0 Or douSH = 0)) Then
                    '		strWarnings = "If Gross Generation > 0, Service Hours must be > 0"
                    '		blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strWarnings, "W", drSetup.UnitShortName)
                    '	End If

                    '	If (drPerformance.GrossGen = 0 And (drPerformance.ServiceHours > 0 Or douSH > 0)) Then
                    '		strWarnings = "If Service Hours > 0, Gross Generation must be > 0"
                    '		blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strWarnings, "W", drSetup.UnitShortName)
                    '	End If

                    '	If (drPerformance.ServiceHours = drPerformance.PeriodHours Or douSH = Convert.ToDouble(intValue)) And drPerformance.GrossGen = 0 Then
                    '		strWarnings = "If Service Hours = Period Hours, Gross Generation must be > 0"
                    '		blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strWarnings, "W", drSetup.UnitShortName)
                    '	End If

                    'End If

                ElseIf drSetup.GrossNetBoth = GrossNetOrBoth.NetOnly Then

                    If drPerformance.IsNetGenNull Then
                        drPerformance.NetGen = 0
                    End If

                    If Not drPerformance.IsNetGenNull Then

                        If (drPerformance.NetGen > 0 And (drPerformance.ServiceHours = 0 Or douSH = 0)) Then
                            strWarnings = "If Net Generation > 0, Service Hours must be > 0"
                            blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strWarnings, "W", drSetup.UnitShortName, GetFromDictionary)
                        End If

                        If (drPerformance.NetGen = 0 And (drPerformance.ServiceHours > 0 Or douSH > 0)) Then
                            strWarnings = "If Service Hours > 0, Net Generation is probably not 0"
                            blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strWarnings, "W", drSetup.UnitShortName, GetFromDictionary)
                        End If

                        If (drPerformance.ServiceHours = drPerformance.PeriodHours Or douSH = Convert.ToDouble(intValue)) And drPerformance.NetGen = 0 Then
                            strWarnings = "If Service Hours = Period Hours, Net Generation must be > 0"
                            blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strWarnings, "W", drSetup.UnitShortName, GetFromDictionary)
                        End If

                    End If

                End If

                ' ===================================================================
                ' Do Service hours from Performance 02 card match Event-derived SH ?

                If System.Math.Abs(drPerformance.ServiceHours - Convert.ToDecimal(System.Math.Round(douSH, intDecPlaces))) > douAccuracy Then

                    dtCheck = System.DateTime.Parse(drPerformance.Period & "/01/" & drPerformance.Year.ToString)

                    If blConnect.IncludedEvents(DirectCast(drSetup, System.Data.DataRow), alSHMethod, dtCheck) = 0 Or _
                       blConnect.IncludedEvents(DirectCast(drSetup, System.Data.DataRow), alSHMethod, dtCheck) = 1 Then

                        If drPerformance.RSHours > 0.0 And douRSH(intMonth) = 0.0 Then
                            ' Possibly this unit does not report Reserve Shutdown events -- issue warning only
                            strWarnings = "Service hours (" & drPerformance.ServiceHours.ToString & ") do not match hours derived from event data hours (" & System.Math.Round(douSH, intDecPlaces).ToString & ")"
                            blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strWarnings, "W", drSetup.UnitShortName, GetFromDictionary)
                        Else
                            ' Issue Error since they are both reported and the error is bigger than allowed
                            strErrors = "Service hours (" & drPerformance.ServiceHours.ToString & ") do not match hours derived from event data hours (" & System.Math.Round(douSH, intDecPlaces).ToString & ")"
                            blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strErrors, "E", drSetup.UnitShortName, GetFromDictionary)
                        End If

                    End If

                End If

            Next

        Else
            ' ServiceHourMethod = 2 : Event data is NOT reported
            ' cannot cross-check event and performance 02 record data

            'Dim intValue As Integer
            'Dim intPerfMonth As Int16

            For Each drPerformance In dtPerformance

                intMonth = Convert.ToInt16(drPerformance.Period) - 1

                If drSetup.IsGrossNetBothNull Then
                    drSetup.GrossNetBoth = Convert.ToByte(GrossNetOrBoth.Both.ToString)
                End If

                If drSetup.GrossNetBoth = GrossNetOrBoth.Both Then

                    'If Not drPerformance.IsGrossGenNull Then

                    '	If (drPerformance.GrossGen > 0 And drPerformance.ServiceHours = 0) Then
                    '		strWarnings = "If Gross Generation > 0, Service Hours must be > 0"
                    '		blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strWarnings, "W", drSetup.UnitShortName)
                    '	End If

                    '	If (drPerformance.GrossGen = 0 And drPerformance.ServiceHours > 0) Then
                    '		strWarnings = "If Service Hours > 0, Gross Generation must be > 0"
                    '		blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strWarnings, "W", drSetup.UnitShortName)
                    '	End If

                    '	If (drPerformance.ServiceHours = drPerformance.PeriodHours And drPerformance.GrossGen = 0) Then
                    '		strWarnings = "If Service Hours = Period Hours, Gross Generation must be > 0"
                    '		blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strWarnings, "W", drSetup.UnitShortName)
                    '	End If

                    'End If

                    If Not drPerformance.IsNetGenNull Then

                        If (drPerformance.NetGen > 0 And drPerformance.ServiceHours = 0) Then
                            strWarnings = "If Net Generation > 0, Service Hours must be > 0"
                            blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strWarnings, "W", drSetup.UnitShortName, GetFromDictionary)
                        End If

                        If (drPerformance.NetGen = 0 And drPerformance.ServiceHours > 0) Then
                            strWarnings = "If Service Hours > 0, Net Generation is probably not 0"
                            blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strWarnings, "W", drSetup.UnitShortName, GetFromDictionary)
                        End If

                        If (drPerformance.ServiceHours = drPerformance.PeriodHours And drPerformance.NetGen = 0) Then
                            strWarnings = "If Service Hours = Period Hours, Net Generation must be > 0"
                            blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strWarnings, "W", drSetup.UnitShortName, GetFromDictionary)
                        End If

                    End If

                ElseIf drSetup.GrossNetBoth = GrossNetOrBoth.GrossOnly Then

                    'If Not drPerformance.IsGrossGenNull Then

                    '	If (drPerformance.GrossGen > 0 And drPerformance.ServiceHours = 0) Then
                    '		strWarnings = "If Gross Generation > 0, Service Hours must be > 0"
                    '		blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strWarnings, "W", drSetup.UnitShortName)
                    '	End If

                    '	If (drPerformance.GrossGen = 0 And drPerformance.ServiceHours > 0) Then
                    '		strWarnings = "If Service Hours > 0, Gross Generation must be > 0"
                    '		blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strWarnings, "W", drSetup.UnitShortName)
                    '	End If

                    '	If (drPerformance.ServiceHours = drPerformance.PeriodHours And drPerformance.GrossGen = 0) Then
                    '		strWarnings = "If Service Hours = Period Hours, Gross Generation must be > 0"
                    '		blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strWarnings, "W", drSetup.UnitShortName)
                    '	End If

                    'End If

                ElseIf drSetup.GrossNetBoth = GrossNetOrBoth.NetOnly Then

                    If Not drPerformance.IsNetGenNull Then

                        If (drPerformance.NetGen > 0 And drPerformance.ServiceHours = 0) Then
                            strWarnings = "If Net Generation > 0, Service Hours must be > 0"
                            blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strWarnings, "W", drSetup.UnitShortName, GetFromDictionary)
                        End If

                        If (drPerformance.NetGen = 0 And drPerformance.ServiceHours > 0) Then
                            strWarnings = "If Service Hours > 0, Net Generation is probably not 0"
                            blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strWarnings, "W", drSetup.UnitShortName, GetFromDictionary)
                        End If

                        If (drPerformance.ServiceHours = drPerformance.PeriodHours And drPerformance.NetGen = 0) Then
                            strWarnings = "If Service Hours = Period Hours, Net Generation must be > 0"
                            blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strWarnings, "W", drSetup.UnitShortName, GetFromDictionary)
                        End If

                    End If

                End If

                If drSetup.IsSynchCondDataNull Then
                    drSetup.SynchCondData = True
                End If

                If drPerformance.IsInactiveHoursNull Then
                    drPerformance.InactiveHours = 0
                End If

                If Math.Round(drPerformance.PeriodHours, 2) <> Math.Round((drPerformance.ServiceHours + drPerformance.RSHours + drPerformance.PumpingHours + drPerformance.SynchCondHours + drPerformance.PlannedOutageHours + drPerformance.ForcedOutageHours + drPerformance.MaintOutageHours + drPerformance.ExtofSchedOutages + drPerformance.InactiveHours), 2) Then
                    strErrors = "Available + Unavailable Hours must equal Period Hours"
                    blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strErrors, "E", drSetup.UnitShortName, GetFromDictionary)
                End If

                'If drSetup.PJM And (drPerformance.ServiceHours + drPerformance.SynchCondHours) = 0 And drPerformance.ActualStarts > 0 Then
                '    strErrors = "If Service and Condensing hours are 0, Actual Starts must also be 0 (PJM25)"
                '    blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strErrors, "E", drSetup.UnitShortName, GetFromDictionary)
                'End If

                'If drSetup.PJM And drPerformance.TypUnitLoading = 1 And drPerformance.RSHours > 0 Then
                '    strErrors = "If RS hours are greater than 0, Loading Characteristic cannot be 1 (PJM027/6)"
                '    blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strErrors, "E", drSetup.UnitShortName, GetFromDictionary)
                'End If

            Next

        End If

        ' ----------------------------------------------------------------
        ' Checking Available Capacities against the Dependable Capacities
        ' ----------------------------------------------------------------

        Dim GDC(12) As Decimal
        Dim NDC(12) As Decimal
        'Dim decFuelBtus As Decimal

        For Each drPerformance In dtPerformance

            intMonth = Convert.ToInt16(drPerformance.Period) - 1

            If drPerformance.IsGrossDepCapNull Then
                GDC(intMonth) = 0
            Else
                GDC(intMonth) = drPerformance.GrossDepCap
            End If

            If drPerformance.IsNetDepCapNull Then
                NDC(intMonth) = 0
            Else
                NDC(intMonth) = drPerformance.NetDepCap
            End If

        Next

        Dim vueDeratings As DataView

        vueDeratings = dtEvents01.DefaultView

        vueDeratings.Sort = "StartDateTime, EndDateTime"
        vueDeratings.RowFilter = "EventType IN ('D1', 'D2', 'D3', 'D4', 'PD', 'DE', 'DM', 'DP') AND RevisionCard01 <> 'X'"

        Dim intCounter As Integer
        Dim drvEvent1 As DataRowView
        Dim intMyMonth As Integer

        For intCounter = 0 To vueDeratings.Count - 1

            drvEvent1 = vueDeratings(intCounter)

            If Not Convert.IsDBNull(drvEvent1("StartDateTime")) And Not Convert.IsDBNull(drvEvent1("EventType")) Then

                ' Have a valid Starting Date/time and Event Type

                dtStart = Convert.ToDateTime(drvEvent1("StartDateTime"))

                If Convert.IsDBNull(drvEvent1("EndDateTime")) Then
                    ' This event is open ended use midnight on last day of year
                    dtEnd = Convert.ToDateTime("12/31/" & drPerformance.Year.ToString & " 23:59:59")
                Else
                    dtEnd = Convert.ToDateTime(drvEvent1("EndDateTime"))
                End If

            End If

            'If Not drSetup.IsPJMNull Then
            '	If drSetup.PJM Then
            '		If Not Convert.IsDBNull(drvEvent1("WorkStarted")) Then
            '			If Convert.ToDateTime(drvEvent1("WorkStarted")) <= dtStart Or Convert.ToDateTime(drvEvent1("WorkStarted")) >= dtEnd Then
            '				strErrors = "Work Start Time is outside the event times (PJM116)"
            '				blConnect.LoadErrors(strCurrentUnit, Convert.ToInt16(drvEvent1("Year")), intMyMonth.ToString("00"), Convert.ToInt32(drvEvent1("EventNumber")), 1, strErrors, "E", drSetup.UnitShortName)
            '			End If
            '		End If
            '		If Not Convert.IsDBNull(drvEvent1("WorkEnded")) Then
            '			If Convert.ToDateTime(drvEvent1("WorkEnded")) <= dtStart Or Convert.ToDateTime(drvEvent1("WorkEnded")) >= dtEnd Then
            '				strErrors = "Work End Time is outside the event times (PJM117)"
            '				blConnect.LoadErrors(strCurrentUnit, Convert.ToInt16(drvEvent1("Year")), intMyMonth.ToString("00"), Convert.ToInt32(drvEvent1("EventNumber")), 1, strErrors, "E", drSetup.UnitShortName)
            '			End If
            '		End If
            '	End If
            'End If

            For intMyMonth = dtStart.Month To dtEnd.Month

                'If Not IsDBNull(drvEvent1("GrossAvailCapacity")) Then

                '	If GDC(intMyMonth - 1) > 0 And Convert.ToDecimal(drvEvent1("GrossAvailCapacity").ToString) > GDC(intMyMonth - 1) Then

                '		strErrors = "GAC cannot be greater than GDC for indicated month"
                '		blConnect.LoadErrors(strCurrentUnit, Convert.ToInt16(drvEvent1("Year")), intMyMonth.ToString("00"), Convert.ToInt32(drvEvent1("EventNumber")), 1, strErrors, "E", drSetup.UnitShortName)

                '	End If

                'End If

                If Not IsDBNull(drvEvent1("NetAvailCapacity")) Then

                    If NDC(intMyMonth - 1) > 0 And Convert.ToDecimal(drvEvent1("NetAvailCapacity").ToString) > NDC(intMyMonth - 1) Then

                        strErrors = "NAC cannot be greater than NDC for indicated month"
                        blConnect.LoadErrors(strCurrentUnit, Convert.ToInt16(drvEvent1("Year")), intMyMonth.ToString("00"), Convert.ToInt32(drvEvent1("EventNumber")), 1, strErrors, "E", drSetup.UnitShortName, GetFromDictionary)

                    End If

                End If

            Next

        Next

        vueDeratings.RowFilter = String.Empty

        ' ===================================================
        ' Counting the number of Attempted and Actual Starts 


        ' |---- outage ---|--- outage ---|  |--- outage ---|  |--- outage ---|
        '    |-------------- month ---------------------------------|
        '                          start ^           start ^

        ' |--- outage ---|--- outage ---|--- SF ---|  |--- outage ---|
        '    |-------------- month ---------------------------------|
        '               ATTEMPTED Start ^    start ^

        ' |--- outage ---|--- outage ---|--- SF ---|--- outage ---|
        '    |-------------- month ---------------------------------|
        '               ATTEMPTED Start ^                   start ^

        Dim intAttemptedStarts(12) As Integer
        Dim intActualStarts(12) As Integer

        Dim intHotAttemptedStarts(12) As Integer
        Dim intWarmAttemptedStarts(12) As Integer
        Dim intColdAttemptedStarts(12) As Integer

        Dim intHotActualStarts(12) As Integer
        Dim intWarmActualStarts(12) As Integer
        Dim intColdActualStarts(12) As Integer
        Dim datetimePriorEvent As DateTime
        datetimePriorEvent = Convert.ToDateTime("11/22/1948")

        Dim longInterval As Long     ' time between events in minutes

        Dim vue As DataView

        vue = dtEvents01.DefaultView

        vue.Sort = "StartDateTime, EndDateTime"
        vue.RowFilter = "EventType IN ('U1','U2','U3','SF','PO','MO','SE','RS','PE','ME','IR','MB','RU') AND RevisionCard01 <> 'X'"

        Dim lLastEventWasSF As Boolean
        lLastEventWasSF = False
        Dim intLastSFEventNo As Integer

        If drSetup.IsPJMNull Then
            drSetup.PJM = False
        End If

        If drSetup.IsPJMStartsCountNull Then
            drSetup.PJMStartsCount = True
        End If

        For intCounter = 0 To vue.Count - 1

            drvEvent1 = vue(intCounter)

            If Not Convert.IsDBNull(drvEvent1("StartDateTime")) And Not Convert.IsDBNull(drvEvent1("EventType")) Then

                ' Have a valid Starting Date/time and Event Type

                dtStart = Convert.ToDateTime(drvEvent1("StartDateTime"))

                If Convert.IsDBNull(drvEvent1("ChangeDateTime1")) Or Convert.IsDBNull(drvEvent1("ChangeInEventType1")) Then

                    ' First Change in Event Date/time is null or the first Change in event type is null 
                    ' use End Date/time

                    If Convert.IsDBNull(drvEvent1("EndDateTime")) Then
                        ' This event is open ended use midnight on last day of year
                        dtEnd = Convert.ToDateTime("12/31/" & dtStart.Year.ToString & " 23:59:59")
                    Else
                        dtEnd = Convert.ToDateTime(drvEvent1("EndDateTime"))
                    End If

                    'If Not drSetup.IsPJMNull Then
                    '	If drSetup.PJM Then
                    '		If Not Convert.IsDBNull(drvEvent1("WorkStarted")) Then
                    '			If Convert.ToDateTime(drvEvent1("WorkStarted")) <= dtStart Or Convert.ToDateTime(drvEvent1("WorkStarted")) >= dtEnd Then
                    '				strErrors = "Work Start Time is outside the event times (PJM116)"
                    '				blConnect.LoadErrors(strCurrentUnit, Convert.ToInt16(drvEvent1("Year")), intMyMonth.ToString("00"), Convert.ToInt32(drvEvent1("EventNumber")), 1, strErrors, "E", drSetup.UnitShortName)
                    '			End If
                    '		End If
                    '		If Not Convert.IsDBNull(drvEvent1("WorkEnded")) Then
                    '			If Convert.ToDateTime(drvEvent1("WorkEnded")) <= dtStart Or Convert.ToDateTime(drvEvent1("WorkEnded")) >= dtEnd Then
                    '				strErrors = "Work End Time is outside the event times (PJM117)"
                    '				blConnect.LoadErrors(strCurrentUnit, Convert.ToInt16(drvEvent1("Year")), intMyMonth.ToString("00"), Convert.ToInt32(drvEvent1("EventNumber")), 1, strErrors, "E", drSetup.UnitShortName)
                    '			End If
                    '		End If
                    '	End If
                    'End If

                    dtDate = dtStart

                    If Convert.IsDBNull(drvEvent1("CarryOverLastYear")) Then
                        drvEvent1("CarryOverLastYear") = False
                    End If

                    If Not Convert.ToBoolean(drvEvent1("CarryOverLastYear")) Then

                        If datetimePriorEvent > Convert.ToDateTime("11/22/1948") Then

                            ' There is a prior event

                            If drvEvent1("EventType").ToString = "SF" Then

                                intAttemptedStarts(dtStart.Month - 1) += 1
                                lLastEventWasSF = True
                                intLastSFEventNo = Convert.ToInt32(drvEvent1("EventNumber").ToString)

                                longInterval = DateDiff(DateInterval.Second, datetimePriorEvent, dtStart)

                                If longInterval > 1 Then
                                    strErrors = "SF does not coincide with the End Date of any previous Event or RS"
                                    blConnect.LoadErrors(strCurrentUnit, Convert.ToInt16(drvEvent1("Year")), dtStart.Month.ToString("00"), intLastSFEventNo, 1, strErrors, "E", drSetup.UnitShortName, GetFromDictionary)
                                End If

                            Else

                                longInterval = DateDiff(DateInterval.Second, datetimePriorEvent, dtStart)

                                If longInterval > 61 Then

                                    If lLastEventWasSF Then

                                        lLastEventWasSF = False

                                        If drSetup.PJM Or drSetup.PJMStartsCount Then
                                            'strWarnings = "PJM counts this SF as an ATTEMPTED start"
                                            'blConnect.LoadErrors(strCurrentUnit, Convert.ToInt16(drvEvent1("Year")), datetimePriorEvent.Month.ToString("00"), intLastSFEventNo, 1, strWarnings, "W", drSetup.UnitShortName)
                                            intAttemptedStarts(datetimePriorEvent.Month - 1) += 1
                                            intActualStarts(datetimePriorEvent.Month - 1) += 1
                                        Else
                                            intActualStarts(datetimePriorEvent.Month - 1) += 1
                                        End If

                                    Else
                                        intAttemptedStarts(datetimePriorEvent.Month - 1) += 1
                                        intActualStarts(datetimePriorEvent.Month - 1) += 1
                                    End If

                                ElseIf longInterval = 59 Or longInterval = 60 Or longInterval = 61 Then

                                    If lLastEventWasSF Then

                                        lLastEventWasSF = False

                                        If drSetup.PJM Or drSetup.PJMStartsCount Then
                                            'strWarnings = "PJM counts this SF as an ATTEMPTED start"
                                            'blConnect.LoadErrors(strCurrentUnit, Convert.ToInt16(drvEvent1("Year")), datetimePriorEvent.Month.ToString("00"), intLastSFEventNo, 1, strWarnings, "W", drSetup.UnitShortName)
                                            intAttemptedStarts(datetimePriorEvent.Month - 1) += 1
                                            intActualStarts(datetimePriorEvent.Month - 1) += 1
                                        Else
                                            intActualStarts(datetimePriorEvent.Month - 1) += 1
                                        End If

                                    Else
                                        intAttemptedStarts(datetimePriorEvent.Month - 1) += 1
                                        intActualStarts(datetimePriorEvent.Month - 1) += 1
                                    End If

                                    strWarnings = "1 minute inservice precedes this event -- affects count of Starts"
                                    blConnect.LoadErrors(strCurrentUnit, Convert.ToInt16(drvEvent1("Year")), dtStart.Month.ToString("00"), Convert.ToInt32(drvEvent1("EventNumber")), 1, strWarnings, "W", drSetup.UnitShortName, GetFromDictionary)

                                End If

                                lLastEventWasSF = False

                            End If

                        Else

                            ' There is no prior event -- this is the first one for the year

                            If drvEvent1("EventType").ToString = "SF" Then
                                intAttemptedStarts(dtStart.Month - 1) += 1
                                lLastEventWasSF = True
                                intLastSFEventNo = Convert.ToInt32(drvEvent1("EventNumber").ToString)
                            Else
                                lLastEventWasSF = False
                            End If

                        End If

                    End If

                    datetimePriorEvent = dtEnd

                Else

                    lLastEventWasSF = False

                    If drvEvent1("EventType").ToString.ToUpper = "SF" Then
                        intAttemptedStarts(dtStart.Month - 1) += 1
                    End If

                    If drvEvent1("ChangeInEventType1").ToString.ToUpper = "SF" Then
                        intAttemptedStarts(Convert.ToDateTime(drvEvent1("ChangeDateTime1")).Month - 1) += 1
                        lLastEventWasSF = True
                        intLastSFEventNo = Convert.ToInt32(drvEvent1("EventNumber").ToString)
                    End If

                    If drvEvent1("ChangeInEventType2").ToString.ToUpper = "SF" Then
                        intAttemptedStarts(Convert.ToDateTime(drvEvent1("ChangeDateTime2")).Month - 1) += 1
                        lLastEventWasSF = True
                        intLastSFEventNo = Convert.ToInt32(drvEvent1("EventNumber").ToString)
                    Else
                        Select Case drvEvent1("EventType").ToString.ToUpper
                            Case "U1", "U2", "U3", "PO", "MO", "SE", "RS", "PE", "ME", "IR", "MB", "RU", "CO", "PU"
                                lLastEventWasSF = False
                        End Select

                    End If

                    If Convert.IsDBNull(drvEvent1("EndDateTime")) Then
                        ' This event is open ended use midnight on last day of year
                        dtEnd = Convert.ToDateTime("12/31/" & dtStart.Year.ToString & "  23:59:59")
                    Else
                        dtEnd = Convert.ToDateTime(drvEvent1("EndDateTime"))
                    End If

                    Select Case drvEvent1("EventType").ToString.ToUpper

                        Case "U1", "U2", "U3", "SF", "PO", "MO", "SE", "RS", "PE", "ME", "IR", "MB", "RU"

                            dtDate = dtStart

                            If Not IsNothing(datetimePriorEvent) Then

                                longInterval = DateDiff(DateInterval.Second, datetimePriorEvent, dtStart)

                                If longInterval > 61 Then

                                    If lLastEventWasSF Then

                                        lLastEventWasSF = False

                                        If drSetup.PJM Or drSetup.PJMStartsCount Then
                                            'strWarnings = "PJM counts this SF as an ATTEMPTED start"
                                            'blConnect.LoadErrors(strCurrentUnit, Convert.ToInt16(drvEvent1("Year")), datetimePriorEvent.Month.ToString("00"), intLastSFEventNo, 1, strWarnings, "W", drSetup.UnitShortName)
                                            intAttemptedStarts(datetimePriorEvent.Month - 1) += 1
                                            intActualStarts(datetimePriorEvent.Month - 1) += 1
                                        Else
                                            intActualStarts(datetimePriorEvent.Month - 1) += 1
                                        End If

                                    Else
                                        intAttemptedStarts(datetimePriorEvent.Month - 1) += 1
                                        intActualStarts(datetimePriorEvent.Month - 1) += 1
                                    End If

                                ElseIf longInterval = 0 Or longInterval = 1 Then

                                    If lLastEventWasSF Then

                                        lLastEventWasSF = False

                                        If drSetup.PJM Or drSetup.PJMStartsCount Then
                                            'strWarnings = "PJM counts this SF as an ATTEMPTED start"
                                            'blConnect.LoadErrors(strCurrentUnit, Convert.ToInt16(drvEvent1("Year")), datetimePriorEvent.Month.ToString("00"), intLastSFEventNo, 1, strWarnings, "W", drSetup.UnitShortName)
                                            intAttemptedStarts(datetimePriorEvent.Month - 1) += 1
                                            intActualStarts(datetimePriorEvent.Month - 1) += 1
                                        Else
                                            intActualStarts(datetimePriorEvent.Month - 1) += 1
                                        End If

                                    Else
                                        intAttemptedStarts(datetimePriorEvent.Month - 1) += 1
                                        intActualStarts(datetimePriorEvent.Month - 1) += 1
                                    End If

                                    strWarnings = "1 minute inservice precedes this event -- affects count of Starts"
                                    Try
                                        blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, dtStart.Month.ToString("00"), Convert.ToInt32(drvEvent1("EventNumber")), 1, strWarnings, "W", drSetup.UnitShortName, GetFromDictionary)
                                    Catch ex As Exception
                                        blConnect.LoadErrors(strCurrentUnit, dtStart.Year, dtStart.Month.ToString("00"), Convert.ToInt32(drvEvent1("EventNumber")), 1, strWarnings, "W", drSetup.UnitShortName, GetFromDictionary)
                                    End Try

                                End If

                            End If

                    End Select

                    datetimePriorEvent = dtEnd

                End If

                Select Case drvEvent1("EventType").ToString.ToUpper
                    Case "U1", "U2", "U3", "SF", "PO", "MO", "SE", "NC", "RS", "D1", "D2", "D3", "D4", "PD", "DE", "CO", "PU", "ME", "PE", "DM", "DP"
                    Case "RU", "MB", "IR"
                        'strErrors = strCurrentUnit & " " & drvEvent1("Year").ToString & " " & drvEvent1.Item("EventNumber").ToString & " ERROR: Invalid Event Type"
                        'blConnect.LoadErrors(strCurrentUnit, CInt(drvEvent1.Item("Year")), String.Empty, CInt(drvEvent1.Item("EventNumber")), 1, "Contact ISO for this event type", "E", drSetup.UnitShortName, GetFromDictionary)
                    Case Else
                        strErrors = strCurrentUnit & " " & drvEvent1.Item("Year").ToString & " " & drvEvent1.Item("EventNumber").ToString & " ERROR: Invalid Event Type"
                        blConnect.LoadErrors(strCurrentUnit, CInt(drvEvent1.Item("Year")), String.Empty, CInt(drvEvent1.Item("EventNumber")), 1, "Invalid Event Type", "E", drSetup.UnitShortName, GetFromDictionary)
                End Select

            End If

        Next

        If vue.Count > 0 Then

            If System.DateTime.Compare(dtEnd, Convert.ToDateTime("12/31/" & dtStart.Year.ToString & " 23:59:59")) <> 0 Then

                'If Not lLastEventWasSF Then
                intAttemptedStarts(datetimePriorEvent.Month - 1) += 1
                'End If

                intActualStarts(datetimePriorEvent.Month - 1) += 1

            End If

        End If

        '=============================================================================================================================================================
        ' Validate the Full Outage and RS events

        vue.RowFilter = "EventType IN ('U1','U2','U3','MO','PO','SE','SF','RS','PU','CO','PE','ME','IR','MB','RU') AND RevisionCard01 <> 'X'"

        If vue.Count > 0 Then

            drvEvent1 = vue(0)

            ' This handles all outage types; not MO, PO and SE only
            Dim dtEndOfPrior As DateTime
            'Dim dtStartOfNext As DateTime
            Dim intPriorNumber As Integer
            Dim intPriorCauseCode As Integer
            Dim strPriorEventType As String
            Dim longDiff As Long

            If Convert.IsDBNull(drvEvent1.Item("EndDateTime")) Then
                ' The first full outage has an open ended End date/time
                dtEndOfPrior = blConnect.EOM(dtNow)      ' do not convert to UtcNow
            Else
                dtEndOfPrior = Convert.ToDateTime("01/01/1982")
            End If

            intPriorNumber = Convert.ToInt32(drvEvent1.Item("EventNumber"))
            strPriorEventType = drvEvent1.Item("EventType").ToString
            intPriorCauseCode = Convert.ToInt32(drvEvent1.Item("CauseCode"))

            For intCounter = 0 To vue.Count - 1

                drvEvent1 = vue(intCounter)

                If Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Month = 1 And Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Day = 1 And _
                   Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Hour = 0 And Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Minute <= 1 Then

                    ' Looking for a one-minute gap between the first event for the year and the last event for prior year

                    Dim strSQLSelectPriorYear As String
                    Dim dsChecking As DataSet

                    strSQLSelectPriorYear = "SELECT * FROM EventData01 WHERE UtilityUnitCode = '" & drvEvent1.Item("UtilityUnitCode").ToString & "'"

                    If blConnect.ThisProvider = "Oracle" Then

                        If Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Minute = 0 Then

                            'strSQLSelectPriorYear += " AND EndDateTime = '" & System.DateTime.Parse("12/31/" & (Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Year - 1).ToString & " 23:59:00") & "'"
                            strSQLSelectPriorYear += " AND EndDateTime = to_date ('" & System.DateTime.Parse("12/31/" & (Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Year - 1).ToString & " 23:59:00").ToString("MM/dd/yyyy HH:mm:ss") & "', 'mm/dd/yyyy HH24:MI:SS')"

                        ElseIf Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Minute = 1 Then

                            'strSQLSelectPriorYear += " AND EndDateTime = '" & System.DateTime.Parse("12/31/" & (Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Year - 1).ToString & " 23:59:59") & "'"
                            strSQLSelectPriorYear += " AND EndDateTime = to_date ('" & System.DateTime.Parse("12/31/" & (Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Year - 1).ToString & " 23:59:59").ToString("MM/dd/yyyy HH:mm:ss") & "', 'mm/dd/yyyy HH24:MI:SS')"

                        End If

                    Else

                        If Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Minute = 0 Then

                            strSQLSelectPriorYear += " AND EndDateTime = '" & System.DateTime.Parse("12/31/" & (Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Year - 1).ToString & " 23:59:00") & "'"

                        ElseIf Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Minute = 1 Then

                            strSQLSelectPriorYear += " AND EndDateTime = '" & System.DateTime.Parse("12/31/" & (Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Year - 1).ToString & " 23:59:59") & "'"

                        End If

                    End If

                    strSQLSelectPriorYear += " AND EventType IN ('U1','U2','U3','MO','PO','SE','SF','RS','PU','CO','PE','ME','IR','MB','RU') AND RevisionCard01 <> 'X'"

                    dsChecking = blConnect.GetADataset(strSQLSelectPriorYear, Nothing)

                    If Not IsNothing(dsChecking) Then
                        If dsChecking.Tables(0).Rows.Count > 0 Then
                            strWarnings = "1 minute gap between this event and in last event in " & (Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Year - 1).ToString
                            blConnect.LoadErrors(strCurrentUnit, Convert.ToInt16(drvEvent1("Year")), Nothing, Convert.ToInt32(drvEvent1("EventNumber")), 1, strWarnings, "W", drSetup.UnitShortName, GetFromDictionary)
                        End If
                    End If

                End If

                ' Checking for SE following a PO, MO or SE

                longDiff = DateDiff(DateInterval.Second, dtEndOfPrior, Convert.ToDateTime(drvEvent1.Item("StartDateTime")))

                If vue.Count > 1 And longDiff <= 61 And (drvEvent1.Item("EventType").ToString <> "SE" And drvEvent1.Item("EventType").ToString <> "ME" And drvEvent1.Item("EventType").ToString <> "PE") Then

                    If longDiff = 0 Then
                        If Convert.IsDBNull(drvEvent1.Item("EventType")) = False Then
                            If Not ETChange(strPriorEventType, drvEvent1.Item("EventType").ToString) Then
                                lHasErrors = True
                                strErrors = "Transition from " & strPriorEventType & " (" & intPriorNumber.ToString & ") to " & _
                                   drvEvent1.Item("EventType").ToString & " (" & drvEvent1.Item("EventNumber").ToString & ") is not allowed"
                                blConnect.LoadErrors(strCurrentUnit, Convert.ToInt16(drvEvent1("Year")), intMyMonth.ToString("00"), Convert.ToInt32(drvEvent1("EventNumber")), 1, strErrors, "E", drSetup.UnitShortName, GetFromDictionary)
                            End If
                        End If
                    ElseIf longDiff >= 59 Then
                        If Convert.IsDBNull(drvEvent1.Item("EventType")) = False Then
                            If Not ETChange(strPriorEventType, drvEvent1.Item("EventType").ToString) Then
                                lHasErrors = True
                                strErrors = "Transition from " & strPriorEventType & " (" & intPriorNumber.ToString & ") to " & _
                                   drvEvent1.Item("EventType").ToString & " (" & drvEvent1.Item("EventNumber").ToString & ") is not allowed"
                                blConnect.LoadErrors(strCurrentUnit, Convert.ToInt16(drvEvent1("Year")), intMyMonth.ToString("00"), Convert.ToInt32(drvEvent1("EventNumber")), 1, strErrors, "E", drSetup.UnitShortName, GetFromDictionary)
                            Else
                                strWarnings = "1 minute gap between this event and event # " & intPriorNumber.ToString
                                'blConnect.LoadErrors(strCurrentUnit, Convert.ToInt16(drvEvent1("Year")), intMyMonth.ToString("00"), Convert.ToInt32(drvEvent1("EventNumber")), 1, strErrors, "E", drSetup.UnitShortName, GetFromDictionary)
                            End If
                        End If
                        'ElseIf longDiff = 1 Then
                        '    If dtEndOfPrior.Hour = 23 And dtEndOfPrior.Minute = 59 And dtEndOfPrior.Second = 59 And _
                        '        Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Hour = 0 And _
                        '        Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Minute = 0 And _
                        '        Convert.ToDateTime(drvEvent1.Item("StartDateTime")).Second = 0 And _
                        '        ETChange(strPriorEventType, drvEvent1.Item("EventType").ToString) Then
                        '        lHasErrors = True
                        '        strErrors = "End date/time and Start date/time are not exactly the same: " & strPriorEventType & " (" & intPriorNumber.ToString & ") to " & _
                        '            drvEvent1.Item("EventType").ToString & " (" & drvEvent1.Item("EventNumber").ToString & ")"
                        '        blConnect.LoadErrors(strCurrentUnit, Convert.ToInt16(drvEvent1("Year")), intMyMonth.ToString("00"), Convert.ToInt32(drvEvent1("EventNumber")), 1, strErrors, "E", drSetup.UnitShortName, GetFromDictionary)
                        '    ElseIf Not ETChange(strPriorEventType, drvEvent1.Item("EventType").ToString) Then
                        '        lHasErrors = True
                        '        strErrors = "Transition from " & strPriorEventType & " (" & intPriorNumber.ToString & ") to " & _
                        '            drvEvent1.Item("EventType").ToString & " (" & drvEvent1.Item("EventNumber").ToString & ") is not allowed"
                        '        blConnect.LoadErrors(strCurrentUnit, Convert.ToInt16(drvEvent1("Year")), intMyMonth.ToString("00"), Convert.ToInt32(drvEvent1("EventNumber")), 1, strErrors, "E", drSetup.UnitShortName, GetFromDictionary)
                        '    End If
                    ElseIf longDiff > 1 Then
                        lHasErrors = True
                        strErrors = "Unexpected event transition error: " & strPriorEventType & " (" & intPriorNumber.ToString & ") to " & _
                          drvEvent1.Item("EventType").ToString & " (" & drvEvent1.Item("EventNumber").ToString & ") is not allowed"
                        blConnect.LoadErrors(strCurrentUnit, Convert.ToInt16(drvEvent1("Year")), intMyMonth.ToString("00"), Convert.ToInt32(drvEvent1("EventNumber")), 1, strErrors, "E", drSetup.UnitShortName, GetFromDictionary)
                    ElseIf longDiff < 0 Then
                        lHasErrors = True
                        If intPriorNumber.ToString.Trim = drvEvent1.Item("EventNumber").ToString.Trim Then
                            strErrors = "Overlapping Events: " & _
                             drvEvent1.Item("EventType").ToString & " (" & drvEvent1.Item("EventNumber").ToString & ") is overlapped by another event and this is not allowed"
                            blConnect.LoadErrors(strCurrentUnit, Convert.ToInt16(drvEvent1("Year")), Nothing, Convert.ToInt32(drvEvent1("EventNumber")), 1, strErrors, "E", drSetup.UnitShortName, GetFromDictionary)
                        Else
                            strErrors = "Overlapping Events: " & strPriorEventType & " (" & intPriorNumber.ToString & ") and " & _
                             drvEvent1.Item("EventType").ToString & " (" & drvEvent1.Item("EventNumber").ToString & ") is not allowed"
                            blConnect.LoadErrors(strCurrentUnit, Convert.ToInt16(drvEvent1("Year")), Nothing, Convert.ToInt32(drvEvent1("EventNumber")), 1, strErrors, "E", drSetup.UnitShortName, GetFromDictionary)
                        End If
                    End If

                End If

                intPriorNumber = Convert.ToInt32(drvEvent1.Item("EventNumber"))
                strPriorEventType = drvEvent1.Item("EventType").ToString
                intPriorCauseCode = Convert.ToInt32(drvEvent1.Item("CauseCode"))
                If Convert.IsDBNull(drvEvent1.Item("EndDateTime")) Then
                    ' The first full outage has an open ended End date/time
                    dtEndOfPrior = blConnect.EOM(dtNow)     ' do not convert to UtcNow
                Else
                    dtEndOfPrior = Convert.ToDateTime(drvEvent1.Item("EndDateTime"))
                End If

            Next

        End If


        dtCheck = dtNow

        For Each drPerformance In dtPerformance

            dtCheck = System.DateTime.Parse(drPerformance.Period & "/01/" & drPerformance.Year.ToString)

            If blConnect.IncludedEvents(DirectCast(drSetup, System.Data.DataRow), alSHMethod, dtCheck) = 0 Then ' drSetup.ServiceHourMethod = 0 Then
                ' This loop works for saved performance records including current month since a "Save" was performned when the Batch Check started

                intMonth = Convert.ToInt16(drPerformance.Period) - 1

                If drPerformance.IsActualStartsNull Then
                    drPerformance.ActualStarts = 0
                End If

                If drPerformance.IsAttemptedStartsNull Then
                    drPerformance.AttemptedStarts = 0
                End If

                ' 2006 NERC GADS changes ... set these back to "W"
                If drPerformance.ActualStarts <> intActualStarts(intMonth) Then
                    Dim strErr As String = "Actual Starts (" & drPerformance.ActualStarts.ToString & ") do not match event derived starts data (" & intActualStarts(intMonth).ToString & ")"
                    blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strErr, "W", drSetup.UnitShortName, GetFromDictionary)
                End If

                If drPerformance.AttemptedStarts <> intAttemptedStarts(intMonth) Then
                    Dim strErr As String = "Attempted Starts (" & drPerformance.AttemptedStarts.ToString & ") do not match event derived starts data (" & intAttemptedStarts(intMonth).ToString & ")"
                    blConnect.LoadErrors(strCurrentUnit, drPerformance.Year, drPerformance.Period, Nothing, Nothing, strErr, "W", drSetup.UnitShortName, GetFromDictionary)
                End If

            End If

        Next

        vue.RowFilter = String.Empty

        If strErrors.Trim <> String.Empty Then
            blConnect.UpdateErrorCheckStatus(strCurrentUnit, "E")
        Else
            blConnect.UpdateErrorCheckStatus(strCurrentUnit, "O")
        End If

        Return strErrors

    End Function

#End Region

#Region " DoWebCalcs(blConnect, dtSetup) "

    'Private Sub DoWebCalcs(ByVal blConnect As Calcs, ByVal dtSetup As ARWindowsUI.SetupDE.SetupDataTable, ByVal myUser As String)

    '    'Dim sScript As String
    '    Dim SQLProcessedCommand As String
    '    'Dim SqlString As String
    '    Dim intCount As Integer
    '    Dim intYearCount As Integer
    '    'Dim intGranularity As Integer
    '    'Dim dsGroups As DataSet
    '    'Dim strEX As String
    '    Dim dtCCGrp As ARWindowsUI.CauseCodeGroups.CauseCodeGroupsDataTable
    '    'Dim intValue As Integer

    '    intCount = blConnect.GetPerfCount()

    '    If intCount = 0 Then
    '        'Me.lblStatus.Text = "No Performance data in Analysis & Reporting tables" & vbCrLf & vbCrLf & "Data needs to be loaded from Data Entry tables"
    '        Exit Sub
    '    End If

    '    dtCCGrp = blConnect.GetCCGrpData()

    '    'Dim dtSetupDE As SetupDE.SetupDataTable
    '    'Dim drSetupDE As SetupDE.SetupRow
    '    Dim intYear As Integer = System.DateTime.Now.Year

    '    'dtSetupDE = GetSetupDEData()
    '    intCount = dtSetup.Rows.Count

    '    Dim aIntYears As New ArrayList
    '    Dim strBase As String = String.Empty
    '    Dim dtTimeMast As ARWindowsUI.TimeMast.TimeMastDataTable = New ARWindowsUI.TimeMast.TimeMastDataTable
    '    Dim intMaxMonthP As Integer
    '    Dim intMinMonthP As Integer

    '    Dim strMaxPeriod As String
    '    Dim strMinPeriod As String
    '    'Dim objTest As Object
    '    Dim dtMaxPerf As DateTime = DateTime.Now
    '    Dim dtMinPerf As DateTime = DateTime.Now

    '    Dim stringSelect As String
    '    Dim parmsMax(2, 2) As String
    '    Dim myObj As Object
    '    Dim intTempOMC As Integer = 4
    '    Dim lDidPerformanceRecords As Boolean = False
    '    Dim lDidEventRecords As Boolean = False

    '    Dim drSetupDE As ARWindowsUI.SetupDE.SetupRow
    '    'Dim boolPlantMgtControl As Boolean = True
    '    For Each drSetupDE In dtSetup

    '        blConnect.mydrSetupDE = drSetupDE

    '        If drSetupDE.IsMonthlyNull Then
    '            drSetupDE.Monthly = True
    '        End If

    '        ' aIntYears contains the list of years that have changed or new data for this unit
    '        aIntYears = blConnect.UnprocessedYears(drSetupDE.UtilityUnitCode)

    '        '???? make one open date at .now and compare to the retirement date for the unit
    '        ' try doing a max year on performance then max period -- if Qn then convert to month (n x 3)
    '        ' apply it to all open-ended events

    '        If Not IsNothing(aIntYears) Then
    '            ' the aIntYears is nothing for some reason

    '            If aIntYears.Count > 0 Then
    '                ' this unit has changed data for these years
    '                For i As Int16 = 0 To 1
    '                    If i = 0 Then
    '                        ' All Cause Codes
    '                        'boolPlantMgtControl = False
    '                        blConnect.intOMC = 4
    '                    Else
    '                        ' Calculate only those things that are under plant management's control
    '                        'boolPlantMgtControl = True
    '                        blConnect.intOMC = 2
    '                    End If

    '                    For intYearCount = 0 To (aIntYears.Count - 1)

    '                        ' for each year generate the following Granularity if set in Setup table

    '                        lDidPerformanceRecords = False
    '                        lDidEventRecords = False

    '                        intYear = Convert.ToInt32(aIntYears(intYearCount))

    '                        strBase = drSetupDE.UnitShortName.PadRight(10) & " | " & intYear.ToString

    '                        parmsMax(0, 0) = "@UtilityUnitCode"
    '                        parmsMax(0, 1) = "UtilityUnitCode, " + drSetupDE.UtilityUnitCode

    '                        parmsMax(1, 0) = "@Year"
    '                        parmsMax(1, 1) = "Year, " + intYear.ToString

    '                        stringSelect = "SELECT MAX(Period) FROM PerformanceDataAR WHERE (UtilityUnitCode = @UtilityUnitCode) AND (Year = @Year)"

    '                        myObj = blConnect.objExecuteScalar(stringSelect, parmsMax)

    '                        If IsDBNull(myObj) Then
    '                            ' this was a situation when loading from 82-column data that there was event data
    '                            ' but no performance data for a particular year so when it went to find the max
    '                            ' period for a nonexistent year it returned a dbnull value when could not be
    '                            ' converted to a CStr(myObj)
    '                            Exit For
    '                        End If

    '                        strMaxPeriod = CStr(myObj)

    '                        ' strMinPeriod will be one of two formats:  01-12 or X1-X4 since this is coming from Performance Data

    '                        If strMaxPeriod.StartsWith("X") = True Then
    '                            intMaxMonthP = 3 * (CInt(strMaxPeriod.Substring(1)) - 1) + 1
    '                        Else
    '                            intMaxMonthP = (CInt(strMaxPeriod))
    '                        End If

    '                        dtMaxPerf = blConnect.EOM(Date.Parse(intMaxMonthP.ToString() + "/01/" + intYear.ToString))

    '                        ' ------------------------------------

    '                        stringSelect = "SELECT MIN(Period) FROM PerformanceDataAR WHERE (UtilityUnitCode = @UtilityUnitCode) AND (Year = @Year)"

    '                        myObj = blConnect.objExecuteScalar(stringSelect, parmsMax)

    '                        If IsDBNull(myObj) Then
    '                            ' this was a situation when loading from 82-column data that there was event data
    '                            ' but no performance data for a particular year so when it went to find the max
    '                            ' period for a nonexistent year it returned a dbnull value when could not be
    '                            ' converted to a CStr(myObj)
    '                            Exit For
    '                        End If

    '                        strMinPeriod = CStr(myObj)

    '                        ' strMinPeriod will be one of two formats:  01-12 or X1-X4 since this is coming from Performance Data

    '                        If strMinPeriod.StartsWith("X") = True Then
    '                            intMinMonthP = 3 * (CInt(strMinPeriod.Substring(1)) - 1) + 1
    '                        Else
    '                            intMinMonthP = (CInt(strMinPeriod))
    '                        End If

    '                        dtMinPerf = Date.Parse(intMinMonthP.ToString() + "/01/" + intYear.ToString & " 00:00:00")

    '                        Try
    '                            If Not IsNothing(dtTimeMast) Then
    '                                dtTimeMast.Clear()
    '                            End If

    '                        Catch ex As Exception

    '                        End Try

    '                        ' get the TimeLine datatable
    '                        dtTimeMast = blConnect.CreateTimeLine(drSetupDE.UtilityUnitCode, _
    '                         dtMinPerf, _
    '                         dtMaxPerf)

    '                        'TODO must do both calc runs
    '                        ' Monthly

    '                        blConnect.Calcs(drSetupDE, _
    '                         dtMinPerf, _
    '                         dtMaxPerf, _
    '                         dtTimeMast, _
    '                         Calcs.Granularity.Monthly)

    '                        lDidPerformanceRecords = True
    '                        lDidEventRecords = True


    '                        ' Set the flag to show that this year's data has been processed

    '                        SQLProcessedCommand = "UPDATE PerformanceDataAR SET Processed = 1 WHERE (Year = " & intYear.ToString & ") AND (UtilityUnitCode = '" & drSetupDE.UtilityUnitCode & "')"
    '                        intCount = blConnect.intExecuteNonQuery(SQLProcessedCommand, Nothing)

    '                        SQLProcessedCommand = "UPDATE EventData01AR SET Processed = 1 WHERE (Year = " & intYear.ToString & ") AND (UtilityUnitCode = '" & drSetupDE.UtilityUnitCode & "')"
    '                        intCount = blConnect.intExecuteNonQuery(SQLProcessedCommand, Nothing)

    '                        SQLProcessedCommand = "UPDATE EventData02AR SET Processed = 1 WHERE (Year = " & intYear.ToString & ") AND (UtilityUnitCode = '" & drSetupDE.UtilityUnitCode & "')"
    '                        intCount = blConnect.intExecuteNonQuery(SQLProcessedCommand, Nothing)


    '                    Next

    '                Next

    '            End If

    '        End If

    '    Next

    '    ' ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    '    '   AT THIS POINT ALL OF THE UNIT DATA HAS BEEN RECALCULATED
    '    ' ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    '    ' ***********************************************
    '    '   Calculate the groups found in ARGroups 
    '    ' ***********************************************

    '    'Dim GroupIDReader As IDataReader
    '    'Dim GroupIDArray As New ArrayList

    '    'GroupIDReader = blConnect.GetUserGroupsReader(myUser)
    '    'While GroupIDReader.Read()
    '    '    GroupIDArray.Add(CInt(GroupIDReader.GetValue(0)))
    '    'End While
    '    '' always call Close when done reading
    '    'GroupIDReader.Close()

    '    'Dim intGroupCount As Integer

    '    'dsGroups = blConnect.GetGroups()
    '    'intGroupCount = dsGroups.Tables(0).Rows.Count

    '    'Dim dtSumEventHours As ARWindowsUI.EventHours.EventHoursDataTable

    '    'Dim drGroups As DataRow
    '    ''Dim strTemp As String
    '    'Dim drSumEventHours As ARWindowsUI.EventHours.EventHoursRow
    '    'boolPlantMgtControl = True
    '    'Dim IsMyGroup As Boolean = False

    '    'For i As Int16 = 0 To 1 ' ISO NE does ALL cause codes only

    '    '    If i = 0 Then
    '    '        ' All CC
    '    '        boolPlantMgtControl = False
    '    '    Else
    '    '        ' Plant Mgt
    '    '        boolPlantMgtControl = True
    '    '    End If

    '    '    For Each drGroups In dsGroups.Tables(0).Rows

    '    '        IsMyGroup = False

    '    '        For intValue = 0 To (GroupIDArray.Count - 1)
    '    '            If CInt(GroupIDArray.Item(intValue)) = CInt(drGroups.Item("GroupID")) Then
    '    '                IsMyGroup = True
    '    '                Exit For
    '    '            End If
    '    '        Next

    '    '        If IsMyGroup Then

    '    '            If IsDBNull(drGroups.Item("GroupName")) Then
    '    '                strBase = "Unknown".PadRight(10)
    '    '            Else
    '    '                strBase = drGroups.Item("GroupShortName").ToString.Trim.PadRight(10)
    '    '            End If

    '    '            If IsNothing(boolPlantMgtControl) Then
    '    '                boolPlantMgtControl = False
    '    '            End If

    '    '            dtSumEventHours = blConnect.GetdtSumEventHours(Convert.ToInt32(drGroups.Item("GroupID")), boolPlantMgtControl)
    '    '            intCount = dtSumEventHours.Rows.Count

    '    '            If intCount > 0 Then

    '    '                For Each drSumEventHours In dtSumEventHours.Rows

    '    '                    drSumEventHours.UnitShortName = drGroups.Item("GroupShortName").ToString.Trim
    '    '                    drSumEventHours.PlantMgtControl = boolPlantMgtControl

    '    '                Next


    '    '                Try

    '    '                    blConnect.PerformanceIndexes_Calc_Wtd(dtSumEventHours, blConnect.boolWeighted)

    '    '                    dtSumEventHours.AcceptChanges()

    '    '                    ' load calculated data to master tables

    '    '                    blConnect.LoadSumEventHours(drGroups.Item("GroupShortName").ToString.Trim, dtSumEventHours)

    '    '                Catch ex As Exception

    '    '                    Microsoft.ApplicationBlocks.ExceptionManagement.ExceptionManager.Publish(ex)

    '    '                End Try

    '    '            End If

    '    '            dtSumEventHours.Rows.Clear()

    '    '        End If

    '    '    Next

    '    'Next

    '    'Dim dtSumPerformanceRecords As ARWindowsUI.PerformanceRecords.PerformanceRecordsDataTable
    '    'Dim drSumPerformanceRecords As ARWindowsUI.PerformanceRecords.PerformanceRecordsRow
    '    'Dim dtEFORdTotal As ARWindowsUI.EFORd.EFORdDataTable
    '    ''Dim drEFORd As EFORd.EFORdRow
    '    'Dim myColumn As DataColumn

    '    'For Each drGroups In dsGroups.Tables(0).Rows

    '    '    IsMyGroup = False

    '    '    For intValue = 0 To (GroupIDArray.Count - 1)
    '    '        If CInt(GroupIDArray.Item(intValue)) = CInt(drGroups.Item("GroupID")) Then
    '    '            IsMyGroup = True
    '    '            Exit For
    '    '        End If
    '    '    Next

    '    '    If IsMyGroup Then

    '    '        If IsDBNull(drGroups.Item("GroupName")) Then
    '    '            strBase = "Unknown".PadRight(10)
    '    '        Else
    '    '            strBase = drGroups.Item("GroupShortName").ToString.Trim.PadRight(10)
    '    '        End If

    '    '        dtSumPerformanceRecords = blConnect.GetdtSumPerformanceRecords(Convert.ToInt32(drGroups.Item("GroupID")))
    '    '        intCount = dtSumPerformanceRecords.Rows.Count

    '    '        If intCount > 0 Then

    '    '            For Each drSumPerformanceRecords In dtSumPerformanceRecords.Rows

    '    '                drSumPerformanceRecords.UnitShortName = drGroups.Item("GroupShortName").ToString.Trim
    '    '                drSumPerformanceRecords.lQuarterlyData = False
    '    '                drSumPerformanceRecords.UtilityUnitCode = drGroups.Item("GroupID").ToString().PadLeft(6)

    '    '                If Not drSumPerformanceRecords.IsActualStartsNull Then
    '    '                    If Not drSumPerformanceRecords.IsAttemptedStartsNull Then
    '    '                        If drSumPerformanceRecords.AttemptedStarts > 0 And drSumPerformanceRecords.ActualStarts >= 0 Then
    '    '                            drSumPerformanceRecords.StartingReliability = Math.Round(Convert.ToDecimal((100.0 * drSumPerformanceRecords.ActualStarts) / drSumPerformanceRecords.AttemptedStarts), 3)
    '    '                        End If
    '    '                    End If
    '    '                End If

    '    '                If Not drSumPerformanceRecords.IsNetGenNull And Not drSumPerformanceRecords.IsN_ServiceHoursNull Then

    '    '                    If drSumPerformanceRecords.NetGen >= 0 And drSumPerformanceRecords.N_ServiceHours > 0 Then
    '    '                        Try
    '    '                            drSumPerformanceRecords.NOF = Math.Round(((100.0 * drSumPerformanceRecords.NetGen) / drSumPerformanceRecords.N_ServiceHours), 3)
    '    '                        Catch ex As System.Exception
    '    '                            strEX = ex.Message
    '    '                        End Try
    '    '                    End If

    '    '                End If

    '    '                If Not drSumPerformanceRecords.IsNetGenNull And Not drSumPerformanceRecords.IsN_PeriodHoursNull Then

    '    '                    If drSumPerformanceRecords.NetGen >= 0 And drSumPerformanceRecords.N_PeriodHours > 0 Then
    '    '                        Try
    '    '                            drSumPerformanceRecords.NCF = Math.Round(((100.0 * drSumPerformanceRecords.NetGen) / drSumPerformanceRecords.N_PeriodHours), 3)
    '    '                        Catch ex As System.Exception
    '    '                            strEX = ex.Message
    '    '                        End Try
    '    '                    End If

    '    '                End If

    '    '                If Not drSumPerformanceRecords.IsGrossGenNull And Not drSumPerformanceRecords.IsG_ServiceHoursNull Then

    '    '                    If drSumPerformanceRecords.GrossGen >= 0 And drSumPerformanceRecords.G_ServiceHours > 0 Then
    '    '                        Try
    '    '                            drSumPerformanceRecords.GOF = Math.Round(((100.0 * drSumPerformanceRecords.GrossGen) / drSumPerformanceRecords.G_ServiceHours), 3)
    '    '                        Catch ex As Exception
    '    '                            strEX = ex.Message
    '    '                        End Try
    '    '                    End If

    '    '                End If

    '    '                If Not drSumPerformanceRecords.IsGrossGenNull And Not drSumPerformanceRecords.IsG_PeriodHoursNull Then

    '    '                    If drSumPerformanceRecords.GrossGen >= 0 And drSumPerformanceRecords.G_PeriodHours > 0 Then
    '    '                        Try
    '    '                            drSumPerformanceRecords.GCF = Math.Round(((100.0 * drSumPerformanceRecords.GrossGen) / drSumPerformanceRecords.G_PeriodHours), 3)
    '    '                        Catch ex As Exception
    '    '                            strEX = ex.Message
    '    '                        End Try
    '    '                    End If

    '    '                End If

    '    '                If Not drSumPerformanceRecords.IsTotalBtusNull Then

    '    '                    If Not drSumPerformanceRecords.IsNetGenNull Then
    '    '                        If drSumPerformanceRecords.NetGen > 0 Then
    '    '                            Try
    '    '                                drSumPerformanceRecords.NHR = Math.Round((drSumPerformanceRecords.TotalBtus / (1000.0 * drSumPerformanceRecords.NetGen)), 3)
    '    '                            Catch ex As Exception
    '    '                                strEX = ex.Message
    '    '                            End Try
    '    '                        End If
    '    '                    End If

    '    '                    If Not drSumPerformanceRecords.IsGrossGenNull Then
    '    '                        If drSumPerformanceRecords.GrossGen > 0 Then
    '    '                            Try
    '    '                                drSumPerformanceRecords.GHR = Math.Round((drSumPerformanceRecords.TotalBtus / (1000.0 * drSumPerformanceRecords.GrossGen)), 3)
    '    '                            Catch ex As Exception
    '    '                                strEX = ex.Message
    '    '                            End Try
    '    '                        End If
    '    '                    End If
    '    '                End If

    '    '            Next

    '    '            Try

    '    '                dtSumPerformanceRecords.AcceptChanges()

    '    '                ' load calculated data to master tables

    '    '                blConnect.LoadSumPerformanceRecords(drGroups.Item("GroupShortName").ToString.Trim, dtSumPerformanceRecords)

    '    '            Catch ex As Exception

    '    '                Microsoft.ApplicationBlocks.ExceptionManagement.ExceptionManager.Publish(ex)

    '    '            Finally


    '    '            End Try

    '    '        End If

    '    '        dtSumPerformanceRecords.Rows.Clear()

    '    '        ' Calculate this group's EFORd and FORd values

    '    '        dtEFORdTotal = blConnect.GetEFORdTotalData(drGroups.Item("GroupID").ToString.Trim)

    '    '        ' this do each below calculates the groups the same way as the units per the ISO-NE specs

    '    '        For Each myColumn In dtEFORdTotal.Columns
    '    '            myColumn.ReadOnly = False
    '    '        Next

    '    '        blConnect.LoadGroupEFORd(drGroups.Item("GroupShortName").ToString.Trim, dtEFORdTotal)

    '    '        dtEFORdTotal.Rows.Clear()

    '    '    End If

    '    'Next

    'End Sub

#End Region

End Module
