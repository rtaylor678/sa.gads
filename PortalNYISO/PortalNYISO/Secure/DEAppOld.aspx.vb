﻿'Imports Microsoft.DSG.Security.CryptoServices
'Imports BusinessLayer.BusinessLayer
'Imports SetupSettings.Settings
'Imports Microsoft.ApplicationBlocks.ExceptionManagement
'Imports System
'Imports System.Globalization
'Imports System.IO
'Imports System.Web.HttpRequest
'Imports System.Security.Principal
'Imports Microsoft.VisualBasic
'Imports System.Threading
'Imports PortalNYISO.WebUtilityModule
''Imports Microsoft.Web.UI.WebControls
'Imports PortalNYISO.Controls
'Imports PortalNYISO.Process

'Imports System.Collections.Specialized
'Imports LanguageBuilder

Public Class DEAppOld
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub


    Protected Sub btnEventData_Click(sender As Object, e As EventArgs) Handles btnEventData.Click
        MultiPageMain.ActiveViewIndex = 0
    End Sub

    Protected Sub btnPerformanceData_Click(sender As Object, e As EventArgs) Handles btnPerformanceData.Click
        MultiPageMain.ActiveViewIndex = 1
    End Sub

    Protected Sub btnFinalValidation_Click(sender As Object, e As EventArgs) Handles btnFinalValidation.Click
        MultiPageMain.ActiveViewIndex = 2
    End Sub

    Protected Sub btnEditExistingEvents_Click(sender As Object, e As EventArgs) Handles btnEditExistingEvents.Click
        MultiPageEvent.ActiveViewIndex = 0 ' EventEdit
    End Sub

    Protected Sub btnTabEventErrors_Click(sender As Object, e As EventArgs) Handles btnTabEventErrors.Click
        MultiPageEvent.ActiveViewIndex = 1 ' EventErrors
    End Sub

    Protected Sub btnOperatingData_Click(sender As Object, e As EventArgs) Handles btnOperatingData.Click
        MultiPagePerformance.ActiveViewIndex = 0  ' Perf1
    End Sub

    Protected Sub btnUnitTimeInfo_Click(sender As Object, e As EventArgs) Handles btnUnitTimeInfo.Click
        MultiPagePerformance.ActiveViewIndex = 1  ' Perf3
    End Sub

End Class