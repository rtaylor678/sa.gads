﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ErrorDefault.aspx.vb" Inherits="PortalNYISO.ErrorDefault" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <h1 style="text-align: center">NYISO GADS Portal</h1>
    <h2 style="text-align: center">ERROR IN APPLICATION</h2>
    <form id="Form1" runat="server">
        <h3 style="text-align: center">It is possible that your authentication has <i>timed out</i>.</h3>
        <h3 style="text-align: center">Therefore, please attempt to re-start the application.</h3>
        <h3 style="text-align: center">If this does not solve the problem, contact the <b>NYISO GADS </b> administrator.</h3>
    </form>
</body>
</html>
