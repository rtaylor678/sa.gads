﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CheckResults.aspx.vb" Inherits="PortalNYISO.CheckResults" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Check Results</title>
    <link href="../NewStyles.css" type="text/css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <table style="width: 760px">
            <tr>
                <td><a href="../MainDE.aspx">
                    <asp:Image ID="Image1" ImageUrl="../images/logo.gif" runat="server" AlternateText="NYISO: New York Independent System Operator" /></a>
                </td>
            </tr>
            <tr style="width: 100%">
                <td style="text-align: left; width: 100%; background-color: blue;">
                    <asp:Label ID="Label3" runat="server" ForeColor="White" Font-Size="9" Font-Bold="True" BackColor="Blue">Revisions/Errors Check Results</asp:Label></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr style="width: 100%">
                <td style="text-align: center; width: 100%;">
                    <asp:Label ID="Label1" runat="server" ForeColor="Red" Font-Size="8pt" Font-Bold="True">The following units have either been revised since the last error check (R) or have identified errors (E)</asp:Label></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr style="width: 100%">
                <td style="text-align: center; width: 100%;">
                    <asp:Panel ID="pnldgUnitStatus" Style="OVERFLOW: scroll" runat="server" Height="500" Width="100%">
                        <asp:DataGrid ID="dgUnitStatus" runat="server" Width="100%">
                            <HeaderStyle Font-Bold="True" Wrap="False" ForeColor="White" BackColor="Blue"></HeaderStyle>
                        </asp:DataGrid>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr style="width: 100%">
                <td style="text-align: center; width: 100%;">
                    <asp:Label ID="Label2" runat="server" Font-Size="8pt" Font-Bold="True">You must correct any errors and re-run the error check before you can write out the GADS data</asp:Label></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr style="width: 100%">
                <td style="text-align: center; width: 100%;">
                    <asp:Button ID="btnReturn" runat="server" Text="Return"></asp:Button></td>
            </tr>
        </table>
    </form>
</body>
</html>
