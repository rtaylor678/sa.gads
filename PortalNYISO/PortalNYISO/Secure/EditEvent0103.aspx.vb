﻿Imports BusinessLayer.BusinessLayer.GADSNGBusinessLayer
Imports System.Collections

Public Class EditEvent0103
    Inherits System.Web.UI.Page

    Private AppName As String = "GADSNG"
    Private connect As String = ""
    'Private blConnect As BusinessLayer.BusinessLayer.GADSNGBusinessLayer
    Private dtSetup As BusinessLayer.Setup.SetupDataTable
    Private drSetup As BusinessLayer.Setup.SetupRow
    Private dsCauseCodes As DataSet
    Private dtPerformance As BusinessLayer.Performance.PerformanceDataDataTable
    'Private drPerformance As BusinessLayer.Performance.PerformanceDataRow
    Private dsAllEvents As AllEventDataISO
    Private dsGridEvents As DataSet
    Private dsFailMech As DataSet
    Private dsCauseCodeExt As DataSet
    Private dsSavedVerbDesc As DataSet
    Private intCurrentYear As Integer
    Private strCurrentUnit As String = ""
    Private strCurrentPeriod As String = ""
    Private strCurrentMonthNo As String = ""
    'Private intSECauseCode As Integer
    'Private dtSEBeginning As DateTime
    Private boolFromTreeView As Boolean = False
    Private myUser As String = ""
    Private alPeriods As New ArrayList
    Private lbUnitsText As String = ""
    Private l_regex As System.Text.RegularExpressions.Regex
    Private lForceRefresh As Boolean
    Private strEventNumber As String = ""

    Public Enum GrossNetOrBoth
        GrossOnly = 0
        NetOnly = 1
        Both = 2
    End Enum

    Public drvDetail As DataRowView
    Private vueDetail As DataView
    Private initialEventType As String

    Private lCarriesOverToNextYear As Boolean
    Private lCarriedOverFromLastYear As Boolean
    Private strAddNewEvent As String

    Private lACValid As Boolean
    Private lDatesValid As Boolean
    Private lCauseCodeValid As Boolean
    Private lEventTypeValid As Boolean
    Private lVerbalDescValid As Boolean

    ' This is the column in the CauseCodes Table where the Description is found
    Const CauseCodeDescLocation As Int16 = 22

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            Dim strMode As String
            strMode = Request.QueryString("mode").ToUpper

            'Dim strTemp2 As String
            'strTemp2 = Request.QueryString("fName")

            RestoreFromSession()
            
            If strMode = "EDIT" Then
                strEventNumber = Session("EventNumber").ToString
                dsGridEvents = CType(Session("dsGridEvents"), DataSet)
                dsGridEvents.Tables("EventData01").DefaultView.RowFilter = "EventNumber = " & strEventNumber
                drvDetail = dsGridEvents.Tables("EventData01").DefaultView.Item(0)
                drSetup = dtSetup.FindByUtilityUnitCode(drvDetail.Item("UtilityUnitCode"))
                Me.lblFormHeader.Text = "Edit Event For " & Me.intCurrentYear.ToString & _
                 " - Event Number " & drvDetail.Row.Item("EventNumber").ToString() & _
                 " - " & lbUnitsText

            Else

                'createNewData()
                'Me.tbEventVerbalDesc_RFValidator1.Enabled = False

            End If

            'Select Case Me.drSetup.InputDataRequirements
            '	Case 0
            '		' NERC full set
            '		Me.gbWorkDetails.Enabled = True
            '		Me.tbWorkStartedMonth.Enabled = True
            '		Me.tbWorkStartedDay.Enabled = True
            '		Me.tbWorkStartedTime.Enabled = True
            '		Me.tbWorkEndedMonth.Enabled = True
            '		Me.tbWorkEndedDay.Enabled = True
            '		Me.tbWorkEndedTime.Enabled = True
            '		Me.tbManHoursWorked.Enabled = True

            '		Me.tbEventVerbalDesc.Enabled = True
            '		Me.cbCauseCodeExtension.Enabled = True
            '		Me.tbCauseCode.Enabled = True
            '		Me.btnCauseCodeTreeView.Enabled = True
            '		Me.chkDominantDerating.Enabled = True

            '	Case 1
            '		' NYISO reduced set
            '		Me.gbWorkDetails.Enabled = False
            '		Me.tbWorkStartedMonth.Enabled = False
            '		Me.tbWorkStartedDay.Enabled = False
            '		Me.tbWorkStartedTime.Enabled = False
            '		Me.tbWorkEndedMonth.Enabled = False
            '		Me.tbWorkEndedDay.Enabled = False
            '		Me.tbWorkEndedTime.Enabled = False
            '		Me.tbManHoursWorked.Enabled = False

            '		Me.tbEventVerbalDesc.Enabled = False
            '		Me.cbCauseCodeExtension.Enabled = False
            '		Me.tbCauseCode.Enabled = False
            '		Me.btnCauseCodeTreeView.Enabled = False
            '		Me.chkDominantDerating.Enabled = False

            'End Select

            Me.lCarriedOverFromLastYear = False
            Me.lCarriesOverToNextYear = False
            Me.strAddNewEvent = ""

            lACValid = True
            lDatesValid = True
            lCauseCodeValid = True
            lEventTypeValid = True
            lVerbalDescValid = True

            LoadEventTypeLB(Me.lbEventType)

            If drSetup.IsCauseCodeExtEditableNull Then
                drSetup.CauseCodeExtEditable = False
            End If

            'LoadCauseCodeExtCB(dsCauseCodeExt, Me.cbCauseCodeExtension)

            EditDetail()
            
            'If Me.drSetup.InputDataRequirements = 1 Then
            '	' This is required to have something in the Verbal Description field when inputting the
            '	' NYISO reduced set -- needed to pass the validation checks on it being a required field -- cannot
            '	' just leave it blank.
            '	Me.tbEventVerbalDesc.Text = "."
            'End If

            'Page.DataBind()
            Session.Item("boolFromTreeView") = boolFromTreeView
            Session.Item("newCauseCode") = tbCauseCode.Text
            Session.Item("newNERCCauseCodeDesc") = lblNERCCauseCodeDesc.Text

        Else
            boolFromTreeView = Convert.ToBoolean(Session.Item("boolFromTreeView"))
            If boolFromTreeView = True Then
                tbCauseCode.Text = Session.Item("newCauseCode").ToString
                lblNERCCauseCodeDesc.Text = Session.Item("newNERCCauseCodeDesc").ToString
                Session.Item("boolFromTreeView") = False
            End If
        End If
    End Sub

#Region " btnSaveNewEvent_Click(ByVal sender As Object, ByVal e As System.EventArgs) "

    Private Sub btnSaveNewEvent_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveNewEvent.Click

        'Page.Validate()
        If Not Page.IsValid Then
            Exit Sub
        End If

        SaveEventRecord()
        Session.Item("redoform") = True

        ' this causes the datagrid in the main form to automatically databind on the close of this window
        ' this could be used in all event detail forms to correctly postback
        Dim sScript As String = "<script language=javascript>"
        sScript += "window.opener.__doPostBack('','');window.close();"
        sScript += "</script>"
 
        Response.Write(sScript)

    End Sub

#End Region


#Region " btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) "

    Public Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click

        Dim strTemp As String

        Try
            'Me.tbEventVerbalDesc_RFValidator1.Enabled = False
            Me.RestoreFromSession()
            Session.Item("redoform") = False

            If IsDBNull(drvDetail.Item("EventType")) Or drvDetail.Item("EventType").ToString.Trim = String.Empty Or _
             Request.QueryString("mode").ToUpper = "NEW" Then

                Try
                    drvDetail.CancelEdit()
                    drvDetail.Row.Delete()
                    Me.SaveToSession()
                Catch ex As System.Exception
                    strTemp = ex.ToString
                End Try

            End If

        Catch ex As System.Exception
            strTemp = ex.ToString
        End Try


        ' this causes the datagrid in the main form to automatically databind on the close of this window
        ' this could be used in all event detail forms to correctly postback

        Dim sScript As String = "<script language=javascript>"
        sScript += "window.opener.__doPostBack('','');window.close();"
        sScript += "</script>"
        Dim cs As ClientScriptManager = Page.ClientScript
        Dim cstype As Type = Me.GetType()
        cs.RegisterStartupScript(cstype, "ClsScript", sScript)

    End Sub

#End Region

    Private Sub OpenWindow(ByVal htmlPage As String)
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "child", "window.open('" & htmlPage & "','','')", True)
    End Sub


#Region " SaveToSession() "

    Private Sub SaveToSession()

        'Session.Item("AppName") = AppName
        'Session.Item("connect") = connect
        'Session.Item("blConnect") = blConnect
        'Session.Item("dtSetup") = dtSetup
        'Session.Item("drSetup") = drSetup
        'Session.Item("dsCauseCodes") = dsCauseCodes
        'Session.Item("dtPerformance") = dtPerformance
        'Session.Item("drPerformance") = drPerformance
        Session.Item("dsAllEvents") = dsAllEvents
        Session.Item("dsGridEvents") = dsGridEvents
        ' The following are changed in the WebUtilityModule and should not be saved to the SessionState regardless
        'Session.Item("dsFailMech") = dsFailMech
        'Session.Item("dsCauseCodeExt") = dsCauseCodeExt
        Session.Item("dsSavedVerbDesc") = dsSavedVerbDesc
        'Session.Item("intCurrentYear") = intCurrentYear
        'Session.Item("strCurrentUnit") = strCurrentUnit
        'Session.Item("strCurrentPeriod") = strCurrentPeriod
        'Session.Item("strCurrentMonthNo") = strCurrentMonthNo
        'Session.Item("intSECauseCode") = intSECauseCode
        'Session.Item("dtSEBeginning") = dtSEBeginning
        'Session.Item("myUser") = myUser
        'Session.Item("alPeriods") = alPeriods


    End Sub

#End Region

#Region " RestoreFromSession() "

    Private Sub RestoreFromSession()

        AppName = Session("AppName").ToString
        connect = Session("connect").ToString
        'blConnect = CType(Session("blConnect"), BusinessLayer.BusinessLayer.GADSNGBusinessLayer)
        dtSetup = CType(Session("dtSetup"), BusinessLayer.Setup.SetupDataTable)
        'drSetup = CType(Session("drSetup"), BusinessLayer.Setup.SetupRow)
        dsCauseCodes = CType(Session("dsCauseCodes"), DataSet)
        dtPerformance = CType(Session("dtPerformance"), BusinessLayer.Performance.PerformanceDataDataTable)
        'drPerformance = CType(Session("drPerformance"), BusinessLayer.Performance.PerformanceDataRow)
        dsAllEvents = CType(Session("dsAllEvents"), AllEventDataISO)
        dsGridEvents = CType(Session("dsGridEvents"), DataSet)
        dsFailMech = CType(Session("dsFailMech"), DataSet)
        dsCauseCodeExt = CType(Session("dsCauseCodeExt"), DataSet)
        dsSavedVerbDesc = CType(Session("dsSavedVerbDesc"), DataSet)
        intCurrentYear = CType(Session("intCurrentYear"), Integer)
        strCurrentUnit = Session("strCurrentUnit").ToString
        strCurrentPeriod = Session("strCurrentPeriod").ToString
        strCurrentMonthNo = Session("strCurrentMonthNo").ToString
        'intSECauseCode = CType(Session("intSECauseCode"), Integer)
        'dtSEBeginning = CType(Session("dtSEBeginning"), DateTime)
        'myUser = Session("myUser").ToString
        myUser = Context.User.Identity.Name
        alPeriods = CType(Session("alPeriods"), ArrayList)
        lbUnitsText = Session("lbUnitsText").ToString
        'drvDetail = CType(Session("drvDetail"), DataRowView)
        strEventNumber = Session("EventNumber").ToString
    End Sub

#End Region

#Region " EditDetail() "

    Public Sub EditDetail()

        Dim strValue As String
        Dim dtTemp As DateTime

        'Me.tbGAC.Enabled = False
        Me.tbNAC.Enabled = False
        'Me.chkDominantDerating.Enabled = False

        If drSetup.IsGrossNetBothNull Then
            drSetup.GrossNetBoth = Convert.ToByte(GrossNetOrBoth.NetOnly)
            'Me.tbGAC.Enabled = True
            Me.tbNAC.Enabled = True
            'Me.chkDominantDerating.Enabled = True
        End If

        Me.strAddNewEvent = ""

        ' Sets or re-sets the Contribution Code to 1 for these records
        drvDetail.Item("ContribCode") = 1

        If drvDetail.Item("CarryOverLastYear") Is DBNull.Value Then
            Me.lCarriedOverFromLastYear = False
        Else
            Me.lCarriedOverFromLastYear = Convert.ToBoolean(drvDetail.Item("CarryOverLastYear"))
        End If

        If drvDetail.Item("CarryOverNextYear") Is DBNull.Value Then
            Me.lCarriesOverToNextYear = False
        Else
            Me.lCarriesOverToNextYear = Convert.ToBoolean(drvDetail.Item("CarryOverNextYear"))
        End If

        'Me.lblHowManyChars.Text = "Verbal Description (" & Me.tbEventVerbalDesc.MaxLength.ToString & " characters maximum)"

        ' Cause code and cause code extension

        Me.tbCauseCode.Text = drvDetail.Item("CauseCode").ToString

        ' This error checks the incoming Cause Code to see if it is valid and to setup the display of the NERC Cause Code description
        Dim foundRows As DataRow()

        Try
            If drvDetail.Item("CauseCode").ToString <> "0" And drvDetail.Item("CauseCode").ToString <> String.Empty Then

                foundRows = dsCauseCodes.Tables(0).Select("CauseCode = " & drvDetail.Item("CauseCode").ToString & " AND " & drSetup.UnitType & " = True")

                If foundRows.Length <> 0 Then
                    Me.lblNERCCauseCodeDesc.Text = foundRows(0)(CauseCodeDescLocation).ToString
                    Me.lblNERCCauseCodeDesc.DataBind()
                End If

            End If

        Catch

        End Try

        ' Verbal Description -- 86 characters max
        'Me.tbEventVerbalDesc.Text = drvDetail.Item("VerbalDesc86").ToString

        ' Cause Code Extension ComboBox
        'Me.cbCauseCodeExtension.DataBind()

        'Try
        '	Me.cbCauseCodeExtension.SelectedValue = drvDetail.Item("CauseCodeExt").ToString
        'Catch ex As System.Exception
        '	Me.cbCauseCodeExtension.SelectedIndex = Me.cbCauseCodeExtension.Items.Count - 1
        'End Try

        ' Deratings
        'Me.tbGAC.Text = drvDetail.Item("GrossAvailCapacity").ToString

        Try
            If Not IsDBNull(drvDetail.Item("NetAvailCapacity")) Then
                If IsNumeric(drvDetail.Item("NetAvailCapacity")) Then
                    Me.tbNAC.Text = Convert.ToDecimal(drvDetail.Item("NetAvailCapacity")).ToString
                Else
                    Me.tbNAC.Text = ""
                End If
            Else
                Me.tbNAC.Text = ""
            End If

        Catch ex As Exception
            Me.tbNAC.Text = ""
        End Try

        Try
            Me.lbEventType.SelectedValue = drvDetail.Item("EventType").ToString
        Catch ex As System.Exception
            Me.lbEventType.SelectedIndex = 0
        End Try

        If Not Me.lbEventType.SelectedValue Is Nothing Then

            If Me.lbEventType.SelectedValue.ToString.Trim <> String.Empty Then

                strValue = Me.lbEventType.SelectedValue.ToString

                Select Case strValue
                    Case "PO", "MO", "U1", "U2", "U3", "SF", "SE", "CO", "ME", "PE", "PU"

                        Me.gbDeratings.Enabled = False
                        'Me.tbGAC.Enabled = False
                        Me.tbNAC.Enabled = False
                        'Me.chkDominantDerating.Enabled = False

                    Case "RS"

                        Me.gbDeratings.Enabled = False
                        'Me.tbGAC.Enabled = False
                        Me.tbNAC.Enabled = False
                        'Me.chkDominantDerating.Enabled = False

                        Me.tbCauseCode.Text = "0000"
                        Me.tbCauseCode.Enabled = False

                        Me.lblNERCCauseCodeDesc.Text = "Reserve Shutdown"

                    Case "D1", "D2", "D3", "D4", "PD", "DE", "DM", "DP"

                        Me.gbDeratings.Enabled = True
                        'Me.tbGAC.Enabled = True
                        Me.tbNAC.Enabled = True
                        'Me.chkDominantDerating.Enabled = True


                    Case "NC"

                        ' the NC is for Homer City and Economic Dispatch
                        Me.gbDeratings.Enabled = True
                        Me.tbNAC.Enabled = False
                        'Me.tbGAC.Enabled = True
                        'Me.chkDominantDerating.Enabled = False

                    Case Else
                        Me.gbDeratings.Enabled = True
                        Me.tbNAC.Enabled = True
                        'Me.tbGAC.Enabled = True
                        'Me.chkDominantDerating.Enabled = True

                End Select

            Else
                Me.gbDeratings.Enabled = True
                Me.tbNAC.Enabled = True
                'Me.tbGAC.Enabled = True
                'Me.chkDominantDerating.Enabled = True

            End If

        Else
            Me.gbDeratings.Enabled = True
            Me.tbNAC.Enabled = True
            'Me.tbGAC.Enabled = True
            'Me.chkDominantDerating.Enabled = True

        End If

        'Me.tbEventVerbalDesc.MaxLength = 86


        'If Convert.IsDBNull(drvDetail.Item("DominantDerate")) Then
        '	Me.chkDominantDerating.Checked = False
        'Else
        '	Me.chkDominantDerating.Checked = CType(drvDetail.Item("DominantDerate"), Boolean)
        'End If

        'If Convert.IsDBNull(drvDetail.Item("ManhoursWorked")) Then
        '	tbManHoursWorked.Text = ""
        'Else
        '	tbManHoursWorked.Text = drvDetail.Item("ManhoursWorked").ToString
        'End If

        'Dim b As Binding

        ' Start of Event Date/Time

        If CType(drvDetail.Item("StartDateTime"), DateTime).ToString("HH:mm:ss") = "23:59:59" Then
            tbStartOfEventTime.Text = "2400"
        Else
            tbStartOfEventTime.Text = CType(drvDetail.Item("StartDateTime"), DateTime).ToString("HHmm")
        End If

        'AddHandler b.Format, AddressOf DateTimeToTimeString
        'AddHandler b.Parse, AddressOf TimeStringToStartDateTime

        tbStartOfEventDay.Text = CType(drvDetail.Item("StartDateTime"), DateTime).ToString("dd")

        'AddHandler b.Format, AddressOf DateTimeToDayString
        'AddHandler b.Parse, AddressOf DayStringToStartDateTime

        tbStartOfEventMonth.Text = CType(drvDetail.Item("StartDateTime"), DateTime).ToString("MM")

        'AddHandler b.Format, AddressOf DateTimeToMonthString
        'AddHandler b.Parse, AddressOf MonthStringToStartDateTime

        If Convert.IsDBNull(drvDetail.Item("EndDateTime")) Then
            tbEndOfEventMonth.Text = ""
            tbEndOfEventDay.Text = ""
            tbEndOfEventTime.Text = ""
        Else

            dtTemp = Convert.ToDateTime(drvDetail.Item("EndDateTime"))

            tbEndOfEventMonth.Text = dtTemp.ToString("MM")
            tbEndOfEventDay.Text = dtTemp.ToString("dd")

            If dtTemp.ToString("HH:mm:ss") = "23:59:59" Then
                If Me.lCarriesOverToNextYear And tbEndOfEventMonth.Text = "12" And tbEndOfEventDay.Text = "31" Then
                    tbEndOfEventTime.Text = "24XX"
                Else
                    tbEndOfEventTime.Text = "2400"
                End If
            Else
                tbEndOfEventTime.Text = dtTemp.ToString("HHmm")
            End If

        End If

        ' Work Started Date/Time

        'If Convert.IsDBNull(drvDetail.Item("WorkStarted")) Then
        '	tbWorkStartedMonth.Text = ""
        '	tbWorkStartedDay.Text = ""
        '	tbWorkStartedTime.Text = ""
        'Else

        '	If IsDate(drvDetail.Item("WorkStarted")) Then

        '		dtTemp = Convert.ToDateTime(drvDetail.Item("WorkStarted"))

        '		tbWorkStartedMonth.Text = dtTemp.ToString("MM")
        '		tbWorkStartedDay.Text = dtTemp.ToString("dd")

        '		If dtTemp.ToString("HH:mm:ss") = "23:59:59" Then
        '			tbWorkStartedTime.Text = "2400"
        '		Else
        '			tbWorkStartedTime.Text = dtTemp.ToString("HHmm")
        '		End If

        '	End If

        'End If

        ' Work Ended Date/Time

        'If Convert.IsDBNull(drvDetail.Item("WorkEnded")) Then
        '	tbWorkEndedMonth.Text = ""
        '	tbWorkEndedDay.Text = ""
        '	tbWorkEndedTime.Text = ""
        'Else

        '	dtTemp = Convert.ToDateTime(drvDetail.Item("WorkEnded"))

        '	tbWorkEndedMonth.Text = dtTemp.ToString("MM")
        '	tbWorkEndedDay.Text = dtTemp.ToString("dd")

        '	If dtTemp.ToString("HH:mm:ss") = "23:59:59" Then
        '		tbWorkEndedTime.Text = "2400"
        '	Else
        '		tbWorkEndedTime.Text = dtTemp.ToString("HHmm")
        '	End If

        'End If

        initialEventType = Me.drvDetail.Item("EventType").ToString

        'If drSetup.InputDataRequirements = 1 Then

        '	' NYISO reduced set
        '	Me.gbWorkDetails.Enabled = False
        '	Me.tbWorkStartedMonth.Enabled = False
        '	Me.tbWorkStartedDay.Enabled = False
        '	Me.tbWorkStartedTime.Enabled = False
        '	Me.tbWorkEndedMonth.Enabled = False
        '	Me.tbWorkEndedDay.Enabled = False
        '	Me.tbWorkEndedTime.Enabled = False
        '	Me.tbManHoursWorked.Enabled = False

        '	Me.tbEventVerbalDesc.Enabled = False
        '	Me.tbEventVerbalDesc_RFValidator1.Enabled = False
        '	Me.cbCauseCodeExtension.Enabled = False
        '	Me.tbCauseCode.Enabled = False
        '	Me.btnCauseCodeTreeView.Enabled = False
        '	Me.chkDominantDerating.Enabled = False

        'End If

        'Try

        '	If IsDBNull(drvDetail.Item("EventType")) Or drvDetail.Item("EventType").ToString.Trim = String.Empty Then

        '		Try
        '			drvDetail.CancelEdit()
        '			drvDetail.Row.Delete()
        '			Me.SaveToSession()
        '		Catch
        '			drvDetail.EndEdit()
        '		End Try

        '	Else

        '		drvDetail.EndEdit()

        '	End If

        'Catch ex as System.Exception

        '	drvDetail.CancelEdit()
        '	Me.strAddNewEvent = ""

        'End Try

        drvDetail.EndEdit()

    End Sub

#End Region


#Region " lbEventType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) "

    Public Sub lbEventType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbEventType.SelectedIndexChanged

        ProcesslbEventType()

    End Sub

#End Region


#Region " ProcesslbEventType() "

    Private Sub ProcesslbEventType()

        Dim strValue As String
        lEventTypeValid = True

        Try

            dtSetup = CType(Session("dtSetup"), BusinessLayer.Setup.SetupDataTable)
            strCurrentUnit = Session("strCurrentUnit").ToString
            'drSetup = CType(Session("drSetup"), BusinessLayer.Setup.SetupRow)
            drSetup = dtSetup.FindByUtilityUnitCode(strCurrentUnit)
            If lbEventType.SelectedIndex <> -1 Then

                If Not Me.lbEventType.SelectedValue Is Nothing Then

                    If Me.lbEventType.SelectedValue.ToString.Trim <> String.Empty Then

                        'Select Case drSetup.InputDataRequirements
                        '	Case 0
                        '		' NERC full set
                        '		Me.gbWorkDetails.Enabled = True

                        '		' Enables / disables the Work Details fields based on WorkDetails field in Setup
                        '		If drSetup.IsWorkDetailsNull Then
                        '			Me.gbWorkDetails.Enabled = True
                        '		Else
                        '			Me.gbWorkDetails.Enabled = drSetup.WorkDetails()
                        '		End If

                        '	Case 1
                        '		' NYISO reduced set
                        '		Me.gbWorkDetails.Enabled = False

                        'End Select

                        strValue = Me.lbEventType.SelectedValue.ToString
                        lCauseCodeValid = True
                        lVerbalDescValid = True

                        Select Case strValue
                            Case "PO", "MO", "U1", "U2", "U3", "SF", "SE", "ME", "PE", "PU", "CO"
                                Me.gbDeratings.Enabled = False
                                'Me.tbGAC.Text = ""
                                Me.tbNAC.Text = ""
                                'Me.tbGAC.Enabled = False
                                Me.tbNAC.Enabled = False
                                'Me.chkDominantDerating.Enabled = False
                                'Me.chkDominantDerating.Checked = False

                                'Me.tbCauseCode.Enabled = True
                                'Me.btnCauseCodeTreeView.Enabled = True
                                'Select Case drSetup.InputDataRequirements
                                '	Case 0
                                '		' NERC full set
                                '		Me.tbCauseCode.Enabled = True
                                '		Me.btnCauseCodeTreeView.Enabled = True
                                '		If Not Convert.IsDBNull(Me.tbCauseCode) And Me.tbCauseCode.Text <> String.Empty Then
                                '			If Convert.ToInt32(Me.tbCauseCode.Text) = 0 Then
                                '				Me.lCauseCodeValid = False
                                '			End If
                                '			If Me.tbEventVerbalDesc.Text.ToUpper.Trim = "RESERVE SHUTDOWN" Then
                                '				lVerbalDescValid = False
                                '			End If
                                '		End If

                                '	Case 1
                                '		' NYISO reduced set
                                '		Me.tbCauseCode.Enabled = False
                                '		Me.btnCauseCodeTreeView.Enabled = False
                                '		Me.tbEventVerbalDesc.Text = "Not Required"
                                'End Select

                            Case "RS"
                                Me.gbDeratings.Enabled = False
                                'Me.tbGAC.Text = ""
                                Me.tbNAC.Text = ""
                                'Me.chkDominantDerating.Checked = False
                                'Me.tbGAC.Enabled = False
                                Me.tbNAC.Enabled = False
                                'Me.chkDominantDerating.Enabled = False

                                'Me.gbWorkDetails.Enabled = False

                                'Me.btnCauseCodeTreeView.Enabled = False
                                'Me.tbCauseCode.Enabled = False
                                lCauseCodeValid = True
                                lEventTypeValid = True
                                lVerbalDescValid = True

                                Me.tbCauseCode.Text = "0000"
                                Me.lblNERCCauseCodeDesc.Text = "Reserve Shutdown"

                                'Select Case drSetup.InputDataRequirements
                                '	Case 0
                                '		' NERC full set
                                '		Me.tbCauseCode.Text = "0000"
                                '		Me.tbEventVerbalDesc.Text = "Reserve Shutdown"
                                '		Me.lblNERCCauseCodeDesc.Text = "Reserve Shutdown"

                                '	Case 1
                                '		' NYISO reduced set
                                '		Me.tbCauseCode.Text = ""
                                '		Me.tbEventVerbalDesc.Text = "Not Required"
                                '		Me.lblNERCCauseCodeDesc.Text = ""

                                'End Select

                            Case "D1", "D2", "D3", "D4", "PD", "DE", "DM", "DP"
                                Me.gbDeratings.Enabled = True
                                'Me.tbGAC.Enabled = True
                                Me.tbNAC.Enabled = True
                                'Me.chkDominantDerating.Enabled = True
                                'Me.tbCauseCode.Enabled = True
                                'Me.btnCauseCodeTreeView.Enabled = True

                                If drSetup.GrossNetBoth = GrossNetOrBoth.NetOnly Then
                                    Me.tbNAC.Enabled = True
                                    If Me.tbNAC.Text = String.Empty Then
                                        lACValid = False
                                    End If
                                ElseIf drSetup.GrossNetBoth = GrossNetOrBoth.GrossOnly Then
                                    'Me.tbGAC.Enabled = True
                                    'If Me.tbGAC.Text = String.Empty Then
                                    '	lACValid = False
                                    'End If
                                Else
                                    'Me.tbGAC.Enabled = True
                                    Me.tbNAC.Enabled = True
                                    If Me.tbNAC.Text = String.Empty Then
                                        lACValid = False
                                    End If
                                    'If Me.tbGAC.Text = String.Empty And Me.tbNAC.Text = String.Empty Then
                                    '	lACValid = False
                                    'End If
                                End If

                                'Select Case drSetup.InputDataRequirements
                                '	Case 0
                                '		' NERC full set
                                '		Me.tbCauseCode.Enabled = True
                                '		Me.btnCauseCodeTreeView.Enabled = True
                                '		If Not Convert.IsDBNull(Me.tbCauseCode) And Me.tbCauseCode.Text <> String.Empty Then
                                '			If Convert.ToInt32(Me.tbCauseCode.Text) = 0 Then
                                '				Me.lCauseCodeValid = False
                                '			End If
                                '			If Me.tbEventVerbalDesc.Text.ToUpper.Trim = "RESERVE SHUTDOWN" Then
                                '				lVerbalDescValid = False
                                '			End If
                                '		End If

                                '	Case 1
                                '		' NYISO reduced set
                                '		Me.tbCauseCode.Enabled = False
                                '		Me.btnCauseCodeTreeView.Enabled = False
                                '		Me.tbEventVerbalDesc.Text = "Not Required"
                                'End Select

                            Case "NC"
                                ' the NC is for Homer City and Economic Dispatch
                                Me.gbDeratings.Enabled = True
                                'Me.tbGAC.Enabled = True
                                Me.tbNAC.Enabled = True
                                'Me.chkDominantDerating.Enabled = False
                                'Me.tbCauseCode.Enabled = True
                                'Me.btnCauseCodeTreeView.Enabled = True
                                'Select Case drSetup.InputDataRequirements
                                '	Case 0
                                '		' NERC full set
                                '		Me.tbCauseCode.Enabled = True
                                '		Me.btnCauseCodeTreeView.Enabled = True
                                '		If Not Convert.IsDBNull(Me.tbCauseCode) And Me.tbCauseCode.Text <> String.Empty Then
                                '			If Convert.ToInt32(Me.tbCauseCode.Text) = 0 Then
                                '				Me.lCauseCodeValid = False
                                '			End If
                                '			If Me.tbEventVerbalDesc.Text.ToUpper.Trim = "RESERVE SHUTDOWN" Then
                                '				lVerbalDescValid = False
                                '			End If
                                '		End If

                                '	Case 1
                                '		' NYISO reduced set
                                '		Me.tbCauseCode.Enabled = False
                                '		Me.btnCauseCodeTreeView.Enabled = False
                                '		Me.tbEventVerbalDesc.Text = "Not Required"
                                'End Select
                            Case Else
                                Me.gbDeratings.Enabled = True
                                Me.tbNAC.Enabled = True
                                'Me.tbGAC.Enabled = True
                                'Me.chkDominantDerating.Enabled = True

                                Me.tbCauseCode.Enabled = True
                                Me.btnCauseCodeTreeView.Enabled = True
                                'Select Case drSetup.InputDataRequirements
                                '	Case 0
                                '		' NERC full set
                                '		Me.tbCauseCode.Enabled = True
                                '		Me.btnCauseCodeTreeView.Enabled = True

                                '	Case 1
                                '		' NYISO reduced set
                                '		Me.tbCauseCode.Enabled = False
                                '		Me.btnCauseCodeTreeView.Enabled = False
                                'End Select
                        End Select

                    Else
                        Me.gbDeratings.Enabled = True
                        Me.tbNAC.Enabled = True
                        'Me.tbGAC.Enabled = True
                        'Me.chkDominantDerating.Enabled = True

                    End If

                Else
                    Me.gbDeratings.Enabled = True
                    Me.tbNAC.Enabled = True
                    'Me.tbGAC.Enabled = True
                    'Me.chkDominantDerating.Enabled = True

                End If

                If Me.gbDeratings.Enabled = False Then
                    ' if you can't enter available capacities, then they must be valid
                    lACValid = True
                End If

                'Me.tbEventVerbalDesc.MaxLength = 86

                'setButtons()

                'drvDetail = CType(Session("drvDetail"), DataRowView)
                strEventNumber = Session("EventNumber").ToString
                dsGridEvents = CType(Session("dsGridEvents"), DataSet)
                dsGridEvents.Tables("EventData01").DefaultView.RowFilter = "EventNumber = " & strEventNumber
                drvDetail = dsGridEvents.Tables("EventData01").DefaultView.Item(0)

                If Not drvDetail Is Nothing Then

                    ' When you change the event type on the 01 card it needs to be carried through to the 04-99 for this event

                    dsGridEvents = CType(Session("dsGridEvents"), DataSet)
                    dsGridEvents.Tables("EventData02").DefaultView.RowFilter = "EventNumber = " & drvDetail.Item("EventNumber").ToString

                    Dim drvTemp As DataRowView

                    For Each drvTemp In dsGridEvents.Tables("EventData02").DefaultView
                        drvTemp.Item("EventType") = Me.lbEventType.SelectedValue.ToString
                    Next

                    Session.Item("dsGridEvents") = dsGridEvents

                End If

            End If

        Catch ex As System.Exception

            strValue = ex.ToString

        End Try

    End Sub

#End Region


#Region " StartDateTime_CustomValidator(ByVal s As Object, ByVal e As ServerValidateEventArgs) "

    Public Sub StartDateTime_CustomValidator(ByVal s As Object, ByVal e As ServerValidateEventArgs)

        Dim strDateTime As String
        Dim strFixTime As String
        Dim dtResult As DateTime
        dtSetup = CType(Session("dtSetup"), BusinessLayer.Setup.SetupDataTable)
        strCurrentUnit = Session("strCurrentUnit").ToString
        'drSetup = CType(Session("drSetup"), BusinessLayer.Setup.SetupRow)
        drSetup = dtSetup.FindByUtilityUnitCode(strCurrentUnit)
        intCurrentYear = CType(Session("intCurrentYear"), Integer)

        strFixTime = Me.tbStartOfEventTime.Text

        If strFixTime.Trim = String.Empty Then
            e.IsValid = False
            Exit Sub
        End If

        If strFixTime.IndexOf(":") <= 0 Then
            strFixTime = strFixTime.Substring(0, (strFixTime.Length - 2)) & ":" & strFixTime.Substring(strFixTime.Length - 2)
        End If

        If Me.tbStartOfEventTime.Text.EndsWith("XX") Then

            strFixTime = strFixTime.Substring(0, (strFixTime.Length - 2)) & "00"

            ' If strFixTime = "24:XX" Then it gets converted to 24:00

            ' At this point then we have either 24:00, 0:00 or 00:00 -- normally

            ' Could be nn:XX that gets converted to nn:00, but that's okay too

            If strFixTime = "00:00" Or strFixTime = "0:00" Then
                If (Me.tbStartOfEventMonth.Text = "1" Or Me.tbStartOfEventMonth.Text = "01") And (Me.tbStartOfEventDay.Text = "1" Or Me.tbStartOfEventDay.Text = "01") Then
                    Me.lCarriedOverFromLastYear = True
                Else
                    Me.lCarriedOverFromLastYear = False
                End If
            Else
                Me.lCarriedOverFromLastYear = False
            End If

        Else
            Me.lCarriedOverFromLastYear = False
        End If

        If strFixTime = "24:00" Then
            strFixTime = "23:59:59.999"
        End If

        strDateTime = Me.tbStartOfEventMonth.Text & "/" & Me.tbStartOfEventDay.Text & "/" & Convert.ToString(intCurrentYear) & _
           " " & strFixTime

        If IsDate(strDateTime) Then

            dtResult = System.DateTime.Parse(strDateTime)

            ' ValidDSTStart returns either an error message or an empty string

            strFixTime = ValidDSTStart(dtResult, drSetup.DaylightSavingTime)

            Me.StartDateTime_CV.ErrorMessage = strFixTime

            If strFixTime.Trim = String.Empty Then
                e.IsValid = True
            Else
                e.IsValid = False
            End If

        Else

            e.IsValid = False

        End If

    End Sub

#End Region

#Region " EndDateTime_CustomValidator(ByVal s As Object, ByVal e As ServerValidateEventArgs) "

    Public Sub EndDateTime_CustomValidator(ByVal s As Object, ByVal e As ServerValidateEventArgs)

        Dim strDateTime As String
        Dim strFixTime As String
        Dim dtResult As DateTime
        'Dim dtStart As DateTime
        'Dim dtEnd As DateTime
        'Dim dtWindowStart As DateTime
        'Dim dtWindowEnd As DateTime
        'Dim strET As String
        dtSetup = CType(Session("dtSetup"), BusinessLayer.Setup.SetupDataTable)
        strCurrentUnit = Session("strCurrentUnit").ToString
        'drSetup = CType(Session("drSetup"), BusinessLayer.Setup.SetupRow)
        drSetup = dtSetup.FindByUtilityUnitCode(strCurrentUnit)
        intCurrentYear = CType(Session("intCurrentYear"), Integer)
        'drvDetail = CType(Session("drvDetail"), DataRowView)
        strEventNumber = Session("EventNumber").ToString
        dsGridEvents = CType(Session("dsGridEvents"), DataSet)
        dsGridEvents.Tables("EventData01").DefaultView.RowFilter = "EventNumber = " & strEventNumber
        drvDetail = dsGridEvents.Tables("EventData01").DefaultView.Item(0)

        If Me.tbEndOfEventDay.Text = String.Empty And Me.tbEndOfEventMonth.Text = String.Empty And _
         Me.tbEndOfEventTime.Text = String.Empty Then
            e.IsValid = True
            Exit Sub
        End If

        strFixTime = Me.tbEndOfEventTime.Text

        If strFixTime.IndexOf(":") <= 0 Then
            strFixTime = strFixTime.Substring(0, (strFixTime.Length - 2)) & ":" & strFixTime.Substring(strFixTime.Length - 2)
        End If

        If Me.tbEndOfEventTime.Text.EndsWith("XX") Then

            strFixTime = strFixTime.Substring(0, (strFixTime.Length - 2)) & "00"

            ' If strFixTime = "24:XX" Then it gets converted to 24:00

            ' At this point then we have either 24:00, 0:00 or 00:00 -- normally

            ' Could be nn:XX that gets converted to nn:00, but that's okay too

            If strFixTime = "24:00" Then
                If Me.tbEndOfEventMonth.Text = "12" And Me.tbEndOfEventDay.Text = "31" Then
                    Me.lCarriesOverToNextYear = True
                Else
                    Me.lCarriesOverToNextYear = False
                End If
            Else
                Me.lCarriesOverToNextYear = False
            End If
        Else
            Me.lCarriesOverToNextYear = False
        End If

        If strFixTime = "24:00" Then
            strFixTime = "23:59:59.999"
        End If

        strDateTime = Me.tbEndOfEventMonth.Text & "/" & Me.tbEndOfEventDay.Text & "/" & Convert.ToString(intCurrentYear) & _
        " " & strFixTime

        If IsDate(strDateTime) Then

            dtResult = System.DateTime.Parse(strDateTime)

            ' ValidDSTStart returns either an error message or an empty string

            strFixTime = ValidDSTStart(dtResult, drSetup.DaylightSavingTime)

            Me.EndDateTime_CV.ErrorMessage = strFixTime

            If strFixTime.Trim = String.Empty Then

                e.IsValid = True

                ' --------------------------------------------------------------
                Dim strDateTime2 As String
                Dim strFixTime2 As String
                Dim dtResult2 As DateTime

                strFixTime2 = Me.tbStartOfEventTime.Text

                If strFixTime2.Trim <> String.Empty Then

                    If strFixTime2.IndexOf(":") <= 0 Then
                        strFixTime2 = strFixTime2.Substring(0, (strFixTime2.Length - 2)) & ":" & strFixTime2.Substring(strFixTime2.Length - 2)
                    End If

                    If Me.tbStartOfEventTime.Text.EndsWith("XX") Then

                        strFixTime2 = strFixTime2.Substring(0, (strFixTime2.Length - 2)) & "00"

                        ' If strFixTime = "24:XX" Then it gets converted to 24:00

                        ' At this point then we have either 24:00, 0:00 or 00:00 -- normally

                        ' Could be nn:XX that gets converted to nn:00, but that's okay too

                    End If

                    If strFixTime2 = "24:00" Then
                        strFixTime2 = "23:59:59.999"
                    End If

                    strDateTime2 = Me.tbStartOfEventMonth.Text & "/" & Me.tbStartOfEventDay.Text & "/" & Convert.ToString(intCurrentYear) & _
                     " " & strFixTime2

                    If IsDate(strDateTime2) Then

                        dtResult2 = System.DateTime.Parse(strDateTime2)

                        ' ValidDSTStart returns either an error message or an empty string

                        strFixTime2 = ValidDSTStart(dtResult2, drSetup.DaylightSavingTime)

                        If strFixTime2.Trim = String.Empty Then
                            If dtResult2 >= dtResult Then
                                e.IsValid = False
                                Me.EndDateTime_CV.ErrorMessage = "Event Date/times are reversed or are the same"
                                Me.EndDateTime_CV.ToolTip = "Event Date/times are reversed or are the same"
                            Else
                                Me.EndDateTime_CV.ErrorMessage = "Invalid End of Event Date/time"
                                Me.EndDateTime_CV.ToolTip = "Invalid End of Event Date/time"
                            End If

                        End If

                    End If

                End If

            Else
                e.IsValid = False
            End If

        Else
            e.IsValid = False
        End If

    End Sub

#End Region

#Region " tbStartOfEventDay_CustV_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) "

    Private Sub tbStartOfEventDay_CustV_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles tbStartOfEventDay_CustV.ServerValidate

        intCurrentYear = CType(Session("intCurrentYear"), Integer)

        If Convert.ToInt16(Me.tbStartOfEventMonth.Text) <= 12 Then

            If Convert.ToInt16(Me.tbStartOfEventDay.Text) > System.DateTime.DaysInMonth(intCurrentYear, Convert.ToInt16(Me.tbStartOfEventMonth.Text)) Then

                Me.tbStartOfEventDay_CustV.ErrorMessage = "Start of Event Day range must be between 1 and " & _
                 System.DateTime.DaysInMonth(intCurrentYear, Convert.ToInt16(Me.tbStartOfEventMonth.Text)).ToString
                Me.tbStartOfEventDay_CustV.ToolTip = "Range must be between 1 and " & _
                 System.DateTime.DaysInMonth(intCurrentYear, Convert.ToInt16(Me.tbStartOfEventMonth.Text)).ToString

                args.IsValid = False

            End If

        End If

    End Sub

#End Region

#Region " tbStartOfEventTime_CustV_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) "

    Public Sub tbStartOfEventTime_CustV_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles tbStartOfEventTime_CustV.ServerValidate

        'Dim l_regex As System.Text.RegularExpressions.Regex

        ' Best 24 clock that also allows XX
        'a_sqlStatement = " 4XX".PadLeft(4)
        'MsgBox(l_regex.IsMatch(a_sqlStatement, "^(20|21|22|23|24|[ 0-1]\d)[0-5X][0-9X]$"), MsgBoxStyle.Information, a_sqlStatement)

        Try

            If System.Text.RegularExpressions.Regex.IsMatch(Me.tbStartOfEventTime.Text.PadLeft(4), "^(20|21|22|23|24|[ 0-1]\d)[0-5X][0-9X]$") Then
                If Me.tbStartOfEventTime.Text <> "24XX" Then
                    If Me.tbStartOfEventTime.Text > "2400" Then

                        Me.tbStartOfEventTime_CustV.ErrorMessage = "Start of Event:  Invalid Military Time format as hhmm or hhXX <br>" & _
                        "Range must be between 0000 and 2400 - include leading 0 <br>" & _
                        "Valid values also includes 00XX <br>" & _
                        "X's must be entered in uppercase"
                        args.IsValid = False
                    Else
                        args.IsValid = True
                    End If
                Else
                    ' Start of Event TIME = 24XX -- which is not valid -- only for end of event time
                    args.IsValid = False
                End If
            Else
                Me.tbStartOfEventTime_CustV.ErrorMessage = "Start of Event:  Invalid Military Time format as hhmm or hhXX <br>" & _
                  "Range must be between 0000 and 2400 - include leading 0 <br>" & _
                  "Valid values also includes 00XX <br>" & _
                  "X's must be entered in uppercase"
                args.IsValid = False
            End If

        Catch
            Me.tbStartOfEventTime_CustV.ErrorMessage = "Start of Event:  Invalid Military Time format as hhmm or hhXX"
            args.IsValid = False
        End Try

    End Sub

#End Region

#Region " tbEndOfEventDay_CustV_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) "

    Private Sub tbEndOfEventDay_CustV_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles tbEndOfEventDay_CustV.ServerValidate

        intCurrentYear = CType(Session("intCurrentYear"), Integer)

        If Convert.ToInt16(Me.tbEndOfEventMonth.Text) <= 12 Then

            If Convert.ToInt16(Me.tbEndOfEventDay.Text) > System.DateTime.DaysInMonth(intCurrentYear, Convert.ToInt16(Me.tbEndOfEventMonth.Text)) Then

                Me.tbEndOfEventDay_CustV.ErrorMessage = "End of Event Day range must be between 1 and " & _
                System.DateTime.DaysInMonth(intCurrentYear, Convert.ToInt16(Me.tbEndOfEventMonth.Text)).ToString

                Me.tbEndOfEventDay_CustV.ToolTip = "Range must be between 1 and " & _
                System.DateTime.DaysInMonth(intCurrentYear, Convert.ToInt16(Me.tbEndOfEventMonth.Text)).ToString

                args.IsValid = False

            End If

        End If

    End Sub

#End Region

#Region " tbEndOfEventTime_CustV_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) "

    Public Sub tbEndOfEventTime_CustV_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles tbEndOfEventTime_CustV.ServerValidate

        'Dim l_regex As System.Text.RegularExpressions.Regex

        ' Best 24 clock that also allows XX
        'a_sqlStatement = " 4XX".PadLeft(4)
        'MsgBox(l_regex.IsMatch(a_sqlStatement, "^(20|21|22|23|24|[ 0-1]\d)[0-5X][0-9X]$"), MsgBoxStyle.Information, a_sqlStatement)

        Try

            If System.Text.RegularExpressions.Regex.IsMatch(Me.tbEndOfEventTime.Text.PadLeft(4), "^(20|21|22|23|24|[ 0-1]\d)[0-5X][0-9X]$") Then
                If Me.tbEndOfEventTime.Text <> "24XX" Then
                    If Me.tbEndOfEventTime.Text > "2400" Then

                        Me.tbEndOfEventTime_CustV.ErrorMessage = "End of Event:  Invalid Military Time format as hhmm or hhXX <br>" & _
                        "Range must be between 0000 and 2400 - include leading 0 <br>" & _
                        "Valid values also includes 24XX <br>" & _
                        "X's must be entered in uppercase"

                        args.IsValid = False

                    Else
                        args.IsValid = True
                    End If
                Else
                    args.IsValid = True
                End If
            Else
                Me.tbEndOfEventTime_CustV.ErrorMessage = "End of Event:  Invalid Military Time format as hhmm or hhXX <br>" & _
                  "Range must be between 0000 and 2400 - include leading 0 <br>" & _
                  "Valid values also includes 24XX <br>" & _
                  "X's must be entered in uppercase"
                args.IsValid = False
            End If

        Catch
            Me.tbEndOfEventTime_CustV.ErrorMessage = "End of Event:  Invalid Military Time format as hhmm or hhXX"
            args.IsValid = False
        End Try

    End Sub

#End Region


#Region " SaveEventRecord() "

    Private Sub SaveEventRecord()

        Dim strError As String
        Dim strDateTime As String
        Dim strFixTime As String
        Dim dtResult As DateTime
        'Dim dtStart As DateTime
        'Dim dtEnd As DateTime
        'Dim dtWindowStart As DateTime
        'Dim dtWindowEnd As DateTime
        'Dim strET As String
        Dim strTemp As String
        Dim strFormat As String = "MM/dd/yyyy"

        Try

            RestoreFromSession()

            drvDetail.BeginEdit()

            drvDetail.Item("EventType") = Me.lbEventType.SelectedValue.ToString.Substring(0, 2)

            strFixTime = Me.tbStartOfEventTime.Text

            If strFixTime.Trim = String.Empty Then
                strFixTime = "00:00"
            End If

            If strFixTime.IndexOf(":") <= 0 Then
                strFixTime = strFixTime.Substring(0, (strFixTime.Length - 2)) & ":" & strFixTime.Substring(strFixTime.Length - 2)
            End If

            If Me.tbStartOfEventTime.Text.EndsWith("XX") Then

                strFixTime = strFixTime.Substring(0, (strFixTime.Length - 2)) & "00"

                ' If strFixTime = "24:XX" Then it gets converted to 24:00

                ' At this point then we have either 24:00, 0:00 or 00:00 -- normally

                ' Could be nn:XX that gets converted to nn:00, but that's okay too

                If strFixTime = "00:00" Or strFixTime = "0:00" Then
                    If (Me.tbStartOfEventMonth.Text = "1" Or Me.tbStartOfEventMonth.Text = "01") And (Me.tbStartOfEventDay.Text = "1" Or Me.tbStartOfEventDay.Text = "01") Then
                        Me.lCarriedOverFromLastYear = True
                    Else
                        Me.lCarriedOverFromLastYear = False
                    End If
                Else
                    Me.lCarriedOverFromLastYear = False
                End If

            Else
                Me.lCarriedOverFromLastYear = False
            End If

            If strFixTime = "24:00" Then
                strFixTime = "23:59:59.999"
            End If

            strDateTime = Me.tbStartOfEventMonth.Text & "/" & Me.tbStartOfEventDay.Text & "/" & Convert.ToString(intCurrentYear) & _
               " " & strFixTime

            If IsDate(strDateTime) Then

                dtResult = System.DateTime.Parse(strDateTime)

                ' ValidDSTStart returns either an error message or an empty string

                strFixTime = ValidDSTStart(dtResult, drSetup.DaylightSavingTime)

                If strFixTime.Trim = String.Empty Then
                    drvDetail.Item("StartDateTime") = dtResult

                    strTemp = dtResult.ToString("HH:mm:ss")

                    If strTemp = "23:59:59" Then
                        drvDetail.Item("strStartDateTime") = dtResult.ToString(strFormat) & " 24:00"
                    Else
                        drvDetail.Item("strStartDateTime") = dtResult.ToString(strFormat) & dtResult.ToString(" HH:mm")
                    End If

                End If

            End If

            drvDetail.Item("CarryOverLastYear") = Me.lCarriedOverFromLastYear

            ' -----------------------------------------------------------------

            If Me.tbEndOfEventDay.Text = String.Empty Or Me.tbEndOfEventMonth.Text = String.Empty Or _
              Me.tbEndOfEventTime.Text = String.Empty Then

                ' one of the required fields has been left empty

                drvDetail.Item("EndDateTime") = DBNull.Value
                drvDetail.Item("CarryOverNextYear") = False

            Else

                strFixTime = Me.tbEndOfEventTime.Text

                If strFixTime.Trim = String.Empty Then
                    strFixTime = "00:00"
                End If

                If strFixTime.IndexOf(":") <= 0 Then
                    strFixTime = strFixTime.Substring(0, (strFixTime.Length - 2)) & ":" & strFixTime.Substring(strFixTime.Length - 2)
                End If

                If Me.tbEndOfEventTime.Text.EndsWith("XX") Then

                    strFixTime = strFixTime.Substring(0, (strFixTime.Length - 2)) & "00"

                    ' If strFixTime = "24:XX" Then it gets converted to 24:00

                    ' At this point then we have either 24:00, 0:00 or 00:00 -- normally

                    ' Could be nn:XX that gets converted to nn:00, but that's okay too

                    If strFixTime = "24:00" Then
                        If Me.tbEndOfEventMonth.Text = "12" And Me.tbEndOfEventDay.Text = "31" Then
                            Me.lCarriesOverToNextYear = True
                        Else
                            Me.lCarriesOverToNextYear = False
                        End If
                    Else
                        Me.lCarriesOverToNextYear = False
                    End If
                Else
                    Me.lCarriesOverToNextYear = False
                End If

                If strFixTime = "24:00" Then
                    strFixTime = "23:59:59.999"
                End If

                strDateTime = Me.tbEndOfEventMonth.Text & "/" & Me.tbEndOfEventDay.Text & "/" & Convert.ToString(intCurrentYear) & _
                   " " & strFixTime

                If IsDate(strDateTime) Then

                    dtResult = System.DateTime.Parse(strDateTime)

                    ' ValidDSTStart returns either an error message or an empty string

                    strFixTime = ValidDSTStart(dtResult, drSetup.DaylightSavingTime)

                    If strFixTime.Trim = String.Empty Then

                        drvDetail.Item("EndDateTime") = dtResult

                        strTemp = dtResult.ToString("HH:mm:ss")

                        If strTemp = "23:59:59" Then
                            drvDetail.Item("strEndDateTime") = dtResult.ToString(strFormat) & " 24:00"
                        Else
                            drvDetail.Item("strEndDateTime") = dtResult.ToString(strFormat) & dtResult.ToString(" HH:mm")
                        End If

                    End If

                End If

                drvDetail.Item("CarryOverNextYear") = Me.lCarriesOverToNextYear

            End If

            ' -----------------------------------------------------------------

            'If Me.tbGAC.Text <> String.Empty Then
            '	If IsNumeric(Me.tbGAC.Text) Then
            '		drvDetail.Item("GrossAvailCapacity") = Convert.ToInt16(Me.tbGAC.Text)
            '	Else
            '		drvDetail.Item("GrossAvailCapacity") = DBNull.Value
            '	End If
            'Else
            '	drvDetail.Item("GrossAvailCapacity") = DBNull.Value
            'End If

            If Me.tbNAC.Text <> String.Empty Then
                If IsNumeric(Me.tbNAC.Text) Then
                    drvDetail.Item("NetAvailCapacity") = Convert.ToDecimal(Me.tbNAC.Text)
                Else
                    drvDetail.Item("NetAvailCapacity") = DBNull.Value
                End If
            Else
                drvDetail.Item("NetAvailCapacity") = DBNull.Value
            End If

            ' -----------------------------------------------------------------
            drvDetail.Item("CauseCode") = Convert.ToInt16(Me.tbCauseCode.Text)
            drvDetail.Item("ContribCode") = 1

            ' -----------------------------------------------------------------

            Select Case Me.drSetup.InputDataRequirements
                Case 0
                    ' NERC full set

                    'If Me.tbManHoursWorked.Text <> String.Empty Or IsNumeric(Me.tbManHoursWorked.Text) Then
                    '	drvDetail.Item("ManhoursWorked") = Convert.ToInt16(Me.tbManHoursWorked.Text)
                    'End If

                    ' -----------------------------------------------------------------

                    'strFixTime = Me.tbWorkStartedTime.Text

                    'If strFixTime.Trim = String.Empty Then
                    '	strFixTime = "00:00"
                    'End If

                    'If strFixTime.IndexOf(":") <= 0 Then
                    '	strFixTime = strFixTime.Substring(0, (strFixTime.Length - 2)) & ":" & strFixTime.Substring(strFixTime.Length - 2)
                    'End If

                    'If Me.tbWorkStartedTime.Text.EndsWith("XX") Then
                    '	strFixTime = strFixTime.Substring(0, (strFixTime.Length - 2)) & "00"
                    'End If

                    'If strFixTime = "24:00" Then
                    '	strFixTime = "23:59:59.999"
                    'End If

                    'strDateTime = Me.tbWorkStartedMonth.Text & "/" & Me.tbWorkStartedDay.Text & "/" & Convert.ToString(intCurrentYear) & _
                    '   " " & strFixTime

                    'If IsDate(strDateTime) Then

                    '	dtResult = System.DateTime.Parse(strDateTime)

                    '	' ValidDSTStart returns either an error message or an empty string

                    '	strFixTime = ValidDSTStart(dtResult, drSetup.DaylightSavingTime)

                    '	If strFixTime.Trim = String.Empty Then
                    '		drvDetail.Item("WorkStarted") = dtResult
                    '	End If

                    'End If

                    ' -----------------------------------------------------------------

                    'If Me.tbWorkEndedDay.Text = String.Empty Or Me.tbWorkEndedMonth.Text = String.Empty Or _
                    '  Me.tbWorkEndedTime.Text = String.Empty Then

                    '	' one of the required fields has been left empty

                    '	drvDetail.Item("WorkEnded") = DBNull.Value

                    'Else

                    '	strFixTime = Me.tbWorkEndedTime.Text

                    '	If strFixTime.Trim = String.Empty Then
                    '		strFixTime = "00:00"
                    '	End If

                    '	If strFixTime.IndexOf(":") <= 0 Then
                    '		strFixTime = strFixTime.Substring(0, (strFixTime.Length - 2)) & ":" & strFixTime.Substring(strFixTime.Length - 2)
                    '	End If

                    '	If Me.tbWorkEndedTime.Text.EndsWith("XX") Then
                    '		strFixTime = strFixTime.Substring(0, (strFixTime.Length - 2)) & "00"
                    '	End If

                    '	If strFixTime = "24:00" Then
                    '		strFixTime = "23:59:59.999"
                    '	End If

                    '	strDateTime = Me.tbWorkEndedMonth.Text & "/" & Me.tbWorkEndedDay.Text & "/" & Convert.ToString(intCurrentYear) & _
                    '	   " " & strFixTime

                    '	If IsDate(strDateTime) Then

                    '		dtResult = System.DateTime.Parse(strDateTime)

                    '		' ValidDSTStart returns either an error message or an empty string

                    '		strFixTime = ValidDSTStart(dtResult, drSetup.DaylightSavingTime)

                    '		If strFixTime.Trim = String.Empty Then
                    '			drvDetail.Item("WorkEnded") = dtResult
                    '		End If

                    '	End If

                    'End If

                    ' -----------------------------------------------------------------

                    drvDetail.Item("FailureMechCode") = DBNull.Value
                    drvDetail.Item("TripMech") = DBNull.Value
                    drvDetail.Item("CumFiredHours") = DBNull.Value
                    drvDetail.Item("CumEngineStarts") = DBNull.Value

                    If drvDetail.Item("EventType").ToString.Trim = "RS" Then
                        drvDetail.Item("CauseCode") = 0
                        drvDetail.Item("VerbalDesc86") = "Reserve Shutdown"
                    Else

                        If IsDBNull(drvDetail.Item("CauseCode")) Then
                            drvDetail.Item("CauseCode") = 7777
                        ElseIf Convert.ToInt16(drvDetail.Item("CauseCode")) = 0 Then
                            drvDetail.Item("CauseCode") = 7777
                        End If

                        If IsDBNull(drvDetail.Item("VerbalDesc86")) Then
                            drvDetail.Item("VerbalDesc86") = "NA"
                        ElseIf drvDetail.Item("VerbalDesc86").ToString.Trim = String.Empty Then
                            drvDetail.Item("VerbalDesc86") = "NA"
                        End If

                    End If

                    If IsDBNull(drvDetail.Item("CauseCodeExt")) Then
                        drvDetail.Item("CauseCodeExt") = String.Empty
                    End If

                    'If drSetup.CauseCodeExtEditable And Me.cbCauseCodeExtension.SelectedValue.ToString <> String.Empty Then
                    '	drvDetail.Item("CauseCodeExt") = Me.cbCauseCodeExtension.SelectedValue.Substring(0, 2)
                    'ElseIf Me.cbCauseCodeExtension.SelectedValue.ToString <> String.Empty Then
                    '	drvDetail.Item("CauseCodeExt") = Me.cbCauseCodeExtension.SelectedValue.Substring(0, 2)
                    'End If

                    drvDetail.Item("DominantDerate") = False

                Case 1
                    ' NYISO reduced set

                    drvDetail.Item("ManhoursWorked") = DBNull.Value
                    drvDetail.Item("WorkStarted") = DBNull.Value
                    drvDetail.Item("WorkEnded") = DBNull.Value
                    drvDetail.Item("FailureMechCode") = DBNull.Value
                    drvDetail.Item("TripMech") = DBNull.Value
                    drvDetail.Item("CumFiredHours") = DBNull.Value
                    drvDetail.Item("CumEngineStarts") = DBNull.Value
                    drvDetail.Item("PrimaryAlert") = DBNull.Value
                    drvDetail.Item("PJMIOCode") = DBNull.Value
                    drvDetail.Item("VerbalDesc86") = DBNull.Value
                    'drvDetail.Item("CauseCode") = 0
                    'drvDetail.Item("CauseCodeExt") = DBNull.Value

            End Select

            ' -----------------------------------------------------------------
            drvDetail.EndEdit()

            Session.Item("dsGridEvents") = dsGridEvents

        Catch ex As System.Exception

            strError = ex.ToString

        Finally

            Me.strAddNewEvent = ""

        End Try

    End Sub

#End Region



#Region " tbCauseCode_CustV_ServerValidate "

    Public Sub tbCauseCode_CustV_ServerValidate(ByVal source As System.Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles tbCauseCode_CustV.ServerValidate
        Dim foundRows As DataRow()
        dtSetup = CType(Session("dtSetup"), BusinessLayer.Setup.SetupDataTable)
        strCurrentUnit = Session("strCurrentUnit").ToString
        'drSetup = CType(Session("drSetup"), BusinessLayer.Setup.SetupRow)
        drSetup = dtSetup.FindByUtilityUnitCode(strCurrentUnit)
        dsCauseCodes = CType(Session("dsCauseCodes"), DataSet)
        Dim intYear As Int16 = CType(Session("intCurrentYear"), Integer)
        Me.tbCauseCode_CustV.ErrorMessage = ""
        Me.tbCauseCode_CustV.ToolTip = ""
        Dim num As Integer = Integer.Parse(args.Value)
        Try
            If num <> 0 And num.ToString.Trim <> String.Empty And IsNumeric(num) Then
                'foundRows = Me.dsCauseCodes.Tables(0).Select("CauseCode = " & num.ToString & " AND " & Me.drSetup.UnitType & " = True AND " & intYear.ToString & " >= StartingYear AND " & intYear.ToString & " <= EndingYear")
                foundRows = Me.dsCauseCodes.Tables(0).Select("CauseCode = " & num.ToString & " AND " & intYear.ToString & " >= StartingYear AND " & intYear.ToString & " <= EndingYear")
                If foundRows.Length = 0 Then
                    Me.tbCauseCode_CustV.ErrorMessage = "Invalid cause code"
                    Me.tbCauseCode_CustV.ToolTip = "Invalid cause code"
                    args.IsValid = False
                    Me.lblNERCCauseCodeDesc.Text = String.Empty
                Else
                    args.IsValid = True
                    Me.tbCauseCode.Text = num.ToString("000#")
                    Me.lblNERCCauseCodeDesc.Text = foundRows(0)(CauseCodeDescLocation).ToString
                End If
                Me.lblNERCCauseCodeDesc.DataBind()
            Else
                args.IsValid = True
                Me.tbCauseCode.Text = num.ToString("000#")
            End If

        Catch ex As System.Exception
            Me.tbCauseCode_CustV.ErrorMessage = "Invalid cause code"
            Me.tbCauseCode_CustV.ToolTip = "Invalid cause code"
            args.IsValid = False
        End Try
    End Sub

#End Region


#Region " lbEventType_CustV_ServerValidate "

    Private Sub lbEventType_CustV_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles lbEventType_CustV.ServerValidate

        'Dim strTemp As String
        dtSetup = CType(Session("dtSetup"), BusinessLayer.Setup.SetupDataTable)
        strCurrentUnit = Session("strCurrentUnit").ToString
        'drSetup = CType(Session("drSetup"), BusinessLayer.Setup.SetupRow)
        drSetup = dtSetup.FindByUtilityUnitCode(strCurrentUnit)

        If Me.drSetup.InputDataRequirements = 0 Then

            ' NERC FULL data set

            If Me.lbEventType.SelectedValue = String.Empty Then
                Me.lbEventType_CustV.ErrorMessage = "Must select an Event Type"
                Me.lbEventType_CustV.ToolTip = "Must select an Event Type"
                args.IsValid = False
                'Else
                '             strTemp = "7777"

                '	If Me.lbEventType.SelectedValue <> "RS" And strTemp = String.Empty Then
                '		Me.lbEventType_CustV.ErrorMessage = "Only RS can have Cause Code = 0000"
                '		Me.lbEventType_CustV.ToolTip = "Only RS can have Cause Code = 0000"
                '		args.IsValid = False
                '	ElseIf Me.lbEventType.SelectedValue = "RS" And strTemp <> String.Empty Then
                '		Me.lbEventType_CustV.ErrorMessage = "RS must have Cause Code = 0000"
                '		Me.lbEventType_CustV.ToolTip = "RS must have Cause Code = 0000"
                '		args.IsValid = False
                '	Else
                '		Me.lbEventType_CustV.ErrorMessage = "Must select an Event Type"
                '		Me.lbEventType_CustV.ToolTip = "Must select an Event Type"
                '		args.IsValid = True
                '	End If

            End If

        End If

    End Sub

#End Region

#Region " btnCauseCodeTreeView_Click(ByVal sender As Object, ByVal e As System.EventArgs) "

    Protected Sub btnCauseCodeTreeView_Click(sender As Object, e As EventArgs) Handles btnCauseCodeTreeView.Click

        Session.Item("boolFromTreeView") = True
        'drSetup = CType(Session("drSetup"), BusinessLayer.Setup.SetupRow)

        Session.Item("newCauseCode") = tbCauseCode.Text
        Session.Item("newNERCCauseCodeDesc") = lblNERCCauseCodeDesc.Text
        
        Dim sScript As String
        sScript = ""
        sScript += "<script language='javascript'>"
        sScript += "var cc_win = window.open('CauseCodeTreeView.aspx?WhichForm=EditEvent0103', '_new'); cc_win.focus();"
        sScript += "</script>"

        Response.Write(sScript)

    End Sub

#End Region


End Class