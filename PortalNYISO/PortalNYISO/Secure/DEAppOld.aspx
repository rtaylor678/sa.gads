﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="DEAppOld.aspx.vb" Inherits="PortalNYISO.DEAppOld" Debug="false" Trace="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title>NYISO GADS Portal</title>
	<%--<link href="../Styles.css" type="text/css" rel="stylesheet" />--%>
	<%--<script src="DeApp.js" type="text/javascript"></script>--%>
</head>
<body>
	<form id="form1" runat="server">
		<div>
			<table id="EntireApp">
				<tr>
					<td colspan="4"><a href="../MainDE.aspx">
						<asp:Image ID="Image1" ImageUrl="../images/logo.gif" runat="server" AlternateText="NYISO: New York Independent System Operator" /></a>
					</td>
				</tr>
				<tr style="height: 12px">
					<td colspan="4"></td>
				</tr>
				<tr>
					<td colspan="4">
						<asp:Button ID="btnUandP" runat="server" Height="24" Width="150" Text="Upload &amp; Process"></asp:Button>
						<asp:Button ID="btnEditData" runat="server" Height="24" Width="150" Text="Edit Data" />
						<asp:Button ID="btnReports" runat="server" Height="24" Width="150" Text="Reports" />
						<asp:Button ID="HelpIndex" runat="server" Height="24" Width="150" Text="Help Index" />
					</td>
				</tr>
				<tr style="height: 12px">
					<td colspan="4"></td>
				</tr>
				<tr style="text-align:center">
					<td colspan="2" style="text-align: center">
						<asp:Button ID="mnuRefresh" Text="Refresh Display" Width="180" runat="server"></asp:Button>
					</td>
					<td colspan="2" style="text-align: center">
						<asp:Button ID="btnProcessButton" Text="Process Data" Width="180" runat="server"></asp:Button>
					</td>
				</tr>
				<tr>
					<td colspan="4">
						<hr />
						<asp:LinkButton ID="lnkbutRefresh" runat="server" Visible="false">LinkButton</asp:LinkButton></td>
				</tr>
				<tr>
					<td>
						<asp:DropDownList ID="lbUnits" runat="server" AutoPostBack="True" /></td>
					<td style="width: 35%">
						<asp:Label ID="lblStatus" runat="server" ForeColor="Black" Font-Bold="True" Font-Size="8pt"
							BorderStyle="None">OK</asp:Label></td>
					<td style="FONT-SIZE: 10pt; text-align: right;">Reporting&nbsp;Month</td>
					<td>
						<asp:DropDownList ID="lbUpdateMonth" runat="server" AutoPostBack="True" /></td>
				</tr>
				<tr>
					<td colspan="4">&nbsp;</td>
				</tr>
				<tr>
					<td id="TabStrip1" colspan="4">
						<asp:Button ID="btnEventData" runat="server" Height="24" Width="150" Text="Event Data" />
						<asp:Button ID="btnPerformanceData" runat="server" Height="24" Width="150" Text="Performance Data" />
						<asp:Button ID="btnFinalValidation" runat="server" Height="24" Width="150" Text="Final Validation" />
					</td>
				</tr>
			</table>
			<asp:Panel ID="Panel1" Width="765px" runat="server" BorderStyle="Inset" BorderColor="Black">

				<asp:MultiView ID="MultiPageMain" runat="server" ActiveViewIndex="0">
					<%--===============================================================================--%>
					<asp:View ID="EventDataTab" runat="server">
						<table>
							<tr>
								<td id="tabstripEvent">
									<asp:Button ID="btnEditExistingEvents" runat="server" Height="24" Width="150" Text="Edit Existing Events" BackColor="LightBlue"  />
									<asp:Button ID="btnTabEventErrors" runat="server" Height="24" Width="150" Text="Event Errors" BackColor="LightBlue" />
								</td>
							</tr>
						</table>
						<asp:Panel ID="Panel2" Width="760px" runat="server" BorderStyle="Inset" HorizontalAlign="Center" >
							<asp:MultiView ID="MultiPageEvent" runat="server" ActiveViewIndex="0">
								<asp:View ID="EventEdit" runat="server">
									<table>
										<tr>
											<td style="width: 124px">
												<asp:Panel ID="Event1ButtonsPanel" runat="server" Width="112px" Height="219px">
													<table style="width: 100%">

														<tr>
															<td style="text-align: center">
																<asp:Button ID="btnNewEvent" Text="New Event" Width="100%" runat="server" /></td>
														</tr>

														<tr>
															<td style="text-align: center">
																<asp:Button ID="btnEditRecord" Text="Edit" Width="100%" runat="server"></asp:Button></td>
														</tr>

														<tr>
															<td style="text-align: center">
																<asp:Button ID="btnDeleteEvent" runat="server" Text="Delete" Width="100%"></asp:Button></td>
														</tr>

														<tr>
															<td style="text-align: center">
																<asp:Button ID="btnSaveEvents" runat="server" Text="Submit" Width="100%"></asp:Button></td>
														</tr>

														<tr>
															<td style="text-align: center">
																<asp:Button ID="btnEventList" runat="server" Text="List Events" Width="100%"></asp:Button></td>
														</tr>

														<tr>
															<td style="text-align: center">
																<asp:Button ID="btndgEventsRefresh" Text="Refresh Events"
																	Width="100%" runat="server"></asp:Button></td>
														</tr>
													</table>

												</asp:Panel>
											</td>
											<td>

												<asp:Panel ID="Event1DataGridsPanel" runat="server" BorderStyle="Inset" Width="600px" Height="510px">
													<table style="width: 100%">
														<tr>
															<td style="width: 100%;">
																<asp:Label ID="LabelPriCause" runat="server" Font-Size="9pt" ForeColor="White" Width=" 100%"
																	BackColor="Navy" Font-Names="Verdana">Primary Cause of Event</asp:Label>
															</td>
														</tr>
														<tr>
															<td style="height: 500px">
																<asp:Panel ID="pnldgEvents" Style="OVERFLOW: scroll" runat="server" Width="590px" Height="468px" ScrollBars="Auto">
																	<asp:DataGrid ID="dgEvents" runat="server" ItemStyle-Height="27px"
																		Font-Size="9pt" Width="100%" Height="100%" Font-Names="Microsoft Sans Serif" PageSize="9999" HeaderStyle-Height="27px"
																		AllowSorting="True" DataKeyField="EventNumber" CellPadding="5"
																		AutoGenerateColumns="False" SelectedItemStyle-BackColor="Navy">
																		<Columns>
																			<asp:TemplateColumn>
																				<ItemTemplate>
																					<asp:Button runat="server" Text="      " CommandName="Select" ID="Button2"></asp:Button>
																				</ItemTemplate>
																			</asp:TemplateColumn>
																			<asp:TemplateColumn SortExpression="EventNumber" ItemStyle-Wrap="False" HeaderStyle-Wrap="False" ItemStyle-HorizontalAlign="Right"
																				ItemStyle-BackColor="white" HeaderText="Event Number">
																				<ItemTemplate>
																					<asp:Label ID="EventNumber" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"EventNumber") %>'>
																					</asp:Label>
																				</ItemTemplate>
																			</asp:TemplateColumn>
																			<asp:TemplateColumn SortExpression="EventType, StartDateTime, EndDateTime" ItemStyle-Wrap="False" HeaderStyle-Wrap="False"
																				ItemStyle-HorizontalAlign="Center" ItemStyle-BackColor="white" HeaderText="Event Type">
																				<ItemTemplate>
																					<asp:Label ID="EventType" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"EventType") %>'>
																					</asp:Label>
																				</ItemTemplate>
																			</asp:TemplateColumn>
																			<asp:TemplateColumn SortExpression="CauseCode, EventType, StartDateTime, EndDateTime" ItemStyle-Wrap="False"
																				HeaderStyle-Wrap="False" ItemStyle-HorizontalAlign="Right" ItemStyle-BackColor="white" HeaderText="Cause Code">
																				<ItemTemplate>
																					<asp:Label ID="CauseCodeAdd" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"CauseCode","{0:0000}") %>'>
																					</asp:Label>
																				</ItemTemplate>
																			</asp:TemplateColumn>
																			<asp:TemplateColumn SortExpression="CauseCodeExt" ItemStyle-Wrap="False" HeaderStyle-Wrap="False" ItemStyle-HorizontalAlign="Left"
																				ItemStyle-BackColor="white" HeaderText="-Ext">
																				<ItemTemplate>
																					<asp:Label ID="CauseCodeExtAdd" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"CauseCodeExt") %>'>
																					</asp:Label>
																				</ItemTemplate>
																			</asp:TemplateColumn>
																			<asp:TemplateColumn SortExpression="StartDateTime" ItemStyle-Wrap="False" HeaderStyle-Wrap="False" ItemStyle-HorizontalAlign="Left"
																				ItemStyle-BackColor="white" HeaderText="Start Date/Time" HeaderStyle-HorizontalAlign="Center">
																				<ItemTemplate>
																					<asp:Label ID="StartDateTime" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"StartDateTime","{0:MM/dd/yyyy HH:mm}") %>'>
																					</asp:Label>
																				</ItemTemplate>
																			</asp:TemplateColumn>
																			<asp:TemplateColumn SortExpression="EndDateTime" ItemStyle-Wrap="False" HeaderStyle-Wrap="False" ItemStyle-HorizontalAlign="Left"
																				ItemStyle-BackColor="white" HeaderText="End Date/Time" HeaderStyle-HorizontalAlign="Center">
																				<ItemTemplate>
																					<asp:Label ID="EndDateTime" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"EndDateTime","{0:MM/dd/yyyy HH:mm}") %>'>
																					</asp:Label>
																				</ItemTemplate>
																			</asp:TemplateColumn>
																			<asp:TemplateColumn SortExpression="VerbalDesc86" ItemStyle-Wrap="False" HeaderStyle-Wrap="False" ItemStyle-BackColor="white"
																				HeaderText="Verbal Description">
																				<ItemTemplate>
																					<asp:Label runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"VerbalDesc86") %>' ID="Label1">
																					</asp:Label>
																				</ItemTemplate>
																			</asp:TemplateColumn>
																			<asp:TemplateColumn SortExpression="GrossAvailCapacity" ItemStyle-Wrap="False" HeaderStyle-Wrap="False"
																				ItemStyle-HorizontalAlign="Right" ItemStyle-BackColor="white" HeaderText="GAC">
																				<ItemTemplate>
																					<asp:Label runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"GrossAvailCapacity") %>' ID="Label2">
																					</asp:Label>
																				</ItemTemplate>
																			</asp:TemplateColumn>
																			<asp:TemplateColumn SortExpression="NetAvailCapacity" ItemStyle-Wrap="False" HeaderStyle-Wrap="False"
																				ItemStyle-HorizontalAlign="Right" ItemStyle-BackColor="white" HeaderText="NAC">
																				<ItemTemplate>
																					<asp:Label runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"NetAvailCapacity") %>' ID="Label3">
																					</asp:Label>
																				</ItemTemplate>
																			</asp:TemplateColumn>
																			<%--<asp:TemplateColumn SortExpression="WorkStarted" ItemStyle-Wrap="False" HeaderStyle-Wrap="False" ItemStyle-HorizontalAlign="Left"
																			ItemStyle-BackColor="white" HeaderText="Work Started">
																			<ItemTemplate>
																				<asp:Label runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"WorkStarted","{0:MM/dd/yyyy HH:mm}") %>' ID="Label4">
																				</asp:Label>
																			</ItemTemplate>
																		</asp:TemplateColumn>--%>
																			<%--<asp:TemplateColumn SortExpression="WorkEnded" ItemStyle-Wrap="False" HeaderStyle-Wrap="False" ItemStyle-HorizontalAlign="Left"
																			ItemStyle-BackColor="white" HeaderText="Work Ended">
																			<ItemTemplate>
																				<asp:Label runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"WorkEnded","{0:MM/dd/yyyy HH:mm}") %>' ID="Label5">
																				</asp:Label>
																			</ItemTemplate>
																		</asp:TemplateColumn>--%>
																			<%--<asp:TemplateColumn SortExpression="ManhoursWorked" ItemStyle-Wrap="False" HeaderStyle-Wrap="False"
																			ItemStyle-HorizontalAlign="Right" ItemStyle-BackColor="white" HeaderText="Manhours">
																			<ItemTemplate>
																				<asp:Label runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"ManhoursWorked") %>' ID="Label6">
																				</asp:Label>
																			</ItemTemplate>
																		</asp:TemplateColumn>--%>
																			<asp:TemplateColumn SortExpression="CarryOverLastYear" ItemStyle-Wrap="False" HeaderStyle-Wrap="False"
																				ItemStyle-HorizontalAlign="Center" ItemStyle-BackColor="white" HeaderText="Started Prior Year">
																				<ItemTemplate>
																					<asp:Label runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"CarryOverLastYear") %>' ID="Label7">
																					</asp:Label>
																				</ItemTemplate>
																			</asp:TemplateColumn>
																			<asp:TemplateColumn SortExpression="CarryOverNextYear" ItemStyle-Wrap="False" HeaderStyle-Wrap="False"
																				ItemStyle-HorizontalAlign="Center" ItemStyle-BackColor="white" HeaderText="Ends Next Year">
																				<ItemTemplate>
																					<asp:Label runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"CarryOverNextYear") %>' ID="Label8">
																					</asp:Label>
																				</ItemTemplate>
																			</asp:TemplateColumn>
																			<%--<asp:TemplateColumn SortExpression="PrimaryAlert" ItemStyle-Wrap="False" HeaderStyle-Wrap="False" ItemStyle-HorizontalAlign="Center"
																			ItemStyle-BackColor="white" HeaderText="Alert">
																			<ItemTemplate>
																				<asp:Label runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"PrimaryAlert") %>' ID="Label9">
																				</asp:Label>
																			</ItemTemplate>
																		</asp:TemplateColumn>--%>
																			<%--<asp:TemplateColumn SortExpression="FailureMechCode" ItemStyle-Wrap="False" HeaderStyle-Wrap="False"
																			ItemStyle-HorizontalAlign="Left" ItemStyle-BackColor="white" HeaderText="Fail Mech">
																			<ItemTemplate>
																				<asp:Label runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"FailureMechCode") %>' ID="Label10">
																				</asp:Label>
																			</ItemTemplate>
																		</asp:TemplateColumn>--%>
																			<%--<asp:TemplateColumn SortExpression="TripMech" ItemStyle-Wrap="False" HeaderStyle-Wrap="False" ItemStyle-HorizontalAlign="Center"
																			ItemStyle-BackColor="white" HeaderText="Trip Mech">
																			<ItemTemplate>
																				<asp:Label runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"TripMech") %>' ID="Label11">
																				</asp:Label>
																			</ItemTemplate>
																		</asp:TemplateColumn>--%>
																			<%--<asp:TemplateColumn SortExpression="CumFiredHours" ItemStyle-Wrap="False" HeaderStyle-Wrap="False" ItemStyle-HorizontalAlign="Right"
																			ItemStyle-BackColor="white" HeaderText="Fired Hrs">
																			<ItemTemplate>
																				<asp:Label runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"CumFiredHours") %>' ID="Label12">
																				</asp:Label>
																			</ItemTemplate>
																		</asp:TemplateColumn>--%>
																			<%--<asp:TemplateColumn SortExpression="CumEngineStarts" ItemStyle-Wrap="False" HeaderStyle-Wrap="False"
																			ItemStyle-HorizontalAlign="Right" ItemStyle-BackColor="white" HeaderText="Eng Starts">
																			<ItemTemplate>
																				<asp:Label runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"CumEngineStarts") %>' ID="Label13">
																				</asp:Label>
																			</ItemTemplate>
																		</asp:TemplateColumn>--%>
																			<asp:TemplateColumn SortExpression="RevisionCard01" ItemStyle-Wrap="False" HeaderStyle-Wrap="False"
																				ItemStyle-HorizontalAlign="Center" ItemStyle-BackColor="white" HeaderText="Rev">
																				<ItemTemplate>
																					<asp:Label ID="RevisionCard01" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"RevisionCard01") %>'>
																					</asp:Label>
																				</ItemTemplate>
																			</asp:TemplateColumn>
																		</Columns>
																	</asp:DataGrid>

																</asp:Panel>
															</td>

														</tr>
													</table>

												</asp:Panel>
											</td>
										</tr>

									</table>

								</asp:View>
								<asp:View ID="EventErrors" runat="server">
									<table style="WIDTH: 750px; HEIGHT: 550px">
										<tr>
											<%--style="width: 100%">--%>
											<td style="vertical-align: top; text-align: center; width: 17%;">
												<p>&nbsp;</p>
												<p>
													<asp:Button ID="btnPrintEventErrors" runat="server" Text="Print" Width="100%"></asp:Button>
												</p>
											</td>
											<td style="vertical-align: top; width: 83%;">
												<asp:TextBox ID="tbEventErrorList" runat="server" BorderStyle="Inset" Width="100%" Height="97%"
													ReadOnly="True" Rows="10" TextMode="MultiLine">No Errors to Display</asp:TextBox></td>
										</tr>
									</table>
								</asp:View>
							</asp:MultiView>
						</asp:Panel>
					</asp:View>
					<%--===============================================================================--%>
					<asp:View ID="OperatingData" runat="Server">
						
						<div>

							<table id="PerformanceButtons">
								<tr>
									<td>
										<asp:Button ID="btnPerfUpdate" runat="server" Text="Submit"></asp:Button>
									</td>
									<td>
										<asp:Button ID="btnPerfCancel" runat="server" Text="Cancel"></asp:Button>
									</td>
									<td>&nbsp; &nbsp;
									</td>
									<td>
										<asp:Button ID="btnPerfListGen" runat="server" Text="List Gen Data"></asp:Button>
									</td>
									<td>
										<asp:Button ID="btnPerfListHours" runat="server" Text="List Time Data"></asp:Button>
									</td>
								</tr>
								<tr><td></td></tr>
							</table>
							<table id="PerformanceTable">
								<tr>
									<td id="tabstripPerformance">
										<asp:Button ID="btnOperatingData" runat="server" Height="24" Width="150" Text="Operating Data" BackColor="LightBlue" />
										<asp:Button ID="btnUnitTimeInfo" runat="server" Height="24" Width="150" Text="Unit Time Info" BackColor="LightBlue" />
									</td>
								</tr>

							</table>
							
                            
                            <div style="width: 760px; border-style:inset;">
								<asp:MultiView ID="MultiPagePerformance" runat="server" ActiveViewIndex="0">
									<asp:View ID="Perf1" runat="server">
										<div>

											<table id="Perf1Table" style="margin: 4px">
												<tr>
													<td>&nbsp;</td>
												</tr>
												<tr style="FONT-SIZE: 8pt">
													<td></td>
													<td>Net Maximum<br />
														Capacity (NMC)</td>
													<td>Net Dependable<br />
														Capacity (NDC)</td>
													<td>Net Actual<br />
														Generation (MWh)</td>
													<td>Number of<br />
														Attempted Unit<br />
														Starts</td>
													<td></td>
													<td>Number of Actual<br />
														Unit Starts</td>
													<td></td>
												</tr>
												<tr>
													<td></td>
													<td style="text-align: center;">
														<asp:TextBox ID="tbNMCValue" runat="server" Width="100px" Text="" MaxLength="7"></asp:TextBox></td>
													<td style="text-align: center;">
														<asp:TextBox ID="tbNDC" runat="server" Width="100px" Text="" MaxLength="7"></asp:TextBox></td>
													<td style="text-align: center;">
														<asp:TextBox ID="tbNAG" runat="server" Width="100px" Text="" MaxLength="10"></asp:TextBox></td>
													<td>
														<asp:TextBox ID="tbAttStarts" runat="server" Width="100px" Text="" MaxLength="3"></asp:TextBox></td>
													<td style="FONT-SIZE: 8pt; text-align: center;">&nbsp; &gt;= &nbsp;</td>
													<td>
														<asp:TextBox ID="tbActStarts" runat="server" Width="100px" Text="" MaxLength="3"></asp:TextBox></td>
												</tr>
												<tr style="vertical-align: top;">
													<td>&nbsp;</td>
													<td>
														<asp:CompareValidator ID="tbNMCValue_KeyPress" runat="server" Font-Size="8pt" ErrorMessage="Invalid NMC on Operating Data tab"
															ControlToValidate="tbNMCValue" Display="Dynamic" Operator="DataTypeCheck" Type="Double" ToolTip="NMC Invalid Data">*</asp:CompareValidator>
														<asp:RangeValidator ID="tbNMCValue_Validating" runat="server" Font-Size="8pt" ErrorMessage="NMC out of range (0-1300) on Operating Data tab"
															ControlToValidate="tbNMCValue" Display="Dynamic" Type="Double" ToolTip="NMC out of range (0-1300)"
															MaximumValue="1300" MinimumValue="0">*</asp:RangeValidator>
													</td>
													<td>
														<asp:CompareValidator ID="tbNDC_KeyPress" runat="server" Font-Size="8pt" ErrorMessage="Invalid NDC on Operating Data tab"
															ControlToValidate="tbNDC" Display="Dynamic" Operator="DataTypeCheck" Type="Double" ToolTip="NDC Invalid Data">*</asp:CompareValidator>
														<asp:CompareValidator ID="tbNDC_Validating2" runat="server" Font-Size="8pt" ErrorMessage="NDC cannot be bigger than NMC on Operating Data tab"
															ControlToValidate="tbNDC" Display="Dynamic" Operator="LessThanEqual" Type="Double" ToolTip="NDC cannot be bigger than NMC"
															ControlToCompare="tbNMCValue">*</asp:CompareValidator>
														<asp:RangeValidator ID="tbNDC_Validating1" runat="server" Font-Size="8pt" ErrorMessage="NDC out of range (0-1300) on Operating Data tab"
															ControlToValidate="tbNDC" Display="Dynamic" Type="Double" ToolTip="NDC out of range (0-1300)" MaximumValue="1300"
															MinimumValue="0">*</asp:RangeValidator>
													</td>
													<td>
														<asp:CompareValidator ID="tbNAG_KeyPress" runat="server" Font-Size="8pt" ErrorMessage="Invalid NAG on Operating Data tab"
															ControlToValidate="tbNAG" Display="Dynamic" Operator="DataTypeCheck" Type="Double" ToolTip="NAG Invalid Data">*</asp:CompareValidator>
														<asp:RangeValidator ID="tbNAG_Validating" runat="server" Font-Size="8pt" ErrorMessage="NAG out of range (-999,999-9,999,999) on Operating Data tab"
															ControlToValidate="tbNAG" Display="Dynamic" Type="Double" ToolTip="NAG out of range (-999,999-9,999,999)"
															MaximumValue="9999999" MinimumValue="-999999">*</asp:RangeValidator>
													</td>
													<td>
														<asp:CompareValidator ID="tbAttStarts_KeyPress" runat="server" Font-Size="8pt" ErrorMessage="Invalid Attempted Starts on Operating Data tab"
															ControlToValidate="tbAttStarts" Display="Dynamic" Operator="DataTypeCheck" Type="Integer" ToolTip="Attempted Starts Invalid Data">*</asp:CompareValidator>
														<asp:RangeValidator ID="tbAttStarts_Validating" runat="server" Font-Size="8pt" ErrorMessage="Attempted Starts out of range (0-999) on Operating Data tab"
															ControlToValidate="tbAttStarts" Display="Dynamic" Type="Integer" ToolTip="Attempted Starts out of range (0-999)"
															MaximumValue="999" MinimumValue="0">*</asp:RangeValidator></td>
													<td>
														<asp:CompareValidator ID="StartsCompare_Validator" runat="server" Font-Size="8pt" ErrorMessage="Actual Starts must be <= Attempted Starts"
															ControlToValidate="tbActStarts" Display="Dynamic" Operator="LessThanEqual" Type="Integer" ToolTip="Actual Starts must be <= Attempted Starts"
															ControlToCompare="tbAttStarts">*</asp:CompareValidator></td>
													<td>
														<asp:CompareValidator ID="tbActStarts_KeyPress" runat="server" Font-Size="8pt" ErrorMessage="Invalid Actual Starts on Operating Data tab"
															ControlToValidate="tbActStarts" Display="Dynamic" Operator="DataTypeCheck" Type="Integer" ToolTip="Actual Starts Invalid Data">*</asp:CompareValidator>
														<asp:RangeValidator ID="tbActStarts_Validating" runat="server" Font-Size="8pt" ErrorMessage="Actual Starts out of range (0-999) on Operating Data tab"
															ControlToValidate="tbActStarts" Display="Dynamic" Type="Integer" ToolTip="Actual Starts out of range (0-999)"
															MaximumValue="999" MinimumValue="0">*</asp:RangeValidator></td>
												</tr>
											</table>
											<table id="Performance01TULC">
												<tr>
													<td>&nbsp;</td>
												</tr>
												<tr>
													<td>&nbsp; &nbsp;</td>
													<td style="FONT-SIZE: 8pt">Typical Unit Loading<br />
														Characteristics</td>
													<td>
														<asp:DropDownList ID="lbTULC" runat="server" AutoPostBack="True" Font-Size="8pt" Width="450px"></asp:DropDownList></td>
												</tr>
											</table>
											<table id="Performance01StartsFill">
												<tr>
													<td>&nbsp; &nbsp;</td>
													<td style="FONT-WEIGHT: bold; FONT-SIZE: 8pt">Auto Fill uses Available Event data 
																		to fill in Starts fields.<br />
														You must enter ALL events including RS.</td>
													<td>&nbsp;</td>
													<td>
														<asp:Button ID="btnStartsFill" runat="server" Text="Auto Fill"
															Font-Size="8pt"></asp:Button></td>
												</tr>
												<tr>
													<td>&nbsp;</td>
												</tr>
											</table>
										
                                        </div>
									</asp:View>

									<asp:View ID="Perf3" runat="server">
										<div>

											<table id="Perf3Table">
												<tr>
													<td>&nbsp;</td>
												</tr>
												<tr>
													<td>&nbsp;</td>
													<td style="FONT-SIZE: 8pt">Unit Service Hours</td>
													<td>
														<asp:TextBox ID="tbServiceHours" runat="server" Font-Size="8pt" Width="100px" Text="" MaxLength="6"></asp:TextBox></td>
													<td>
														<asp:CompareValidator ID="tbServiceHours_KeyPress" runat="server" Font-Size="8pt" ErrorMessage="Invalid Service Hours on Unit Time Info tab"
															ControlToValidate="tbServiceHours" Display="Dynamic" Operator="DataTypeCheck" Type="Double" ToolTip="Service Hours Invalid Data">*</asp:CompareValidator>
														<asp:RangeValidator ID="tbServiceHours_Validating" runat="server" Font-Size="8pt" ErrorMessage="Service Hours out of range"
															ControlToValidate="tbServiceHours" Display="Dynamic" Type="Double" ToolTip="Service Hours out of range"
															MaximumValue="2208" MinimumValue="0">*</asp:RangeValidator></td>
													<td style="FONT-SIZE: 8pt">Planned Outage Hours</td>
													<td>
														<asp:TextBox ID="tbPOHours" runat="server" Font-Size="8pt" Width="100px" Text="" MaxLength="6"></asp:TextBox></td>
													<td>
														<asp:CompareValidator ID="tbPOHours_KeyPress" runat="server" Font-Size="8pt" ErrorMessage="Invalid Planned Outage Hours on Unit Time Info tab"
															ControlToValidate="tbPOHours" Display="Dynamic" Operator="DataTypeCheck" Type="Double" ToolTip="PO Hours Invalid Data">*</asp:CompareValidator>
														<asp:RangeValidator ID="tbPOHours_Validating" runat="server" Font-Size="8pt" ErrorMessage="Planned Outage Hours out of range"
															ControlToValidate="tbPOHours" Display="Dynamic" Type="Double" ToolTip="Planned Outage Hours out of range"
															MaximumValue="2208" MinimumValue="0">*</asp:RangeValidator></td>
												</tr>
												<tr>
													<td></td>
													<td style="FONT-SIZE: 8pt">Reserve Shutdown Hours</td>
													<td>
														<asp:TextBox ID="tbRSHours" runat="server" Font-Size="8pt" Width="100px" Text="" MaxLength="6"></asp:TextBox></td>
													<td>
														<asp:CompareValidator ID="tbRSHours_KeyPress" runat="server" Font-Size="8pt" ErrorMessage="Invalid Reserve Shutdown Hours on Unit Time Info tab"
															ControlToValidate="tbRSHours" Display="Dynamic" Operator="DataTypeCheck" Type="Double" ToolTip="RS Hours Invalid Data">*</asp:CompareValidator>
														<asp:RangeValidator ID="tbRSHours_Validating" runat="server" Font-Size="8pt" ErrorMessage="Reserve Shutdown Hours out of range"
															ControlToValidate="tbRSHours" Display="Dynamic" Type="Double" ToolTip="Reserve Shutdown Hours out of range"
															MaximumValue="2208" MinimumValue="0">*</asp:RangeValidator></td>
													<td style="FONT-SIZE: 8pt">Unplanned (Forced) Outage +<br />
														Startup Failure Hours</td>
													<td>
														<asp:TextBox ID="tbFOHSFHours" runat="server" Font-Size="8pt" Width="100px" Text="" MaxLength="6"></asp:TextBox></td>
													<td>
														<asp:CompareValidator ID="tbFOHSFHours_KeyPress" runat="server" Font-Size="8pt" ErrorMessage="Invalid Forced Outage and Startup Failure Hours on Unit Time Info tab"
															ControlToValidate="tbFOHSFHours" Display="Dynamic" Operator="DataTypeCheck" Type="Double" ToolTip="FOH/SF Hours Invalid Data">*</asp:CompareValidator>
														<asp:RangeValidator ID="tbFOHSFHours_Validating" runat="server" Font-Size="8pt" ErrorMessage="Forced Outage &amp; Startup Failures Hours out of range"
															ControlToValidate="tbFOHSFHours" Display="Dynamic" Type="Double" ToolTip="Forced Outage &amp; Startup Failures Hours out of range"
															MaximumValue="2208" MinimumValue="0">*</asp:RangeValidator></td>
												</tr>
												<tr>
													<td></td>
													<td style="FONT-SIZE: 8pt">Pumping Hours</td>
													<td>
														<asp:TextBox ID="tbPumpingHours" runat="server" Font-Size="8pt" Width="100px" Text="" MaxLength="6"></asp:TextBox></td>
													<td>
														<asp:CompareValidator ID="tbPumpingHours_KeyPress" runat="server" Font-Size="8pt" ErrorMessage="Invalid Pumping Hours on Unit Time Info tab"
															ControlToValidate="tbPumpingHours" Display="Dynamic" Operator="DataTypeCheck" Type="Double" ToolTip="Pumping Hours Invalid Data">*</asp:CompareValidator>
														<asp:RangeValidator ID="tbPumpingHours_Validating" runat="server" Font-Size="8pt" ErrorMessage="Pumping Hours out of range"
															ControlToValidate="tbPumpingHours" Display="Dynamic" Type="Double" ToolTip="Pumping Hours out of range"
															MaximumValue="2208" MinimumValue="0">*</asp:RangeValidator></td>
													<td style="FONT-SIZE: 8pt">Maintenance Outage Hours</td>
													<td>
														<asp:TextBox ID="tbMOHours" runat="server" Font-Size="8pt" Width="100px" Text="" MaxLength="6"></asp:TextBox></td>
													<td>
														<asp:CompareValidator ID="tbMOHours_KeyPress" runat="server" Font-Size="8pt" ErrorMessage="Invalid Maintenance Outage Hours on Unit Time Info tab"
															ControlToValidate="tbMOHours" Display="Dynamic" Operator="DataTypeCheck" Type="Double" ToolTip="MO Hours Invalid Data">*</asp:CompareValidator>
														<asp:RangeValidator ID="tbMOHours_Validating" runat="server" Font-Size="8pt" ErrorMessage="Maintenance Outage Hours out of range"
															ControlToValidate="tbMOHours" Display="Dynamic" Type="Double" ToolTip="Maintenance Outage Hours out of range"
															MaximumValue="2208" MinimumValue="0">*</asp:RangeValidator></td>
												</tr>
												<tr>
													<td></td>
													<td style="FONT-SIZE: 8pt">Synchronous<br />
														Condensing Hours</td>
													<td>
														<asp:TextBox ID="tbSynCondHours" runat="server" Font-Size="8pt" Width="100px" Text="" MaxLength="6"></asp:TextBox></td>
													<td>
														<asp:CompareValidator ID="tbSynCondHours_KeyPress" runat="server" Font-Size="8pt" ErrorMessage="Invalid Synchronous Condensing Hours on Unit Time Info tab"
															ControlToValidate="tbSynCondHours" Display="Dynamic" Operator="DataTypeCheck" Type="Double" ToolTip="Syn Cond Hours Invalid Data">*</asp:CompareValidator>
														<asp:RangeValidator ID="tbSynCondHours_Validating" runat="server" Font-Size="8pt" ErrorMessage="Synchronous Condensing Hours out of range"
															ControlToValidate="tbSynCondHours" Display="Dynamic" Type="Double" ToolTip="Synchronous Condensing Hours out of range"
															MaximumValue="2208" MinimumValue="0">*</asp:RangeValidator></td>
													<td style="FONT-SIZE: 8pt">Extension of Scheduled<br />
														Outages Hours</td>
													<td>
														<asp:TextBox ID="tbSEHours" runat="server" Font-Size="8pt" Width="100px" Text="" MaxLength="6"></asp:TextBox></td>
													<td>
														<asp:CompareValidator ID="tbSEHours_KeyPress" runat="server" Font-Size="8pt" ErrorMessage="Invalid Extension of Scheduled Outage Hours on Unit Time Info tab"
															ControlToValidate="tbSEHours" Display="Dynamic" Operator="DataTypeCheck" Type="Double" ToolTip="SE Hours Invalid Data">*</asp:CompareValidator>
														<asp:RangeValidator ID="tbSEHours_Validating" runat="server" Font-Size="8pt" ErrorMessage="Schedule Extension Hours out of range"
															ControlToValidate="tbSEHours" Display="Dynamic" Type="Double" ToolTip="Schedule Extension Hours out of range"
															MaximumValue="2208" MinimumValue="0">*</asp:RangeValidator></td>
												</tr>
												<tr>
													<td></td>
													<td></td>
													<td></td>
													<td></td>
													<td></td>
													<td></td>
													<td style="FONT-SIZE: 8pt">PERIOD HOURS</td>
												</tr>
												<tr>
													<td></td>
													<td style="FONT-SIZE: 8pt">AVAILABLE HOURS</td>
													<td style="text-align: right">
														<asp:Label ID="lblAvailHoursValue" runat="server" Font-Size="8pt" Text="000.00"></asp:Label></td>
													<td></td>
													<td style="FONT-SIZE: 8pt">UNAVAILABLE HOURS</td>
													<td style="text-align: right">
														<asp:Label ID="lblUnavailHoursValue" runat="server" Font-Size="8pt" Text="000.00"></asp:Label></td>
													<td>
														<asp:TextBox ID="tbPeriodHours" runat="server" Font-Size="8pt" Width="100px" Text="" MaxLength="6"></asp:TextBox></td>
													<td>&nbsp;</td>
													<td>
														<asp:CompareValidator ID="tbPeriodHours_KeyPress" runat="server" Font-Size="8pt" ErrorMessage="Invalid Period Hours on Unit Time Info tab"
															ControlToValidate="tbPeriodHours" Display="Dynamic" Operator="DataTypeCheck" Type="Integer" ToolTip="Period Hours Invalid Data">*</asp:CompareValidator>
													</td>
												</tr>
												<tr>
													<td></td>
													<td></td>
													<td>
														<asp:CustomValidator ID="lblAvailHoursValue_Validating" runat="server" Font-Size="8pt" ErrorMessage="Available Hours + Unavailable Hours must equal Period Hours"
															ControlToValidate="tbPeriodHours" Display="Dynamic" ToolTip="Available Hours + Unavailable Hours must equal Period Hours">*</asp:CustomValidator></td>
													<td></td>
													<td></td>
													<td>
														<asp:CustomValidator ID="lblUnavailHoursValue_Validating" runat="server" Font-Size="8pt" ErrorMessage="Available Hours + Unavailable Hours must equal Period Hours"
															ControlToValidate="tbPeriodHours" Display="Dynamic" ToolTip="Available Hours + Unavailable Hours must equal Period Hours">*</asp:CustomValidator></td>
												</tr>
												<tr>
													<td></td>
													<td></td>
													<td></td>
													<td></td>
													<td></td>
													<td style="FONT-SIZE: 8pt; text-align: right;">
														<asp:Label runat="server" ID="Label47">Inactive Hours<br />
																		(IR, MB, RU)</asp:Label></td>
													<td>
														<asp:TextBox ID="tbInactiveHours" Text="" Font-Size="8pt" MaxLength="6" Width="100px" runat="server"></asp:TextBox>
													</td>
												</tr>
												<tr>
													<td></td>
													<td></td>
													<td></td>
													<td></td>
													<td></td>
													<td colspan="2" style="FONT-SIZE: 8pt; text-align: right;">
														<asp:Label runat="server" ID="Label49">AH + UAH + IH must equal PH</asp:Label></td>
												</tr>
											</table>

											<table id="CreateHoursButtonTable">
												<tr>
													<td>&nbsp; &nbsp;</td>
													<td style="FONT-WEIGHT: bold; FONT-SIZE: 8pt">Auto Fill uses available event data 
																		to fill in Hours fields.<br />
														You must enter ALL events including RS.</td>
													<td>&nbsp; &nbsp;</td>
													<td>
														<asp:Button ID="btnCreateHours" runat="server" Text="Auto Fill"
															Font-Size="8pt"></asp:Button></td>
												</tr>
												<tr>
													<td>&nbsp;</td>
													<td>
														<asp:CompareValidator ID="tbInactiveHours_KeyPress" runat="server" ErrorMessage="Invalid Inactive Hours on Unit Time Info tab"
															ControlToValidate="tbInactiveHours" Display="Dynamic" Operator="DataTypeCheck" ToolTip="Inactive Hours Invalid Data"
															Type="Double" Font-Size="8pt">*</asp:CompareValidator>
														<asp:RangeValidator ID="tbInactiveHours_Validating" runat="server" Font-Size="8pt" ErrorMessage="Inactive Hours out of range for indicated month"
															ControlToValidate="tbInactiveHours" Display="Dynamic" Type="Double" ToolTip="Inactive Hours out of range for indicated month"
															MaximumValue="745" MinimumValue="0">*</asp:RangeValidator></td>
												</tr>
											</table>

											<table id="ExplainTable">
												<tr>
													<td>&nbsp; &nbsp;</td>
												</tr>
												<tr>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
												</tr>
												<tr>
													<td>&nbsp; &nbsp;</td>
												</tr>
											</table>

										</div>

									</asp:View>

								</asp:MultiView>
							</div>
						</div>
						
					</asp:View>
					<%--===============================================================================--%>
					<asp:View ID="ErrorCheckOutputTab" runat="Server">

						<table style="width: 100%; height: 561px">
							<tr style="height: 100%">
								<td style="vertical-align: top; text-align: center; width: 15%; height: 100%;">
									<br />
									<p>
										<asp:Label ID="Label22" Font-Size="8pt" runat="server">The Final Validation performs a consistency error check between the Event and Performance data and is required before you can write out the NERC and ISO files</asp:Label>
									</p>
									<br />
									<p>
										<asp:Label ID="Labe2l22" Font-Size="8pt" runat="server">Press Error Check button</asp:Label>
									</p>
									<asp:Button ID="btnBatchCheck" runat="server" Text="Error Check"></asp:Button><br />
									&nbsp;
												<br />
									<asp:Button ID="btnPrintErrors" runat="server" Text="Print"></asp:Button><br />
									&nbsp;
												<br />
									<asp:Label ID="Label23" Font-Size="8pt" Font-Bold="True" runat="server" Font-Underline="True">Level</asp:Label><br />
									<asp:Label ID="Label24" Font-Size="8pt" runat="server">E - Error</asp:Label><br />
									<asp:Label ID="Label25" Font-Size="8pt" runat="server">M - Missing record</asp:Label><br />
									<asp:Label ID="Label26" Font-Size="8pt" runat="server">W - Warning</asp:Label></td>
								<td style="height: 100%">
									<asp:Panel ID="pnldgErrors" Style="OVERFLOW: scroll" Width="100%" Height="100%" runat="server">
										<asp:DataGrid ID="dgErrors" runat="server" Font-Size="9pt" Width="100%" Height="100%" Font-Names="Microsoft Sans Serif"
											PageSize="9999" AllowSorting="True" DataKeyField="UtilityUnitCode"
											CellPadding="5" AutoGenerateColumns="False">
											<Columns>
												<asp:TemplateColumn SortExpression="UnitShortName" ItemStyle-Wrap="False" HeaderStyle-Wrap="False" ItemStyle-HorizontalAlign="Left"
													ItemStyle-BackColor="white" HeaderText="Unit">
													<ItemTemplate>
														<asp:Label ID="UnitShortName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"UnitShortName") %>'>
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn SortExpression="Period" ItemStyle-Wrap="False" HeaderStyle-Wrap="False" ItemStyle-HorizontalAlign="Center"
													ItemStyle-BackColor="white" HeaderText="Month">
													<ItemTemplate>
														<asp:Label ID="Period" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Period") %>'>
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn SortExpression="Year" ItemStyle-Wrap="False" HeaderStyle-Wrap="False" ItemStyle-HorizontalAlign="Right"
													ItemStyle-BackColor="white" HeaderText="Year">
													<ItemTemplate>
														<asp:Label ID="Year" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Year") %>'>
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn SortExpression="EventNumber" ItemStyle-Wrap="False" HeaderStyle-Wrap="False" ItemStyle-HorizontalAlign="Right"
													ItemStyle-BackColor="white" HeaderText="Event">
													<ItemTemplate>
														<asp:Label ID="EventNumberError" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"EventNumber") %>'>
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn SortExpression="EventEvenCardNo" ItemStyle-Wrap="False" HeaderStyle-Wrap="False"
													ItemStyle-HorizontalAlign="Right" ItemStyle-BackColor="white" HeaderText="Card No">
													<ItemTemplate>
														<asp:Label ID="EventEvenCardNo" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"EventEvenCardNo") %>'>
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn SortExpression="ErrorSeverity" ItemStyle-Wrap="False" HeaderStyle-Wrap="False" ItemStyle-HorizontalAlign="Center"
													ItemStyle-BackColor="white" HeaderText="Level">
													<ItemTemplate>
														<asp:Label ID="ErrorSeverityLevel" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"ErrorSeverity") %>'>
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn SortExpression="ErrorMessage" ItemStyle-Wrap="False" HeaderStyle-Wrap="False" ItemStyle-HorizontalAlign="Left"
													ItemStyle-BackColor="white" HeaderText="Error Description">
													<ItemTemplate>
														<asp:Label ID="ErrorMessage" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"ErrorMessage") %>'>
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
											</Columns>
										</asp:DataGrid>
									</asp:Panel>
								</td>
							</tr>
						</table>

					</asp:View>
				</asp:MultiView>

			</asp:Panel>
			<asp:ValidationSummary ID="ValidationSummaryMain" Style="FONT-WEIGHT: bold; FONT-SIZE: 8pt; FONT-FAMILY: verdana"
				runat="server" HeaderText="The following errors exist"></asp:ValidationSummary>
		</div>
	</form>
</body>
</html>
