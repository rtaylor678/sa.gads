﻿Imports PortalNYISO.Process

Public Class ProcessView
    Inherits System.Web.UI.Page

    Public Enum ProcessStatus 'must be updated in WebProcessEnum table
        Queued = 0        'not yet processed
        Working           'processing
        Completed         'finished processing
        Rejected          'error found
        None              'when a user has never started a process or no process queued
        Aborted           'user cancelled queued item
        Invalid           'returned when requesting info on an invalid workid
        InvalidFileFormat 'for FileCheck type
    End Enum

    Private _WorkerStatus As ProcessInfo
    Private _Worker As New WorkManager(Server.MapPath("~"))
    'Private _Worker As New WorkManager(Application("AppPath").ToString)

    Private _WorkID As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        _WorkID = Integer.Parse(Request.QueryString("WorkID"))

        UpdateStatusRow()

        EditButton.Attributes.Add("onmouseup", "parent.window.location='DEApp.aspx'")
    End Sub

    Private Sub UpdateStatusRow()

        _WorkerStatus = _Worker.GetProcessInfo(_WorkID)

        TypeLabel.Text = _WorkerStatus.ProcessTypeDesc

        StatusLabel.Text = _WorkerStatus.StatusDesc

        AbortButton.Visible = False    'default to false
        EditButton.Visible = False

        Select Case _WorkerStatus.Status
            Case ProcessStatus.None
                StatusLabel.ForeColor = Drawing.Color.Black
            Case ProcessStatus.Queued
                StatusLabel.ForeColor = Drawing.Color.Black
                AbortButton.Visible = True
            Case ProcessStatus.Working
                StatusLabel.ForeColor = Drawing.Color.DarkBlue
            Case ProcessStatus.Rejected
                StatusLabel.ForeColor = Drawing.Color.DarkRed
                EditButton.Visible = True
            Case ProcessStatus.Completed
                StatusLabel.ForeColor = Drawing.Color.DarkGreen
            Case ProcessStatus.Invalid, ProcessStatus.InvalidFileFormat
                StatusLabel.ForeColor = Drawing.Color.DarkRed
        End Select

        If _WorkerStatus.Status <> ProcessStatus.None Then
            InitiatedLabel.Text = _WorkerStatus.Initiated.ToShortDateString & " " & _WorkerStatus.Initiated.ToShortTimeString
        Else
            InitiatedLabel.Text = "N/A"
        End If

    End Sub

    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)

        writer.RenderBeginTag(HtmlTextWriterTag.Title)

        Dim myUser As String = String.Empty

        'Dim cert As HttpClientCertificate
        'cert = Request.ClientCertificate
        'Dim strTemp As String

        'If cert.IsPresent Then

        '    If cert.IsValid Then

        '        strTemp = cert.Get("SUBJECTOU").ToString

        '        If strTemp.IndexOf("-") > 0 Then
        '            myUser = strTemp.Substring(strTemp.IndexOf("-") + 1).Trim
        '        Else
        '            myUser = "INVALID USER"
        '        End If
        '    Else
        '        myUser = "Invalid Cert"
        '    End If
        'Else
        '    myUser = "Cert NOT Present"
        'End If

        'writer.Write("GADS NxL (" & myUser & ")")
        writer.Write("NYISO GADS Portal")
        writer.RenderEndTag()

        'only add refresh for iframes for those items that can change status
        If _WorkerStatus.Status = ProcessStatus.Queued OrElse _WorkerStatus.Status = ProcessStatus.Working Then
            'writer.Write("<meta http-equiv=refresh content=60>")
            writer.Write("<meta http-equiv=""refresh"" content=""60"">")
        End If

        MyBase.Render(writer)

    End Sub

    Private Sub AbortButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AbortButton.Click
        _Worker.AbortItem(_WorkerStatus.WorkID)
        UpdateStatusRow()
    End Sub

    Private Sub Page_Error(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Error
        'makes main window redirect instead of messing up iframe contents
        Response.Write("<script>window.open('../GeneralError.aspx','_top');</script>")
    End Sub
End Class