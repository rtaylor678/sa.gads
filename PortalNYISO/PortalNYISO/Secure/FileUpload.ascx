<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="FileUpload.ascx.vb" Inherits="PortalNYISO.Controls.FileUpload" %>
<div id="ControlFrame" style="OVERFLOW: visible; WIDTH: 1056px; POSITION: relative; HEIGHT: 24px"
    ms_positioning="GridLayout" runat="server">
    <asp:Image ID="StatusImage" Style="Z-INDEX: 101; LEFT: 8px; POSITION: absolute; TOP: 0px" Height="20px"
        runat="server" Width="20px" ImageUrl="../images/UncheckedFile.gif" EnableViewState="False"></asp:Image>
    <input id="FileInput" runat="server" style="Z-INDEX: 102; LEFT: 32px; WIDTH: 424px; POSITION: absolute; TOP: 0px; HEIGHT: 22px"
        type="file" size="51" name="File0">
    <asp:Label ID="StatusLabel" Style="Z-INDEX: 103; LEFT: 464px; POSITION: absolute; TOP: 0px"
        runat="server" Visible="False" Width="544px" ForeColor="DarkRed" CssClass="Message">Error</asp:Label>
</div>

