'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace Controls

    Partial Public Class FileUpload

        '''<summary>
        '''ControlFrame control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents ControlFrame As Global.System.Web.UI.HtmlControls.HtmlGenericControl

        '''<summary>
        '''StatusImage control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents StatusImage As Global.System.Web.UI.WebControls.Image

        '''<summary>
        '''FileInput control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents FileInput As Global.System.Web.UI.HtmlControls.HtmlInputFile

        '''<summary>
        '''StatusLabel control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents StatusLabel As Global.System.Web.UI.WebControls.Label
    End Class
End Namespace
