﻿Public Class CheckResults
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim dsResults As DataSet

        dsResults = CType(Session("dsResults"), DataSet)
        Me.dgUnitStatus.DataSource = dsResults
        Me.dgUnitStatus.DataMember = "Setup"
        Me.dgUnitStatus.DataBind()
    End Sub

    Protected Sub btnReturn_Click(sender As Object, e As EventArgs) Handles btnReturn.Click
        Dim sScript As String
        sScript = ""

        sScript += "<script language='javascript'>"
        sScript += "window.close();"
        sScript += "</script>"

        Response.Write(sScript)
    End Sub
End Class