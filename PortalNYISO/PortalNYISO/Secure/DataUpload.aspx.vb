﻿Imports System.Text.RegularExpressions
Imports System.IO
Imports PortalNYISO.Controls
Imports PortalNYISO.Process
Imports System.Security.Principal
Imports System.Web.HttpApplication
Imports System.Web.UI.HtmlControls.HtmlGenericControl
Imports ARWindowsUI.ARWindowsUI
Imports System.Threading
Imports System.Collections

Public Class DataUpload
    Inherits System.Web.UI.Page

    Private _WorkManager As WorkManager
    Private myUser As String
    Private StartPeriodsArray As New ArrayList
    Private EndPeriodsArray As New ArrayList
    Private strDTFormat As String = "MMMM yyyy"
    Private strMode As String
    Private boolCalcsStarted As Boolean = False

    Private _ProcessFiles As Hashtable

    Public Enum ProcessStatus 'must be updated in WebProcessEnum table
        Queued = 0        'not yet processed
        Working           'processing
        Completed         'finished processing
        Rejected          'error found
        None              'when a user has never started a process or no process queued 
        Aborted           'user cancelled queued item
        Invalid           'returned when requesting info on an invalid workid
        InvalidFileFormat 'for FileCheck type
    End Enum

    Public Enum FileUploadStatus 'used for upload file control
        NotValidated = 0  'displays question mark
        NoError
        Invalid           'displays red X
        Accepted          'displays checkmark
    End Enum

    Public Enum ProcessType 'must be updated in WebProcessEnum table
        FileCheck = 0  'ASCII test routines
        DataCheck      'ISO routines
        Analysis       'Data analysis routines
        None           'for invalid processes, requests, etc.
    End Enum

    Public Enum LogTypes 'must be updated in SQL table
        Information = 0  'misc info
        Alert            'possible failure or warning
        GeneralError     'error source unknown
        DBError          'error on database operation
        FileError        'error on file operation
        CodeError        'error in source code
        ThreadError      'error in worker thread
        WebError         'IIS or other related web errors
    End Enum

    Private Sub DataUpload_Init(sender As Object, e As EventArgs) Handles Me.Init
        Session("UploadCounter") = 1 'resets appended id to file upload controls
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Put user code to initialize the page here
        If IsPostBack Then
            FileUploadFrame.Style.Item("height") = DisplayHeight.Value
        End If
        'as soon as upload button is clicked, hide button, etc.

        myUser = Context.User.Identity.Name

        Try
            'UploadButton.Attributes.Add("onmouseup", "HideForUpload();" & Page.GetPostBackEventReference(UploadButton) & ";RefreshImage('UploadingImage');Uploading=true;")
            ' ClientScript.GetPostBackEventReference

            UploadButton.Attributes.Add("onmouseup", "HideForUpload();" & ClientScript.GetPostBackEventReference(UploadButton, "") & ";RefreshImage('UploadingImage');Uploading=true;")

            _WorkManager = New WorkManager(Application("AppPath").ToString)

            UpdateProcesses()
            HistoryRepeater.DataSource = _WorkManager.GetHistoryList(myUser)
            HistoryRepeater.DataBind()

            If Not IsPostBack Then

                'Me.comboEndPeriod.Items.Clear()
                'Me.comboStartPeriod.Items.Clear()

                StartPeriodsArray.Clear()
                EndPeriodsArray.Clear()

                Dim connect As String = Application("AppPath").ToString
                Dim blConnect As New Calcs(Application("AppPath").ToString)

                Dim rdrGetPeriods As IDataReader
                rdrGetPeriods = blConnect.GetMyPeriodsList("Monthly")

                While rdrGetPeriods.Read

                    If Not rdrGetPeriods.IsDBNull(0) Then
                        StartPeriodsArray.Add(New StartList(rdrGetPeriods.GetDateTime(0).ToString(strDTFormat), rdrGetPeriods.GetDateTime(0)))
                        EndPeriodsArray.Add(New EndList(rdrGetPeriods.GetDateTime(0).ToString(strDTFormat), rdrGetPeriods.GetDateTime(0)))
                    End If

                End While

                rdrGetPeriods.Close()
                rdrGetPeriods.Dispose()

                ' This initiates the Analysis from the Data Entry form

                strMode = Request.QueryString("choice")

                If Not boolCalcsStarted Then

                    If strMode = "CALC" Then
                        '_WorkManager.AddProcess(myUser, ProcessType.DataCheck)
                        _WorkManager.AddProcess(myUser, ProcessType.Analysis)
                        UpdateProcesses()
                        boolCalcsStarted = True

                    End If

                End If

            End If

        Catch ex As Exception

            Dim sScript As String = String.Empty
            sScript = "<script language='javascript'>"
            sScript += "alert('" & ex.ToString & "');"
            sScript += "</script>"

            Try
                Response.Write(sScript)
            Catch exAlert As System.Exception
                sScript = exAlert.ToString
            End Try

        End Try

    End Sub

    Private Sub UpdateProcesses()

        Dim NewRows As ArrayList
        NewRows = _WorkManager.GetProcessList(Context.User.Identity.Name)

        StatusRepeater.DataSource = NewRows
        StatusRepeater.DataBind()

    End Sub

    Private Sub CheckFiles()

        Dim FileNames As String = ""
        Dim FileControl As FileUpload
        Dim FileName As String

        Me.UploadButton.Enabled = False
        Me.UploadingImage.Visible = True
        Me.UploadingImage.DataBind()
        Me.UploadingImage.ImageUrl = "../images/processing.gif"
        Me.UploadingLabel.Enabled = True
        Me.UploadingLabel.Visible = True
        Me.UploadingLabel.DataBind()

        Dim connect As String = Application("AppPath").ToString

        Dim blConnect As New ARWindowsUI.ARWindowsUI.Calcs(Application("AppPath").ToString)

        Dim intSleepSeconds As Integer = 0

        intSleepSeconds = blConnect.GetAnalysisSettingInt("THREADSLEEP", "Seconds", -1)

        If intSleepSeconds > 0 Then
            intSleepSeconds *= 1000
        ElseIf intSleepSeconds < 0 Then
            blConnect.SaveAnalysisSettingStr("THREADSLEEP", "Seconds", 0)
            intSleepSeconds = 0
        End If

        blConnect = Nothing

        'add filenames to file name list
        For Each NewControl As Control In UploadMultipleFileContainer.Controls        'each new control is a FileUpload User Control

            If TypeName(NewControl).Contains("fileupload_ascx") Then  '"FileUpload_ascx" -> "secure_fileupload_ascx"

                FileControl = CType(NewControl, FileUpload)

                If FileControl.FileName <> "" Then
                    FileNames += Path.GetFileName(FileControl.FileName) & ","
                    ProcessButton.Enabled = True
                End If

            End If

        Next

        'check for duplicates

        Dim RegEx As Regex

        Dim intNumberOfFiles As Integer = UploadMultipleFileContainer.Controls.Count

        For Each NewControl As Control In UploadMultipleFileContainer.Controls        'each new control is a FileUpload User Control

            If TypeName(NewControl).Contains("fileupload_ascx") Then  '"FileUpload_ascx" -> "secure_fileupload_ascx"

                FileControl = CType(NewControl, FileUpload)

                FileName = FileControl.FileName

                If Not FileControl Is Nothing AndAlso FileControl.Status <> FileUploadStatus.Accepted Then

                    If FileName <> "" Then

                        RegEx = New Regex(Path.GetFileName(FileName))

                        If RegEx.Matches(FileNames).Count > 1 Then                       'duplicate filenames on same upload

                            FileControl.Status = FileUploadStatus.Invalid
                            FileControl.SetMessage(Chr(34) & Path.GetFileName(FileName) & Chr(34) & " was entered more than once.", True)

                        Else

                            'if file accepted, store posted file
                            Dim SaveFile As String

                            'For Index As Integer = 0 To Request.Files.Count - 1
                            Dim FileInput As HtmlInputFile
                            FileInput = CType(FileControl.FindControl("FileInput"), HtmlInputFile)

                            If FileInput.PostedFile.FileName <> "" Then

                                Try
                                    ' need to get ID from certificate

                                    ' Create a separate folder for your uploaded content and change the NTFS file permissions on the upload folder

                                    ' By doing this, you can configure the behavior of uploaded content differently from the rest of your Web application. 
                                    ' Grant the upload folder Read and Write permissions for the IIS worker process identity. For IIS 6.0 in Windows Server 2003, 
                                    ' you can use the IIS_WPG user group for this. For IIS 7.0 and later, you can use the IIS_IUSRS user group. 

                                    ' It is important not to grant Script Permissions on this folder.
                                    ' To disable script permissions in configuration for IIS 7.0 and later versions, you have to set the accessPolicy flag on the handlers section not to have the Script  value. 


                                    'SaveFile = Server.MapPath("~\UploadFiles\") & RegEx.Replace(myUser, "[\\/]", ".") & "_" & Format(Date.Now, "yyyy-MM-dd HH.mm.ss") & "_" & Path.GetFileName(FileName)
                                    SaveFile = Server.MapPath("~/UploadFiles/") & RegEx.Replace(myUser, "[\\/]", ".") & "_" & Format(Date.Now, "yyyy-MM-dd HH.mm.ss") & "_" & Path.GetFileName(FileName)
                                    _WorkManager.AddProcess(myUser, ProcessType.FileCheck, SaveFile)
                                    FileInput.PostedFile.SaveAs(SaveFile)
                                    FileControl.FileName = ""                                    'this file will no longer be checked for a duplicate name becuase it was accepted and saved as a different name
                                    FileControl.LongFileName = SaveFile
                                    'add to queue
                                    FileControl.Status = FileUploadStatus.Accepted
                                    FileControl.SetMessage(Chr(34) & Path.GetFileName(FileName) & Chr(34) & " was accepted.", False)
                                    UpdateProcesses()

                                    If intSleepSeconds > 0 AndAlso intNumberOfFiles > 1 Then

                                        ' 20,000 msec works for EUCG based on March 2009 testing
                                        Thread.Sleep(intSleepSeconds)

                                    End If

                                Catch ex As Exception
                                    FileControl.Status = FileUploadStatus.Invalid
                                    FileControl.SetMessage("Error: Unable to save file on server.", True)

                                    If IsNothing(ex.InnerException.Message) Then
                                        _WorkManager.LogMessageToDB(LogTypes.FileError, "Error saving uploaded file.", ex.Message)
                                    Else
                                        _WorkManager.LogMessageToDB(LogTypes.FileError, "Error saving uploaded file.", ex.Message, ex.InnerException.Message)
                                    End If

                                End Try

                            End If

                        End If

                    Else
                        FileControl.Status = FileUploadStatus.NotValidated
                        FileControl.SetMessage("", False)
                    End If

                End If

            End If

        Next

        Me.UploadingImage.Visible = False
        Me.UploadingLabel.Enabled = False
        Me.UploadButton.Enabled = True

    End Sub

    Private Sub UploadButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UploadButton.Click
        CheckFiles()
    End Sub

    Private Sub ProcessButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ProcessButton.Click
        _WorkManager.AddProcess(myUser, ProcessType.DataCheck)
        UpdateProcesses()
    End Sub

    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        writer.RenderBeginTag(HtmlTextWriterTag.Title)
        'writer.Write("GADS NxL (" & myUser & ")")
        writer.Write("NYISO GADS Portal")
        writer.RenderEndTag()
        MyBase.Render(writer)
    End Sub

    Private Sub Page_Error(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Error
        Response.Write("<script>window.open('GeneralError.aspx','_top');</script>")
        Response.End()
    End Sub

    Private Sub btnCurrentMonth_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCurrentMonth.Click
        HistoryRepeater.DataSource = _WorkManager.GetHistoryList(myUser)
        HistoryRepeater.DataBind()
    End Sub

    Private Sub btnLast2Months_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLast2Months.Click
        HistoryRepeater.DataSource = _WorkManager.GetHistoryList2(myUser)
        HistoryRepeater.DataBind()
    End Sub

    Private Sub ProcessRefreshButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ProcessRefreshButton.Click
        RefreshListboxes()
        'Page.DataBind()
    End Sub

    Private Sub RefreshListboxes()

        StartPeriodsArray.Clear()
        EndPeriodsArray.Clear()

        Dim connect As String
        connect = Application("AppPath").ToString

        Dim blConnect As New Calcs(Application("AppPath").ToString)

        Dim rdrGetPeriods As IDataReader = Nothing

        Try
            rdrGetPeriods = blConnect.GetMyPeriodsList("Monthly")

            While rdrGetPeriods.Read

                If Not rdrGetPeriods.IsDBNull(0) Then
                    StartPeriodsArray.Add(New StartList(rdrGetPeriods.GetDateTime(0).ToString(strDTFormat), rdrGetPeriods.GetDateTime(0)))
                    EndPeriodsArray.Add(New EndList(rdrGetPeriods.GetDateTime(0).ToString(strDTFormat), rdrGetPeriods.GetDateTime(0)))
                End If

            End While

        Catch ex As System.ArgumentOutOfRangeException

            Dim sScript As String = String.Empty
            sScript = "<script language='javascript'>"
            sScript += "alert('" & ex.ToString & "');"
            sScript += "</script>"

            Try
                Response.Write(sScript)
            Catch exAlert As System.Exception
                sScript = exAlert.ToString
            End Try

        Finally

            rdrGetPeriods.Close()
            rdrGetPeriods.Dispose()

        End Try

        'Page.DataBind()

    End Sub

    Private Sub btnDoOFReports_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Dim sScript As String = String.Empty
        sScript = "<script language='javascript'>"
        sScript += "var new_win=window.open('ReportsPage.aspx', 'child'); new_win.focus();"
        sScript += "</script>"
        Response.Write(sScript)

    End Sub

    Protected Sub btnUandP_Click(sender As Object, e As EventArgs) Handles btnUandP.Click
        'Response.Redirect("~\secure\DataUpload.aspx")
        Response.Redirect("~/secure/DataUpload.aspx")
    End Sub

    Protected Sub btnEditData_Click(sender As Object, e As EventArgs) Handles btnEditData.Click
        'Response.Redirect("~\secure\DEApp.aspx")
        Response.Redirect("~/secure/DEApp.aspx")
    End Sub

    Protected Sub btnReports_Click(sender As Object, e As EventArgs) Handles btnReports.Click
        'Response.Redirect("~\secure\ReportsPage.aspx")
        Response.Redirect("~/secure/ReportsPage.aspx")
    End Sub

    Protected Sub HelpIndex_Click(sender As Object, e As EventArgs) Handles HelpIndex.Click
        Dim sScript As String = String.Empty
        sScript = "<script language='javascript'>"
        sScript += "var new_win=window.open('HTML/default.htm', 'child'); new_win.focus();"
        sScript += "</script>"
        Response.Write(sScript)
    End Sub
End Class

Public Class StartList

    Private myPeriodDesc As String
    Private myPeriodValue As DateTime

    Public Sub New(ByVal strPeriodDesc As String, ByVal dtPeriodValue As DateTime)
        Me.myPeriodDesc = strPeriodDesc
        Me.myPeriodValue = dtPeriodValue
    End Sub

    Public ReadOnly Property PeriodDesc() As String
        Get
            Return myPeriodDesc
        End Get
    End Property

    Public ReadOnly Property PeriodValue() As DateTime
        Get
            Return myPeriodValue
        End Get
    End Property

End Class

Public Class EndList

    Private myPeriodDesc As String
    Private myPeriodValue As DateTime

    Public Sub New(ByVal strPeriodDesc As String, ByVal dtPeriodValue As DateTime)
        Me.myPeriodDesc = strPeriodDesc
        Me.myPeriodValue = dtPeriodValue
    End Sub

    Public ReadOnly Property PeriodDesc() As String
        Get
            Return myPeriodDesc
        End Get
    End Property

    Public ReadOnly Property PeriodValue() As DateTime
        Get
            Return myPeriodValue
        End Get
    End Property

End Class