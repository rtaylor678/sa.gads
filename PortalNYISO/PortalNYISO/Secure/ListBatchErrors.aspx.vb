﻿Imports BusinessLayer.BusinessLayer
Imports Microsoft.Reporting.WebForms
Imports System.Data.Common
Imports System.Data
Imports System.IO
Imports System.Web
Imports System.Collections

Public Class ListBatchErrors
    Inherits System.Web.UI.Page

    Public AppName As String = "GADSNG"
    Public connect As String = ""
    'Public blConnect As GADSNGBusinessLayer
    Public dtSetup As BusinessLayer.Setup.SetupDataTable
    Public drSetup As BusinessLayer.Setup.SetupRow
    Public dsCauseCodes As DataSet
    Public dtPerformance As BusinessLayer.Performance.PerformanceDataDataTable
    Public drPerformance As BusinessLayer.Performance.PerformanceDataRow
    Public dsAllEvents As AllEventDataISO
    Public dsGridEvents As DataSet
    Public dsErrors As DataSet
    Public dsFailMech As DataSet
    Public dsCauseCodeExt As DataSet
    Public dsSavedVerbDesc As DataSet
    Public dsRS As BusinessLayer.dsBulkRS
    Public intCurrentYear As Integer
    Public strCurrentUnit As String = ""
    Public strCurrentPeriod As String = ""
    Public strCurrentMonthNo As String = ""
    'Public intSECauseCode As Integer
    'Public dtSEBeginning As DateTime
    Public myUser As String = ""
    Public alPeriods As New ArrayList
    Public lbUnitsText As String = ""
    Public intYearLimit As Integer = 1980

    Public mydtErrors As New DataTable
    Public dsTemp As DataSet

    Private Sub ListBatchErrors_Load(sender As Object, e As EventArgs) Handles Me.Load

        RestoreFromSession()
        mydtErrors.Merge(dsErrors.Tables("Errors"))
        mydtErrors.TableName = "Errors"

        'ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/secure") & "\BatchErrors.rdlc"
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("BatchErrors.rdlc")
        ReportViewer1.LocalReport.DisplayName = "GADS Errors Report"
        ReportViewer1.LocalReport.DataSources.Clear()
        ReportViewer1.LocalReport.EnableExternalImages = True
        ReportViewer1.ProcessingMode = ProcessingMode.Local
        ReportViewer1.AsyncRendering = False
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.LocalReport.DataSources.Add(New ReportDataSource("dsErrors_Errors", mydtErrors))
        ReportViewer1.LocalReport.Refresh()

    End Sub

#Region " RestoreFromSession() "

    Public Sub RestoreFromSession()

        AppName = Session("AppName").ToString
        connect = Session("connect").ToString
        'blConnect = CType(Session("blConnect"), BusinessLayer.BusinessLayer.GADSNGBusinessLayer)
        dtSetup = CType(Session("dtSetup"), BusinessLayer.Setup.SetupDataTable)
        drSetup = CType(Session("drSetup"), BusinessLayer.Setup.SetupRow)
        dsCauseCodes = CType(Session("dsCauseCodes"), DataSet)
        dtPerformance = CType(Session("dtPerformance"), BusinessLayer.Performance.PerformanceDataDataTable)
        drPerformance = CType(Session("drPerformance"), BusinessLayer.Performance.PerformanceDataRow)
        dsAllEvents = CType(Session("dsAllEvents"), AllEventDataISO)
        dsGridEvents = CType(Session("dsGridEvents"), DataSet)
        dsFailMech = CType(Session("dsFailMech"), DataSet)
        dsCauseCodeExt = CType(Session("dsCauseCodeExt"), DataSet)
        dsSavedVerbDesc = CType(Session("dsSavedVerbDesc"), DataSet)
        dsRS = CType(Session("dsRS"), BusinessLayer.dsBulkRS)
        intCurrentYear = CType(Session("intCurrentYear"), Integer)
        strCurrentUnit = Session("strCurrentUnit").ToString
        strCurrentPeriod = Session("strCurrentPeriod").ToString
        strCurrentMonthNo = Session("strCurrentMonthNo").ToString
        'intSECauseCode = CType(Session("intSECauseCode"), Integer)
        'dtSEBeginning = CType(Session("dtSEBeginning"), DateTime)
        'myUser = Session("myUser").ToString
        myUser = Context.User.Identity.Name
        alPeriods = CType(Session("alPeriods"), ArrayList)
        lbUnitsText = Session("lbUnitsText").ToString
        intYearLimit = CType(Session("intYearLimit"), Integer)
        dsErrors = CType(Session("dsErrors"), DataSet)

    End Sub

#End Region

    Protected Sub btnPDF_Click(sender As Object, e As EventArgs)

        RestoreFromSession()
        mydtErrors.Clear()
        mydtErrors.Merge(dsErrors.Tables("Errors"))
        mydtErrors.TableName = "Errors"

        Dim warnings As Warning() = Nothing
        Dim streamids As String() = Nothing
        Dim mimeType As String = Nothing
        Dim encoding As String = Nothing
        Dim extension As String = Nothing
        Dim bytes As Byte()

        ReportViewer1.LocalReport.ReportPath = Server.MapPath("BatchErrors.rdlc")
        ReportViewer1.LocalReport.DisplayName = "GADS Errors Report"
        ReportViewer1.LocalReport.DataSources.Clear()
        ReportViewer1.LocalReport.EnableExternalImages = True
        ReportViewer1.ProcessingMode = ProcessingMode.Local
        ReportViewer1.AsyncRendering = False
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.LocalReport.DataSources.Add(New ReportDataSource("dsErrors_Errors", mydtErrors))

        bytes = ReportViewer1.LocalReport.Render("PDF", Nothing, mimeType, encoding, extension, streamids, warnings)

        ' Now that you have all the bytes representing the PDF report, buffer it and send it to the client.
        Response.Buffer = True
        Response.Clear()
        Response.ContentType = mimeType
        Response.AddHeader("content-disposition", "attachment; filename=GADS_Errors_Report." + extension)
        Response.BinaryWrite(bytes) ' create the file
        Response.Flush() ' send it to the client to download

    End Sub

   
    Protected Sub btnExcel_Click(sender As Object, e As EventArgs)
        RestoreFromSession()
        mydtErrors.Clear()
        mydtErrors.Merge(dsErrors.Tables("Errors"))
        mydtErrors.TableName = "Errors"

        Dim warnings As Warning() = Nothing
        Dim streamids As String() = Nothing
        Dim mimeType As String = Nothing
        Dim encoding As String = Nothing
        Dim extension As String = Nothing
        Dim bytes As Byte()

        ReportViewer1.LocalReport.ReportPath = Server.MapPath("BatchErrors.rdlc")
        ReportViewer1.LocalReport.DisplayName = "GADS Errors Report"
        ReportViewer1.LocalReport.DataSources.Clear()
        ReportViewer1.LocalReport.EnableExternalImages = True
        ReportViewer1.ProcessingMode = ProcessingMode.Local
        ReportViewer1.AsyncRendering = False
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.LocalReport.DataSources.Add(New ReportDataSource("dsErrors_Errors", mydtErrors))

        bytes = ReportViewer1.LocalReport.Render("Excel", Nothing, mimeType, encoding, extension, streamids, warnings)

        ' Now that you have all the bytes representing the Excel report, buffer it and send it to the client.
        Response.Buffer = True
        Response.Clear()
        Response.ContentType = mimeType
        Response.AddHeader("content-disposition", "attachment; filename=GADS_Errors_Report." + extension)
        Response.BinaryWrite(bytes) ' create the file
        Response.Flush() ' send it to the client to download
    End Sub
End Class