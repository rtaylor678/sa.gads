﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes
<Assembly: AssemblyTitle("NYISO GADS Portal")> 
<Assembly: AssemblyDescription("Portal for uploading GADS data to NYISO")>
<Assembly: AssemblyCompany("Engineering Data Technologies")>
<Assembly: AssemblyProduct("NYISO GADS Portal")> 
<Assembly: AssemblyCopyright("Copyright © 2014")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(True)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("a125e7eb-5cdc-4869-8b34-83b9e551fd20")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("17.7.5.0")>
<Assembly: AssemblyFileVersion("17.7.5.0")>
