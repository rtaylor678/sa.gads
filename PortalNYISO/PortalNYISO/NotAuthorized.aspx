﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="NotAuthorized.aspx.vb" Inherits="PortalNYISO.NotAuthorized" Debug="false" Trace="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Not Authorized</title>
    <link href="Styles.css" type="text/css" rel="stylesheet" />
</head>
<body>
    <h1 style="text-align: center">NYISO GADS portal</h1>
    <h2 style="text-align: center">Not Authorized.</h2>
    <form id="form1" runat="server">
        <table style="text-align: center; width: 100%; font-weight: bold;">
            <tr  >
                <td >Not authorized to access the selected resource.
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
