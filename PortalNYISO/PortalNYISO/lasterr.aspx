﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="lasterr.aspx.vb" Inherits="PortalNYISO.lasterr" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ERROR IN APPLICATION</title>
    <link href="Styles.css" type="text/css" rel="stylesheet" />
</head>
<body>
    <h1 style="align-content: center">NYISO GADS Portal</h1>
    <h2 style="align-content: center">ERROR IN APPLICATION</h2>
    <form id="form1" runat="server">
        <table style="align-content: center">
            <tr>
                <td>
                    <asp:Label ID="lblErrorMessage" runat="server">No Errors</asp:Label>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
