﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="MainDE.aspx.vb" Inherits="PortalNYISO.MainDE" Debug="false" Trace="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>NYISO GADS Portal</title>
</head>
<body>
    <form id="form1" runat="server">
        <table>
            <tr>
                <td>
                    <asp:Image ID="Image1" ImageUrl="images/logo.gif" runat="server" AlternateText="NYISO: New York Independent System Operator" />
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="2" style="font-family: Arial; font-size: large; height: 24px;">Generating Availability Data System (<strong>GADS</strong>) Portal
                </td>
                <%--<td>&nbsp;
                </td>--%>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label Font-Names="Arial" Font-Size="Medium" ID="lblName" runat="server" /><br />

                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="UploadButton" runat="server" Height="24" Width="150" Text="Upload &amp; Process"></asp:Button>
                </td>
                <td style="font-family: Arial; font-size: medium; height: 24px;">Upload GADS ASCII files to the NYISO server, process uploaded files or check status
                    of processing.
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr>

            <tr>
                <td>
                    <asp:Button ID="EditButton" runat="server" Height="24" Width="150" Text="Edit Data" />
                </td>
                <td style="font-family: Arial; font-size: medium; height: 24px;">View and correct uploaded data.
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="ReportButton" runat="server" Height="24" Width="150" Text="Reports" />
                </td>
                <td style="font-family: Arial; font-size: medium; height: 24px;">View reports from results of last processed files.
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label Font-Names="Arial" Font-Size="Small" ID="lblAuthType" runat="server" /></td>
            </tr>
        </table>

    </form>
</body>
</html>
